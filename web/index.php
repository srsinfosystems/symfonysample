<?php

// ini_set('max_execution_time', '120');
require_once(dirname(__FILE__).'/../config/ProjectConfiguration.class.php');


$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'prod', true);
sfContext::createInstance($configuration)->dispatch();
