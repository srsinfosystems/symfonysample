var a_global_liarary = {
	
	initReportColumn: function (selectId,checkboxClass) {

        var options = '';
        var firstOrder = [];
        var ct = 0;
        var oldorder = [];
         
        $("."+checkboxClass).each(function (chkbox) {

            firstOrder[$(this).val()] = $(this).attr('data-position');
            if ($(this).attr('data-position')) {
                ct++;
            }
            oldorder.push({val: $(this).val(), text: $(this).parent().find('label').text(), selected: $(this).is(":checked")});
        })
        

               if(selectId != "select2reports"){
	        $("."+checkboxClass).each(function (chkbox) {
	            $(this).parent("li").parent("ul.checkbox_list").hide();
	        });
	    }

        var neworder = [];
        for (var r = 0; r < oldorder.length; r++) {
            if (firstOrder[oldorder[r].val]) {
                var kk = firstOrder[oldorder[r].val];
            } else {
                var kk = ct;
                ct++;
            }
            neworder[kk] = oldorder[r];
        }
        for (var newod in neworder) {

            if (neworder[newod]['selected']) {
                options += '<option selected value="' + neworder[newod]['val'] + '">' + neworder[newod]['text'] + '</option>';
            } else {
                options += '<option value="' + neworder[newod]['val'] + '">' + neworder[newod]['text'] + '</option>';
            }
        }
        $('#'+selectId).html(options);
    },
    initReportSelect: function (selectId, checkboxId, checkboxClass) {

        setTimeout(function () {
            $('#'+selectId).select2('destroy');
            var printselect = $('#'+selectId).select2Sortable({width: '100%'});

            var opts = $('#'+selectId).val();

            if(selectId == "select2DoctorProfile"){ 
                printselect.on("change", function (e) {
                if(e.val.length > 1){

                    if (e.removed) {
                        $("#"+checkboxId + e.removed.id).trigger('click');
                        $("#"+checkboxId + "all").attr('checked', false);
                    }
                    if (e.added) {
                        if(e.added.id == "all"){ 
                            var skipper = $("#"+checkboxId + "all");
                            $("."+checkboxClass+":checkbox").not(skipper).attr("checked",false);
                            $("#"+checkboxId + e.added.id).trigger('click');

                        }else{
                            $("#"+checkboxId + "all").attr('checked', false);
                            $("#"+checkboxId + e.added.id).trigger('click');

                            //$("#"+checkboxId + e.added.id).attr('checked', true);
                        }
                    } 
                }

                if (e.removed) {
                    $("#"+checkboxId + e.removed.id).attr('checked', false);

                }
                if (e.added) {
                    $("#"+checkboxId + e.added.id).attr('checked', true);
                }
                $('#'+selectId).html('');
                $('#'+selectId).select2('destroy');
                a_global_liarary.initReportColumn(selectId, checkboxClass);
                $('#'+selectId).select2Sortable({width: '100%'});

              });
            }else if(selectId == "select2reports"){
                printselect.on("change", function (e) {

                if (e.removed) {
                    $("#"+checkboxId + e.removed.id).attr('checked', false);
                }
                if (e.added) {
                    $("#"+checkboxId + e.added.id).attr('checked', true);
                }
                $('#'+selectId).html('');
                $('#'+selectId).select2('destroy');
                //            printselect.val(null).trigger('change');
                a_global_liarary.initReportColumn(selectId, checkboxClass);
                $('#'+selectId).select2Sortable({width: '100%'});
                //            printselect.trigger("change");

              });
            }else{
            printselect.on("change", function (e) {
                
                if(e.val.length > 1){

                    if (e.removed) {
                        $("#"+checkboxId + "all").attr('checked', false);
                    }
                    if (e.added) {
                        if(e.added.id == "all"){
                            var skipper = $("#"+checkboxId + "all");
                            $(":checkbox").not(skipper).prop("checked",false);
                        }else
                            $("#"+checkboxId + "all").attr('checked', false);
                    } 
                }

                if (e.removed) {

                    $("#"+checkboxId + e.removed.id).attr('checked', false);
                    $("#"+checkboxId + e.removed.id).trigger('click');
                    $("#"+checkboxId + e.removed.id).attr('checked', false);
                }
                if (e.added) {
                    $("#"+checkboxId + e.added.id).attr('checked', true);
                    $("#"+checkboxId + e.added.id).trigger('click');
                    $("#"+checkboxId + e.added.id).attr('checked', true);
                }

                $('#'+selectId).html('');
                $('#'+selectId).select2('destroy');
                a_global_liarary.initReportColumn(selectId, checkboxClass);
                $('#'+selectId).select2Sortable({width: '100%'});

            });
        }

        }, 1000);
    },
}