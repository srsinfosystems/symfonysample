var location_edit = {

    form_id: 'stage_3_office_hours',
    location_schedule_form_clear: null,

    initialize: function () {
        sk.datepicker('#location_schedule_effective_start');
        sk.datepicker('#location_schedule_effective_end');


        $('#location_schedule_effective_start').on('changeDate', function (e) {
            $('#location_schedule_effective_end').skDP('setStartDate', e.date);
        });

        $('#location_schedule_effective_end').on('changeDate', function (e) {
            $('#location_schedule_effective_start').skDP('setEndDate', e.date);
        });

        $('#location_phone1').blur(function (e) {
            quickClaim.formatPhone('#location_phone1');
        });
        $('#location_phone2').blur(function (e) {
            quickClaim.formatPhone('#location_phone2');
        });
        $('#location_phone3').blur(function (e) {
            quickClaim.formatPhone('#location_phone3');
        });
        $('#location_phone4').blur(function (e) {
            quickClaim.formatPhone('#location_phone4');
        });
        $('#location_fax').blur(function (e) {
            quickClaim.formatPhone('#location_fax');
        });

        $('#location_postal_code').blur(function (e) {
            quickClaim.formatPostal('#location_postal_code');
        });

        if ($('#location_loc_combo').size()) {
            $('#location_name').blur(function () {
                location_edit.handleLocCombo();
            });
            $('#location_code').blur(function () {
                location_edit.handleLocCombo();
            });
        }

        this.setuptablesorter1s();
        this.setupClickableRows();
        this.setupFormClears();

        this.setupLocationScheduleWatchers();

        if ($('#location_moh_ab_ba_num').size()) {
            this.setupAbBaNum();
        }

        if ($('input.hl7_partner_id').size()) {
            $('input.hl7_partner_id:last').closest('tr').after($('<tr class="small_row"><td colspan="2">&nbsp;</td></tr>'));
        }
    },

    fillLocationScheduleDayRow: function (day, row) {
        console.log('.' + day + ' .open');
       
        var open = $('.' + day + ' .open', $(row));
        var close = $('.' + day + ' .close', $(row));
        console.log(open);
        if (open.text()) {
            var o_hr = parseInt($('.hour', $(open)).text(), 10) + '';
            var o_mn = parseInt($('.min', $(open)).text(), 10) + '';
            var o_am = $('.ampm', $(open)).text() + '';

            $('#location_schedule_' + day + '_open_hour').val(o_hr);
            $('#location_schedule_' + day + '_open_minute').val(o_mn);
            $('#location_schedule_' + day + '_open_ampm').val(o_am);
        }

        if (close.text()) {
            var c_hr = parseInt($('.hour', $(close)).text(), 10) + '';
            var c_mn = parseInt($('.min', $(close)).text(), 10) + '';
            var c_am = $('.ampm', $(close)).text() + '';

            $('#location_schedule_' + day + '_close_hour').val(c_hr);
            $('#location_schedule_' + day + '_close_minute').val(c_mn);
            $('#location_schedule_' + day + '_close_ampm').val(c_am);
        }
    },
    handleLocCombo: function () {
        if (!$('#location_id').val() && $('#location_name').val() && $('#location_code').val() && !$('#location_loc_combo').val()) {
            var combo_val = $('#location_code').val() + '_' + $('#location_name').val();
            combo_val = combo_val.replace(' ', '-');

            $('#location_loc_combo').val(combo_val);
        }
    },
    handleLocationScheduleRowClick: function (row) {
        id = $(row).attr('id');
        $('tr#' + id).click(function () {
            var end_date = $('.effective_end', $(row)).text();
            var editable = (end_date == '');
            if (!editable) {
                editable = Date.today().compareTo(Date.parseExact(end_date, quickClaim.getParseDateFormat(global_js.getNewDateFormat()))) != 1;
            }

            if (editable) {
                $('#tablesorter1_location_schedules tr.current_edit').removeClass('current_edit');
                $(this).addClass('current_edit');

                $('#location_schedule_id').val($(this).attr('id'));
                console.log(row);
                location_edit.fillLocationScheduleDayRow('sun', row);
                location_edit.fillLocationScheduleDayRow('mon', row);
                location_edit.fillLocationScheduleDayRow('tues', row);
                location_edit.fillLocationScheduleDayRow('wed', row);
                location_edit.fillLocationScheduleDayRow('thurs', row);
                location_edit.fillLocationScheduleDayRow('fri', row);
                location_edit.fillLocationScheduleDayRow('sat', row);

                $('#location_schedule_effective_start').val($('.effective_start', $(row)).text());
                $('#location_schedule_effective_end').val($('.effective_end', $(row)).text());

                quickClaim.focusTopField();
            } else {
                alert('Schedules that have already ended are not editable.');
            }
        });
    },
    clearLocationScheduleDayRow: function (day) {
        $('#location_schedule_' + day + '_open_hour').val('');
        $('#location_schedule_' + day + '_open_minute').val('');
        $('#location_schedule_' + day + '_close_hour').val('');
        $('#location_schedule_' + day + '_close_minute').val('');
    },
    handleLocationClear: function () {
        location_edit.clearLocationScheduleDayRow('sun');
        location_edit.clearLocationScheduleDayRow('mon');
        location_edit.clearLocationScheduleDayRow('tues');
        location_edit.clearLocationScheduleDayRow('wed');
        location_edit.clearLocationScheduleDayRow('thurs');
        location_edit.clearLocationScheduleDayRow('fri');
        location_edit.clearLocationScheduleDayRow('sat');

        $('#location_schedule_id').val('');
        $('#location_schedule_effective_start').val('');
        $('#location_schedule_effective_end').val('');

        $('#' + location_edit.form_id + ' .error').removeClass('error');
        $('#' + location_edit.form_id + ' ul.error_list').parent().parent().remove();
        $('div.form_error_bottom_box').remove();
    },
    setupAbBaNum: function () {
        $('#location_moh_ab_ba_num').blur(function () {
            val = $(this).val();

            if (val.length == 7) {
                val = val.substr(0, 4) + '-' + val.substr(4);
            }

            $(this).val(val);
        });
    },
    setupClickableRows: function () {
        if ('#tablesorter1_location_schedules') {
            $('#tablesorter1_location_schedules .clickable_row').each(function () {
                location_edit.handleLocationScheduleRowClick($(this));
            });
        }
    },
    setupFormClears: function () {
        $('#' + this.location_schedule_form_clear).click(function () {
            location_edit.handleLocationClear();
        });
    },
    setupLocationScheduleWatchers: function () {
        this.setupLocationScheduleWatcher('_open_minute');
        this.setupLocationScheduleWatcher('_open_hour');
        this.setupLocationScheduleWatcher('_open_ampm');
        this.setupLocationScheduleWatcher('_close_minute');
        this.setupLocationScheduleWatcher('_close_hour');
        this.setupLocationScheduleWatcher('_close_ampm');
    },
    setupLocationScheduleWatcher: function (w) {
        if ($('#location_schedule_sun' + w).size()) {
            $('#location_schedule_sun' + w).blur(function () {
                value = $('#location_schedule_sun' + w).val();

                if (!$('#location_schedule_mon' + w).val()) {
                    $('#location_schedule_mon' + w).val(value);
                }
                if (!$('#location_schedule_tues' + w).val()) {
                    $('#location_schedule_tues' + w).val(value);
                }
                if (!$('#location_schedule_wed' + w).val()) {
                    $('#location_schedule_wed' + w).val(value);
                }
                if (!$('#location_schedule_thurs' + w).val()) {
                    $('#location_schedule_thurs' + w).val(value);
                }
                if (!$('#location_schedule_fri' + w).val()) {
                    $('#location_schedule_fri' + w).val(value);
                }
                if (!$('#location_schedule_sat' + w).val()) {
                    $('#location_schedule_sat' + w).val(value);
                }
            });

        } else {
            $('#location_schedule_mon' + w).blur(function () {
                value = $('#location_schedule_mon' + w).val();

                if (!$('#location_schedule_tues' + w).val()) {
                    $('#location_schedule_tues' + w).val(value);
                }
                if (!$('#location_schedule_wed' + w).val()) {
                    $('#location_schedule_wed' + w).val(value);
                }
                if (!$('#location_schedule_thurs' + w).val()) {
                    $('#location_schedule_thurs' + w).val(value);
                }
                if (!$('#location_schedule_fri' + w).val()) {
                    $('#location_schedule_fri' + w).val(value);
                }
            });
        }
    },
    setuptablesorter1s: function () {
        $('#tablesorter1_location_schedules').tablesorter1({
            widgets: ['zebra', 'hover']
        });
    }
};