var build_schedule = {
    actions: {
        save: null,
        status_change: null,
        download_pdf: null
    },

    location_hours: {},
    events: {},
    holidays: {},
    prefix: 'dsb',
    form_id: 'dsb_form',
    ajax_lock: false,

    initialize: function () {
        for (var a in this.holidays) {
            $('td#date_' + this.holidays[a].date + ' div.datefield').after($('<div class="holiday">' + build_schedule.holidays[a].name + '</div>'));
        }

        $('#generate_cal td').click(function (e) {
            date = $(e.currentTarget).attr('id').replace('date_', '');

            build_schedule.buildNewEvent(date);
            return false;
        });

        $('#calendar_form_dialog').dialog({
            width: 650,
            height: 550,
            autoOpen: false,
            modal: true
        });

        $('#dsb_button')
                .click(function () {
                    build_schedule.handleDsbSubmit();
                    return false;
                });

        $('#status_change')
                .button()
                .click(function () {
                    build_schedule.handleStatusChangeClick();
                    return false;
                });

        $('#change_status_go')
                .button()
                .click(function () {
                    build_schedule.handleStatusChangeGoClick();
                    return false;
                });

        $('#download_as_pdf')
                .button()
                .click(function () {
                    build_schedule.handlePdfClick();
                    return false;
                });

        $('#' + build_schedule.prefix + '_location_id').change(function () {
            build_schedule.fillLocationHours();
        });

        $('#dsb_form input, #dsb_form select').change(function () {
            if ($(this).attr('id') != build_schedule.prefix + '_override_appointment_overlap_check') {
                $('#override_appointment_overlap_check_div').hide();
                $('#' + build_schedule.prefix + '_override_appointment_overlap_check').prop('checked', false);
            }
            if ($(this).attr('id') != build_schedule.prefix + '_override_location_check') {
                $('#' + build_schedule.prefix + '_override_location_check').prop('checked', false);
                $('#override_location_check_div').hide();
            }
        });

        /* Regions*/
        $("#regions ul li input").addClass("regions");
        a_global_liarary.initReportSelect("select2Regions","schedule_regions_","regions");
        a_global_liarary.initReportColumn("select2Regions","regions");

        /* Locations*/
        $("#locations ul li input").addClass("locations");
        a_global_liarary.initReportSelect("select2Locations","schedule_locations_","locations");
        a_global_liarary.initReportColumn("select2Locations","locations");

        /* Doctors*/
        $("#doctors ul li input").addClass("doctors");
        a_global_liarary.initReportSelect("select2Doctors","schedule_doctors_","doctors");
        a_global_liarary.initReportColumn("select2Doctors","doctors");
    },
    /*initReportColumn: function (selectId,checkboxParentId) {

        var options = '';
        var firstOrder = [];
        var ct = 0;
        var oldorder = [];
         
        $("."+checkboxParentId).each(function (chkbox) {

            firstOrder[$(this).val()] = $(this).attr('data-position');
            if ($(this).attr('data-position')) {
                ct++;
            }
            oldorder.push({val: $(this).val(), text: $(this).parent().find('label').text(), selected: $(this).is(":checked")});
        })
        $("."+checkboxParentId).each(function (chkbox) {
            $(this).parent("li").parent("ul.checkbox_list").hide();
        });

        var neworder = [];
        for (var r = 0; r < oldorder.length; r++) {
            if (firstOrder[oldorder[r].val]) {
                var kk = firstOrder[oldorder[r].val];
            } else {
                var kk = ct;
                ct++;
            }
            neworder[kk] = oldorder[r];
        }
        for (var newod in neworder) {

            if (neworder[newod]['selected']) {
                options += '<option selected value="' + neworder[newod]['val'] + '">' + neworder[newod]['text'] + '</option>';
            } else {
                options += '<option value="' + neworder[newod]['val'] + '">' + neworder[newod]['text'] + '</option>';
            }
        }
        $('#'+selectId).html(options);
    },
    initReportSelect: function (seleceId, checkboxId, checkboxParentId) {

        setTimeout(function () {
            $('#'+seleceId).select2('destroy');
            var printselect = $('#'+seleceId).select2Sortable({width: '100%'});
            var opts = $('#'+seleceId).val();

            printselect.on("change", function (e) {
                if(e.val.length > 1){

                    if (e.removed) {
                        $("#"+checkboxId + "all").attr('checked', false);
                    }
                    if (e.added) {
                        if(e.added.id == "all"){
                            var skipper = $("#"+checkboxId + "all");
                            $(":checkbox").not(skipper).prop("checked",false);
                        }else
                            $("#"+checkboxId + "all").attr('checked', false);
                    } 
                }

                if (e.removed) {
                    $("#"+checkboxId + e.removed.id).attr('checked', false);
                }
                if (e.added) {
                    $("#"+checkboxId + e.added.id).attr('checked', true);
                }

                $('#'+seleceId).html('');
                $('#'+seleceId).select2('destroy');
                build_schedule.initReportColumn(seleceId, checkboxParentId);
                $('#'+seleceId).select2Sortable({width: '100%'});

            });


        }, 1000);
    },
*/
    addCalEvent: function (e) {
        if (e.status_class == 'status_vacation') {
            e.id += 'v';
        }

        build_schedule.events[e.id] = e;

        $('#event_' + e.id).remove();
        $('#tooltip_' + e.id).remove();

        var event_div = '<div id="event_' + e.id + '" class="calendar_event ' + e.status_class + '">' + e.title + '</div>';

        if (e.status_class == 'status_vacaction') {
            $('td#date_' + e.date).append($(event_div));
        } else {
            var added = false;

            $('td#date_' + e.date + ' div.calendar_event').each(function () {
                id = $(this).attr('id').replace('event_', '');

                if (build_schedule.events[id].status_class == 'status_vacation') {
                    $('#event_' + e.id).remove();
                    $(this).before($(event_div));
                    added = true;
                    return false;
                } else if (e.loc_code < build_schedule.events[id].loc_code) {
                    $(event_div).remove();
                    $(this).before($(event_div));
                    added = true;
                    return false;
                } else if (e.loc_code == build_schedule.events[id].loc_code && e.start_time < build_schedule.events[id].start_time) {
                    $(event_div).remove();
                    $(this).before($(event_div));
                    added = true;
                    return false;
                }
            });

            if (!added) {
                $('td#date_' + e.date).append($(event_div));
            }
        }

        $('#event_' + e.id).tooltip({
            items: 'div',
            content: function () {
                return '<span>' + e.tooltip + '</span>';
            }
        });

        if (e.status_class != 'status_vacation') {
           
            $('#event_' + e.id).click(function (event) {
             
                build_schedule.showEventForm(e);
                return false;
            });
        } else {
            $('#event_' + e.id).click(function (event) {
                
                alert('You cannot modify a vacation slot.');
                return false;
            });
        }
    },
    buildNewEvent: function (event_date) {
        return build_schedule.showEventForm({
            'id': null,
            'doctor_general_schedule_id': null,
            'doctor_id': null,
            'location_id': null,
            'status': null,
            'date': event_date,
            'doctor_name': null,
            'location_name': null,
            'doc_code': null,
            'loc_code': null,
            'start_time': null,
            'end_time': null,
            'status_string': null,
            'title': null,
            'tooltip': null,
            'notes': ''
        });
    },
    clearDsbFormErrors: function () {
        $('#error_message').text('').removeClass('ui-state-error');
        $('#dsb_form :input.error').removeClass('error');
        $('#dsb_form label.error').remove();
        $("#dsb_form select").removeClass('cliponeErrorInput');
        $("#dsb_form input").removeClass('cliponeErrorInput');
    },
    fillLocationHours: function () {
        var location_id = $('#' + build_schedule.prefix + '_location_id').val();
        var date = $('#' + build_schedule.prefix + '_date').val();
        var dow = null;

        date = Date.parseExact(date, quickClaim.getParseDateFormat(quickClaim.dateFormat));
        if (date) {
            var dow = date.toString('ddd').toLowerCase();
        }

        if (dow && build_schedule.location_hours.hasOwnProperty(location_id)) {
            if (dow == 'thu')
                dow = 'thurs';
            if (dow == 'tue')
                dow = 'tues';
            $('#' + build_schedule.prefix + '_start_time_hour').val(build_schedule.location_hours[location_id][dow].oh);
            $('#' + build_schedule.prefix + '_start_time_minute').val(build_schedule.location_hours[location_id][dow].om);
            $('#' + build_schedule.prefix + '_start_time_ampm').val(build_schedule.location_hours[location_id][dow].oa);
            $('#' + build_schedule.prefix + '_end_time_hour').val(build_schedule.location_hours[location_id][dow].ch);
            $('#' + build_schedule.prefix + '_end_time_minute').val(build_schedule.location_hours[location_id][dow].cm);
            $('#' + build_schedule.prefix + '_end_time_ampm').val(build_schedule.location_hours[location_id][dow].ca);
        }
    },
    handleDsbSubmit: function () {
        if (build_schedule.ajax_lock) {
            return;
        }
        build_schedule.clearDsbFormErrors();
        var data = $('#dsb_form').serialize();

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: build_schedule.actions.save,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#error_message', '#dsb', true);

                    if (rs.hasOwnProperty('show_override')) {
                        for (var b in rs.show_override) {
                            $('#' + rs.show_override[b] + '_div').show();
                        }
                    }

                } else if (rs.hasOwnProperty('data')) {
                    build_schedule.addCalEvent(rs.data);
                    $('#calendar_form_dialog').dialog('close');
                }
            },
            beforeSend: function () {
                build_schedule.ajax_lock = true;
                $('#spinner').show();
                $('#spinner_text').text('Saving Appointment block');
            },
            complete: function () {
                build_schedule.ajax_lock = false;
                $('#spinner').hide();
                $('#spinner_text').text('');
            }
        });
    },
    downloadURL: function (url) {
        var iframe = $('#hiddenDownloader');

        if (iframe.size() == 0) {
            iframe = $('<iframe id="hiddenDownloader" style="display:none;" />');
            $('body').append(iframe);
        }

        iframe.attr('src', url);

    },
    handlePdfClick: function () {
        var data = $('#parameters_form').serialize();
        this.downloadURL(this.actions.download_pdf + '?' + data);
    },
    handleStatusChangeClick: function () {
        $('#change_status_div').show();

        $('#change_status_pending').prop('checked', false);
        $('#change_status_posted').prop('checked', false);
        $('#change_status_cancelled').prop('checked', false);
    },
    handleStatusChangeGoClick: function () {
        if (build_schedule.ajax_lock) {
            return;
        }

        if ($('#change_status_posted').prop('checked') || $('#change_status_pending').prop('checked') || $('#change_status_cancelled').prop('checked') || $('#change_status_deleted').prop('checked')) {
            var ids = this.getEventIds();
            var status = $('input[name="change_status[]"]:checked').val();

            var data = 'status_change[ids]=' + ids
                    + '&status_change[status]=' + status;

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: build_schedule.actions.status_change,
                data: data,

                success: function (rs) {
                    if (rs.hasOwnProperty('data')) {
                        for (var a in rs.data) {
                            build_schedule.addCalEvent(rs.data[a]);
                        }
                    }
                    $('#change_status_div').hide();
                },
                beforeSend: function () {
                    build_schedule.ajax_lock = true;
                    $('#spinner').show();
                    $('#spinner_text').text('Updating All Appointments.');
                },
                complete: function () {
                    build_schedule.ajax_lock = false;
                    $('#spinner').hide();
                    $('#spinner_text').text('');
                }
            });
        }
    },
    getEventIds: function () {
        var ids = new Array();
        for (var a in this.events)
        {
            if (this.events[a].id) {
                ids[ids.length] = this.events[a].id;
            }
        }
        return ids;
    },
    showEventForm: function (event) {
        $('input[name="' + build_schedule.prefix + '[status]"]').prop('checked', false);

        if (event) {
            $('#override_location_check_div').hide();
            $('#override_appointment_overlap_check_div').hide();
            $('#calendar_form_dialog').dialog('open');
            this.clearDsbFormErrors();

            $('#' + build_schedule.prefix + '_id').val(event.id);
            $('#' + build_schedule.prefix + '_doctor_id').val(event.doctor_id);
            $('#' + build_schedule.prefix + '_location_id').val(event.location_id);
            $('#' + build_schedule.prefix + '_notes').val(event.notes);
            $('#' + build_schedule.prefix + '_override_location_check').prop('checked', false);
            $('#' + build_schedule.prefix + '_override_appointment_overlap_check').prop('checked', false);

            if (event.start_time) {
                time = event.start_time.split(':');
                hour = parseInt(time[0], 10);
                minute = parseInt(time[1], 10);

                ampm = 'am';
                if (hour == 12) {
                    ampm = 'pm';
                } else if (hour > 12) {
                    ampm = 'pm';
                    hour = hour - 12;
                }

            } else {
                hour = '';
                minute = '';
                ampm = '';
            }

            $('#' + build_schedule.prefix + '_start_time_hour').val(hour);
            $('#' + build_schedule.prefix + '_start_time_minute').val(minute);
            $('#' + build_schedule.prefix + '_start_time_ampm').val(ampm);

            if (event.end_time) {
                time = event.end_time.split(':');
                hour = parseInt(time[0], 10);
                minute = parseInt(time[1], 10);

                ampm = 'am';
                if (hour == 12) {
                    ampm = 'pm';
                } else if (hour > 12) {
                    ampm = 'pm';
                    hour = hour - 12;
                }
            } else {
                hour = '';
                minute = '';
                ampm = '';
            }

            $('#' + build_schedule.prefix + '_end_time_hour').val(hour);
            $('#' + build_schedule.prefix + '_end_time_minute').val(minute);
            $('#' + build_schedule.prefix + '_end_time_ampm').val(ampm);

            var status = event.status ? event.status : 1;

            $('#' + build_schedule.prefix + '_status_' + status).prop('checked', true);

            $('#' + build_schedule.prefix + '_date').val(event.date);            
        }
        quickClaim.focusTopField(this.form_id);
        return false;
    }
};

