var general_schedule = {
    actions: {
        save: null,
        index: null
    },

    location_hours: [],

    prefix: 'schedule',
    row_prefix: 'doctor_schedules',
    row_count: 0,
    ajax_lock: false,

    initialize: function () {
        general_schedule.row_count = $('#form_table tbody tr.form_row').size();

        $('#save_schedule')
                .button()
                .click(function () {
                    general_schedule.handleSaveClick();
                });

        $('#cancel_schedule')
                .button()
                .click(function () {
                    window.location = general_schedule.actions.index;
                });
    },
    initializeRow: function (c) {
        sk.datepicker('#' + general_schedule.prefix + '_' + general_schedule.row_prefix + '_' + c + '_start_date');

        $('#' + general_schedule.prefix + '_' + general_schedule.row_prefix + '_' + c + '_start_date').on('changeDate', function (e) {
            general_schedule.getWeekday(c);
            $('#' + general_schedule.prefix + '_' + general_schedule.row_prefix + '_' + c + '_end_date').skDP('setStartDate', e.date);
        });

        general_schedule.getWeekday(c);

        sk.datepicker('#' + general_schedule.prefix + '_' + general_schedule.row_prefix + '_' + c + '_end_date');

        $('#' + general_schedule.prefix + '_' + general_schedule.row_prefix + '_' + c + '_end_date').on('changeDate', function (e) {
            $('#' + general_schedule.prefix + '_' + general_schedule.row_prefix + '_' + c + '_start_date').skDP('setEndDate', e.date);
        });

        var prefix = general_schedule.prefix + '_' + general_schedule.row_prefix + '_' + c;
        $('#' + prefix + '_start_date').blur(function () {
            general_schedule.checkForLastRow(c);

        });
        $('#' + prefix + '_location_id').change(function () {
            general_schedule.checkForLastRow(c);
        });
        $('#' + prefix + '_start_time_hour').change(function () {
            general_schedule.checkForLastRow(c);
        });
        $('#' + prefix + '_start_time_minute').change(function () {
            general_schedule.checkForLastRow(c);
        });
        $('#' + prefix + '_end_time_hour').change(function () {
            general_schedule.checkForLastRow(c);
        });
        $('#' + prefix + '_end_time_minute').change(function () {
            general_schedule.checkForLastRow(c);
        });
        $('#' + prefix + '_recurrence_type').change(function () {
            general_schedule.checkForLastRow(c);
        });
        $('#' + prefix + '_end_date').change(function () {
            general_schedule.checkForLastRow(c);
        });

        $('#' + prefix + '_location_id').change(function () {
            general_schedule.fillLocationHours(c);
        });
        $('#' + prefix + '_start_date').change(function () {
            general_schedule.fillLocationHours(c);
        });

        $('#clear_' + c).click(function () {
            general_schedule.deleteRow(c);
        });
    },
    deleteRow: function (row_num) {
        var num_rows = $('#form_table tbody tr').size();
        if (num_rows > 1) {
            $('#form_row_' + row_num).remove();
            $('#' + this.prefix + '_' + this.row_prefix + '_' + row_num + '_id').remove();
        }
        if(num_rows <= 2)
        {
            $('.ui-icon-close').hide();
        }

    },
    handleSaveClick: function () {
        if (general_schedule.ajax_lock) {
            return;
        }
        $('#general_schedule_form :input.error').removeClass('error');
        $('#error_message').hide();
        $('#error_message').text('');

        var data = $('#general_schedule_form').serialize();

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: general_schedule.actions.save,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#error_message', '#general_schedule_form', true);

                    if (rs.hasOwnProperty('show_override')) {
                        for (var a in rs['show_override']) {
                            $('#' + general_schedule.prefix + '_' + rs.show_override[a]).parent('div').show();
                        }
                    }
                    if (rs.hasOwnProperty('show_end_date_override')) {
                        for (var a in rs['show_end_date_override']) {
                            $('#' + general_schedule.prefix + '_' + rs.show_end_date_override[a]).parent('div').show();
                        }
                    }
                } else if (rs.hasOwnProperty('success')) {
                    $('#success_message_text').text('Saved Doctor Work Schedule.  Refreshing the page');
                    $('#spinner').show();
                    $('#success_message').show();

                    window.location.href = '/general_schedule';
                }
            },
            beforeSend: function () {
                general_schedule.ajax_lock = true;
                $('#spinner').show();
                $('#spinner_text').text('Saving doctor work schedule');
            },
            complete: function () {
                general_schedule.ajax_lock = false;
                $('#spinner').hide();
                $('#spinner_text').text('');
            }
        });

    },
    fillLocationHours: function (row_num) {
        var prefix = general_schedule.prefix + '_' + general_schedule.row_prefix + '_' + row_num;
        var location_id = $('#' + prefix + '_location_id').val();
        var date = $('#' + prefix + '_start_date').skDP("getDate");
        var dow = null;
//        date = Date.parseExact(date, quickClaim.getParseDateFormat(quickClaim.dateFormat));
        if (date) {
            var dow = date.toString('ddd').toLowerCase();
        }

        if (dow && general_schedule.location_hours.hasOwnProperty(location_id)) {
            if (dow == 'thu')
                dow = 'thurs';
            if (dow == 'tue')
                dow = 'tues';
            $('#' + prefix + '_start_time_hour').val(general_schedule.location_hours[location_id][dow].oh);
            $('#' + prefix + '_start_time_minute').val(general_schedule.location_hours[location_id][dow].om);
            $('#' + prefix + '_start_time_ampm').val(general_schedule.location_hours[location_id][dow].oa);
            $('#' + prefix + '_end_time_hour').val(general_schedule.location_hours[location_id][dow].ch);
            $('#' + prefix + '_end_time_minute').val(general_schedule.location_hours[location_id][dow].cm);
            $('#' + prefix + '_end_time_ampm').val(general_schedule.location_hours[location_id][dow].ca);
        }
    },
    checkForLastRow: function (row_num) {
        var last_row = $('#form_table tbody tr:last').attr('id');
        last_row = last_row.replace('form_row_', '');

        if (row_num != last_row) {
            return false;
        }

        general_schedule.insertRow();
    },
    insertRow: function () {
        var old_count = 0;
        var first_row_id = $('#form_table tbody tr:first').attr('id');
        old_count = first_row_id.replace('form_row_', '');

        

        var clonedRow = $("#form_table tbody tr#form_row_" + old_count).clone();
        $(clonedRow).attr('id', 'form_row_' + general_schedule.row_count);
        $('.row_num', $(clonedRow)).text(general_schedule.row_count + 1);
        $('.skWeekDay').show();
        clonedRow = general_schedule.setupNewRowIds(clonedRow, old_count);
        $(clonedRow).find('.skWeekDay').hide();
        $("#form_table tbody").append($(clonedRow));
        general_schedule.drawNewHiddenRowItem('id', old_count);
        var num_rows = $('#form_table tbody tr').size();
        if(num_rows > 1)
        {
            $('.ui-icon-close').show();
        }
        general_schedule.initializeRow(general_schedule.row_count);
        general_schedule.row_count++;

    },
    getWeekday: function (row_num) {
        var dow = '';
        var date = $('#' + general_schedule.prefix + '_' + general_schedule.row_prefix + '_' + row_num + '_start_date').skDP("getDate");
//        date = Date.parseExact(date, quickClaim.getParseDateFormat(quickClaim.dateFormat));
//        console.log(row_num);
        if ($('#' + general_schedule.prefix + '_' + general_schedule.row_prefix + '_' + row_num + '_start_date').val() != '') {
            dow = date.toString('ddd');
            $('#row_' + row_num + '_weekday').text(dow);
        }

    },
    setupNewRowIds: function (row, old) {
        $(":input", row).each(function (e) {
            id = this.id.replace('_' + old + '_', '_' + general_schedule.row_count + '_');
            $(this).attr('id', id);
            name = $(this).attr('name').replace('[' + old + ']', '[' + general_schedule.row_count + ']');
            $(this).attr('name', name);
            $(this).val('');
        });
        $('.hasDatepick', row).each(function () {
            $(this).removeClass('hasDatepick');
        });
        $('span', row).each(function (e) {
            id = this.id.replace('_' + old + '_', '_' + general_schedule.row_count + '_');
            $(this).attr('id', id);
            $(this).val('');
        });

        $('td.ui-widget div', row).each(function (e) {
            id = this.id.replace('_' + old, '_' + general_schedule.row_count);
            $(this).attr('id', id);
        });

        return row;
    },
    drawNewHiddenRowItem: function (name, old) {
        old = 0;
        field = $("#" + general_schedule.prefix + '_' + general_schedule.row_prefix + '_' + old + '_' + name).clone();

        $(field).attr('id', $(field).attr('id').replace('_' + old + '_', '_' + general_schedule.row_count + '_'));
        $(field).attr('name', $(field).attr('name').replace('[' + old + ']', '[' + general_schedule.row_count + ']'));
        $(field).val('');

        $("#" + general_schedule.prefix + '_' + general_schedule.row_prefix + '_' + old + '_' + name).parent().append(field);
    }
};