var appt_template = {

	actions: {
		archive: '',
		end: '',
		save: ''
	},
	data_table_row_buttons: {
		copy: {
			class_prefix: 'copy_button',
			id_prefix: 'copy_',
			value: 'Copy',
			attribute: 'can_copy'
		},
		end: {
			class_prefix: 'end_button',
			id_prefix: 'end_',
			value: 'End',
			attribute: 'can_end'
		},
		archive: {
			class_prefix: 'archive_button',
			id_prefix: 'archive_',
			value: 'Archive',
			attribute: 'can_archive'
		}
	},
	data_table_button_template: '<input type="button" class="ui-button-inline %button_class% table_button" id="%button_id%" value="%button_value%" />',
	data_table_form_template: '<div id="end_div_%id%" class="end_div" style="display:none; padding-top:2px;">'
		+ '	<form id="end_at_form_%id%">' + "\r\n"
		+ '		<input type="hidden" readonly="readonly" id="end_at_%id%_id" name="end_at[id]" value="%id%" />' + "\r\n"
		+ '		<input type="text" id="end_at_%id%_date" name="end_at[date]" value="%date%" style="width: 75px" />' + "\r\n"
		+ '		<input type="button" class="ui-button-inline table_button end_button_submit" id="end_submit_%id%" value="Submit" />' + "\r\n"
		+ '	</form>' + "\r\n"
		+ '</div>',
	data_table: 'repeating_events_data',
	data_table_columns: [],
	default_date: null,
	prefix: 'appointment_template',
	data: {},
	search_form: 'repeating_events_form',
	message: {
		checkmark_id: 'checkmark',
		error_id: 'error_icon',
		spinner_id: 'spinner',
		text_id: 'messages_text'
	},
	modal_message: {
		checkmark_id: 'modal_checkmark',
		error_id: 'modal_error_icon',
		spinner_id: 'modal_spinner',
		text_id: 'modal_messages_text'
	},
	show_archived: false,
	end_div: 'end_div',
	search_prefix: 'appt_template_search',
	edit_form: 'appointment_template_form',
	edit_form_error: 'at_error_message',
	tablesorter1_initialized: false,
	ajax_lock: false,

	initialize: function() {
		$('#repeating_search_button').button();

		$('#new_template_button')
			.button()
			.click(function() {

				$('#' + appt_template.edit_form).clearForm();
				$('#' + appt_template.edit_form + ' select').val('');

				appt_template.clearErrorMessages();
				$('#appointment_template_form_div').dialog('open');
			});

                sk.datepicker('#' + this.search_prefix + '_start_date');
                sk.datepicker('#' + appt_template.search_prefix + '_end_date');
                
                $('#' + this.search_prefix + '_start_date').on('changeDate', function(){
                    $('#' + appt_template.search_prefix + '_end_date').skDP('setStartDate', e.date);                    
                });
                
                var search_prefix = this.search_prefix;
                $('#' + appt_template.search_prefix + '_end_date').on('changeDate',function (e) {
                    $('#' + search_prefix + '_start_date').skDP('setEndDate', e.date);
                });
                

		$('#' + this.prefix + '_start_date').datepick({
			dateFormat: quickClaim.dateFormat,
			renderer: $.datepick.themeRollerRenderer,
			onSelect: function() {
				$('#' + this.search_prefix + '_end_date').datepick('option', 'defaultDate', $(this).val());
				$('.override_row').hide();
				$('#appointment_template_override_repeating_check').prop('checked', false);
			}
		});

		$('#' + this.prefix + '_end_date').datepick({
			dateFormat: quickClaim.dateFormat,
			renderer: $.datepick.themeRollerRenderer,
			onSelect: function() {
				$('.override_row').hide();
				$('#appointment_template_override_repeating_check').prop('checked', false);
			}
		});

		$('#appointment_template_form_div').dialog({
			autoOpen: false,
			height: $(document).height() - 400,
			modal: true,
			width:  $('body').width() - 75,
			open: function() {

			}
		});

		$('#template_form_submit').button();
		$('#template_form_submit').click(function () {
			appt_template.submitAppointmentTemplateForm();
		});

		$('#' + appt_template.data_table + ' thead tr th').each(function () {
			var class_name = $(this).attr('class').replace('header', '');
			class_name = jQuery.trim(class_name);
			appt_template.data_table_columns[appt_template.data_table_columns.length] = class_name;
		});

		if ($('#' + appt_template.data_table + ' tbody tr').size()) {
			$('#' + appt_template.data_table).tablesorter1({
				widgets: ['zebra', 'hover']
			});
			appt_template.tablesorter1_initialized = true;
		}

		$('#appointment_template_override_repeating_check').parent('td').parent('tr').addClass('override_row');
		$('.override_row').hide();

		$('#appointment_template_form select').change(function () {
			$('.override_row').hide();
			$('#appointment_template_override_repeating_check').prop('checked', false);
		});
		$('#appointment_template_form input[name="appointment_template[repeat_weekdays][]"]').click(function () {
			$('.override_row').hide();
			$('#appointment_template_override_repeating_check').prop('checked', false);
		});
	},

	archiveButtonClick: function(button_id) {
		if (appt_template.ajax_lock) {
			return;
		}
		var id =  button_id.replace('archive_', '');
		var end_date = $('tr#' + id + ' td.end_date').text();

		if (end_date) {
			var data = $('#' + appt_template.search_form).serialize() + '&archive_id=' + id;

			$.ajax({
				type: 'post',
				dataType: 'json',
				url: appt_template.actions.archive,
				data: data,
				success: function(rs) {
					if (rs.hasOwnProperty('errors')) {
						$('#' + appt_template.message.error_id).show();
						$('#' + appt_template.message.text_id).text(rs.errors);
						quickClaim.focusTopField();
					}
					if (rs.hasOwnProperty('appointment_templates')) {
						for (var a in rs.appointment_templates) {
							appt_template.updateAppointmentTemplate(rs.appointment_templates[a]);
						}
						$('#' + appt_template.message.checkmark_id).show();
						$('#' + appt_template.message.text_id).text('Archived Repeating Appointment').show();
					}
				},
				beforeSend: function() {
					appt_template.ajax_lock = true;
					$('#' + appt_template.message.checkmark_id).hide();
					$('#' + appt_template.message.error_id).hide();
					$('#' + appt_template.message.spinner_id).show();
					$('#' + appt_template.message.text_id).text('Archiving Repeating Appointment').show();
				},
				complete: function() {
					appt_template.ajax_lock = false;
					$('#' + appt_template.message.spinner_id).hide();
				},
				error: function() {
					$('#' + appt_template.message.error_id).show();
					$('#' + appt_template.message.text_id).text('An error occurred while contacting the server.');
			}
			});

		}
		else {
			$('#' + appt_template.message.error_id).show();
			$('#' + appt_template.message.text_id).text('You can only archive Repeating Appointments with an end date.');
		}
	},

	endButtonClick: function(button_id) {
		var id =  button_id.replace('end_', '');
		$('.end_div').hide();
		$('#' + appt_template.end_div + '_' + id).show();
	},

	endSubmitClick: function(button) {
		if (appt_template.ajax_lock) {
			return;
		}
		var id = button.replace('end_submit_', '');
		var data = $('#end_at_form_' + id).serialize() + '&' + $('#' + appt_template.search_form).serialize();

		$.ajax({
			type: 'post',
			dataType: 'json',
			url: appt_template.actions.end,
			data: data,
			success: function(rs) {
				if (rs.hasOwnProperty('errors')) {
					$('#' + appt_template.message.error_id).show();
					$('#' + appt_template.message.text_id).text(rs.errors);
					quickClaim.focusTopField();
				}
				if (rs.hasOwnProperty('appointment_templates')) {
					for (var a in rs.appointment_templates) {
						appt_template.updateAppointmentTemplate(rs.appointment_templates[a]);
					}
					$('.end_div').hide();
					$('#' + appt_template.message.checkmark_id).show();
					$('#' + appt_template.message.text_id).text('Ended Repeating Appointment').show();
				}
			},
			beforeSend: function() {
				appt_template.ajax_lock = true;
				$('#' + appt_template.message.checkmark_id).hide();
				$('#' + appt_template.message.error_id).hide();
				$('#' + appt_template.message.spinner_id).show();
				$('#' + appt_template.message.text_id).text('Ending Repeating Appointment').show();
			},
			complete: function() {
				appt_template.ajax_lock = false;
				$('#' + appt_template.message.spinner_id).hide();
			},
			error: function() {
				$('#' + appt_template.message.error_id).show();
				$('#' + appt_template.message.text_id).text('An error occurred while contacting the server.');
			}
		});

	},

	copyButtonClick: function(button_id) {
		var id =  button_id.replace('copy_', '');
		var appointment_template = appt_template.data[id];
		var prefix = appt_template.prefix;

		$('#' + prefix + '_id').val('');
		$('#' + prefix + '_doctor_id').val(appointment_template.doctor_id);
		$('#' + prefix + '_location_id').val(appointment_template.location_id);
		$('#' + prefix + '_appointment_type_id').val(appointment_template.appointment_type_id);
		$('#' + prefix + '_repeat_weekdays_sun').prop('checked', appointment_template.repeat_sun);
		$('#' + prefix + '_repeat_weekdays_mon').prop('checked', appointment_template.repeat_mon);
		$('#' + prefix + '_repeat_weekdays_tues').prop('checked', appointment_template.repeat_tues);
		$('#' + prefix + '_repeat_weekdays_wed').prop('checked', appointment_template.repeat_wed);
		$('#' + prefix + '_repeat_weekdays_thurs').prop('checked', appointment_template.repeat_thurs);
		$('#' + prefix + '_repeat_weekdays_fri').prop('checked', appointment_template.repeat_fri);
		$('#' + prefix + '_repeat_weekdays_sat').prop('checked', appointment_template.repeat_sat);
		$('#' + prefix + '_start_date').val('');
		$('#' + prefix + '_end_date').val('');

		var time = this.parseTimeString(appointment_template.start_time_ampm);
		$('#' + prefix + '_start_time_hour').val(parseInt(time[0], 10));
		$('#' + prefix + '_start_time_minute').val(parseInt(time[1], 10));
		$('#' + prefix + '_start_time_ampm').val(time[2]);

		var time = this.parseTimeString(appointment_template.end_time_ampm);
		$('#' + prefix + '_end_time_hour').val(parseInt(time[0], 10));
		$('#' + prefix + '_end_time_minute').val(parseInt(time[1], 10));
		$('#' + prefix + '_end_time_ampm').val(time[2]);

		appt_template.clearErrorMessages();
		$('#appointment_template_form_div').dialog('open');
	},

	parseTimeString: function(time) {
		time = time.split(':');
		var hr = time[0];
		time = time[1].split(' ');
		var min = time[0];
		var ampm = time[1];

		return new Array(hr, min, ampm);
	},

	clearErrorMessages: function() {
		$('#' + appt_template.edit_form + ' label.error').remove();
		$('#' + appt_template.edit_form + ' .error').removeClass('error');
		$('#' + appt_template.edit_form + ' #' + appt_template.edit_form_error)
			.hide()
			.text('');
	},

	updateAppointmentTemplate: function(appointment_template) {
		var tr = $('tr#' + appointment_template.id);

		if (!tr.size()) {
			tr = '<tr id="' + appointment_template.id + '">';

			for (var a in appt_template.data_table_columns) {
				tr += '<td class="' + appt_template.data_table_columns[a] + '"></td>';
			}

			tr += '</tr>';
			tr = $(tr);

			tr.appendTo($('table#' + appt_template.data_table + ' tbody'));
		}

		for (var a in appt_template.data_table_columns) {
			var className = appt_template.data_table_columns[a];

			switch (className) {
				case 'doctor_id':
				case 'location_id':
					$('.' + className, tr).text($('#' + appt_template.prefix + '_' + className + ' option[value=' + appointment_template[className] + ']').text());
					break;

				case 'appointment_type_id':
					var text = $('#' + appt_template.prefix + '_' + className + ' option[value=' + appointment_template[className] + ']').text();
					text = text.replace(' [No DR]', '').replace(' [DR]', '');
					$('.' + className, tr).text(text);
					break;

				case 'repeat_sun':
				case 'repeat_mon':
				case 'repeat_tues':
				case 'repeat_wed':
				case 'repeat_thurs':
				case 'repeat_fri':
				case 'repeat_sat':
				case 'is_archived':
					$('.' + className, tr).text(appointment_template[className] ? 'Y' : 'N');
					break;

				case 'times':
					$('.' + className, tr).text(appointment_template.start_time_ampm + ' - ' + appointment_template.end_time_ampm);
					break;

				case 'id':
				case 'start_date':
				case 'end_date':
					$('.' + className, tr).text(appointment_template[className]);
					break;

				case 'actions':
					// if no copy button
					if (!$('#copy_' + appointment_template.id, tr).size() && appointment_template[appt_template.data_table_row_buttons.copy.attribute]) {
						var button = appt_template.makeDataTableButton(appointment_template.id, 'copy');
						$(button).appendTo($('.' + className, tr));

						var id = $(button).attr('id');
						$('#' + id).button();
						$('#' + id).click(function() {
							appt_template.copyButtonClick($(this).attr('id'));
						});
					}
					else if (!appointment_template[appt_template.data_table_row_buttons.copy.attribute]) {
						$('#copy_' + appointment_template.id, tr).remove();
					}

					// if no end button
					if (!$('#end_' + appointment_template.id, tr).size() && appointment_template[appt_template.data_table_row_buttons.end.attribute]) {
						var button = appt_template.makeDataTableButton(appointment_template.id, 'end');
						$(button).appendTo($('.' + className, tr));

						var id = $(button).attr('id');
						$('#' + id).button();
						$('#' + id).click(function() {
							appt_template.endButtonClick($(this).attr('id'));
						});
					}
					else if (!appointment_template[appt_template.data_table_row_buttons.end.attribute]) {
						$('#end_' + appointment_template.id, tr).remove();
					}

					// if no archive button
					if (!$('#archive_' + appointment_template.id, tr).size() && appointment_template[appt_template.data_table_row_buttons.archive.attribute]) {
						var button = appt_template.makeDataTableButton(appointment_template.id, 'archive');
						$(button).appendTo($('.' + className, tr));

						var id = $(button).attr('id');
						$('#' + id).button();
						$('#' + id).click(function() {
							appt_template.archiveButtonClick($(this).attr('id'));
						});
					}
					else if (!appointment_template[appt_template.data_table_row_buttons.archive.attribute]) {
						$('#archive_' + appointment_template.id, tr).remove();
					}

					// if no end div
					if (!$('#end_div_' + appointment_template.id, tr).size()) {
						var div = appt_template.data_table_form_template;

						div = div.replace(/%id%/g, appointment_template.id);
						div = div.replace(/%date%/g, appointment_template.end_date ? appointment_template.end_date : appt_template.default_date);

						$(div).appendTo($('.' + className, tr));

						$('.end_button_submit', tr)
							.button()
							.click(function() {
								appt_template.endSubmitClick($(this).attr('id'));
							});
					}

					break;

				default:
					break;
			}
		};

		if (!appt_template.data.hasOwnProperty(appointment_template.id)) {
			$('#end_at_' + appointment_template.id + '_date').datepick({
				dateFormat: quickClaim.dateFormat,
				renderer: $.datepick.themeRollerRenderer
			});
		}

		appt_template.data[appointment_template.id] = appointment_template;

		if (appointment_template.is_archived && !appt_template.show_archived) {
			$('tr#' + appointment_template.id).hide();
		}

		if (appt_template.tablesorter1_initialized) {
			$('#' + appt_template.data_table).trigger('appendCache');
		}
		else {
			$('#' + appt_template.data_table).tablesorter1({
				widgets: ['zebra', 'hover']
			});
			appt_template.tablesorter1_initialized = true;
		}

	},

	makeDataTableButton: function(id, type) {
		var button = appt_template.data_table_button_template;
		var params = appt_template.data_table_row_buttons[type];

		button = button.replace(/%button_class%/g, params.class_prefix);
		button = button.replace(/%button_id%/g, params.id_prefix + id);
		button = button.replace(/%button_value%/g, params.value);

		return button;
	},

	submitAppointmentTemplateForm: function() {
		if (appt_template.ajax_lock) {
			return;
		}
		appt_template.clearErrorMessages();

		var data = $('#' + appt_template.edit_form).serialize() + '&' + $('#' + appt_template.search_form).serialize();

		$.ajax({
			type: 'post',
			dataType: 'json',
			url: appt_template.actions.save,
			data: data,
			success: function(rs) {
				if (rs.hasOwnProperty('errors')) {
					quickClaim.showFormErrors(rs.errors, '#at_error_message', '#appointment_template', true);
					quickClaim.focusTopField('appointment_template_form');

					if (rs.hasOwnProperty('show_repeating_override')) {
						$('.override_row').show();
					}
					else {
						$('.override_row').hide();
					}
				}
				if (rs.hasOwnProperty('appointment_templates')) {
					for (var a in rs.appointment_templates) {
						appt_template.updateAppointmentTemplate(rs.appointment_templates[a]);
					}

					$('#appointment_template_form_div').dialog('close');
					$('.end_div').hide();
					$('#' + appt_template.message.checkmark_id).show();
					$('#' + appt_template.message.text_id).text('Saved Repeating Appointment').show();
				}
			},
			beforeSend: function() {
				appt_template.ajax_lock = true;
				$('#' + appt_template.message.checkmark_id).hide();
				$('#' + appt_template.message.error_id).hide();
				$('#' + appt_template.message.spinner_id).show();

				$('#' + appt_template.message.text_id).text('Saving Repeating Appointment').show();
			},
			complete: function() {
				appt_template.ajax_lock = false;
				$('#' + appt_template.message.spinner_id).hide();
			},
			error: function() {
				$('#' + appt_template.message.error_id).show();
				$('#' + appt_template.message.text_id).text('An error occurred while contacting the server.');
			}
		});
	}
};