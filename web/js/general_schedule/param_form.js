var param_form = {
    prefix: 'schedule',
    initialize: function () {
        sk.datepicker('#' + this.prefix + '_start_date');
        sk.datepicker('#' + param_form.prefix + '_end_date');
        

        $('#' + this.prefix + '_start_date').on('changeDate',function (e) {
            $('#' + param_form.prefix + '_end_date').skDP('setStartDate', e.date);
        });
        
        $('#' + this.prefix + '_end_date').on('changeDate',function (e) {
            $('#' + param_form.prefix + '_start_date').skDP('setEndDate', e.date);
        });

        $('#regions input[type=checkbox]').click(function () {
            param_form.oneOrAll('region', this);
            param_form.updateLocationList();
        });

        $('#locations input[type=checkbox]').click(function () {
            param_form.oneOrAll('location', this);
            param_form.updateLocationList();
        });

        $('#doctors input[type=checkbox]').click(function () {
            param_form.oneOrAll('doctor', this);
            param_form.updateLocationList();
        });

        param_form.oneOrAll('region');
        param_form.oneOrAll('location');
        param_form.oneOrAll('doctor');

        param_form.updateLocationList();

        $('.pdf_options_toggle').click(function () {
            param_form.togglePDFOptions();
        });
    },
    addLocationClass: function (location_id, class_name) {
        form_id = this.prefix + '_locations_' + location_id;
        $('#' + form_id).parent().addClass(class_name);
    },
    addDoctorClass: function (doctor_id, class_name) {
        form_id = this.prefix + '_doctors_' + doctor_id;
        $('#' + form_id).parent().addClass(class_name);
    },
    oneOrAll: function (field_name, elem) {
        if (elem != null && $(elem).attr('id') == this.prefix + '_' + field_name + 's_all') {
            $('#' + field_name + 's input[type=checkbox]').prop('checked', false);
            $('#' + this.prefix + '_' + field_name + 's_all').prop('checked', true);
        } else if ($('#' + field_name + 's input[type=checkbox]:checked').size() == 0) {
            $('#' + this.prefix + '_' + field_name + 's_all').prop('checked', true);
        } else if ($('#' + field_name + 's input[type=checkbox]:checked').size() > 1) {
            $('#' + this.prefix + '_' + field_name + 's_all').prop('checked', false);
        }
    },
    togglePDFOptions: function () {
        if ($('#pdf_options_toggle').hasClass('ui-icon-plusthick')) {
            $('#pdf_options_toggle').removeClass('ui-icon-plusthick');
            $('#pdf_options_toggle').addClass('ui-icon-minusthick');
            $('#pdf_options').show();
        } else {
            $('#pdf_options_toggle').removeClass('ui-icon-minusthick');
            $('#pdf_options_toggle').addClass('ui-icon-plusthick');
            $('#pdf_options').hide();
        }
    },
    updateLocationList: function () {
        if ($('#regions input[type=checkbox]:checked').size() == 1 && $('#' + param_form.prefix + '_regions_all').prop('checked')) {
            $('#locations li').show();
            $('#doctors li').show();
        } else {
            //$('#locations li').hide();
            //$('#doctors li').hide();

            $('#regions input[type=checkbox]').each(function () {
                if ($(this).prop('checked')) {
                    id = $(this).attr('id').replace('schedule_regions_', '');
                    $('#locations li.region_' + id).show();
                    $('#doctors li.region_' + id).show();
                }
            });
        }

        if ($('#locations input[type=checkbox]:checked').size() == 0) {
            $('#' + param_form.prefix + '_locations_all').prop('checked', true);
        }
        this.updateDoctorList();

        $('#' + param_form.prefix + '_locations_all').parent().show();
        $('#' + param_form.prefix + '_doctors_all').parent().show();

        param_form.uncheckHidden('location');
        param_form.uncheckHidden('doctor');
    },
    updateDoctorList: function () {
        // if ($('#' + param_form.prefix + '_locations_all').prop('checked')) {
        //     return;
        // }
        if($('#schedule_locations_all').prop("checked") == true){
            $('#doctors ul li').removeClass('notused');
        }
        
        $('#doctors li input[type=checkbox]').each(function () {
            var classes = $(this).parent().attr('class');
            console.log(classes);
            if (classes) {
                classes = classes.split(' ');

                var display = false;
                for (var a in classes) {
                    if (classes[a].indexOf('location_') == 0) {
                        display = display || $('#' + param_form.prefix + '_' + classes[a].replace('location_', 'locations_')).prop('checked');
                         $(this).parent().removeClass('notused');
                    }
                }

                if (!display) {
                   if($('#schedule_locations_all').prop("checked") == false){
                   $(this).parent().addClass('notused');
                   var a = $(this).val();
                    setTimeout(function () {
                       $('#select2Doctors option[value='+a+']').remove();
                    }, 500);
                   }
                  // $(this).parent().hide();
                }
            }

        });

        param_form.selectedRefreshVal();
    },
    selectedRefreshVal:function(){

        var a = 1;
        var options = '';
        $('#select2Doctors').select2('destroy');
        $('#doctors .checkbox_list li:not(".notused") input[type=checkbox]').each(function () {
            var optionVal = $(this).val();
            var optionText = $(this).next('label').text();
            if($(this).prop("checked") == true){
             options += '<option value="' + optionVal + '" selected="selected">' + optionText + '</option>';  

            } else {
             options += '<option value="' + optionVal + '">' + optionText + '</option>';
            }
        });    
        $('#select2Doctors').html(options);
        $('#select2Doctors').select2Sortable({width: '100%'});
        //$("#select2Doctors").trigger('change');     
    },
    uncheckHidden: function (prefix) {
        $('#' + prefix + 's input[type=checkbox]:checked').each(function () {
            if ($(this).parent().css('display') == 'none') {
                $(this).prop('checked', false);
            }
        });
    }

};