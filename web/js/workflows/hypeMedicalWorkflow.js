var workflowActions = {
    actions: {
        claim: '/claim?patient_id={{patientID}}',
        patient: '/patient/view?id={{patientID}}',
        download: '/client_workflows/download',
        patientAutocompleteSearch: '/patient/autocompleteSearch',
        attended: null,
        appointment: null,
        updatePatient: null,
        generateDoc: null,
        hcv: null,
        getPatient: null,
        getLabels: null
    },
    workflowAutocomplete: {
        delay: 300,
        minLength: 3,
        position: { my: 'left top', at: 'left bottom'},
        open: function(event, ui) {

            event.stopImmediatePropagation();
        },
        close: function (event, ui) {

            event.stopImmediatePropagation();
        },
        select: function( event, ui ) {
            $('#' + event.target.id).val(ui.item.value);
            event.stopImmediatePropagation();

            return false;
        },
        focus: function( event, ui ) {
            $('#' + event.target.id).val(ui.item.value);
            return false;
        },
        source: function (req, res) {
            $.ajax({
                dataType: 'json',
                type: 'get',
                data: 'name=' + req.term,
                url: workflowActions.actions.patientAutocompleteSearch,
                cache: true,
                success: function (rs) {
                    var result_set = [];

                    for (patient in rs) {
                        result_set[patient] = { label: rs[patient].fname + ' ' + rs[patient].lname + ': ' + rs[patient].address, value: rs[patient].hcn_num } ;
                    }

                    res(result_set);
                }
            });
        }
    },
    allow_attended: false,
    messageID: null,
    parent: null,
    steps: [],
    stepActions: {},
    healthCardData: {},
    validated: false,
    currentStep: 0,
    walkinFlag: false,
    documents: [],

    setupDymoLinks: function () {
        $('#dymo_label_address_button').button();
        $('#dymo_label_address_button').click(function (event) {
            if (!($('#workflowCardDetailsTable td:first').html())) {
                alert('no active patient.'); return false;
            }
            $('#dymo_label_address_button').prop('disabled', true);
            workflowActions.printNumberofCopy('address');
            //workflowActions.PrintLabel('address');
            $('#dymo_label_address_button').prop('disabled', false);
        });

        $('#dymo_label_dob_button').button();
        $('#dymo_label_dob_button').click(function (event) {
            if (!($('#workflowCardDetailsTable td:first').html())) {
                alert('no active patient.'); return false;
            }

            $('#dymo_label_dob_button').prop('disabled', true);
            workflowActions.printNumberofCopy('dob');
            //workflowActions.PrintLabel('dob');

            $('#dymo_label_dob_button').prop('disabled', false);
        });

        $('#dymo_label_name_button').button();
        $('#dymo_label_name_button').click(function (event) {
            if (!($('#workflowCardDetailsTable td:first').html())) {
                alert('no active patient.'); return false;
            }

            $('#dymo_label_name_button').prop('disabled', true);

            workflowActions.printNumberofCopy('name');
            //workflowActions.PrintLabel('name');


            $('#dymo_label_name_button').prop('disabled', false);
        });

        $('#dymo_label_name_mirrored_button').button();
        $('#dymo_label_name_mirrored_button').click(function (event) {
            if (!($('#workflowCardDetailsTable td:first').html())) {
                alert('no active patient.'); return false;
            }

            $('#dymo_label_name_mirrored_button').prop('disabled', true);

            workflowActions.printNumberofCopy('namemirror');
            //workflowActions.PrintLabel('namemirror');


            $('#dymo_label_name_mirrored_button').prop('disabled', false);
        });

        $('#dymo_label_chart_button').button();
        $('#dymo_label_chart_button').click(function (event) {
            if (!($('#workflowCardDetailsTable td:first').html())) {
                alert('no active patient.'); return false;
            }

            $('#dymo_label_chart_button').prop('disabled', true);

            workflowActions.printNumberofCopy('chart');
            //workflowActions.PrintLabel('chart');


            $('#dymo_label_chart_button').prop('disabled', false);
        });

        $('#dymo_label_chart_refdoc_button').button();
        $('#dymo_label_chart_refdoc_button').click(function (event) {
            if (!($('#workflowCardDetailsTable td:first').html())) {
                alert('no active patient.'); return false;
            }

            $('#dymo_label_chart_refdoc_button').prop('disabled', true);


             workflowActions.printNumberofCopy('chartwrefdoc');
            //workflowActions.PrintLabel('chartwrefdoc');


            $('#dymo_label_chart_refdoc_button').prop('disabled', false);
        });

        $('#dymo_label_ref_dr_button').button();
        $('#dymo_label_ref_dr_button').click(function (event) {
            if (!($('#workflowCardDetailsTable td:first').html())) {
                alert('no active patient.'); return false;
            }

            $('#dymo_label_ref_dr_button').prop('disabled', true);

            workflowActions.printNumberofCopy('refdr');
            //workflowActions.PrintLabel('refdr');


            $('#dymo_label_ref_dr_button').prop('disabled', false);
        });


        $('.redbtn').click(function (e) {
            $(this).css('background', '#ecabab');
        });

    },
    initialize: function () {
        $('#healthCardWorkflow').autocomplete(workflowActions.workflowAutocomplete);

        $('#create_appt').click(function () {
            workflowActions.createWalkIn(workflowActions.healthCardData.patient_id, true);
        });        

        $('#attended_input').click(function() {
           workflowActions.setAttended(workflowActions.healthCardData.patient_id, true);
        });

        $('#workflowDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height:  $(window).height() - 100,
            modal: true,
            width:  900,
            open: function () {

            },
            close: function () {
                $('#workflowCardDetailsTable').find('.patientData').text('');
                workflowActions.healthCardData = {};
                workflowActions.currentStep = 0;

                $('#healthCardValidationResponse')
                    .text('')
                    .hide()
                    .removeClass('hcv_mod10_pass')
                    .removeClass('hcv_fail')
                    .removeClass('hcv_success');

                $('#healthCardWorkflow').val('');
                $('#upcomingAppointmentsDiv').empty();
                $('#attended_input').css('display', 'inline');
                $('#healthCardWorkflowButton').attr('disabled', 'false').removeAttr('disabled');
                workflowActions.walkinFlag = false;
            }
        });

        $('#healthCardWorkflow')
            .keyup(function (e) {
                if (e.which == 13 && $(this).val().length > 7) {
                    workflowActions.handleHealthCardEntry($('#healthCardWorkflow').val());
                }
            });

        $('#healthCardWorkflowButton')
            .button()
            .click(function () {
                workflowActions.handleHealthCardEntry($('#healthCardWorkflow').val());
            });

        $('#downloadBtn').click(function() {
            var prov = 'prov=' +$('#downloadTest').val();

            $.ajax({
                type: 'post',
                url: workflowActions.actions.download,
                data: prov,
                dataType: 'json',
                success: function(rs) {
                    alert(rs);
                },
                error: function(xhr, status, err) {
                    alert('An error occurred: ' + status + ' / ' + err);
                }

            });
        });
        this.setupDymoLinks();
    },
    setAttended: function (patientID, removeBtn) {
        if (!patientID) {
            return false;
        }

        if (removeBtn) {
            if (!workflowActions.allow_attended) {
                $('#attended_input').remove();
                return false;
            }
        }

        var dataURL = 'patient_id=' + patientID;

        $.ajax({
            type: 'post',
            url: workflowActions.actions.attended,
            data: dataURL,
            dataType: 'json',
            success: function(rs) {
                quickClaim.hideAjaxMessage(workflowActions.messageID);

                var message = 'Completed, updated ' + rs.length + ' appointments';
                quickClaim.showAjaxMessage(workflowActions.messageID, 'success', message);

                $('#' + patientID).css('color', 'green');
            },
            error: function () {
                quickClaim.ajaxLocked = false;
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(workflowActions.messageID, 'error', message);
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                var message = 'Updating patient appointments';

                $('#spinner_' + patientID).show();

                quickClaim.showAjaxMessage(workflowActions.messageID, 'spin', message);
                quickClaim.showOverlayMessage(message);
            },
            complete: function () {
                $('#spinner_' + patientID).hide();

                $('#' + patientID + ' a').css('color', 'green');

                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
            }
        });
    },
    createWalkIn: function (patientID, patientUpdate) {

        if (workflowActions.walkinFlag) {
            if (!confirm("You've already created an appointment today, do you want to make another?") == true) {
                return;
            }
        }

        if (!patientID) {
            return false;
        }

        if ($(".doctorBtn[selected]").attr('id').length == 0) {
            alert('No active scheduled doctor!');
            return false;
        }

        var dataURL = 'patient_id=' + patientID + '&doctor_id=' + $(".doctorBtn[selected]").attr('id').substring(7);

        $.ajax({
            type: 'post',
            url: workflowActions.actions.walkin,
            data: dataURL,
            dataType: 'json',
            success: function(rs) {
                quickClaim.ajaxLocked = false;
                if (rs.hasOwnProperty('err')) {
                    alert(rs['err']);
                    quickClaim.showAjaxMessage(workflowActions.messageID, 'error', message);
                    quickClaim.hideAjaxMessage(workflowActions.messageID);
                    workflowActions.walkinFlag = true;
                    return;
                } else {
                    quickClaim.hideAjaxMessage(workflowActions.messageID);

                    var message = rs;
                    quickClaim.showAjaxMessage(workflowActions.messageID, 'success', message);

                    workflowActions.walkinFlag = true;
                    $('#checkmark_' + patientID).show();
                }

                $('#attended_input').css('display', '');
                if (patientUpdate) {
                    workflowActions.handlePatientUpdate(false, false);
                }
            },
            error: function () {
                quickClaim.ajaxLocked = false;
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(workflowActions.messageID, 'error', message);
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                var message = 'Creating a walk-in appointment...';

                $('#spinner_' + patientID).show();

                quickClaim.showAjaxMessage(workflowActions.messageID, 'spin', message);
                quickClaim.showOverlayMessage(message);
            },
            complete: function () {
                $('#spinner_' + patientID).hide();
                $('#' + patientID + ' a').css('color', 'orange');


                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
            }
        });
    },
    createWalkInForPatientEdit: function (patientID, patientUpdate, doctorId, location, start, close, currentTime) {
        
        // var todaysAppointment = $("#todaysAppointments > div").length;
        var todaysAppointment = $("#todaysAppointments > div.walkinAppointment").length;        
        if (todaysAppointment > 0) {
            if (!confirm("You've already created an appointment today, do you want to make another?") == true) {
                return;
            }
        }

        if (!patientID) {
            return false;
        }

        if (!doctorId) {
            alert('No active scheduled doctor!');
            return false;
        }

        var dataURL = 'patient_id=' + patientID + '&doctor_id=' + doctorId + '&location_id=' + location;        
        
        if(currentTime >= start && currentTime <= close )
            workflowActions.bookAppointment(dataURL);
        else {
            var conf = confirm("Doctor is not available at the moment. Do you still want to continue?");
            if(conf) {
                workflowActions.bookAppointment(dataURL);
            }
        }
        
    },
    bookAppointment: function(dataURL){
        $.ajax({
            type: 'post',
            url: workflowActions.actions.walkin,
            data: dataURL,
            dataType: 'json',
            success: function(rs) {
                // console.log(rs);
                quickClaim.ajaxLocked = false;
                if (rs.hasOwnProperty('err')) {
                    alert(rs['err']);
                    quickClaim.showAjaxMessage(workflowActions.messageID, 'error', message);
                    quickClaim.hideAjaxMessage(workflowActions.messageID);
                    return;
                } else {
                    quickClaim.hideAjaxMessage(workflowActions.messageID);

                    var message = rs;
                    quickClaim.showAjaxMessage(workflowActions.messageID, 'success', message);
                    // $('#checkmark_' + patientID).show();
                }

                $('#todaysAppointments').append("<div>"+rs+"</div>");   
                                
            },
            error: function () {
                quickClaim.ajaxLocked = false;
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(workflowActions.messageID, 'error', message);
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                var message = 'Creating a walk-in appointment...';

                $('#spinner').show();

                quickClaim.showAjaxMessage(workflowActions.messageID, 'spin', message);
                quickClaim.showOverlayMessage(message);
            },
            complete: function () {
                $('#spinner').hide();
                $('#' + patientID + ' a').css('color', 'orange');

                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
            }
        });
    },
    initializeSteps: function (steps) {
        for (var a in steps) {
            workflowActions.initializeStep(a, steps[a]);
        }

        for (var step in workflowActions.steps) {
            $('#workflowSteps').append('<li>' + workflowActions.steps[step] + '</li>');
        }
    },
    initializeStep: function (stepName, stepParams) {
        workflowActions.steps[workflowActions.steps.length] = stepName;

        if (stepParams.hasOwnProperty('javascriptFunction')) {
            workflowActions.stepActions[stepName] = stepParams['javascriptFunction'];
        }


        if (stepName == 'HCSwipe') {
            this.reader = new CardReader("�", "%", "\r");
            this.reader.observe($(document));

            this.reader.cardRead(function (value) {
                $('#workflowCardDetailsTable').find('.header').removeClass('header');
                workflowActions.handleHealthCardSwipe(value);
            });
        }

        if (stepName == 'PatientUpdate') {
            workflowActions.actions.updatePatient = stepParams['action'];
        }

        if (stepName == 'GenerateDocs') {
            workflowActions.actions.generateDoc = stepParams['action'];
            workflowActions.documents = stepParams['documentNames'];
        }

        if (stepName == 'HCValidate') {
            workflowActions.actions.hcv = stepParams['action'];
        }

        if (stepName == 'GoToAppointments') {
            workflowActions.actions.appointment = stepParams['action'];
        }

        if (stepName == 'GoToClaims') {
            workflowActions.actions.claim = stepParams['action'];
        }

        if (stepName == 'GoToPatients') {
            workflowActions.actions.patient = stepParams['action'];
        }
    },

    handleHealthCardEntry: function (string) {
        var data = {};
        var hcn = '';
        var vc = '';
        string = string.trim().toUpperCase();

        var idx = quickClaim.strpos(string, ' ');
        if (idx !== false) {
            hcn = string.substring(0, idx);
            vc = string.substring(idx + 1, string.length);
        }
        else {
            hcn = string;
        }

        data.hcn_num = hcn;
        data.dob = '';
        data.version_code = vc;
        data.fname = '';
        data.lname = '';
        data.sex = '';
        data.expiry_date = '';

        workflowActions.healthCardData = data;

        $('#workflowDialog').dialog('open');
        for (var a in data) {
            $('#workflowCardDetailsTable').find('.' + a).text(data[a]);
        }
        $('#workflowFeeServiceDetailsTable tbody tr').remove();

        workflowActions.goToNextStep();
    },
    handleHealthCardSwipe: function (string) {
        var data = workflowActions.healthCardData = workflowActions.parseCardString(string);

        for (var a in data) {
            $('#workflowCardDetailsTable').find('.' + a).text(data[a]);
        }
        $('#workflowFeeServiceDetailsTable tbody tr').remove();

        $('#workflowDialog').dialog('open');
        workflowActions.goToNextStep();
    },
    handlePatientUpdate: function (cont, createPatient) {
        if (quickClaim.ajaxLocked) {
            return;
        }
        if (!workflowActions.healthCardData.hasOwnProperty('hcn_num')) {
            return;
        }

        var data = workflowActions.healthCardData;
        data.create_new = createPatient;

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: workflowActions.actions.updatePatient,
            data: data,

            success: function (rs) {
                // console.log(rs);
                if (rs.hasOwnProperty('success')) {
                    quickClaim.showAjaxMessage(workflowActions.messageID, 'success', 'Saved Patient Profile');

                    if (rs.hasOwnProperty('data')) {
                        workflowActions.healthCardData.patient_id = rs['data']['patient_id'];

                        var $table = $('#workflowCardDetailsTable');
                        $table.find('th.patient_id').text('Patient ID');

                        for (var a in rs['data']) {
                            $table.find('td.' + a).text(rs['data'][a]);
                        }

                        $('#patient_id').html(rs['data'].patient_id);

                        if (rs.hasOwnProperty('appts')) {
                            $('#upcomingAppointmentsDiv').empty();
                            $('#todayAppointmentTitle').css('display', '');

                            workflowActions.walkinFlag = true;
                            workflowActions.allow_attended = true;

                            let systime = new Date().addDays(1).getTime();
                            $('#upcomingAppointmentsDiv').html('');
                            $('#todaysAppointments').html('');
                            $('#todaysAppointmentsChild').html('');
                            $('#upcomingAppointmentsDivChild').html('');
                            for (var appt in rs.appts) {
                                var html = '';
                                var appendDivUpcomming = rs.data['patient_id'] == rs.appts[appt].patient_id ? 'upcomingAppointmentsDiv' : 'upcomingAppointmentsDivChild';
                                var appendDivToday = rs.data['patient_id'] == rs.appts[appt].patient_id ? 'todaysAppointments' : 'todaysAppointmentsChild';
                                
                                if (new Date(rs.appts[appt].start).getTime() > systime) {
                                    html += rs.appts[appt].appointment_type_str + ': ' + new Date(rs.appts[appt].start).toLocaleString() + ' at ' + rs.appts[appt].patient_location + ' with ' + rs.appts[appt].doctor + ' for ' + rs.appts[appt].patient_fname + ' ' + rs.appts[appt].patient_lname;
                                    $('#'+appendDivUpcomming).append('<ul><li>' + html + '</li></ul>');
                                } else {
                                    html += rs.appts[appt].appointment_type_str + ': ' + new Date(rs.appts[appt].start).toLocaleString() + ' at ' + rs.appts[appt].patient_location + ' with ' + rs.appts[appt].doctor + ' for ' + rs.appts[appt].patient_fname + ' ' + rs.appts[appt].patient_lname;
                                    $('#'+appendDivToday).append('<ul><li>' + html + '</li></ul>');
                                }
                            }
                            var chkarr = ['upcomingAppointmentsDiv', 'upcomingAppointmentsDivChild', 'todaysAppointments', 'todaysAppointmentsChild'];
                            for(var i in chkarr){
                                if($("#"+chkarr[i]).html() == ''){
                                    $("#"+chkarr[i]).css('display', 'none');
                                } else {
                                    $("#"+chkarr[i]).css('display', 'block');
                                }
                            }
                        } else {
                            $('#attended_input').css('display', 'none');
                            $('#todayAppointmentTitle').css('display', 'none');
                            $('#todayAppointmentTitleChild').css('display', 'none');
                            $('#todayAppointmentTitleChild').css('display', 'none');
                        }

                        if (rs.hasOwnProperty('related')) {
                            $('.childrenFound').empty();
                            $('#relatedTitle').css('display', '');

                            for (var child in rs.related) {
                                if(rs.related[child].address != ''){
                                    let html = '';
                                    let spinner = '<img  style="display: none;" id="spinner_' + rs.related[child].id + '" src="/images/icons/spinner.gif">';
                                    let checkmark = '<img height="16" style="display: none;" id="checkmark_' + rs.related[child].id + '" src="/images/icons/checkmark.png">';

                                    html += 'Age ' + rs.related[child].age + ': ' + rs.related[child].fname + ' ' + rs.related[child].lname + ' | ' + rs.related[child].address;
                                    $('.childrenFound').append('<li class="relatedChildWalkIn" id="' + rs.related[child].id + '"><a>' + html + '</a>'
                                    + spinner + checkmark + '</li>');
                                }                                
                            }

                            $('.relatedChildWalkIn').click(function(e) {
                                if (e.ctrlKey) {
                                    workflowActions.setAttended($(this).attr('id'), true);
                                } else {
                                    workflowActions.createWalkIn($(this).attr('id'), true);
                                }
                            });
                        } else {
                            $('#relatedTitle').css('display', 'none');
                            $('.childrenFound').css('display', '');
                        }

                        if (rs.hasOwnProperty('doctors')) {
                            $('.relatedDoctorButtons').empty();
                            for (let doctor in rs.doctors) {
                                let html = '<button title="' + rs.doctors[doctor].fname + ' ' + rs.doctors[doctor].lname + '" id="doctor_' + rs.doctors[doctor].id + '" class="btn btn-squared doctorBtn">';
                                html += rs.doctors[doctor].qc_doctor_code + '</button>&nbsp;';
                                $('.relatedDoctorButtons').append(html);
//                                $('#doctor_' + rs.doctors[doctor].id).button();
//                                $('#doctor_' + rs.doctors[doctor].id).click(function(e) {
//                                    $('.doctorBtn').css('color', 'black');
//                                    $('.doctorBtn').attr('selected', false);
//                                    $(this).css('color', 'orange');
//                                    $(this).attr('selected', true);
//                                });
                            }

                            $('.doctorBtn').click(function(){
                                $('.doctorBtn').removeClass('btn-primary');
                                $(this).addClass('btn-primary').attr('selected', true);
                            })
                            $('#doctor_' + rs.doctors[0].id).addClass('btn-primary').attr('selected', true);

                        }

                        if (cont != false) {
                            workflowActions.goToNextStep();
                        }
                    }
                } else {
                    quickClaim.ajaxLocked = false;
                    var message = 'No patient was found.';
                    quickClaim.showAjaxMessage(workflowActions.messageID, 'error', message);

                    quickClaim.hideOverlayMessage();

                    $('#continue_input')
                        .click(function () {
                            workflowActions.goToNextStep();
                        })
                        .focus();

                    $('#patient_input')
                        .click(function () {
                            console.log('clicked');
                            workflowActions.goToPatients();
                        });

                    $('#past_appt_input')
                        .click(function () {
                            workflowActions.goToPatients(3);
                        });

                    $('#past_claims_input')
                        .click(function () {
                            workflowActions.goToPatients(2);
                        });

                    $('#past_notes_input')
                        .click(function () {
                            workflowActions.goToPatients(4);
                        });

                    $('#appointment_input')
                        .click(function () {
                            workflowActions.goToAppointmentBook();
                        });

                    $('#claim_input')
                        .click(function () {
                            workflowActions.goToClaims();
                        });

                    $('#optionButtonArray').css('display','inline');
                }
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                var message = 'Updating Patient Record';

                quickClaim.showAjaxMessage(workflowActions.messageID, 'spin', message);
                quickClaim.showOverlayMessage(message);
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
            },
            error: function () {
                quickClaim.ajaxLocked = false;
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(workflowActions.messageID, 'error', message);
            }
        });
    },

    handleGenerateDocs: function () {
        var patientID = workflowActions.healthCardData['patient_id'];

        for (var a in workflowActions.documents) {
            var document = workflowActions.documents[a];
            var url = workflowActions.actions.generateDoc;

            url = url.replace('{{documentName}}', document);
            url = url.replace('{{patientID}}', patientID);

            window.open(url, '_blank');
        }

        workflowActions.goToNextStep();
    },

    handleHCVRequest: function () {
        if (quickClaim.hcvLocked) {
            return;
        }

        var url = workflowActions.actions.hcv;
        url = url.replace('{{patientID}}', workflowActions.healthCardData.patient_id);
        url = url.replace('{{version_code}}', workflowActions.healthCardData.version_code);
        url = url.replace('{{hcn_num}}', workflowActions.healthCardData.hcn_num);

        $('#healthCardValidationResponse').text('').hide();
        $('#healthCardWorkflow').val('');

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: url,
            success: function (rs) {
                quickClaim.showAjaxMessage(workflowActions.messageID, 'success', 'Received Health Card Validation Response');

                $('#healthCardValidationResponse')
                    .text(rs['response_message'])
                    .addClass(rs['class_name'])
                    .show();

                $('#optionButtonArray').css('display','inline');

                var td = [];
                var new_content = '';

                if (rs.hasOwnProperty('fee_service_details')) {
                    for (var index = 0; index < rs['fee_service_details'].length; index++) {
                        for (var a in rs['fee_service_details'][index]) {
                            if(a == 'feeServiceResponseCode' || a == 'feeServiceResponseDescription'){
                                if(rs['fee_service_details'][index]['feeServiceResponseCode'] == '101'){
                                    new_content += '<td class="sk-text-green">' + rs['fee_service_details'][index][a] + '</td>';
                                } else {
                                    new_content += '<td class="sk-text-red">' + rs['fee_service_details'][index][a] + '</td>';
                                }
                            } else {
                                new_content += '<td>' + rs['fee_service_details'][index][a] + '</td>';
                            }
                        }
                        $("#workflowFeeServiceDetailsTable tbody:last").append($('<tr class="fee_detail">').append(new_content));
                        new_content = '';
                    }
                }

                var $table = $('#workflowCardDetailsTable');
                $table.find('th.patient_id').text('Patient ID');

                var validResponseCode = function (rs) {
                    switch (rs) {
                        case '50':
                        case '51':
                        case '52':
                        case '53':
                        case '54':
                        case '55':
                            return true;
                        case '05':
                        case '10':
                        case '15':
                        case '20':
                        case '25':
                        case '60':
                        case '65':
                        case '70':
                        case '75':
                        case '80':
                        case '83':
                        case '90':
                        case '99':
                        case '9A':
                        case '9B':
                        case '9C':
                        case '9D':
                        case '9E':
                        case '9F':
                        case '9G':
                        case '9H':
                        case '9I':
                        case '9J':
                        case '9K':
                        case '9L':
                        case '9M':
                            return false;
                        default:
                            return false;
                    }
                };

                function getAge(fromdate, todate){
                    if(todate) todate= new Date(todate);
                    else todate= new Date();

                    var age= [], fromdate= new Date(fromdate),
                        y= [todate.getFullYear(), fromdate.getFullYear()],
                        ydiff= y[0]-y[1],
                        m= [todate.getMonth(), fromdate.getMonth()],
                        mdiff= m[0]-m[1],
                        d= [todate.getDate(), fromdate.getDate()],
                        ddiff= d[0]-d[1];

                    if(mdiff < 0 || (mdiff=== 0 && ddiff<0))--ydiff;
                    if(mdiff<0) mdiff+= 12;
                    if(ddiff<0){
                        fromdate.setMonth(m[1]+1, 0);
                        ddiff= fromdate.getDate()-d[1]+d[0];
                        --mdiff;
                    }
                    if(ydiff> 0) age.push(ydiff+ ' year'+(ydiff> 1? 's ':' '));
                    if(mdiff> 0) age.push(mdiff+ ' month'+(mdiff> 1? 's':''));
                    if(ddiff> 0) age.push(ddiff+ ' day'+(ddiff> 1? 's':''));
                    if(age.length>1) age.splice(age.length-1,0,' and ');
                    return age.join('');
                }

                if (rs.hasOwnProperty('results')) {
                    var dob = new Date(rs['results'][0]['dateOfBirth']);
                    var edate = rs['results'][0]['expiryDate'];
                }
                if (new Date().getFullYear() - dob.getFullYear() < 5) {
                    var datestring = dob.getMonth() + '/' + dob.getDay() + '/' + dob.getFullYear();
                    rs['results'][0]['dateOfBirth'] = dob.toDateString()  + ' - ' + getAge(datestring);
                } else {
                    rs['results'][0]['dateOfBirth'] = dob.toDateString();
                }

                if (rs['results'][0]['expiryDate'] != null) {
                    var dateExp = new Date(rs['results'][0]['expiryDate']);
                    rs['results'][0]['expiryDate'] = dateExp.toDateString();
                }

                for (var a in rs['results'][0]) {
                    if (rs['results'][0][a] != null)
                        $table.find('td.' + a).text(rs['results'][0][a]);
                }

                Date.prototype.yyyymmdd = function() {
                    var yyyy = this.getFullYear().toString();
                    var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
                    var dd  = this.getDate().toString();
                    return yyyy + '' + (mm[1]?mm:"0"+mm[0]) + '' + (dd[1]?dd:"0"+dd[0]); // padding
                };

                var personObj = rs.results[0];

                var data = {};

                data.hcn_num = personObj.healthNumber;
                data.dob = dob.yyyymmdd();
                data.version_code = personObj.versionCode;
                data.fname = personObj.firstName;
                data.lname = personObj.lastName;
                data.sex = personObj.gender;
                data.patient_id = workflowActions.healthCardData.patient_id;

                if (dateExp) {
                    data.expiry_date = dateExp.yyyymmdd();
                }

                workflowActions.healthCardData = data;

                if (validResponseCode(rs['response_code'])) {
                    //why would i ever do this wtf

                    //workflowActions.handlePatientUpdate(false, true);
                }

                $('#continue_input')
                    .click(function () {
                        workflowActions.goToNextStep();
                    })
                    .focus();

                $('#patient_input')
                    .click(function () {
                        workflowActions.goToPatients();
                    });

                $('#past_appt_input')
                    .click(function () {
                        workflowActions.goToPatients(3);
                    });

                $('#past_claims_input')
                    .click(function () {
                        workflowActions.goToPatients(2);
                    });

                $('#past_notes_input')
                    .click(function () {
                        workflowActions.goToPatients(4);
                    });

                $('#appointment_input')
                    .click(function () {
                        workflowActions.goToAppointmentBook();
                    });

                $('#claim_input')
                    .click(function () {
                        workflowActions.goToClaims();
                    });
            },
            beforeSend: function () {
                $('#healthCardWorkflowButton').attr('disabled', 'true');
                quickClaim.hcvLocked = true;
                var message = 'Validating Health Card';

                quickClaim.showAjaxMessage(workflowActions.messageID, 'spin', message);
                quickClaim.showOverlayMessage(message);
            },
            complete: function () {
                $('#healthCardWorkflowButton').attr('disabled', 'false');
                quickClaim.hcvLocked = false;
                quickClaim.hideOverlayMessage();
            },
            error: function () {
                $('#healthCardWorkflowButton').attr('disabled', 'false');
                quickClaim.ajaxLocked = false;
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(workflowActions.messageID, 'error', message);
            }
        });


    },

    goToAppointmentBook: function () {
        var $form = $('#dashboard_appointment_book_form');

        if (workflowActions.healthCardData['patient_id']) {
            $form.find('#appointment_book_patient_id').val(workflowActions.healthCardData['patient_id']);
            $form.find('#appointment_book_appointment_id').val('');
            $form.find('#appointment_book_mode').val('create');
        } else {
            $form.find('#appointment_book_patient_id').val('');
            $form.find('#appointment_book_appointment_id').val('');
        }

        $form.submit();
    },

    goToClaims: function () {
        var url = workflowActions.actions.claim;

        if (workflowActions.healthCardData['patient_id']) {
            url = url.replace('{{patientID}}', workflowActions.healthCardData['patient_id']);
        } else {
            url = url.replace('{{patientID}}', '');
        }

        window.open(url, '_blank');
    },

    goToPatients: function (tab) {
        var url = workflowActions.actions.patient;

        if (workflowActions.healthCardData['patient_id']) {
            url = url.replace('{{patientID}}', workflowActions.healthCardData['patient_id']);
        } else {
            url = url.replace('{{patientID}}', '');
        }

        if (tab) {
            url += '&tab=' + tab;
        }

        window.open(url, '_blank');

    },

    goToNextStep: function () {

        if ($('#workflowDialog').is(":visible")) {
            $('#healthCardWorkflowButton').attr('disabled', 'true');
        }

        workflowActions.currentStep++;

        var nextStepNum = workflowActions.currentStep;
        if (workflowActions.steps[nextStepNum]) {
            var nextStepName = workflowActions.steps[nextStepNum];
            workflowActions[workflowActions.stepActions[nextStepName]]();
        }
    },

    parseCardString: function (string) {
        var data = {};
        var tracks = string.split('^');
        var name = tracks[1];
        name = name.split('/');

        data.hcn_num = string.substr(7, 10);
        data.expiry_date = tracks[2].substr(0, 2);
        data.sex = tracks[2].substr(7, 1);
        data.dob = tracks[2].substr(8, 8);
        data.version_code = tracks[2].substr(16, 2);
        data.fname = name[1];
        data.lname = name[0];

        data.sex = (data.sex == '1') ? 'M' : 'F';
        if (data.expiry_date == '00') {
            data.expiry_date = '';
        }
        else {
            data.expiry_date = '20' + data.expiry_date + data.dob.substr(4, 4);
        }

        return data;
    },
    PrintLabel: function (s,noCopy = null) {
        var patient_id = $('#patient_id').html();
        var dataURL = 'patient_id=' + patient_id;

        $.ajax({
            type: 'GET',
            data: dataURL,
            url: workflowActions.actions.getPatient,
            success: function(rs) {

                if (rs.hasOwnProperty('error')) {
                    alert(rs['error']);
                    return;
                }

                var patientObj = JSON.parse(rs);

                var patientid = patientObj.id;
                var patientaddr = patientObj.address;
                var patienthomephone = patientObj.hphone;
                var patientcellphone = patientObj.cellphone;
                var patientophone = patientObj.other_phone;
                var patientwphone = patientObj.wphone;
                var patientdob = patientObj.dob;
                var patientgender = patientObj.sex;
                var patientname = patientObj.lname;
                var patientfname = patientObj.fname;
                var patientlfname = patientname + ', ' + patientlfname;
                var patientchart = patientObj.patient_number;
                var patientcity = patientObj.city;
                var patientpostalcode = patientObj.postal_code;
                var patientprovince = patientObj.province;
                var patienthcn = patientObj.hcn_num;
                var patientversion = patientObj.hcn_version_code;

                if (patienthomephone) {
                    patienthomephone = patienthomephone.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
                }

                if (patientcellphone) {
                    patientcellphone = patientcellphone.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
                }

                if (patientophone) {
                    patientophone = patientophone.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
                }

                if (patientwphone) {
                    patientwphone = patientwphone.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
                }

                var patient = {
                    id: patientid,
                    name: patientname,
                    fname: patientfname,
                    lfname: patientlfname,
                    addr: patientaddr,
                    hphone: patienthomephone,
                    cphone: patientcellphone,
                    ophone: patientophone,
                    wphone: patientwphone,
                    dob: patientdob,
                    gender: patientgender,
                    chart: patientchart,
                    city: patientcity,
                    post: patientpostalcode,
                    prov: patientprovince,
                    hcn: patienthcn,
                    vers: patientversion
                };

                var labelXML;
                var dataURL = 'type=' + s;

                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: workflowActions.actions.getLabels,
                    data: dataURL,
                    success: function(rs) {
                        if (rs.hasOwnProperty('error')) {
                            alert(rs['error']);
                            return;
                        }

                        labelXML = rs;

                        if (labelXML) {
                            var label = dymo.label.framework.openLabelXml(labelXML);

                            if (patient) {
                                var label_objects = label.getObjectNames();

                                for (obj in label_objects) {
                                    if (workflowActions.contains(label.getObjectNames(), label_objects[obj])) {
                                        var ele = label_objects[obj];
                                        var new_text = patient[ele.toLowerCase()];
                                        if (new_text) {
                                            label.setObjectText(label_objects[obj], new_text);
                                        }
                                    }


                                }
                            }

                            // select printer to print on
                            // for simplicity sake just use the first LabelWriter printer
                            var printers = dymo.label.framework.getPrinters();
                            if (printers.length == 0)
                                throw "No DYMO printers are installed. Install DYMO printers.";

                            var printerName = "";
                            for (var i = 0; i < printers.length; ++i) {
                                var printer = printers[i];
                                if (printer.printerType == "LabelWriterPrinter") {
                                    printerName = printer.name;
                                    break;
                                }
                            }

                            if (printerName == "")
                                throw "No LabelWriter printers found. Install LabelWriter printer";

                            // finally print the label
                            var i;
                            for(i = 0; i < noCopy; i++)
                            {
                                label.print(printerName);
                            }
                        }
                    },
                    error: function(rs) {
                        alert('An error has occured: ' + rs);
                    }
                });
            }
        });
    },
    contains: function(a, obj) {
        for (var i = 0; i < a.length; i++) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    },
    printNumberofCopy: function(action){
            printLabelString = action.replace('dymo','');
            if(workflowActions.canPrintCopies == true){
            $('.background_left').css('background','none');
            //Print number of copy code
            $("#print_number_of_copy_button").attr("disabled", false);
            $("#print_number_of_copy").on( "keypress", function(event) {
                    if (event.keyCode == 13 && !event.shiftKey) {
                        event.preventDefault();
                        $("#print_number_of_copy_button").trigger('click');
                    }
            });
            $('#dymo_print_number_of_copy_dialog').dialog({
                autoOpen: true,
                closeOnEscape: false,
                modal: true,
                position: {my: 'top', at: 'top+100'},
                minHeight: 350,
                width: 500
            });
            $('#print_number_of_copy').focus();
            var count = 1;
            $("#print_number_of_copy_button").click(function(){
                    var noCopy = $("#print_number_of_copy").val().trim();
                    if(count == 1) {
                      if(noCopy == '') {
                            alert('Please enter valid number');
                            return false;
                      }
                      workflowActions.PrintLabel(printLabelString,noCopy);
                      $("#print_number_of_copy_button").attr("disabled", true); 
                      $('#print_number_of_copy').val(1); 
                      $('#dymo_print_number_of_copy_dialog').dialog('close');
                    }
                    count++;
             });
        } else {
            workflowActions.PrintLabel(printLabelString,1);
        }
    }
};

