$(document).ready(function () {

    function alphaOnly(a) {
        var b = '';
        for (var i = 0; i < a.length; i++) {
            if ((a[i] >= 'A' && a[i] <= 'z') || a[i] == ' ')
                b += a[i];
        }
        return b;
    }

// first time report coloum data entry

    function initReportColumn() {
        var options = '';
        var firstOrder = [];
        var ct = 0;
        var oldorder = [];
        $("#mainReportSection input[type='checkbox']").each(function (chkbox) {
            firstOrder[$(this).val()] = $(this).attr('data-position');
            if ($(this).attr('data-position')) {
                ct++;
            }
            oldorder.push({val: $(this).val(), text: $(this).parent().find('label').text(), selected: $(this).is(":checked")});
        })
        var neworder = [];
        for (var r = 0; r < oldorder.length; r++) {
            if (firstOrder[oldorder[r].val]) {
                var kk = firstOrder[oldorder[r].val];
            } else {
                var kk = ct;
                ct++;
            }
            neworder[kk] = oldorder[r];
        }

        for (var newod in neworder) {
            if (neworder[newod]['selected']) {
                options += '<option selected value="' + neworder[newod]['val'] + '">' + neworder[newod]['text'].replace(/ *\([^)]*\) */g, "") + '</option>';
            } else {
                options += '<option value="' + neworder[newod]['val'] + '">' + neworder[newod]['text'].replace(/ *\([^)]*\) */g, "") + '</option>';
            }
        }

        $('#select2pc').html(options);
    }
    
    initReportColumn();
    var selectoptions = $('#select2pc');

//console.log(neworder);

    setTimeout(function () {

    $('#mainReportSection input[type= "checkbox"]').trigger("change");

        $('#select2pc').select2('destroy');
        var printselect = $('#select2pc').select2Sortable({width: '100%'});

        var opts = $('#select2pc').val();

        printselect.on("change", function (e) {

            if (e.removed) {
                $("#past_claims_report_columns_" + e.removed.id).attr('checked', false);
            }
            if (e.added) {
                $("#past_claims_report_columns_" + e.added.id).attr('checked', true);
            }
            $('#select2pc').html('');
            $('#select2pc').select2('destroy');
//            printselect.val(null).trigger('change');
            initReportColumn();
            $('#select2pc').select2Sortable({width: '100%'});
//            printselect.trigger("change");

        });
        $('#select2pc').html('');
        $('#select2pc').select2('destroy');
        initReportColumn();
        $('#select2pc').select2Sortable({width: '100%'});
            
        $("#mainReportSection input[type='checkbox']").change(function () {
            $('#select2pc').html('');
            $('#select2pc').select2('destroy');
            initReportColumn();
            $('#select2pc').select2Sortable({width: '100%'});
        });

        $(".btnColChange").click(function () {
            //setTimeout(function(){
            $('#select2pc').html('');
            printselect.val(null).trigger('change');
            initReportColumn();
            printselect.trigger("change");
            //},500);
        });



    }, 1000);



    $("#find_button").click(function () {
        $('#select2pc').select2SortableOrder();
        //$('#e15_val').val( $('#select2pc').val());
    })

// advanced search config

    var allClaimItemStatus = [];
    $(".getClaimStatus input").each(function (e) {
        var val = $(this).val();
        if (val != 'all') {
            allClaimItemStatus.push({id: val, text: val.replace(/\_/g, ' ')});
        }
    })
    var allClaimItemType = [];
    $(".getClaimType input").each(function (e) {
        var val = $(this).val();
        if (val != 'all') {
            allClaimItemType.push({id: val, text: val.replace(/\_/g, ' ')});
        }
    })
    var allPayment = [];
    $(".getPayment input").each(function (e) {
        var val = $(this).val();
        if (val != 'all') {
            allPayment.push({id: val, text: val.replace(/\_/g, ' ')});
        }
    })
    var allPaymentMethod = [];
    $(".getPaymentMethod select option").each(function (e) {
        var val = $(this).val();
        if ($(this).text() != '[Payment Method]') {
            allPaymentMethod.push({id: 'payment_method', text: $(this).text()});
        }
    })

    var allDoctorProfile = [];
    $(".getDoctorProfiles select option").each(function (e) {
        var val = $(this).val();
        if (val != '' && $(this).text() != '[Doctor Profile]') {
            allDoctorProfile.push({id: 'doctor_id', text: $(this).text()});
        }

    })
    var allBillingLocation = [];
    $(".getBillingLocation select option").each(function (e) {
        var val = $(this).val();
        if (val != '' && $(this).text() != '[Location]') {
            allBillingLocation.push({id: 'claim_location_id', text: $(this).text()});
        }

    })
    var allRAFile = [];
    $(".getRaFile select option").each(function (e) {
        var val = $(this).val();
        if (val != '' && $(this).text() != '[RA]') {
            allRAFile.push({id: 'ra', text: $(this).text()});
        }

    })
    var allBatchNumbers = [];
    $(".getRaFile select option").each(function (e) {
        var val = $(this).val();
        if (val != '' && $(this).text() != '[Batch Number]') {
            allBatchNumbers.push({id: 'batch_number', text: $(this).text()});
        }

    })
    var allManualReview = [];
    $(".getManualReview select option").each(function (e) {
        var val = $(this).val();
        if (val != '' && $(this).text() != '') {
            allManualReview.push({id: 'manual_review', text: $(this).text()});
        }

    })
    var allGroupNumber = [];
    $(".getGroupNumber select option").each(function (e) {
        var val = $(this).val();
        if (val != '' && $(this).text() != '[Group Number]') {
            allGroupNumber.push({id: 'group_number', text: $(this).text()});
        }

    })


    var ALL_OPTIONS = [
        {
            id: 'claim_status-all',
            text: 'Claim Item Status',
            children: allClaimItemStatus
        },
        {
            id: 'claim_type-all',
            text: 'Claim Type',
            children: allClaimItemType
        },
        {
            id: 'payment_status-all',
            text: 'Payment Status',
            children: allPayment
        },
        {
            id: '',
            text: 'Payment Method',
            children: allPaymentMethod
        },
        {
            id: '',
            text: 'Doctor Profile',
            children: allDoctorProfile
        },
        {
            id: '',
            text: 'Billing Location',
            children: allBillingLocation
        },
        {
            id: '',
            text: 'RA File',
            children: allRAFile
        },
        {
            id: '',
            text: 'Batch Number',
            children: allBatchNumbers
        },
        {
            id: '',
            text: 'Manual Review',
            children: allManualReview
        },
        {
            id: '',
            text: 'Group Number',
            children: allGroupNumber
        }
    ];
//    $(".txtMainSearchFields").select2({tags: []});

    $('.txtMainSearchFields').select2({
        multiple: true,
        placeholder: "Select fruits...",
        data: ALL_OPTIONS,
        query: function (options) {
            var selectedIds = options.element.select2('val');
            var selectableGroups = $.map(this.data, function (group) {
                var areChildrenAllSelected = true;
                $.each(group.children, function (i, child) {
                    if (selectedIds.indexOf(child.id) < 0) {
                        areChildrenAllSelected = false;
                        return false; // Short-circuit $.each()
                    }
                });
                return !areChildrenAllSelected ? group : null;
            });
            options.callback({results: selectableGroups});
        }
    }).on('select2-selecting', function (e) {
        var $select = $(this);
        if (e.val == '') {
            e.preventDefault();
            $select.select2('data', $select.select2('data').concat(e.choice.children));
            $select.select2('close');
        }
    });

    $('#showValue').click(function () {
        //console.log($('#fruitSelect').val());
    });




//    $(".newDrProfile").html($("#past_claims_doctor_id").html());
//    $(".newDrProfile option:eq(0)").text('All Doctor Profile');
    var otherSearchOptions = '';
    $(".claim_type_div ul li").each(function () {
        otherSearchOptions += '<option value="' + $(this).text() + '">' + $(this).text() + '</option>';
    })
    $(".otherSearchFilter").html(otherSearchOptions);
    $(".otherSearchFilter option:eq(0)").text('Claim Type');
    setTimeout(function () {
        $(".otherSearchFilter").select2('destroy');
        $(".otherSearchFilter").select2({placeholder: "Claim Type", width: '100%'})
    }, 200);


//    $(".txtMainSearchFields").select2({tags: []});

    init();


    $("#panel_tab5_example1 input[type='text']").blur(function () {
        inputFunctions($(this).attr('name'), $(this).val());
    })
    $("#panel_tab5_example2 input[type='text']").blur(function () {
        inputFunctions($(this).attr('name'), $(this).val());
    })
    $("#panel_tab5_example3 input[type='text']").blur(function () {         
        inputFunctions($(this).attr('name'), $(this).val());
    })
    $("#panel_tab5_example4 input[type='text']").blur(function () {
        inputFunctions($(this).attr('name'), $(this).val());
    })

    function inputFunctions(name, val) {
        name = name.replace('past_claims', '');
        name = name.replace(/\[/g, '');
        name = name.replace(/\]/g, '');
        if (val != '' && val != ' ' && val != '  ' && val != '  ') {

            if (chkValInPreviousData(name)) {
                if (val == '') {
                    var newdata = removeFromOldData(name, val);
                    updateDataSet(newdata);
                } else {
                    var oldval = $(".txtMainSearchFields").select2('data');
                    for (var j = 0; j < oldval.length; j++) {
                        if (oldval[j]['id'] == name) {
                            oldval[j]['text'] = val;
                        }
                    }
                    updateDataSet(oldval);
                }
            } else {
                var newdata = pushToOldData(name, val);
                updateDataSet(newdata);
            }
        } else {
            if (chkValInPreviousData(name)) {
                var newdata = removeFromOldData(name, val);
                updateDataSet(newdata);
            }
        }
    }


    function chkBoxesInit() {
        $(".checkboxesInputs input[type='checkbox']").each(function () {
            var name = $(this).attr('name');
            name = name.replace('past_claims', '');
            name = name.replace(/\[/g, '');
            name = name.replace(/\]/g, '');
            var val = $(this).val();

            if ($(this).attr('checked')) {

                if (val == 'all') {
                    val = name + '-' + val;
                }
                if (!chkValInPreviousData(val)) {
                    var newdata = pushToOldData(val, val.replace(/\_/g, ' '));
                    updateDataSet(newdata);
                }
            } else {
                if (val == 'all') {
                    val = name + '-' + val;
                    var newdata = removeFromOldData(val, val.replace(/\_/g, ' '));
                    updateDataSet(newdata);
                }
                if (chkValInPreviousData(val)) {
                    var newdata = removeFromOldData(val, val.replace(/\_/g, ' '));
                    updateDataSet(newdata);
                }
            }
        })
    }

    $(".checkboxesInputs input[type='checkbox']").change(function () {
        var name = $(this).attr('name');
        name = name.replace('past_claims', '');
        name = name.replace(/\[/g, '');
        name = name.replace(/\]/g, '');
        var val = $(this).val();
        var OldData = $(".txtMainSearchFields").select2('data');
        if ($(this).attr('checked')) {
            if (val == 'all') {
                val = name + '-' + val;
                if (name == 'claim_status') {
                    var array = ['unsubmitted', 'submitted', 'rejected', 'rejected_closed', 'paid', 'paid_by_user'];
                }
                if (name == 'claim_type') {
                    var array = ['hcp-p', 'hcp-s', 'wcb', 'rmb', 'direct', 'third_party', 'adjudication'];
                }
                var newvar = [];
                for (var j in OldData) {
                    var k = false;
                    if (typeof OldData[j] !== 'undefined') {
                        for (var i in array) {
                            //console.log(OldData[j]['id']+' '+ array[i]);
                            if (OldData[j]['id'] != array[i]) {
                                k = false;
                            } else {
                                k = true;
                                break;
                            }
                        }
                    }
                    if (k) {
                        delete OldData[j];

                    }
                }
                var te = [];
                for (var t in OldData) {
                    if (typeof OldData[t] !== 'undefined') {
                        te.push(OldData[t])
                    }
                }
                OldData = te;
                if (!chkValInPreviousData(val)) {
                    OldData.push({id: val, text: val.replace(/\_/g, ' ')});
                    updateDataSet(OldData);
                }
            } else {
                var testval = name + '-all';
                for (var j = 0; j < OldData.length; j++) {
                    if (OldData[j]['id'] == testval) {
                        OldData.splice(j, 1);
                    }
                }
                OldData.push({id: val, text: val.replace(/\_/g, ' ')});
                updateDataSet(OldData);
            }
        } else {
            if (val != 'all') {
                for (var j = 0; j < OldData.length; j++) {
                    if (OldData[j]['id'] == val) {
                        OldData.splice(j, 1);
                    }
                }
                updateDataSet(OldData);
            }
//                if (chkValInPreviousData(val)) {
//                    var newdata = removeFromOldData(val, val.replace(/\_/g, ' '));
//                    updateDataSet(newdata);
//                }
        }
        chkAllUncheckorNot('.checkboxesInputs');
    })


   

    function paymentTypeChkox() {
        $(".paymentTypeChkbox input").each(function () {
            var name = $(this).attr('name');
            name = name.replace('past_claims', '');
            name = name.replace(/\[/g, '');
            name = name.replace(/\]/g, '');
            var val = $(this).val();

            if ($(this).attr('checked')) {
                if (val == 'all') {
                    val = name + '-' + val;
                }
                if (!chkValInPreviousData(val)) {
                    var newdata = pushToOldData(val, val.replace(/\_/g, ' '));
                    updateDataSet(newdata);
                }
            } else {
                if (val == 'all') {
                    val = name + '-' + val;
                    var newdata = removeFromOldData(val, val.replace(/\_/g, ' '));
                    updateDataSet(newdata);
                }
                if (chkValInPreviousData(val)) {
                    var newdata = removeFromOldData(val, val.replace(/\_/g, ' '));
                    updateDataSet(newdata);
                }
            }
        })
        chkAllUncheckorNot(".paymentTypeChkbox");
    }

    function claimTypeChkbox() {
        $(".claimTypeChkbox input").each(function () {
            var name = $(this).attr('name');
            name = name.replace('past_claims', '');
            name = name.replace(/\[/g, '');
            name = name.replace(/\]/g, '');
            var val = $(this).val();
            if ($(this).attr('checked')) {
                if (val == 'all') {
                    val = name + '-' + val;
                }
                if (!chkValInPreviousData(val)) {
                    var newdata = pushToOldData(val, val.replace(/\_/g, ' '));
                    updateDataSet(newdata);
                }
            } else {
                if (val == 'all') {
                    val = name + '-' + val;
                    var newdata = removeFromOldData(val, val.replace(/\_/g, ' '));
                    updateDataSet(newdata);
                }
                if (chkValInPreviousData(val)) {
                    var newdata = removeFromOldData(val, val.replace(/\_/g, ' '));
                    updateDataSet(newdata);
                }
            }
        })
        chkAllUncheckorNot(".claimTypeChkbox");
    }

    function claimStatusChkbox() {
        $(".claimStatusChkbox input").each(function () {
            var name = $(this).attr('name');
            name = name.replace('past_claims', '');
            name = name.replace(/\[/g, '');
            name = name.replace(/\]/g, '');
            var val = $(this).val();

            if ($(this).attr('checked')) {

                if (val == 'all') {
                    val = name + '-' + val;
                }
                //console.log(chkValInPreviousData(val));
                if (!chkValInPreviousData(val)) {
                    var newdata = pushToOldData(val, val.replace(/\_/g, ' '));
                    //console.log(newdata);
                    updateDataSet(newdata);
                }
            } else {
                if (val == 'all') {
                    val = name + '-' + val;
                    var newdata = removeFromOldData(val, val.replace(/\_/g, ' '));
                    updateDataSet(newdata);
                }
                if (chkValInPreviousData(val)) {
                    var newdata = removeFromOldData(val, val.replace(/\_/g, ' '));
                    updateDataSet(newdata);
                }
            }
        })
        chkAllUncheckorNot(".claimStatusChkbox");
    }

    function chkAllUncheckorNot(from) {
        var val = $(from + ' input:checked').val();
        if (val == 'all') {
            var name = $(from + ' input:checked').attr('name');
            name = name.replace('past_claims', '');
            name = name.replace(/\[/g, '');
            name = name.replace(/\]/g, '');
            val = name + '-' + val;
            var newdata = {id: val, text: val.replace(/\_/g, ' ')};
            var oldval = $(".txtMainSearchFields").select2('data');
            if (oldval.length > 0) {
                oldval.push(newdata);
            } else {
                oldval = newdata;
            }
            updateDataSet(oldval);
        }
    }

    function getCheckedCount(from) {
        var count = 0;
        $(from).each(function () {
            if ($(this).attr('checked')) {
                count++;
            }
        })
        return count;
    }

    function chkValInPreviousData(olddata) {
        var oldval = $(".txtMainSearchFields").select2('data');
        if (oldval.length > 0) {
            for (var j = 0; j < oldval.length; j++) {
                if (oldval[j]['id'] == olddata) {
                    return true;
                }
            }
        }
        return false;
    }

    function pushToOldData(id, name) {
        var oldval = $(".txtMainSearchFields").select2('data');
        var newdata = {id: id, text: name};
        if (oldval.length > 0) {
            oldval.push(newdata);
        } else {
            oldval = newdata;
        }
        return oldval;
    }

    $("#clear_button").click(function () {
        var locations = pastClaims.actions.location_id;
        // console.log(pastClaims.form.formID);
        $('#' + pastClaims.form.formID + ' select').each(function () {

            $(this).val($(this).find('option:eq(0)').val()).trigger('change.select2');
            var data = $(this).select2('data');
            var name = $(this).attr('name');
            // console.log(name);

            name = name.replace('past_claims', '');
            name = name.replace(/\[/g, '');
            name = name.replace(/\]/g, '');
            var newdata = removeFromOldData(name, data.text, "1");
            updateDataSet(newdata);
            $('#past_claims_claim_location_id').val(pastClaims.actions.location_id);
            $("#past_claims_claim_location_id").trigger('change');
        })
    })

    function removeFromOldData(id, name, flag=null) {   
        var oldval = $(".txtMainSearchFields").select2('data');
        for (var j = 0; j < oldval.length; j++) {
            if(flag == "1"){
               if (oldval[j]['id'] == id || oldval[j]['id'] == "admit_date" || oldval[j]['id'] == "admit_date_start" || oldval[j]['id'] == "admit_date_end" || oldval[j]['id'] == "batch_date_start" || oldval[j]['id'] == "batch_date_end") {
                    oldval.splice(j);
                }
            }else{
                if (oldval[j]['id'] == id) {
                    oldval.splice(j);
                }
            }
        }
        return oldval;
    }

    function updateDataSet(data) {
        $(".txtMainSearchFields").select2("data", null);
        $(".txtMainSearchFields").select2("data", data);
    }

    function init() {
        setTimeout(function () {
            $(".firstHide").trigger('click');
            $(".firstHide").trigger('click');
            chkBoxesInit();
            toggleThirdPartyDiv('hide');
            toggleDirectPaymentDiv('hide');
        }, 500)
    }

    function toggleThirdPartyDiv(state) {
        if (state == 'show') {
            $('.third_party_div').each(function () {
                $(this).attr('style', 'display:block');
            });
        } else {
            $('.third_party_div').each(function () {
                $(this).attr('style', 'display:none');
            });
        }
    }
    function toggleDirectPaymentDiv(state) {
        if (state == 'show') {
            $('.direct_payment_div').each(function () {
                $(this).attr('style', 'display:block');
            });
        } else {
            $('.direct_payment_div').each(function () {
                $(this).attr('style', 'display:none');
            });
        }
    }

// checkbox onchange events
    $(".claimStatusChkbox input").change(function () {
        if ($(this).attr('checked')) {
            $(".claimStatusChkbox2 input[value='" + $(this).val() + "']").attr('checked', true);
        } else {
            $(".claimStatusChkbox2 input[value='" + $(this).val() + "']").attr('checked', false);
        }
    })




    $(".claimStatusChkbox2 input").change(function () {
        if ($(this).attr('checked')) {
            $(".claimStatusChkbox input[value='" + $(this).val() + "']").attr('checked', true);
        } else {
            $(".claimStatusChkbox input[value='" + $(this).val() + "']").attr('checked', false);
        }
        claimStatusChkbox();
        claimTypeChkbox();
        paymentTypeChkox();
    })

    $(".patientCheckbox input").change(function () {
        claimStatusChkbox();
        claimTypeChkbox();
        paymentTypeChkox();
        patientCheckbox();
    })


     function patientCheckbox() {
        $(".patientCheckbox input").each(function () {
            var name = $(this).attr('name');
             name = name.replace('past_claims', '');
            name = name.replace(/\[/g, '');
            name = name.replace(/\]/g, '');
            var val = $(this).val();

            if ($(this).attr('checked')) {
                if (val == 'all') {
                    val = name + '-' + val;
                }
                if (!chkValInPreviousData(val)) {
                    var newdata = pushToOldData(val, val.replace(/\_/g, ' '));
                    updateDataSet(newdata);
                }
            } else {
                if (val == 'all') {
                    val = name + '-' + val;
                    var newdata = removeFromOldData(val, val.replace(/\_/g, ' '));
                    updateDataSet(newdata);
                }
                if (chkValInPreviousData(val)) {
                    var newdata = removeFromOldData(val, val.replace(/\_/g, ' '));
                    updateDataSet(newdata);
                }
            }
        })
        chkAllUncheckorNot(".patientCheckbox");
    }
    



    $(".claim_type_div input").change(function () {
        if ($(this).attr('checked')) {
            $(".claim_type_div2 input[value='" + $(this).val() + "']").attr('checked', true);
            if ($(this).val() == 'third_party') {
                toggleThirdPartyDiv('show');
            }
            if ($(this).val() == 'direct') {
                toggleDirectPaymentDiv('show');
            }
        } else {
            $(".claim_type_div2 input[value='" + $(this).val() + "']").attr('checked', false);
            if ($(this).val() == 'third_party') {
                toggleThirdPartyDiv('hide');
            }
            if ($(this).val() == 'direct') {
                toggleDirectPaymentDiv('hide');
            }
        }
    })
    $(".claim_type_div2 input").change(function () {
        if ($(this).attr('checked')) {
            $(".claim_type_div input[value='" + $(this).val() + "']").attr('checked', true);
            if ($(this).val() == 'third_party') {
                toggleThirdPartyDiv('show');
            }
            if ($(this).val() == 'direct') {
                toggleDirectPaymentDiv('show');
            }
        } else {
            $(".claim_type_div input[value='" + $(this).val() + "']").attr('checked', false);
            if ($(this).val() == 'third_party') {
                toggleThirdPartyDiv('hide');
            }
            if ($(this).val() == 'direct') {
                toggleDirectPaymentDiv('hide');
            }
        }
        claimStatusChkbox();
        claimTypeChkbox();
        paymentTypeChkox();
    })

// advanced search select2 events
    $(".txtMainSearchFields").on('change', function (e) {
        if (e.removed) {
            if (e.removed.id == 'payment_status-all' || e.removed.id == 'claim_status-all' || e.removed.id == 'claim_type-all') {

                updateDataSet(pushToOldData(e.removed.id, e.removed.id.replace('_', ' ')));
            } else if (e.removed.id == 'batch_date') {
                $(".batchDateStart").skDP('setDate', null);
                $(".batchDateEnd").skDP('setDate', null);
            } /*else if (e.removed.id == 'admit_date') {
                $(".admitDateStart").skDP('setDate', null);
                $(".admitDateEnd").skDP('setDate', null);
            } */else if (e.removed.id == 'patient_dob') {
                $(".patientDOBStart").skDP('setDate', null);
                $(".patientDOBEnd").skDP('setDate', null);
            } else if(e.removed.id == 'payment_method'){
                    $('#s2id_past_claims_payment_method').removeClass('form-control');
            }else {
                $('form').find('input[value="' + e.removed.id + '"]').each(function () {
                    $(this).attr('checked', false);
                });
                $('form').find('input[name="past_claims[' + e.removed.id + ']"]').each(function () {
                    $(this).val('');
                });
                $('form').find('select[name="past_claims[' + e.removed.id + ']"]').each(function () {
                    //$(this).attr('disabled', true);
                    $(this).select2('destroy');
                    $(this).val(null);
                    $(this).select2();
//                    setTimeout(function(){$(this).trigger('change');},200);
                });
            }
            if (e.removed.id == 'direct') {
                toggleDirectPaymentDiv('hide');
            }
            if (e.removed.id == 'third_party') {
                toggleThirdPartyDiv('hide');
            }
        } else if (e.added) {
            if (e.added.id == 'claim_status-all') {
                $(".claim_status input").each(function () {
                    if ($(this).val() == 'all') {
                        $(this).attr('checked', true);
                    } else {
                        $(this).attr('checked', false);
                    }
                })
            } else if (e.added.id == 'claim_type-all') {
                $(".claim_type input").each(function () {
                    if ($(this).val() == 'all') {
                        $(this).attr('checked', true);
                    } else {
                        $(this).attr('checked', false);
                    }
                })
            } else if (e.added.id == 'payment_status-all') {
                $(".paymentTypeChkbox input").each(function () {
                    if ($(this).val() == 'all') {
                        $(this).attr('checked', true);
                    } else {
                        $(this).attr('checked', false);
                    }
                })
            } else if (e.added.id == 'payment_method') {
                $(".paymentMethodSelect").select2('destroy');
                $(".paymentMethodSelect").val(e.added.text.toUpperCase());//.trigger('change');
                $(".paymentMethodSelect").select2();
            } else {
                $('form').find('select[name="past_claims[' + e.added.id + ']"]').each(function () {
                    $(this).select2('destroy');
                    $(this).children("option").each(function () {
                        if ($(this).text().match(e.added.text)) {
                            this.selected = true;
                        }
                    });
                    $(this).select2();
                });
            }
            if (e.added.id == 'direct') {
                toggleDirectPaymentDiv('show');
            }
            if (e.added.id == 'third_party') {
                toggleThirdPartyDiv('show');
            }
            $('form').find('input[value="' + e.added.id + '"]').each(function () {
                $(this).attr('checked', true);
            });
        }

    })

// select box change event
    $(".drSelectMainPopular, .claimLocationPopular, .selectRAPopular, .selectBatchIdPopular, .facilityNumberSk, .skrefDoctor").change(function () {
        var data = $(this).select2('data');
        if (data.text != '[Doctor Profile]' || data.text != '[Location]' || data.text != '[RA]' || data.text != '[Batch Number]' || data.text != '[Facility Number]' || data.text != '[Referring Doctor]') {
            inputFunctions($(this).attr('name'), data.text);
        }
    })
    $("#past_claims_payment_method").change(function () {
        var data = $(this).select2('data');
        if (data.text != '[Payment Method]') {
            inputFunctions($(this).attr('name'), $(data).val());
        }
    })
    $("#past_claims_updated_by, #past_claims_created_by, #past_claims_manual_review, #past_claims_group_number, #past_claims_provider_number, #past_claims_spec_code").change(function () {
        var data = $(this).select2('data');
        if (data.text != '[Users]' || data.text != '[Provider Number]' || data.text != '[Group Number]' || data.text != '[Specialty Code]') {
            inputFunctions($(this).attr('name'), data.text);
        }
    })
    $("#panel_tab5_example4 select").change(function () {
        var data = $(this).select2('data');
        inputFunctions($(this).attr('name'), data.text);
    })

// select box linking
    var testSelectEvent = true;
    $('.drSelectMainPopular').on('change', function (e) {
        $('.drSelectMainClaim').select2('destroy');
        $('.drSelectMainClaim').val($(this).val());
        $('.drSelectMainClaim').select2();
//        if(testSelectEvent){
//            testSelectEvent = false;
//            $('.drSelectMainClaim').val($(this).val()).trigger('change');            
//        }else{ testSelectEvent = true; }
    })
    $('.drSelectMainClaim').on('change', function (e) {
        $('.drSelectMainPopular').select2('destroy');
        $('.drSelectMainPopular').val($(this).val());
        $('.drSelectMainPopular').select2();
        $('.drSelectMainPopular').trigger('change');
//       if(testSelectEvent){
//            testSelectEvent = false;
//            //console.log($(this).val());
//            $('.drSelectMainPopular').val($(this).val());
//            setTimeout(function(){$('.drSelectMainPopular').trigger('change'); },200);
//        }else{ testSelectEvent = true; }
    })

    $('.claimLocationClaim').on('change', function (e) {
        $('.claimLocationPopular').select2('destroy');
        $('.claimLocationPopular').val($(this).val());
        $('.claimLocationPopular').select2();
        $('.claimLocationPopular').trigger('change');
//        if(testSelectEvent){
//            testSelectEvent = false;
//            $('.claimLocationPopular').val($(this).val()).trigger('change');            
//        }else{ testSelectEvent = true; }        
    })
    $('.claimLocationPopular').on('change', function (e) {
        $('.claimLocationClaim').select2('destroy');
        $('.claimLocationClaim').val($(this).val());
        $('.claimLocationClaim').select2();

//        if(testSelectEvent){
//            testSelectEvent = false;
//            $('.claimLocationClaim').val($(this).val()).trigger('change');            
//        }else{ testSelectEvent = true; } 
//        
    })


    $('.selectBatchIdClaim').on('change', function (e) {
        $('.selectBatchIdPopular').select2('destroy');
        $('.selectBatchIdPopular').val($(this).val());
        $('.selectBatchIdPopular').select2();
        $('.selectBatchIdPopular').trigger('change');
//        if(testSelectEvent){
//            testSelectEvent = false;
//            $('.selectBatchIdPopular').val($(this).val()).trigger('change');            
//        }else{ testSelectEvent = true; }        
    })
    $('.selectBatchIdPopular').on('change', function (e) {
        $('.selectBatchIdClaim').select2('destroy');
        $('.selectBatchIdClaim').val($(this).val());
        $('.selectBatchIdClaim').select2();

//        if(testSelectEvent){
//            testSelectEvent = false;
//            $('.selectBatchIdClaim').val($(this).val()).trigger('change');            
//        }else{ testSelectEvent = true; } 

    })


    $('.selectRAPopular').on('change', function (e) {
        $('.selectRAClaim').select2('destroy');
        $('.selectRAClaim').val($(this).val());
        $('.selectRAClaim').select2();
//        if(testSelectEvent){
//            testSelectEvent = false;
//            $('.selectRAClaim').val($(this).val()).trigger('change');            
//        }else{ testSelectEvent = true; }        
    })
    $('.selectRAClaim').on('change', function (e) {
        $('.selectRAPopular').select2('destroy');
        $('.selectRAPopular').val($(this).val());
        $('.selectRAPopular').select2();
        $('.selectRAPopular').trigger('change');
//        if(testSelectEvent){
//            testSelectEvent = false;
//            $('.selectRAPopular').val($(this).val()).trigger('change');            
//        }else{ testSelectEvent = true; } 

    })


    $('.selectThirdPartyPopular').on('change', function (e) {
        if (testSelectEvent) {
            testSelectEvent = false;
            $('.selectThirdPartyService').val($(this).val()).trigger('change');
        } else {
            testSelectEvent = true;
        }
    })
    $('.selectThirdPartyService').on('change', function (e) {
        if (testSelectEvent) {
            testSelectEvent = false;
            $('.selectThirdPartyPopular').val($(this).val()).trigger('change');
        } else {
            testSelectEvent = true;
        }

    })





    $(".txtServiceCodePopular").blur(function () {
        $(".txtServiceCodeService").val($(this).val());
    })
    $(".txtServiceCodeService").blur(function () {
        $(".txtServiceCodePopular").val($(this).val());
    })
    $(".txtDiagCodePopular").blur(function () {
        $(".txtDiagCodeService").val($(this).val());
    })
    $(".txtDiagCodeService").blur(function () {
        $(".txtDiagCodePopular").val($(this).val());
    })


    sk.datepicker(".batchDateStart");
    $(".batchDateStart").on('changeDate', function (e) {
        $(".batchDateEnd").skDP('setStartDate', e.date);
        var date = $(this).val();
        var end = $(".batchDateEnd").val();
        end = end != '' ? end : null;
        batchDateExecute(date, end, 'Batch', 'batch_id');
    })

    sk.datepicker(".batchDateEnd");
    $(".batchDateEnd").on('changeDate', function (e) {
        $(".batchDateStart").skDP('setEndDate', e.date);
        var date = $(this).val();
        var end = $(".batchDateStart").val();
        end = end != '' ? end : null;
        batchDateExecute(end, date, 'Batch', 'batch_id');
    })

    sk.datepicker(".admitDateStart");
    $(".admitDateStart").on('changeDate', function (e) { 
        $(".admitDateEnd").skDP('setStartDate', e.date);
        var date = $(this).val();
        var end = $(".admitDateEnd").val();
        end = end != '' ? end : null;
        batchDateExecute(date, end, 'Admit', 'admit_date');
    })

    sk.datepicker(".admitDateEnd");
    $(".admitDateEnd").on('changeDate', function (e) {
        $(".admitDateStart").skDP('setEndDate', e.date);
        var date = $(this).val();
        var end = $(".admitDateStart").val();
        end = end != '' ? end : null;
        batchDateExecute(end, date, 'Admit', 'admit_date');
    })


    sk.datepicker(".patientDOBStart");
    $(".patientDOBStart").on('changeDate', function (e) {
        $(".patientDOBEnd").skDP('setStartDate', e.date);
        var date = $(this).val();
        var end = $(".patientDOBEnd").val();
        end = end != '' ? end : null;
        batchDateExecute(date, end, 'DOB', 'patient_dob');
    })
    sk.datepicker(".patientDOBEnd");
    $(".patientDOBEnd").on('changeDate', function (e) {
        $(".patientDOBStart").skDP('setEndDate', e.date);
        var date = $(this).val();
        var end = $(".patientDOBStart").val();
        end = end != '' ? end : null;
        batchDateExecute(end, date, 'DOB', 'patient_dob');
    })

    function batchDateExecute(start = null, end = null, text = '', id = '') {
        /*console.log(start);
        console.log(end);
        console.log(text);
        console.log(id);*/
        var newdata = removeFromOldData(id, '');
        updateDataSet(newdata);
        if (start == null && end != null) {
            var name = text + ' end date: ' + end;
            var newdata = pushToOldData(id, name);
            updateDataSet(newdata);
        } else if (start != null && end == null) {
            var name = text + ' start date: ' + start;
            var newdata = pushToOldData(id, name);
            updateDataSet(newdata);
        } else if (start != null && end != null) {
            var name = text + ' date: ' + start + ' to ' + end;
            var newdata = pushToOldData(id, name);
            updateDataSet(newdata);
        } else {
            var newdata = removeFromOldData(id, '');
            updateDataSet(newdata);
    }
    }



})
