
var contextMenu = {
    actions: {

    },
    active: {
        edit: true,
        delete_claim: true,
        submit: true,
        resubmit: true,
        mark_paid: true,
        mark_closed: true,
        details: true,
        pdf: true,
    },

    initialize: function () {
        var parameters = {
            selector: '.actions_button',
            trigger: 'hover',
            autoHide: true,
            className: 'claims_menu',
            callback: function (key, options, event) {
            },
            items: {
                edit: {
                    name: 'Edit Claim',
                    icon: 'edit',
                    callback: function (key, options, event) {
                        var item_id = contextMenu.getTriggeredItem(options);
                        pastClaims.handleEditClick(pastClaims.getClaimId(item_id), item_id, quickClaim.isRightClick(event));
                    },  
                    disabled: function () {
                        return !contextMenu.active['edit'];
                    }
                },
                resubmit: {
                    name: 'Mark Claim As Unsubmitted',
                    icon: 'resubmit',
                    callback: function (key, options, event) {
                        var item_id = contextMenu.getTriggeredItem(options);
                        pastClaims.handleResubmitClick(new Array(item_id));
                    },
                    disabled: function () {
                        return !contextMenu.active['resubmit'];
                    }
                },
                submit: {
                    name: 'Submit Claim',
                    icon: 'submit',
                    callback: function (key, options, event) {
                        var item_id = contextMenu.getTriggeredItem(options);
                        var claim_id = pastClaims.getClaimId(item_id);
                        pastClaims.handleSubmitClick(new Array(claim_id));
                    },
                    disabled: function () {
                        return !contextMenu.active['submit'];
                    }
                },
                sep1: '----',
                mark_paid: {
                    name: 'Mark Item As Paid',
                    icon: 'mark_paid',
                    callback: function (key, options, event) {
                        var item_id = contextMenu.getTriggeredItem(options);
                        var obj2 = JSON.parse(localStorage.getItem(item_id));
                        if (obj2['pay_progs']['DIRECT']) {
                            pastClaims.handleEditClick(pastClaims.getClaimId(item_id), item_id, quickClaim.isRightClick(event));
                        } else {
                            pastClaims.handleMarkPaidClick(new Array(item_id));
                        }
                    },
                    disabled: function () {
                        return !contextMenu.active['mark_paid'];
                    }
                },
                mark_closed: {
                    name: 'Mark Item As Closed',
                    icon: 'mark_closed',
                    callback: function (key, options, event) {
                        var item_id = contextMenu.getTriggeredItem(options);
                        pastClaims.handleMarkClosedClick(new Array(item_id));
                    },
                    disabled: function () {
                        return !contextMenu.active['mark_closed'];

                    }
                },
                replace_doctor: {
                    name: 'Replace Doctor',
                    icon: 'replace_doctor',
                    callback: function (key, options, event) {
                        var item_id = contextMenu.getTriggeredItem(options);
                        var claim_id = pastClaims.getClaimId(item_id);
                        pastClaims.handleMarkDoctorReplaceClick(claim_id);
                    },
                    disabled: function () {
                        return !contextMenu.active['mark_closed'];
                    }
                },
                delete: {
                    name: 'Delete All Items & Claim',
                    icon: 'delete_claim',
                     callback: function (key, options, event) {
                        var item_id = contextMenu.getTriggeredItem(options);
                        contextMenu.deleteBillingCycleSearchClaim(pastClaims.getClaimId(item_id));
                        // if (confirm("Are you sure you want to delete this claim?")) {
                        // window.location.href =  pastClaims.actions.delete_claim + '?id=' + pastClaims.getClaimId(item_id);
                        // //window.open(pastClaims.actions.delete_claim + '?id=' + pastClaims.getClaimId(item_id), 'delete_claim');
                        // //pastClaims.handleEditClick(pastClaims.getClaimId(item_id), item_id, quickClaim.isRightClick(event));
                        // } else
                        // {
                        //     return false;
                        // }

                    },
                    disabled: function () {
                        return !contextMenu.active['delete_claim'];
                    }
                },
                sep2: '----',
                details: {
                    name: 'Claim Details',
                    icon: 'details',
                    callback: function (key, options, event) {
                        var item_id = contextMenu.getTriggeredItem(options);
                        pastClaims.handleDetailsActionClick(pastClaims.getClaimId(item_id), quickClaim.isRightClick(event));
                    }
                },
                pdf: {
                    name: 'Claim Details PDF',
                    icon: 'pdf',
                    callback: function (key, options, event) {
                        var item_id = contextMenu.getTriggeredItem(options);
                        pastClaims.handlePdfActionClick(pastClaims.getClaimId(item_id));
                    }
                },
                sep3: '----',
                profile: {
                    name: 'Patient Profile',
                    icon: 'profile',
                    callback: function (key, options, event) {
                        var item_id = contextMenu.getTriggeredItem(options);
                        pastClaims.handleDetailsProfileActionClick(pastClaims.getPatientId(item_id), quickClaim.isRightClick(event));
                        // if (quickClaim.isRightClick(event)) {
                        //     //alert(1);
                        //     window.open(pastClaims.actions.profile + '?id=' + pastClaims.getPatientId(item_id), 'profile');
                        // } else {
                        //     // alert(2);
                        //     // alert(item_id);
                        //     window.location = pastClaims.actions.profile + '?id=' + pastClaims.getPatientId(item_id);
                        // }
                    }
                }
            }
        };

        this.active.delete_claim = (contextMenu.actions.delete_claim) ? contextMenu.actions.delete_claim : false;

        for (var a in this.active) {
            if (!this.active[a]) {
                delete parameters.items[a];
            }
        }

        if (!$.isEmptyObject(parameters.items)) {
            $.contextMenu(parameters);
            contextMenu.setTitle('Placeholder Title');
        }
    },
    createButton: function (id) {
        $('a#' + id).hover(function () {
            var item_id = $(this).attr('id').replace('actions_', '');
            var item_count = pastClaims.data[item_id].item_count;
            contextMenu.setTitle('Claim ' + pastClaims.getClaimId(item_id));
            contextMenu.setActiveItems(item_id);
            if (item_count > 1) {
                $('li.icon-mark_paid span').text('Mark Item As Paid [' + item_count + ' Items]');
                $('li.icon-resubmit span').text('Mark Claim As Unsubmitted [' + item_count + ' Items]');
                $('li.icon-submit span').text('Submit Claim [' + item_count + ' Items]');
                $('li.icon-mark_closed span').text('Mark Item As Closed [' + item_count + ' Items]');
            } else {
                $('li.icon-mark_paid span').text('Mark Item As Paid');
                $('li.icon-resubmit span').text('Mark Claim As Unsubmitted');
                $('li.icon-submit span').text('Submit Claim');
                $('li.icon-mark_closed span').text('Mark Item As Closed');
            }
        });
    }, 
    deleteBillingCycleSearchClaim:function (claimId)
    {
        if(confirm("Are you sure you want to delete?")) {
            $.ajax({
                    type: 'GET',
                    url:  pastClaims.actions.delete_claim,
                    data: {id:claimId},
                    success: function (rs) {
                        //$('#past_claims_results_table .claim_'+claimId).remove();
                        var table = $('#past_claims_results_table').DataTable();
                        table.rows($('.claim_'+claimId)).remove().draw();
                    },
                    error: function (rs) {
                    }
                    });
        }
        else
        {
            return false;
        }
    },
    getTriggeredItem: function (options) {
        var id = $(options.$trigger).attr('id');
        if (id) {
            return id.replace('actions_', '');
        }
        return null;
    },
    disable: function (item) {
        contextMenu.active[item] = false;
    },
    enable: function (item) {
        contextMenu.active[item] = true;
    },
    setActiveItems: function (item_id) {
        if (!pastClaims.data[item_id]['mark_paid']) {
            contextMenu.disable('mark_paid');
        } else {
            contextMenu.enable('mark_paid');
        }

        if (!pastClaims.data[item_id]['mark_closed']) {
            contextMenu.disable('mark_closed');
        } else {
            contextMenu.enable('mark_closed');
        }

        if (!pastClaims.data[item_id]['submit']) {
            contextMenu.disable('submit');
        } else {
            contextMenu.enable('submit');
        }

        if (!pastClaims.data[item_id]['resubmit']) {
            contextMenu.disable('resubmit');
        } else {
            contextMenu.enable('resubmit');
        }

        if (!pastClaims.data[item_id]['edit']) {
            contextMenu.disable('edit');
        } else {
            contextMenu.enable('edit');
        }
        if (!pastClaims.data[item_id]['delete_claim']) {
            contextMenu.disable('delete_claim');
        } else {
            contextMenu.enable('delete_claim');
        }
    },
    setTitle: function (title) {
        $('.claims_menu').attr('data-menutitle', title);
    }
};