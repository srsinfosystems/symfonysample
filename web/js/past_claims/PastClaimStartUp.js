
$('#past_claims_results_table').on('update', function () {
    alert('Table updated, launching kill space goats sequence now.')
});

$(document).ready(function () {
$("#past_claims_payment_status_all").prop('checked', true);
        $('#tabs').tabs({
disabled: [1],
        postShow: function () {
        //quickClaim.focusTopField();
        }
});
        $('#tabs').tabs('tabSwitching');
        $('.pastClaimsPager .pagesize').val(<?php echo $form->getPageSize(); ?>);

contextMenu.active.mark_closed = <?php echo $sf_user->hasCredential(hype::PERMISSION_NAME_CLAIM_MARK_AS_PAID) ? 'true' : 'false'; ?>;
contextMenu.active.mark_paid = <?php echo $sf_user->hasCredential(hype::PERMISSION_NAME_CLAIM_MARK_AS_PAID) ? 'true' : 'false'; ?>;
contextMenu.active.submit = <?php echo $can_submit ? 'true' : 'false'; ?>;
contextMenu.initialize();

claimStatusActions.actions.mark_closed = '<?php echo url_for('claimItem/markClosedGroup'); ?>';
claimStatusActions.actions.mark_paid = '<?php echo url_for('claimItem/markPaidGroup'); ?>';
claimStatusActions.actions.resubmit = '<?php echo url_for('claim/resubmitGroup'); ?>';

claimStatusActions.baseClass = 'pastClaims';
claimStatusActions.messages.spinnerHtml = '<?php echo image_tag('icons/spinner.gif'); ?>';
claimStatusActions.messages.checkmarkHtml = '<?php echo image_tag('icons/checkmark.png', array('height' => '16')); ?>';

claimStatusActions.initialize();

pastClaims.actions.claimPDF = '<?php echo url_for('claim/pdf'); ?>';
pastClaims.actions.details = '<?php echo url_for('claim/details'); ?>';
pastClaims.actions.edit = '<?php echo url_for('claim/index'); ?>';
pastClaims.actions.profile = '<?php echo url_for('patient/index'); ?>';
pastClaims.actions.refdocAutocomplete = '<?php echo url_for('referring_doctor/autocomplete'); ?>';
pastClaims.actions.search = '<?php echo url_for('past_claim/search'); ?>';
pastClaims.actions.prevSearch = '<?php echo url_for('past_claim/previousSearch'); ?>';
pastClaims.actions.changeItems = '<?php echo url_for('past_claim/changeItems'); ?>';

pastClaims.results.menuHtml = '<?php echo image_path('icons/redDown.png'); ?>';
pastClaims.results.sortOrder = <?php echo json_encode($form->getSortOrderJSON()); ?>;
pastClaims.results.paginationSize = <?php echo $form->getPageSize(); ?>;
pastClaims.results.showTotals = <?php echo $sf_user->hasCredential(hype::PERMISSION_NAME_PAST_CLAIMS_TOTAL) ? 'true' : 'false'; ?>;
pastClaims.results.showRebill = <?php echo $sf_user->getClientHasModule(hype::MODULE_REBILLING) ? 'true' : 'false'; ?>;

pastClaims.form.formID = '<?php echo sfOutputEscaper::escape('esc_specialchars', $form_id); ?>';
pastClaims.form.prefix = '<?php echo sfOutputEscaper::escape('esc_specialchars', $form_prefix); ?>';

pastClaims.initialize();

batch_create.actions.batch_create = "<?php echo url_for('ohip/batchCreate'); ?>";
batch_create.actions.batch_distinct_counts = "<?php echo url_for('ohip/submitBatchCount'); ?>";
batch_create.actions.summaryPDF = "<?php echo url_for('ohip/batchSummaryPDF'); ?>";
batch_create.error_html = $('#error_icon').html().replace("style=\"display:none\"", ""),
batch_create.spinner_html = $('#spinner').html().replace("style=\"display:none\"", ""),
batch_create.initialize();

setupGlobalKeypresses();
$('#tabs').show();
//quickClaim.focusTopField();
});

function setupGlobalKeypresses() {
        $(document).keydown(function (event) {
if (event.which == 13) { // enter keypress
var current_tab = $('#tabs').tabs('option', 'active');
        if (current_tab == 0) {
pastClaims.handleFindClick();
}
}
});
}

$(document).ready(function () {
        $(".txtMainSearchFields").select2({tags: []});
$("#panel_tab5_example1 input").change(function () {
        var name = $(this).attr('name');
        name = name.replace('past_claims', '');
        name = name.replace(/\[/g, '');
        name = name.replace(/\]/g, '');
        var val = $(this).val();
        var oldval = $(".txtMainSearchFields").select2('data');
        // console.log(oldval);
        if (name == 'claim_status') {
if (val == 'all') {

if (oldval.length > 0) {
for (var oa = 0; oa < oldval.length; oa++) {
if (oldval[oa]['id'] == "unsubmitted" || oldval[oa]['id'] == 'submitted' || oldval[oa]['id'] == 'rejected' || oldval[oa]['id'] == 'rejected_closed' || oldval[oa]['id'] == 'paid' || oldval[oa] == 'paid_by_user') {
oldval.splice(oa);
}
}
var newdata = {id: 'all', text: 'ALL'};
oldval.push(newdata);
$(".txtMainSearchFields").select2("data", null);
$(".txtMainSearchFields").select2("data", oldval);
} else {
        $(".txtMainSearchFields").select2("data", [{id: 'all', text: 'ALL'}]);
}
} else {
        console.log(oldval);
        if (oldval.length > 0) {

var dRes = false;
        for (var a = 0; a < oldval.length; a++) {
if (oldval[a]['id'] == val) {
} else if (oldval[a]['id'] == 'all') {
        oldval.splice(a);
} else {
        var newdata = {id: val, text: val};
oldval.push(newdata);
}
}
$(".txtMainSearchFields").select2("data", null);
$(".txtMainSearchFields").select2("data", oldval);
} else {
                $(".txtMainSearchFields").select2("data", [{id: val, text: val}]);
}
}
}
//console.log(name);
//console.log($(this).val());
})

setTimeout(function () {
                $(".firstHide").trigger('click');
                $(".firstHide").trigger('click');
}, 500)
})

