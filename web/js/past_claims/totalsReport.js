var totalsReport = {
    actions: {
        search: null
    },
    messages: {
        error: 'error_icon',
        spinner: 'spinner',
        text: 'messages_text'
    },
    resultsTableInitialized: false,
    resultTableData: null,
    tableId: null,

    form: {
        initPerformanceGraph: function (data) {
            //console.log(data);
            var container = document.getElementById('visualization');

            var groups = new vis.DataSet();

            groups.add({
                id: 0,
                content: 'Paid',
                options: {
                    style: 'bar'
                }
            });

            groups.add({
                id: 1,
                content: 'Submitted',
                options: {
                    drawPoints: false
                }
            });

            groups.add({
                id: 2,
                content: 'Outstanding'
            });

            var items = new vis.DataSet({
                type: {x: 'ISODate', y: 'Number'}
            });


           
            data.forEach(function (entry) {
                entry.date = entry.date + '-01';
                //replace(',', '')
                items.add({x: entry.date, y: parseInt(entry.total_paid.substring(1).replace(/,/g, "")), group: 0});
                items.add({x: entry.date, y: parseInt(entry.total_submitted.substring(1).replace(/,/g, "")), group: 1});
                items.add({x: entry.date, y: parseInt(entry.total_outstanding.substring(1).replace(/,/g, "")), group: 2});
            });

            var options = {
                start: new Date().addYears(-1),
                end: new Date(),
                legend: true
            };
            var graph2d = new vis.Graph2d(container, items, groups, options);
        },
        initialize: function () {


            totalsReport.form.moveFields();
            totalsReport.form.toggleFields();
            totalsReport.form.formatDoctorDialogLists();

            sk.datepicker('#monthly_totals_date_range_start');
            sk.datepicker('#monthly_totals_date_range_end')


            $('#totalsSubmitButton').button();
            $("#totalsSubmitButton").click(function () {
                totalsReport.sendSearchAjax();
            });

            $('#totalsForm :input').change(function () {
                totalsReport.form.toggleFields($(this).prop('id'));
                if (totalsReport.form.isSubmittable()) {
                    $('#totalsSubmitButton').button('enable');
                } else {
                    $('#totalsSubmitButton').button('disable');
                }
            });

            $('#doctorSelectionDialog').dialog({
                closeOnEscape: true,
                autoOpen: false,
                height: $(window).height() - 200,
                modal: true,
                width: '80%',
                open: function () {
                    $('#docotorSelectionButtonset .ui-state-focus').removeClass('ui-state-focus');
                }
            });

            $('#docotrSelectionWrench').click(function () {
                $('#doctorSelectionDialog').dialog('open');
            });

//            $('#docotorSelectionButtonset').buttonset();

            $('#totalsForm :input[name="monthly_totals[doctor_selection_type]"]').click(function () {
                totalsReport.sendSearchAjax();
                
                totalsReport.form.updateDoctorSelectionValues();
                totalsReport.form.updateDoctorSelectionType2($(this).val());
            });

            $('#doctorSelectionDialog :input[name="monthly_totals[doctor_selection_type2]"]').click(function () {
                totalsReport.form.updateDoctorSelectionValues();
                totalsReport.form.updateDoctorSelectionType($(this).val());
            });

            $('#doctorSelectButton').button();
            $('#doctorSelectButton').click(function () {
                totalsReport.form.updateDoctorSelectionValues();
                $('#doctorSelectionDialog').dialog('close');
                totalsReport.sendSearchAjax();
            });

            if (totalsReport.form.isSubmittable()) {
                $('#totalsSubmitButton').button('enable');
            } else {
                $('#totalsSubmitButton').button('disable');
            }
        },
        isSubmittable: function () {
            // date type -> must be one picked
            if ($('#totalsForm input[name="monthly_totals[date_type]"]:checked').size() == 0) {
                return false;
            }

            // date range -> radio must be picked; if other, we need at least start date
            if ($('#totalsForm input[name="monthly_totals[date_range]"]:checked').val() == 'other') {
                return ($('#totalsForm #monthly_totals_date_range_start').val() != '');
            }

            if ($('#totalsForm input[name="monthly_totals[date_range]"]:checked').size() == 0) {
                return false;
            }

            // doctor selection type -> radio must be picked
            if ($('#totalsForm input[name="monthly_totals[doctor_selection_type]"]:checked').size() == 0) {
                return false;
            }

            return true;
        },
        formatDoctorDialogLists: function () {
            var listSize = $('#doctorProfileSelection ul li').size();
            if (listSize > 40) {
                $('#doctorProfileSelection').addClass('threeCols');
            } else if (listSize > 20) {
                $('#doctorProfileSelection').addClass('twoCols');
            }

            var listSize = $('#providerNumberSelection ul li').size();
            if (listSize > 20) {
                $('#providerNumberSelection').addClass('twoCols');
            }

            $('#groupNumberSelection input').click(function () {
                totalsReport.form.toggleAllCheckbox('group_numbers', $(this).val());
            });

            $('#providerNumberSelection input').click(function () {
                totalsReport.form.toggleAllCheckbox('provider_numbers', $(this).val());
            });

            $('#doctorProfileSelection input').click(function () {
                totalsReport.form.toggleAllCheckbox('doctor_ids', $(this).val());
            });

            totalsReport.form.toggleAllCheckbox('group_numbers', null);
            totalsReport.form.toggleAllCheckbox('provider_numbers', null);
            totalsReport.form.toggleAllCheckbox('doctor_ids', null);
        },
        moveFields: function () {
            var html = $('#monthly_totals_date_range_start').parent().html();
            $('#monthly_totals_date_range_other').closest('li').append('<br />' + '<div id="otherDateRange" style="padding-left:16px;padding-top:5px;">' + html + '</div>');
            $('span #monthly_totals_date_range_start').parent().remove('span');

            var groupNumLI = $('#monthly_totals_doctor_selection_type_group_num').parent('li');
            html = '<li id="groupNumberListItem" class="selectionDetailsLI" style="display:none">GroupNumberListItem</li>';
            $(groupNumLI).after($(html));

            var providerNumLI = $('#monthly_totals_doctor_selection_type_provider_num').parent('li');
            html = '<li id="providerNumberListItem" class="selectionDetailsLI" style="display:none">ProviderNumberListItem</li>';
            $(providerNumLI).after($(html));

            var groupProviderNumLI = $('#monthly_totals_doctor_selection_type_group_provider').parent('li');
            html = '<li id="providerGroupNumberListItemGroup" class="selectionDetailsLI" style="display:none">Group</li>'
                    + '<li id="providerGroupNumberListItemProvider" class="selectionDetailsLI" style="display:none">Provider</li>';
            $(groupProviderNumLI).after($(html));

            var doctorProfileLI = $('#monthly_totals_doctor_selection_type_doctor_id').parent('li');
            html = '<li id="profileListItem" class="selectionDetailsLI" style="display:none">Profile</li>';
            $(doctorProfileLI).after($(html));

        },
        toggleFields: function (id) {
            // only show date range if OTHER is picked from date range type
            if ($('#totalsForm input[name="monthly_totals[date_range]"]:checked').val() == 'other') {
                $('#otherDateRange').show();

                if (id == 'monthly_totals_date_range_other') {
                    $('#totalsForm #monthly_totals_date_range_start').datepick('show');
                }
            } else {
                $('#otherDateRange').hide();
            }
        },
        toggleAllCheckbox: function (type, value) {
            var count = $('input[name="monthly_totals[' + type + '][]"]:checked').size();
            if (value == 'all') {
                $('input[name="monthly_totals[' + type + '][]"]:checked').each(function () {
                    if ($(this).val() != 'all') {
                        $(this).prop('checked', false);
                    } else {
                        $(this).prop('checked', (count > 0));
                    }
                });
            } else if (count == 0) {
                $('input[name="monthly_totals[' + type + '][]"][value=all]').prop('checked', true);
            } else {
                $('input[name="monthly_totals[' + type + '][]"][value=all]').prop('checked', false);
            }

            var count = $('input[name="monthly_totals[' + type + '][]"]:checked').size();
            if (count == 0) {
                $('input[name="monthly_totals[' + type + '][]"][value=all]').prop('checked', true);
            }
        },
        updateDoctorSelectionType: function (val) {
            $('#monthly_totals_doctor_selection_type_' + val).prop('checked', true);
            totalsReport.form.updateDoctorSelectionValues();
            totalsReport.form.toggleDoctorDialogSelections(val);
        },
        updateDoctorSelectionType2: function (val) {
            $('#monthly_totals_doctor_selection_type2_' + val).prop('checked', true);
            $("#docotorSelectionButtonset").buttonset('refresh');
            totalsReport.form.toggleDoctorDialogSelections(val);
        },
        updateDoctorSelectionValues: function () {
            var doctorSelectionType = $('#totalsForm :input[name="monthly_totals[doctor_selection_type]"]:checked').val();

            if (doctorSelectionType == 'none') {
                $('#groupNumberListItem').hide().text('');
                $('#providerNumberListItem').hide().text('');
                $('#providerGroupNumberListItemGroup').hide().text('');
                $('#providerGroupNumberListItemProvider').hide().text('');
                $('#profileListItem').hide().text('');
            } else if (doctorSelectionType == 'group_num') {
                var isAll = false;
                var text = '';

                $('select[name="monthly_totals[group_numbers][]"]:selected').each(function () {
                    if ($(this).val() != 'all') {
                        if (text.length) {
                            text += ', ';
                        }
                        text += $(this).val();
                    } else {
                        isAll = true;
                    }
                });

                if (isAll) {
                    $('#groupNumberListItem').hide().text('');
                } else {
                    $('#groupNumberListItem').show().text(text);
                }
                $('#providerNumberListItem').hide().text('');
                $('#providerGroupNumberListItemGroup').hide().text('');
                $('#providerGroupNumberListItemProvider').hide().text('');
                $('#profileListItem').hide().text('');
            } else if (doctorSelectionType == 'provider_num') {
                var isAll = false;
                var text = '';

                $('select[name="monthly_totals[provider_numbers][]"]:selected').each(function () {
                    if ($(this).val() != 'all') {
                        if (text.length) {
                            text += ', ';
                        }
                        text += $(this).val();
                    } else {
                        isAll = true;
                    }
                });

                if (isAll) {
                    $('#providerNumberListItem').hide().text('');
                } else {
                    $('#providerNumberListItem').show().text(text);
                }

                $('#groupNumberListItem').hide().text('');
                $('#providerGroupNumberListItemGroup').hide().text('');
                $('#providerGroupNumberListItemProvider').hide().text('');
                $('#profileListItem').hide().text('');
            } else if (doctorSelectionType == 'group_provider') {
                var isAllGroup = false;
                var isAllProv = false;

                var textGroup = '';
                var textProv = '';

                $('input[name="monthly_totals[group_numbers][]"]:checked').each(function () {
                    if ($(this).val() != 'all') {
                        if (textGroup.length) {
                            textGroup += ', ';
                        }
                        textGroup += $(this).val();
                    } else {
                        isAllGroup = true;
                    }
                });
                $('input[name="monthly_totals[provider_numbers][]"]:checked').each(function () {
                    if ($(this).val() != 'all') {
                        if (textProv.length) {
                            textProv += ', ';
                        }
                        textProv += $(this).val();
                    } else {
                        isAllProv = true;
                    }
                });

                if (isAllGroup) {
                    $('#providerGroupNumberListItemGroup').hide().text('');
                } else {
                    $('#providerGroupNumberListItemGroup').show().text(textGroup);
                }

                if (isAllProv) {
                    $('#providerGroupNumberListItemProvider').hide().text('');
                } else {
                    $('#providerGroupNumberListItemProvider').show().text(textProv);
                }

                $('#groupNumberListItem').hide().text('');
                $('#providerNumberListItem').hide().text('');
                $('#profileListItem').hide().text('');
            } else if (doctorSelectionType == 'doctor_id') {
                var text = '';
                var isAll = false;

                $('select[name="monthly_totals[doctor_ids][]"]:selected').each(function () {
                    if ($(this).val() != 'all') {
                        var quickCode = $('label[for=' + $(this).prop('id') + ']').text();
                        quickCode = quickCode.substring(0, quickCode.indexOf(' - '));

                        if (text.length) {
                            text += ', ';
                        }
                        text += quickCode;
                    } else {
                        isAll = true;
                    }
                });

                if (isAll) {
                    $('#profileListItem').hide().text('');
                } else {
                    $('#profileListItem').show().text(text);
                }

                $('#groupNumberListItem').hide().text('');
                $('#providerNumberListItem').hide().text('');
                $('#providerGroupNumberListItemGroup').hide().text('');
                $('#providerGroupNumberListItemProvider').hide().text('');
            }
        },
        toggleDoctorDialogSelections: function (val) {
            if (val == 'none') {
                $('#groupNumberSelection').hide();
                $('#providerNumberSelection').hide();
                $('#doctorProfileSelection').hide();
            } else if (val == 'group_num') {
                $('#groupNumberSelection').show();
                $('#providerNumberSelection').hide();
                $('#doctorProfileSelection').hide();
            } else if (val == 'provider_num') {
                $('#groupNumberSelection').hide();
                $('#providerNumberSelection').show();
                $('#doctorProfileSelection').hide();
            } else if (val == 'group_provider') {
                $('#groupNumberSelection').show();
                $('#providerNumberSelection').show();
                $('#doctorProfileSelection').hide();
            } else if (val == 'doctor_id') {
                $('#groupNumberSelection').hide();
                $('#providerNumberSelection').hide();
                $('#doctorProfileSelection').show();
            }
        }
    },

    initialize: function () {
        totalsReport.form.initialize();
        totalsReport.initTable();
        //totalsReport.GroupNumber.init();
        /* Group Number*/
        $("#groupNumberSelection ul li input").addClass("groupNumberSelection");
        a_global_liarary.initReportSelect("select2GroupNumber","monthly_totals_group_numbers_","groupNumberSelection");
        a_global_liarary.initReportColumn("select2GroupNumber","groupNumberSelection");

        /* Provider Number*/

        $("#providerNumberSelection ul li input").addClass("providerNumberSelection");
        a_global_liarary.initReportSelect("select2ProviderNumber","monthly_totals_provider_numbers_","providerNumberSelection");
        a_global_liarary.initReportColumn("select2ProviderNumber","providerNumberSelection");

        /* Provider Number*/

        $("#doctorProfileSelection ul li input").addClass("doctorProfileSelection");
        /*$(".doctorProfileSelection").click(function(){
            a_global_liarary.initReportSelect("select2DoctorProfile","monthly_totals_doctor_ids_","doctorProfileSelection");
            a_global_liarary.initReportColumn("select2DoctorProfile","doctorProfileSelection");
        });*/
        a_global_liarary.initReportSelect("select2DoctorProfile","monthly_totals_doctor_ids_","doctorProfileSelection");
        a_global_liarary.initReportColumn("select2DoctorProfile","doctorProfileSelection");

        

    },   
    initTable: function () {
        var tableId = 'resultsTable';
        if ( $.fn.DataTable.isDataTable( '#resultsTable' ) ) {
            $('#resultsTable').DataTable().destroy();
            $('#resultsTable').html('');
        }
        if (totalsReport.resultsTableInitialized == false) {
            totalsReport.resultsTableInitialized = true;
            $("#totalsSubmitButton").trigger('click');
            return false;
        }
        // thead
        var colHeader = [];
        for (var i in totalsReport.resultTableData.headers) {
            console.log(i);
            if (i == 'select') {
                colHeader.push({title: '<label class="CHKcontainer"><input type="checkbox" id="batchesTableSelectAll" value="1" checked="checked" /><span class="CHKcheckmark"></span></label>', class: 'edit_checkbox'});
            } else {
                colHeader.push({title: totalsReport.resultTableData.headers[i], class: i});
            }
        }
        
        var allData = totalsReport.resultTableData.data;
        var tblData = [];
        for (var i in allData) {
            var FinalSingle = [];
            for (var j in totalsReport.resultTableData.headers) {
                console.log(j);
                console.log(allData[i])
                FinalSingle.push(allData[i][j]);
            }
            tblData.push(FinalSingle);
        }
        console.log(colHeader);
        console.log(tblData);
                
        var table = totalsReport.dataTableWithLoader(tableId, true, ['pdf'], true, colHeader, tblData);
        $(".dataTables_empty").addClass('text-center').removeClass('dataTables_empty');

        totalsReport.tableId = tableId;
        totalsReport.form.initPerformanceGraph(totalsReport.resultTableData.data);
        global_js.dtLoader(totalsReport.tableId, 'stop');
    },
    newInitTable: function(rs){
        var tableId = 'resultsTable';
        if ( $.fn.DataTable.isDataTable( '#resultsTable' ) ) {
            $('#resultsTable').DataTable().destroy();
            $('#resultsTable').html('');
        }
        if (totalsReport.resultsTableInitialized == false) {
            totalsReport.resultsTableInitialized = true;
            $("#totalsSubmitButton").trigger('click');
            return false;
        }
        // thead
        var colHeader = [];
        for (var i in rs.headers) {
            console.log(i);
            if (i == 'select') {
                colHeader.push({title: '<label class="CHKcontainer"><input type="checkbox" id="batchesTableSelectAll" value="1" checked="checked" /><span class="CHKcheckmark"></span></label>', class: 'edit_checkbox'});
            } else {
                colHeader.push({title: rs.headers[i], class: i});
            }
        }
        
        var allData = rs.data;
        var tblData = [];
        for (var i in allData) {
            var FinalSingle = [];
            for (var j in rs.headers) {
                console.log(j);
                console.log(allData[i])
                FinalSingle.push(allData[i][j]);
            }
            tblData.push(FinalSingle);
        }
        console.log(colHeader);
        console.log(tblData);
                
        var table = totalsReport.dataTableWithLoader(tableId, true, ['pdf'], true, colHeader, tblData);
        $(".dataTables_empty").addClass('text-center').removeClass('dataTables_empty');

        totalsReport.tableId = tableId;
        totalsReport.form.initPerformanceGraph(rs.data);
        global_js.dtLoader(totalsReport.tableId, 'stop');

        if($('#monthly_totals_date_range_lastYear:checked').length == 1){
            $('select[name="resultsTable_length"]').val('12').trigger('change');
        }
    },
    update_table: function (tableInst) {

        if (totalsReport.resultsTableInitialized == false) {
            totalsReport.resultsTableInitialized = true;
            $("#totalsSubmitButton").trigger('click');

            return false;
        }

        tableInst = $("#" + totalsReport.tableId).DataTable();
        tableInst.clear().draw();

        var allData = totalsReport.resultTableData.data;
        var tblData = [];
        for (var i in allData) {
            var allSingle = allData[i];
            var FinalSingle = [];
            for (var j in allSingle) {
                FinalSingle.push(allSingle[j]);
            }
            tblData.push(FinalSingle);
        }

        setTimeout(Function.prototype, 500);
        tableInst.rows.add(tblData).draw();
        setTimeout(function () {
            $(".selectRsltTbl").select2('destroy');
            var aa = 0;
            var apiDt = $("#" + totalsReport.tableId).dataTable().api();
            apiDt.columns().every(function () {
                var column = this;
                var columnText = $.trim($(column.header())[0].innerText);
                $("#" + totalsReport.tableId + " thead tr:eq(1) th:eq(" + aa + ")").html('');

                var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                        .appendTo($("#" + totalsReport.tableId + " thead tr:eq(1) th:eq(" + aa + ")")/*$("#" + pastClaims.resultsTable + " thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                    );
                            val =  val.replace(/\\/gi, "");
                            column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                        });

                column.data().unique().sort().each(function (d, j) {
                    if(d != '') {
                    select.append('<option value="' + d + '">' + d + '</option>');
                    }
                });
                aa++;
            });

            $(".selectRsltTbl").select2({
                placeholder: "Search",
                allowClear: true,
                dropdownAutoWidth: true,
                width: '98%'
            });
            $('#' + totalsReport.tableId + ' .select2-arrow').hide();

            totalsReport.form.initPerformanceGraph(totalsReport.resultTableData.data);
            global_js.dtLoader(totalsReport.tableId, 'stop');           
        }, 1000);
    },
    dataTableWithLoader: function (tableId, filterFirst = false, ActionsBtns = [], select = true, colHeader = [], data = []) {
        var buttons = [];
        for (var s in ActionsBtns) {
            switch (ActionsBtns[s]) {
                case 'print':
                    buttons.push({
                        "extend": 'print',
                        "text": '<i class="fa-print fa"></i> Print',
                        "className": 'btn btn-blue btn-squared'
                    });
                    break;
                case 'excel':
                    buttons.push({
                        "extend": 'excel',
                        "text": '<i class="clip-file-excel"></i> Excel',
                        "className": 'btn btn-info btn-squared'
                    });
                    break;
                case 'pdf':

                    buttons.push({
                        extend: 'pdf',
                        text: '<i class="clip-file-pdf"></i> PDF',
                        className: 'btn btn-danger btn-squared pdfHtml5Btn',
                        extend: 'pdfHtml5',
//                        orientation: 'landscape',
                        filename: 'Total Reports',
                        pageSize: 'LEGAL',
                        customize: function (doc) {
                            doc.content[1].table.widths = ['25%', '15%', '15%', '15%', '10%', '10%', '10%'];
                            doc.styles.tableBodyOdd.alignment = 'left';
                            doc.styles.tableBodyOdd.noWrap = true;
                            doc.styles.tableHeader.alignment = 'left';
                            doc.styles.tableHeader.fontSize = 10;
                            doc.defaultStyle.fontSize = 8;
                            doc.styles.title = {
                                fontSize: '20',
                                alignment: 'left',
                                margin: [425, -50, 0, 10],
                            };
                            doc.content.splice(1, 0, {
                                margin: [0, -50, 0, 12],
                                alignment: 'right',
                                width: 250,
                                image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA5UAAACnCAYAAACB8njoAAAACXBIWXMAAC4jAAAuIwF4pT92AAAgAElEQVR42u3dfXBc5WHv8d/qzW/glXmLcQ0SJgQuwW9JeAkkkTxpCbSlkrNF3dtp481NMjfT2zuIy72dO8kMrP9oJ5O2gzxJKZ00ZU0y7bLpXuRLU0guvV6FJIVAYslOiCHBlsAYA8bW4mvZlmXv/WMfCVmWdvc5e3b3vHw/MxqwvXu055znnH1+53mLFAoFAQAAAADgRBOHAAAAAABAqAQAAAAAECoBAAAAAIRKAAAAAAChEgAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAAIRKAAAAAAChEgAAAABAqAQAAAAAECoBAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAABAqAQAAAAA1FFLWHc8ncl2S+o0P+2SNph/ape0foG35SUNz/pzbtZ/R+N9sVGKFAAAAIAwiRQKhTAEyA2Sus3PBkkdNfx1QyZ45iQNEzQBAAAAECr9FyI7TYDsNf+NNvDjjJiAORjvi+UocgAAAAAIld4Mku0mRPZr4e6rjZaXNCgpRcAEAAAAQKj0RpjslpSQtMVnH31M0oAJmOMURQAAAACEyvqGyYSkpGo7PrJetktKMv4SAAAAAKGSMEm4BAAAAECoDFOYbD52WC3Hj0iSml57peRrC8tXqBC9SJJ0auUHCJcAAAAACJUeD5MbVBx72OXG9hYdellNr72i5jffUNORvJpfeqeq7Z2Ntursmkt1duXlOnPp5Tq96v06c+ElbnzUvNnvAcZcAgAAACBU2ofJdhVbJu+pZjutRw+o5dd71DK2Xy27Dtbls59ZvUxnrunQmSvW6NRV61RoW1rN5sYkJZgtFgAAAAChsvJA2S0pJYddXVuPHlDbL55X8569aj5wvOH7c/qjazR1zfXVBsxtKnaJpdUSAAAAAKGyRKAckMPWycUvPau25/696i6ttXI22qqpD12jyQ9/QqdXrHayiTFJvfG+2DBFFwAAAACh8tww2SlpUNJ6qx2YnNDikSG17fyJmvKnfXPgpzau0uTHftPpZD/3xvtiAxRfAAAAAIRKzXR3HZQUDXqYdDFc7lBxrCXdYQEAAACEN1SapUIesXnPkt05tT015OswOV+4nPhUn+3MsSOSugmWAAAAAEIZKtOZbErSlkpfv+jQy1r0/e96dsykG0723qaT67tsJvTJm2DJOEsAAAAA4QmVNoEyMjmhpT/5ntqe/GkoTsqZ1ct0anPMpksswRIAAABAQzR5PVC2Hj2gC9N/F5pAKUnNB45r6dce1bIfPl7pW6KSculMdgNFGgAa8r3WHtD96uTsAgA8FyptAuWS3Tld8FcPB7q7ayltT/5Uyx99UM3HDhMsAcC7watbUiKggTLBGQbg8XtVO0chZKHSJlAue/oftfixp0N/gppfekcX/O3faNGhlwmWAOBNGyT1B3C/kmbfAMCrgbJXEsvshSlUVhooI5MTWv7og2rb+SJnZ/ok5U9r6dce1eKXniVYAoA3Q2WHmc08KBW1TvOdzXcJAC/rl7SF1sqQhErzRVs2UE6Pnwxrd9dyljz6L1ry/JOVBstBLjAAqFuolILVVXS65bWD0wvAi8zQg6459yw0SEsdTnivKliHsvXoAS39xjcDtfZkLSwe/JGa8kd1/Df/sNxLOyTlxFNmAKi19ea/XelMtjveF8v5vKLWPjsgB2GfAB9ehxskdZp6XHuF9blhSeOm/jca74uNBvwwJWf9f/+cP6POarqkiLkgciq2nBEoXTS56fpKgqUkbY/3xRK+L6iRSKe5uVZqtFAojDb4M1f6JVDxZ45EItNfLraGC4XCuEfOpaN9KBQKOZePty/KkQvnvhE8c9zqUPHrlrQzSPfcdCablPTArL/6bLwvlmpQpdovZX443hcbd7iftt9vVoL0QMBBmXB8Xhq0b72SuvVe61u1xkw9PCdp0C/HwuK62T/nrxtyr6piH2zrLeNeXj6wpcYHKuV2oIzeuka3bP+u1Wd5dsvvKP/jfXU7qFMbV+nY7/+J1Xsu/OeH1LLrYMWvL445/cdKguWWdCab89NFtoDEnEpOOVvV+CdWG+ZUNt34zJ2SHnfwWbxwPKaD0S4Hb91hvhTdPN6Vfubp/x3RrCfAknJ1Dk4DLlY0as0T5a2O1/nce27S5y0EiTL7SJk/36YK7lFufb/Z1sem/zevYkvWqPkZNqHLT2XVtkxUc17qFST7TZiM1uBXdKg4/GyLpEfSmewOEy79XifUAt8xSZM9/PT9YVNvGTIPHcIVKs2JXV+ysjY5oSVPfIcWSofadr6os5fldGJd2fI1kM5kh738dAOVKRQKg5FIZMhBReuBSCSS8kDrkdMZ2rwwVmKmi+OswDkmaVDSQFha5lA2VE6XV1+O7zFzIHR4JFTCXVFz/+qac84D25rl8essqfqPWe6R1JPOZAdM+Brw4wOwWROJnRei05lsb7wvNkgpq7+mGp3sXkn3lAuUTMpTvcWPPV3JrLBR+evJDWoTsBo65XYkEumVs1aHrR4ObB3mXrc/EomkTDdthMt85zzh44nSkhUGZwTHdGvWI5KOpjPZwSDNZOy1MJnOZEfNsW7kJFjR6e+udCabMiHN7/epautI8FqonNXttaSlPxgkULpkyaP/otajB8q9bL0ZJwOfKxQKw5K2O3hrTyQS6W7gR3cSavPyz/pTW0y4HDDjOxEOXQtU2HxXKTfjQ+er6EaZTTxUelTsKjmezmSTnHt3ri2PhMkFv7v8cq7NZ+wtdU9mWb2AhEpTASzZL3zJ7hzrULps6Te+qcjkRLmXPcCFFhj9JnDVI9hVLRKJJB1+kfZ7ZYIhC/dIypnxowh2RXFDmWvUb5Il/o3yHD5RFcd6jhIunQegdCY7qOK4Oa8vz/OAT67zfpUff0prpd9DpXnKWXI9ytajB7T4sac58m6fyPxpXfC/U54NFXCXCVpOzuX6SCSSqHOgbHd4gx8pFAopn56i9QTLUCh1fjvMUBC/VH67Vbp7ejenO/ThcthPZdoj19Soii2/cCmkV1if2OLDLr2EyjlKVgAjkxNakv4WR71GWnYd1JLduXIv62KsRGCCZVLF6cJt1bt7ZtneCwvw+5PGKMEy1KHSb2W43PcCFTR0SHrcjLmk1bJ0+Emq2DoZ5Wi4fp+q9JjSWunXUJnOZPtVpml/6U++p+YDxznqNbT4sacrGV85wBdCYDi5aUbrdbM1gWqLg7duL7cuJcESPgiVvhjbU2ImRUIl5tOjYpdY7mvnX0vt6Uw2pRouEUOdp/IASl3Xh6HSnLRkqdcsOvSy2p78KUe8DpY88R3PhArUVqFQGFRx3SJbD9RpplKnk/MkA3SaopIGmbwnkCqZzdgP99qkS/uK8IhK2kXPp/Pqwjk5e5CK8sc3Ibtxqb6cMC30oVJlBs1GJie06PEsR7tOml96p5JusP08wQkMTy4xUsUSIkFc87FDLOsTtApOpa00nh7bU8FMirNf28mZxxyPmDUPCZTFQLm+Rr9iSNIOSVsX+BmSNBLww5ysY/0IDrS4dCGVPGmLR4bo9lpnbU8N6eR1N6nQtnShl0y3ViY5Wv5WKBSGI5HINpVZG3YePZFIpLsW3UxNq5yTisaY6jeZVF7ScIl/d7tlpicSifSa1mX4n03Xv4SH77WVzKQ4e59HOfWY4550Jtse74slCJSu2WG2mYv3xYYtP0+3ihNr9dYw5Nb7GHfL2ey5HelMNhHvi6W4TH0QKst9ITUfO6y2nT/hSNdZU/60lv5gUMd/8w9Lnrt0JjsQ74uNc8R8Lym7AezTBlSbKcTLjrFeaD/quITIcKFQ6K4gIHebY9St6mfxG5BUr1C5KSDjUoMQKj15r7WYSXH2Pnv5ocimeF8sDGV+a7wvlqzg/HaqOBa23Zy76Z9aLG2xJZ3JKqTB0q1AOf1QNVXNvcJcAzlJSVMG+h3WD7xWx3EqIXoK+SZUlryBLH7u+2rKn+ZIN0Dbzhd18ubDOnPhJQu9ZLq/OcuM+FyhUBg3a0E+aPnW9ZFIJOHm0h1mrKaTLidDXlxCxASznN6bNbfXfME5emrq9vGGL0Jl1JQbr533XsuKJhOz+Ei8Lzaq91qWB+c8TOjVe61ZboWNLelMdjzeFwtNl0MzKU+1gXJMUrIWrWmmDPSb2Wir+e5q5DHuVnU9h7rSmWx3SB44NVRTlSc6UapwFlspX+QoN9Di575f7iX0Nw9OsByQN5YYSSqgS4gUCoXxQqGQKhQKnSqOY3F6fOB/XQE477afqZPTHoiwOR7vi6XifbFEvC/WLumzcjbh23zuCcvkPWbVg2on5dka74t11rp75vQ5V/HB0FYVh3/4Rb9HtoFahkpV0EqJxmrb+aKajx0u9ZIOFjMOFCdf5q7NBmy6ijpdQmTYTwfarBO6ycGXcwdLjPi+MukkXHnqXutgJkUpIOOzcF7gSMX7Yt3mfuZGuHwk6MuNmNazB6vYxIikjZV0Y65BuEyacLnDJ/faHhc21cNEYx4OlebkLPikNjI54YtWyuita9TxxR6t2/ZlffgfvnreT8cXe3TR7Tf4+iRXEO4TXArBYLpqNnKJESdfkHn59CmiOd5Orh+uOX9zWmH2Ujl3VAZNZRrBDJe5WeFyrMrNDQZ1hnmzX6kqNrFdUrftBDwun+vReF+sV9JmebvVMunRbWEe1YypLDvjq2d3esUi/cYf3KErP/0ZLb3q+pKvveTjd0mSJsff1r5v/rVef+wpTR095a+T/LNfKfKJiVIzwfaYmduYsCcYEpL2O3jfgCpcWmA+kUgkIedLiPi27BUKhcFIJLJddi20VMzDGSq70plspxnn1MhKcbecj1Hq5PQHP1yalsYBOe/e2WEq8UHsdpiS83GJ2700mVG8LzZozvWgPNYTwTReubnmZy913dqqpvtrycpn6wve7MkWvXWNPvrYP+q6+75SNlDO1tZ+qa677yv6+Pf/j6K3rvHXSc6f1qL9u6s6n/BVyBmVtM3BW3tM91UngbJdzp4CjplupH5nuw90I/S37jqWlVqopqJPqAxHsBw34efeKjZzT9Bats3+OO2OudWLs+OaVssNKrageonbDySiYmyl90Kleaqx4FOaxa/u9uS6lJdtvlm3bP+uVZicL1zesv27umzzzb460W3P/TuhMlySctalxelMwE6XEAnEDd4EeavxKU4DPDyhmvFiWxrZLdCFMUqU23CFywEVJ/JxKmizy6ccvm97vcdPOjjXCa/MkGrukbUI4IRKr4XKcie6+eWfe25HL7r9Bm38asq17X3wS3+pZWtX+uZEN7/0jlqPHij1kp6gjn8II9Od1MkX2HrTjdUmHHXK+RIigwE67Lb7wvXmQyaUVbsEQyMrNtVWbJlkKnzBMlVFsFwflNlgzbIcTh6ebg/p+p3V3iNrsa5mNCyzE/spVHaX+seWn/3KW4HqgqVa/+cPubrNtvZLdd29/81XJ7vtF89XdV7hu2BZryVGkg5v/kG7sY9SOQ8FN85bQ0KlS2OUojyADG2wDO0ySqbMO7luR0TrWL2OdWjKY2BCpTnZC44HWvzqbjXlT3tqJz/437eqrf1S17d7ycfv8tX4yuY9e8u9hC6wweMkuFU87qCKJUS2mS6jQTJMcSNUWgSzhE/uB7U6BvBfsEzK2TIUHQFoHXLScpaX1MvEMNZ6LY913kF57OYweyBUlgseXuz6Ws0YynIuv+MO/4TKA8fLrVnJRRYwZskLJ5WASpcYSTrYdl4BfFLo5xlsYcWt+2RdrwGXn/4TKsMrIWfj9YMQKq2v8UbP9OxTtvfGAdlPMpTkMHsjVJb8Mmn+1VioDuClt3zSV5+3bX/J0N/B4rCB5LQiWXKChSqWEEkSwOBjbgWqej8tT8i9MUqEypAyrW5OAmKXX+sXppXV9toZMpMcwf5Y245bHZD9hFBd1He9ESoX/BJsPnbYk7O+1tLSq67Xos7l5x7UfW979vM2v7qPykLImG6mTsbCLLjESJVLiATyi5bZXENR4emUu5NH1HOslZu/i8pYuIPloKQhj5f3Rl87SUpKXY71drP8zbCDMsk58kCoXHA8ZevBX/tip9/4129r71//z5mfsX/6mibHnQfBxasuOfegemxM6Tmh8sXXCJXhNCB3lxhxuoRIIsDH2HbyElpr/cftMNVTj6flDp/+l9JFUQg9JxVy383bYK5P23WFh7yyNIfPjnW3g2M9uxymLN+7hdbKBoZKsz7lwoHltX2e3tn87h/qh5/epN33/LnGHt4x87P3/of0zO2/pfzuHzra7rKrr6r6s7XsOlifE54/zbjKEDLdTZ08bT1viREz1vIBB9vaYcZ4BpXttcPEPsE/x7WqnNtyvYWIyli4mdBk2zLkxyE2vR69pnlQUWylHJ1VJlOyn/E+wWFvUKhUmVaspnHvPnif2P+iXvj8n+r4nkPz/vvU0VN64fN/6qjFsnXZBb466a1HSwZYKgrBDZYpFac3tzV3iRGn3VeDPq26beVjlFLpO7XoydFbyyU6HD79b9SxgL+k6hTSGsk2dIzQSun4PtXlQvmzrZ/0s0RS40JlycBRr9Y2J176269q6uipkq+ZOnpKr33nG678vkWHXvbssYi8+Xqpf+7gsgg0J8FuZokRM26wx8E2tgZwCZH3rqlia67NtZMP8vEgVDq7vmok6aNjAR8xLUO2wyq6fRR0Si6h52LQhn14X6iLsW2ZjIrl9FzT4taXSJkulQ01sf9FvfX4cxW99sjwLl1d7wrp5ERdf1/zm2+Uu5FuMIOeETCFQiEXiUR2OAiGD0QikZSctVLm5bx10w+B0smkRbk6fLSBSCTSiO4jKdMqHiimglmrh26JWoQ/M2SlVuMfvRgqB9KZbEPKvAlYYTQou7WK/fQwotvh8YDdfapT9utdz1uniPfFxtOZrG2ZTPIwoDGhcsEm4pbjRzy7k28/+28Vv/bM/5uo++drO3KgvpXgkycdn2cfeSASiTzAJT6vfjlrbcw5rFT3B3wJkZSD41KPULm+QccjF9DzXMvKcEc6k03UIJjUsgW004PniDLfmH23qcB3BPiaH2FdSkeSlq8fMzMQl9qeVZms0f03dFzr/tr0lne7vp44+HrFrz3+y9eDf9LLL3lCt6YAq2KJESeVgZEgtlpJxRbKSCQy6DCg8zTbf7otX287iYmrAdDh03+bBcTXUyTgJFDXeX3Wel7zOYpDXe5TJUOoCfY7LLeZ4GzUP1QuXKk8dcKzO3ls3ysVv7bcuMuKD+xrr3j3pJdf8oRBy8E3IPtZ0pwI5OQ8ZmzpsMNAuYPxlL5k87At76CSst7lyrbttTdkWyn2UThAjZgKvO24Sr/UMToJlTVne5/MV9iiaDvkpov7Wf1DpS81okurVQV18mQDfucEpT/ETHfUZI1/zfYgLSFiWiYTkUhkWNJOOe/GNUAJDHyoHHb4tNyVhzBm/KdtZS0p+xmJOykWkP3ySH7pDWV7jx+lKFjfp2zveRV9f5pJfFhexK+hstzkLyhRWX2z/l1u6z2OE54MlinZd9GrVF4+XKsrEol0z/lJRiKRARMkj0p6RNV1+wv6Wp1BrvzYVDCHHT5A6HFpHb9+FWc1rNT0Mgi24YBQCSeh0g/XvHXZZoLDmt+nbCf9s62DbGH9XY+EygomfwnXgT3l6+PBmMrwqFXwG/BBF8+uSCRSmP2jYgvk7J8HJN0jd8aP5RX8tTqDyvaeOGoqmTnZrw3rRhlx9PQ/3hcbl11Xxm6KBiTZTsTmh4q77WfMUwxqfp9KmXtUpSE/5eC8JDktdQiV9DW2PLCHfN1yy5jKkDCtZttd3uyY6OI57xcoYyl9y/b7b3huYLOQqGYx7nQmm5Dd0/+xOWOUbFpbOikaqENg8wNaKWt7n3JyL3Xynt5q7r+ESjT+JOSPchDQSEm5+5Q1GfAlRJzYHtRZcKkEn2/2otwmsNmM7YmqurE9ySorXTaV4w4qYBBjCVH7+9R2h8u12H7vRkWPorqESiqJtToJ4xxaNI5pPXOrZXGI8DRvoExwGHzNpvvrmAsVG0eVmnQm2yu7sZ/5eT7beA2PDQiVCDnTSmk7CZKjOooJora9sfi+rnWoZACyncjEKT9/fFJu+Li1xAhP+AiUQWQzpnZ0gevLpjdAh6l41fr6G5hnjFKOUAlLtFajlqFtqMoMkrJ8vdP7b+i1uLWhwuLFHM1Zml96x88fnwcIIVMoFMYjkUhSxdlNqwlQlJ33bC0UCslGBlo1pgUhF6ST6GA+gfP2P94XG09nsoOyW+Q7YVMZMp+zy4XKlm2Z6fTQ6aLMNwYPFlDL+1RV36PxvlgunckOWf7efgdhlFDp1obOvO9ytWofR9SByNv1nzTs1MoPBP2wbvfADWGDpAd9FCxTkUgk4eCGLzGz6WxjkhIeWDokxfIlDakwD5eoGNmEyq50Jts9e3xmBSHU6h453xileF9sNJ3J+jVQpCyOFxpnNID7RGttbQLimEvXdMqybrPe8v4LN0MlnGs+cJyDUIMvrUZXqCORiF9v+DsdvG+AyXmKa2g1uHUSjQ+V81aYTVizfVqeUAWtYGZttS0OrvWF2HzOLopI6LW7cY343HqKQdn71AbVuZVy1v03lc5kk7Iby5kUyyZZsZ39dcH1tgrLV3A054aKyQlPfq6z0dYw3vBRAYdLjIyFPEiNSdoqqZNASagsM/bHtnxUuhi37XaHysykaPUdwILhXCMB3CfroRzMhFyWbW+mucsdVct2W13c22obKhdsiShEL+JoztF25IA3Q+WaSwmVcPPGmwrhMRqTtE3SpkKh0FkoFFhGJbhsWiCGygTOnOwnxOqvoCLrZiulk+8AKl7hZnv+PT/2fp4JrMIarl3hsDeF2+td206YVsm9ElWEygW/aKaWESqdaD52uP6hsr3swzQqx0DRiAkKO1Rsjdws6SoTJPsZsxj4ilC3W9+RVVRSEmVaQGyf/g9VME7IttLfTWkJdViwXR7CL3WMEUKla2zve/Mtd+TGg4JBy7dtoQW6AaHyzIWXcDQdaDl+pP6hMrqi3IXHDJ4Ig6FCoRAp87OhUCh0FwqFXtMaOWjW9UQ4uDKecs79NSW7p+VRSb0LVOjbHYTKlBv7QWUaTh8o+Gjyk9FaH4uQPHhw0ptiwGFrsdvhVmISwsozjeXrS4aNqY2r1LLrIEfViOSPSCu997nOXnF1qX8e48wBgKOwVGlleUDSA5YVofnCYK8JnZWqaIxSvC82bDkDbCdFhVBZaRn00b4NS+ohVFbNSSjrNBPr1ELe8r7Zn85kaxVyQx0qR0uGlZWXSyJUTmvd8zNF3j1a8jVN+aN1/1xluiqPcuYAwFFYqrSXh22o7Ehnsr3xvtjgPGHThs3rR1T5eFJmvgyvXsvX53y0bznL6zS6wHUaWg57U0j2LZu1NN1bJMUZLc2q+2u5bpFnLr2cIzo7se86qMWDPyr507bzxbp+pjOrl5XrqpzjzAGAJLvp7/OVPsk2r7OdZbl/TmUtIbuxbHnZjScataw80gU2fIEhIbsWH8kHk/RU+Vl7KRnn3beiAdiPJKfS5VBpLDi73elV7+eIetyZa8rWQRhPCYAKs31Isr132lZSuuZ8Jtun/7bdt2z3h1AZPgkH7/FNK565XoZsQyUTu1RdRryoI53J8sCgBqFywS+aMxdeojOrl3FUvRwqr1hT7iU5jhIAqKah0qwTaVth7TeBt1v2XU5tp+cftXx9J0UmPEwZtF3IfqzM+qheZBuCo2Jil+kykpD9zMBexnmtQagsGTrOrL2Oo+phZVqTRxiIDAC1D5VG0vL1W8wSDrbv2+7g3m5b+e+myIRK0sF7Uj7cTyctq/20VjouI17WRTf/eofKK6/mqHrUmWsvZjwlANQmVNqGsOmlFWxnw0zKvoUo6fCz2eikyISDaYHqcvBW34VKhz0KQt9aabqKdgRw12itdDNUmqedCy4Ie/LKdTobbQ31QT0bbdXUxlVWP/U4Zqc3bKzqgQEAhIhVpbmKtfdsA5/trIg7quhyaBN4O2idCUVYaJd9V2pJGvJh19dqwvADpldBWAU1fG0J+XktqaWKC+zBhf5x6kPX1H1WU0+FyjWX6tjv/4nVey7854fUVOM1PievuqFcpYhpsAFQcbbv4jRSxa8bNJX0Ws2QOFDFe0dl19qwQTycDLpBh2V1wK87HO+LpcyaiR0OjlXouks6HG/rt8BMi+U8mhy+r+SXxtQNH+HIeszUxlXlur7u4CgBwEw4sg1fTius4zWscA9V0YJa9rveheMGf4WFlMOwMBaAh9ZOrtH16Ux2IIRFJRnw/UvQK8PFUGnWq1ywW8yplR/w/CywizqX68KbO0Jzok+v/VC5l9BKCQDOwlG1SzGlarQf1W7XdnKfTopOYANlv5wvSB+EVp2U7Mc/S9I9ZgxqWMpJp4LdSikVW+oT3BXO11LFewdUogvs6ds+qubHnvbkTi9bu1JXf/5zamu/WHveTOrU6LuBPslno606ee0tpV5iuyg2ABAqXQqV8b7YaDqT3V5FpX0+Y/G+WLWhkrUqMT0xz4MO3z4UhKE18b7YuOkC+4iT+nI6kx02DTJePs/t0/taxWaSTsqIR+75Nt26++XjLt1eDJWDpW4yJ6+7SW3RITXlT3tup6/+/Od0+W//kSTp2nvf0e57/jzQJ3ly001lzyVLiQDADNsn7W5UFgdcDpVJF7YxXOPjBu8HylSV5TIwY8/M2MqEg3IelZRLZ7LdXg2WJlDmzPnKOdxGp4OyMhTvi3V7YP+Tkh6weEtHOpNNuPDgLlCaqri4RlViHF6hbWklYaZuFnUu16o//qQ+/A9f1cTrr878/cTrr2rdti9r1R9/Uos6lwfyJJ9cX/b+x0UBAHI0SY/cmNXSVDbdemLvSu8T87Ax76BiCf9fB+3pTDZXZaDc6vXWuTqG5NnBFlsAAA/YSURBVJlg6cFz3a3iuPD1VW4qWaf31EKqjmWBUOnkJJxc3+WZ5UVOjb6rtfd/XZd8/C5d/YUvzfz91V/4ki7/7T/S2vu/HshusJObrlehbWmpl4xVOZEDAASJbShys+uWW92pBlzsfTJc4+MH74WMXhMyqml5Hon3xZJBOzYmJG+tIlju9NIYS9NCt1NVzj5tWjp7Ld/mmfqneTC43fJt6734kMC3odL0k19w4LKXWiujt64p+5pla1cG6uSejbbq5M23l3tZkssAAGbUe5Keir9TGxBOnewflSz/hsnOdCY7KOnxKkNG3kHA8FOwTKq6h0mPpDPZVCNnEDXnOie7Lp+l9DsoM16rf6Yc7jfcCJWVFAqvtFbmf7xPz/+XuzU5/vZ5/zY5/rZ2/VlCx/ccCtTJndx0U7llRPL0BweAqkLRqMu/v9qK1naXx8gzA2w4wmRK0n5JPS5ssteNLuEe1yvLruFzbJE03IiWLjOT77BcGgNtwrFtuBrzWv3TtJrarjncQ5d/F0OlKRSlWyvv8MbY/SPf/7l+8Rf/47y/H/nyn+itx58L1Ik9G22tZCwlM1cBwLka1lJpDFZZWU26/HlyNT5+aFyQTJjWqv1yb5Koz4ZhSI15cNNd5bXaoWJ32MF6BJN0JtudzmRHVZxkM+riphMOtpfy6Kl1Ui9OCpKqm/117gFdcJrlE+u61Tq8S80vvdPwHc7v+qX+7aYNet/vfkySdPiZ5wN5Yk/FN5cbS5knVALAuRVtB5UjV0OlWbpgQM66pe2oQQuR7fbWU5I8V643SGo3IajT/LcWC3V/Nky9n+J9sWHT6vdIlZvqUbHFa7uklNuh3IzhTKh2szPbtlJ6tv5pZvgdsPwe6E1nsu2souBSqDQnIVnqJnXirrt1wUsPNz5szZmMJ4iT80xtXKWTV64r+yCACwAItIFIJNKoazxVKBT8WLm0bWUbq9F9NOUwVLpeUTNraFqHmAbN+jmQzmQbVeb767jPiQq6TXbWKDgSKM+v/8qFYCkVW4u3pDPZMRV7LAw6DZimfPSan5qVAxNYbbc/4PH6p+1DvagJ1sl6fEeZ3gU1+d6u9hpucfHDJEtdVKdXrNbknR9W25M/reqXHP/l63p2y+9Yv2euwz9YuIXSdvsnDx4+589N+97Whf/8kNU2mva97cpJOBtt1cSn+iqpCNFKCQRbI1uMcj49ZrahcrRGFdVR02ph0yVxqIbdDodk18rRKfe7BXu9zNdz0pWOOgdGAmX9guX0+b1H0j1muyPmXjO8wP11uiW60/zUc8yZkyDl9bLipKdIvUJltIbnt+rvjxaXL6pEqZ09/rHNat43WlU32Kmjp5T/8b6qP++psYVbKKvdflP+tJp2HWzIlXCq51PlJueZLvwAgHN1eyg8pyxDZS0fFI5aVmQ2yIV1MuF5eRUn5cmF/UDUIFjOtt78TE+i9IAX9tm0hto+3Nju9UmczBAE24d60XQmmwj75JctLm+vX9KuUi84cdfdWnrom2rKn3b8SyY3Xa+m8WLL+VTHVZKk5jffUOTkyZk/S1LTqZNqe/KnOtl7W3Fnx/arZddBtaxYpPff+zm1RVdo/MXdmpqY0DvPPK+Vd2xS/qWXdGVvTJP5o8r/8hdqixYfPh5/7VUtu+JKSdL47j1qX7dWJ948pLcef04dX+zRiTcP6UhueGa7r/z9N3V8zyFdtvnmYth78221r1urQ0/t1Mo7Nmns4R266PYbtPp3epT/5c819vCOqg/+5KbrdfLaW8q9bMhMWw8AOD8M2YatWlVsculMttIWwrEa39dt97ObohR4IwrHLK+2wXLUPFCJhmCXk3V6TyMMyH7yqqS83wpbU00uX1DDkraVes3pFat1qudTVf2eqRs+oqmOqzTVcZVO3HinCstX6NRtt+vkJ39PheUrdOLGOzV53Y06/rHNmrzzwzpx452aev9anW0vBsTf+IM7dPmdcS1d3amWxUu09v6v69p7/6vWfO4+Xdkb08W3fkrtH9yo6LX/ofh3n/6Mlr//Gl1331d0yU23adH7LtV1931FH/zSX2rZ2pW67r6v6Iq7Yur8Qt/MdqddcVdMV9wVU/u6tcXX9fXouvu+okWdy3Xj33xHS1d36vLf+l21rFhU1TE5c+3FmvhE2WWh8ioO1gYAzOKFSXrmUWkFpdYVNdv97KREBdq2eF9sA4Fy3npwTsWHKiNB3k/TSmnbDXOHX8qMyTO2a5F2NGKJmMCGyllfbiUXbz557S2avPPDVf2Syetu1OR1N0qSWvf8TKdXrNbpFavVuudnxb87+GtFJid0dtFiSVLzWwdmWjclqa39Ur06mNarj+7Q5Pjbuvy3/0ivfecbM//+8t9t097kw3p3z7N69X89qneef0GSdPgnP1J+1y9ntnHF3Z8+77Pt+/bfL7jm5RV3f0GSdPHHi5/9hc//qZ6NfUFTR085PhZno606cdfd5WZ7lYqT8/AlAADns14Ko9YTs5Rbssuox3rDtt8bHRSnQBqTtCneF2MITfn7Qrek7QHeTSdlwG9zeTi5ryYJle5eTOOqoDXs+Mc26/RH17jyO1t2HVTzscNqPnZYLWYs48lrb1HL8SNqfaH4nX/mstUzLZWvP/aU3vjXb2vt/V/XRd0b9MaTaUnSa5kdeuXvv6l39zyrG//mO1q2duV5v+uSm25Ty4XvhbfL74xrcrw4yc7oNzJ6d8+z2vjVlC66/YZ5P2tb+6XF0LvsAkmqKkzOhPTP/EedXrG63MuGmJwHAFwLlfVqiRio8t/dqiRbCfsT+4DJS9oa74t1Mn6y8rpwvC+WkLRZ1a1l6TmmV0eP5duG/FZ2KnyoN1eXWcKHUOniichJ2lo2WN4e15lrL3b0O9r2Pq+2ve/N4Npy/Ihajh+Z+fOS55/UBX/1sJoPHJdUbKmcdlH3Bk28/mrxde9bqRMHi7PDnhp9V5fcdrOOHxgtbvPC81v+Dv/kRzN/f/iZJ9TWfqne+fH3Zrab37tHkrT4fe87/73PPKGJ/S8W//9Hz0mS1m37stZt+7Iuuv0GdXyxx/o4nPjM7+rUyg9U8oXQKwCAW6GyXrObpkpUSuu53ttIjY8nPBomJXXG+2JJDoej+vCgit3Bt/ng426r8L7mpCykfHoKnXzu0LbkN9XwQkqqTH/kQttSHYv/Z+tg2fTWQTXlj6opf1SLDr383t+9VWylXHToZTXlj868ftGhl1WIXqSzl62aCZJX3P0FHX7mCb3+2FM68eYhHX7mieK/rfoNXX5nXG/867eV//E+5ffu0Yk3D2nq2IQOP/PEzJjKw888odeeyOrwM0/oUO5p5ffuOWe7b/3LD4t35L17ZraR37tHr3zrIR1+5gkd33NIr3zjL3TxrcXxpRdceaValy+3DpQVTMwjSQnWpAQAV0PQaJ0qpeMlKjaDdby32+5vJ0XKt8Yk3TsdJqk/VH8Nmy7DV0na4cGPuF3SVfG+WH+5c21aKW0nsBnz8ayoTh7abTHHKXRaarz9XvNFFC0XLC9M/13FS40sfuzpmf9v2/nieX+39GuPnvP6uX8ee3jHObOtvvX4c3rr8WLL4d77H9Le+99bY/LXX/3WzP//9D/92TnvOee/em5m27PNfv/0aw5+699m/m32v9coUG5ltlcAKFlRapf9OMBcnSs298zz98k6foZh2XV5o6XSX0ZUnLV0sNZjhUMcLkcl9ZrAkXQQztw03cshZTnXhpNWuKSPz5mT5UWk4jDAZNjKeFOtT4aKg5VL9id32mIZVhaBcgddVgCgJgFotF4fzlT65k76Ue/13kbrcExRH0OmPG2VtEnSCjOba5JAWZ/r2Yy3XKFii3A9Z4rdIWlzvC/Wbs53xde1efiWsA2vAVi70UlrZb85XqHSUoeLZzidyfarzIKwTlosCZQljShYy4fkavz6WlXCtvrsM4fpc4/67BpIeehY+6VM2LApO2rATNopnfu0PNWAc7qVMl+T+0muDtfheEACo22Z8Ox93jS8DEgaMK2XvSo2xHTLvXUuR8zxyrnQa63dQcDyfZkzOeZes/+2x2u8gvK51SO7WvV9KFIoFOrySdOZbKJcsJSkyOSElv5gcKZbK4rORlt1Kr5ZJ69cV8nLxyRtYBwEAARHOpPNqbg23FC8L9bNEQECe613qjguuduEk9kt/11z6nujc4LBuKRhZupFvdUtVJqLJKUK+yUvef5JLR78EWdI0pnVy3Qi/seVLBsiFbsad9OFBQACV9FMqPhwdjNj5QEAoQ2VtsFy8au7tSj9uJryp0N7gk5/dI2O3x5XoW0pgRIACJY5WikBAKEPlbbBsvnYYS39XkYtuw6G6sScjbZq8o4unVhXcd2BQAkAAAAgHKHSBMuFpkif15LdObU9NRSKVsupjat08pO/V2l3VwIlAAAAgPCFShMsE6pg8p5pQW+1dNA6KRVn9upmUh4AAAAAoQuVJlj2qjhFdMXTJy869LIWPZ5V84HjgTkRk5uu18QneisdOzltSFIvgRIAAABAaEOlCZYbJA1K6rB53+KXnlXb/93p63A5uel6nbz5dp258BLbt26L98X6KcIAAAAAQh8qTbBsV7HFssf2vX4Ml1WEybykBNPJAwAAACBUzh8u+yUlZdEddtqiQy+r5ecvqG3ni5482GdWL9Ppj2zQyfVdtt1cp42o2N11lKILAAAAgFC5cLDcoGKr5XpHOzU5oUX7d6t1z88aPqnP2Wirpj50jaZu+IhOrfxANZvaGu+LJSmyAAAAAAiVlYdLx62WcwNm82v71Pyrsbp0kZ3auEpTHVdp6v1rbZYFWciQpH6WCwEAAABAqHQWLDtNsNzixvaajx1W69GDirz5ulrG9ivydr6qoDm1cZXOtrfr7GWrdPayVdW2Rs6WN2EyRTEFAAAAQKisPlx2m3DZVYvtNx87rJbjR2b+3PTaK+f8e2H5ChWiF8382cXwOF+YHJA0wFIhAAAAAAiVPguXDUSYBAAAAECorHO47JeDJUgIkwAAAAAQ8lA5K1x2mnCZUBUT+jTAkKQUYyYBAAAAECq9EzB7JU3/eDFgjqi4VMoga00CAAAAIFR6P2B2m5/1DfoYeUk580OQBAAAAECo9GnAbDfhcsOs/9aiJXNE0vD0T7wvlqOIAQAAACBUBjdobpDUaX5kAmclRs2PVGyFHI/3xYYpTgAAAAAIlQAAAAAAVKiJQwAAAAAAIFQCAAAAAAiVAAAAAABCJQAAAACAUAkAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAABCJQAAAACAUAkAAAAAIFQCAAAAAAiVAAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAIFQCAAAAAAiVAAAAAABCJQAAAAAg0P4//U9DqP/Pc9YAAAAASUVORK5CYII='
                            });
                            doc['footer'] = (function (page, pages) {
                                return {
                                    columns: [
                                        {
                                            alignment: 'left',
                                            text: [
                                                {text: 'Printed on:' + moment().format(sk.getMomentDatetimeFormat())}
                                            ]
                                        },
                                        {
                                            alignment: 'right',
                                            text: [
                                                'Page ',
                                                {text: page.toString(), italics: true},
                                                ' of ',
                                                {text: pages.toString(), italics: true}
                                            ]
                                        }
                                    ],
                                    margin: [10, 0]
                                }
                            });

                        }
                    });
                    break;
                case 'share':
                    buttons.push({
                        "text": '<i class="clip-share"></i> Share',
                        "className": 'btn btn-primary btn-squared'
                    });
                    break;
            }
        }

        if (filterFirst == false) {
            var coldef = [{"targets": 0, "orderable": false}];
        } else {
            var coldef = {};
        }
        var sort = '1';
        for(var j in colHeader){
            if(colHeader[j]['title'] == '# Items'){
                sort = j;
                break;
            }
        }
        console.log('colHeader',colHeader);
        var table = $("#" + tableId).DataTable({
            "paging": true,
            "lengthMenu": [10,12, 25, 50, 100, 200, 250, 500],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "order": [[sort, "desc"]],
            "info": true,
            "autoWidth": false,
            "responsive": false,
            "select": select,
            "keys": true,
            "dom": 'Bfrtip',
            "buttons": buttons,
            "columnDefs": coldef,
            "columns": colHeader,
            "data": data,
            "oLanguage": {
                "sSearch": "",
                "sLengthMenu": "Per page _MENU_",
                "oPaginate": {
                    "sNext": "<i class='clip-chevron-right'></i>",
                    "sPrevious": "<i class='clip-chevron-left'></i>"
                },
                "sInfo": "_START_ to _END_ rows out of _TOTAL_",
            },
            "sDom": '<"wrapper"<"row"<"col-sm-12" <"col-sm-8 actionButtonsDiv' + tableId + '"> <"col-sm-4 printBtnDiv" Bl > <"col-sm-12" <"sk-progress" <"sk-blue indeterminate skl' + tableId + '">><"row well customWell" <"col-sm-4 customSearchInput' + tableId + '" f> <"col-sm-1 customSearchField' + tableId + '">  <"col-sm-7 CustomPagination' + tableId + '"ip> > > >  >rt<"clear">>',
//            "sDom": '<"wrapper"<"row"<"col-sm-12" <"col-sm-8 actionButtonsDiv"> <"col-sm-4 printBtnDiv" l > <"col-sm-12" <"row well customWell" <"col-sm-4 customSearchInput" f> <"col-sm-8 CustomPagination"ip> > > >  >rt<"clear">>',
            initComplete: function () {

                //$("#patient_service_history_results_table_length").append('<button type="button" id="printVacationTable1"  class="btn btn-danger btn-squared"><i class="fa fa-print"></i> Print</button>');
                $(".CustomPagination" + tableId).prepend('<div style="float:left"><button data-toggle="tooltip" title="Filters" type="button" class="btn btn-default filterToggle' + tableId + '"><i class="fa fa-filter"></i></button></div>');

                $("#" + tableId).find(".entireClickable").click(function () {
                    if ($(this).find('input').prop('checked')) {
                        $(this).find('input').prop('checked', false);
                        $(this).removeClass('highlight');
                    } else {
                        $(this).find('input').prop('checked', true);
                        $(this).addClass('highlight');
                    }
                })

                var inputHtml = '<div class="input-group">' +
                        '<input type="text" placeholder="Contains..." data-focus="true" class="form-control DatatableAllRounderSearch' + tableId + '" />' +
                        '<span class="input-group-addon cursorPointer btnClearSearchTxt' + tableId + '"> ' +
                        '<i class="clip-cancel-circle-2 "></i>' +
                        '</span>' +
                        '<span class="input-group-addon cursorPointer"> ' +
                        '<i class="clip-search-2"></i>' +
                        '</span>' +
                        '</div>';

                $(".customSearchInput" + tableId).html(inputHtml);

                global_js.triggerFocusField();

                var searchoptions = $("#" + tableId + " thead tr:eq(0) th");
                var customfilterinputs = '<tr>';
                for (var j = 0; j < searchoptions.length; j++) {
                    customfilterinputs += '<th></th>';
                }
                customfilterinputs += '</tr>';
                $("#" + tableId + " thead").append(customfilterinputs);
                var aa = 0;
                this.api().columns().every(function () {
                    var column = this;
                    var columnText = $.trim($(column.header())[0].innerText);

                    if ($(column.header())[0].cellIndex != 0 || filterFirst == true) {

                        if (($.trim($(column.header())[0].innerText) == 'Description' && tableId == 'explCodeTable')) {
                            $('<input type="text" placeholder="Search" class="form-control btn-squared" />')
                                    .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                    .on('keyup change', function () {
                                        if (column.search() !== this.value) {
                                            column
                                                    .search(this.value)
                                                    .draw();
                                        }
                                    });
                        } else {
                            var select = $('<select class="selectRsltTbl' + tableId + '"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                    .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                    .on('change', function () {
                                        var val = //$.fn.dataTable.util.escapeRegex(
                                                $(this).val().trim();
                                        //);
                                        column.search(val, true, false).draw();
                                    });

                            column.data().unique().sort().each(function (d, j) {
                                if (columnText == 'Permissions' && tableId == 'user_type_table') {
                                    var arr = d.trim();
                                    arr = arr.split(',');
                                    for (var i in arr) {
                                        select.append('<option value="' + arr[i] + '">' + arr[i] + '</option>');
                                    }
                                } else {
                                   if(d != '') {
                                   select.append('<option value="' + d + '">' + d + '</option>');
                                   }
                                }

                            });
                        }

                    }
                    aa++;
                });


                $(".filterToggle" + tableId).click(function () {
                    $("#" + tableId + " thead tr:eq(1)").toggle();
                });
                //$(".filterToggle" + tableId).trigger("click");
                $(".selectRsltTbl" + tableId).select2({
                    placeholder: "Search",
                    allowClear: true,
                    dropdownAutoWidth: true,
//                    maximumInputLength: 20,
                    width: '98%'
                });
                $(".custActionBtns" + tableId).detach().appendTo(".actionButtonsDiv" + tableId);
                $(".actionButtonsDiv" + tableId).attr('style', 'padding-left:0px');
            }
        });

        $(".DatatableAllRounderSearch" + tableId).keyup(function () {
            table.search($(this).val(), true).draw();
        });

        $(".btnClearSearchTxt" + tableId).click(function () {
            $(".DatatableAllRounderSearch" + tableId).val('');
            table.search('', true).draw();
        });
        setTimeout(function () {
            $(".dataTables_length select").select2('destroy');
            $("#" + tableId).find('.select2-arrow').hide();
            $("#" + tableId + " thead tr:eq(1)").toggle();
        }, 200);
        $("#tabs").on("tabsactivate", function (event, ui) {
            if ($("#" + tableId + " thead tr:eq(1)").is(":visible")) {
                $("#" + tableId + " thead tr:eq(1)").toggle();
            }
        });

        $('div.dataTables_filter input').addClass('form-control');
        $('div.dataTables_filter input').attr('placeholder', 'Search');
        $(".DTsearchlabel").html('<i class="clip-search"></i>');
        $('.dataTables_filter').attr('style', 'width:100%');
        $('.dataTables_filter label').attr('style', 'width:100%');
        $('.dataTables_length select').attr('style', 'font-size: 12px;padding: 5px 3px;background: #fff;');
        $('div.dataTables_filter input').attr('style', 'float:right;width:100%');
        $(".dataTables_wrapper .dataTables_paginate .paginate_button").attr('style', 'background:#fff !important;color:#444444 !important; border-radius: 0;');

        table.on('responsive-display', function (e, datatable, row, showHide, update) {
            $('td ul').attr('style', 'width:100% !important');
            $('td ul').addClass('row');
            $('td ul li').addClass('col-sm-4');
            $(".dropdown-menu li").removeClass('col-sm-4');
        });

        return table;
    },

    sendSearchAjax: function () {
        if (quickClaim.ajaxLocked) {
            return;
        }

        var data = $('#totalsForm').serialize() + '&' + $('#totalsDialogForm').serialize();

        $.ajax({
            type: 'post',
            dataType: 'json',
            data: data,
            url: totalsReport.actions.search,
            success: function (rs) {
                var data = rs.data;
                
                
                 $(".actionButtonsDivresultsTable").html('<i>' + rs.description + '</i>').addClass('text-info');
                totalsReport.resultTableData.data = rs.data;
                totalsReport.resultTableData.headers = rs.headers;
                totalsReport.newInitTable(rs);
            },
            beforeSend: function () {
                global_js.dtLoader(totalsReport.tableId, 'start');
                $('#visualization').empty();
                quickClaim.ajaxRunning = true;
                $('#messages .icons div').hide();
                $('#' + totalsReport.messages.spinner).show();
                $('#' + totalsReport.messages.text).text('Searching').show();
            },
            complete: function () {
                quickClaim.ajaxRunning = false;
                $('#' + totalsReport.messages.spinner).hide();
            },
            error: function () {
                $('#' + totalsReport.messages.error).show();
                $('#' + totalsReport.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });
    }
}