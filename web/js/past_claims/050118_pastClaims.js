var pastClaims = {
    actions: {
        claimPDF: null,
        details: null,
        edit: null,
        profile: null,
        refdocAutocomplete: null,
        search: null,
        prevSearch: null,
        changeItems: null
    },
    data: {},
    errors: {},
    explCodes: {},
    messages: {
        checkmark: 'checkmark',
        error: 'error_icon',
        spinner: 'spinner',
        text: 'messages_text'
    },
    resultsTable: 'past_claims_results_table',
    selectedIDs: new Array(),

    csv: {
        initialize: function () {
            $('#csvOptionsDialog').dialog({
                closeOnEscape: true,
                autoOpen: false,
                height: $(window).height() - 200,
                modal: true,
                width: $(window).width() - 200
            });

            $('#downloadCSV').button();
            $('#downloadCSV').click(function () {
                pastClaims.csv.handleCsvClick();
            });
        },

        downloadComplete: function () {
            if ($('#hiddenDownloader').contents().find('body').text()) {
                $('#messages .icons div').hide();
                $('#' + pastClaims.messages.error).show();
                $('#' + pastClaims.messages.text).html('<div class="alert alert-info">An error occurred while generating the CSV File</div>').show();
            } else {
                $('#messages .icons div').hide();
                $('#' + pastClaims.messages.checkmark).show();
                $('#' + pastClaims.messages.text).html('<div class="alert alert-info">Generated the CSV File</div>').show();
            }
            $('#csvOptionsDialog').dialog('close');
            quickClaim.hideOverlayMessage();
        },
        handleCsvClick: function () {
            hypeFileDownload.tokenName = "pastClaimsCSVToken";
            hypeFileDownload.setReturnFunction(pastClaims.csv.downloadComplete);

            var url = pastClaims.actions.search + '?'
                    + $('#' + pastClaims.form.formID).serialize()
                    + '&' + $('#csvForm').serialize()
                    + '&footerText=' + $('#CSVFooterText').text()
                    + '&mode=csv'
                    + '&pastClaimsCSVToken=' + hypeFileDownload.blockResubmit();

            quickClaim.showOverlayMessage('Generating CSV');
            $('#hiddenDownloader').remove();
            $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
        },
        openDialog: function () {
            $('#CSVFooterText').text($('#results-table .pastClaimsPager .pagedisplay').text());
            $('#csvOptionsDialog').dialog('open');
        }
    },
    form: {
        errorDiv: 'past_claims_errors',
        formID: '',
        prefix: '',
        refDocCache: {},

        initialize: function () {
            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_service_date_start');
            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_service_date_end');

            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_dob_start');
            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_dob_end');

            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_creation_date_start');
            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_creation_date_end');

            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_paid_date_start');
            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_paid_date_end');

            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_batch_date_start');
            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_batch_date_end');

            pastClaims.form.setupRefDocAutocomplete('referring_doctor');

            $('#' + pastClaims.form.prefix + '_patient_id').keyup(function () {
                pastClaims.form.togglePatientDetails();
            });
            $('#' + pastClaims.form.prefix + '_claim_id').keyup(function () {
                pastClaims.form.toggleClaimDetails();
            });
            $('#' + pastClaims.form.prefix + '_moh_claim').keyup(function () {
                pastClaims.form.toggleMohDetails();
            });

            $('#clear_button').button();
            $('#clear_button').click(function () {
                pastClaims.form.clearForm();
            });

            $('#find_button').button();
            $('#find_button').click(function () {
                return pastClaims.handleFindClick();
            });

            $('#pdfButton').button();
            $('#pdfButton').button('disable');
            $('#pdfButton').click(function () {
                return pastClaims.pdf.openDialog();
            });

            $('#csvButton').button();
            $('#csvButton').button('disable');
            $('#csvButton').click(function () {
                return pastClaims.csv.openDialog();
            });



            $('#printButton').button();
            $('#printButton').click(function () {
                $('#past_claims_results_table_form').prepend($('#past_claims_totals'));

                $("#past_claims_results_table_form").printElement({
                    pageTitle: 'billingCyclePrintOut' + new Date().getTime() + '.pdf',
                    overrideElementCSS: ['css/jquery-tablesorter1.css', 'css/jquery-tablesorter/jquery.tablesorter.pager.css', 'css/jquery-tablesorter/theme.blue.css']
                });
            });

            $('#' + pastClaims.form.formID + ' div.claim_type input').click(function () {
                pastClaims.form.toggleAllCheckbox('claim_type', $(this).val());
            });

            $('#' + pastClaims.form.formID + ' div.claim_status input').click(function () {
                pastClaims.form.toggleAllCheckbox('claim_status', $(this).val());
            });

            $('#' + pastClaims.form.formID + ' div.payment_status input').click(function () {
                pastClaims.form.toggleAllCheckbox('payment_status', $(this).val());
            });

            $('#search input[type=button]').button();
            $('#unsubmitted_search').click(function () {
                pastClaims.form.quickSearchClick('unsubmitted');
            });
            $('#submitted_search').click(function () {
                pastClaims.form.quickSearchClick('submitted');
            });
            $('#rejected_search').click(function () {
                pastClaims.form.quickSearchClick('rejected');
            });
            $('#todays_claim_search').click(function () {
                pastClaims.form.quickSearchClick('today');
            });
            $('#todays_billed_claim_search').click(function () {
                pastClaims.form.quickSearchClick('today_billed');
            });
            $('#unresolved_search').click(function () {
                pastClaims.form.quickSearchClick('unresolved');
            });
            $('#resolved_search').click(function () {
                pastClaims.form.quickSearchClick('resolved');
            });

            //Quick columns
            $('#admit_search').click(function () {
                pastClaims.form.applyChanges('admit_date');
            });

            $('#fee_search').click(function () {
                pastClaims.form.applyChanges('fee');
            });

            $('#errors_search').click(function () {
                pastClaims.form.applyChanges('errors');
            });

            $('#basic_md').click(function () {
                pastClaims.form.applyChanges('basic_md');
            });

            $('#my_billing').click(function () {
                pastClaims.form.applyChanges('my_billing');
            });

            $('#billing').click(function () {
                pastClaims.form.applyChanges('billing');
            });

            $('#basic_track').click(function () {
                pastClaims.form.applyChanges('basic_track');
            });


            $('#previous_search').click(function () {
                $.ajax({
                    type: 'json',
                    url: pastClaims.actions.prevSearch,
                    method: 'GET',
                    success: function (rs) {
                        alert(rs);
                    }
                });
            });

            pastClaims.form.toggleAllCheckbox('claim_status');
            pastClaims.form.toggleAllCheckbox('claim_type');
            pastClaims.form.toggleAllCheckbox('payment_status');
        },

        clearErrors: function () {
            $('label.error').remove();
            $('.error').removeClass('error');
            $('#' + pastClaims.form.errorDiv).text('').hide();
        },
        clearForm: function () {
            $('#' + pastClaims.form.formID + ' input[type=text]').val('');
            $('#' + pastClaims.form.formID + ' select').val('');

            pastClaims.form.toggleAllCheckbox('claim_type', 'all');
            pastClaims.form.toggleAllCheckbox('claim_status', 'all');
            pastClaims.form.toggleAllCheckbox('payment_status', 'all');
            pastClaims.form.togglePatientDetails();
            pastClaims.form.toggleClaimDetails();
            pastClaims.form.toggleMohDetails();
        },
        applyChanges: function (value) {
            if (value == 'errors') {
                $('.reportColoumnsChkbox').prop('checked', false);

                $('input[value="doctor_code"]').prop('checked', true);
                $('input[value="charge_to"]').prop('checked', true);
                $('input[value="patient_number"]').prop('checked', true);
                $('input[value="patient_name"]').prop('checked', true);
                $('input[value="health_card_vc"]').prop('checked', true);
                $('input[value="dob"]').prop('checked', true);
                $('input[value="sex"]').prop('checked', true);
                $('input[value="service_code"]').prop('checked', true);
                $('input[value="diag_code"]').prop('checked', true);
                $('input[value="num_serv"]').prop('checked', true);
                $('input[value="fee_subm"]').prop('checked', true);
                $('input[value="fee_paid"]').prop('checked', true);
                $('input[value="service_date"]').prop('checked', true);
                $('input[value="errors"]').prop('checked', true);
            } else if (value == 'fee') {
                $('.reportColoumnsChkbox').prop('checked', false);

                $('input[value="doctor_code"]').prop('checked', true);
                $('input[value="charge_to"]').prop('checked', true);
                $('input[value="patient_number"]').prop('checked', true);
                $('input[value="patient_name"]').prop('checked', true);
                $('input[value="health_card_vc"]').prop('checked', true);
                $('input[value="dob"]').prop('checked', true);
                $('input[value="sex"]').prop('checked', true);
                $('input[value="service_code"]').prop('checked', true);
                $('input[value="diag_code"]').prop('checked', true);
                $('input[value="num_serv"]').prop('checked', true);
                $('input[value="fee_subm"]').prop('checked', true);
                $('input[value="service_date"]').prop('checked', true);
            } else if (value == 'admit_date') {
                $('.reportColoumnsChkbox').prop('checked', false);

                $('input[value="doctor_code"]').prop('checked', true);
                $('input[value="charge_to"]').prop('checked', true);
                $('input[value="patient_number"]').prop('checked', true);
                $('input[value="patient_name"]').prop('checked', true);
                $('input[value="health_card_vc"]').prop('checked', true);
                $('input[value="dob"]').prop('checked', true);
                $('input[value="sex"]').prop('checked', true);
                $('input[value="admit_date"]').prop('checked', true);
                $('input[value="facility_num"]').prop('checked', true);
                $('input[value="service_code"]').prop('checked', true);
                $('input[value="diag_code"]').prop('checked', true);
                $('input[value="num_serv"]').prop('checked', true);
                $('input[value="fee_subm"]').prop('checked', true);
                $('input[value="service_date"]').prop('checked', true);
                $('input[value="errors"]').prop('checked', true);
            } else if (value == 'basic_md') {
                $('.reportColoumnsChkbox').prop('checked', false);

                $('input[value="provider_number"]').prop('checked', true);
                $('input[value="group_num"]').prop('checked', true);
                $('input[value="account_num"]').prop('checked', true);
                $('input[value="charge_to"]').prop('checked', true);
                $('input[value="patient_name"]').prop('checked', true);
                $('input[value="health_card_vc"]').prop('checked', true);
                $('input[value="admit_date"]').prop('checked', true);
                $('input[value="claim_status"]').prop('checked', true);
                $('input[value="service_code"]').prop('checked', true);
                $('input[value="diag_code"]').prop('checked', true);
                $('input[value="num_serv"]').prop('checked', true);
                $('input[value="fee_subm"]').prop('checked', true);
                $('input[value="service_date"]').prop('checked', true);
                $('input[value="fee_paid"]').prop('checked', true);

            } else if (value == 'my_billing') {
                $('.reportColoumnsChkbox').prop('checked', false);

                $('input[value="doctor_code"]').prop('checked', true);
                $('input[value="account_num"]').prop('checked', true);
                $('input[value="charge_to"]').prop('checked', true);
                $('input[value="health_card_vc"]').prop('checked', true);
                $('input[value="patient_name"]').prop('checked', true);
                $('input[value="service_code"]').prop('checked', true);
                $('input[value="diag_code"]').prop('checked', true);
                $('input[value="num_serv"]').prop('checked', true);
                $('input[value="fee_subm"]').prop('checked', true);
                $('input[value="fee_paid"]').prop('checked', true);
                $('input[value="service_date"]').prop('checked', true);
                $('input[value="errors"]').prop('checked', true);

            } else if (value == 'billing') {
                $('.reportColoumnsChkbox').prop('checked', false);

                $('input[value="doctor_code"]').prop('checked', true);
                $('input[value="account_num"]').prop('checked', true);
                $('input[value="moh_claim"]').prop('checked', true);
                $('input[value="charge_to"]').prop('checked', true);
                $('input[value="health_card_vc"]').prop('checked', true);
                $('input[value="admit_date"]').prop('checked', true);
                $('input[value="claim_status_code"]').prop('checked', true);

                $('input[value="service_code"]').prop('checked', true);
                $('input[value="diag_code"]').prop('checked', true);
                $('input[value="num_serv"]').prop('checked', true);
                $('input[value="fee_subm"]').prop('checked', true);
                $('input[value="service_date"]').prop('checked', true);
                $('input[value="fee_paid"]').prop('checked', true);
                $('input[value="errors"]').prop('checked', true);
            } else if (value == 'basic_track') {
                $('.reportColoumnsChkbox').prop('checked', false);


                $('input[value="account_num"]').prop('checked', true);
                $('input[value="moh_claim"]').prop('checked', true);
                $('input[value="charge_to"]').prop('checked', true);
                $('input[value="batch_num"]').prop('checked', true);
                $('input[value="health_card_vc"]').prop('checked', true);


                $('input[value="claim_status"]').prop('checked', true);
                $('input[value="service_code"]').prop('checked', true);
                $('input[value="diag_code"]').prop('checked', true);

                $('input[value="created_user_id"]').prop('checked', true);
                $('input[value="updated_user_id"]').prop('checked', true);

                $('input[value="reconcile"]').prop('checked', true);
                $('input[value="error_reconcile"]').prop('checked', true);


                $('input[value="errors"]').prop('checked', true);
            }

        },
        quickSearchClick: function (value) {
            if (value == 'today') {
                var today = new Date();
                today = today.toString(quickClaim.getParseDateFormat(quickClaim.dateFormat));

                $('#past_claims_service_date_start').val(today);
                $('#past_claims_service_date_end').val(today);
                $('#past_claims_creation_date_start').val('');
                $('#past_claims_creation_date_end').val('');

                $('.claim_type input:checked').prop('checked', false);
                $('.claim_status input:checked').prop('checked', false);
                pastClaims.form.toggleAllCheckbox('claim_type');
                pastClaims.form.toggleAllCheckbox('claim_status');
            } else if (value == 'today_billed') {
                var today = new Date();
                today = today.toString(quickClaim.getParseDateFormat(quickClaim.dateFormat));

                $('#past_claims_service_date_start').val('');
                $('#past_claims_service_date_end').val('');
                $('#past_claims_creation_date_start').val(today);
                $('#past_claims_creation_date_end').val(today);

                $('.claim_type input:checked').prop('checked', false);
                $('.claim_status input:checked').prop('checked', false);
                pastClaims.form.toggleAllCheckbox('claim_type');
                pastClaims.form.toggleAllCheckbox('claim_status');
            } else if (value == 'resolved') {
                $('.claim_status input').prop('checked', false);
                $('.payment_status input').prop('checked', false);

                $('#' + pastClaims.form.prefix + '_payment_status_partial_paid').prop('checked', true);
                $('#' + pastClaims.form.prefix + '_payment_status_fully_paid').prop('checked', true);

                $('#' + pastClaims.form.prefix + '_claim_status_rejected_closed').prop('checked', true);
                $('#' + pastClaims.form.prefix + '_claim_status_paid').prop('checked', true);
                $('#' + pastClaims.form.prefix + '_claim_status_paid_by_user').prop('checked', true);
            } else if (value == 'unresolved') {
                $('.claim_status input').prop('checked', false);
                $('.payment_status input').prop('checked', false);

                $('#' + pastClaims.form.prefix + '_payment_status_unpaid').prop('checked', true);
                $('#' + pastClaims.form.prefix + '_payment_status_partial_paid').prop('checked', true);

                $('#' + pastClaims.form.prefix + '_claim_status_unsubmitted').prop('checked', true);
                $('#' + pastClaims.form.prefix + '_claim_status_submitted').prop('checked', true);
                $('#' + pastClaims.form.prefix + '_claim_status_rejected').prop('checked', true);
            } else {
                $('.claim_status input').prop('checked', false);
                $('#' + pastClaims.form.prefix + '_claim_status_' + value).prop('checked', true);
            }
            $('#find_button').click();
        },
        setupDatePicker: function (id) {
            var ssDateFormat = 'yy-m-d';
            $('#' + id).datepicker({
                dateFormat: ssDateFormat,
                changeMonth: true,
                renderer: $.datepick.themeRollerRenderer
            });
        },
        setupRefDocAutocomplete: function (prefix) {
            var doctor = $('#' + pastClaims.form.prefix + '_' + prefix);
            var description = $('#' + pastClaims.form.prefix + '_' + prefix + '_description');

            if (doctor.size() && description.size()) {
                description.autocomplete({
                    minLength: 2,
                    select: function (event, ui) {
                        doctor.val(ui.item.provider_num);
                        description.val(ui.item.label);
                    },
                    source: function (request, response) {
                        if (request.term in pastClaims.form.refDocCache) {
                            return response(pastClaims.form.refDocCache[request.term]);
                        }

                        $.ajax({
                            url: pastClaims.actions.refdocAutocomplete,
                            dataType: 'json',
                            data: request,
                            success: function (data) {
                                pastClaims.form.refDocCache[request.term] = data;
                                return response(data);
                            }
                        });
                    }
                });
            }
        },
        toggleAllCheckbox: function (type, value) {
            //console.log('test');
            var count = $('#' + pastClaims.form.formID + ' div.' + type + ' input:checked').size();
            if (value == 'all') {
                $('#' + pastClaims.form.formID + ' div.' + type + ' input:checked').each(function () {
                    if ($(this).val() != 'all') {
                        $(this).prop('checked', false);
                    } else {
                        $(this).prop('checked', (count > 0));
                    }
                });
            } else if (count == 0) {
                $('#' + pastClaims.form.formID + ' div.' + type + ' input[value=all]').prop('checked', true);
            } else {
                $('#' + pastClaims.form.formID + ' div.' + type + ' input[value=all]').prop('checked', false);
            }

            var count = $('#' + pastClaims.form.formID + ' div.' + type + ' input:checked').size();
            if (count == 0) {
                $('#' + pastClaims.form.formID + ' div.' + type + ' input[value=all]').prop('checked', true);
            }

            if (type == 'claim_type') {
                pastClaims.form.toggleThirdPartyDirectRow();
            }
        },
        toggleClaimDetails: function () {
            var value = jQuery.trim($('#' + pastClaims.form.prefix + '_claim_id').val());
            var disabled = value ? true : false;

            $('#' + pastClaims.form.prefix + '_claim_location_id').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_account_number').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_service_date_start').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_service_date_end').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_creation_date_start').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_creation_date_end').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_service_code').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_diagnostic_code').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_doctor_id').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_group_number').prop('disabled', disabled);
        },
        toggleMohDetails: function () {
            var value = jQuery.trim($('#' + pastClaims.form.prefix + '_moh_claim').val());
            var disabled = value ? true : false;

            $('#' + pastClaims.form.prefix + '_expl_code').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_ra').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_batch_id').prop('disabled', disabled);
        },
        togglePatientDetails: function () {
            var value = jQuery.trim($('#' + pastClaims.form.prefix + '_patient_id').val());
            var disabled = value ? true : false;

            $('#' + pastClaims.form.prefix + '_fname').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_lname').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_patient_location_id').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_sex').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_dob_start').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_dob_end').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_health_num').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_referring_doctor_description').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_province').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_region_id').prop('disabled', disabled);
        },
        toggleThirdPartyDirectRow: function () {
            var direct_checked = $('#' + pastClaims.form.formID + ' div.claim_type input[value=direct]:checked').size();
            var third_party_checked = $('#' + pastClaims.form.formID + ' div.claim_type input[value=third_party]:checked').size();
            var all_checked = $('#' + pastClaims.form.formID + ' div.claim_type input[value=all]:checked').size();

            if (direct_checked || all_checked || third_party_checked) {
                $('.third_party_direct_row').show();
                if (direct_checked || all_checked) {
                    $('.third_party_direct_row .direct').show();
                } else {
                    $('.third_party_direct_row .direct').hide();
                }

                if (third_party_checked || all_checked) {
                    $('.third_party_direct_row .third_party').show();
                } else {
                    $('.third_party_direct_row .third_party').hide();
                }
            } else {
                $('.third_party_direct_row').hide();
            }
        }
    },
    pdf: {
        initialize: function () {
            $('#pdfOptionsDialog').dialog({
                closeOnEscape: true,
                autoOpen: false,
                height: $(window).height() - 200,
                modal: true,
                width: $(window).width() - 200
            });

            $('#downloadPDF').button();
            $('#downloadPDF').click(function () {
                pastClaims.pdf.handlePdfClick();
            });

            $('#downloadMPDF').button();
            $('#downloadMPDF').click(function () {


                $('#new_window_parameter_1').val($('body').html());
                $('#invisible_form').submit();
                document.forms.invisible_form_pdf.submit();


            });
        },

        downloadComplete: function () {
            if ($('#hiddenDownloader').contents().find('body').text()) {
                $('#messages .icons div').hide();
                $('#' + pastClaims.messages.error).show();
                $('#' + pastClaims.messages.text).html('<div class="alert alert-info">An error occurred while generating the PDF File</div>').show();
            } else {
                $('#messages .icons div').hide();
                $('#' + pastClaims.messages.checkmark).show();
                $('#' + pastClaims.messages.text).html('<div class="alert alert-info">Generated the PDF File</div>').show();
            }
            $('#pdfOptionsDialog').dialog('close');
            quickClaim.hideOverlayMessage();
        },
        handlePdfClick: function () {
            hypeFileDownload.tokenName = "pastClaimsPDFToken";
            hypeFileDownload.setReturnFunction(pastClaims.pdf.downloadComplete);

            var url = pastClaims.actions.search + '?'
                    + $('#' + pastClaims.form.formID).serialize()
                    + '&' + $('#pdfForm').serialize()
                    + '&footerText=' + $('#PDFFooterText').text()
                    + '&mode=pdf'
                    + '&pastClaimsPDFToken=' + hypeFileDownload.blockResubmit();

            quickClaim.showOverlayMessage('Generating PDF');
            $('#hiddenDownloader').remove();
            $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');

        },
        openDialog: function () {
            $('#PDFFooterText').text($('#results-table .pastClaimsPager .pagedisplay').text());
            $('#pdfOptionsDialog').dialog('open');
        }
    },
    refine: {
        initialize: function () {
            $('#refine_search_claim_type').change(function () {
                pastClaims.refine.handleRefineSearchEntry('claim_type', $(this).val());
            });
            $('#refine_search_doctor_id').change(function () {
                pastClaims.refine.handleRefineSearchEntry('doctor_id', $(this).val());
            });
            $('#refine_search_claim_id').keypress(function (e) {
                if (e.which == 13) {
                    pastClaims.refine.handleRefineSearchEntry('claim_id', $(this).val());
                }
            });
            $('#clear_refine_terms').button();
            $('#clear_refine_terms').click(function () {
                $('#refine_search input:text, #refine_search select').val('');
                $('#' + pastClaims.form.prefix + '_claim_id').val('');
                $('#' + pastClaims.form.prefix + '_doctor_id').val('');
                pastClaims.form.toggleAllCheckbox('claim_type', 'all');
            });
        },
        handleRefineSearchEntry: function (type, value) {
            if (type == 'claim_type') {
                $('.claim_type input').prop('checked', false);
                if (value) {
                    $('#' + pastClaims.form.prefix + '_claim_type_' + value).prop('checked', true);
                } else {
                    pastClaims.form.toggleAllCheckbox('claim_type');
                }
            } else {
                $('#' + pastClaims.form.prefix + '_' + type).val(value);
            }
            $('#find_button').click();
            $('#' + pastClaims.resultsTable + ' tbody tr').remove();
            //$('#' + pastClaims.resultsTable + ' tfoot tr').remove();
        }
    },
    results: {
        menuHtml: null,
        columns: new Array(),
        counts: {
            edit: 0,
            markClosed: 0,
            markPaid: 0,
            resubmit: 0,
            submit: 0
        },
        initialized: false,
        pageNumber: 0,
        paginationSize: 500,
        paginationSort: [],
        rowPrefix: 'claimItemRow',
        showTotals: false,
        showRebill: false,
        sortOrder: {},
        subtotals: {},
        totals: {},

        initializePager: function () {
            $('#' + pastClaims.resultsTable + ' thead th:last').append(quickClaim.tablesorterTooltip);
            var headers = {},
                    $table = $('#' + pastClaims.resultsTable), i,
                    l = $table.find('thead th').length,
                    // choose the columns you want to sort (zero-based index)
                    sortcolumns = [];

// build headers object; based on sortcolumn selections
            for (i = 0; i < l; i++) {
                // if ($.inArray(i, sortcolumns) < 0) {
                headers[i] = {sorter: false}
                // }
            }

            $('#' + pastClaims.resultsTable).tablesorter({
                theme: 'blue',
                serverSideSorting: false,
                headerTemplate: '{content} {icon}',
                //widgets: ['selectRow', 'zebra'],

                headers: headers,
                sortList: pastClaims.results.paginationSort,
                dateFormat: quickClaim.dateFormat,
                debug: false
            });

            $('#' + pastClaims.resultsTable).tablesorterPager({
                container: $('.pastClaimsPager'),
                size: pastClaims.results.paginationSize,
                cssPageSize: '.pagesize',
                removeRows: false,
                ajaxUrl: pastClaims.actions.search + '?{sortList:sort}&pageNumber={page}&size={size}&{filterList:fcol}',
                customAjaxUrl: function (table, url) {
                    return url + '&' + $('#' + pastClaims.form.formID).serialize();
                },
                ajaxObject: {
                    dataType: 'json',
                    complete: function () {
                        $('#past_claims_results_table_form .sortTip').tooltip();

                        quickClaim.hideOverlayMessage();
                        $('#resultsCheckAll').click(function () {
                            var checked = $(this).prop('checked');
                            $('#' + pastClaims.resultsTable + ' input[type=checkbox]').prop('checked', checked);
                        });

                        pastClaims.results.buildErrorTooltips();
                        pastClaims.results.buildActionButtons();


                        if ($('#edit_checkbox_button')) {
                            $('.actions_button').each(function () {
                                contextMenu.createButton($(this).prop('id'));
                            });
                        }



                        quickClaim.ajaxLocked = false;
                        $('#' + pastClaims.messages.spinner).hide();
                        $(window).trigger('resize');


                        var table = $(".DataTablePastClaimResult").DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": true,
                            "ordering": true,
                            "order": [[3, "desc"]],
                            "info": true,
                            "autoWidth": false,
                            "responsive": false,
//                            "scrollX": true,
                            "dom": 'Bfrtip',
                            "buttons": [
                                {
                                    "extend": 'pdf',
                                    "text": '<i class="clip-file-pdf"></i> PDF',
                                    "className": 'btn btn-danger btn-squared',
                                    extend: 'pdfHtml5',
                                    orientation: 'landscape',
                                    pageSize: 'LEGAL'
                                },
                                /*{
                                 "extend": 'csv',
                                 "text": '<i class="clip-file-openoffice"></i> CSV',
                                 "className": 'btn btn-warning btn-squared'
                                 },*/
                                {
                                    "extend": 'excel',
                                    "text": '<i class="clip-file-excel"></i> Excel',
                                    "className": 'btn btn-info btn-squared'
                                },
                                {
                                    "extend": 'print',
                                    "text": '<i class="fa-print fa"></i> Print',
                                    "className": 'btn btn-blue btn-squared'
                                },
                                        /*{
                                         "extend": 'copy',
                                         "text": '<i class="clip-copy-2"></i> Copy',
                                         "className": 'btn btn-teal btn-squared'
                                         }*/
                            ],
                            "oLanguage": {
                                "sSearch": "",
                                "oPaginate": {
                                    "sNext": "<i class='clip-chevron-right'></i>",
                                    "sPrevious": "<i class='clip-chevron-left'></i>"
                                }

                            },
                            "columnDefs": [
                                {"targets": 0, "width": '0%', "orderable": false}
                            ],

                            "sDom": '<"wrapper"<"row"<"col-sm-12" <"row" <"col-sm-7" <"col-sm-6 small"l><"col-sm-6"f<"DTsearchlabel">><"col-sm-8"p><"col-sm-4 small"i>> <"col-sm-5 pdfTableOptions small"B>>>>rt<"clear">>',

                            initComplete: function () {
                                $('tfoot th').each(function () {
                                    if ($(this).attr('style')) {
                                    } else {
                                        $(this).attr('style', 'padding:5px 2px;width:20px');
                                    }

                                })
                                $('td').each(function () {
                                    if ($(this).attr('style')) {
                                    } else {
                                        $(this).attr('style', 'padding:5px 2px;width:20px');
                                    }

                                })
                                var searchoptions = $("#past_claims_results_table tfoot").html();
                                $("#past_claims_results_table thead").append(searchoptions);
                                $("#past_claims_results_table tfoot").remove();
                                //console.log('------------------------------');
//                this.api().row(':eq(0)').every(function(){
//                    console.log($(this).html());
//                    var column = this;
//                    console.log();
//                    var select = $('<select class="selectRsltTbl"><option value=""></option></select>')
//                    .appendTo( /*$("#past_claims_results_table thead tr")*/ column)
//                    .on( 'change', function () {
//                        var val = $.fn.dataTable.util.escapeRegex(
//                            $(this).val()
//                        );
// 
//                        column
//                            .search( val ? '^'+val+'$' : '', true, false )
//                            .draw();
//                    } );
//                })


                                //console.log('------------------------------');
                                var aa = 0;
                                this.api().columns().every(function () {
                                    var column = this;
                                    //console.log($("#past_claims_results_table thead tr:eq(1)"));

                                    if ($(column.header())[0].column != 0) {
                                        var select = $('<select class="selectRsltTbl"><option value=""></option></select>')
                                                .appendTo($("#past_claims_results_table thead tr:eq(1) th:eq(" + aa + ")")/*$("#past_claims_results_table thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                                                .on('change', function () {
                                                    var val = $.fn.dataTable.util.escapeRegex(
                                                            $(this).val()
                                                            );

                                                    column
                                                            .search(val ? '^' + val + '$' : '', true, false)
                                                            .draw();
                                                });

                                        column.data().unique().sort().each(function (d, j) {
                                            select.append('<option value="' + d + '">' + d + '</option>')
                                        });
                                    }
                                    aa++;
                                });
                            }



                        });

                        $(".otherSearchFilter").change(function () {
                            var data = $(this).select2({placeholder: "Claim Type", width: '100%'}).val();

                            if (data && data.length > 0) {
                                var d2 = '';
                                for (var i in data) {
                                    data[i] = data[i].substring(1, data[i].length);
                                    d2 += data[i] + '|';
                                }
                                d2 = d2.substring(0, d2.length - 1);
                                console.log("'" + d2 + "'");
                                $(".DataTablePastClaimResult").DataTable().columns('.charge_to')
                                        .search(d2, true)
                                        .draw();
                            } else {
                                table
                                        .columns('.charge_to')
                                        .search('')
                                        .draw();
                            }
                        })

                        $(".newDrProfile").change(function () {
                            var data = $(this).select2('data');
                            var aa = $(".DataTablePastClaimResult").DataTable().columns('.doctor').data();
                            if (aa.length) {
                                if (data) {
                                    data = data.text;
                                    $(".DataTablePastClaimResult").DataTable().columns('.doctor')
                                            .search(data)
                                            .draw();
                                } else {
                                    table
                                            .columns('.doctor')
                                            .search('')
                                            .draw();
                                }
                            } else {
                                if (data) {
                                    data = data.text;
                                    //console.log($("#past_claims_doctor_id option:contains("+data+")").val());
                                    var newoptionval = $(".newDrProfile option:contains(" + data + ")").val();
                                    $(".drSelectMainPopular").select2("val", newoptionval).trigger('change');
                                    console.log(newoptionval);
                                    $("#find_button").trigger('click');
                                    $(".DataTablePastClaimResult").dataTable().fnDestroy();

                                } else {
                                    table.columns('.doctor').search('').draw();
                                }
                            }
                        })

                        //console.log($(".DataTablePastClaimResult").DataTable().columns('.doctor').data());


                        $('div.dataTables_filter input').addClass('form-control');
                        $('div.dataTables_filter input').attr('placeholder', 'Search');
                        $(".DTsearchlabel").html('<i class="clip-search"></i>');
                        $('.dataTables_filter').attr('style', 'width:100%');
                        $('.dataTables_filter label').attr('style', 'width:100%');
                        $('.dataTables_length select').attr('style', 'font-size: 12px;padding: 5px 3px;background: #fff;');
                        $('div.dataTables_filter input').attr('style', 'float:right;width:98%;margin-right:5px;');
                        $(".dataTables_wrapper .dataTables_paginate .paginate_button").attr('style', 'background:#fff !important;color:#444444 !important; border-radius: 0;');


//                        table.on('responsive-display', function (e, datatable, row, showHide, update) {
//                            $('td ul').attr('style', 'width:100% !important');
//                            $('td ul').addClass('row');
//                            $('td ul li').addClass('col-sm-4');
//                            $(".dropdown-menu li").removeClass('col-sm-4');
//                        });




                        $("#past_claims_results_table thead tr:eq(1)").slideUp();
                        
                        $("#past_claims_results_table thead tr").hover(function () {
                            $("#past_claims_results_table thead tr:eq(1)").slideDown();
//                            console.log('down');
                        },function () {
                           $("#past_claims_results_table thead tr:eq(1)").slideUp();
//                            console.log('up');
                        });
                        
//                        $("#past_claims_results_table thead tr").mouseover(function () {
//                            $("#past_claims_results_table thead tr:eq(1)").slideDown();
//                            console.log('down');
//                        }).mouseout(function () {
//                           $("#past_claims_results_table thead tr:eq(1)").slideUp();
//                            console.log('up');
//                        });

                       
                        $(".selectRsltTbl").select2({
                            placeholder: "Search",
                            allowClear: true,
                            width: '100%'
                        });
                        var width_nav = $(".main-navigation").width();
                        if (width_nav > 40) {
                            $(".navigation-toggler").trigger('click');
                        }
                        $('.select2-arrow').hide();
                    },
                    beforeSend: function () {
                        $(".DataTablePastClaimResult").dataTable().fnDestroy();
                        quickClaim.ajaxLocked = true;
                        //quickClaim.showOverlayMessage('Searching for claims');
                        $('#messages .icons div').hide();
                        $('#' + pastClaims.messages.spinner).show();
                        $('#' + pastClaims.messages.text).html('Searching for claims').show();
                        $('#past_claims_results_table_form input[type=button]').remove();
                        $('#pdfButton').button('disable');
                        $('#csvButton').button('disable');
                    },
                    error: function () {
                        $('#' + pastClaims.messages.error).show();
                        $('#' + pastClaims.messages.text).html('<div class="alert alert-info">An error occurred while contacting the server.  Please refresh the page and try again.</div>').show();
                    }
                },
                ajaxProcessing: function (rs) {
                    if (rs.hasOwnProperty('errors')) {
                        quickClaim.showFormErrors(rs.errors, '#' + pastClaims.form.errorDiv, '#' + pastClaims.form.formID, true);

                        $('#' + pastClaims.messages.error).show();
                        $('#' + pastClaims.messages.text).html('<div class="alert alert-info">The form contains invalid data.</div>').show();
                        return false;
                    }

                    if (rs.hasOwnProperty('count') && rs.count) {
                        $('#pdfButton').button('enable');
                        $('#csvButton').button('enable');
                    }

                    $('#tabs').tabs('enable', 1);
                    $('#tabs').tabs('option', 'active', 1);

                    $('#' + pastClaims.messages.text).html('<div class="alert alert-info">Received Results.</div>').show();
                    $('#' + pastClaims.messages.checkmark).show();

                    if (rs.hasOwnProperty('data')) {
                        // $('#' + pastClaims.resultsTable + ' tfoot tr').remove();

                        pastClaims.data = {};
                        pastClaims.errors = {};
                        pastClaims.explCodes = rs.explCodes;

                        var data = rs.data;
                        var rows = '';

                        for (var a in pastClaims.results.counts) {
                            pastClaims.results.counts[a] = 0;
                        }

                        if (pastClaims.results.showTotals) {
                            pastClaims.results.subtotals = rs.subtotals;
                            pastClaims.results.totals = rs.totals;
                        }

                        if (rs.hasOwnProperty('sortOrder')) {
                            pastClaims.results.sortOrder = rs.sortOrder;

                            pastClaims.results.clearResultsNoRefresh();


                        }

                        if (rs.hasOwnProperty('filter')) {
                            var filter = rs.filter;
                        }

                        for (var a in data) {
                            var rowID = pastClaims.results.rowPrefix + '_' + data[a]['id'];

                            rows += '<tr id="' + rowID + '">';
                            rows += pastClaims.results.buildTableRow(data[a], data[a].id, filter);
                            rows += '</tr>' + "\n";
                        }

                        if (pastClaims.results.showTotals) {
                            pastClaims.results.buildTotalsRow();
                        }

                        return [rs.count, $(rows)];
                    }
                }
            });

        },
        buildActionButtons: function () {
            var buttons = '';

            if (pastClaims.results.canDoAction('edit')) {
                buttons += '<button type="button" id="edit_checkbox_button" name="edit_checkbox_button"style="margin:2px;" class="btn-info btn btn-squared"><i class="clip-pencil-2"></i> Edit Selected Claims</button>';
            }
            if (pastClaims.results.canDoAction('resubmit')) {
                buttons += '<button type="button" id="resubmit_checkbox_button" name="resubmit_checkbox_button" style="margin:2px;" class="btn-danger btn btn-squared"><i class="clip-error"></i> Mark Selected Claims As Unsubmitted</button>';
            }
            if (pastClaims.results.canDoAction('submit')) {
                buttons += '<button type="button" id="submit_checkbox_button" name="submit_checkbox_button" style="margin:2px;" class="btn-primary btn btn-squared" ><i class="clip-pencil-2"></i> Submit Selected Claims</button>';
            }
            if (pastClaims.results.showRebill) {
                buttons += '<button type="button" id="rebill_checkbox_button" name="rebill_checkbox_button" style="margin:2px;"  class="btn-blue btn-squared btn" ><i class="clip-file-check"></i> Re-bill selected claims</button>';
            }
            if (pastClaims.results.canDoAction('mark_paid')) {
                buttons += '<button type="button" id="mark_paid_checkbox_button" name="mark_paid_checkbox_button" style="margin:2px;" class="btn-green btn btn-squared" ><i class="clip-checkmark-circle-2"></i> Mark Selected Claim Items As Paid</button>';
            }
            if (pastClaims.results.canDoAction('mark_closed')) {
                buttons += '<button type="button" id="mark_closed_checkbox_button" name="mark_closed_checkbox_button" style="margin:2px;"  class="btn-danger btn btn-squared"><i class="clip-close-3"></i> Mark Selected Claim Items As Closed</button>';
            }

            $('#action_buttons').empty();
            $(buttons).appendTo($('#action_buttons'));

            //$(buttons).appendTo($('#' + pastClaims.resultsTable + '_form'));
            $('#action_buttons input[type=button]').button();

            $('#edit_checkbox_button').click(function () {
                pastClaims.handleEditGroupClick();
            });
            $('#resubmit_checkbox_button').click(function () {
                pastClaims.handleResubmitClick(pastClaims.results.getActionClaimItemIDs('resubmit'));
            });

            $('#submit_checkbox_button').click(function () {
                pastClaims.handleSubmitClick(pastClaims.results.getActionClaimIDs('submit'));
            });

            $('#rebill_checkbox_button').click(function () {
                pastClaims.handleRebillClick();
            });

            $('#mark_paid_checkbox_button').click(function () {
                pastClaims.handleMarkPaidClick(pastClaims.results.getActionClaimItemIDs('mark_paid'));
            });
            $('#mark_closed_checkbox_button').click(function () {
                pastClaims.handleMarkClosedClick(pastClaims.results.getActionClaimItemIDs('mark_closed'));
            });
        },
        buildErrorTooltips: function () {
            for (var claimItemId in pastClaims.errors) {
                var errs = pastClaims.errors[claimItemId].split(', ');
                var title = '';

                for (var a in errs) {
                    if (title.length) {
                        title += '<br />';
                    }
                    title += errs[a] + ': ';

                    if (pastClaims.explCodes.hasOwnProperty(errs[a])) {
                        title += pastClaims.explCodes[errs[a]];
                    } else {
                        title += 'Unknown error code';
                    }
                }
                $('#' + pastClaims.results.rowPrefix + '_' + claimItemId + ' div.errors').attr('title', title);
            }
        },
        buildTableRow: function (data, id, filter) {

            if (filter) {
                for (var i in filter) {
                    if (pastClaims.results.columns[i]) {
                        if (!(data[pastClaims.results.columns[i]].includes(filter[i]))) {
                            return;
                        }
                    }
                }
            }

            var claimItemId = id;
            var claimId = data['claim_id'];
            var tds = '';

            pastClaims.data[claimItemId] = {};
            pastClaims.data[claimItemId]['claim_id'] = data['claim_id'];
            pastClaims.data[claimItemId]['patient_id'] = data['patient_id'];
            pastClaims.data[claimItemId]['item_count'] = data['item_count'];
            pastClaims.data[claimItemId]['edit'] = data['actions'].hasOwnProperty('edit');
            pastClaims.data[claimItemId]['resubmit'] = data['actions'].hasOwnProperty('resubmit');
            pastClaims.data[claimItemId]['submit'] = data['actions'].hasOwnProperty('submit');
            pastClaims.data[claimItemId]['mark_closed'] = data['actions'].hasOwnProperty('mark_closed');
            pastClaims.data[claimItemId]['mark_paid'] = data['actions'].hasOwnProperty('mark_paid');

            for (var a = 0; a < pastClaims.results.columns.length; a++) {
                var className = pastClaims.results.columns[a];
                className = className.replace(' ', '_');
                if (className == 'edit_checkbox') {
                    tds += '<td class="' + className + '">'
                            + '<input type="checkbox" id="claim_' + id + '" name="claim[' + id + ']" value="' + data['claim_id'] + '" checked="checked" />'
                            + '</td>';
                } else if (className == 'actions') {
                    tds += '<td class="context_menu actions">'
                            + '<img class="actions_button" id="actions_' + claimItemId + '" src="' + pastClaims.results.menuHtml + '" />&nbsp;&nbsp;';

                    if (data['item_count'] > 1) {
                        tds += data['item_count'] + ' Items in Claim';
                    }
                    tds += '</td>';
                } else if (className == 'errors') {
                    tds += '<td class="' + className + '" title="">' + data['errors'] + '</td>';
                    if (data['errors']) {
                        pastClaims.errors[claimItemId] = data['errors'];
                    }
                } else {
                    console.log(className);
                    console.log(data);
                    var title = typeof data[className] != 'undefined' ? data[className].replace('_', ' ') : '';
                    var newWidth = '0px';
                    if(className == 'service_code' || className == 'diag_code' ){
                        newWidth = '6px';
                    }
                    
                    tds += '<td style="width:'+newWidth+'" class="' + className + '">' + title + '</td>';
                }
            }


            return tds;
        },
        buildTotalsRow: function () {
            $('#past_claims_totals .subtotals .fee_subm').text('$' + number_format(pastClaims.results.subtotals.submitted, 2, '.', ','));
            $('#past_claims_totals .totals .fee_subm').text('$' + number_format(pastClaims.results.totals.submitted, 2, '.', ','));

            $('#past_claims_totals .subtotals .fee_paid').text('$' + number_format(pastClaims.results.subtotals.paid, 2, '.', ','));
            $('#past_claims_totals .totals .fee_paid').text('$' + number_format(pastClaims.results.totals.paid, 2, '.', ','));

            $('#past_claims_totals .subtotals .num_claims').text(number_format(pastClaims.results.subtotals.claims, 0, '.', ','));
            $('#past_claims_totals .totals .num_claims').text(number_format(pastClaims.results.totals.claims, 0, '.', ','));

            $('#past_claims_totals .subtotals .num_items').text(number_format(pastClaims.results.subtotals.claimItems, 0, '.', ','));
            $('#past_claims_totals .totals .num_items').text(number_format(pastClaims.results.totals.claimItems, 0, '.', ','));

            $('#past_claims_totals .subtotals .num_patients').text(number_format(pastClaims.results.subtotals.patients, 0, '.', ','));
            $('#past_claims_totals .totals .num_patients').text(number_format(pastClaims.results.totals.patients, 0, '.', ','));

            for (var a in pastClaims.results.totals.pay_progs) {
                var className = a.replace(' - ', '').replace(' ', '') + 'Totals';

                $('#past_claims_totals tbody tr.' + className).remove();

                var tr = '<tr class="' + className + ' PayProgSubtotals">'
                        + '<td>' + a + '</td>'
                        + '<td class="fee_subm">$' + number_format(pastClaims.results.totals.pay_progs[a].submitted, 2, '.', ',') + '</td>'
                        + '<td class="fee_paid">$' + number_format(pastClaims.results.totals.pay_progs[a].paid, 2, '.', ',') + '</td>'
                        + '<td class="num_claims">' + number_format(pastClaims.results.totals.pay_progs[a].claims, 0, '.', ',') + '</td>'
                        + '<td class="num_items">' + number_format(pastClaims.results.totals.pay_progs[a].claimItems, 0, '.', ',') + '</td>'
                        + '<td class="num_patients">n/a</td>'
                        + '</tr>';

                $('#past_claims_totals tbody .totals').before(tr);
            }

            for (var a in pastClaims.results.subtotals.pay_progs) {
                var className = a.replace(' - ', '').replace(' ', '') + 'Subtotals';

                $('#past_claims_totals tbody tr.' + className).remove();

                var tr = '<tr class="' + className + ' PayProgSubtotals">'
                        + '<td>' + a + '</td>'
                        + '<td class="fee_subm">$' + number_format(pastClaims.results.subtotals.pay_progs[a].submitted, 2, '.', ',') + '</td>'
                        + '<td class="fee_paid">$' + number_format(pastClaims.results.subtotals.pay_progs[a].paid, 2, '.', ',') + '</td>'
                        + '<td class="num_claims">' + number_format(pastClaims.results.subtotals.pay_progs[a].claims, 0, '.', ',') + '</td>'
                        + '<td class="num_items">' + number_format(pastClaims.results.subtotals.pay_progs[a].claimItemCount, 0, '.', ',') + '</td>'
                        + '<td class="num_patients">n/a</td>'
                        + '</tr>';

                $('#past_claims_totals tbody .subtotals').before(tr);
            }

        },
        canDoAction: function (action) {
            for (var a in pastClaims.data) {
                if (pastClaims.data[a][action]) {
                    return true;
                }
            }

            return false;
        },
        clearResultsNoRefresh: function () {
            var columns = new Array();
            var count = 0;
            columns[count++] = 'edit_checkbox';

            var newColumnsOrder = $('select[name="past_claims[report_columns][]"]').val();

            for (var i = 0; i < newColumnsOrder.length; i++) {
                if (newColumnsOrder[i]) {
                    var className = newColumnsOrder[i].replace('_', ' ');
                    columns[count++] = className;
                }
            }
//            $('input[name="past_claims[report_columns][]"]:checked').each(function () {
//                var className = $(this).prop('id').replace(pastClaims.form.prefix + '_report_columns_', '');
//                columns[count++] = className;
//            });

            columns[count++] = 'actions';

            pastClaims.results.paginationSort = new Array();
            count = 0;
            for (var a in pastClaims.results.sortOrder) {
                var order = pastClaims.results.sortOrder[a];
                var colNumber = jQuery.inArray(a, columns);
                if (colNumber != -1) {
                    pastClaims.results.paginationSort[count++] = new Array(colNumber, order);
                }
            }

            pastClaims.results.columns = columns;

//            $('#' + pastClaims.resultsTable).tablesorter({
//               // sortList: pastClaims.results.paginationSort
//            });


        },
        clearResults: function () {
            var columns = new Array();
            var count = 0;

            $('#' + pastClaims.resultsTable).remove();
            $('#past_claims_totals tbody tr.PayProgSubtotals').remove();
//            $(".DataTablePastClaimResult").dataTable().fnDestroy(true);
            $('#past_claims_results_table_form').html($('<div style="overflow:auto"><table id="' + pastClaims.resultsTable + '" class="DataTablePastClaimResult table table-hover "><thead></thead><tbody></tbody><tfoot></tfoot></table></div><br/>'));

            var tr = '<tr><th class="edit_checkbox" style="padding-left:0px;"><input type="checkbox" id="resultsCheckAll" value="1" checked="checked" /></th>';
            columns[count++] = 'edit_checkbox';
            //$('input[name="past_claims[report_columns][]"]:checked')
            var newColumnsOrder = $('select[name="past_claims[report_columns][]"]').val();

            for (var i = 0; i < newColumnsOrder.length; i++) {

                if (newColumnsOrder[i]) {

                    var text = newColumnsOrder[i];
                    var className = newColumnsOrder[i];
                    text = text.replace('_', ' ').replace(/\_/g, ' ');
                    var newWidth = '20px';
                    if(className == 'service_code' || className == 'diag_code' ){
                        newWidth = '6px';
                    }
                    tr += '<th style="width:'+newWidth+'" class="' + className + '">' + text.charAt(0).toUpperCase() + text.slice(1);
                    +'</th>';
                    columns[count++] = className;
                }
            }
            /*$('select[name="past_claims[report_columns]"]').each(function () {
             var className = $(this).val();//$(this).prop('id').replace(pastClaims.form.prefix + '_report_columns_', '');
             var text = className;//.replace( '_', ' ');//$('label[for="' + $(this).prop('id') + '"]').text();
             tr += '<th class="' + className + '">' + text + '</th>';
             columns[count++] = className;
             });*/
            tr += '<th class="actions">Actions</th></tr>';
            columns[count++] = 'actions';

            var subhead = '<tr><th></th>';

            var newColumnsOrder = $('select[name="past_claims[report_columns][]"]').val();
            for (var i = 0; i < newColumnsOrder.length; i++) {
                if (newColumnsOrder[i]) {
                    subhead += '<th></th>';
                }
            }
            subhead += '<th></th></tr>';

            $('#' + pastClaims.resultsTable + ' thead').append($(tr));
            $('#' + pastClaims.resultsTable + ' tfoot').append($(subhead));
            $('#' + pastClaims.resultsTable + ' thead th.edit_checkbox').addClass('sorter-false');
            $('#' + pastClaims.resultsTable + ' thead th.actions').addClass('sorter-false');
            $('#' + pastClaims.resultsTable + ' thead th.health_card_vc').addClass('sorter-false');
            $('#' + pastClaims.resultsTable + ' thead th.provider_number').addClass('sorter-false');
            $('#' + pastClaims.resultsTable + ' thead th.group_num').addClass('sorter-false');
            $('#' + pastClaims.resultsTable + ' thead th.spec_code').addClass('sorter-false');

            pastClaims.results.paginationSort = new Array();
            count = 0;
            for (var a in pastClaims.results.sortOrder) {
                var order = pastClaims.results.sortOrder[a];
                var colNumber = jQuery.inArray(a, columns);
                if (colNumber != -1) {
                    pastClaims.results.paginationSort[count++] = new Array(colNumber, order);
                }
            }

            pastClaims.results.columns = columns;
            pastClaims.results.initializePager();

        },
        getActionClaimIDs: function (action) {
            var ids = new Array();
            $('#' + pastClaims.resultsTable + '_form tbody input:checked').each(function () {
                var claimItemId = $(this).closest('tr').attr('id').replace(pastClaims.results.rowPrefix + '_', '');
                if (pastClaims.data[claimItemId][action]) {
                    ids[ids.length] = pastClaims.getClaimId(claimItemId);
                }
            });
            return ids;
        },
        getActionClaimItemIDs: function (action) {
            var ids = new Array();
            $('#' + pastClaims.resultsTable + '_form tbody input:checked').each(function () {
                var claimItemId = $(this).closest('tr').attr('id').replace(pastClaims.results.rowPrefix + '_', '');
                if (pastClaims.data[claimItemId][action]) {
                    ids[ids.length] = claimItemId;
                }
            });
            return ids;
        }
    },

    initialize: function () {
        $('#past_claims_ra').prop('disabled', true);
        pastClaims.form.initialize();
        pastClaims.refine.initialize();
        pastClaims.pdf.initialize();
        pastClaims.csv.initialize();
    },

    getClaimId: function (claimItemId) {
        if (pastClaims.data.hasOwnProperty(claimItemId)) {
            return pastClaims.data[claimItemId]['claim_id'];
        }
        return $('#' + pastClaims.results.rowPrefix + '_' + claimItemId + ' div.claim_id').text();
    },
    getPatientId: function (claimItemId) {
        if (pastClaims.data.hasOwnProperty(claimItemId)) {
            return pastClaims.data[claimItemId]['patient_id'];
        }
    },
    handleDetailsActionClick: function (claim_id, new_tab) {
        if (new_tab) {
            window.open(pastClaims.actions.details + '?id=' + claim_id, 'details');
        } else {
            window.location = pastClaims.actions.details + '?id=' + claim_id;
        }
    },
    handleEditClick: function (claim_id, item_id, new_tab) {
        if (new_tab) {
            for (var a in pastClaims.data) {
                if (pastClaims.data[a].claim_id == claim_id) {
                    $('tr#' + pastClaims.results.rowPrefix + '_' + a).addClass('edited');
                }
            }
            $('tr#' + pastClaims.results.rowPrefix + '_' + item_id).addClass('edited');
            window.open(pastClaims.actions.edit + '?id=' + claim_id, 'claim');
        } else {
            window.location = pastClaims.actions.edit + '?id=' + claim_id;
        }
    },
    handleEditGroupClick: function () {
        quickClaim.showOverlayMessage('Loading Claim Edit Interface');
        $('#' + pastClaims.resultsTable + '_form tbody input:checked').each(function () {
            var claimItemId = $(this).prop('id').replace('claim_', '');
            if (!pastClaims.data[claimItemId]['edit']) {
                $(this).prop('checked', false);
            }
        });
        $('#' + pastClaims.resultsTable + '_form').submit();
    },
    handleFindClick: function () {
        if (quickClaim.ajaxLocked) {
            return;
        }

        pastClaims.form.clearErrors();
        pastClaims.results.clearResults();
    },
    handleMarkClosedClick: function (claimItemIDs) {
        pastClaims.selectedIDs = claimItemIDs;
        claimStatusActions.markClosed.openDialog(claimItemIDs);
    },
    handleMarkPaidClick: function (claimItemIDs) {
        pastClaims.selectedIDs = claimItemIDs;
        claimStatusActions.markPaid.openDialog(claimItemIDs);
    },
    handlePdfActionClick: function (claim_id) {
        pastClaims.setupIframe();
        pastClaims.setupHiddenForm(new Array(), pastClaims.actions.claimPDF + '?id=' + claim_id, '');
    },
    handleResubmitClick: function (claimItemIDs) {
        pastClaims.selectedIDs = claimItemIDs;
        claimStatusActions.resubmit.openDialog(claimItemIDs);
    },
    handleSubmitClick: function (claimIDs) {
        if (!claimIDs.length) {
            quickClaim.showOverlayMessage('No Submittable Claims are selected.');
            setTimeout(function () {
                quickClaim.hideOverlayMessage();
            }, 2000);
            return;
        }

        var data = 'claim_ids=' + claimIDs.join(',');
        batch_create.handleDoctorLocationCountRequest(data);
    },
    handleRebillClick: function () {
        $('#rebillConfirmDialog').dialog('open');
        $('#rebill_new_date').datepicker();
    },
    confirmRebillClick: function (date) {
        var claimIDs = pastClaims.results.getActionClaimIDs('resubmit');
        var claimItemIDs = pastClaims.results.getActionClaimItemIDs('resubmit');

        $.ajax({
            type: 'post',
            dataType: 'json',
            data: 'c_ids=' + claimIDs.join(',') + '&ids=' + claimItemIDs.join(',') + '&date=' + date,
            url: pastClaims.actions.changeItems,
            success: function (rs) {
                $('#rebillCompleteDialog').dialog('open');

                var html = '<ul>';
                if (rs.res) {
                    for (var msg in rs.res) {
                        html += '<li>' + rs.res[msg] + '</li>';
                    }
                }

                html += '</ul><h4>Errors</h4><ul>';

                if (rs.error) {
                    for (var err in rs.error) {
                        html += '<li>' + 'Claim for ' + rs.error[err] + '</li>';
                    }

                    html += '</ul>';
                }


                $('.rebillCount').html(html);
            },
            beforeSend: function () {
                quickClaim.showOverlayMessage('Cloning selected claims');
            },
            complete: function () {
                quickClaim.hideOverlayMessage();
            }
        });
    },
    setupHiddenForm: function (fields, action, prefix) {
        setTimeout(function () {
            var frame_body = $('#hiddenDownloader').contents().find('body');
            var form = '<form id="hidden_form" action="' + action + '" method="POST">';

            for (var a in fields) {
                var name = fields[a].substring(0, fields[a].indexOf('='));
                var value = fields[a].substring(fields[a].indexOf('=') + 1);

                name = name.replace(pastClaims.form.prefix, prefix);
                form += '<input type="text" value="' + value + '" id="' + name + '" name="' + name + '" />';
            }
            form += '</form>';
            frame_body.append($(form));
            $('#hiddenDownloader').contents().find('#hidden_form').submit();
        }, 15000);
    },
    setupIframe: function () {
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    setupIframeWithURL: function (url, params, mode) {
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '?' + params + '"  style="width: 0; height: 0; border: none; display: none;" onload="pastClaims.' + mode + '.downloadComplete();"></iframe>');
    }
};
