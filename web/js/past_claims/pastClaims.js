var pastClaims = {
    hlx8lines: {},
    actions: {
        claimPDF: null,
        details: null,
        edit: null,
        profile: null,
        refdocAutocomplete: null,
        search: null,
        prevSearch: null,
        changeItems: null
    },
    subtabledata: [], 
    request: null,
    currentColCount: 0,
    isBckgrndActive: false,
    isAddedActionBtn: false,
    dataSet: [],
    itemArray: [],
    resetAll: false,
    dtCurrentPage: 1,
    maxPages: 1,
    dtCurrentResults: 500,
    data: {},
    errors: {},
    explCodes: {},
    messages: {
        checkmark: 'checkmark',
        error: 'error_icon',
        spinner: 'spinner',
        text: 'messages_text' 
    },
    resultsTable: 'past_claims_results_table',
    selectedIDs: new Array(),
    csv: {
        initialize: function () {
            $('#csvOptionsDialog').dialog({
                closeOnEscape: true,
                autoOpen: false,
                height: $(window).height() - 200,
                modal: true,
                width: $(window).width() - 200
            });

           
            $('#markFacility').dialog({
                    resizable: false,
                    modal: true,
                    autoOpen: false,
                    width: 500,
                    buttons: {
                        "Yes":{
                            click: function () {
                                var claim_id = $("#markFacility").data('claim_id');
                                pastClaims.confirmReplaceFacility($('#doctor_moh_facility_num').val(),$('#facility_id').val(),claim_id);
                                $('#markFacility').dialog('close');
                            },
                            text: 'Yes',
                            class: 'btn btn-success'
                        },
                        "No":{
                            click: function () {
                                $('#markFacility').dialog('close');
                            },
                            text: 'No',
                            class: 'btn btn-danger'
                        }                        
                    }
                });

            $('#downloadCSV').button();
            $('#downloadCSV').click(function () {
                pastClaims.csv.handleCsvClick();
            });
        },
        downloadComplete: function () {
            if ($('#hiddenDownloader').contents().find('body').text()) {
                $('#messages .icons div').hide();
                $('#' + pastClaims.messages.error).show();
                $('#' + pastClaims.messages.text).html('<div class="alert alert-info">An error occurred while generating the CSV File</div>').show();
            } else {
                $('#messages .icons div').hide();
                $('#' + pastClaims.messages.checkmark).show();
                $('#' + pastClaims.messages.text).html('<div class="alert alert-info">Generated the CSV File</div>').show();
            }
            $('#csvOptionsDialog').dialog('close');
            quickClaim.hideOverlayMessage();
        },
        handleCsvClick: function () {
            hypeFileDownload.tokenName = "pastClaimsCSVToken";
            hypeFileDownload.setReturnFunction(pastClaims.csv.downloadComplete);

            var url = pastClaims.actions.search + '?'
                    + $('#' + pastClaims.form.formID).serialize()
                    + '&' + $('#csvForm').serialize()
                    + '&footerText=' + $('#CSVFooterText').text()
                    + '&mode=csv'
                    + '&pastClaimsCSVToken=' + hypeFileDownload.blockResubmit();

            quickClaim.showOverlayMessage('Generating CSV');
            $('#hiddenDownloader').remove();
            $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
        },
        openDialog: function () {
            $('#CSVFooterText').text($('#results-table .pastClaimsPager .pagedisplay').text());
            $('#csvOptionsDialog').dialog('open');
        }
    },
    form: {
        errorDiv: 'past_claims_errors',
        formID: '',
        prefix: '',
        refDocCache: {},

        initialize: function () {

            if ('serviceWorker' in navigator) {
                navigator.serviceWorker.register('/clipone/test.js')
                        .then(function (reg) {
                            // registration worked
                        }).catch(function (error) {
                    // registration failed
                });
            }
            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_service_date_start');
            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_service_date_end');

            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_dob_start');
            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_dob_end');

            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_creation_date_start');
            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_creation_date_end');

            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_paid_date_start');
            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_paid_date_end');

            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_batch_date_start');
            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_batch_date_end');

            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_admit_date_start');
            pastClaims.form.setupDatePicker(pastClaims.form.prefix + '_admit_date_end');

            pastClaims.form.setupRefDocAutocomplete('referring_doctor');

            $('#' + pastClaims.form.prefix + '_patient_id').keyup(function () {
                pastClaims.form.togglePatientDetails();
            });
            $('#' + pastClaims.form.prefix + '_claim_id').keyup(function () {
                pastClaims.form.toggleClaimDetails();
            });
            $('#' + pastClaims.form.prefix + '_moh_claim').keyup(function () {
                pastClaims.form.toggleMohDetails();
            });

            $('#clear_button').button();
            $('#clear_button').click(function () {
                pastClaims.form.clearForm();
            });

            $('#find_button').button();
            $('#find_button').click(function () {
                return pastClaims.handleFindClick();
            });
            $('#csvButton').click(function () {
                return pastClaims.csv.openDialog();
            });
            $('#printButton').button();
            $('#printButton').click(function () {
                $('#past_claims_results_table_form').prepend($('#past_claims_totals'));

                $("#past_claims_results_table_form").printElement({
                    pageTitle: 'billingCyclePrintOut' + new Date().getTime() + '.pdf',
                    overrideElementCSS: ['css/jquery-tablesorter1.css', 'css/jquery-tablesorter/jquery.tablesorter.pager.css', 'css/jquery-tablesorter/theme.blue.css']
                });
            });

            $('#' + pastClaims.form.formID + ' div.claim_type input').click(function () {
                pastClaims.form.toggleAllCheckbox('claim_type', $(this).val());
            });

            $('#' + pastClaims.form.formID + ' div.claim_status input').click(function () {
                pastClaims.form.toggleAllCheckbox('claim_status', $(this).val());
            });

            $('#' + pastClaims.form.formID + ' div.payment_status input').click(function () {
                pastClaims.form.toggleAllCheckbox('payment_status', $(this).val());
            });

            $('#search input[type=button]').button();
            $('#unsubmitted_search').click(function () {
                pastClaims.form.quickSearchClick('unsubmitted');
            });
            $('#submitted_search').click(function () {
                pastClaims.form.quickSearchClick('submitted');
            });
            $('#rejected_search').click(function () {
                pastClaims.form.quickSearchClick('rejected');
            });
            $('#todays_claim_search').click(function () {
                pastClaims.form.quickSearchClick('today');
            });
            $('#todays_billed_claim_search').click(function () {
                pastClaims.form.quickSearchClick('today_billed');
            });
            $('#unresolved_search').click(function () {

                var defaultData = JSON.parse($("#DefaultCols").val());

                var serivce_start = $("#past_claims_service_date_start").val();
                var serivce_end = $("#past_claims_service_date_end").val();


                if ($("#past_claims_service_date_start").val() == '' && $("#past_claims_service_date_end").val() == '') {
                    $("#past_claims_service_date_start").skDP('update', new Date(new Date().setMonth(new Date().getMonth() - 6)));

                    $("#past_claims_service_date_end").skDP('update', new Date(new Date().setDate(new Date().getDate() - (7 * 6))));
                } else if (serivce_start == defaultData.service_date_start && serivce_end == defaultData.service_date_end) {
                    $("#past_claims_service_date_start").skDP('update', new Date(new Date().setMonth(new Date().getMonth() - 6)));

                    $("#past_claims_service_date_end").skDP('update', new Date(new Date().setDate(new Date().getDate() - (7 * 6))));

                }

                pastClaims.form.quickSearchClick('unresolved');
            });
            $('#resolved_search').click(function () {
                pastClaims.form.quickSearchClick('resolved');
            });

            //Quick columns
            $('#admit_search').click(function () {
                pastClaims.form.applyChanges('admit_date');
            });

            $('#fee_search').click(function () {
                pastClaims.form.applyChanges('fee');
            });

            $('#errors_search').click(function () {
                pastClaims.form.applyChanges('errors');
            });

            $('#basic_md').click(function () {
                pastClaims.form.applyChanges('basic_md');
            });

            $('#my_billing').click(function () {
                pastClaims.form.applyChanges('my_billing');
            });

            $('#billing').click(function () {
                pastClaims.form.applyChanges('billing');
            });

            $('#basic_track').click(function () {
                pastClaims.form.applyChanges('basic_track');
            });


            $('#previous_search').click(function () {
                $.ajax({
                    type: 'json',
                    url: pastClaims.actions.prevSearch,
                    method: 'GET',
                    success: function (rs) {
                        alert(rs);
                    }
                });
            });

            $('#password').keypress(function (e) {
                if (e.which == '13') {
                    $('.administratorlogin').trigger('click');
                }
            });

            pastClaims.form.toggleAllCheckbox('claim_status');
            pastClaims.form.toggleAllCheckbox('claim_type');
            pastClaims.form.toggleAllCheckbox('payment_status');

        },

        clearErrors: function () {
            $('label.error').remove();
            $('.error').removeClass('error');
            $('#' + pastClaims.form.errorDiv).text('').hide();
        },
        clearForm: function () {
            $('#' + pastClaims.form.formID + ' input[type=text]').val('');

            // pastClaims.form.toggleAllCheckbox('claim_type', 'all');
            pastClaims.form.toggleAllCheckbox('claim_status', 'all');
            pastClaims.form.toggleAllCheckbox('payment_status', 'all');
            pastClaims.form.togglePatientDetails();
            pastClaims.form.toggleClaimDetails();
            pastClaims.form.toggleMohDetails();

            var defaultCols = JSON.parse($("#DefaultCols").val());
            //console.log(defaultCols);
            // console.log(defaultCols['claim_type']);
            var notall = true;

            /*$(".claimTypeChkbox input[type='checkbox']").each(function () {
                if ($.inArray($(this).val(), defaultCols['claim_type'][0]) >= 0) {
                    notall = true;
                    $(this).prop('checked', true);
                }
            });*/
        var reportVal = ['service_code','diag_code','num_serv','errors','service_date','claim_status','fee_paid','fee_subm','doctor_code','charge_to','account_num','patient_id','patient_name','health_card_vc','sex','dob'];
            $("#mainReportSection input[type='checkbox']").each(function () {
                //alert($(this).val());                
                if ($.inArray($(this).val(), reportVal) >= 0) {
                    $("#mainReportSection input[type='checkbox'][value='" + $(this).val() + "']").prop('checked', true);
                    $(this).prop('checked', true);
                }else{
                    $(this).prop('checked', false);
                }
            });

            if (notall) {
                $("#past_claims_claim_status_all").prop('checked', false).trigger('change')
            }
            var notall = false;
            $(".claimStatusChkbox input[type='checkbox']").each(function () {
                if ($.inArray($(this).val(), defaultCols['claim_status']) >= 0) {
                    notall = true;
                    $(".claimStatusChkbox2 input[type='checkbox'][value='" + $(this).val() + "']").prop('checked', true);
                    $(this).prop('checked', true);
                }
            });
            if (notall) {
                $("#past_claims_claim_type_all").prop('checked', false).trigger('change');
            }
            var notall = false;
            $(".claimTypeChkbox input[type='checkbox']").each(function () {
                if ($.inArray($(this).val(), defaultCols['claim_type']) >= 0) {
                    notall = true;
                    $(".claim_type_div2 input[type='checkbox'][value='" + $(this).val() + "']").prop('checked', false).trigger('click');
                    $(this).prop('checked', false).trigger('click');
                }else{
                    $(".claim_type_div2 input[type='checkbox'][value='" + $(this).val() + "']").prop('checked', true).trigger('click');
                    $(this).prop('checked', true).trigger('click');
                }
            });


            if (defaultCols['creation_date_end'] != '') {
                $("#past_claims_creation_date_end").skDP('setDate', new Date(global_js.getFormattedDate(defaultCols['creation_date_end'])));
            }
            if (defaultCols['creation_date_start'] != '') {
                $("#past_claims_creation_date_start").skDP('setDate', new Date(global_js.getFormattedDate(defaultCols['creation_date_start'])));
            }
            if (defaultCols['pdf_option_header'] != '') {
                $("#reportHeader").val(defaultCols['pdf_option_header']);
            }
            if (defaultCols['pdf_option_header'] != '') {
                $("#reportHeader").val(defaultCols['pdf_option_header']);
            }
            if (defaultCols['service_date_start'] != '') {
                $("#past_claims_service_date_start").skDP('setDate', new Date(global_js.getFormattedDate(defaultCols['service_date_start'])));
            }
            if (defaultCols['service_date_end'] != '') {
                $("#past_claims_service_date_end").skDP('setDate', new Date(global_js.getFormattedDate(defaultCols['service_date_end'])));
            }

            $("#past_claims_report_columns_patient_id").trigger('change');
        },
        applyChanges: function (value) {
            if (value == 'errors') {
                $('.reportColoumnsChkbox').prop('checked', false);
                $('input[value="doctor_code"]').prop('checked', true);
                $('input[value="charge_to"]').prop('checked', true);
                $('input[value="patient_number"]').prop('checked', true);
                $('input[value="patient_name"]').prop('checked', true);
                $('input[value="health_card_vc"]').prop('checked', true);
                $('input[value="dob"]').prop('checked', true);
                $('input[value="sex"]').prop('checked', true);
                $('input[value="service_code"]').prop('checked', true);
                $('input[value="diag_code"]').prop('checked', true);
                $('input[value="num_serv"]').prop('checked', true);
                $('input[value="fee_subm"]').prop('checked', true);
                $('input[value="fee_paid"]').prop('checked', true);
                $('input[value="service_date"]').prop('checked', true);
                $('input[value="errors"]').prop('checked', true);
            } else if (value == 'fee') {
                $('.reportColoumnsChkbox').prop('checked', false);

                $('input[value="doctor_code"]').prop('checked', true);
                $('input[value="charge_to"]').prop('checked', true);
                $('input[value="patient_number"]').prop('checked', true);
                $('input[value="patient_name"]').prop('checked', true);
                $('input[value="health_card_vc"]').prop('checked', true);
                $('input[value="dob"]').prop('checked', true);
                $('input[value="sex"]').prop('checked', true);
                $('input[value="service_code"]').prop('checked', true);
                $('input[value="diag_code"]').prop('checked', true);
                $('input[value="num_serv"]').prop('checked', true);
                $('input[value="fee_subm"]').prop('checked', true);
                $('input[value="service_date"]').prop('checked', true);
            } else if (value == 'admit_date') {
                $('.reportColoumnsChkbox').prop('checked', false);

                $('input[value="doctor_code"]').prop('checked', true);
                $('input[value="charge_to"]').prop('checked', true);
                $('input[value="patient_number"]').prop('checked', true);
                $('input[value="patient_name"]').prop('checked', true);
                $('input[value="health_card_vc"]').prop('checked', true);
                $('input[value="dob"]').prop('checked', true);
                $('input[value="sex"]').prop('checked', true);
                $('input[value="admit_date"]').prop('checked', true);
                $('input[value="facility_num"]').prop('checked', true);
                $('input[value="service_code"]').prop('checked', true);
                $('input[value="diag_code"]').prop('checked', true);
                $('input[value="num_serv"]').prop('checked', true);
                $('input[value="fee_subm"]').prop('checked', true);
                $('input[value="service_date"]').prop('checked', true);
                $('input[value="errors"]').prop('checked', true);
            } else if (value == 'basic_md') {
                $('.reportColoumnsChkbox').prop('checked', false);

                $('input[value="provider_number"]').prop('checked', true);
                $('input[value="group_num"]').prop('checked', true);
                $('input[value="account_num"]').prop('checked', true);
                $('input[value="charge_to"]').prop('checked', true);
                $('input[value="patient_name"]').prop('checked', true);
                $('input[value="health_card_vc"]').prop('checked', true);
                $('input[value="admit_date"]').prop('checked', true);
                $('input[value="claim_status"]').prop('checked', true);
                $('input[value="service_code"]').prop('checked', true);
                $('input[value="diag_code"]').prop('checked', true);
                $('input[value="num_serv"]').prop('checked', true);
                $('input[value="fee_subm"]').prop('checked', true);
                $('input[value="service_date"]').prop('checked', true);
                $('input[value="fee_paid"]').prop('checked', true);

            } else if (value == 'my_billing') {
                $('.reportColoumnsChkbox').prop('checked', false);

                $('input[value="doctor_code"]').prop('checked', true);
                $('input[value="account_num"]').prop('checked', true);
                $('input[value="charge_to"]').prop('checked', true);
                $('input[value="health_card_vc"]').prop('checked', true);
                $('input[value="patient_name"]').prop('checked', true);
                $('input[value="service_code"]').prop('checked', true);
                $('input[value="diag_code"]').prop('checked', true);
                $('input[value="num_serv"]').prop('checked', true);
                $('input[value="fee_subm"]').prop('checked', true);
                $('input[value="fee_paid"]').prop('checked', true);
                $('input[value="service_date"]').prop('checked', true);
                $('input[value="errors"]').prop('checked', true);

            } else if (value == 'billing') {
                $('.reportColoumnsChkbox').prop('checked', false);

                $('input[value="doctor_code"]').prop('checked', true);
                $('input[value="account_num"]').prop('checked', true);
                $('input[value="moh_claim"]').prop('checked', true);
                $('input[value="charge_to"]').prop('checked', true);
                $('input[value="health_card_vc"]').prop('checked', true);
                $('input[value="admit_date"]').prop('checked', true);
                $('input[value="claim_status_code"]').prop('checked', true);

                $('input[value="service_code"]').prop('checked', true);
                $('input[value="diag_code"]').prop('checked', true);
                $('input[value="num_serv"]').prop('checked', true);
                $('input[value="fee_subm"]').prop('checked', true);
                $('input[value="service_date"]').prop('checked', true);
                $('input[value="fee_paid"]').prop('checked', true);
                $('input[value="errors"]').prop('checked', true);
            } else if (value == 'basic_track') {
                $('.reportColoumnsChkbox').prop('checked', false);


                $('input[value="account_num"]').prop('checked', true);
                $('input[value="moh_claim"]').prop('checked', true);
                $('input[value="charge_to"]').prop('checked', true);
                $('input[value="batch_num"]').prop('checked', true);
                $('input[value="health_card_vc"]').prop('checked', true);


                $('input[value="claim_status"]').prop('checked', true);
                $('input[value="service_code"]').prop('checked', true);
                $('input[value="diag_code"]').prop('checked', true);

                $('input[value="created_user_id"]').prop('checked', true);
                $('input[value="updated_user_id"]').prop('checked', true);

                $('input[value="reconcile"]').prop('checked', true);
                $('input[value="error_reconcile"]').prop('checked', true);


                $('input[value="errors"]').prop('checked', true);
            }

        },
        quickSearchClick: function (value) {
            if (value == 'today') {
                var today = new Date();
                today = today.toString(quickClaim.getParseDateFormat(global_js.getNewDateFormat()));

                $('#past_claims_service_date_start').val(today);
                $('#past_claims_service_date_end').val(today);
                $('#past_claims_creation_date_start').val('');
                $('#past_claims_creation_date_end').val('');

                $('.claim_type input:checked').prop('checked', false);
                $('.claim_status input:checked').prop('checked', false);
                pastClaims.form.toggleAllCheckbox('claim_type');
                pastClaims.form.toggleAllCheckbox('claim_status');
            } else if (value == 'today_billed') {
                var today = new Date();
                today = today.toString(quickClaim.getParseDateFormat(global_js.getNewDateFormat()));

                $('#past_claims_service_date_start').val('');
                $('#past_claims_service_date_end').val('');
                $('#past_claims_creation_date_start').val(today);
                $('#past_claims_creation_date_end').val(today);

                $('.claim_type input:checked').prop('checked', false);
                $('.claim_status input:checked').prop('checked', false);
                pastClaims.form.toggleAllCheckbox('claim_type');
                pastClaims.form.toggleAllCheckbox('claim_status');
            } else if (value == 'resolved') {
                $('.claim_status input').prop('checked', false);
                $('.payment_status input').prop('checked', false);

                $('#' + pastClaims.form.prefix + '_payment_status_partial_paid').prop('checked', true);
                $('#' + pastClaims.form.prefix + '_payment_status_fully_paid').prop('checked', true);

                $('#' + pastClaims.form.prefix + '_claim_status_rejected_closed').prop('checked', true);
                $('#' + pastClaims.form.prefix + '_claim_status_paid').prop('checked', true);
                $('#' + pastClaims.form.prefix + '_claim_status_paid_by_user').prop('checked', true);
            } else if (value == 'unresolved') {
                $('.claim_status input').prop('checked', false);
                $('.payment_status input').prop('checked', false);

                $('#' + pastClaims.form.prefix + '_payment_status_unpaid').prop('checked', true).trigger('change');
                $('#' + pastClaims.form.prefix + '_payment_status_partial_paid').prop('checked', true).trigger('change');

                $('#' + pastClaims.form.prefix + '_claim_status_unsubmitted').prop('checked', true).trigger('change');
                $('#' + pastClaims.form.prefix + '_claim_status_submitted').prop('checked', true).trigger('change');
                $('#' + pastClaims.form.prefix + '_claim_status_rejected').prop('checked', true).trigger('change');

            } else {
                $('.claim_status input').prop('checked', false);
                $('#' + pastClaims.form.prefix + '_claim_status_' + value).prop('checked', true).trigger('change');
            }
            $('#find_button').click();
        },
        setupDatePicker: function (id) {

            sk.datepicker('#' + id);

        },
        setupRefDocAutocomplete: function (prefix) {
            var doctor = $('#' + pastClaims.form.prefix + '_' + prefix);
            var description = $('#' + pastClaims.form.prefix + '_' + prefix + '_description');

            if (doctor.size() && description.size()) {
                description.autocomplete({
                    minLength: 2,
                    select: function (event, ui) {
                        doctor.val(ui.item.provider_num);
                        description.val(ui.item.label);
                    },
                    source: function (request, response) {
                        if (request.term in pastClaims.form.refDocCache) {
                            return response(pastClaims.form.refDocCache[request.term]);
                        }

                        $.ajax({
                            url: pastClaims.actions.refdocAutocomplete,
                            dataType: 'json',
                            data: request,
                            success: function (data) {
                                pastClaims.form.refDocCache[request.term] = data;
                                return response(data);
                            }
                        });
                    }
                });
            }
        },
        toggleAllCheckbox: function (type, value) {
            var count = $('#' + pastClaims.form.formID + ' div.' + type + ' input:checked').length;
            if (type == 'claim_type' || type == 'claim_status') {
                count--;
            }

            if (value == 'all') {
                $('#' + pastClaims.form.formID + ' div.' + type + ' input:checked').each(function () {
                    if ($(this).val() != 'all') {
                        // $(this).prop('checked', false);
                        $(this).prop('checked', true).trigger('click');
                    } else {
                        $(this).prop('checked', (count > 0));
                    }
                });
            } else if (count == 0) {
                $('#' + pastClaims.form.formID + ' div.' + type + ' input[value=all]').prop('checked', true);
            } else {
                $('#' + pastClaims.form.formID + ' div.' + type + ' input[value=all]').prop('checked', false);
            }

            var count = $('#' + pastClaims.form.formID + ' div.' + type + ' input:checked').size();
            if (count == 0) {
                $('#' + pastClaims.form.formID + ' div.' + type + ' input[value=all]').prop('checked', true);
            }

            if (type == 'claim_type') {
                pastClaims.form.toggleThirdPartyDirectRow();
            }
        },
        toggleClaimDetails: function () {
            var value = jQuery.trim($('#' + pastClaims.form.prefix + '_claim_id').val());
            var disabled = value ? true : false;

            $('#' + pastClaims.form.prefix + '_claim_location_id').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_account_number').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_service_date_start').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_service_date_end').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_creation_date_start').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_creation_date_end').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_service_code').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_diagnostic_code').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_doctor_id').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_group_number').prop('disabled', disabled);
            $('#past_claims_admit_date_start').prop('disabled', disabled);
            $('#past_claims_admit_date_end').prop('disabled', disabled);

        },
        toggleMohDetails: function () {
            var value = jQuery.trim($('#' + pastClaims.form.prefix + '_moh_claim').val());
            var disabled = value ? true : false;


            $('#' + pastClaims.form.prefix + '_expl_code').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_ra').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_batch_id').prop('disabled', disabled);
            
        },
        togglePatientDetails: function () {
            var value = jQuery.trim($('#' + pastClaims.form.prefix + '_patient_id').val());
            var disabled = value ? true : false;

            $('#' + pastClaims.form.prefix + '_fname').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_lname').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_patient_location_id').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_sex').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_dob_start').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_dob_end').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_health_num').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_referring_doctor_description').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_province').prop('disabled', disabled);
            $('#' + pastClaims.form.prefix + '_region_id').prop('disabled', disabled);
        },
        toggleThirdPartyDirectRow: function () {
            var direct_checked = $('#' + pastClaims.form.formID + ' div.claim_type input[value=direct]:checked').size();
            var third_party_checked = $('#' + pastClaims.form.formID + ' div.claim_type input[value=third_party]:checked').size();
            var all_checked = $('#' + pastClaims.form.formID + ' div.claim_type input[value=all]:checked').size();

            if (direct_checked || all_checked || third_party_checked) {
                $('.third_party_direct_row').show();
                if (direct_checked || all_checked) {
                    $('.third_party_direct_row .direct').show();
                } else {
                    $('.third_party_direct_row .direct').hide();
                }

                if (third_party_checked || all_checked) {
                    $('.third_party_direct_row .third_party').show();
                } else {
                    $('.third_party_direct_row .third_party').hide();
                }
            } else {
                $('.third_party_direct_row').hide();
            }
        }
    },
    pdf: {
        initialize: function () {
            $('#pdfOptionsDialog').dialog({
                closeOnEscape: true,
                autoOpen: false,
                height: $(window).height() - 200,
                modal: true,
                width: $(window).width() - 200
            });

            $('#downloadPDF').button();
            $('#downloadPDF').click(function () {
                pastClaims.pdf.handlePdfClick();
            });

            $('#downloadMPDF').button();
            $('#downloadMPDF').click(function () {


                $('#new_window_parameter_1').val($('body').html());
                $('#invisible_form').submit();
                document.forms.invisible_form_pdf.submit();


            });
        },

        downloadComplete: function () {
            if ($('#hiddenDownloader').contents().find('body').text()) {
                $('#messages .icons div').hide();
                $('#' + pastClaims.messages.error).show();
                $('#' + pastClaims.messages.text).html('<div class="alert alert-info">An error occurred while generating the PDF File</div>').show();
            } else {
                $('#messages .icons div').hide();
                $('#' + pastClaims.messages.checkmark).show();
                $('#' + pastClaims.messages.text).html('<div class="alert alert-info">Generated the PDF File</div>').show();
            }
            $('#pdfOptionsDialog').dialog('close');
            quickClaim.hideOverlayMessage();
        },
        handlePdfClick: function () {
            hypeFileDownload.tokenName = "pastClaimsPDFToken";
            hypeFileDownload.setReturnFunction(pastClaims.pdf.downloadComplete);

            var url = pastClaims.actions.search + '?'
                    + $('#' + pastClaims.form.formID).serialize()
                    + '&' + $('#pdfForm').serialize()
                    + '&footerText=' + $('#PDFFooterText').text()
                    + '&mode=pdf'
                    + '&pastClaimsPDFToken=' + hypeFileDownload.blockResubmit();

            quickClaim.showOverlayMessage('Generating PDF');
            $('#hiddenDownloader').remove();
            $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');

        },
        openDialog: function () {
            $('#PDFFooterText').text($('#results-table .pastClaims .pagedisplay').text());
            $('#pdfOptionsDialog').dialog('open');
        }
    },
    refine: {
        initialize: function () {

            $('#refine_search_claim_type').change(function () {
                pastClaims.refine.handleRefineSearchEntry('claim_type', $(this).val());
            });
            $('#refine_search_doctor_id').change(function () {
                pastClaims.refine.handleRefineSearchEntry('doctor_id', $(this).val());
            });
            $('#refine_search_claim_id').keypress(function (e) {
                if (e.which == 13) {
                    pastClaims.refine.handleRefineSearchEntry('claim_id', $(this).val());
                }
            });
            $('#clear_refine_terms').button();
            $('#clear_refine_terms').click(function () {
                $('#refine_search input:text, #refine_search select').val('');
                $('#' + pastClaims.form.prefix + '_claim_id').val('');
                $('#' + pastClaims.form.prefix + '_doctor_id').val('');
                pastClaims.form.toggleAllCheckbox('claim_type', 'all');
            });
        },
        handleRefineSearchEntry: function (type, value) {
            if (type == 'claim_type') {
                $('.claim_type input').prop('checked', false);
                if (value) {
                    $('#' + pastClaims.form.prefix + '_claim_type_' + value).prop('checked', true);
                } else {
                    pastClaims.form.toggleAllCheckbox('claim_type');
                }
            } else {
                $('#' + pastClaims.form.prefix + '_' + type).val(value);
            }
            $('#find_button').click();
            $('#' + pastClaims.resultsTable + ' tbody tr').remove();
            //$('#' + pastClaims.resultsTable + ' tfoot tr').remove();
        }
    },
    facility: {
        actions: {
            lookup: null
        },
        cache: [],
        data: {},
        ajaxLock: false,
        initialize: function () {
            for (var a in pastClaims.facility.data) {
                pastClaims.facility.cache[pastClaims.facility.cache.length] = pastClaims.facility.data[a].toString;
            }


            var $description = $('#facility_description');

            $description.autocomplete({
                minLength: 1,
                select: function (event, ui) {
                    var facilityNum = ui.item.label.substring(0, 4);
                    pastClaims.facility.fillInFacility(facilityNum);
                    event.stopPropagation();
                },
                source: pastClaims.facility.cache
            });

            $description.blur(function () {
                var facilityNum = $(this).val().substring(0, 4);
                pastClaims.facility.fillInFacility(facilityNum);
            });
            
        },

        fillInFacility: function (facilityNum) {

            if (pastClaims.facility.data.hasOwnProperty(facilityNum)) {
                var d = pastClaims.facility.data[facilityNum];
                $('#doctor_moh_facility_num').val(d.facility_num);
                $('#facility_description').val(d.toString);
                $('#facility_id').val(d.facility_id);
            } else {
                pastClaims.facility.lookup(facilityNum);
            }
        },
        lookup: function (facilityNum) {
            if (pastClaims.facility.ajaxLock) {
                return false;
            }

            var data = 'facilityNum=' + facilityNum;

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: providers_edit.facility.actions.lookup,
                data: data,
                success: function (rs) {

                    if (rs.hasOwnProperty('data') && rs.data.hasOwnProperty('0')) {
                        pastClaims.facility.data[rs.data[0].facility_num] = rs.data[0];
                        pastClaims.facility.fillInFacility(rs.data[0].facility_num);
                    } else {
                        $('#doctor_moh_facility_num').val($('#facility_description').val());
                    }
                },
                beforeSend: function (rs) {
                    pastClaims.facility.ajaxLock = true;
                },
                complete: function (rs) {
                    pastClaims.facility.ajaxLock = false;
                },
                error: function (rs) {
                }
            });
        }
    },
    results: {
        menuHtml: null,
        columns: new Array(),
        counts: {
            edit: 0,
            markClosed: 0,
            markPaid: 0,
            resubmit: 0,
            submit: 0
        },
        initialized: false,
        pageNumber: 0,
        paginationSize: 25,
        paginationSort: [],
        rowPrefix: 'claimItemRow',
        showTotals: false,
        showRebill: false,
        sortOrder: {},
        subtotals: {},
        totals: {},
        graphstotals: {},

        initializePager: function () {
            $('#' + pastClaims.resultsTable + ' thead th:last').append(quickClaim.tablesorterTooltip);
            var headers = {},
                    $table = $('#' + pastClaims.resultsTable), i,
                    l = $table.find('thead th').length,
                    // choose the columns you want to sort (zero-based index)
                    sortcolumns = [];

// build headers object; based on sortcolumn selections
            for (i = 0; i < l; i++) {
                // if ($.inArray(i, sortcolumns) < 0) {
                headers[i] = {sorter: false}
                // }
            }

            $('#' + pastClaims.resultsTable).tablesorter({
                theme: 'blue',
                serverSideSorting: false,
                headerTemplate: '{content} {icon}',
                //widgets: ['selectRow', 'zebra'],

                headers: headers,
                sortList: pastClaims.results.paginationSort,
                dateFormat: global_js.getNewDateFormat(),
                debug: false
            });
            if (isNaN(pastClaims.results.paginationSize)) {
                pastClaims.results.paginationSize = 1;
            }
            $('#' + pastClaims.resultsTable).tablesorterPager({
                container: $('.pastClaimsPager'),
                size: pastClaims.results.paginationSize,
                cssPageSize: '.pagesize',
                removeRows: false,
                ajaxUrl: pastClaims.actions.search + '?{sortList:sort}&pageNumber={page}&size={size}&{filterList:fcol}',
                customAjaxUrl: function (table, url) {
                    return url + '&' + $('#' + pastClaims.form.formID).serialize();
                },
                ajaxObject: {
                    dataType: 'json',
                    complete: function () {

                        $('#past_claims_results_table_form .sortTip').tooltip();

                        quickClaim.hideOverlayMessage();
                        $('#resultsCheckAll').click(function () {
                            var checked = $(this).prop('checked');
                            $('#' + pastClaims.resultsTable + ' input[type=checkbox]').prop('checked', checked);
                        });

                        pastClaims.results.buildErrorTooltips();
                        pastClaims.results.buildActionButtons();


                        if ($('#edit_checkbox_button')) {
                            $('.actions_button').each(function () {
                                contextMenu.createButton($(this).prop('id'));
                            });
                        }



                        quickClaim.ajaxLocked = false;
                        $('#' + pastClaims.messages.spinner).hide();
                        $(window).trigger('resize');

                        var columnsData = [];

                        columnsData.push({"width": "10px"});

                        var newColumnsOrder = $('select[name="past_claims[report_columns][]"]').val();

                        for (var i = 0; i < newColumnsOrder.length; i++) {

                            if (newColumnsOrder[i]) {

                                var text = newColumnsOrder[i];
                                var title = pastClaims.getColumnTitle(text);
                                columnsData.push({title: title});
                            }
                        }
                        columnsData.push({"width": "10px"});

                        var table = $(".DataTablePastClaimResult").DataTable({
                            "paging": true,
                            "lengthMenu": [10, 25, 50, 100, 200, 250, 500],
                            "lengthChange": true,
                            "searching": true,
                            "ordering": true,
                            "order": [[1, "asc"]],
                            "info": true,
                            "autoWidth": false,
                            "responsive": false,
                            "scrollX": true,
                            "scrollY": true,
                            "dom": 'Bfrtip',
                            "buttons": [
                                {
                                    "extend": 'pdf',
                                    "text": '<i class="clip-file-pdf"></i> PDF',
                                    "className": 'btn btn-danger btn-squared',
                                    extend: 'pdfHtml5',
                                    orientation: 'landscape',
                                    pageSize: 'LEGAL'
                                },
                                {
                                    "extend": 'excel',
                                    "text": '<i class="clip-file-excel"></i> Excel',
                                    "className": 'btn btn-info btn-squared'
                                },
                                {
                                    "extend": 'print',
                                    "text": '<i class="fa-print fa"></i> Print',
                                    "className": 'btn btn-blue btn-squared',
                                    orientation: 'landscape',
                                    pageSize: 'LEGAL'
                                },
                                {
                                    "extend": 'copy',
                                    "text": '<i class="clip-share"></i> Share',
                                    "className": 'btn btn-primary btn-squared'
                                },
                            ],
                            "oLanguage": {
                                "sSearch": "",
                                "sLengthMenu": "Per page _MENU_",
                                "oPaginate": {
                                    "sNext": "<i class='clip-chevron-right'></i>",
                                    "sPrevious": "<i class='clip-chevron-left'></i>"
                                },
                                "sInfo": "_START_ to _END_ rows out of _TOTAL_",

                            },
                            "columns": columnsData,
                            "columnDefs": [
                                {"targets": 0, "orderable": false}
                            ],
//                            "language": {
//                                "oSearch": "",
//                                "lengthMenu": "Per page _MENU_",
////                                "zeroRecords": "No report found.",
////                                "info": "Showing page _PAGE_ of _PAGES_",
////                                "infoEmpty": "No records available",
////                                "infoFiltered": "(filtered from _MAX_ total records)"
//                            },

                            "sDom": '<"wrapper"<"row"<"col-sm-12" <"col-sm-6 actionButtonsDiv"> <"col-sm-6" Bl > <"col-sm-12" <"row well customWell" <"col-sm-3 customSearchInput" f> <"col-sm-3 customSearchField">  <"col-sm-6 CustomPagination"ip> > > >  >rt<"clear">>',
// #D2EDF7                           "sDom": '<"wrapper"<"row"<"col-sm-12" <"row" <"col-sm-7" <"col-sm-6 small"l><"col-sm-6"f<"DTsearchlabel">><"col-sm-8"p><"col-sm-4 small"i>> <"col-sm-5 pdfTableOptions small"B>>>>rt<"clear">>',

                            initComplete: function () {

                                // $('input[type="checkbox"]').iCheck({
                                //     checkboxClass: 'icheckbox_square-aero',
                                //     increaseArea: '10%' // optional
                                // });

                                $(".entireClickable").click(function () {
                                    if ($(this).find('input').prop('checked')) {
                                        $(this).find('input').prop('checked', false);
                                        $(this).removeClass('highlight');
                                    } else {
                                        $(this).find('input').prop('checked', true);
                                        $(this).addClass('highlight');
                                    }

                                })
                                var inputHtml = '<div class="input-group">' +
                                        '<input type="text" placeholder="Contains..." class="form-control DatatableAllRounderSearch" />' +
                                        '<span class="input-group-addon cursorPointer btnClearSearchTxt"> ' +
                                        '<i class="clip-cancel-circle-2 "></i>' +
                                        '</span>' +
                                        '<span class="input-group-addon cursorPointer"> ' +
                                        '<i class="clip-search-2"></i>' +
                                        '</span>' +
                                        '</div>';

                                $(".customSearchInput").html(inputHtml);

                                var inputHtml2 = '<div class="input-group">' +
                                        '<select class="newDrProfile DatatableAllRounderSearch"></select>' +
                                        '<span class="input-group-addon cursorPointer resetDrProfile"> ' +
                                        '<i class="clip-cancel-circle-2 "></i>' +
                                        '</span>' +
                                        '</div>';
                                $(".customSearchField").html(inputHtml2);
                                $(".newDrProfile").html($("#past_claims_doctor_id").html());
                                $(".newDrProfile option:eq(0)").text('Change Dr. Profile');
                                $(".drSelectMainPopular").select2('destroy');
                                var currentDrProfile = $(".drSelectMainPopular").val();
                                $(".drSelectMainPopular").select2();
                                $(".newDrProfile option[value='" + currentDrProfile + "']").prop('selected', true);
                                $(".newDrProfile").select2({width: '100%'});

                                $('tfoot th').each(function () {
                                    if ($(this).attr('style')) {
                                    } else {
                                        $(this).attr('style', 'padding:5px 2px;');
                                    }

                                })
                                $('td').each(function () {
                                    if ($(this).attr('style')) {
                                    } else {
                                        $(this).attr('style', 'padding:5px 2px;');
                                    }

                                })

                                var searchoptions = $("#" + pastClaims.resultsTable + " thead tr:eq(0) th");
                                var customfilterinputs = '<tr>';
                                for (var j = 0; j < searchoptions.length; j++) {
                                    customfilterinputs += '<th></th>';
                                }
                                customfilterinputs += '</tr>';
                                $("#" + pastClaims.resultsTable + " thead").append(customfilterinputs);

//                this.api().row(':eq(0)').every(function(){
//                    var column = this;
//                    var select = $('<select class="selectRsltTbl"><option value=""></option></select>')
//                    .appendTo( /*$("#past_claims_results_table thead tr")*/ column)
//                    .on( 'change', function () {
//                        var val = $.fn.dataTable.util.escapeRegex(
//                            $(this).val()
//                        );
// 
//                        column
//                            .search( val ? '^'+val+'$' : '', true, false )
//                            .draw();
//                    } );
//                })


                                var aa = 0;
                                this.api().columns().every(function () {
                                    var column = this;
                                     var columnText = $.trim($(column.header())[0].innerText);
                                    if ($(column.header())[0].column != 0) {
                                        var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                                .appendTo($("#past_claims_results_table thead tr:eq(1) th:eq(" + aa + ")")/*$("#past_claims_results_table thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                                                .on('change', function () {
                                                    var val = $.fn.dataTable.util.escapeRegex(
                                                            $(this).val()
                                                            );
                                                    val =  val.replace(/\\/gi, "");
                                                    column
                                                            .search(val ? '^' + val + '$' : '', true, false)
                                                            .draw();
                                                });

                                        column.data().unique().sort().each(function (d, j) {
                                            if(d != '') {
                                            select.append('<option value="' + d + '">' + d + '</option>');
                                            }
                                        });
                                    }
                                    aa++;
                                });


                                // print button append
//                                $('<button type="button" onclick="pastClaims.pdf.openDialog()" name="pdfButton"  class="btn btn-danger btn-squared" ><i class="clip-file-pdf"></i> PDF</button><button type="button" onclick="pastClaims.csv.openDialog()" name="csvButton" class="btn btn-info btn-squared" style="margin-left:3px"><i class="clip-file-excel"></i> CSV</button><button type="button" id="csvButton" name="pdfButton" class="btn btn-primary btn-squared" style="margin-left:3px"><i class="clip-share"></i> Share</button>').appendTo(".dt-buttons");
//$('<button type="button" class="btn btn-primary btn-squared" style="margin-left:3px"><i class="clip-share"></i> Share</button>').appendTo(".dt-buttons");

                            }



                        });

                        $(".DatatableAllRounderSearch").keyup(function () {
                            table.search($(this).val(), true).draw();
                        })
                        $(".resetDrProfile").click(function () {
                            $(".newDrProfile").select2('destroy');
                            $(".newDrProfile option:eq(0)").prop('selected', true);
                            $(".newDrProfile").select2();
                            $(".newDrProfile").trigger('change');//select2('destroy');

                        })


                        $(".CHKcontainer input[type='checkbox']").change(function () {
                            if ($('.CHKcontainer input[type="checkbox"]:checked').length > 0) {
                                $(".actionButtonsDiv a").attr('disabled', false);
                            } else {    

                                $(".actionButtonsDiv a").attr('disabled', true);
                            }
                        });

                        $(".btnClearSearchTxt").click(function () {
                            $(".DatatableAllRounderSearch").val('');
                            table.search('', true).draw();
                        })
                        $(".otherSearchFilter").change(function () {
                            var data = $(this).select2({placeholder: "Claim Type", width: '100%'}).val();

                            if (data && data.length > 0) {
                                var d2 = '';
                                for (var i in data) {
                                    data[i] = data[i].substring(1, data[i].length);
                                    d2 += data[i] + '|';
                                }
                                d2 = d2.substring(0, d2.length - 1);
                                $(".DataTablePastClaimResult").DataTable().columns('.charge_to')
                                        .search(d2, true)
                                        .draw();
                            } else {
                                table
                                        .columns('.charge_to')
                                        .search('')
                                        .draw();
                            }
                        })

                        $(".newDrProfile").change(function () {
                            var data = $(this).select2('data');
                            var aa = $(".DataTablePastClaimResult").DataTable().columns('.doctor').data();
                            if (aa.length) {
                                if (data) {
                                    data = data.text;
                                    $(".DataTablePastClaimResult").DataTable().columns('.doctor')
                                            .search(data)
                                            .draw();
                                } else {
                                    table
                                            .columns('.doctor')
                                            .search('')
                                            .draw();
                                }
                            } else {
                                if (data) {
                                    data = data.text;
                                    var newoptionval = $(".newDrProfile option:contains(" + data + ")").val();
                                    $(".drSelectMainPopular").select2("val", newoptionval).trigger('change');
                                    $("#find_button").trigger('click');
                                    $(".DataTablePastClaimResult").dataTable().fnDestroy();

                                } else {
                                    table.columns('.doctor').search('').draw();
                                }
                            }
                        })



                        $('div.dataTables_filter input').addClass('form-control');
                        $('div.dataTables_filter input').attr('placeholder', 'Search');
                        $(".DTsearchlabel").html('<i class="clip-search"></i>');
                        $('.dataTables_filter').attr('style', 'width:100%');
                        $('.dataTables_filter label').attr('style', 'width:100%');
                        $('.dataTables_length select').attr('style', 'font-size: 12px;padding: 5px 3px;background: #fff;');
                        $('div.dataTables_filter input').attr('style', 'float:right;width:98%;margin-right:5px;');
                        $(".dataTables_wrapper .dataTables_paginate .paginate_button").attr('style', 'background:#fff !important;color:#444444 !important; border-radius: 0;');


//                        table.on('responsive-display', function (e, datatable, row, showHide, update) {
//                            $('td ul').attr('style', 'width:100% !important');
//                            $('td ul').addClass('row');
//                            $('td ul li').addClass('col-sm-4');
//                            $(".dropdown-menu li").removeClass('col-sm-4');
//                        });




                        // $("#past_claims_results_table thead tr:eq(1)").slideUp();

                        // $("#past_claims_results_table thead tr").hover(function () {
                        //     $("#past_claims_results_table thead tr:eq(1)").slideDown();
                        // }, function () {
                        //     $("#past_claims_results_table thead tr:eq(1)").slideUp();
                        // });

                        // $("#past_claims_results_table thead tr").mouseover(function () {
                        //     $("#past_claims_results_table thead tr:eq(1)").slideDown();
                        // }).mouseout(function () {
                        //    $("#past_claims_results_table thead tr:eq(1)").slideUp();
                        // });
                        $(".CustomPagination").prepend('<div style="float:left"><button data-toggle="tooltip" title="Filters" type="button" class="btn btn-default filterToggle"><i class="fa fa-filter"></i></button></div>');
                        $("#past_claims_results_table thead tr:eq(1)").toggle();
                        $(".filterToggle").click(function () {
                            $("#past_claims_results_table thead tr:eq(1)").toggle();
                        })
                        $(".selectRsltTbl").select2({
                            placeholder: "Search",
                            allowClear: true,
                            dropdownAutoWidth: true,
                            width: '98%'
                        });
//                        var width_nav = $(".main-navigation").width();
//                        if (width_nav > 40) {
//                            $(".navigation-toggler").trigger('click');
//                        }
                        $('.select2-arrow').hide();
                    },
                    beforeSend: function () {
                        $(".DataTablePastClaimResult").DataTable().destroy();
                        $("#" + pastClaims.resultsTable + " thead tr:eq(1)").remove();
                        quickClaim.ajaxLocked = true;
                        //quickClaim.showOverlayMessage('Searching for claims');
                        $('#messages .icons div').hide();
                        $('#' + pastClaims.messages.spinner).show();
                        $('#' + pastClaims.messages.text).html('Searching for claims').show();
                        $('#past_claims_results_table_form input[type=button]').remove();
//                        $('#pdfButton').button('disable');
//                        $('#csvButton').button('disable');
                    },
                    error: function () {
                        $('#' + pastClaims.messages.error).show();
                        $('#' + pastClaims.messages.text).html('<div class="alert alert-info">An error occurred while contacting the server.  Please refresh the page and try again.</div>').show();
                    }
                },
                ajaxProcessing: function (rs) {
                    if (rs.hasOwnProperty('errors')) {
                        quickClaim.showFormErrors(rs.errors, '#' + pastClaims.form.errorDiv, '#' + pastClaims.form.formID, true);

                        $('#' + pastClaims.messages.error).show();
                        $('#' + pastClaims.messages.text).html('<div class="alert alert-info">The form contains invalid data.</div>').show();
                        return false;
                    }

                    if (rs.hasOwnProperty('count') && rs.count) {
//                        $('#pdfButton').button('enable');
//                        $('#csvButton').button('enable');
                    }

                    $('#tabs').tabs('enable', 1);
                    $('#tabs').tabs('option', 'active', 1);

                    $('#' + pastClaims.messages.text).html('Processing Results.').show();
                    $('#' + pastClaims.messages.checkmark).show();

                    if (rs.hasOwnProperty('data')) {
                        // $('#' + pastClaims.resultsTable + ' tfoot tr').remove();

                        pastClaims.data = {};
                        pastClaims.errors = {};
                        pastClaims.explCodes = rs.explCodes;

                        var data = rs.data;
                        var rows = '';

                        $(".skStart").html(pastClaims.results.paginationSize - 500);
                        $(".skEnd").html(pastClaims.results.paginationSize);
                        $(".skTotal").html(rs.totals.claimItems);

                        var loopcount = Math.round(rs.totals.claimItems / 500);
                        var $htmlCustomPage = '';
                        $htmlCustomPage += '<option value="">Select Range</option>';
                        if (loopcount == 0) {
                            $htmlCustomPage += '<option value="500">0 - ' + rs.totals.claimItems + '</option>';
                        } else {
                            for (var cnt = 1; cnt <= loopcount; cnt++) {
                                var end = cnt * 500;
                                var start = end - 500;
                                var endtxt = 0;
                                if (loopcount == cnt) {
                                    endtxt = rs.totals.claimItems;
                                } else {
                                    endtxt = end;
                                }

                                $htmlCustomPage += '<option value="' + end + '">' + start + ' - ' + endtxt + '</option>';
                            }
                        }


                        $(".customPageSize").html($htmlCustomPage);
                        $(".customPageSize").on('change', function () {
                            $(".DataTablePastClaimResult").dataTable().fnDestroy();
                            $("#" + pastClaims.resultsTable + " thead tr:eq(1)").remove();
                            pastClaims.results.paginationSize = $(this).val();


                        });
                        $('.customPageSize').val(pastClaims.results.paginationSize);
                        for (var a in pastClaims.results.counts) {
                            pastClaims.results.counts[a] = 0;
                        }

                        if (pastClaims.results.showTotals) {
                            pastClaims.results.subtotals = rs.subtotals;
                            pastClaims.results.totals = rs.totals;
                        }

                        if (rs.hasOwnProperty('sortOrder')) {
                            pastClaims.results.sortOrder = rs.sortOrder;

                            pastClaims.results.clearResultsNoRefresh();


                        }

                        if (rs.hasOwnProperty('filter')) {
                            var filter = rs.filter;
                        }

                        for (var a in data) {
                            var rowID = pastClaims.results.rowPrefix + '_' + data[a]['id'];

                            rows += '<tr class="entireClickable" id="' + rowID + '">';
                            rows += pastClaims.results.buildTableRow(data[a], data[a].id, filter);
                            rows += '</tr>' + "\n";
                        }

                        if (pastClaims.results.showTotals) {
                            pastClaims.results.buildTotalsRow();
                        }

                        return [rs.count, $(rows)];
                    }
                }
            });

        },
        buildActionButtons: function () {
            var buttons = '<div class="btn-group TextWhite">';

            if (pastClaims.results.canDoAction('edit')) {
                buttons += '<a id="edit_checkbox_button" name="edit_checkbox_button" class="btn btn-primary">' +
                        '<i class="clip-pencil-2"></i> Edit' +
                        '</a>';//'<button type="button" style="margin:2px;" class="btn-info btn btn-squared"></button>';
            }
            if (pastClaims.results.canDoAction('submit')) {
                buttons += '<a class="btn btn-primary" id="submit_checkbox_button" name="submit_checkbox_button" href="javascript:void(0);">' +
                        '<i class="clip-checkmark-2"></i> Submit' +
                        '</a>';
            }
            if (pastClaims.results.showRebill) {
                buttons += '<a id="rebill_checkbox_button" name="rebill_checkbox_button" class="btn btn-primary " href="javascript:void(0);">' +
                        '<i class="clip-file-check"></i> Re-bill' +
                        '</a>';
            }
            if (pastClaims.results.canDoAction('resubmit')) {
                buttons += '<a id="resubmit_checkbox_button" name="resubmit_checkbox_button" class="btn btn-primary hidden-xs" href="javascript:void(0);">' +
                        '<i class="fa-times-circle-o fa"></i> Mark as Unsubmitted' +
                        '</a>';//'<button type="button"  style="margin:2px;" class="btn-danger btn btn-squared"></button>';
            }


            if (pastClaims.results.canDoAction('mark_paid')) {
                buttons += '<a id="mark_paid_checkbox_button" name="mark_paid_checkbox_button" class="btn btn-primary" href="javascript:void(0);">' +
                        '<i class="clip-checkmark-circle-2"></i> Mark as Paid' +
                        '</a>';
            }

            // buttons += '<a id="mark_replace_doctor_checkbox_button" name="mark_replace_doctor_checkbox_button" class="btn btn-primary" href="javascript:void(0);">' +
            //             '<i class="clip-user-2"></i> Replace Doctor' +
            //             '</a>';
            if(pastClaims.can_replace_sli == true || pastClaims.can_replace_facility  == true || pastClaims.can_replace_admit_date  == true 
                || pastClaims.can_replace_location  == true || pastClaims.can_replace_doctor  == true)
            {
                buttons += '<div class="dropdown" style="float: left;"><button style="border-radius:0px !important;" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Replace Data <span class="caret"></span></button><ul class="dropdown-menu">';
                if(pastClaims.can_replace_sli == true)
                {
                  buttons += '<li><a id="mark_replace_sli_checkbox_button" name="mark_replace_sli_checkbox_button" href="javascript:void(0);">SLI</a></li>';
                } 
                if(pastClaims.can_replace_facility  == true)
                {
                 buttons += '<li><a id="mark_replace_facility_checkbox_button" name="mark_replace_facility_checkbox_button" href="javascript:void(0);"> Facility #</a></li>';
                }
                if(pastClaims.can_replace_admit_date  == true)
                {
                  buttons += '<li><a id="mark_replace_admit_date_checkbox_button" name="mark_replace_admit_date_checkbox_button" href="javascript:void(0);"> Admit Date</a></li>';
                }
                if(pastClaims.can_replace_location  == true)
                {   
                  buttons += '<li><a id="mark_replace_location_checkbox_button" name="mark_replace_location_checkbox_button" href="javascript:void(0);"> Location</a></li>';
                }
                if(pastClaims.can_replace_doctor  == true)
                {
                  buttons += '<li><a id="mark_replace_doctor_checkbox_button" name="mark_replace_doctor_checkbox_button" href="javascript:void(0);"> Replace Doctor</a></li>';
                }
                buttons += '</ul></div>';
            }

            if (pastClaims.results.canDoAction('mark_closed')) {
                buttons += '<a id="mark_closed_checkbox_button" name="mark_closed_checkbox_button" class="btn btn-primary" href="javascript:void(0);">' +
                        '<i class="clip-cancel-circle-2"></i> Close' +
                        '</a>';
            }
            buttons += '</div>';
            $('#action_buttons').empty();
            setTimeout(function () {
                $('.actionButtonsDiv').html('');
                $(buttons).appendTo($('.actionButtonsDiv'));

                //$(buttons).appendTo($('#' + pastClaims.resultsTable + '_form'));
                $('#action_buttons input[type=button]').button();

                $('#edit_checkbox_button').on('click', function () {
                    pastClaims.handleEditGroupClick(pastClaims.results.getActionClaimIDs('edit'));
                });
                $('#resubmit_checkbox_button').click(function () {
                    pastClaims.handleResubmitClick(pastClaims.results.getActionClaimItemIDs('resubmit'));
                });

                $('#submit_checkbox_button').click(function () {
                    pastClaims.handleSubmitClick(pastClaims.results.getActionClaimIDs('submit'));
                });

                $('#rebill_checkbox_button').click(function () {
                    pastClaims.handleRebillClick();
                });

                $('#mark_paid_checkbox_button').click(function () {
                    pastClaims.handleMarkPaidClick(pastClaims.results.getActionClaimItemIDs('mark_paid'));
                });

                



                $('#mark_replace_facility_checkbox_button').click(function(){
                     var claimIDs = $('#past_claims_results_table .checkboxChk:checked').map(function() {return $(this).attr('data-claim_id');}).get().join(',');
                     if(pastClaims.actions.replacedatalogin == 'false') {
                        pastClaims.handleMarkAdminLogin('mark_replace_facility_checkbox_button');
                     } else {
                        pastClaims.handleMarkFacilityReplaceClick(claimIDs);
                     }
                });

                $('#mark_replace_location_checkbox_button').click(function(){
                     var claimIDs = $('#past_claims_results_table .checkboxChk:checked').map(function() {return $(this).attr('data-claim_id');}).get().join(',');
                     if(pastClaims.actions.replacedatalogin == 'false') {
                        pastClaims.handleMarkAdminLogin('mark_replace_location_checkbox_button');
                     } else {
                        pastClaims.handleMarkLocationReplaceClick(claimIDs);
                     }
                });

                $('#mark_replace_doctor_checkbox_button').click(function(){
                     var claimIDs = $('#past_claims_results_table .checkboxChk:checked').map(function() {return $(this).attr('data-claim_id');}).get().join(',');
                     if(pastClaims.actions.replacedatalogin == 'false') {
                        pastClaims.handleMarkAdminLogin('mark_replace_doctor_checkbox_button');
                     } else {
                     pastClaims.handleMarkDoctorReplaceClick(claimIDs);
                     }
                });

                $('#mark_replace_sli_checkbox_button').click(function(){
                     var claimIDs = $('#past_claims_results_table .checkboxChk:checked').map(function() {return $(this).attr('data-claim_id');}).get().join(',');
                     if(pastClaims.actions.replacedatalogin == 'false') {
                        pastClaims.handleMarkAdminLogin('mark_replace_sli_checkbox_button');
                     } else {
                     pastClaims.handleMarkSliReplaceClick(claimIDs);
                     }  
                });

                 $('#mark_replace_admit_date_checkbox_button').click(function(){
                     var claimIDs = $('#past_claims_results_table .checkboxChk:checked').map(function() {return $(this).attr('data-claim_id');}).get().join(',');
                     if(pastClaims.actions.replacedatalogin == 'false') {
                        pastClaims.handleMarkAdminLogin('mark_replace_admit_date_checkbox_button');
                     } else {
                        pastClaims.handleMarkAdmitdateReplaceClick(claimIDs);
                     }
                });




                $('#mark_closed_checkbox_button').click(function () {
                    pastClaims.handleMarkClosedClick(pastClaims.results.getActionClaimItemIDs('mark_closed'));
                });

                // $(".dt-buttons").append('<div class="btn-group"><button type="button" class="btn btn-primary btn-squared">Save View</button><button type="button" class="btn btn-primary btn-squared dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button></div>');
            }, 1000)
        },
        buildErrorTooltips: function () {
            for (var claimItemId in pastClaims.errors) {
                var errs = pastClaims.errors[claimItemId].split(', ');
                var title = '';

                for (var a in errs) {
                    if (title.length) {
                        title += '\n';
                    }
                    console.log(errs[a]);
                    if (pastClaims.explCodes.hasOwnProperty(errs[a])) {
                        title += pastClaims.explCodes[errs[a]];
                        if (pastClaims.hlx8lines[claimItemId]) {
                            var msg = pastClaims.hlx8lines[claimItemId];
                        } else {
                            var msg = '';
                        }
                        if (msg != '') {
                            title += ' - Error Code Message: ' + msg;
                        }
                    } else {
                        title += 'Unknown error code';
                    }
                }
                title = title.replace('?-&gt;', '').replace('&lt;-?', '').replace(/\HX8/g, '');
               // console.log('title',claimItemId);
                $('#errorTitle_' + claimItemId).attr('title', title);
            }
        },
        buildTableRow: function (data, id, filter) {
            if (filter) {
                for (var i in filter) {
                    if (pastClaims.results.columns[i]) {
                        if (!(data[pastClaims.results.columns[i]].includes(filter[i]))) {
                            return;
                        }
                    }
                }
            }

            var claimItemId = id;
            var claimId = data['claim_id'];
            var tds = '';

            pastClaims.data[claimItemId] = {};
            pastClaims.data[claimItemId]['claim_id'] = data['claim_id'];
            pastClaims.data[claimItemId]['patient_id'] = data['patient_id'];
            pastClaims.data[claimItemId]['item_count'] = data['item_count'];
            pastClaims.data[claimItemId]['edit'] = data['actions'].hasOwnProperty('edit');
            pastClaims.data[claimItemId]['resubmit'] = data['actions'].hasOwnProperty('resubmit');
            pastClaims.data[claimItemId]['submit'] = data['actions'].hasOwnProperty('submit');
            pastClaims.data[claimItemId]['mark_closed'] = data['actions'].hasOwnProperty('mark_closed');
            pastClaims.data[claimItemId]['mark_paid'] = data['actions'].hasOwnProperty('mark_paid');

            for (var a = 0; a < pastClaims.results.columns.length; a++) {
                var className = pastClaims.results.columns[a];
                className = className.replace(' ', '_');
                if (className == 'edit_checkbox') {
                    tds += '<td class="' + className + '">'
                            + '<label class="CHKcontainer">'
                            + '<input type="checkbox" class="checkboxChk" id="claim_' + id + '" name="claim[' + id + ']" value="' + data['claim_id'] + '" checked="checked" />'
                            + ' <span class="CHKcheckmark"></span>'
                            + ' </label>'

                            + '</td>';
                } else if (className == 'actions') {
                    tds += '<td class="context_menu actions highlight">'
//                            + '<a class="btn btn-bricky actions_button" id="actions_' + claimItemId + '" href="#" ><i class="clip-arrow-down"></i></a>';
                            + '<img class="actions_button" id="actions_' + claimItemId + '" src="' + pastClaims.results.menuHtml + '" />&nbsp;&nbsp;';

                    if (data['item_count'] > 1) {
                        tds += data['item_count'] + ' Items in Claim';
                    }
                    tds += '</td>';
                } else if (className == 'errors') {
                    tds += '<td class="' + className + '" title="">' + data['errors'] + '</td>';
                    if (data['errors']) {
                        pastClaims.errors[claimItemId] = data['errors'];
                    }
                } else {
                    var title = typeof data[className] != 'undefined' ? data[className].replace('_', ' ') : '';
                    var newWidth = '0px';
                    if (className == 'service_code' || className == 'diag_code') {
                        newWidth = '6px';
                    }

                    tds += '<td style="width:' + newWidth + '" class="' + className + '">' + title + '</td>';
                }
            }


            return tds;
        },
        buildTotalsRow: function () {
            $('#past_claims_totals .subtotals .fee_subm').text('$' + number_format(pastClaims.results.subtotals.submitted, 2, '.', ','));
            $('#past_claims_totals .totals .fee_subm').text('$' + number_format(pastClaims.results.totals.submitted, 2, '.', ','));

            $('#past_claims_totals .subtotals .fee_paid').text('$' + number_format(pastClaims.results.subtotals.paid, 2, '.', ','));
            $('#past_claims_totals .totals .fee_paid').text('$' + number_format(pastClaims.results.totals.paid, 2, '.', ','));

            $('#past_claims_totals .subtotals .num_claims').text(number_format(pastClaims.results.subtotals.claims, 0, '.', ','));
            $('#past_claims_totals .totals .num_claims').text(number_format(pastClaims.results.totals.claims, 0, '.', ','));

            $('#past_claims_totals .subtotals .num_items').text(number_format(pastClaims.results.subtotals.claimItems, 0, '.', ','));
            $('#past_claims_totals .totals .num_items').text(number_format(pastClaims.results.totals.claimItems, 0, '.', ','));

            $('#past_claims_totals .subtotals .num_patients').text(number_format(pastClaims.results.subtotals.patients, 0, '.', ','));
            $('#past_claims_totals .totals .num_patients').text(number_format(pastClaims.results.totals.patients, 0, '.', ','));

            for (var a in pastClaims.results.totals.pay_progs) {
                var className = a.replace(' - ', '').replace(' ', '') + 'Totals';

                $('#past_claims_totals tbody tr.' + className).remove();

                var tr = '<tr class="' + className + ' PayProgSubtotals" data-print="true">'
                        + '<td>' + a + '</td>'
                        + '<td class="fee_subm">$' + number_format(pastClaims.results.totals.pay_progs[a].submitted, 2, '.', ',') + '</td>'
                        + '<td class="fee_paid">$' + number_format(pastClaims.results.totals.pay_progs[a].paid, 2, '.', ',') + '</td>'
                        + '<td class="num_claims">' + number_format(pastClaims.results.totals.pay_progs[a].claims, 0, '.', ',') + '</td>'
                        + '<td class="num_items">' + number_format(pastClaims.results.totals.pay_progs[a].claimItems, 0, '.', ',') + '</td>'
                        + '<td class="num_patients">n/a</td>'
                        + '</tr>';

                $('#past_claims_totals tbody .totals').before(tr);
            }

            for (var a in pastClaims.results.subtotals.pay_progs) {
                var className = a.replace(' - ', '').replace(' ', '') + 'Subtotals';

                $('#past_claims_totals tbody tr.' + className).remove();

                var tr = '<tr class="' + className + ' PayProgSubtotals" data-print="false">'
                        + '<td>' + a + '</td>'
                        + '<td class="fee_subm">$' + number_format(pastClaims.results.subtotals.pay_progs[a].submitted, 2, '.', ',') + '</td>'
                        + '<td class="fee_paid">$' + number_format(pastClaims.results.subtotals.pay_progs[a].paid, 2, '.', ',') + '</td>'
                        + '<td class="num_claims">' + number_format(pastClaims.results.subtotals.pay_progs[a].claims, 0, '.', ',') + '</td>'
                        + '<td class="num_items">' + number_format(pastClaims.results.subtotals.pay_progs[a].claimItemCount, 0, '.', ',') + '</td>'
                        + '<td class="num_patients">n/a</td>'
                        + '</tr>';

                $('#past_claims_totals tbody .subtotals').before(tr);
            }

        },
        canDoAction: function (action) {
            for (var a in pastClaims.data) {
                if (pastClaims.data[a][action]) {
                    return true;
                }
            }

            return false;
        },
        clearResultsNoRefresh: function () {
            var columns = new Array();
            var count = 0;
            columns[count++] = 'edit_checkbox';

            var newColumnsOrder = $('select[name="past_claims[report_columns][]"]').val();

            for (var i = 0; i < newColumnsOrder.length; i++) {
                if (newColumnsOrder[i]) {
                    var className = newColumnsOrder[i].replace('_', ' ');
                    columns[count++] = className;
                }
            }
//            $('input[name="past_claims[report_columns][]"]:checked').each(function () {
//                var className = $(this).prop('id').replace(pastClaims.form.prefix + '_report_columns_', '');
//                columns[count++] = className;
//            });

            columns[count++] = 'actions';

            pastClaims.results.paginationSort = new Array();
            count = 0;
            for (var a in pastClaims.results.sortOrder) {
                var order = pastClaims.results.sortOrder[a];
                var colNumber = jQuery.inArray(a, columns);
                if (colNumber != -1) {
                    pastClaims.results.paginationSort[count++] = new Array(colNumber, order);
                }
            }

            pastClaims.results.columns = columns;

//            $('#' + pastClaims.resultsTable).tablesorter({
//               // sortList: pastClaims.results.paginationSort
//            });


        },
        clearResults: function () {
            var columns = new Array();
            var count = 0;

            $('#' + pastClaims.resultsTable).remove();
            $('#past_claims_totals tbody tr.PayProgSubtotals').remove();
//            $(".DataTablePastClaimResult").dataTable().fnDestroy(true);
            $('#past_claims_results_table_form').html($('<div style="overflow:inherit"><table style="background:#fff" id="' + pastClaims.resultsTable + '" class="DataTablePastClaimResult table table-hover well customWellTable"><thead></thead><tbody></tbody><tfoot></tfoot></table></div><br/>'));

            var tr = '<tr><th class="edit_checkbox">' +
                    '<label class="CHKcontainer">' +
                    '<input type="checkbox" id="resultsCheckAll" value="1" checked="checked" />' +
                    '<span class="CHKcheckmark" style="top:-15px"></span></label>' +
                    '</th>';
            columns[count++] = 'edit_checkbox';
            //$('input[name="past_claims[report_columns][]"]:checked')
            var newColumnsOrder = $('select[name="past_claims[report_columns][]"]').val();

            for (var i = 0; i < newColumnsOrder.length; i++) {

                if (newColumnsOrder[i]) {

                    var text = pastClaims.getColumnTitle(newColumnsOrder[i]);
                    var className = newColumnsOrder[i];
                    text = text.replace(/\_/g, ' ');
                    tr += '<th class="' + className + '">' + text.charAt(0).toUpperCase() + text.slice(1);
                    +'</th>';
                    columns[count++] = className;
                }
            }
            /*$('select[name="past_claims[report_columns]"]').each(function () {
             var className = $(this).val();//$(this).prop('id').replace(pastClaims.form.prefix + '_report_columns_', '');
             var text = className;//.replace( '_', ' ');//$('label[for="' + $(this).prop('id') + '"]').text();
             tr += '<th class="' + className + '">' + text + '</th>';
             columns[count++] = className;
             });*/
            tr += '<th class="highlight actions">Actions</th></tr>';
            columns[count++] = 'actions';

            var subhead = '<tr class="filterTR"><th></th>';

            var newColumnsOrder = $('select[name="past_claims[report_columns][]"]').val();
            for (var i = 0; i < newColumnsOrder.length; i++) {
                if (newColumnsOrder[i]) {
                    subhead += '<th></th>';
                }
            }
            subhead += '<th></th></tr>';

            $('#' + pastClaims.resultsTable + ' thead').append($(tr));
            $('#' + pastClaims.resultsTable + ' tfoot').append($(subhead));
            $('#' + pastClaims.resultsTable + ' thead th.edit_checkbox').addClass('sorter-false');
            $('#' + pastClaims.resultsTable + ' thead th.actions').addClass('sorter-false');
            $('#' + pastClaims.resultsTable + ' thead th.health_card_vc').addClass('sorter-false');
            $('#' + pastClaims.resultsTable + ' thead th.provider_number').addClass('sorter-false');
            $('#' + pastClaims.resultsTable + ' thead th.group_num').addClass('sorter-false');
            $('#' + pastClaims.resultsTable + ' thead th.spec_code').addClass('sorter-false');

            pastClaims.results.paginationSort = new Array();
            count = 0;
            for (var a in pastClaims.results.sortOrder) {
                var order = pastClaims.results.sortOrder[a];
                var colNumber = jQuery.inArray(a, columns);
                if (colNumber != -1) {
                    pastClaims.results.paginationSort[count++] = new Array(colNumber, order);
                }
            }

            pastClaims.results.columns = columns;
            pastClaims.results.initializePager();

        },
        getActionClaimIDs: function (action) {
            var ids = new Array();
            var table = $('#' + pastClaims.resultsTable).DataTable();

            var data = table.rows({filter: 'applied', page: 'current'}).data();
            for (var i in data) {
                var claimItemId = $(data[i][0]).find('input[type="checkbox"]').val();

                try {
                    if ($("input[type='checkbox'][id='" + claimItemId + "']").is(':checked')) {
                        if ($("input[type='checkbox'][id='" + claimItemId + "']").attr('data-' + action) == 'true') {
                            ids[ids.length] = $("input[type='checkbox'][id='" + claimItemId + "']").attr('data-claim_id');
                        } else {
                            $("input[type='checkbox'][id='" + claimItemId + "']").prop('checked', false);
                        }
                    }
                } catch (e) {
//                    $(data[i][0]).find('input[type="checkbox"]').prop('checked', false);
                }
            }

            return ids;
        },
        getActionClaimItemIDs: function (action) {
            var ids = new Array();
            var table = $('#' + pastClaims.resultsTable).DataTable();
            var data = table.rows({filter: 'applied', page: 'current'}).data();
            for (var i in data) {
                var claimItemId = $(data[i][0]).find('input[type="checkbox"]').attr('id');
                try {
                    if ($("input[type='checkbox'][id='" + claimItemId + "']").is(':checked')) {
                        if ($("input[type='checkbox'][id='" + claimItemId + "']").attr('data-' + action) == 'true') {
                            ids[ids.length] = claimItemId;
                        } else {
                            $("input[type='checkbox'][id='" + claimItemId + "']").prop('checked', false);
                        }
                    }
                } catch (e) {
//                    $(data[i][0]).find('input[type="checkbox"]').prop('checked', false);
                }
            }
            return ids;
        }
    },

    initialize: function () {
        //$('#past_claims_ra').prop('disabled', true);
        pastClaims.form.initialize();
        pastClaims.refine.initialize();
        pastClaims.facility.initialize();
        pastClaims.pdf.initialize();
        pastClaims.csv.initialize();
    },

    getClaimId: function (claimItemId) {

        if (pastClaims.data.hasOwnProperty(claimItemId)) {
            return pastClaims.data[claimItemId]['claim_id'];
        }

        var get_id = $('input[type="hidden"][value="' + claimItemId + '"]').attr('data-claim_id');
        if (typeof get_id != 'undefined') {
            return get_id;
        }
        return claimItemId;
    },
    getClaimItemId: function (claimId) {

        var get_id = $('input[type="hidden"][value="' + claimId + '"]').attr('data-item_id');
        if (typeof get_id != 'undefined') {
            return get_id;
        }
        return claimItemId;
    },
    getPatientId: function (claimItemId) {
        // if (pastClaims.data.hasOwnProperty(claimItemId)) {
        //     return pastClaims.data[claimItemId]['patient_id'];
        // }

         if (pastClaims.data.hasOwnProperty(claimItemId)) {
            return pastClaims.data[claimItemId]['patient_id'];
        }

        var get_id = $('input[type="hidden"][value="' + claimItemId + '"]').attr('data-patient_id');
        if (typeof get_id != 'undefined') {
            return get_id;
        }
        return claimItemId;
    },
    handleDetailsActionClick: function (claim_id, new_tab) {
        if (new_tab) {
            window.open(pastClaims.actions.details + '?id=' + claim_id, 'details');
        } else {
            window.location = pastClaims.actions.details + '?id=' + claim_id;
        }
    },
    handleDetailsProfileActionClick: function (claim_id, new_tab) {
        if (new_tab) {
            window.open(pastClaims.actions.profile + '?id=' + claim_id, 'details');
        } else {
            window.location = pastClaims.actions.profile + '?id=' + claim_id;
        }
    },
    handleEditClick: function (claim_id, item_id, new_tab) {
        if (new_tab) {
            for (var a in pastClaims.data) {
                if (pastClaims.data[a].claim_id == claim_id) {
                    $('tr#' + pastClaims.results.rowPrefix + '_' + a).addClass('edited');
                } 
            }

            $('input[type="hidden"][value="' + claim_id + '"]').closest('tr').find('td').addClass('sk-blue');
            window.open(pastClaims.actions.edit + '?id=' + claim_id, 'claim');
        } else {
            //return false;
            window.location = pastClaims.actions.edit + '?id=' + claim_id;
        }
    },
    handleEditGroupClick: function (ids) {
        quickClaim.showOverlayMessage('Loading Claim Edit Interface');
        var ClientIdsJoin = ids.join(',');
        $("#inputBulkEdit").val(ClientIdsJoin);
        $("#pastClaimBulkEditForm").submit();
    },
    handleFindClick: function () {

        if (pastClaims.isBckgrndActive == true) {
            pastClaims.resetAll = true;
        }
        if (pastClaims.resetAll == true) {
            pastClaims.dtCurrentPage = 1;
            pastClaims.dtCurrentPage = 1;
            if ($.fn.DataTable.isDataTable("#" + pastClaims.resultsTable)) {
                //var datatable = $("#" + pastClaims.resultsTable).DataTable();
                //datatable.clear().draw();
                $("#" + pastClaims.resultsTable).dataTable().fnDestroy();
                $("#" + pastClaims.resultsTable).html('');
            }
            $(".ProgressBarMain").css('width', '0%');

            //astClaims.processDataTableData();
            //return;
        }
        var doctors = localStorage.getItem('doctors');
        var locations = localStorage.getItem('locations');
        localStorage.clear();
        localStorage.setItem('doctors',doctors);
        localStorage.setItem('locations',locations);
        pastClaims.form.clearErrors();
        pastClaims.initDatatable();
        pastClaims.isBckgrndActive = true;
//        pastClaims.results.clearResults();
    },
    handleMarkClosedClick: function (claimItemIDs) {
        pastClaims.selectedIDs = claimItemIDs;
        claimStatusActions.markClosed.openDialog(claimItemIDs);
    },
    handleMarkPaidClick: function (claimItemIDs) {
        pastClaims.selectedIDs = claimItemIDs;
        //console.log('add',claimItemIDs);
        claimStatusActions.markPaid.openDialog(claimItemIDs);
    },
    handleMarkAdminLogin:function(modelid)    {
         $('#markAdminLogin').data('modelid', modelid).dialog('open');
    },
    confirmReLogin:function(password,modelid){
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: 'password=' + password,
            url: pastClaims.actions.checkLogin,
            success: function (rs) {
                if(rs.code == 200)
                {
                  pastClaims.actions.replacedatalogin = 'true';
                  $('#'+modelid).trigger('click');
                  pastClaims.actions.replacedatalogin = 'false';
                  sk.skNotify(rs.message, 'success');
                }
                else
                {
                    $('#markAdminLogin').data('modelid', modelid).dialog('open');
                     sk.skNotify(rs.message, 'danger');
                }
            },
            beforeSend: function () {
                quickClaim.showOverlayMessage('Loading...');
            },
            complete: function () {
                quickClaim.hideOverlayMessage();
            }
        });
    },
    handleMarkFacilityReplaceClick:function(claim_id){
         $('#markFacility').data('claim_id', claim_id).dialog('open');
    },
    confirmReplaceFacility:function(facility_num,facility_id,claim_id){
        var claimIDs = claim_id;
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: 'c_ids=' + claimIDs + '&facility_id=' + facility_id + '&facility_num=' + facility_num,
            url: pastClaims.actions.changeFacility,
            success: function (rs) {
                if(rs.code == 200)
                {
                var FacilityThIndex = $('#past_claims_results_table tr th:contains("Facility")').index();
                if(rs.data == null) {
                     var claimidArr = claimIDs.split(',');
                     for(let j in claimidArr)
                     {
                         $("#past_claims_results_table .claim_"+claimidArr[j]+" td").eq(FacilityThIndex).html('<strong></strong>');
                     }
                }
                else
                {

                for(let j in rs.data)
                {
                    if(FacilityThIndex !== -1)
                    {
                         $("#past_claims_results_table .claim_"+rs.data[j]['claim_id']+" td").eq(FacilityThIndex).html('<strong>'+rs.data[j]['facility_num']+'</strong>');
                    }
                }

                }
                  sk.skNotify(rs.message, 'success');
                  $('#facility_description').val(' ');
                  $('#doctor_moh_facility_num').val(' ');
                  $('#facility_id').val(' ');
                }
                else
                {
                     sk.skNotify(rs.message, 'danger');
                }
            },
            beforeSend: function () {
                quickClaim.showOverlayMessage('Updating Facility');
            },
            complete: function () {
                quickClaim.hideOverlayMessage();
            }
        });
    },
    handleMarkLocationReplaceClick:function(claim_id){
         $('#markLocation').data('claim_id', claim_id).dialog('open');
    },
    confirmReplaceLocation: function (location,claim_id) {
        var claimIDs = claim_id;
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: 'c_ids=' + claimIDs + '&location=' + location,
            url: pastClaims.actions.changeLocation,
            success: function (rs) {
                if(rs.code == 200)
                {
                var LocationThIndex = $('#past_claims_results_table tr th:contains("Location")').index();
                for(let j in rs.data)
                {
                    if(LocationThIndex !== -1)
                    {
                         $("#past_claims_results_table .claim_"+rs.data[j]['claim_id']+" td").eq(LocationThIndex).html('<strong>'+rs.data[j]['location']+'</strong>');
                    }
                }
                  sk.skNotify(rs.message, 'success');
                  $('#replace_location_id').val(' ');
                }
                else
                {
                     sk.skNotify(rs.message, 'danger');
                }
            },
            beforeSend: function () {
                quickClaim.showOverlayMessage('Updating Location');
            },
            complete: function () {
                quickClaim.hideOverlayMessage();
            }
        });
    },
    handleMarkAdmitdateReplaceClick:function(claim_id) {
        $('#replace_admit_date_id').datepicker();
        $('#markAdmitdate').data('claim_id', claim_id).dialog('open');
        // pastClaims.selectedIDs = claimItemIDs;
        // claimStatusActions.markPaid.openDialog(claimItemIDs);
    },
    confirmReplaceAdmitdate: function (admit_date,claim_id) {
        var claimIDs = claim_id;
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: 'c_ids=' + claimIDs + '&admit_date=' + admit_date,
            url: pastClaims.actions.changeAdmitdate,
            success: function (rs) {
                if(rs.code == 200)
                {
                var AdmitDateThIndex = $('#past_claims_results_table tr th:contains("Admit date")').index();
                for(let j in rs.data)
                {
                    if(AdmitDateThIndex !== -1)
                    {
                         $("#past_claims_results_table .claim_"+rs.data[j]['claim_id']+" td").eq(AdmitDateThIndex).html('<strong>'+rs.data[j]['admit_date']+'</strong>');
                    }
                }
                  sk.skNotify(rs.message, 'success');
                  $('#replace_admit_date_id').val(' ');
                }
                else
                {
                     sk.skNotify(rs.message, 'danger');
                }
            },
            beforeSend: function () {
                quickClaim.showOverlayMessage('Updating Admit Date');
            },
            complete: function () {
                quickClaim.hideOverlayMessage();
            }
        });
    },
    handleMarkSliReplaceClick:function(claim_id) {
        $('#markSliDoctor').data('claim_id', claim_id).dialog('open');
        // pastClaims.selectedIDs = claimItemIDs;
        // claimStatusActions.markPaid.openDialog(claimItemIDs);
    },
    confirmReplaceSli: function (sli,claim_id) {
        var claimIDs = claim_id;
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: 'c_ids=' + claimIDs + '&sli=' + sli,
            url: pastClaims.actions.changeSli,
            success: function (rs) {
                if(rs.code == 200)
                {
                var SliThIndex = $('#past_claims_results_table tr th:contains("Sli")').index();
                for(let j in rs.data)
                {
                    if(SliThIndex !== -1)
                    {
                         $("#past_claims_results_table .claim_"+rs.data[j]['claim_id']+" td").eq(SliThIndex).html('<strong>'+rs.data[j]['sli']+'</strong>');
                    }
                }
                  sk.skNotify(rs.message, 'success');
                  $('#replace_sli_id').val(' ');
                }
                else
                {
                     sk.skNotify(rs.message, 'danger');
                }
            },
            beforeSend: function () {
                quickClaim.showOverlayMessage('Updating Sli');
            },
            complete: function () {
                quickClaim.hideOverlayMessage();
            }
        });
    },
    handleMarkDoctorReplaceClick:function(claim_id) {
        $('#markReplaceDoctor').data('claim_id', claim_id).dialog('open');
        // pastClaims.selectedIDs = claimItemIDs;
        // claimStatusActions.markPaid.openDialog(claimItemIDs);
    },
    confirmReplaceDoctor: function (doctor_id,claim_id) {
        var claimIDs = claim_id;
        //var claimIDs = $('#past_claims_results_table .checkboxChk:checked').map(function() {return $(this).attr('data-claim_id');}).get().join(',');
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: 'c_ids=' + claimIDs + '&doctor_id=' + doctor_id,
            url: pastClaims.actions.changeDoctors,
            success: function (rs) {
                if(rs.code == 200)
                {
                    //2972
               var drCodeIndex = -1;
               $('#past_claims_results_table tr th').each(function(index){
                    if ($(this).text() == 'DR'){
                        drCodeIndex = index;
                    }
                });  

               //alert(indexReturn);

                //var drCodeIndex = $('#past_claims_results_table tr th:contains("DR")').index();
                var drNameThIndex = $('#past_claims_results_table tr th:contains("Doctor")').index();
                for(let j in rs.data)
                {
                    if(drCodeIndex !== -1)
                    {

                         $("#past_claims_results_table .claim_"+rs.data[j]['claim_id']+" td").eq(drCodeIndex).html('<strong>'+rs.data[j]['doctor_code']+'</strong>');
                    }
                    if(drNameThIndex !== -1)
                    {
                         $("#past_claims_results_table .claim_"+rs.data[j]['claim_id']+" td").eq(drNameThIndex).html('<strong>'+rs.data[j]['doctor_name']+'</strong>');
                    }
                }
                  // $('#find_button').trigger('click');
                  // $('#find_button').trigger('click');
                  sk.skNotify(rs.message, 'success');
                  $('#replace_doctor_id').val(' ').trigger('change.select2');
                }
                else
                {
                     sk.skNotify(rs.message, 'danger');
                }
            },
            beforeSend: function () {
                quickClaim.showOverlayMessage('Updating Doctors');
            },
            complete: function () {
                quickClaim.hideOverlayMessage();
            }
        });
    },
    handlePdfActionClick: function (claim_id) {
        var message = "Claim details PDF generation process started.";
        $.notify(message, {
            element: 'body',
            type: "info",
            allow_dismiss: true,
            newest_on_top: true,
            placement: {
                from: "top",
                align: "right"
            },
            delay: 5000,
            z_index: 99999,
            mouse_over: 'pause',
            animate: {
                enter: 'animated bounceIn',
                exit: 'animated bounceOut'
            },
            template:  `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                        <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                        <span data-notify="icon"></span>
                        <span data-notify="title">{1}</span>
                        <span data-notify="message">{2}</span>
                        <div class="progress" data-notify="progressbar">
                        <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                        </div>
                        <a href="{3}" target="{4}" data-notify="url"></a>
                        </div>`

        });
        pastClaims.setupIframe();
        pastClaims.setupHiddenForm(new Array(), pastClaims.actions.claimPDF + '?id=' + claim_id, '');
    },
    handleResubmitClick: function (claimItemIDs) {
        pastClaims.selectedIDs = claimItemIDs;
        claimStatusActions.resubmit.openDialog(claimItemIDs);
    },
    handleSubmitClick: function (claimIDs) {
        if (!claimIDs.length) {
            quickClaim.showOverlayMessage('No Submittable Claims are selected.');
            setTimeout(function () {
                quickClaim.hideOverlayMessage();
            }, 2000);
            return;
        }

        var data = 'claim_ids=' + claimIDs.join(',');
        batch_create.handleDoctorLocationCountRequest(data);
        $('#batch_creation_dialog').on('dialogclose', function (event) {
            $('#unsubmitted_search').trigger('click');
            $('#unsubmitted_search').trigger('click');
        });
    },
    handleRebillClick: function () {
        $('#rebill_new_date').datepicker();
        $('#rebillConfirmDialog').dialog('open');
    },
    confirmRebillClick: function (date) {
        var claimIDs = pastClaims.results.getActionClaimIDs('resubmit');
        var claimItemIDs = pastClaims.results.getActionClaimItemIDs('resubmit');

        $.ajax({
            type: 'post',
            dataType: 'json',
            data: 'c_ids=' + claimIDs.join(',') + '&ids=' + claimItemIDs.join(',') + '&date=' + date,
            url: pastClaims.actions.changeItems,
            success: function (rs) {
                $('#rebillCompleteDialog').dialog('open');

                var html = '<ul>';
                if (rs.res) {
                    for (var msg in rs.res) {
                        html += '<li>' + rs.res[msg] + '</li>';
                    }
                }
                
                if (rs.error) {
                    html += '</ul><h4>Errors</h4><ul>';
                    for (var err in rs.error) {
                        html += '<li>' + 'Claim for ' + rs.error[err] + '</li>';
                    }
                    html += '</ul>';
                }
                $('.rebillCount').html(html);
            },
            beforeSend: function () {
                quickClaim.showOverlayMessage('Cloning selected claims');
            },
            complete: function () {
                quickClaim.hideOverlayMessage();
            }
        });
    },
    setupHiddenForm: function (fields, action, prefix) {
        setTimeout(function () {
            var frame_body = $('#hiddenDownloader').contents().find('body');
            var form = '<form id="hidden_form" action="' + action + '" method="POST">';

            for (var a in fields) {
                var name = fields[a].substring(0, fields[a].indexOf('='));
                var value = fields[a].substring(fields[a].indexOf('=') + 1);

                name = name.replace(pastClaims.form.prefix, prefix);
                form += '<input type="text" value="' + value + '" id="' + name + '" name="' + name + '" />';
            }
            form += '</form>';
            frame_body.append($(form));
            $('#hiddenDownloader').contents().find('#hidden_form').submit();
        }, 15000);
    },
    setupIframe: function () {
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    setupIframeWithURL: function (url, params, mode) {
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '?' + params + '"  style="width: 0; height: 0; border: none; display: none;" onload="pastClaims.' + mode + '.downloadComplete();"></iframe>');
    },
    getColumnTitle: function (text) {
        var text2 = '';
        switch (text) {
            case 'doctor_code':
                text2 = 'DR';
                break;
            case 'account_num':
                text2 = 'Acct';
                break;
            case 'patient_id':
                text2 = 'PID';
                break;
            case 'patient_name':
                text2 = 'patient';
                break;
            case 'health_card_vc':
                text2 = 'HCN';
                break;
            case 'dob':
                text2 = 'DOB';
                break;
            case 'claim_status':
                text2 = 'Status';
                break;
            case 'service_code':
                text2 = 'Serv Code';
                break;
            case 'num_serv':
                text2 = 'NS';
                break;
            case 'fee_subm':
                text2 = 'Fee';
                break;
            case 'fee_paid':
                text2 = 'Paid';
                break;
            case 'errors':
                text2 = 'Expl. Code';
                break;
            case 'provider_number':
                text2 = 'Provider';
                break;
            case 'group_num':
                text2 = 'Group';
                break;
            case 'spec_code':
                text2 = 'Spec';
                break;
            case 'claim_id':
                text2 = 'CID';
                break;
            case 'moh_claim':
                text2 = 'MOH';
                break;
            case 'batch_num':
                text2 = 'Batch';
                break;
            case 'patient_number':
                text2 = 'Chart';
                break;
            case 'lname':
                text2 = 'Last';
                break;
            case 'fname':
                text2 = 'First';
                break;
            case 'expiry_date':
                text2 = 'HCN Expiry';
                break;
            case 'postal_code':
                text2 = 'Postal';
                break;
            case 'hphone':
                text2 = 'Home Ph';
                break;
            case 'wphone':
                text2 = 'Work Ph';
                break;
            case 'refdoc_num':
                text2 = 'Refdoc';
                break;
            case 'facility_num':
                text2 = 'Facility';
                break;
            case 'manual_review':
                text2 = 'Review';
                break;
            case 'service_date':
                text2 = 'S Date';
                break;
            case 'created_username':
                text2 = 'Created User Name';
                break;
            case 'error_reconcile':
                text2 = 'Error File';
                break;
            case 'updated_at':
                text2 = 'Date Updated';
                break;

            default:
                text2 = text;
                break;

        }
        text2 = text2.replace(/\_/g, ' ');
        var title = text2.charAt(0).toUpperCase() + text2.slice(1);
        return title;
    },
    getColumnWidth: function (text) {
        var width = '';
        switch (text) {
            case 'doctor_code':
                width = '10px';
                break;
            case 'account_num':
                width = '10px';
                break;
            case 'patient_id':
                width = '10px';
                break;
            case 'patient_name':
                width = '10px';
                break;
            case 'health_card_vc':
                width = '10px';
                break;
            case 'charge_to':
                width = '10px';
                break;
            case 'patient_name':
                width = '10px';
                break;
            case 'doctor':
                width = '10px';
                break;

            case 'dob':
                width = '10px';
                break;
            case 'claim_status':
                width = '10px';
                break;
            case 'service_code':
                width = '10px';
                break;
            case 'num_serv':
                width = '10px';
                break;
            case 'fee_subm':
                width = '10px';
                break;
            case 'fee_paid':
                width = '10px';
                break;
            case 'errors':
                width = '10px';
                break;
            case 'provider_number':
                width = '10px';
                break;
            case 'group_num':
                width = '10px';
                break;
            case 'spec_code':
                width = '10px';
                break;
            case 'claim_id':
                width = '10px';
                break;
            case 'moh_claim':
                width = '10px';
                break;
            case 'batch_num':
                width = '10px';
                break;
            case 'patient_number':
                width = '10px';
                break;
            case 'lname':
                width = '10px';
                break;
            case 'fname':
                width = '10px';
                break;
            case 'expiry_date':
                width = '10px';
                break;
            case 'postal_code':
                width = '10px';
                break;
            case 'hphone':
                width = '10px';
                break;
            case 'wphone':
                width = '10px';
                break;
            case 'refdoc_num':
                width = '10px';
                break;
            case 'facility_num':
                width = '10px';
                break;
            case 'manual_review':
                width = '10px';
                break;
            case 'service_date':
                width = '10px';
                break;
            case 'created_username':
                width = '10px';
                break;
            case 'error_reconcile':
                width = '10px';
                break;
            case 'updated_at':
                width = '10px';
                break;
            case 'city':
                width = '10px';
                break;
            default:
                width = '10px';
                break;

        }

        return width;
    },
    initDatatable: function () {
        $("#past_claims_errors").hide();
        $("#service_based").parent('.tab-content').attr('style', 'border-color:#ddd');
        $('a[href="#service_based"]').attr('style', 'border-left-color:#ddd;border-right-color:#ddd;');
        var newColumnsOrder = $('select[name="past_claims[report_columns][]"]').val();

        pastClaims.results.columns = newColumnsOrder;
        if (newColumnsOrder == null) {
            $("#past_claims_errors").html('Please select report column').addClass('alert alert-danger').show();
            $("#service_based").parent('.tab-content').attr('style', 'border-color:#BD1431');
            $('a[href="#service_based"]').attr('style', 'border-left-color:#BD1431;border-right-color:#BD1431');
            return false;
        }

        var colHeader = [];
        quickClaim.ajaxLocked = true;
        if ($.fn.DataTable.isDataTable("#" + pastClaims.resultsTable)) {
            $("#" + pastClaims.resultsTable).dataTable().fnDestroy();
            $("#" + pastClaims.resultsTable).html('');
        }

        colHeader.push({
            title: '<span class="edit_checkbox"><label class="CHKcontainer"><input type="checkbox" id="resultsCheckAll" value="1" checked="checked" /><span class="CHKcheckmark"></span></label></span>',
            width: '20px'
        });

        for (var i = 0; i < newColumnsOrder.length; i++) {
            if (newColumnsOrder[i]) {
                var text = pastClaims.getColumnTitle(newColumnsOrder[i]);
                var width = pastClaims.getColumnWidth(text);
                text = text.replace(/\_/g, ' ');

                text = text == 'Updated user id' ? 'Updated By UserID' : text;
                text = text == 'Updated username' ? 'Updated By User Name' : text;
                text = text == 'Created user id' ? 'Created By User Name' : text;
                text = text == 'Created User Name' ? 'Created By User Name' : text;

                var arr = newColumnsOrder[i].split('_');
                if ($.inArray('date', arr) != -1 || newColumnsOrder[i] == 'dob') {
                    colHeader.push({title: text.charAt(0).toUpperCase() + text.slice(1), width: '100px', sType: 'dateField', bSortable: true});
                } else {
                    colHeader.push({title: text.charAt(0).toUpperCase() + text.slice(1), width: '100px'});
                }

            }
        }

        colHeader.push({title: 'Actions', class: 'noExport'});

        var _customizeExcelOptions = function (xlsx) {
            columns: "thead th:not(.noExport)";
            var sheet = xlsx.xl.worksheets['sheet1.xml'];
            var numrows = 5;
            var clR = $('row', sheet);

            function Addrow(index, data) {
                var msg = '<row r="' + index + '">'
                for (var i = 0; i < data.length; i++) {
                    var key = data[i].key;
                    var value = data[i].value;
                    msg += '<c t="inlineStr" r="' + key + index + '">';
                    msg += '<is>';
                    msg += '<t>' + value + '</t>';
                    msg += '</is>';
                    msg += '</c>';
                }
                msg += '</row>';
                return msg;
            }

            //insert

            var r2 = Addrow(2, [{key: 'E', value: 'pdfHeaderDateRange'}]);
            var r3 = Addrow(3, [{key: 'B', value: 'pdfMessage'}]);

            var startSumPoint = +clR.length + 2;
            var i = 0;
            $("#past_claims_totals thead tr th").each(function () {
                var r1 = Addrow(startSumPoint, [{key: getKey(i), value: $(this).text()}]);
                sheet.childNodes[0].childNodes[1].innerHTML = r1 + sheet.childNodes[0].childNodes[1].innerHTML;
                i++;
            });


            startSumPoint = +clR.length + 3;
            $("#past_claims_totals tbody tr").each(function () {
                if ($(this).attr('data-print') == 'true') {
                    var i = 0;

                    $(this).find('td').each(function () {
                        var r2 = Addrow(startSumPoint, [{key: getKey(i), value: $(this).text()}]);
                        sheet.childNodes[0].childNodes[1].innerHTML = r2 + sheet.childNodes[0].childNodes[1].innerHTML;
                        i++;
                    })
                    startSumPoint++;
                }
            });

            function getKey(num) {
                switch (num) {
                    case 0:
                        return 'A';
                        break;
                    case 1:
                        return 'B';
                        break;
                    case 2:
                        return 'C';
                        break;
                    case 3:
                        return 'D';
                        break;
                    case 4:
                        return 'E';
                        break;
                    case 5:
                        return 'F';
                        break;
                }
            }
//                sheet.childNodes[0].childNodes[1].innerHTML = r1 + r2 + r3 + sheet.childNodes[0].childNodes[1].innerHTML;


            $('row c[r^="B6"]', sheet).attr('s', '48');
            $('row c[r^="A6"]', sheet).attr('s', '48');

            $('row c[r^="B3"]', sheet).attr('s', '48');
            $('row c[r^="E1"]', sheet).attr('s', '48');
            $('row c[r^="E2"]', sheet).attr('s', '48');

        }
        if (!$.fn.DataTable.isDataTable('#' + pastClaims.resultsTable)) {
           var CurDate = moment().format(sk.getMomentDatetimeFormat());
           var orientation = $("input[name='orientation']:checked").val();
         
           var datatable = $('#' + pastClaims.resultsTable).DataTable({
                "paging": true,
                "lengthMenu": [10, 25, 50, 100, 200, 250, 500],                
                "lengthChange": true,
                "pageLength": sk.skdt_page_size,
                "searching": true,
                "ordering": true,
                "stateSave": true,
                "info": true,
                "dom": 'Bfrtip',
                'fnCreatedRow': function (nRow, aData, iDataIndex) {
                    var ClaimIdsAdd = $('td .item_id', nRow).val();
                    var ClaimIds = $('td .claim_id', nRow).val();
                    //console.log(hidden_fields);
                    $(nRow).addClass('claim_' + ClaimIds); 
                    $(nRow).attr('id', 'tr_' + ClaimIdsAdd); // or whatever you choose to set as the id
                },
                "buttons": [
                    {    

                        extend: 'pdf',
                        text: '<i class="clip-file-pdf"></i> PDF',
                        className: 'btn btn-danger btn-squared',
                        extend: 'pdfHtml5',
                        //orientation: 'landscape',
                        orientation: orientation,
                        pageSize: 'LEGAL',
                        footer: true,
                        title: $("#reportHeader").val() != '' ? $("#reportHeader").val()+' \n'+CurDate : 'Billing Cycle'+' \n'+CurDate,
                        messageTop: $("#reportSubheader").val() != '' ? $("#reportSubheader").val() : '',
                        exportOptions: {
                            columns: ["thead th:not(.noExport)"]

//                            format: {
//                                body: function ( data, row, column, node ) {
//                                    // Strip $ from salary column to make it numeric
//                                    return column === 5 ?
//                                        data.replace( /[$,]/g, '' ) :
//                                        data;
//                                }
//                            }
                        },
                        customize: function (doc) {
                            if(orientation == 'portrait')
                            {
                                 doc.defaultStyle.fontSize = sk.print_font_size - 1;
                                 doc.styles.title = {
                                    fontSize: '10',
                                    alignment: 'left',
                                    margin: [425, -50, 0, 50],
                                  };
                            }
                            else
                            {
                                 doc.defaultStyle.fontSize = sk.print_font_size;
                                 doc.styles.title = {
                                        fontSize: '20',
                                        alignment: 'left',
                                        margin: [425, -50, 0, 50],
                                 };
                            }
                           
//                            doc.content.splice( 1, 0, {
//                                margin: [ 0, 0, 0, 12 ],
//                                alignment: 'center',
//                                image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA5UAAACnCAYAAACB8njoAAAACXBIWXMAAC4jAAAuIwF4pT92AAAgAElEQVR42u3dfXBc5WHv8d/qzW/glXmLcQ0SJgQuwW9JeAkkkTxpCbSlkrNF3dtp481NMjfT2zuIy72dO8kMrP9oJ5O2gzxJKZ00ZU0y7bLpXuRLU0guvV6FJIVAYslOiCHBlsAYA8bW4mvZlmXv/WMfCVmWdvc5e3b3vHw/MxqwvXu055znnH1+53mLFAoFAQAAAADgRBOHAAAAAABAqAQAAAAAECoBAAAAAIRKAAAAAAChEgAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAAIRKAAAAAAChEgAAAABAqAQAAAAAECoBAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAABAqAQAAAAA1FFLWHc8ncl2S+o0P+2SNph/ape0foG35SUNz/pzbtZ/R+N9sVGKFAAAAIAwiRQKhTAEyA2Sus3PBkkdNfx1QyZ45iQNEzQBAAAAECr9FyI7TYDsNf+NNvDjjJiAORjvi+UocgAAAAAIld4Mku0mRPZr4e6rjZaXNCgpRcAEAAAAQKj0RpjslpSQtMVnH31M0oAJmOMURQAAAACEyvqGyYSkpGo7PrJetktKMv4SAAAAAKGSMEm4BAAAAECoDFOYbD52WC3Hj0iSml57peRrC8tXqBC9SJJ0auUHCJcAAAAACJUeD5MbVBx72OXG9hYdellNr72i5jffUNORvJpfeqeq7Z2Ntursmkt1duXlOnPp5Tq96v06c+ElbnzUvNnvAcZcAgAAACBU2ofJdhVbJu+pZjutRw+o5dd71DK2Xy27Dtbls59ZvUxnrunQmSvW6NRV61RoW1rN5sYkJZgtFgAAAAChsvJA2S0pJYddXVuPHlDbL55X8569aj5wvOH7c/qjazR1zfXVBsxtKnaJpdUSAAAAAKGyRKAckMPWycUvPau25/696i6ttXI22qqpD12jyQ9/QqdXrHayiTFJvfG+2DBFFwAAAACh8tww2SlpUNJ6qx2YnNDikSG17fyJmvKnfXPgpzau0uTHftPpZD/3xvtiAxRfAAAAAIRKzXR3HZQUDXqYdDFc7lBxrCXdYQEAAACEN1SapUIesXnPkt05tT015OswOV+4nPhUn+3MsSOSugmWAAAAAEIZKtOZbErSlkpfv+jQy1r0/e96dsykG0723qaT67tsJvTJm2DJOEsAAAAA4QmVNoEyMjmhpT/5ntqe/GkoTsqZ1ct0anPMpksswRIAAABAQzR5PVC2Hj2gC9N/F5pAKUnNB45r6dce1bIfPl7pW6KSculMdgNFGgAa8r3WHtD96uTsAgA8FyptAuWS3Tld8FcPB7q7ayltT/5Uyx99UM3HDhMsAcC7watbUiKggTLBGQbg8XtVO0chZKHSJlAue/oftfixp0N/gppfekcX/O3faNGhlwmWAOBNGyT1B3C/kmbfAMCrgbJXEsvshSlUVhooI5MTWv7og2rb+SJnZ/ok5U9r6dce1eKXniVYAoA3Q2WHmc08KBW1TvOdzXcJAC/rl7SF1sqQhErzRVs2UE6Pnwxrd9dyljz6L1ry/JOVBstBLjAAqFuolILVVXS65bWD0wvAi8zQg6459yw0SEsdTnivKliHsvXoAS39xjcDtfZkLSwe/JGa8kd1/Df/sNxLOyTlxFNmAKi19ea/XelMtjveF8v5vKLWPjsgB2GfAB9ehxskdZp6XHuF9blhSeOm/jca74uNBvwwJWf9f/+cP6POarqkiLkgciq2nBEoXTS56fpKgqUkbY/3xRK+L6iRSKe5uVZqtFAojDb4M1f6JVDxZ45EItNfLraGC4XCuEfOpaN9KBQKOZePty/KkQvnvhE8c9zqUPHrlrQzSPfcdCablPTArL/6bLwvlmpQpdovZX443hcbd7iftt9vVoL0QMBBmXB8Xhq0b72SuvVe61u1xkw9PCdp0C/HwuK62T/nrxtyr6piH2zrLeNeXj6wpcYHKuV2oIzeuka3bP+u1Wd5dsvvKP/jfXU7qFMbV+nY7/+J1Xsu/OeH1LLrYMWvL445/cdKguWWdCab89NFtoDEnEpOOVvV+CdWG+ZUNt34zJ2SHnfwWbxwPKaD0S4Hb91hvhTdPN6Vfubp/x3RrCfAknJ1Dk4DLlY0as0T5a2O1/nce27S5y0EiTL7SJk/36YK7lFufb/Z1sem/zevYkvWqPkZNqHLT2XVtkxUc17qFST7TZiM1uBXdKg4/GyLpEfSmewOEy79XifUAt8xSZM9/PT9YVNvGTIPHcIVKs2JXV+ysjY5oSVPfIcWSofadr6os5fldGJd2fI1kM5kh738dAOVKRQKg5FIZMhBReuBSCSS8kDrkdMZ2rwwVmKmi+OswDkmaVDSQFha5lA2VE6XV1+O7zFzIHR4JFTCXVFz/+qac84D25rl8essqfqPWe6R1JPOZAdM+Brw4wOwWROJnRei05lsb7wvNkgpq7+mGp3sXkn3lAuUTMpTvcWPPV3JrLBR+evJDWoTsBo65XYkEumVs1aHrR4ObB3mXrc/EomkTDdthMt85zzh44nSkhUGZwTHdGvWI5KOpjPZwSDNZOy1MJnOZEfNsW7kJFjR6e+udCabMiHN7/epautI8FqonNXttaSlPxgkULpkyaP/otajB8q9bL0ZJwOfKxQKw5K2O3hrTyQS6W7gR3cSavPyz/pTW0y4HDDjOxEOXQtU2HxXKTfjQ+er6EaZTTxUelTsKjmezmSTnHt3ri2PhMkFv7v8cq7NZ+wtdU9mWb2AhEpTASzZL3zJ7hzrULps6Te+qcjkRLmXPcCFFhj9JnDVI9hVLRKJJB1+kfZ7ZYIhC/dIypnxowh2RXFDmWvUb5Il/o3yHD5RFcd6jhIunQegdCY7qOK4Oa8vz/OAT67zfpUff0prpd9DpXnKWXI9ytajB7T4sac58m6fyPxpXfC/U54NFXCXCVpOzuX6SCSSqHOgbHd4gx8pFAopn56i9QTLUCh1fjvMUBC/VH67Vbp7ejenO/ThcthPZdoj19Soii2/cCmkV1if2OLDLr2EyjlKVgAjkxNakv4WR71GWnYd1JLduXIv62KsRGCCZVLF6cJt1bt7ZtneCwvw+5PGKMEy1KHSb2W43PcCFTR0SHrcjLmk1bJ0+Emq2DoZ5Wi4fp+q9JjSWunXUJnOZPtVpml/6U++p+YDxznqNbT4sacrGV85wBdCYDi5aUbrdbM1gWqLg7duL7cuJcESPgiVvhjbU2ImRUIl5tOjYpdY7mvnX0vt6Uw2pRouEUOdp/IASl3Xh6HSnLRkqdcsOvSy2p78KUe8DpY88R3PhArUVqFQGFRx3SJbD9RpplKnk/MkA3SaopIGmbwnkCqZzdgP99qkS/uK8IhK2kXPp/Pqwjk5e5CK8sc3Ibtxqb6cMC30oVJlBs1GJie06PEsR7tOml96p5JusP08wQkMTy4xUsUSIkFc87FDLOsTtApOpa00nh7bU8FMirNf28mZxxyPmDUPCZTFQLm+Rr9iSNIOSVsX+BmSNBLww5ysY/0IDrS4dCGVPGmLR4bo9lpnbU8N6eR1N6nQtnShl0y3ViY5Wv5WKBSGI5HINpVZG3YePZFIpLsW3UxNq5yTisaY6jeZVF7ScIl/d7tlpicSifSa1mX4n03Xv4SH77WVzKQ4e59HOfWY4550Jtse74slCJSu2WG2mYv3xYYtP0+3ihNr9dYw5Nb7GHfL2ey5HelMNhHvi6W4TH0QKst9ITUfO6y2nT/hSNdZU/60lv5gUMd/8w9Lnrt0JjsQ74uNc8R8Lym7AezTBlSbKcTLjrFeaD/quITIcKFQ6K4gIHebY9St6mfxG5BUr1C5KSDjUoMQKj15r7WYSXH2Pnv5ocimeF8sDGV+a7wvlqzg/HaqOBa23Zy76Z9aLG2xJZ3JKqTB0q1AOf1QNVXNvcJcAzlJSVMG+h3WD7xWx3EqIXoK+SZUlryBLH7u+2rKn+ZIN0Dbzhd18ubDOnPhJQu9ZLq/OcuM+FyhUBg3a0E+aPnW9ZFIJOHm0h1mrKaTLidDXlxCxASznN6bNbfXfME5emrq9vGGL0Jl1JQbr533XsuKJhOz+Ei8Lzaq91qWB+c8TOjVe61ZboWNLelMdjzeFwtNl0MzKU+1gXJMUrIWrWmmDPSb2Wir+e5q5DHuVnU9h7rSmWx3SB44NVRTlSc6UapwFlspX+QoN9Di575f7iX0Nw9OsByQN5YYSSqgS4gUCoXxQqGQKhQKnSqOY3F6fOB/XQE477afqZPTHoiwOR7vi6XifbFEvC/WLumzcjbh23zuCcvkPWbVg2on5dka74t11rp75vQ5V/HB0FYVh3/4Rb9HtoFahkpV0EqJxmrb+aKajx0u9ZIOFjMOFCdf5q7NBmy6ijpdQmTYTwfarBO6ycGXcwdLjPi+MukkXHnqXutgJkUpIOOzcF7gSMX7Yt3mfuZGuHwk6MuNmNazB6vYxIikjZV0Y65BuEyacLnDJ/faHhc21cNEYx4OlebkLPikNjI54YtWyuita9TxxR6t2/ZlffgfvnreT8cXe3TR7Tf4+iRXEO4TXArBYLpqNnKJESdfkHn59CmiOd5Orh+uOX9zWmH2Ujl3VAZNZRrBDJe5WeFyrMrNDQZ1hnmzX6kqNrFdUrftBDwun+vReF+sV9JmebvVMunRbWEe1YypLDvjq2d3esUi/cYf3KErP/0ZLb3q+pKvveTjd0mSJsff1r5v/rVef+wpTR095a+T/LNfKfKJiVIzwfaYmduYsCcYEpL2O3jfgCpcWmA+kUgkIedLiPi27BUKhcFIJLJddi20VMzDGSq70plspxnn1MhKcbecj1Hq5PQHP1yalsYBOe/e2WEq8UHsdpiS83GJ2700mVG8LzZozvWgPNYTwTReubnmZy913dqqpvtrycpn6wve7MkWvXWNPvrYP+q6+75SNlDO1tZ+qa677yv6+Pf/j6K3rvHXSc6f1qL9u6s6n/BVyBmVtM3BW3tM91UngbJdzp4CjplupH5nuw90I/S37jqWlVqopqJPqAxHsBw34efeKjZzT9Bats3+OO2OudWLs+OaVssNKrageonbDySiYmyl90Kleaqx4FOaxa/u9uS6lJdtvlm3bP+uVZicL1zesv27umzzzb460W3P/TuhMlySctalxelMwE6XEAnEDd4EeavxKU4DPDyhmvFiWxrZLdCFMUqU23CFywEVJ/JxKmizy6ccvm97vcdPOjjXCa/MkGrukbUI4IRKr4XKcie6+eWfe25HL7r9Bm38asq17X3wS3+pZWtX+uZEN7/0jlqPHij1kp6gjn8II9Od1MkX2HrTjdUmHHXK+RIigwE67Lb7wvXmQyaUVbsEQyMrNtVWbJlkKnzBMlVFsFwflNlgzbIcTh6ebg/p+p3V3iNrsa5mNCyzE/spVHaX+seWn/3KW4HqgqVa/+cPubrNtvZLdd29/81XJ7vtF89XdV7hu2BZryVGkg5v/kG7sY9SOQ8FN85bQ0KlS2OUojyADG2wDO0ySqbMO7luR0TrWL2OdWjKY2BCpTnZC44HWvzqbjXlT3tqJz/437eqrf1S17d7ycfv8tX4yuY9e8u9hC6wweMkuFU87qCKJUS2mS6jQTJMcSNUWgSzhE/uB7U6BvBfsEzK2TIUHQFoHXLScpaX1MvEMNZ6LY913kF57OYweyBUlgseXuz6Ws0YynIuv+MO/4TKA8fLrVnJRRYwZskLJ5WASpcYSTrYdl4BfFLo5xlsYcWt+2RdrwGXn/4TKsMrIWfj9YMQKq2v8UbP9OxTtvfGAdlPMpTkMHsjVJb8Mmn+1VioDuClt3zSV5+3bX/J0N/B4rCB5LQiWXKChSqWEEkSwOBjbgWqej8tT8i9MUqEypAyrW5OAmKXX+sXppXV9toZMpMcwf5Y245bHZD9hFBd1He9ESoX/BJsPnbYk7O+1tLSq67Xos7l5x7UfW979vM2v7qPykLImG6mTsbCLLjESJVLiATyi5bZXENR4emUu5NH1HOslZu/i8pYuIPloKQhj5f3Rl87SUpKXY71drP8zbCDMsk58kCoXHA8ZevBX/tip9/4129r71//z5mfsX/6mibHnQfBxasuOfegemxM6Tmh8sXXCJXhNCB3lxhxuoRIIsDH2HbyElpr/cftMNVTj6flDp/+l9JFUQg9JxVy383bYK5P23WFh7yyNIfPjnW3g2M9uxymLN+7hdbKBoZKsz7lwoHltX2e3tn87h/qh5/epN33/LnGHt4x87P3/of0zO2/pfzuHzra7rKrr6r6s7XsOlifE54/zbjKEDLdTZ08bT1viREz1vIBB9vaYcZ4BpXttcPEPsE/x7WqnNtyvYWIyli4mdBk2zLkxyE2vR69pnlQUWylHJ1VJlOyn/E+wWFvUKhUmVaspnHvPnif2P+iXvj8n+r4nkPz/vvU0VN64fN/6qjFsnXZBb466a1HSwZYKgrBDZYpFac3tzV3iRGn3VeDPq26beVjlFLpO7XoydFbyyU6HD79b9SxgL+k6hTSGsk2dIzQSun4PtXlQvmzrZ/0s0RS40JlycBRr9Y2J176269q6uipkq+ZOnpKr33nG678vkWHXvbssYi8+Xqpf+7gsgg0J8FuZokRM26wx8E2tgZwCZH3rqlia67NtZMP8vEgVDq7vmok6aNjAR8xLUO2wyq6fRR0Si6h52LQhn14X6iLsW2ZjIrl9FzT4taXSJkulQ01sf9FvfX4cxW99sjwLl1d7wrp5ERdf1/zm2+Uu5FuMIOeETCFQiEXiUR2OAiGD0QikZSctVLm5bx10w+B0smkRbk6fLSBSCTSiO4jKdMqHiimglmrh26JWoQ/M2SlVuMfvRgqB9KZbEPKvAlYYTQou7WK/fQwotvh8YDdfapT9utdz1uniPfFxtOZrG2ZTPIwoDGhcsEm4pbjRzy7k28/+28Vv/bM/5uo++drO3KgvpXgkycdn2cfeSASiTzAJT6vfjlrbcw5rFT3B3wJkZSD41KPULm+QccjF9DzXMvKcEc6k03UIJjUsgW004PniDLfmH23qcB3BPiaH2FdSkeSlq8fMzMQl9qeVZms0f03dFzr/tr0lne7vp44+HrFrz3+y9eDf9LLL3lCt6YAq2KJESeVgZEgtlpJxRbKSCQy6DCg8zTbf7otX287iYmrAdDh03+bBcTXUyTgJFDXeX3Wel7zOYpDXe5TJUOoCfY7LLeZ4GzUP1QuXKk8dcKzO3ls3ysVv7bcuMuKD+xrr3j3pJdf8oRBy8E3IPtZ0pwI5OQ8ZmzpsMNAuYPxlL5k87At76CSst7lyrbttTdkWyn2UThAjZgKvO24Sr/UMToJlTVne5/MV9iiaDvkpov7Wf1DpS81okurVQV18mQDfucEpT/ETHfUZI1/zfYgLSFiWiYTkUhkWNJOOe/GNUAJDHyoHHb4tNyVhzBm/KdtZS0p+xmJOykWkP3ySH7pDWV7jx+lKFjfp2zveRV9f5pJfFhexK+hstzkLyhRWX2z/l1u6z2OE54MlinZd9GrVF4+XKsrEol0z/lJRiKRARMkj0p6RNV1+wv6Wp1BrvzYVDCHHT5A6HFpHb9+FWc1rNT0Mgi24YBQCSeh0g/XvHXZZoLDmt+nbCf9s62DbGH9XY+EygomfwnXgT3l6+PBmMrwqFXwG/BBF8+uSCRSmP2jYgvk7J8HJN0jd8aP5RX8tTqDyvaeOGoqmTnZrw3rRhlx9PQ/3hcbl11Xxm6KBiTZTsTmh4q77WfMUwxqfp9KmXtUpSE/5eC8JDktdQiV9DW2PLCHfN1yy5jKkDCtZttd3uyY6OI57xcoYyl9y/b7b3huYLOQqGYx7nQmm5Dd0/+xOWOUbFpbOikaqENg8wNaKWt7n3JyL3Xynt5q7r+ESjT+JOSPchDQSEm5+5Q1GfAlRJzYHtRZcKkEn2/2otwmsNmM7YmqurE9ySorXTaV4w4qYBBjCVH7+9R2h8u12H7vRkWPorqESiqJtToJ4xxaNI5pPXOrZXGI8DRvoExwGHzNpvvrmAsVG0eVmnQm2yu7sZ/5eT7beA2PDQiVCDnTSmk7CZKjOooJora9sfi+rnWoZACyncjEKT9/fFJu+Li1xAhP+AiUQWQzpnZ0gevLpjdAh6l41fr6G5hnjFKOUAlLtFajlqFtqMoMkrJ8vdP7b+i1uLWhwuLFHM1Zml96x88fnwcIIVMoFMYjkUhSxdlNqwlQlJ33bC0UCslGBlo1pgUhF6ST6GA+gfP2P94XG09nsoOyW+Q7YVMZMp+zy4XKlm2Z6fTQ6aLMNwYPFlDL+1RV36PxvlgunckOWf7efgdhlFDp1obOvO9ytWofR9SByNv1nzTs1MoPBP2wbvfADWGDpAd9FCxTkUgk4eCGLzGz6WxjkhIeWDokxfIlDakwD5eoGNmEyq50Jts9e3xmBSHU6h453xileF9sNJ3J+jVQpCyOFxpnNID7RGttbQLimEvXdMqybrPe8v4LN0MlnGs+cJyDUIMvrUZXqCORiF9v+DsdvG+AyXmKa2g1uHUSjQ+V81aYTVizfVqeUAWtYGZttS0OrvWF2HzOLopI6LW7cY343HqKQdn71AbVuZVy1v03lc5kk7Iby5kUyyZZsZ39dcH1tgrLV3A054aKyQlPfq6z0dYw3vBRAYdLjIyFPEiNSdoqqZNASagsM/bHtnxUuhi37XaHysykaPUdwILhXCMB3CfroRzMhFyWbW+mucsdVct2W13c22obKhdsiShEL+JoztF25IA3Q+WaSwmVcPPGmwrhMRqTtE3SpkKh0FkoFFhGJbhsWiCGygTOnOwnxOqvoCLrZiulk+8AKl7hZnv+PT/2fp4JrMIarl3hsDeF2+td206YVsm9ElWEygW/aKaWESqdaD52uP6hsr3swzQqx0DRiAkKO1Rsjdws6SoTJPsZsxj4ilC3W9+RVVRSEmVaQGyf/g9VME7IttLfTWkJdViwXR7CL3WMEUKla2zve/Mtd+TGg4JBy7dtoQW6AaHyzIWXcDQdaDl+pP6hMrqi3IXHDJ4Ig6FCoRAp87OhUCh0FwqFXtMaOWjW9UQ4uDKecs79NSW7p+VRSb0LVOjbHYTKlBv7QWUaTh8o+Gjyk9FaH4uQPHhw0ptiwGFrsdvhVmISwsozjeXrS4aNqY2r1LLrIEfViOSPSCu997nOXnF1qX8e48wBgKOwVGlleUDSA5YVofnCYK8JnZWqaIxSvC82bDkDbCdFhVBZaRn00b4NS+ohVFbNSSjrNBPr1ELe8r7Zn85kaxVyQx0qR0uGlZWXSyJUTmvd8zNF3j1a8jVN+aN1/1xluiqPcuYAwFFYqrSXh22o7Ehnsr3xvtjgPGHThs3rR1T5eFJmvgyvXsvX53y0bznL6zS6wHUaWg57U0j2LZu1NN1bJMUZLc2q+2u5bpFnLr2cIzo7se86qMWDPyr507bzxbp+pjOrl5XrqpzjzAGAJLvp7/OVPsk2r7OdZbl/TmUtIbuxbHnZjScataw80gU2fIEhIbsWH8kHk/RU+Vl7KRnn3beiAdiPJKfS5VBpLDi73elV7+eIetyZa8rWQRhPCYAKs31Isr132lZSuuZ8Jtun/7bdt2z3h1AZPgkH7/FNK565XoZsQyUTu1RdRryoI53J8sCgBqFywS+aMxdeojOrl3FUvRwqr1hT7iU5jhIAqKah0qwTaVth7TeBt1v2XU5tp+cftXx9J0UmPEwZtF3IfqzM+qheZBuCo2Jil+kykpD9zMBexnmtQagsGTrOrL2Oo+phZVqTRxiIDAC1D5VG0vL1W8wSDrbv2+7g3m5b+e+myIRK0sF7Uj7cTyctq/20VjouI17WRTf/eofKK6/mqHrUmWsvZjwlANQmVNqGsOmlFWxnw0zKvoUo6fCz2eikyISDaYHqcvBW34VKhz0KQt9aabqKdgRw12itdDNUmqedCy4Ie/LKdTobbQ31QT0bbdXUxlVWP/U4Zqc3bKzqgQEAhIhVpbmKtfdsA5/trIg7quhyaBN4O2idCUVYaJd9V2pJGvJh19dqwvADpldBWAU1fG0J+XktqaWKC+zBhf5x6kPX1H1WU0+FyjWX6tjv/4nVey7854fUVOM1PievuqFcpYhpsAFQcbbv4jRSxa8bNJX0Ws2QOFDFe0dl19qwQTycDLpBh2V1wK87HO+LpcyaiR0OjlXouks6HG/rt8BMi+U8mhy+r+SXxtQNH+HIeszUxlXlur7u4CgBwEw4sg1fTius4zWscA9V0YJa9rveheMGf4WFlMOwMBaAh9ZOrtH16Ux2IIRFJRnw/UvQK8PFUGnWq1ywW8yplR/w/CywizqX68KbO0Jzok+v/VC5l9BKCQDOwlG1SzGlarQf1W7XdnKfTopOYANlv5wvSB+EVp2U7Mc/S9I9ZgxqWMpJp4LdSikVW+oT3BXO11LFewdUogvs6ds+qubHnvbkTi9bu1JXf/5zamu/WHveTOrU6LuBPslno606ee0tpV5iuyg2ABAqXQqV8b7YaDqT3V5FpX0+Y/G+WLWhkrUqMT0xz4MO3z4UhKE18b7YuOkC+4iT+nI6kx02DTJePs/t0/taxWaSTsqIR+75Nt26++XjLt1eDJWDpW4yJ6+7SW3RITXlT3tup6/+/Od0+W//kSTp2nvf0e57/jzQJ3ly001lzyVLiQDADNsn7W5UFgdcDpVJF7YxXOPjBu8HylSV5TIwY8/M2MqEg3IelZRLZ7LdXg2WJlDmzPnKOdxGp4OyMhTvi3V7YP+Tkh6weEtHOpNNuPDgLlCaqri4RlViHF6hbWklYaZuFnUu16o//qQ+/A9f1cTrr878/cTrr2rdti9r1R9/Uos6lwfyJJ9cX/b+x0UBAHI0SY/cmNXSVDbdemLvSu8T87Ax76BiCf9fB+3pTDZXZaDc6vXWuTqG5NnBFlsAAA/YSURBVJlg6cFz3a3iuPD1VW4qWaf31EKqjmWBUOnkJJxc3+WZ5UVOjb6rtfd/XZd8/C5d/YUvzfz91V/4ki7/7T/S2vu/HshusJObrlehbWmpl4xVOZEDAASJbShys+uWW92pBlzsfTJc4+MH74WMXhMyqml5Hon3xZJBOzYmJG+tIlju9NIYS9NCt1NVzj5tWjp7Ld/mmfqneTC43fJt6734kMC3odL0k19w4LKXWiujt64p+5pla1cG6uSejbbq5M23l3tZkssAAGbUe5Keir9TGxBOnewflSz/hsnOdCY7KOnxKkNG3kHA8FOwTKq6h0mPpDPZVCNnEDXnOie7Lp+l9DsoM16rf6Yc7jfcCJWVFAqvtFbmf7xPz/+XuzU5/vZ5/zY5/rZ2/VlCx/ccCtTJndx0U7llRPL0BweAqkLRqMu/v9qK1naXx8gzA2w4wmRK0n5JPS5ssteNLuEe1yvLruFzbJE03IiWLjOT77BcGgNtwrFtuBrzWv3TtJrarjncQ5d/F0OlKRSlWyvv8MbY/SPf/7l+8Rf/47y/H/nyn+itx58L1Ik9G22tZCwlM1cBwLka1lJpDFZZWU26/HlyNT5+aFyQTJjWqv1yb5Koz4ZhSI15cNNd5bXaoWJ32MF6BJN0JtudzmRHVZxkM+riphMOtpfy6Kl1Ui9OCpKqm/117gFdcJrlE+u61Tq8S80vvdPwHc7v+qX+7aYNet/vfkySdPiZ5wN5Yk/FN5cbS5knVALAuRVtB5UjV0OlWbpgQM66pe2oQQuR7fbWU5I8V643SGo3IajT/LcWC3V/Nky9n+J9sWHT6vdIlZvqUbHFa7uklNuh3IzhTKh2szPbtlJ6tv5pZvgdsPwe6E1nsu2souBSqDQnIVnqJnXirrt1wUsPNz5szZmMJ4iT80xtXKWTV64r+yCACwAItIFIJNKoazxVKBT8WLm0bWUbq9F9NOUwVLpeUTNraFqHmAbN+jmQzmQbVeb767jPiQq6TXbWKDgSKM+v/8qFYCkVW4u3pDPZMRV7LAw6DZimfPSan5qVAxNYbbc/4PH6p+1DvagJ1sl6fEeZ3gU1+d6u9hpucfHDJEtdVKdXrNbknR9W25M/reqXHP/l63p2y+9Yv2euwz9YuIXSdvsnDx4+589N+97Whf/8kNU2mva97cpJOBtt1cSn+iqpCNFKCQRbI1uMcj49ZrahcrRGFdVR02ph0yVxqIbdDodk18rRKfe7BXu9zNdz0pWOOgdGAmX9guX0+b1H0j1muyPmXjO8wP11uiW60/zUc8yZkyDl9bLipKdIvUJltIbnt+rvjxaXL6pEqZ09/rHNat43WlU32Kmjp5T/8b6qP++psYVbKKvdflP+tJp2HWzIlXCq51PlJueZLvwAgHN1eyg8pyxDZS0fFI5aVmQ2yIV1MuF5eRUn5cmF/UDUIFjOtt78TE+i9IAX9tm0hto+3Nju9UmczBAE24d60XQmmwj75JctLm+vX9KuUi84cdfdWnrom2rKn3b8SyY3Xa+m8WLL+VTHVZKk5jffUOTkyZk/S1LTqZNqe/KnOtl7W3Fnx/arZddBtaxYpPff+zm1RVdo/MXdmpqY0DvPPK+Vd2xS/qWXdGVvTJP5o8r/8hdqixYfPh5/7VUtu+JKSdL47j1qX7dWJ948pLcef04dX+zRiTcP6UhueGa7r/z9N3V8zyFdtvnmYth78221r1urQ0/t1Mo7Nmns4R266PYbtPp3epT/5c819vCOqg/+5KbrdfLaW8q9bMhMWw8AOD8M2YatWlVsculMttIWwrEa39dt97ObohR4IwrHLK+2wXLUPFCJhmCXk3V6TyMMyH7yqqS83wpbU00uX1DDkraVes3pFat1qudTVf2eqRs+oqmOqzTVcZVO3HinCstX6NRtt+vkJ39PheUrdOLGOzV53Y06/rHNmrzzwzpx452aev9anW0vBsTf+IM7dPmdcS1d3amWxUu09v6v69p7/6vWfO4+Xdkb08W3fkrtH9yo6LX/ofh3n/6Mlr//Gl1331d0yU23adH7LtV1931FH/zSX2rZ2pW67r6v6Iq7Yur8Qt/MdqddcVdMV9wVU/u6tcXX9fXouvu+okWdy3Xj33xHS1d36vLf+l21rFhU1TE5c+3FmvhE2WWh8ioO1gYAzOKFSXrmUWkFpdYVNdv97KREBdq2eF9sA4Fy3npwTsWHKiNB3k/TSmnbDXOHX8qMyTO2a5F2NGKJmMCGyllfbiUXbz557S2avPPDVf2Syetu1OR1N0qSWvf8TKdXrNbpFavVuudnxb87+GtFJid0dtFiSVLzWwdmWjclqa39Ur06mNarj+7Q5Pjbuvy3/0ivfecbM//+8t9t097kw3p3z7N69X89qneef0GSdPgnP1J+1y9ntnHF3Z8+77Pt+/bfL7jm5RV3f0GSdPHHi5/9hc//qZ6NfUFTR085PhZno606cdfd5WZ7lYqT8/AlAADns14Ko9YTs5Rbssuox3rDtt8bHRSnQBqTtCneF2MITfn7Qrek7QHeTSdlwG9zeTi5ryYJle5eTOOqoDXs+Mc26/RH17jyO1t2HVTzscNqPnZYLWYs48lrb1HL8SNqfaH4nX/mstUzLZWvP/aU3vjXb2vt/V/XRd0b9MaTaUnSa5kdeuXvv6l39zyrG//mO1q2duV5v+uSm25Ty4XvhbfL74xrcrw4yc7oNzJ6d8+z2vjVlC66/YZ5P2tb+6XF0LvsAkmqKkzOhPTP/EedXrG63MuGmJwHAFwLlfVqiRio8t/dqiRbCfsT+4DJS9oa74t1Mn6y8rpwvC+WkLRZ1a1l6TmmV0eP5duG/FZ2KnyoN1eXWcKHUOniichJ2lo2WN4e15lrL3b0O9r2Pq+2ve/N4Npy/Ihajh+Z+fOS55/UBX/1sJoPHJdUbKmcdlH3Bk28/mrxde9bqRMHi7PDnhp9V5fcdrOOHxgtbvPC81v+Dv/kRzN/f/iZJ9TWfqne+fH3Zrab37tHkrT4fe87/73PPKGJ/S8W//9Hz0mS1m37stZt+7Iuuv0GdXyxx/o4nPjM7+rUyg9U8oXQKwCAW6GyXrObpkpUSuu53ttIjY8nPBomJXXG+2JJDoej+vCgit3Bt/ng426r8L7mpCykfHoKnXzu0LbkN9XwQkqqTH/kQttSHYv/Z+tg2fTWQTXlj6opf1SLDr383t+9VWylXHToZTXlj868ftGhl1WIXqSzl62aCZJX3P0FHX7mCb3+2FM68eYhHX7mieK/rfoNXX5nXG/867eV//E+5ffu0Yk3D2nq2IQOP/PEzJjKw888odeeyOrwM0/oUO5p5ffuOWe7b/3LD4t35L17ZraR37tHr3zrIR1+5gkd33NIr3zjL3TxrcXxpRdceaValy+3DpQVTMwjSQnWpAQAV0PQaJ0qpeMlKjaDdby32+5vJ0XKt8Yk3TsdJqk/VH8Nmy7DV0na4cGPuF3SVfG+WH+5c21aKW0nsBnz8ayoTh7abTHHKXRaarz9XvNFFC0XLC9M/13FS40sfuzpmf9v2/nieX+39GuPnvP6uX8ee3jHObOtvvX4c3rr8WLL4d77H9Le+99bY/LXX/3WzP//9D/92TnvOee/em5m27PNfv/0aw5+699m/m32v9coUG5ltlcAKFlRapf9OMBcnSs298zz98k6foZh2XV5o6XSX0ZUnLV0sNZjhUMcLkcl9ZrAkXQQztw03cshZTnXhpNWuKSPz5mT5UWk4jDAZNjKeFOtT4aKg5VL9id32mIZVhaBcgddVgCgJgFotF4fzlT65k76Ue/13kbrcExRH0OmPG2VtEnSCjOba5JAWZ/r2Yy3XKFii3A9Z4rdIWlzvC/Wbs53xde1efiWsA2vAVi70UlrZb85XqHSUoeLZzidyfarzIKwTlosCZQljShYy4fkavz6WlXCtvrsM4fpc4/67BpIeehY+6VM2LApO2rATNopnfu0PNWAc7qVMl+T+0muDtfheEACo22Z8Ox93jS8DEgaMK2XvSo2xHTLvXUuR8zxyrnQa63dQcDyfZkzOeZes/+2x2u8gvK51SO7WvV9KFIoFOrySdOZbKJcsJSkyOSElv5gcKZbK4rORlt1Kr5ZJ69cV8nLxyRtYBwEAARHOpPNqbg23FC8L9bNEQECe613qjguuduEk9kt/11z6nujc4LBuKRhZupFvdUtVJqLJKUK+yUvef5JLR78EWdI0pnVy3Qi/seVLBsiFbsad9OFBQACV9FMqPhwdjNj5QEAoQ2VtsFy8au7tSj9uJryp0N7gk5/dI2O3x5XoW0pgRIACJY5WikBAKEPlbbBsvnYYS39XkYtuw6G6sScjbZq8o4unVhXcd2BQAkAAAAgHKHSBMuFpkif15LdObU9NRSKVsupjat08pO/V2l3VwIlAAAAgPCFShMsE6pg8p5pQW+1dNA6KRVn9upmUh4AAAAAoQuVJlj2qjhFdMXTJy869LIWPZ5V84HjgTkRk5uu18QneisdOzltSFIvgRIAAABAaEOlCZYbJA1K6rB53+KXnlXb/93p63A5uel6nbz5dp258BLbt26L98X6KcIAAAAAQh8qTbBsV7HFssf2vX4Ml1WEybykBNPJAwAAACBUzh8u+yUlZdEddtqiQy+r5ecvqG3ni5482GdWL9Ppj2zQyfVdtt1cp42o2N11lKILAAAAgFC5cLDcoGKr5XpHOzU5oUX7d6t1z88aPqnP2Wirpj50jaZu+IhOrfxANZvaGu+LJSmyAAAAAAiVlYdLx62WcwNm82v71Pyrsbp0kZ3auEpTHVdp6v1rbZYFWciQpH6WCwEAAABAqHQWLDtNsNzixvaajx1W69GDirz5ulrG9ivydr6qoDm1cZXOtrfr7GWrdPayVdW2Rs6WN2EyRTEFAAAAQKisPlx2m3DZVYvtNx87rJbjR2b+3PTaK+f8e2H5ChWiF8382cXwOF+YHJA0wFIhAAAAAAiVPguXDUSYBAAAAECorHO47JeDJUgIkwAAAAAQ8lA5K1x2mnCZUBUT+jTAkKQUYyYBAAAAECq9EzB7JU3/eDFgjqi4VMoga00CAAAAIFR6P2B2m5/1DfoYeUk580OQBAAAAECo9GnAbDfhcsOs/9aiJXNE0vD0T7wvlqOIAQAAACBUBjdobpDUaX5kAmclRs2PVGyFHI/3xYYpTgAAAAAIlQAAAAAAVKiJQwAAAAAAIFQCAAAAAAiVAAAAAABCJQAAAACAUAkAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAABCJQAAAACAUAkAAAAAIFQCAAAAAAiVAAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAIFQCAAAAAAiVAAAAAABCJQAAAAAg0P4//U9DqP/Pc9YAAAAASUVORK5CYII='
//                            } );
                            var subTblHead = [];
                            var subTbl = [];
                            $("#past_claims_totals thead tr th").each(function () {
                                subTblHead.push($(this).text());
                            });
                            subTbl.push(subTblHead);
                            $("#past_claims_totals tbody tr").each(function () {
                                if ($(this).attr('data-print') == 'true') {
                                    var tddata = [];
                                    $(this).find('td').each(function () {
                                        tddata.push($(this).text());
                                    })
                                    if (tddata.length > 0) {
                                        subTbl.push(tddata);
                                    }
                                }
                            });

                            if(orientation == 'portrait')
                            {
                                doc.content.splice(1, 0, {
                                    //margin: [0, -10, 0, 20],
                                    margin: [0, -10, 0, 60],
                                    alignment: 'left',
                                    width: 120,
                                    image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA5UAAACnCAYAAACB8njoAAAACXBIWXMAAC4jAAAuIwF4pT92AAAgAElEQVR42u3dfXBc5WHv8d/qzW/glXmLcQ0SJgQuwW9JeAkkkTxpCbSlkrNF3dtp481NMjfT2zuIy72dO8kMrP9oJ5O2gzxJKZ00ZU0y7bLpXuRLU0guvV6FJIVAYslOiCHBlsAYA8bW4mvZlmXv/WMfCVmWdvc5e3b3vHw/MxqwvXu055znnH1+53mLFAoFAQAAAADgRBOHAAAAAABAqAQAAAAAECoBAAAAAIRKAAAAAAChEgAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAAIRKAAAAAAChEgAAAABAqAQAAAAAECoBAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAABAqAQAAAAA1FFLWHc8ncl2S+o0P+2SNph/ape0foG35SUNz/pzbtZ/R+N9sVGKFAAAAIAwiRQKhTAEyA2Sus3PBkkdNfx1QyZ45iQNEzQBAAAAECr9FyI7TYDsNf+NNvDjjJiAORjvi+UocgAAAAAIld4Mku0mRPZr4e6rjZaXNCgpRcAEAAAAQKj0RpjslpSQtMVnH31M0oAJmOMURQAAAACEyvqGyYSkpGo7PrJetktKMv4SAAAAAKGSMEm4BAAAAECoDFOYbD52WC3Hj0iSml57peRrC8tXqBC9SJJ0auUHCJcAAAAACJUeD5MbVBx72OXG9hYdellNr72i5jffUNORvJpfeqeq7Z2Ntursmkt1duXlOnPp5Tq96v06c+ElbnzUvNnvAcZcAgAAACBU2ofJdhVbJu+pZjutRw+o5dd71DK2Xy27Dtbls59ZvUxnrunQmSvW6NRV61RoW1rN5sYkJZgtFgAAAAChsvJA2S0pJYddXVuPHlDbL55X8569aj5wvOH7c/qjazR1zfXVBsxtKnaJpdUSAAAAAKGyRKAckMPWycUvPau25/696i6ttXI22qqpD12jyQ9/QqdXrHayiTFJvfG+2DBFFwAAAACh8tww2SlpUNJ6qx2YnNDikSG17fyJmvKnfXPgpzau0uTHftPpZD/3xvtiAxRfAAAAAIRKzXR3HZQUDXqYdDFc7lBxrCXdYQEAAACEN1SapUIesXnPkt05tT015OswOV+4nPhUn+3MsSOSugmWAAAAAEIZKtOZbErSlkpfv+jQy1r0/e96dsykG0723qaT67tsJvTJm2DJOEsAAAAA4QmVNoEyMjmhpT/5ntqe/GkoTsqZ1ct0anPMpksswRIAAABAQzR5PVC2Hj2gC9N/F5pAKUnNB45r6dce1bIfPl7pW6KSculMdgNFGgAa8r3WHtD96uTsAgA8FyptAuWS3Tld8FcPB7q7ayltT/5Uyx99UM3HDhMsAcC7watbUiKggTLBGQbg8XtVO0chZKHSJlAue/oftfixp0N/gppfekcX/O3faNGhlwmWAOBNGyT1B3C/kmbfAMCrgbJXEsvshSlUVhooI5MTWv7og2rb+SJnZ/ok5U9r6dce1eKXniVYAoA3Q2WHmc08KBW1TvOdzXcJAC/rl7SF1sqQhErzRVs2UE6Pnwxrd9dyljz6L1ry/JOVBstBLjAAqFuolILVVXS65bWD0wvAi8zQg6459yw0SEsdTnivKliHsvXoAS39xjcDtfZkLSwe/JGa8kd1/Df/sNxLOyTlxFNmAKi19ea/XelMtjveF8v5vKLWPjsgB2GfAB9ehxskdZp6XHuF9blhSeOm/jca74uNBvwwJWf9f/+cP6POarqkiLkgciq2nBEoXTS56fpKgqUkbY/3xRK+L6iRSKe5uVZqtFAojDb4M1f6JVDxZ45EItNfLraGC4XCuEfOpaN9KBQKOZePty/KkQvnvhE8c9zqUPHrlrQzSPfcdCablPTArL/6bLwvlmpQpdovZX443hcbd7iftt9vVoL0QMBBmXB8Xhq0b72SuvVe61u1xkw9PCdp0C/HwuK62T/nrxtyr6piH2zrLeNeXj6wpcYHKuV2oIzeuka3bP+u1Wd5dsvvKP/jfXU7qFMbV+nY7/+J1Xsu/OeH1LLrYMWvL445/cdKguWWdCab89NFtoDEnEpOOVvV+CdWG+ZUNt34zJ2SHnfwWbxwPKaD0S4Hb91hvhTdPN6Vfubp/x3RrCfAknJ1Dk4DLlY0as0T5a2O1/nce27S5y0EiTL7SJk/36YK7lFufb/Z1sem/zevYkvWqPkZNqHLT2XVtkxUc17qFST7TZiM1uBXdKg4/GyLpEfSmewOEy79XifUAt8xSZM9/PT9YVNvGTIPHcIVKs2JXV+ysjY5oSVPfIcWSofadr6os5fldGJd2fI1kM5kh738dAOVKRQKg5FIZMhBReuBSCSS8kDrkdMZ2rwwVmKmi+OswDkmaVDSQFha5lA2VE6XV1+O7zFzIHR4JFTCXVFz/+qac84D25rl8essqfqPWe6R1JPOZAdM+Brw4wOwWROJnRei05lsb7wvNkgpq7+mGp3sXkn3lAuUTMpTvcWPPV3JrLBR+evJDWoTsBo65XYkEumVs1aHrR4ObB3mXrc/EomkTDdthMt85zzh44nSkhUGZwTHdGvWI5KOpjPZwSDNZOy1MJnOZEfNsW7kJFjR6e+udCabMiHN7/epautI8FqonNXttaSlPxgkULpkyaP/otajB8q9bL0ZJwOfKxQKw5K2O3hrTyQS6W7gR3cSavPyz/pTW0y4HDDjOxEOXQtU2HxXKTfjQ+er6EaZTTxUelTsKjmezmSTnHt3ri2PhMkFv7v8cq7NZ+wtdU9mWb2AhEpTASzZL3zJ7hzrULps6Te+qcjkRLmXPcCFFhj9JnDVI9hVLRKJJB1+kfZ7ZYIhC/dIypnxowh2RXFDmWvUb5Il/o3yHD5RFcd6jhIunQegdCY7qOK4Oa8vz/OAT67zfpUff0prpd9DpXnKWXI9ytajB7T4sac58m6fyPxpXfC/U54NFXCXCVpOzuX6SCSSqHOgbHd4gx8pFAopn56i9QTLUCh1fjvMUBC/VH67Vbp7ejenO/ThcthPZdoj19Soii2/cCmkV1if2OLDLr2EyjlKVgAjkxNakv4WR71GWnYd1JLduXIv62KsRGCCZVLF6cJt1bt7ZtneCwvw+5PGKMEy1KHSb2W43PcCFTR0SHrcjLmk1bJ0+Emq2DoZ5Wi4fp+q9JjSWunXUJnOZPtVpml/6U++p+YDxznqNbT4sacrGV85wBdCYDi5aUbrdbM1gWqLg7duL7cuJcESPgiVvhjbU2ImRUIl5tOjYpdY7mvnX0vt6Uw2pRouEUOdp/IASl3Xh6HSnLRkqdcsOvSy2p78KUe8DpY88R3PhArUVqFQGFRx3SJbD9RpplKnk/MkA3SaopIGmbwnkCqZzdgP99qkS/uK8IhK2kXPp/Pqwjk5e5CK8sc3Ibtxqb6cMC30oVJlBs1GJie06PEsR7tOml96p5JusP08wQkMTy4xUsUSIkFc87FDLOsTtApOpa00nh7bU8FMirNf28mZxxyPmDUPCZTFQLm+Rr9iSNIOSVsX+BmSNBLww5ysY/0IDrS4dCGVPGmLR4bo9lpnbU8N6eR1N6nQtnShl0y3ViY5Wv5WKBSGI5HINpVZG3YePZFIpLsW3UxNq5yTisaY6jeZVF7ScIl/d7tlpicSifSa1mX4n03Xv4SH77WVzKQ4e59HOfWY4550Jtse74slCJSu2WG2mYv3xYYtP0+3ihNr9dYw5Nb7GHfL2ey5HelMNhHvi6W4TH0QKst9ITUfO6y2nT/hSNdZU/60lv5gUMd/8w9Lnrt0JjsQ74uNc8R8Lym7AezTBlSbKcTLjrFeaD/quITIcKFQ6K4gIHebY9St6mfxG5BUr1C5KSDjUoMQKj15r7WYSXH2Pnv5ocimeF8sDGV+a7wvlqzg/HaqOBa23Zy76Z9aLG2xJZ3JKqTB0q1AOf1QNVXNvcJcAzlJSVMG+h3WD7xWx3EqIXoK+SZUlryBLH7u+2rKn+ZIN0Dbzhd18ubDOnPhJQu9ZLq/OcuM+FyhUBg3a0E+aPnW9ZFIJOHm0h1mrKaTLidDXlxCxASznN6bNbfXfME5emrq9vGGL0Jl1JQbr533XsuKJhOz+Ei8Lzaq91qWB+c8TOjVe61ZboWNLelMdjzeFwtNl0MzKU+1gXJMUrIWrWmmDPSb2Wir+e5q5DHuVnU9h7rSmWx3SB44NVRTlSc6UapwFlspX+QoN9Di575f7iX0Nw9OsByQN5YYSSqgS4gUCoXxQqGQKhQKnSqOY3F6fOB/XQE477afqZPTHoiwOR7vi6XifbFEvC/WLumzcjbh23zuCcvkPWbVg2on5dka74t11rp75vQ5V/HB0FYVh3/4Rb9HtoFahkpV0EqJxmrb+aKajx0u9ZIOFjMOFCdf5q7NBmy6ijpdQmTYTwfarBO6ycGXcwdLjPi+MukkXHnqXutgJkUpIOOzcF7gSMX7Yt3mfuZGuHwk6MuNmNazB6vYxIikjZV0Y65BuEyacLnDJ/faHhc21cNEYx4OlebkLPikNjI54YtWyuita9TxxR6t2/ZlffgfvnreT8cXe3TR7Tf4+iRXEO4TXArBYLpqNnKJESdfkHn59CmiOd5Orh+uOX9zWmH2Ujl3VAZNZRrBDJe5WeFyrMrNDQZ1hnmzX6kqNrFdUrftBDwun+vReF+sV9JmebvVMunRbWEe1YypLDvjq2d3esUi/cYf3KErP/0ZLb3q+pKvveTjd0mSJsff1r5v/rVef+wpTR095a+T/LNfKfKJiVIzwfaYmduYsCcYEpL2O3jfgCpcWmA+kUgkIedLiPi27BUKhcFIJLJddi20VMzDGSq70plspxnn1MhKcbecj1Hq5PQHP1yalsYBOe/e2WEq8UHsdpiS83GJ2700mVG8LzZozvWgPNYTwTReubnmZy913dqqpvtrycpn6wve7MkWvXWNPvrYP+q6+75SNlDO1tZ+qa677yv6+Pf/j6K3rvHXSc6f1qL9u6s6n/BVyBmVtM3BW3tM91UngbJdzp4CjplupH5nuw90I/S37jqWlVqopqJPqAxHsBw34efeKjZzT9Bats3+OO2OudWLs+OaVssNKrageonbDySiYmyl90Kleaqx4FOaxa/u9uS6lJdtvlm3bP+uVZicL1zesv27umzzzb460W3P/TuhMlySctalxelMwE6XEAnEDd4EeavxKU4DPDyhmvFiWxrZLdCFMUqU23CFywEVJ/JxKmizy6ccvm97vcdPOjjXCa/MkGrukbUI4IRKr4XKcie6+eWfe25HL7r9Bm38asq17X3wS3+pZWtX+uZEN7/0jlqPHij1kp6gjn8II9Od1MkX2HrTjdUmHHXK+RIigwE67Lb7wvXmQyaUVbsEQyMrNtVWbJlkKnzBMlVFsFwflNlgzbIcTh6ebg/p+p3V3iNrsa5mNCyzE/spVHaX+seWn/3KW4HqgqVa/+cPubrNtvZLdd29/81XJ7vtF89XdV7hu2BZryVGkg5v/kG7sY9SOQ8FN85bQ0KlS2OUojyADG2wDO0ySqbMO7luR0TrWL2OdWjKY2BCpTnZC44HWvzqbjXlT3tqJz/437eqrf1S17d7ycfv8tX4yuY9e8u9hC6wweMkuFU87qCKJUS2mS6jQTJMcSNUWgSzhE/uB7U6BvBfsEzK2TIUHQFoHXLScpaX1MvEMNZ6LY913kF57OYweyBUlgseXuz6Ws0YynIuv+MO/4TKA8fLrVnJRRYwZskLJ5WASpcYSTrYdl4BfFLo5xlsYcWt+2RdrwGXn/4TKsMrIWfj9YMQKq2v8UbP9OxTtvfGAdlPMpTkMHsjVJb8Mmn+1VioDuClt3zSV5+3bX/J0N/B4rCB5LQiWXKChSqWEEkSwOBjbgWqej8tT8i9MUqEypAyrW5OAmKXX+sXppXV9toZMpMcwf5Y245bHZD9hFBd1He9ESoX/BJsPnbYk7O+1tLSq67Xos7l5x7UfW979vM2v7qPykLImG6mTsbCLLjESJVLiATyi5bZXENR4emUu5NH1HOslZu/i8pYuIPloKQhj5f3Rl87SUpKXY71drP8zbCDMsk58kCoXHA8ZevBX/tip9/4129r71//z5mfsX/6mibHnQfBxasuOfegemxM6Tmh8sXXCJXhNCB3lxhxuoRIIsDH2HbyElpr/cftMNVTj6flDp/+l9JFUQg9JxVy383bYK5P23WFh7yyNIfPjnW3g2M9uxymLN+7hdbKBoZKsz7lwoHltX2e3tn87h/qh5/epN33/LnGHt4x87P3/of0zO2/pfzuHzra7rKrr6r6s7XsOlifE54/zbjKEDLdTZ08bT1viREz1vIBB9vaYcZ4BpXttcPEPsE/x7WqnNtyvYWIyli4mdBk2zLkxyE2vR69pnlQUWylHJ1VJlOyn/E+wWFvUKhUmVaspnHvPnif2P+iXvj8n+r4nkPz/vvU0VN64fN/6qjFsnXZBb466a1HSwZYKgrBDZYpFac3tzV3iRGn3VeDPq26beVjlFLpO7XoydFbyyU6HD79b9SxgL+k6hTSGsk2dIzQSun4PtXlQvmzrZ/0s0RS40JlycBRr9Y2J176269q6uipkq+ZOnpKr33nG678vkWHXvbssYi8+Xqpf+7gsgg0J8FuZokRM26wx8E2tgZwCZH3rqlia67NtZMP8vEgVDq7vmok6aNjAR8xLUO2wyq6fRR0Si6h52LQhn14X6iLsW2ZjIrl9FzT4taXSJkulQ01sf9FvfX4cxW99sjwLl1d7wrp5ERdf1/zm2+Uu5FuMIOeETCFQiEXiUR2OAiGD0QikZSctVLm5bx10w+B0smkRbk6fLSBSCTSiO4jKdMqHiimglmrh26JWoQ/M2SlVuMfvRgqB9KZbEPKvAlYYTQou7WK/fQwotvh8YDdfapT9utdz1uniPfFxtOZrG2ZTPIwoDGhcsEm4pbjRzy7k28/+28Vv/bM/5uo++drO3KgvpXgkycdn2cfeSASiTzAJT6vfjlrbcw5rFT3B3wJkZSD41KPULm+QccjF9DzXMvKcEc6k03UIJjUsgW004PniDLfmH23qcB3BPiaH2FdSkeSlq8fMzMQl9qeVZms0f03dFzr/tr0lne7vp44+HrFrz3+y9eDf9LLL3lCt6YAq2KJESeVgZEgtlpJxRbKSCQy6DCg8zTbf7otX287iYmrAdDh03+bBcTXUyTgJFDXeX3Wel7zOYpDXe5TJUOoCfY7LLeZ4GzUP1QuXKk8dcKzO3ls3ysVv7bcuMuKD+xrr3j3pJdf8oRBy8E3IPtZ0pwI5OQ8ZmzpsMNAuYPxlL5k87At76CSst7lyrbttTdkWyn2UThAjZgKvO24Sr/UMToJlTVne5/MV9iiaDvkpov7Wf1DpS81okurVQV18mQDfucEpT/ETHfUZI1/zfYgLSFiWiYTkUhkWNJOOe/GNUAJDHyoHHb4tNyVhzBm/KdtZS0p+xmJOykWkP3ySH7pDWV7jx+lKFjfp2zveRV9f5pJfFhexK+hstzkLyhRWX2z/l1u6z2OE54MlinZd9GrVF4+XKsrEol0z/lJRiKRARMkj0p6RNV1+wv6Wp1BrvzYVDCHHT5A6HFpHb9+FWc1rNT0Mgi24YBQCSeh0g/XvHXZZoLDmt+nbCf9s62DbGH9XY+EygomfwnXgT3l6+PBmMrwqFXwG/BBF8+uSCRSmP2jYgvk7J8HJN0jd8aP5RX8tTqDyvaeOGoqmTnZrw3rRhlx9PQ/3hcbl11Xxm6KBiTZTsTmh4q77WfMUwxqfp9KmXtUpSE/5eC8JDktdQiV9DW2PLCHfN1yy5jKkDCtZttd3uyY6OI57xcoYyl9y/b7b3huYLOQqGYx7nQmm5Dd0/+xOWOUbFpbOikaqENg8wNaKWt7n3JyL3Xynt5q7r+ESjT+JOSPchDQSEm5+5Q1GfAlRJzYHtRZcKkEn2/2otwmsNmM7YmqurE9ySorXTaV4w4qYBBjCVH7+9R2h8u12H7vRkWPorqESiqJtToJ4xxaNI5pPXOrZXGI8DRvoExwGHzNpvvrmAsVG0eVmnQm2yu7sZ/5eT7beA2PDQiVCDnTSmk7CZKjOooJora9sfi+rnWoZACyncjEKT9/fFJu+Li1xAhP+AiUQWQzpnZ0gevLpjdAh6l41fr6G5hnjFKOUAlLtFajlqFtqMoMkrJ8vdP7b+i1uLWhwuLFHM1Zml96x88fnwcIIVMoFMYjkUhSxdlNqwlQlJ33bC0UCslGBlo1pgUhF6ST6GA+gfP2P94XG09nsoOyW+Q7YVMZMp+zy4XKlm2Z6fTQ6aLMNwYPFlDL+1RV36PxvlgunckOWf7efgdhlFDp1obOvO9ytWofR9SByNv1nzTs1MoPBP2wbvfADWGDpAd9FCxTkUgk4eCGLzGz6WxjkhIeWDokxfIlDakwD5eoGNmEyq50Jts9e3xmBSHU6h453xileF9sNJ3J+jVQpCyOFxpnNID7RGttbQLimEvXdMqybrPe8v4LN0MlnGs+cJyDUIMvrUZXqCORiF9v+DsdvG+AyXmKa2g1uHUSjQ+V81aYTVizfVqeUAWtYGZttS0OrvWF2HzOLopI6LW7cY343HqKQdn71AbVuZVy1v03lc5kk7Iby5kUyyZZsZ39dcH1tgrLV3A054aKyQlPfq6z0dYw3vBRAYdLjIyFPEiNSdoqqZNASagsM/bHtnxUuhi37XaHysykaPUdwILhXCMB3CfroRzMhFyWbW+mucsdVct2W13c22obKhdsiShEL+JoztF25IA3Q+WaSwmVcPPGmwrhMRqTtE3SpkKh0FkoFFhGJbhsWiCGygTOnOwnxOqvoCLrZiulk+8AKl7hZnv+PT/2fp4JrMIarl3hsDeF2+td206YVsm9ElWEygW/aKaWESqdaD52uP6hsr3swzQqx0DRiAkKO1Rsjdws6SoTJPsZsxj4ilC3W9+RVVRSEmVaQGyf/g9VME7IttLfTWkJdViwXR7CL3WMEUKla2zve/Mtd+TGg4JBy7dtoQW6AaHyzIWXcDQdaDl+pP6hMrqi3IXHDJ4Ig6FCoRAp87OhUCh0FwqFXtMaOWjW9UQ4uDKecs79NSW7p+VRSb0LVOjbHYTKlBv7QWUaTh8o+Gjyk9FaH4uQPHhw0ptiwGFrsdvhVmISwsozjeXrS4aNqY2r1LLrIEfViOSPSCu997nOXnF1qX8e48wBgKOwVGlleUDSA5YVofnCYK8JnZWqaIxSvC82bDkDbCdFhVBZaRn00b4NS+ohVFbNSSjrNBPr1ELe8r7Zn85kaxVyQx0qR0uGlZWXSyJUTmvd8zNF3j1a8jVN+aN1/1xluiqPcuYAwFFYqrSXh22o7Ehnsr3xvtjgPGHThs3rR1T5eFJmvgyvXsvX53y0bznL6zS6wHUaWg57U0j2LZu1NN1bJMUZLc2q+2u5bpFnLr2cIzo7se86qMWDPyr507bzxbp+pjOrl5XrqpzjzAGAJLvp7/OVPsk2r7OdZbl/TmUtIbuxbHnZjScataw80gU2fIEhIbsWH8kHk/RU+Vl7KRnn3beiAdiPJKfS5VBpLDi73elV7+eIetyZa8rWQRhPCYAKs31Isr132lZSuuZ8Jtun/7bdt2z3h1AZPgkH7/FNK565XoZsQyUTu1RdRryoI53J8sCgBqFywS+aMxdeojOrl3FUvRwqr1hT7iU5jhIAqKah0qwTaVth7TeBt1v2XU5tp+cftXx9J0UmPEwZtF3IfqzM+qheZBuCo2Jil+kykpD9zMBexnmtQagsGTrOrL2Oo+phZVqTRxiIDAC1D5VG0vL1W8wSDrbv2+7g3m5b+e+myIRK0sF7Uj7cTyctq/20VjouI17WRTf/eofKK6/mqHrUmWsvZjwlANQmVNqGsOmlFWxnw0zKvoUo6fCz2eikyISDaYHqcvBW34VKhz0KQt9aabqKdgRw12itdDNUmqedCy4Ie/LKdTobbQ31QT0bbdXUxlVWP/U4Zqc3bKzqgQEAhIhVpbmKtfdsA5/trIg7quhyaBN4O2idCUVYaJd9V2pJGvJh19dqwvADpldBWAU1fG0J+XktqaWKC+zBhf5x6kPX1H1WU0+FyjWX6tjv/4nVey7854fUVOM1PievuqFcpYhpsAFQcbbv4jRSxa8bNJX0Ws2QOFDFe0dl19qwQTycDLpBh2V1wK87HO+LpcyaiR0OjlXouks6HG/rt8BMi+U8mhy+r+SXxtQNH+HIeszUxlXlur7u4CgBwEw4sg1fTius4zWscA9V0YJa9rveheMGf4WFlMOwMBaAh9ZOrtH16Ux2IIRFJRnw/UvQK8PFUGnWq1ywW8yplR/w/CywizqX68KbO0Jzok+v/VC5l9BKCQDOwlG1SzGlarQf1W7XdnKfTopOYANlv5wvSB+EVp2U7Mc/S9I9ZgxqWMpJp4LdSikVW+oT3BXO11LFewdUogvs6ds+qubHnvbkTi9bu1JXf/5zamu/WHveTOrU6LuBPslno606ee0tpV5iuyg2ABAqXQqV8b7YaDqT3V5FpX0+Y/G+WLWhkrUqMT0xz4MO3z4UhKE18b7YuOkC+4iT+nI6kx02DTJePs/t0/taxWaSTsqIR+75Nt26++XjLt1eDJWDpW4yJ6+7SW3RITXlT3tup6/+/Od0+W//kSTp2nvf0e57/jzQJ3ly001lzyVLiQDADNsn7W5UFgdcDpVJF7YxXOPjBu8HylSV5TIwY8/M2MqEg3IelZRLZ7LdXg2WJlDmzPnKOdxGp4OyMhTvi3V7YP+Tkh6weEtHOpNNuPDgLlCaqri4RlViHF6hbWklYaZuFnUu16o//qQ+/A9f1cTrr878/cTrr2rdti9r1R9/Uos6lwfyJJ9cX/b+x0UBAHI0SY/cmNXSVDbdemLvSu8T87Ax76BiCf9fB+3pTDZXZaDc6vXWuTqG5NnBFlsAAA/YSURBVJlg6cFz3a3iuPD1VW4qWaf31EKqjmWBUOnkJJxc3+WZ5UVOjb6rtfd/XZd8/C5d/YUvzfz91V/4ki7/7T/S2vu/HshusJObrlehbWmpl4xVOZEDAASJbShys+uWW92pBlzsfTJc4+MH74WMXhMyqml5Hon3xZJBOzYmJG+tIlju9NIYS9NCt1NVzj5tWjp7Ld/mmfqneTC43fJt6734kMC3odL0k19w4LKXWiujt64p+5pla1cG6uSejbbq5M23l3tZkssAAGbUe5Keir9TGxBOnewflSz/hsnOdCY7KOnxKkNG3kHA8FOwTKq6h0mPpDPZVCNnEDXnOie7Lp+l9DsoM16rf6Yc7jfcCJWVFAqvtFbmf7xPz/+XuzU5/vZ5/zY5/rZ2/VlCx/ccCtTJndx0U7llRPL0BweAqkLRqMu/v9qK1naXx8gzA2w4wmRK0n5JPS5ssteNLuEe1yvLruFzbJE03IiWLjOT77BcGgNtwrFtuBrzWv3TtJrarjncQ5d/F0OlKRSlWyvv8MbY/SPf/7l+8Rf/47y/H/nyn+itx58L1Ik9G22tZCwlM1cBwLka1lJpDFZZWU26/HlyNT5+aFyQTJjWqv1yb5Koz4ZhSI15cNNd5bXaoWJ32MF6BJN0JtudzmRHVZxkM+riphMOtpfy6Kl1Ui9OCpKqm/117gFdcJrlE+u61Tq8S80vvdPwHc7v+qX+7aYNet/vfkySdPiZ5wN5Yk/FN5cbS5knVALAuRVtB5UjV0OlWbpgQM66pe2oQQuR7fbWU5I8V643SGo3IajT/LcWC3V/Nky9n+J9sWHT6vdIlZvqUbHFa7uklNuh3IzhTKh2szPbtlJ6tv5pZvgdsPwe6E1nsu2souBSqDQnIVnqJnXirrt1wUsPNz5szZmMJ4iT80xtXKWTV64r+yCACwAItIFIJNKoazxVKBT8WLm0bWUbq9F9NOUwVLpeUTNraFqHmAbN+jmQzmQbVeb767jPiQq6TXbWKDgSKM+v/8qFYCkVW4u3pDPZMRV7LAw6DZimfPSan5qVAxNYbbc/4PH6p+1DvagJ1sl6fEeZ3gU1+d6u9hpucfHDJEtdVKdXrNbknR9W25M/reqXHP/l63p2y+9Yv2euwz9YuIXSdvsnDx4+589N+97Whf/8kNU2mva97cpJOBtt1cSn+iqpCNFKCQRbI1uMcj49ZrahcrRGFdVR02ph0yVxqIbdDodk18rRKfe7BXu9zNdz0pWOOgdGAmX9guX0+b1H0j1muyPmXjO8wP11uiW60/zUc8yZkyDl9bLipKdIvUJltIbnt+rvjxaXL6pEqZ09/rHNat43WlU32Kmjp5T/8b6qP++psYVbKKvdflP+tJp2HWzIlXCq51PlJueZLvwAgHN1eyg8pyxDZS0fFI5aVmQ2yIV1MuF5eRUn5cmF/UDUIFjOtt78TE+i9IAX9tm0hto+3Nju9UmczBAE24d60XQmmwj75JctLm+vX9KuUi84cdfdWnrom2rKn3b8SyY3Xa+m8WLL+VTHVZKk5jffUOTkyZk/S1LTqZNqe/KnOtl7W3Fnx/arZddBtaxYpPff+zm1RVdo/MXdmpqY0DvPPK+Vd2xS/qWXdGVvTJP5o8r/8hdqixYfPh5/7VUtu+JKSdL47j1qX7dWJ948pLcef04dX+zRiTcP6UhueGa7r/z9N3V8zyFdtvnmYth78221r1urQ0/t1Mo7Nmns4R266PYbtPp3epT/5c819vCOqg/+5KbrdfLaW8q9bMhMWw8AOD8M2YatWlVsculMttIWwrEa39dt97ObohR4IwrHLK+2wXLUPFCJhmCXk3V6TyMMyH7yqqS83wpbU00uX1DDkraVes3pFat1qudTVf2eqRs+oqmOqzTVcZVO3HinCstX6NRtt+vkJ39PheUrdOLGOzV53Y06/rHNmrzzwzpx452aev9anW0vBsTf+IM7dPmdcS1d3amWxUu09v6v69p7/6vWfO4+Xdkb08W3fkrtH9yo6LX/ofh3n/6Mlr//Gl1331d0yU23adH7LtV1931FH/zSX2rZ2pW67r6v6Iq7Yur8Qt/MdqddcVdMV9wVU/u6tcXX9fXouvu+okWdy3Xj33xHS1d36vLf+l21rFhU1TE5c+3FmvhE2WWh8ioO1gYAzOKFSXrmUWkFpdYVNdv97KREBdq2eF9sA4Fy3npwTsWHKiNB3k/TSmnbDXOHX8qMyTO2a5F2NGKJmMCGyllfbiUXbz557S2avPPDVf2Syetu1OR1N0qSWvf8TKdXrNbpFavVuudnxb87+GtFJid0dtFiSVLzWwdmWjclqa39Ur06mNarj+7Q5Pjbuvy3/0ivfecbM//+8t9t097kw3p3z7N69X89qneef0GSdPgnP1J+1y9ntnHF3Z8+77Pt+/bfL7jm5RV3f0GSdPHHi5/9hc//qZ6NfUFTR085PhZno606cdfd5WZ7lYqT8/AlAADns14Ko9YTs5Rbssuox3rDtt8bHRSnQBqTtCneF2MITfn7Qrek7QHeTSdlwG9zeTi5ryYJle5eTOOqoDXs+Mc26/RH17jyO1t2HVTzscNqPnZYLWYs48lrb1HL8SNqfaH4nX/mstUzLZWvP/aU3vjXb2vt/V/XRd0b9MaTaUnSa5kdeuXvv6l39zyrG//mO1q2duV5v+uSm25Ty4XvhbfL74xrcrw4yc7oNzJ6d8+z2vjVlC66/YZ5P2tb+6XF0LvsAkmqKkzOhPTP/EedXrG63MuGmJwHAFwLlfVqiRio8t/dqiRbCfsT+4DJS9oa74t1Mn6y8rpwvC+WkLRZ1a1l6TmmV0eP5duG/FZ2KnyoN1eXWcKHUOniichJ2lo2WN4e15lrL3b0O9r2Pq+2ve/N4Npy/Ihajh+Z+fOS55/UBX/1sJoPHJdUbKmcdlH3Bk28/mrxde9bqRMHi7PDnhp9V5fcdrOOHxgtbvPC81v+Dv/kRzN/f/iZJ9TWfqne+fH3Zrab37tHkrT4fe87/73PPKGJ/S8W//9Hz0mS1m37stZt+7Iuuv0GdXyxx/o4nPjM7+rUyg9U8oXQKwCAW6GyXrObpkpUSuu53ttIjY8nPBomJXXG+2JJDoej+vCgit3Bt/ng426r8L7mpCykfHoKnXzu0LbkN9XwQkqqTH/kQttSHYv/Z+tg2fTWQTXlj6opf1SLDr383t+9VWylXHToZTXlj868ftGhl1WIXqSzl62aCZJX3P0FHX7mCb3+2FM68eYhHX7mieK/rfoNXX5nXG/867eV//E+5ffu0Yk3D2nq2IQOP/PEzJjKw888odeeyOrwM0/oUO5p5ffuOWe7b/3LD4t35L17ZraR37tHr3zrIR1+5gkd33NIr3zjL3TxrcXxpRdceaValy+3DpQVTMwjSQnWpAQAV0PQaJ0qpeMlKjaDdby32+5vJ0XKt8Yk3TsdJqk/VH8Nmy7DV0na4cGPuF3SVfG+WH+5c21aKW0nsBnz8ayoTh7abTHHKXRaarz9XvNFFC0XLC9M/13FS40sfuzpmf9v2/nieX+39GuPnvP6uX8ee3jHObOtvvX4c3rr8WLL4d77H9Le+99bY/LXX/3WzP//9D/92TnvOee/em5m27PNfv/0aw5+699m/m32v9coUG5ltlcAKFlRapf9OMBcnSs298zz98k6foZh2XV5o6XSX0ZUnLV0sNZjhUMcLkcl9ZrAkXQQztw03cshZTnXhpNWuKSPz5mT5UWk4jDAZNjKeFOtT4aKg5VL9id32mIZVhaBcgddVgCgJgFotF4fzlT65k76Ue/13kbrcExRH0OmPG2VtEnSCjOba5JAWZ/r2Yy3XKFii3A9Z4rdIWlzvC/Wbs53xde1efiWsA2vAVi70UlrZb85XqHSUoeLZzidyfarzIKwTlosCZQljShYy4fkavz6WlXCtvrsM4fpc4/67BpIeehY+6VM2LApO2rATNopnfu0PNWAc7qVMl+T+0muDtfheEACo22Z8Ox93jS8DEgaMK2XvSo2xHTLvXUuR8zxyrnQa63dQcDyfZkzOeZes/+2x2u8gvK51SO7WvV9KFIoFOrySdOZbKJcsJSkyOSElv5gcKZbK4rORlt1Kr5ZJ69cV8nLxyRtYBwEAARHOpPNqbg23FC8L9bNEQECe613qjguuduEk9kt/11z6nujc4LBuKRhZupFvdUtVJqLJKUK+yUvef5JLR78EWdI0pnVy3Qi/seVLBsiFbsad9OFBQACV9FMqPhwdjNj5QEAoQ2VtsFy8au7tSj9uJryp0N7gk5/dI2O3x5XoW0pgRIACJY5WikBAKEPlbbBsvnYYS39XkYtuw6G6sScjbZq8o4unVhXcd2BQAkAAAAgHKHSBMuFpkif15LdObU9NRSKVsupjat08pO/V2l3VwIlAAAAgPCFShMsE6pg8p5pQW+1dNA6KRVn9upmUh4AAAAAoQuVJlj2qjhFdMXTJy869LIWPZ5V84HjgTkRk5uu18QneisdOzltSFIvgRIAAABAaEOlCZYbJA1K6rB53+KXnlXb/93p63A5uel6nbz5dp258BLbt26L98X6KcIAAAAAQh8qTbBsV7HFssf2vX4Ml1WEybykBNPJAwAAACBUzh8u+yUlZdEddtqiQy+r5ecvqG3ni5482GdWL9Ppj2zQyfVdtt1cp42o2N11lKILAAAAgFC5cLDcoGKr5XpHOzU5oUX7d6t1z88aPqnP2Wirpj50jaZu+IhOrfxANZvaGu+LJSmyAAAAAAiVlYdLx62WcwNm82v71Pyrsbp0kZ3auEpTHVdp6v1rbZYFWciQpH6WCwEAAABAqHQWLDtNsNzixvaajx1W69GDirz5ulrG9ivydr6qoDm1cZXOtrfr7GWrdPayVdW2Rs6WN2EyRTEFAAAAQKisPlx2m3DZVYvtNx87rJbjR2b+3PTaK+f8e2H5ChWiF8382cXwOF+YHJA0wFIhAAAAAAiVPguXDUSYBAAAAECorHO47JeDJUgIkwAAAAAQ8lA5K1x2mnCZUBUT+jTAkKQUYyYBAAAAECq9EzB7JU3/eDFgjqi4VMoga00CAAAAIFR6P2B2m5/1DfoYeUk580OQBAAAAECo9GnAbDfhcsOs/9aiJXNE0vD0T7wvlqOIAQAAACBUBjdobpDUaX5kAmclRs2PVGyFHI/3xYYpTgAAAAAIlQAAAAAAVKiJQwAAAAAAIFQCAAAAAAiVAAAAAABCJQAAAACAUAkAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAABCJQAAAACAUAkAAAAAIFQCAAAAAAiVAAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAIFQCAAAAAAiVAAAAAABCJQAAAAAg0P4//U9DqP/Pc9YAAAAASUVORK5CYII='
                                }, {
                                    margin: [70, -20, 0, 20],
                                    alignment: 'right',
                                    width: 400,
                                    table: /*function (subTbl) {*/
                                            /*return*/ {
                                                //headers are automatically repeated if the table spans over multiple pages
                                                //you can declare how many rows should be treated as headers
                                                headerRows: 1,
                                                alignment: 'right',
                                                widths: [70, 70, 70, 70, 70, 70],
                                                body: subTbl
                                            }
                                    /*}*/
                                });
                            } else {
                                 doc.content.splice(1, 0, {
                                    //margin: [0, -10, 0, 20],
                                    margin: [0, -10, 0, 60],
                                    alignment: 'left',
                                    width: 250,
                                    image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA5UAAACnCAYAAACB8njoAAAACXBIWXMAAC4jAAAuIwF4pT92AAAgAElEQVR42u3dfXBc5WHv8d/qzW/glXmLcQ0SJgQuwW9JeAkkkTxpCbSlkrNF3dtp481NMjfT2zuIy72dO8kMrP9oJ5O2gzxJKZ00ZU0y7bLpXuRLU0guvV6FJIVAYslOiCHBlsAYA8bW4mvZlmXv/WMfCVmWdvc5e3b3vHw/MxqwvXu055znnH1+53mLFAoFAQAAAADgRBOHAAAAAABAqAQAAAAAECoBAAAAAIRKAAAAAAChEgAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAAIRKAAAAAAChEgAAAABAqAQAAAAAECoBAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAABAqAQAAAAA1FFLWHc8ncl2S+o0P+2SNph/ape0foG35SUNz/pzbtZ/R+N9sVGKFAAAAIAwiRQKhTAEyA2Sus3PBkkdNfx1QyZ45iQNEzQBAAAAECr9FyI7TYDsNf+NNvDjjJiAORjvi+UocgAAAAAIld4Mku0mRPZr4e6rjZaXNCgpRcAEAAAAQKj0RpjslpSQtMVnH31M0oAJmOMURQAAAACEyvqGyYSkpGo7PrJetktKMv4SAAAAAKGSMEm4BAAAAECoDFOYbD52WC3Hj0iSml57peRrC8tXqBC9SJJ0auUHCJcAAAAACJUeD5MbVBx72OXG9hYdellNr72i5jffUNORvJpfeqeq7Z2Ntursmkt1duXlOnPp5Tq96v06c+ElbnzUvNnvAcZcAgAAACBU2ofJdhVbJu+pZjutRw+o5dd71DK2Xy27Dtbls59ZvUxnrunQmSvW6NRV61RoW1rN5sYkJZgtFgAAAAChsvJA2S0pJYddXVuPHlDbL55X8569aj5wvOH7c/qjazR1zfXVBsxtKnaJpdUSAAAAAKGyRKAckMPWycUvPau25/696i6ttXI22qqpD12jyQ9/QqdXrHayiTFJvfG+2DBFFwAAAACh8tww2SlpUNJ6qx2YnNDikSG17fyJmvKnfXPgpzau0uTHftPpZD/3xvtiAxRfAAAAAIRKzXR3HZQUDXqYdDFc7lBxrCXdYQEAAACEN1SapUIesXnPkt05tT015OswOV+4nPhUn+3MsSOSugmWAAAAAEIZKtOZbErSlkpfv+jQy1r0/e96dsykG0723qaT67tsJvTJm2DJOEsAAAAA4QmVNoEyMjmhpT/5ntqe/GkoTsqZ1ct0anPMpksswRIAAABAQzR5PVC2Hj2gC9N/F5pAKUnNB45r6dce1bIfPl7pW6KSculMdgNFGgAa8r3WHtD96uTsAgA8FyptAuWS3Tld8FcPB7q7ayltT/5Uyx99UM3HDhMsAcC7watbUiKggTLBGQbg8XtVO0chZKHSJlAue/oftfixp0N/gppfekcX/O3faNGhlwmWAOBNGyT1B3C/kmbfAMCrgbJXEsvshSlUVhooI5MTWv7og2rb+SJnZ/ok5U9r6dce1eKXniVYAoA3Q2WHmc08KBW1TvOdzXcJAC/rl7SF1sqQhErzRVs2UE6Pnwxrd9dyljz6L1ry/JOVBstBLjAAqFuolILVVXS65bWD0wvAi8zQg6459yw0SEsdTnivKliHsvXoAS39xjcDtfZkLSwe/JGa8kd1/Df/sNxLOyTlxFNmAKi19ea/XelMtjveF8v5vKLWPjsgB2GfAB9ehxskdZp6XHuF9blhSeOm/jca74uNBvwwJWf9f/+cP6POarqkiLkgciq2nBEoXTS56fpKgqUkbY/3xRK+L6iRSKe5uVZqtFAojDb4M1f6JVDxZ45EItNfLraGC4XCuEfOpaN9KBQKOZePty/KkQvnvhE8c9zqUPHrlrQzSPfcdCablPTArL/6bLwvlmpQpdovZX443hcbd7iftt9vVoL0QMBBmXB8Xhq0b72SuvVe61u1xkw9PCdp0C/HwuK62T/nrxtyr6piH2zrLeNeXj6wpcYHKuV2oIzeuka3bP+u1Wd5dsvvKP/jfXU7qFMbV+nY7/+J1Xsu/OeH1LLrYMWvL445/cdKguWWdCab89NFtoDEnEpOOVvV+CdWG+ZUNt34zJ2SHnfwWbxwPKaD0S4Hb91hvhTdPN6Vfubp/x3RrCfAknJ1Dk4DLlY0as0T5a2O1/nce27S5y0EiTL7SJk/36YK7lFufb/Z1sem/zevYkvWqPkZNqHLT2XVtkxUc17qFST7TZiM1uBXdKg4/GyLpEfSmewOEy79XifUAt8xSZM9/PT9YVNvGTIPHcIVKs2JXV+ysjY5oSVPfIcWSofadr6os5fldGJd2fI1kM5kh738dAOVKRQKg5FIZMhBReuBSCSS8kDrkdMZ2rwwVmKmi+OswDkmaVDSQFha5lA2VE6XV1+O7zFzIHR4JFTCXVFz/+qac84D25rl8essqfqPWe6R1JPOZAdM+Brw4wOwWROJnRei05lsb7wvNkgpq7+mGp3sXkn3lAuUTMpTvcWPPV3JrLBR+evJDWoTsBo65XYkEumVs1aHrR4ObB3mXrc/EomkTDdthMt85zzh44nSkhUGZwTHdGvWI5KOpjPZwSDNZOy1MJnOZEfNsW7kJFjR6e+udCabMiHN7/epautI8FqonNXttaSlPxgkULpkyaP/otajB8q9bL0ZJwOfKxQKw5K2O3hrTyQS6W7gR3cSavPyz/pTW0y4HDDjOxEOXQtU2HxXKTfjQ+er6EaZTTxUelTsKjmezmSTnHt3ri2PhMkFv7v8cq7NZ+wtdU9mWb2AhEpTASzZL3zJ7hzrULps6Te+qcjkRLmXPcCFFhj9JnDVI9hVLRKJJB1+kfZ7ZYIhC/dIypnxowh2RXFDmWvUb5Il/o3yHD5RFcd6jhIunQegdCY7qOK4Oa8vz/OAT67zfpUff0prpd9DpXnKWXI9ytajB7T4sac58m6fyPxpXfC/U54NFXCXCVpOzuX6SCSSqHOgbHd4gx8pFAopn56i9QTLUCh1fjvMUBC/VH67Vbp7ejenO/ThcthPZdoj19Soii2/cCmkV1if2OLDLr2EyjlKVgAjkxNakv4WR71GWnYd1JLduXIv62KsRGCCZVLF6cJt1bt7ZtneCwvw+5PGKMEy1KHSb2W43PcCFTR0SHrcjLmk1bJ0+Emq2DoZ5Wi4fp+q9JjSWunXUJnOZPtVpml/6U++p+YDxznqNbT4sacrGV85wBdCYDi5aUbrdbM1gWqLg7duL7cuJcESPgiVvhjbU2ImRUIl5tOjYpdY7mvnX0vt6Uw2pRouEUOdp/IASl3Xh6HSnLRkqdcsOvSy2p78KUe8DpY88R3PhArUVqFQGFRx3SJbD9RpplKnk/MkA3SaopIGmbwnkCqZzdgP99qkS/uK8IhK2kXPp/Pqwjk5e5CK8sc3Ibtxqb6cMC30oVJlBs1GJie06PEsR7tOml96p5JusP08wQkMTy4xUsUSIkFc87FDLOsTtApOpa00nh7bU8FMirNf28mZxxyPmDUPCZTFQLm+Rr9iSNIOSVsX+BmSNBLww5ysY/0IDrS4dCGVPGmLR4bo9lpnbU8N6eR1N6nQtnShl0y3ViY5Wv5WKBSGI5HINpVZG3YePZFIpLsW3UxNq5yTisaY6jeZVF7ScIl/d7tlpicSifSa1mX4n03Xv4SH77WVzKQ4e59HOfWY4550Jtse74slCJSu2WG2mYv3xYYtP0+3ihNr9dYw5Nb7GHfL2ey5HelMNhHvi6W4TH0QKst9ITUfO6y2nT/hSNdZU/60lv5gUMd/8w9Lnrt0JjsQ74uNc8R8Lym7AezTBlSbKcTLjrFeaD/quITIcKFQ6K4gIHebY9St6mfxG5BUr1C5KSDjUoMQKj15r7WYSXH2Pnv5ocimeF8sDGV+a7wvlqzg/HaqOBa23Zy76Z9aLG2xJZ3JKqTB0q1AOf1QNVXNvcJcAzlJSVMG+h3WD7xWx3EqIXoK+SZUlryBLH7u+2rKn+ZIN0Dbzhd18ubDOnPhJQu9ZLq/OcuM+FyhUBg3a0E+aPnW9ZFIJOHm0h1mrKaTLidDXlxCxASznN6bNbfXfME5emrq9vGGL0Jl1JQbr533XsuKJhOz+Ei8Lzaq91qWB+c8TOjVe61ZboWNLelMdjzeFwtNl0MzKU+1gXJMUrIWrWmmDPSb2Wir+e5q5DHuVnU9h7rSmWx3SB44NVRTlSc6UapwFlspX+QoN9Di575f7iX0Nw9OsByQN5YYSSqgS4gUCoXxQqGQKhQKnSqOY3F6fOB/XQE477afqZPTHoiwOR7vi6XifbFEvC/WLumzcjbh23zuCcvkPWbVg2on5dka74t11rp75vQ5V/HB0FYVh3/4Rb9HtoFahkpV0EqJxmrb+aKajx0u9ZIOFjMOFCdf5q7NBmy6ijpdQmTYTwfarBO6ycGXcwdLjPi+MukkXHnqXutgJkUpIOOzcF7gSMX7Yt3mfuZGuHwk6MuNmNazB6vYxIikjZV0Y65BuEyacLnDJ/faHhc21cNEYx4OlebkLPikNjI54YtWyuita9TxxR6t2/ZlffgfvnreT8cXe3TR7Tf4+iRXEO4TXArBYLpqNnKJESdfkHn59CmiOd5Orh+uOX9zWmH2Ujl3VAZNZRrBDJe5WeFyrMrNDQZ1hnmzX6kqNrFdUrftBDwun+vReF+sV9JmebvVMunRbWEe1YypLDvjq2d3esUi/cYf3KErP/0ZLb3q+pKvveTjd0mSJsff1r5v/rVef+wpTR095a+T/LNfKfKJiVIzwfaYmduYsCcYEpL2O3jfgCpcWmA+kUgkIedLiPi27BUKhcFIJLJddi20VMzDGSq70plspxnn1MhKcbecj1Hq5PQHP1yalsYBOe/e2WEq8UHsdpiS83GJ2700mVG8LzZozvWgPNYTwTReubnmZy913dqqpvtrycpn6wve7MkWvXWNPvrYP+q6+75SNlDO1tZ+qa677yv6+Pf/j6K3rvHXSc6f1qL9u6s6n/BVyBmVtM3BW3tM91UngbJdzp4CjplupH5nuw90I/S37jqWlVqopqJPqAxHsBw34efeKjZzT9Bats3+OO2OudWLs+OaVssNKrageonbDySiYmyl90Kleaqx4FOaxa/u9uS6lJdtvlm3bP+uVZicL1zesv27umzzzb460W3P/TuhMlySctalxelMwE6XEAnEDd4EeavxKU4DPDyhmvFiWxrZLdCFMUqU23CFywEVJ/JxKmizy6ccvm97vcdPOjjXCa/MkGrukbUI4IRKr4XKcie6+eWfe25HL7r9Bm38asq17X3wS3+pZWtX+uZEN7/0jlqPHij1kp6gjn8II9Od1MkX2HrTjdUmHHXK+RIigwE67Lb7wvXmQyaUVbsEQyMrNtVWbJlkKnzBMlVFsFwflNlgzbIcTh6ebg/p+p3V3iNrsa5mNCyzE/spVHaX+seWn/3KW4HqgqVa/+cPubrNtvZLdd29/81XJ7vtF89XdV7hu2BZryVGkg5v/kG7sY9SOQ8FN85bQ0KlS2OUojyADG2wDO0ySqbMO7luR0TrWL2OdWjKY2BCpTnZC44HWvzqbjXlT3tqJz/437eqrf1S17d7ycfv8tX4yuY9e8u9hC6wweMkuFU87qCKJUS2mS6jQTJMcSNUWgSzhE/uB7U6BvBfsEzK2TIUHQFoHXLScpaX1MvEMNZ6LY913kF57OYweyBUlgseXuz6Ws0YynIuv+MO/4TKA8fLrVnJRRYwZskLJ5WASpcYSTrYdl4BfFLo5xlsYcWt+2RdrwGXn/4TKsMrIWfj9YMQKq2v8UbP9OxTtvfGAdlPMpTkMHsjVJb8Mmn+1VioDuClt3zSV5+3bX/J0N/B4rCB5LQiWXKChSqWEEkSwOBjbgWqej8tT8i9MUqEypAyrW5OAmKXX+sXppXV9toZMpMcwf5Y245bHZD9hFBd1He9ESoX/BJsPnbYk7O+1tLSq67Xos7l5x7UfW979vM2v7qPykLImG6mTsbCLLjESJVLiATyi5bZXENR4emUu5NH1HOslZu/i8pYuIPloKQhj5f3Rl87SUpKXY71drP8zbCDMsk58kCoXHA8ZevBX/tip9/4129r71//z5mfsX/6mibHnQfBxasuOfegemxM6Tmh8sXXCJXhNCB3lxhxuoRIIsDH2HbyElpr/cftMNVTj6flDp/+l9JFUQg9JxVy383bYK5P23WFh7yyNIfPjnW3g2M9uxymLN+7hdbKBoZKsz7lwoHltX2e3tn87h/qh5/epN33/LnGHt4x87P3/of0zO2/pfzuHzra7rKrr6r6s7XsOlifE54/zbjKEDLdTZ08bT1viREz1vIBB9vaYcZ4BpXttcPEPsE/x7WqnNtyvYWIyli4mdBk2zLkxyE2vR69pnlQUWylHJ1VJlOyn/E+wWFvUKhUmVaspnHvPnif2P+iXvj8n+r4nkPz/vvU0VN64fN/6qjFsnXZBb466a1HSwZYKgrBDZYpFac3tzV3iRGn3VeDPq26beVjlFLpO7XoydFbyyU6HD79b9SxgL+k6hTSGsk2dIzQSun4PtXlQvmzrZ/0s0RS40JlycBRr9Y2J176269q6uipkq+ZOnpKr33nG678vkWHXvbssYi8+Xqpf+7gsgg0J8FuZokRM26wx8E2tgZwCZH3rqlia67NtZMP8vEgVDq7vmok6aNjAR8xLUO2wyq6fRR0Si6h52LQhn14X6iLsW2ZjIrl9FzT4taXSJkulQ01sf9FvfX4cxW99sjwLl1d7wrp5ERdf1/zm2+Uu5FuMIOeETCFQiEXiUR2OAiGD0QikZSctVLm5bx10w+B0smkRbk6fLSBSCTSiO4jKdMqHiimglmrh26JWoQ/M2SlVuMfvRgqB9KZbEPKvAlYYTQou7WK/fQwotvh8YDdfapT9utdz1uniPfFxtOZrG2ZTPIwoDGhcsEm4pbjRzy7k28/+28Vv/bM/5uo++drO3KgvpXgkycdn2cfeSASiTzAJT6vfjlrbcw5rFT3B3wJkZSD41KPULm+QccjF9DzXMvKcEc6k03UIJjUsgW004PniDLfmH23qcB3BPiaH2FdSkeSlq8fMzMQl9qeVZms0f03dFzr/tr0lne7vp44+HrFrz3+y9eDf9LLL3lCt6YAq2KJESeVgZEgtlpJxRbKSCQy6DCg8zTbf7otX287iYmrAdDh03+bBcTXUyTgJFDXeX3Wel7zOYpDXe5TJUOoCfY7LLeZ4GzUP1QuXKk8dcKzO3ls3ysVv7bcuMuKD+xrr3j3pJdf8oRBy8E3IPtZ0pwI5OQ8ZmzpsMNAuYPxlL5k87At76CSst7lyrbttTdkWyn2UThAjZgKvO24Sr/UMToJlTVne5/MV9iiaDvkpov7Wf1DpS81okurVQV18mQDfucEpT/ETHfUZI1/zfYgLSFiWiYTkUhkWNJOOe/GNUAJDHyoHHb4tNyVhzBm/KdtZS0p+xmJOykWkP3ySH7pDWV7jx+lKFjfp2zveRV9f5pJfFhexK+hstzkLyhRWX2z/l1u6z2OE54MlinZd9GrVF4+XKsrEol0z/lJRiKRARMkj0p6RNV1+wv6Wp1BrvzYVDCHHT5A6HFpHb9+FWc1rNT0Mgi24YBQCSeh0g/XvHXZZoLDmt+nbCf9s62DbGH9XY+EygomfwnXgT3l6+PBmMrwqFXwG/BBF8+uSCRSmP2jYgvk7J8HJN0jd8aP5RX8tTqDyvaeOGoqmTnZrw3rRhlx9PQ/3hcbl11Xxm6KBiTZTsTmh4q77WfMUwxqfp9KmXtUpSE/5eC8JDktdQiV9DW2PLCHfN1yy5jKkDCtZttd3uyY6OI57xcoYyl9y/b7b3huYLOQqGYx7nQmm5Dd0/+xOWOUbFpbOikaqENg8wNaKWt7n3JyL3Xynt5q7r+ESjT+JOSPchDQSEm5+5Q1GfAlRJzYHtRZcKkEn2/2otwmsNmM7YmqurE9ySorXTaV4w4qYBBjCVH7+9R2h8u12H7vRkWPorqESiqJtToJ4xxaNI5pPXOrZXGI8DRvoExwGHzNpvvrmAsVG0eVmnQm2yu7sZ/5eT7beA2PDQiVCDnTSmk7CZKjOooJora9sfi+rnWoZACyncjEKT9/fFJu+Li1xAhP+AiUQWQzpnZ0gevLpjdAh6l41fr6G5hnjFKOUAlLtFajlqFtqMoMkrJ8vdP7b+i1uLWhwuLFHM1Zml96x88fnwcIIVMoFMYjkUhSxdlNqwlQlJ33bC0UCslGBlo1pgUhF6ST6GA+gfP2P94XG09nsoOyW+Q7YVMZMp+zy4XKlm2Z6fTQ6aLMNwYPFlDL+1RV36PxvlgunckOWf7efgdhlFDp1obOvO9ytWofR9SByNv1nzTs1MoPBP2wbvfADWGDpAd9FCxTkUgk4eCGLzGz6WxjkhIeWDokxfIlDakwD5eoGNmEyq50Jts9e3xmBSHU6h453xileF9sNJ3J+jVQpCyOFxpnNID7RGttbQLimEvXdMqybrPe8v4LN0MlnGs+cJyDUIMvrUZXqCORiF9v+DsdvG+AyXmKa2g1uHUSjQ+V81aYTVizfVqeUAWtYGZttS0OrvWF2HzOLopI6LW7cY343HqKQdn71AbVuZVy1v03lc5kk7Iby5kUyyZZsZ39dcH1tgrLV3A054aKyQlPfq6z0dYw3vBRAYdLjIyFPEiNSdoqqZNASagsM/bHtnxUuhi37XaHysykaPUdwILhXCMB3CfroRzMhFyWbW+mucsdVct2W13c22obKhdsiShEL+JoztF25IA3Q+WaSwmVcPPGmwrhMRqTtE3SpkKh0FkoFFhGJbhsWiCGygTOnOwnxOqvoCLrZiulk+8AKl7hZnv+PT/2fp4JrMIarl3hsDeF2+td206YVsm9ElWEygW/aKaWESqdaD52uP6hsr3swzQqx0DRiAkKO1Rsjdws6SoTJPsZsxj4ilC3W9+RVVRSEmVaQGyf/g9VME7IttLfTWkJdViwXR7CL3WMEUKla2zve/Mtd+TGg4JBy7dtoQW6AaHyzIWXcDQdaDl+pP6hMrqi3IXHDJ4Ig6FCoRAp87OhUCh0FwqFXtMaOWjW9UQ4uDKecs79NSW7p+VRSb0LVOjbHYTKlBv7QWUaTh8o+Gjyk9FaH4uQPHhw0ptiwGFrsdvhVmISwsozjeXrS4aNqY2r1LLrIEfViOSPSCu997nOXnF1qX8e48wBgKOwVGlleUDSA5YVofnCYK8JnZWqaIxSvC82bDkDbCdFhVBZaRn00b4NS+ohVFbNSSjrNBPr1ELe8r7Zn85kaxVyQx0qR0uGlZWXSyJUTmvd8zNF3j1a8jVN+aN1/1xluiqPcuYAwFFYqrSXh22o7Ehnsr3xvtjgPGHThs3rR1T5eFJmvgyvXsvX53y0bznL6zS6wHUaWg57U0j2LZu1NN1bJMUZLc2q+2u5bpFnLr2cIzo7se86qMWDPyr507bzxbp+pjOrl5XrqpzjzAGAJLvp7/OVPsk2r7OdZbl/TmUtIbuxbHnZjScataw80gU2fIEhIbsWH8kHk/RU+Vl7KRnn3beiAdiPJKfS5VBpLDi73elV7+eIetyZa8rWQRhPCYAKs31Isr132lZSuuZ8Jtun/7bdt2z3h1AZPgkH7/FNK565XoZsQyUTu1RdRryoI53J8sCgBqFywS+aMxdeojOrl3FUvRwqr1hT7iU5jhIAqKah0qwTaVth7TeBt1v2XU5tp+cftXx9J0UmPEwZtF3IfqzM+qheZBuCo2Jil+kykpD9zMBexnmtQagsGTrOrL2Oo+phZVqTRxiIDAC1D5VG0vL1W8wSDrbv2+7g3m5b+e+myIRK0sF7Uj7cTyctq/20VjouI17WRTf/eofKK6/mqHrUmWsvZjwlANQmVNqGsOmlFWxnw0zKvoUo6fCz2eikyISDaYHqcvBW34VKhz0KQt9aabqKdgRw12itdDNUmqedCy4Ie/LKdTobbQ31QT0bbdXUxlVWP/U4Zqc3bKzqgQEAhIhVpbmKtfdsA5/trIg7quhyaBN4O2idCUVYaJd9V2pJGvJh19dqwvADpldBWAU1fG0J+XktqaWKC+zBhf5x6kPX1H1WU0+FyjWX6tjv/4nVey7854fUVOM1PievuqFcpYhpsAFQcbbv4jRSxa8bNJX0Ws2QOFDFe0dl19qwQTycDLpBh2V1wK87HO+LpcyaiR0OjlXouks6HG/rt8BMi+U8mhy+r+SXxtQNH+HIeszUxlXlur7u4CgBwEw4sg1fTius4zWscA9V0YJa9rveheMGf4WFlMOwMBaAh9ZOrtH16Ux2IIRFJRnw/UvQK8PFUGnWq1ywW8yplR/w/CywizqX68KbO0Jzok+v/VC5l9BKCQDOwlG1SzGlarQf1W7XdnKfTopOYANlv5wvSB+EVp2U7Mc/S9I9ZgxqWMpJp4LdSikVW+oT3BXO11LFewdUogvs6ds+qubHnvbkTi9bu1JXf/5zamu/WHveTOrU6LuBPslno606ee0tpV5iuyg2ABAqXQqV8b7YaDqT3V5FpX0+Y/G+WLWhkrUqMT0xz4MO3z4UhKE18b7YuOkC+4iT+nI6kx02DTJePs/t0/taxWaSTsqIR+75Nt26++XjLt1eDJWDpW4yJ6+7SW3RITXlT3tup6/+/Od0+W//kSTp2nvf0e57/jzQJ3ly001lzyVLiQDADNsn7W5UFgdcDpVJF7YxXOPjBu8HylSV5TIwY8/M2MqEg3IelZRLZ7LdXg2WJlDmzPnKOdxGp4OyMhTvi3V7YP+Tkh6weEtHOpNNuPDgLlCaqri4RlViHF6hbWklYaZuFnUu16o//qQ+/A9f1cTrr878/cTrr2rdti9r1R9/Uos6lwfyJJ9cX/b+x0UBAHI0SY/cmNXSVDbdemLvSu8T87Ax76BiCf9fB+3pTDZXZaDc6vXWuTqG5NnBFlsAAA/YSURBVJlg6cFz3a3iuPD1VW4qWaf31EKqjmWBUOnkJJxc3+WZ5UVOjb6rtfd/XZd8/C5d/YUvzfz91V/4ki7/7T/S2vu/HshusJObrlehbWmpl4xVOZEDAASJbShys+uWW92pBlzsfTJc4+MH74WMXhMyqml5Hon3xZJBOzYmJG+tIlju9NIYS9NCt1NVzj5tWjp7Ld/mmfqneTC43fJt6734kMC3odL0k19w4LKXWiujt64p+5pla1cG6uSejbbq5M23l3tZkssAAGbUe5Keir9TGxBOnewflSz/hsnOdCY7KOnxKkNG3kHA8FOwTKq6h0mPpDPZVCNnEDXnOie7Lp+l9DsoM16rf6Yc7jfcCJWVFAqvtFbmf7xPz/+XuzU5/vZ5/zY5/rZ2/VlCx/ccCtTJndx0U7llRPL0BweAqkLRqMu/v9qK1naXx8gzA2w4wmRK0n5JPS5ssteNLuEe1yvLruFzbJE03IiWLjOT77BcGgNtwrFtuBrzWv3TtJrarjncQ5d/F0OlKRSlWyvv8MbY/SPf/7l+8Rf/47y/H/nyn+itx58L1Ik9G22tZCwlM1cBwLka1lJpDFZZWU26/HlyNT5+aFyQTJjWqv1yb5Koz4ZhSI15cNNd5bXaoWJ32MF6BJN0JtudzmRHVZxkM+riphMOtpfy6Kl1Ui9OCpKqm/117gFdcJrlE+u61Tq8S80vvdPwHc7v+qX+7aYNet/vfkySdPiZ5wN5Yk/FN5cbS5knVALAuRVtB5UjV0OlWbpgQM66pe2oQQuR7fbWU5I8V643SGo3IajT/LcWC3V/Nky9n+J9sWHT6vdIlZvqUbHFa7uklNuh3IzhTKh2szPbtlJ6tv5pZvgdsPwe6E1nsu2souBSqDQnIVnqJnXirrt1wUsPNz5szZmMJ4iT80xtXKWTV64r+yCACwAItIFIJNKoazxVKBT8WLm0bWUbq9F9NOUwVLpeUTNraFqHmAbN+jmQzmQbVeb767jPiQq6TXbWKDgSKM+v/8qFYCkVW4u3pDPZMRV7LAw6DZimfPSan5qVAxNYbbc/4PH6p+1DvagJ1sl6fEeZ3gU1+d6u9hpucfHDJEtdVKdXrNbknR9W25M/reqXHP/l63p2y+9Yv2euwz9YuIXSdvsnDx4+589N+97Whf/8kNU2mva97cpJOBtt1cSn+iqpCNFKCQRbI1uMcj49ZrahcrRGFdVR02ph0yVxqIbdDodk18rRKfe7BXu9zNdz0pWOOgdGAmX9guX0+b1H0j1muyPmXjO8wP11uiW60/zUc8yZkyDl9bLipKdIvUJltIbnt+rvjxaXL6pEqZ09/rHNat43WlU32Kmjp5T/8b6qP++psYVbKKvdflP+tJp2HWzIlXCq51PlJueZLvwAgHN1eyg8pyxDZS0fFI5aVmQ2yIV1MuF5eRUn5cmF/UDUIFjOtt78TE+i9IAX9tm0hto+3Nju9UmczBAE24d60XQmmwj75JctLm+vX9KuUi84cdfdWnrom2rKn3b8SyY3Xa+m8WLL+VTHVZKk5jffUOTkyZk/S1LTqZNqe/KnOtl7W3Fnx/arZddBtaxYpPff+zm1RVdo/MXdmpqY0DvPPK+Vd2xS/qWXdGVvTJP5o8r/8hdqixYfPh5/7VUtu+JKSdL47j1qX7dWJ948pLcef04dX+zRiTcP6UhueGa7r/z9N3V8zyFdtvnmYth78221r1urQ0/t1Mo7Nmns4R266PYbtPp3epT/5c819vCOqg/+5KbrdfLaW8q9bMhMWw8AOD8M2YatWlVsculMttIWwrEa39dt97ObohR4IwrHLK+2wXLUPFCJhmCXk3V6TyMMyH7yqqS83wpbU00uX1DDkraVes3pFat1qudTVf2eqRs+oqmOqzTVcZVO3HinCstX6NRtt+vkJ39PheUrdOLGOzV53Y06/rHNmrzzwzpx452aev9anW0vBsTf+IM7dPmdcS1d3amWxUu09v6v69p7/6vWfO4+Xdkb08W3fkrtH9yo6LX/ofh3n/6Mlr//Gl1331d0yU23adH7LtV1931FH/zSX2rZ2pW67r6v6Iq7Yur8Qt/MdqddcVdMV9wVU/u6tcXX9fXouvu+okWdy3Xj33xHS1d36vLf+l21rFhU1TE5c+3FmvhE2WWh8ioO1gYAzOKFSXrmUWkFpdYVNdv97KREBdq2eF9sA4Fy3npwTsWHKiNB3k/TSmnbDXOHX8qMyTO2a5F2NGKJmMCGyllfbiUXbz557S2avPPDVf2Syetu1OR1N0qSWvf8TKdXrNbpFavVuudnxb87+GtFJid0dtFiSVLzWwdmWjclqa39Ur06mNarj+7Q5Pjbuvy3/0ivfecbM//+8t9t097kw3p3z7N69X89qneef0GSdPgnP1J+1y9ntnHF3Z8+77Pt+/bfL7jm5RV3f0GSdPHHi5/9hc//qZ6NfUFTR085PhZno606cdfd5WZ7lYqT8/AlAADns14Ko9YTs5Rbssuox3rDtt8bHRSnQBqTtCneF2MITfn7Qrek7QHeTSdlwG9zeTi5ryYJle5eTOOqoDXs+Mc26/RH17jyO1t2HVTzscNqPnZYLWYs48lrb1HL8SNqfaH4nX/mstUzLZWvP/aU3vjXb2vt/V/XRd0b9MaTaUnSa5kdeuXvv6l39zyrG//mO1q2duV5v+uSm25Ty4XvhbfL74xrcrw4yc7oNzJ6d8+z2vjVlC66/YZ5P2tb+6XF0LvsAkmqKkzOhPTP/EedXrG63MuGmJwHAFwLlfVqiRio8t/dqiRbCfsT+4DJS9oa74t1Mn6y8rpwvC+WkLRZ1a1l6TmmV0eP5duG/FZ2KnyoN1eXWcKHUOniichJ2lo2WN4e15lrL3b0O9r2Pq+2ve/N4Npy/Ihajh+Z+fOS55/UBX/1sJoPHJdUbKmcdlH3Bk28/mrxde9bqRMHi7PDnhp9V5fcdrOOHxgtbvPC81v+Dv/kRzN/f/iZJ9TWfqne+fH3Zrab37tHkrT4fe87/73PPKGJ/S8W//9Hz0mS1m37stZt+7Iuuv0GdXyxx/o4nPjM7+rUyg9U8oXQKwCAW6GyXrObpkpUSuu53ttIjY8nPBomJXXG+2JJDoej+vCgit3Bt/ng426r8L7mpCykfHoKnXzu0LbkN9XwQkqqTH/kQttSHYv/Z+tg2fTWQTXlj6opf1SLDr383t+9VWylXHToZTXlj868ftGhl1WIXqSzl62aCZJX3P0FHX7mCb3+2FM68eYhHX7mieK/rfoNXX5nXG/867eV//E+5ffu0Yk3D2nq2IQOP/PEzJjKw888odeeyOrwM0/oUO5p5ffuOWe7b/3LD4t35L17ZraR37tHr3zrIR1+5gkd33NIr3zjL3TxrcXxpRdceaValy+3DpQVTMwjSQnWpAQAV0PQaJ0qpeMlKjaDdby32+5vJ0XKt8Yk3TsdJqk/VH8Nmy7DV0na4cGPuF3SVfG+WH+5c21aKW0nsBnz8ayoTh7abTHHKXRaarz9XvNFFC0XLC9M/13FS40sfuzpmf9v2/nieX+39GuPnvP6uX8ee3jHObOtvvX4c3rr8WLL4d77H9Le+99bY/LXX/3WzP//9D/92TnvOee/em5m27PNfv/0aw5+699m/m32v9coUG5ltlcAKFlRapf9OMBcnSs298zz98k6foZh2XV5o6XSX0ZUnLV0sNZjhUMcLkcl9ZrAkXQQztw03cshZTnXhpNWuKSPz5mT5UWk4jDAZNjKeFOtT4aKg5VL9id32mIZVhaBcgddVgCgJgFotF4fzlT65k76Ue/13kbrcExRH0OmPG2VtEnSCjOba5JAWZ/r2Yy3XKFii3A9Z4rdIWlzvC/Wbs53xde1efiWsA2vAVi70UlrZb85XqHSUoeLZzidyfarzIKwTlosCZQljShYy4fkavz6WlXCtvrsM4fpc4/67BpIeehY+6VM2LApO2rATNopnfu0PNWAc7qVMl+T+0muDtfheEACo22Z8Ox93jS8DEgaMK2XvSo2xHTLvXUuR8zxyrnQa63dQcDyfZkzOeZes/+2x2u8gvK51SO7WvV9KFIoFOrySdOZbKJcsJSkyOSElv5gcKZbK4rORlt1Kr5ZJ69cV8nLxyRtYBwEAARHOpPNqbg23FC8L9bNEQECe613qjguuduEk9kt/11z6nujc4LBuKRhZupFvdUtVJqLJKUK+yUvef5JLR78EWdI0pnVy3Qi/seVLBsiFbsad9OFBQACV9FMqPhwdjNj5QEAoQ2VtsFy8au7tSj9uJryp0N7gk5/dI2O3x5XoW0pgRIACJY5WikBAKEPlbbBsvnYYS39XkYtuw6G6sScjbZq8o4unVhXcd2BQAkAAAAgHKHSBMuFpkif15LdObU9NRSKVsupjat08pO/V2l3VwIlAAAAgPCFShMsE6pg8p5pQW+1dNA6KRVn9upmUh4AAAAAoQuVJlj2qjhFdMXTJy869LIWPZ5V84HjgTkRk5uu18QneisdOzltSFIvgRIAAABAaEOlCZYbJA1K6rB53+KXnlXb/93p63A5uel6nbz5dp258BLbt26L98X6KcIAAAAAQh8qTbBsV7HFssf2vX4Ml1WEybykBNPJAwAAACBUzh8u+yUlZdEddtqiQy+r5ecvqG3ni5482GdWL9Ppj2zQyfVdtt1cp42o2N11lKILAAAAgFC5cLDcoGKr5XpHOzU5oUX7d6t1z88aPqnP2Wirpj50jaZu+IhOrfxANZvaGu+LJSmyAAAAAAiVlYdLx62WcwNm82v71Pyrsbp0kZ3auEpTHVdp6v1rbZYFWciQpH6WCwEAAABAqHQWLDtNsNzixvaajx1W69GDirz5ulrG9ivydr6qoDm1cZXOtrfr7GWrdPayVdW2Rs6WN2EyRTEFAAAAQKisPlx2m3DZVYvtNx87rJbjR2b+3PTaK+f8e2H5ChWiF8382cXwOF+YHJA0wFIhAAAAAAiVPguXDUSYBAAAAECorHO47JeDJUgIkwAAAAAQ8lA5K1x2mnCZUBUT+jTAkKQUYyYBAAAAECq9EzB7JU3/eDFgjqi4VMoga00CAAAAIFR6P2B2m5/1DfoYeUk580OQBAAAAECo9GnAbDfhcsOs/9aiJXNE0vD0T7wvlqOIAQAAACBUBjdobpDUaX5kAmclRs2PVGyFHI/3xYYpTgAAAAAIlQAAAAAAVKiJQwAAAAAAIFQCAAAAAAiVAAAAAABCJQAAAACAUAkAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAABCJQAAAACAUAkAAAAAIFQCAAAAAAiVAAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAIFQCAAAAAAiVAAAAAABCJQAAAAAg0P4//U9DqP/Pc9YAAAAASUVORK5CYII='
                                }, {
                                    margin: [425, -100, 0, 20],
                                    alignment: 'right',
                                    width: 500,
                                    table: /*function (subTbl) {*/
                                            /*return*/ {
                                                // headers are automatically repeated if the table spans over multiple pages
                                                // you can declare how many rows should be treated as headers
                                                headerRows: 1,
                                                alignment: 'right',
                                                widths: [70, 70, 70, 70, 70, 70],
                                                body: subTbl
                                            }
                                    /*}*/
                                });
                            }


                            // doc.content.splice(1, 0, {
                            //     margin: [0, -10, 0, 20],
                            //     alignment: 'left',
                            //     width: 250,
                            //     image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA5UAAACnCAYAAACB8njoAAAACXBIWXMAAC4jAAAuIwF4pT92AAAgAElEQVR42u3dfXBc5WHv8d/qzW/glXmLcQ0SJgQuwW9JeAkkkTxpCbSlkrNF3dtp481NMjfT2zuIy72dO8kMrP9oJ5O2gzxJKZ00ZU0y7bLpXuRLU0guvV6FJIVAYslOiCHBlsAYA8bW4mvZlmXv/WMfCVmWdvc5e3b3vHw/MxqwvXu055znnH1+53mLFAoFAQAAAADgRBOHAAAAAABAqAQAAAAAECoBAAAAAIRKAAAAAAChEgAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAAIRKAAAAAAChEgAAAABAqAQAAAAAECoBAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAABAqAQAAAAA1FFLWHc8ncl2S+o0P+2SNph/ape0foG35SUNz/pzbtZ/R+N9sVGKFAAAAIAwiRQKhTAEyA2Sus3PBkkdNfx1QyZ45iQNEzQBAAAAECr9FyI7TYDsNf+NNvDjjJiAORjvi+UocgAAAAAIld4Mku0mRPZr4e6rjZaXNCgpRcAEAAAAQKj0RpjslpSQtMVnH31M0oAJmOMURQAAAACEyvqGyYSkpGo7PrJetktKMv4SAAAAAKGSMEm4BAAAAECoDFOYbD52WC3Hj0iSml57peRrC8tXqBC9SJJ0auUHCJcAAAAACJUeD5MbVBx72OXG9hYdellNr72i5jffUNORvJpfeqeq7Z2Ntursmkt1duXlOnPp5Tq96v06c+ElbnzUvNnvAcZcAgAAACBU2ofJdhVbJu+pZjutRw+o5dd71DK2Xy27Dtbls59ZvUxnrunQmSvW6NRV61RoW1rN5sYkJZgtFgAAAAChsvJA2S0pJYddXVuPHlDbL55X8569aj5wvOH7c/qjazR1zfXVBsxtKnaJpdUSAAAAAKGyRKAckMPWycUvPau25/696i6ttXI22qqpD12jyQ9/QqdXrHayiTFJvfG+2DBFFwAAAACh8tww2SlpUNJ6qx2YnNDikSG17fyJmvKnfXPgpzau0uTHftPpZD/3xvtiAxRfAAAAAIRKzXR3HZQUDXqYdDFc7lBxrCXdYQEAAACEN1SapUIesXnPkt05tT015OswOV+4nPhUn+3MsSOSugmWAAAAAEIZKtOZbErSlkpfv+jQy1r0/e96dsykG0723qaT67tsJvTJm2DJOEsAAAAA4QmVNoEyMjmhpT/5ntqe/GkoTsqZ1ct0anPMpksswRIAAABAQzR5PVC2Hj2gC9N/F5pAKUnNB45r6dce1bIfPl7pW6KSculMdgNFGgAa8r3WHtD96uTsAgA8FyptAuWS3Tld8FcPB7q7ayltT/5Uyx99UM3HDhMsAcC7watbUiKggTLBGQbg8XtVO0chZKHSJlAue/oftfixp0N/gppfekcX/O3faNGhlwmWAOBNGyT1B3C/kmbfAMCrgbJXEsvshSlUVhooI5MTWv7og2rb+SJnZ/ok5U9r6dce1eKXniVYAoA3Q2WHmc08KBW1TvOdzXcJAC/rl7SF1sqQhErzRVs2UE6Pnwxrd9dyljz6L1ry/JOVBstBLjAAqFuolILVVXS65bWD0wvAi8zQg6459yw0SEsdTnivKliHsvXoAS39xjcDtfZkLSwe/JGa8kd1/Df/sNxLOyTlxFNmAKi19ea/XelMtjveF8v5vKLWPjsgB2GfAB9ehxskdZp6XHuF9blhSeOm/jca74uNBvwwJWf9f/+cP6POarqkiLkgciq2nBEoXTS56fpKgqUkbY/3xRK+L6iRSKe5uVZqtFAojDb4M1f6JVDxZ45EItNfLraGC4XCuEfOpaN9KBQKOZePty/KkQvnvhE8c9zqUPHrlrQzSPfcdCablPTArL/6bLwvlmpQpdovZX443hcbd7iftt9vVoL0QMBBmXB8Xhq0b72SuvVe61u1xkw9PCdp0C/HwuK62T/nrxtyr6piH2zrLeNeXj6wpcYHKuV2oIzeuka3bP+u1Wd5dsvvKP/jfXU7qFMbV+nY7/+J1Xsu/OeH1LLrYMWvL445/cdKguWWdCab89NFtoDEnEpOOVvV+CdWG+ZUNt34zJ2SHnfwWbxwPKaD0S4Hb91hvhTdPN6Vfubp/x3RrCfAknJ1Dk4DLlY0as0T5a2O1/nce27S5y0EiTL7SJk/36YK7lFufb/Z1sem/zevYkvWqPkZNqHLT2XVtkxUc17qFST7TZiM1uBXdKg4/GyLpEfSmewOEy79XifUAt8xSZM9/PT9YVNvGTIPHcIVKs2JXV+ysjY5oSVPfIcWSofadr6os5fldGJd2fI1kM5kh738dAOVKRQKg5FIZMhBReuBSCSS8kDrkdMZ2rwwVmKmi+OswDkmaVDSQFha5lA2VE6XV1+O7zFzIHR4JFTCXVFz/+qac84D25rl8essqfqPWe6R1JPOZAdM+Brw4wOwWROJnRei05lsb7wvNkgpq7+mGp3sXkn3lAuUTMpTvcWPPV3JrLBR+evJDWoTsBo65XYkEumVs1aHrR4ObB3mXrc/EomkTDdthMt85zzh44nSkhUGZwTHdGvWI5KOpjPZwSDNZOy1MJnOZEfNsW7kJFjR6e+udCabMiHN7/epautI8FqonNXttaSlPxgkULpkyaP/otajB8q9bL0ZJwOfKxQKw5K2O3hrTyQS6W7gR3cSavPyz/pTW0y4HDDjOxEOXQtU2HxXKTfjQ+er6EaZTTxUelTsKjmezmSTnHt3ri2PhMkFv7v8cq7NZ+wtdU9mWb2AhEpTASzZL3zJ7hzrULps6Te+qcjkRLmXPcCFFhj9JnDVI9hVLRKJJB1+kfZ7ZYIhC/dIypnxowh2RXFDmWvUb5Il/o3yHD5RFcd6jhIunQegdCY7qOK4Oa8vz/OAT67zfpUff0prpd9DpXnKWXI9ytajB7T4sac58m6fyPxpXfC/U54NFXCXCVpOzuX6SCSSqHOgbHd4gx8pFAopn56i9QTLUCh1fjvMUBC/VH67Vbp7ejenO/ThcthPZdoj19Soii2/cCmkV1if2OLDLr2EyjlKVgAjkxNakv4WR71GWnYd1JLduXIv62KsRGCCZVLF6cJt1bt7ZtneCwvw+5PGKMEy1KHSb2W43PcCFTR0SHrcjLmk1bJ0+Emq2DoZ5Wi4fp+q9JjSWunXUJnOZPtVpml/6U++p+YDxznqNbT4sacrGV85wBdCYDi5aUbrdbM1gWqLg7duL7cuJcESPgiVvhjbU2ImRUIl5tOjYpdY7mvnX0vt6Uw2pRouEUOdp/IASl3Xh6HSnLRkqdcsOvSy2p78KUe8DpY88R3PhArUVqFQGFRx3SJbD9RpplKnk/MkA3SaopIGmbwnkCqZzdgP99qkS/uK8IhK2kXPp/Pqwjk5e5CK8sc3Ibtxqb6cMC30oVJlBs1GJie06PEsR7tOml96p5JusP08wQkMTy4xUsUSIkFc87FDLOsTtApOpa00nh7bU8FMirNf28mZxxyPmDUPCZTFQLm+Rr9iSNIOSVsX+BmSNBLww5ysY/0IDrS4dCGVPGmLR4bo9lpnbU8N6eR1N6nQtnShl0y3ViY5Wv5WKBSGI5HINpVZG3YePZFIpLsW3UxNq5yTisaY6jeZVF7ScIl/d7tlpicSifSa1mX4n03Xv4SH77WVzKQ4e59HOfWY4550Jtse74slCJSu2WG2mYv3xYYtP0+3ihNr9dYw5Nb7GHfL2ey5HelMNhHvi6W4TH0QKst9ITUfO6y2nT/hSNdZU/60lv5gUMd/8w9Lnrt0JjsQ74uNc8R8Lym7AezTBlSbKcTLjrFeaD/quITIcKFQ6K4gIHebY9St6mfxG5BUr1C5KSDjUoMQKj15r7WYSXH2Pnv5ocimeF8sDGV+a7wvlqzg/HaqOBa23Zy76Z9aLG2xJZ3JKqTB0q1AOf1QNVXNvcJcAzlJSVMG+h3WD7xWx3EqIXoK+SZUlryBLH7u+2rKn+ZIN0Dbzhd18ubDOnPhJQu9ZLq/OcuM+FyhUBg3a0E+aPnW9ZFIJOHm0h1mrKaTLidDXlxCxASznN6bNbfXfME5emrq9vGGL0Jl1JQbr533XsuKJhOz+Ei8Lzaq91qWB+c8TOjVe61ZboWNLelMdjzeFwtNl0MzKU+1gXJMUrIWrWmmDPSb2Wir+e5q5DHuVnU9h7rSmWx3SB44NVRTlSc6UapwFlspX+QoN9Di575f7iX0Nw9OsByQN5YYSSqgS4gUCoXxQqGQKhQKnSqOY3F6fOB/XQE477afqZPTHoiwOR7vi6XifbFEvC/WLumzcjbh23zuCcvkPWbVg2on5dka74t11rp75vQ5V/HB0FYVh3/4Rb9HtoFahkpV0EqJxmrb+aKajx0u9ZIOFjMOFCdf5q7NBmy6ijpdQmTYTwfarBO6ycGXcwdLjPi+MukkXHnqXutgJkUpIOOzcF7gSMX7Yt3mfuZGuHwk6MuNmNazB6vYxIikjZV0Y65BuEyacLnDJ/faHhc21cNEYx4OlebkLPikNjI54YtWyuita9TxxR6t2/ZlffgfvnreT8cXe3TR7Tf4+iRXEO4TXArBYLpqNnKJESdfkHn59CmiOd5Orh+uOX9zWmH2Ujl3VAZNZRrBDJe5WeFyrMrNDQZ1hnmzX6kqNrFdUrftBDwun+vReF+sV9JmebvVMunRbWEe1YypLDvjq2d3esUi/cYf3KErP/0ZLb3q+pKvveTjd0mSJsff1r5v/rVef+wpTR095a+T/LNfKfKJiVIzwfaYmduYsCcYEpL2O3jfgCpcWmA+kUgkIedLiPi27BUKhcFIJLJddi20VMzDGSq70plspxnn1MhKcbecj1Hq5PQHP1yalsYBOe/e2WEq8UHsdpiS83GJ2700mVG8LzZozvWgPNYTwTReubnmZy913dqqpvtrycpn6wve7MkWvXWNPvrYP+q6+75SNlDO1tZ+qa677yv6+Pf/j6K3rvHXSc6f1qL9u6s6n/BVyBmVtM3BW3tM91UngbJdzp4CjplupH5nuw90I/S37jqWlVqopqJPqAxHsBw34efeKjZzT9Bats3+OO2OudWLs+OaVssNKrageonbDySiYmyl90Kleaqx4FOaxa/u9uS6lJdtvlm3bP+uVZicL1zesv27umzzzb460W3P/TuhMlySctalxelMwE6XEAnEDd4EeavxKU4DPDyhmvFiWxrZLdCFMUqU23CFywEVJ/JxKmizy6ccvm97vcdPOjjXCa/MkGrukbUI4IRKr4XKcie6+eWfe25HL7r9Bm38asq17X3wS3+pZWtX+uZEN7/0jlqPHij1kp6gjn8II9Od1MkX2HrTjdUmHHXK+RIigwE67Lb7wvXmQyaUVbsEQyMrNtVWbJlkKnzBMlVFsFwflNlgzbIcTh6ebg/p+p3V3iNrsa5mNCyzE/spVHaX+seWn/3KW4HqgqVa/+cPubrNtvZLdd29/81XJ7vtF89XdV7hu2BZryVGkg5v/kG7sY9SOQ8FN85bQ0KlS2OUojyADG2wDO0ySqbMO7luR0TrWL2OdWjKY2BCpTnZC44HWvzqbjXlT3tqJz/437eqrf1S17d7ycfv8tX4yuY9e8u9hC6wweMkuFU87qCKJUS2mS6jQTJMcSNUWgSzhE/uB7U6BvBfsEzK2TIUHQFoHXLScpaX1MvEMNZ6LY913kF57OYweyBUlgseXuz6Ws0YynIuv+MO/4TKA8fLrVnJRRYwZskLJ5WASpcYSTrYdl4BfFLo5xlsYcWt+2RdrwGXn/4TKsMrIWfj9YMQKq2v8UbP9OxTtvfGAdlPMpTkMHsjVJb8Mmn+1VioDuClt3zSV5+3bX/J0N/B4rCB5LQiWXKChSqWEEkSwOBjbgWqej8tT8i9MUqEypAyrW5OAmKXX+sXppXV9toZMpMcwf5Y245bHZD9hFBd1He9ESoX/BJsPnbYk7O+1tLSq67Xos7l5x7UfW979vM2v7qPykLImG6mTsbCLLjESJVLiATyi5bZXENR4emUu5NH1HOslZu/i8pYuIPloKQhj5f3Rl87SUpKXY71drP8zbCDMsk58kCoXHA8ZevBX/tip9/4129r71//z5mfsX/6mibHnQfBxasuOfegemxM6Tmh8sXXCJXhNCB3lxhxuoRIIsDH2HbyElpr/cftMNVTj6flDp/+l9JFUQg9JxVy383bYK5P23WFh7yyNIfPjnW3g2M9uxymLN+7hdbKBoZKsz7lwoHltX2e3tn87h/qh5/epN33/LnGHt4x87P3/of0zO2/pfzuHzra7rKrr6r6s7XsOlifE54/zbjKEDLdTZ08bT1viREz1vIBB9vaYcZ4BpXttcPEPsE/x7WqnNtyvYWIyli4mdBk2zLkxyE2vR69pnlQUWylHJ1VJlOyn/E+wWFvUKhUmVaspnHvPnif2P+iXvj8n+r4nkPz/vvU0VN64fN/6qjFsnXZBb466a1HSwZYKgrBDZYpFac3tzV3iRGn3VeDPq26beVjlFLpO7XoydFbyyU6HD79b9SxgL+k6hTSGsk2dIzQSun4PtXlQvmzrZ/0s0RS40JlycBRr9Y2J176269q6uipkq+ZOnpKr33nG678vkWHXvbssYi8+Xqpf+7gsgg0J8FuZokRM26wx8E2tgZwCZH3rqlia67NtZMP8vEgVDq7vmok6aNjAR8xLUO2wyq6fRR0Si6h52LQhn14X6iLsW2ZjIrl9FzT4taXSJkulQ01sf9FvfX4cxW99sjwLl1d7wrp5ERdf1/zm2+Uu5FuMIOeETCFQiEXiUR2OAiGD0QikZSctVLm5bx10w+B0smkRbk6fLSBSCTSiO4jKdMqHiimglmrh26JWoQ/M2SlVuMfvRgqB9KZbEPKvAlYYTQou7WK/fQwotvh8YDdfapT9utdz1uniPfFxtOZrG2ZTPIwoDGhcsEm4pbjRzy7k28/+28Vv/bM/5uo++drO3KgvpXgkycdn2cfeSASiTzAJT6vfjlrbcw5rFT3B3wJkZSD41KPULm+QccjF9DzXMvKcEc6k03UIJjUsgW004PniDLfmH23qcB3BPiaH2FdSkeSlq8fMzMQl9qeVZms0f03dFzr/tr0lne7vp44+HrFrz3+y9eDf9LLL3lCt6YAq2KJESeVgZEgtlpJxRbKSCQy6DCg8zTbf7otX287iYmrAdDh03+bBcTXUyTgJFDXeX3Wel7zOYpDXe5TJUOoCfY7LLeZ4GzUP1QuXKk8dcKzO3ls3ysVv7bcuMuKD+xrr3j3pJdf8oRBy8E3IPtZ0pwI5OQ8ZmzpsMNAuYPxlL5k87At76CSst7lyrbttTdkWyn2UThAjZgKvO24Sr/UMToJlTVne5/MV9iiaDvkpov7Wf1DpS81okurVQV18mQDfucEpT/ETHfUZI1/zfYgLSFiWiYTkUhkWNJOOe/GNUAJDHyoHHb4tNyVhzBm/KdtZS0p+xmJOykWkP3ySH7pDWV7jx+lKFjfp2zveRV9f5pJfFhexK+hstzkLyhRWX2z/l1u6z2OE54MlinZd9GrVF4+XKsrEol0z/lJRiKRARMkj0p6RNV1+wv6Wp1BrvzYVDCHHT5A6HFpHb9+FWc1rNT0Mgi24YBQCSeh0g/XvHXZZoLDmt+nbCf9s62DbGH9XY+EygomfwnXgT3l6+PBmMrwqFXwG/BBF8+uSCRSmP2jYgvk7J8HJN0jd8aP5RX8tTqDyvaeOGoqmTnZrw3rRhlx9PQ/3hcbl11Xxm6KBiTZTsTmh4q77WfMUwxqfp9KmXtUpSE/5eC8JDktdQiV9DW2PLCHfN1yy5jKkDCtZttd3uyY6OI57xcoYyl9y/b7b3huYLOQqGYx7nQmm5Dd0/+xOWOUbFpbOikaqENg8wNaKWt7n3JyL3Xynt5q7r+ESjT+JOSPchDQSEm5+5Q1GfAlRJzYHtRZcKkEn2/2otwmsNmM7YmqurE9ySorXTaV4w4qYBBjCVH7+9R2h8u12H7vRkWPorqESiqJtToJ4xxaNI5pPXOrZXGI8DRvoExwGHzNpvvrmAsVG0eVmnQm2yu7sZ/5eT7beA2PDQiVCDnTSmk7CZKjOooJora9sfi+rnWoZACyncjEKT9/fFJu+Li1xAhP+AiUQWQzpnZ0gevLpjdAh6l41fr6G5hnjFKOUAlLtFajlqFtqMoMkrJ8vdP7b+i1uLWhwuLFHM1Zml96x88fnwcIIVMoFMYjkUhSxdlNqwlQlJ33bC0UCslGBlo1pgUhF6ST6GA+gfP2P94XG09nsoOyW+Q7YVMZMp+zy4XKlm2Z6fTQ6aLMNwYPFlDL+1RV36PxvlgunckOWf7efgdhlFDp1obOvO9ytWofR9SByNv1nzTs1MoPBP2wbvfADWGDpAd9FCxTkUgk4eCGLzGz6WxjkhIeWDokxfIlDakwD5eoGNmEyq50Jts9e3xmBSHU6h453xileF9sNJ3J+jVQpCyOFxpnNID7RGttbQLimEvXdMqybrPe8v4LN0MlnGs+cJyDUIMvrUZXqCORiF9v+DsdvG+AyXmKa2g1uHUSjQ+V81aYTVizfVqeUAWtYGZttS0OrvWF2HzOLopI6LW7cY343HqKQdn71AbVuZVy1v03lc5kk7Iby5kUyyZZsZ39dcH1tgrLV3A054aKyQlPfq6z0dYw3vBRAYdLjIyFPEiNSdoqqZNASagsM/bHtnxUuhi37XaHysykaPUdwILhXCMB3CfroRzMhFyWbW+mucsdVct2W13c22obKhdsiShEL+JoztF25IA3Q+WaSwmVcPPGmwrhMRqTtE3SpkKh0FkoFFhGJbhsWiCGygTOnOwnxOqvoCLrZiulk+8AKl7hZnv+PT/2fp4JrMIarl3hsDeF2+td206YVsm9ElWEygW/aKaWESqdaD52uP6hsr3swzQqx0DRiAkKO1Rsjdws6SoTJPsZsxj4ilC3W9+RVVRSEmVaQGyf/g9VME7IttLfTWkJdViwXR7CL3WMEUKla2zve/Mtd+TGg4JBy7dtoQW6AaHyzIWXcDQdaDl+pP6hMrqi3IXHDJ4Ig6FCoRAp87OhUCh0FwqFXtMaOWjW9UQ4uDKecs79NSW7p+VRSb0LVOjbHYTKlBv7QWUaTh8o+Gjyk9FaH4uQPHhw0ptiwGFrsdvhVmISwsozjeXrS4aNqY2r1LLrIEfViOSPSCu997nOXnF1qX8e48wBgKOwVGlleUDSA5YVofnCYK8JnZWqaIxSvC82bDkDbCdFhVBZaRn00b4NS+ohVFbNSSjrNBPr1ELe8r7Zn85kaxVyQx0qR0uGlZWXSyJUTmvd8zNF3j1a8jVN+aN1/1xluiqPcuYAwFFYqrSXh22o7Ehnsr3xvtjgPGHThs3rR1T5eFJmvgyvXsvX53y0bznL6zS6wHUaWg57U0j2LZu1NN1bJMUZLc2q+2u5bpFnLr2cIzo7se86qMWDPyr507bzxbp+pjOrl5XrqpzjzAGAJLvp7/OVPsk2r7OdZbl/TmUtIbuxbHnZjScataw80gU2fIEhIbsWH8kHk/RU+Vl7KRnn3beiAdiPJKfS5VBpLDi73elV7+eIetyZa8rWQRhPCYAKs31Isr132lZSuuZ8Jtun/7bdt2z3h1AZPgkH7/FNK565XoZsQyUTu1RdRryoI53J8sCgBqFywS+aMxdeojOrl3FUvRwqr1hT7iU5jhIAqKah0qwTaVth7TeBt1v2XU5tp+cftXx9J0UmPEwZtF3IfqzM+qheZBuCo2Jil+kykpD9zMBexnmtQagsGTrOrL2Oo+phZVqTRxiIDAC1D5VG0vL1W8wSDrbv2+7g3m5b+e+myIRK0sF7Uj7cTyctq/20VjouI17WRTf/eofKK6/mqHrUmWsvZjwlANQmVNqGsOmlFWxnw0zKvoUo6fCz2eikyISDaYHqcvBW34VKhz0KQt9aabqKdgRw12itdDNUmqedCy4Ie/LKdTobbQ31QT0bbdXUxlVWP/U4Zqc3bKzqgQEAhIhVpbmKtfdsA5/trIg7quhyaBN4O2idCUVYaJd9V2pJGvJh19dqwvADpldBWAU1fG0J+XktqaWKC+zBhf5x6kPX1H1WU0+FyjWX6tjv/4nVey7854fUVOM1PievuqFcpYhpsAFQcbbv4jRSxa8bNJX0Ws2QOFDFe0dl19qwQTycDLpBh2V1wK87HO+LpcyaiR0OjlXouks6HG/rt8BMi+U8mhy+r+SXxtQNH+HIeszUxlXlur7u4CgBwEw4sg1fTius4zWscA9V0YJa9rveheMGf4WFlMOwMBaAh9ZOrtH16Ux2IIRFJRnw/UvQK8PFUGnWq1ywW8yplR/w/CywizqX68KbO0Jzok+v/VC5l9BKCQDOwlG1SzGlarQf1W7XdnKfTopOYANlv5wvSB+EVp2U7Mc/S9I9ZgxqWMpJp4LdSikVW+oT3BXO11LFewdUogvs6ds+qubHnvbkTi9bu1JXf/5zamu/WHveTOrU6LuBPslno606ee0tpV5iuyg2ABAqXQqV8b7YaDqT3V5FpX0+Y/G+WLWhkrUqMT0xz4MO3z4UhKE18b7YuOkC+4iT+nI6kx02DTJePs/t0/taxWaSTsqIR+75Nt26++XjLt1eDJWDpW4yJ6+7SW3RITXlT3tup6/+/Od0+W//kSTp2nvf0e57/jzQJ3ly001lzyVLiQDADNsn7W5UFgdcDpVJF7YxXOPjBu8HylSV5TIwY8/M2MqEg3IelZRLZ7LdXg2WJlDmzPnKOdxGp4OyMhTvi3V7YP+Tkh6weEtHOpNNuPDgLlCaqri4RlViHF6hbWklYaZuFnUu16o//qQ+/A9f1cTrr878/cTrr2rdti9r1R9/Uos6lwfyJJ9cX/b+x0UBAHI0SY/cmNXSVDbdemLvSu8T87Ax76BiCf9fB+3pTDZXZaDc6vXWuTqG5NnBFlsAAA/YSURBVJlg6cFz3a3iuPD1VW4qWaf31EKqjmWBUOnkJJxc3+WZ5UVOjb6rtfd/XZd8/C5d/YUvzfz91V/4ki7/7T/S2vu/HshusJObrlehbWmpl4xVOZEDAASJbShys+uWW92pBlzsfTJc4+MH74WMXhMyqml5Hon3xZJBOzYmJG+tIlju9NIYS9NCt1NVzj5tWjp7Ld/mmfqneTC43fJt6734kMC3odL0k19w4LKXWiujt64p+5pla1cG6uSejbbq5M23l3tZkssAAGbUe5Keir9TGxBOnewflSz/hsnOdCY7KOnxKkNG3kHA8FOwTKq6h0mPpDPZVCNnEDXnOie7Lp+l9DsoM16rf6Yc7jfcCJWVFAqvtFbmf7xPz/+XuzU5/vZ5/zY5/rZ2/VlCx/ccCtTJndx0U7llRPL0BweAqkLRqMu/v9qK1naXx8gzA2w4wmRK0n5JPS5ssteNLuEe1yvLruFzbJE03IiWLjOT77BcGgNtwrFtuBrzWv3TtJrarjncQ5d/F0OlKRSlWyvv8MbY/SPf/7l+8Rf/47y/H/nyn+itx58L1Ik9G22tZCwlM1cBwLka1lJpDFZZWU26/HlyNT5+aFyQTJjWqv1yb5Koz4ZhSI15cNNd5bXaoWJ32MF6BJN0JtudzmRHVZxkM+riphMOtpfy6Kl1Ui9OCpKqm/117gFdcJrlE+u61Tq8S80vvdPwHc7v+qX+7aYNet/vfkySdPiZ5wN5Yk/FN5cbS5knVALAuRVtB5UjV0OlWbpgQM66pe2oQQuR7fbWU5I8V643SGo3IajT/LcWC3V/Nky9n+J9sWHT6vdIlZvqUbHFa7uklNuh3IzhTKh2szPbtlJ6tv5pZvgdsPwe6E1nsu2souBSqDQnIVnqJnXirrt1wUsPNz5szZmMJ4iT80xtXKWTV64r+yCACwAItIFIJNKoazxVKBT8WLm0bWUbq9F9NOUwVLpeUTNraFqHmAbN+jmQzmQbVeb767jPiQq6TXbWKDgSKM+v/8qFYCkVW4u3pDPZMRV7LAw6DZimfPSan5qVAxNYbbc/4PH6p+1DvagJ1sl6fEeZ3gU1+d6u9hpucfHDJEtdVKdXrNbknR9W25M/reqXHP/l63p2y+9Yv2euwz9YuIXSdvsnDx4+589N+97Whf/8kNU2mva97cpJOBtt1cSn+iqpCNFKCQRbI1uMcj49ZrahcrRGFdVR02ph0yVxqIbdDodk18rRKfe7BXu9zNdz0pWOOgdGAmX9guX0+b1H0j1muyPmXjO8wP11uiW60/zUc8yZkyDl9bLipKdIvUJltIbnt+rvjxaXL6pEqZ09/rHNat43WlU32Kmjp5T/8b6qP++psYVbKKvdflP+tJp2HWzIlXCq51PlJueZLvwAgHN1eyg8pyxDZS0fFI5aVmQ2yIV1MuF5eRUn5cmF/UDUIFjOtt78TE+i9IAX9tm0hto+3Nju9UmczBAE24d60XQmmwj75JctLm+vX9KuUi84cdfdWnrom2rKn3b8SyY3Xa+m8WLL+VTHVZKk5jffUOTkyZk/S1LTqZNqe/KnOtl7W3Fnx/arZddBtaxYpPff+zm1RVdo/MXdmpqY0DvPPK+Vd2xS/qWXdGVvTJP5o8r/8hdqixYfPh5/7VUtu+JKSdL47j1qX7dWJ948pLcef04dX+zRiTcP6UhueGa7r/z9N3V8zyFdtvnmYth78221r1urQ0/t1Mo7Nmns4R266PYbtPp3epT/5c819vCOqg/+5KbrdfLaW8q9bMhMWw8AOD8M2YatWlVsculMttIWwrEa39dt97ObohR4IwrHLK+2wXLUPFCJhmCXk3V6TyMMyH7yqqS83wpbU00uX1DDkraVes3pFat1qudTVf2eqRs+oqmOqzTVcZVO3HinCstX6NRtt+vkJ39PheUrdOLGOzV53Y06/rHNmrzzwzpx452aev9anW0vBsTf+IM7dPmdcS1d3amWxUu09v6v69p7/6vWfO4+Xdkb08W3fkrtH9yo6LX/ofh3n/6Mlr//Gl1331d0yU23adH7LtV1931FH/zSX2rZ2pW67r6v6Iq7Yur8Qt/MdqddcVdMV9wVU/u6tcXX9fXouvu+okWdy3Xj33xHS1d36vLf+l21rFhU1TE5c+3FmvhE2WWh8ioO1gYAzOKFSXrmUWkFpdYVNdv97KREBdq2eF9sA4Fy3npwTsWHKiNB3k/TSmnbDXOHX8qMyTO2a5F2NGKJmMCGyllfbiUXbz557S2avPPDVf2Syetu1OR1N0qSWvf8TKdXrNbpFavVuudnxb87+GtFJid0dtFiSVLzWwdmWjclqa39Ur06mNarj+7Q5Pjbuvy3/0ivfecbM//+8t9t097kw3p3z7N69X89qneef0GSdPgnP1J+1y9ntnHF3Z8+77Pt+/bfL7jm5RV3f0GSdPHHi5/9hc//qZ6NfUFTR085PhZno606cdfd5WZ7lYqT8/AlAADns14Ko9YTs5Rbssuox3rDtt8bHRSnQBqTtCneF2MITfn7Qrek7QHeTSdlwG9zeTi5ryYJle5eTOOqoDXs+Mc26/RH17jyO1t2HVTzscNqPnZYLWYs48lrb1HL8SNqfaH4nX/mstUzLZWvP/aU3vjXb2vt/V/XRd0b9MaTaUnSa5kdeuXvv6l39zyrG//mO1q2duV5v+uSm25Ty4XvhbfL74xrcrw4yc7oNzJ6d8+z2vjVlC66/YZ5P2tb+6XF0LvsAkmqKkzOhPTP/EedXrG63MuGmJwHAFwLlfVqiRio8t/dqiRbCfsT+4DJS9oa74t1Mn6y8rpwvC+WkLRZ1a1l6TmmV0eP5duG/FZ2KnyoN1eXWcKHUOniichJ2lo2WN4e15lrL3b0O9r2Pq+2ve/N4Npy/Ihajh+Z+fOS55/UBX/1sJoPHJdUbKmcdlH3Bk28/mrxde9bqRMHi7PDnhp9V5fcdrOOHxgtbvPC81v+Dv/kRzN/f/iZJ9TWfqne+fH3Zrab37tHkrT4fe87/73PPKGJ/S8W//9Hz0mS1m37stZt+7Iuuv0GdXyxx/o4nPjM7+rUyg9U8oXQKwCAW6GyXrObpkpUSuu53ttIjY8nPBomJXXG+2JJDoej+vCgit3Bt/ng426r8L7mpCykfHoKnXzu0LbkN9XwQkqqTH/kQttSHYv/Z+tg2fTWQTXlj6opf1SLDr383t+9VWylXHToZTXlj868ftGhl1WIXqSzl62aCZJX3P0FHX7mCb3+2FM68eYhHX7mieK/rfoNXX5nXG/867eV//E+5ffu0Yk3D2nq2IQOP/PEzJjKw888odeeyOrwM0/oUO5p5ffuOWe7b/3LD4t35L17ZraR37tHr3zrIR1+5gkd33NIr3zjL3TxrcXxpRdceaValy+3DpQVTMwjSQnWpAQAV0PQaJ0qpeMlKjaDdby32+5vJ0XKt8Yk3TsdJqk/VH8Nmy7DV0na4cGPuF3SVfG+WH+5c21aKW0nsBnz8ayoTh7abTHHKXRaarz9XvNFFC0XLC9M/13FS40sfuzpmf9v2/nieX+39GuPnvP6uX8ee3jHObOtvvX4c3rr8WLL4d77H9Le+99bY/LXX/3WzP//9D/92TnvOee/em5m27PNfv/0aw5+699m/m32v9coUG5ltlcAKFlRapf9OMBcnSs298zz98k6foZh2XV5o6XSX0ZUnLV0sNZjhUMcLkcl9ZrAkXQQztw03cshZTnXhpNWuKSPz5mT5UWk4jDAZNjKeFOtT4aKg5VL9id32mIZVhaBcgddVgCgJgFotF4fzlT65k76Ue/13kbrcExRH0OmPG2VtEnSCjOba5JAWZ/r2Yy3XKFii3A9Z4rdIWlzvC/Wbs53xde1efiWsA2vAVi70UlrZb85XqHSUoeLZzidyfarzIKwTlosCZQljShYy4fkavz6WlXCtvrsM4fpc4/67BpIeehY+6VM2LApO2rATNopnfu0PNWAc7qVMl+T+0muDtfheEACo22Z8Ox93jS8DEgaMK2XvSo2xHTLvXUuR8zxyrnQa63dQcDyfZkzOeZes/+2x2u8gvK51SO7WvV9KFIoFOrySdOZbKJcsJSkyOSElv5gcKZbK4rORlt1Kr5ZJ69cV8nLxyRtYBwEAARHOpPNqbg23FC8L9bNEQECe613qjguuduEk9kt/11z6nujc4LBuKRhZupFvdUtVJqLJKUK+yUvef5JLR78EWdI0pnVy3Qi/seVLBsiFbsad9OFBQACV9FMqPhwdjNj5QEAoQ2VtsFy8au7tSj9uJryp0N7gk5/dI2O3x5XoW0pgRIACJY5WikBAKEPlbbBsvnYYS39XkYtuw6G6sScjbZq8o4unVhXcd2BQAkAAAAgHKHSBMuFpkif15LdObU9NRSKVsupjat08pO/V2l3VwIlAAAAgPCFShMsE6pg8p5pQW+1dNA6KRVn9upmUh4AAAAAoQuVJlj2qjhFdMXTJy869LIWPZ5V84HjgTkRk5uu18QneisdOzltSFIvgRIAAABAaEOlCZYbJA1K6rB53+KXnlXb/93p63A5uel6nbz5dp258BLbt26L98X6KcIAAAAAQh8qTbBsV7HFssf2vX4Ml1WEybykBNPJAwAAACBUzh8u+yUlZdEddtqiQy+r5ecvqG3ni5482GdWL9Ppj2zQyfVdtt1cp42o2N11lKILAAAAgFC5cLDcoGKr5XpHOzU5oUX7d6t1z88aPqnP2Wirpj50jaZu+IhOrfxANZvaGu+LJSmyAAAAAAiVlYdLx62WcwNm82v71Pyrsbp0kZ3auEpTHVdp6v1rbZYFWciQpH6WCwEAAABAqHQWLDtNsNzixvaajx1W69GDirz5ulrG9ivydr6qoDm1cZXOtrfr7GWrdPayVdW2Rs6WN2EyRTEFAAAAQKisPlx2m3DZVYvtNx87rJbjR2b+3PTaK+f8e2H5ChWiF8382cXwOF+YHJA0wFIhAAAAAAiVPguXDUSYBAAAAECorHO47JeDJUgIkwAAAAAQ8lA5K1x2mnCZUBUT+jTAkKQUYyYBAAAAECq9EzB7JU3/eDFgjqi4VMoga00CAAAAIFR6P2B2m5/1DfoYeUk580OQBAAAAECo9GnAbDfhcsOs/9aiJXNE0vD0T7wvlqOIAQAAACBUBjdobpDUaX5kAmclRs2PVGyFHI/3xYYpTgAAAAAIlQAAAAAAVKiJQwAAAAAAIFQCAAAAAAiVAAAAAABCJQAAAACAUAkAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAABCJQAAAACAUAkAAAAAIFQCAAAAAAiVAAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAIFQCAAAAAAiVAAAAAABCJQAAAAAg0P4//U9DqP/Pc9YAAAAASUVORK5CYII='
                            // }, {
                            //     margin: [425, -100, 0, 20],
                            //     alignment: 'right',
                            //     width: 500,
                            //     table: /*function (subTbl) {*/
                            //             /*return*/ {
                            //                 // headers are automatically repeated if the table spans over multiple pages
                            //                 // you can declare how many rows should be treated as headers
                            //                 headerRows: 1,
                            //                 alignment: 'right',
                            //                 widths: [70, 70, 70, 70, 70, 70],
                            //                 body: subTbl
                            //             }
                            //     /*}*/
                            // });
                            doc['footer'] = (function (page, pages) {
                                return {
                                    columns: [
                                        {
                                            alignment: 'left',
                                            text: [
                                                {text: 'Printed on:' + moment().format(sk.getMomentDatetimeFormat())}
                                            ]
                                        },
                                        {
                                            alignment: 'right',
                                            text: [
                                                'Page ',
                                                {text: page.toString(), italics: true},
                                                ' of ',
                                                {text: pages.toString(), italics: true}
                                            ]
                                        }
                                    ],
                                    margin: [10, 0]
                                }
                            });
                        }
                    },
                    {
                        "extend": 'excelHtml5',
                        "text": '<i class="clip-file-excel"></i> Excel',
                        "className": 'btn btn-info btn-squared',
                        "id":'btnExport',
                        title: $("#reportHeader").val() != '' ? $("#reportHeader").val() : 'Billing Cycle',
                        messageTop: $("#reportSubheader").val() != '' ? $("#reportSubheader").val() : '',
                        exportOptions: {
                            columns: ["thead th:not(.noExport)"]
                        },
                        //"customize": _customizeExcelOptions
                    },
                    {
                        "extend": 'print',
                        "text": '<i class="fa-print fa"></i> Print',
                        "className": 'btn btn-blue btn-squared',
                        orientation: orientation,
                        pageSize: 'LEGAL',
                        title: $("#reportHeader").val() != '' ? $("#reportHeader").val() : 'Billing Cycle',
                        messageTop: $("#reportSubheader").val() != '' ? $("#reportSubheader").val() : '',
                        exportOptions: {
                            columns: ["thead th:not(.noExport)"]
                        }
                    },
                    {
                        "extend": 'copy',
                        "text": '<i class="clip-share"></i> Share',
                        "className": 'btn btn-primary btn-squared',
                        title: $("#reportHeader").val() != '' ? $("#reportHeader").val() : 'Billing Cycle',
                        messageTop: $("#reportSubheader").val() != '' ? $("#reportSubheader").val() : '',
                        customize: function (doc) {
                            columns: ["thead th:not(.noExport)"]
                        }
                    },
                ],
                "oLanguage": {
                    "sSearch": "",
                    "sLengthMenu": "Per page _MENU_",
                    "oPaginate": {
                        "sNext": "<i class='clip-chevron-right'></i>",
                        "sPrevious": "<i class='clip-chevron-left'></i>"
                    },
                    "sInfo": "_START_ to _END_ rows out of _TOTAL_",

                },
                "infoCallback": function( settings, start, end, max, total, pre ) {
                    return start +" to "+ parseInt(end) + ' rows out of '+ total;
                },    
                dom: 'Bfrtip',
                "columns": colHeader,
                "columnDefs": [
                    {"targets": 0, "orderable": false}
                ], //<div class="sk-light-gray w3-light-grey" style="padding: 0px -15px;margin: 0px -15px;"><div class="sk-blue w3-blue" style="height: 5px;width:15%;"></div></div>
                "sDom": '<"wrapper"<"row"<"col-sm-12" <"col-sm-7 actionButtonsDiv"> <"col-sm-5 sk-norightpad" Bl > <"col-sm-12" <"sk-light-gray progressBarCont" <"sk-blue ProgressBarMain">><"row well customWell" <"col-sm-3 customSearchInput" f> <"col-sm-3 customSearchField">  <"col-sm-6 CustomPagination"ip> > > >  >rt<"clear">>',
                initComplete: function () {

                    $(".entireClickable").click(function () {
                        if ($(this).find('input').prop('checked')) {
                            $(this).find('input').prop('checked', false);
                            $(this).removeClass('highlight');
                        } else {
                            $(this).find('input').prop('checked', true);
                            $(this).addClass('highlight');
                        }

                    })
                    var inputHtml = '<div class="input-group">' +
                            '<input type="text" placeholder="Contains..." data-focus="true" class="form-control DatatableAllRounderSearch" />' +
                            '<span class="input-group-addon cursorPointer btnClearSearchTxt"> ' +
                            '<i class="clip-cancel-circle-2 "></i>' +
                            '</span>' +
                            '<span class="input-group-addon cursorPointer"> ' +
                            '<i class="clip-search-2"></i>' +
                            '</span>' +
                            '</div>';

                    $(".customSearchInput").html(inputHtml);

                    var inputHtml2 = '<div class="input-group">' +
                            '<select class="newDrProfile DatatableAllRounderSearch"></select>' +
                            '<span class="input-group-addon cursorPointer resetDrProfile"> ' +
                            '<i class="clip-cancel-circle-2 "></i>' +
                            '</span>' +
                            '</div>';
                    $(".customSearchField").html(inputHtml2);
                    $(".newDrProfile").html($("#past_claims_doctor_id").html());
                    $(".newDrProfile option:eq(0)").text('Change Dr. Profile');
                    $(".drSelectMainPopular").select2('destroy');
                    var currentDrProfile = $(".drSelectMainPopular").val();
                    $(".drSelectMainPopular").select2();
                    $(".newDrProfile option[value='" + currentDrProfile + "']").prop('selected', true);
                    $(".newDrProfile").select2({width: '100%'});



                    var searchoptions = $("#" + pastClaims.resultsTable + " thead tr:eq(0) th");
                    var customfilterinputs = '<tr>';
                    for (var j = 0; j < searchoptions.length; j++) {
                        customfilterinputs += '<th></th>';
                    }
                    customfilterinputs += '</tr>';
                    $("#" + pastClaims.resultsTable + " thead").append(customfilterinputs);

                    $('#' + pastClaims.resultsTable).on('length.dt', function (e, settings, len) {
                    });
                }
            });

            $('#' + pastClaims.resultsTable + " tbody").on('click', 'tr', function () {
                if ($(this).find('input[type="checkbox"]').is(':checked')) {
                    $(this).find('input[type="checkbox"]').prop('checked', false);
                } else {
                    $(this).find('input[type="checkbox"]').prop('checked', true);
                }

            })
            // save page length 
            $('#' + pastClaims.resultsTable).on('length.dt', function () {
                var info = $('#' + pastClaims.resultsTable).DataTable().page.info();
                sk.setClientParam('dt_page_size', info.length);
            });

            $(".DatatableAllRounderSearch").keyup(function () {
                datatable.search($(this).val(), true).draw();
            })
            $(".resetDrProfile").click(function () {
//                            $(".newDrProfile").select2('destroy');
                $(".newDrProfile option:eq(0)").prop('selected', true);
//                            $(".newDrProfile").select2();
                $(".newDrProfile").trigger('change');//select2('destroy');

            })


            $(".CHKcontainer input[type='checkbox']").change(function () {
                if ($('.CHKcontainer input[type="checkbox"]:checked').length > 0) {
                    $(".actionButtonsDiv a").attr('disabled', false);
                } else {
                    $(".actionButtonsDiv a").attr('disabled', true);
                }
            });

            $('#resultsCheckAll').click(function () {
                var checked = $(this).prop('checked');
                $('#' + pastClaims.resultsTable + ' input[type=checkbox]').prop('checked', checked);
            });

            $(".btnClearSearchTxt").click(function () {
                $(".DatatableAllRounderSearch").val('');
                datatable.search('', true).draw();
            })
            $(".otherSearchFilter").change(function () {
                var data = $(this).select2({placeholder: "Claim Type", width: '100%'}).val();

                if (data && data.length > 0) {
                    var d2 = '';
                    for (var i in data) {
                        data[i] = data[i].substring(1, data[i].length);
                        d2 += data[i] + '|';
                    }
                    d2 = d2.substring(0, d2.length - 1);
                    $("#" + pastClaims.resultsTable).DataTable().columns('.charge_to')
                            .search(d2, true)
                            .draw();
                } else {
                    datatable
                            .columns('.charge_to')
                            .search('')
                            .draw();
                }
            })

            $(".newDrProfile").change(function () {
                var data = $(this).select2('data');

                var aa = $("#" + pastClaims.resultsTable).DataTable().columns('.doctor').data();

                if (aa.length) {
                    if (data) {
                        data = data.text;
                        $("#" + pastClaims.resultsTable).DataTable().columns('.doctor')
                                .search(data)
                                .draw();
                    } else {
                        datatable
                                .columns('.doctor')
                                .search('')
                                .draw();
                    }
                } else {
                    if (data) {

                        data = data.text;
                        var newoptionval = $(".newDrProfile option:contains(" + data + ")").val();
                        $(".drSelectMainPopular").select2("val", newoptionval).trigger('change');
                        $("#find_button").trigger('click');
                        //$("#" + pastClaims.resultsTable).dataTable().fnDestroy();

                    } else {
                        datatable.columns('.doctor').search('').draw();
                    }
                }
            })

            $('div.dataTables_filter input').addClass('form-control');
            $('div.dataTables_filter input').attr('placeholder', 'Search');
            $(".DTsearchlabel").html('<i class="clip-search"></i>');
            $('.dataTables_filter').attr('style', 'width:100%');
            $('.dataTables_filter label').attr('style', 'width:100%');
            $('.dataTables_length select').attr('style', 'font-size: 12px;padding: 5px 3px;background: #fff;');
            $('div.dataTables_filter input').attr('style', 'float:right;width:98%;margin-right:5px;');
            $(".dataTables_wrapper .dataTables_paginate .paginate_button").attr('style', 'background:#fff !important;color:#444444 !important; border-radius: 0;');

            $(".CustomPagination").prepend('<div style="float:left"><button data-toggle="tooltip" title="Filters" type="button" class="btn btn-default filterToggle"><i class="fa fa-filter"></i></button></div>');

            $(".filterToggle").click(function () {
                $("#" + pastClaims.resultsTable + " thead tr:eq(1)").toggle();
            })
            pastClaims.isAddedActionBtn = true;
        } else {

            var datatable = $("#" + pastClaims.resultsTable).DataTable();
        }
        var tblWidth = (+newColumnsOrder.length * 10) - 40;

        datatable.on('search.dt', function () {

            pastClaims.clearSubTotalTable();
            pastClaims.updateSubTotalTable();
        })
        datatable.on('length.dt', function (e, settings, len) {

            pastClaims.clearSubTotalTable();
            pastClaims.updateSubTotalTable();
        });
        datatable.on('page.dt', function () {

            pastClaims.clearSubTotalTable();
            setTimeout(function () {
                pastClaims.updateSubTotalTable();
            }, 200);
        });

          pastClaims.processDataTableData(datatable);
          //Past claim sort stay same page http://live.datatables.net/siqaxoqi/1/edited
          var page = 0;
          datatable.on('order', function() {
            if (datatable.page() !== page) {
              datatable.page(page).draw('page');
            }
          });
          datatable.on('page', function() {
            page = datatable.page();
           });
    },
    clearSubTotalTable: function () {

    },
    updateSubTotalTable: function () {
        pastClaims.itemArray = [];
        var data = $("#past_claims_results_table").DataTable().rows({"search": "applied", "filter": "applied", "page": "current"}).data();
        if (data != null && data.length > 0) {
            var obj = {};
            var cnt = 1;
            var patients = [];
            // pastClaims.itemArray = [];
            for (var i in data) {
                var s = data[i][0];
                if (typeof s != 'undefined') {

                    var temp = document.createElement('div');
                    temp.innerHTML = s;
                    var id = $(temp).find('.item_id').val();
                    var obj2 = JSON.parse(localStorage.getItem(id));

                    // if(obj2 != null) {
                    // pastClaims.itemArray.push({date_created :obj2.date_created,submitted :obj2.submitted});
                    // }



                    if (cnt == 1) {
                        obj = obj2;
                    } else {
                        if (obj2 != null) {
                            obj.submitted = +obj.submitted + +obj2.submitted;
                            obj.paid = +obj.paid + +obj2.paid;
//                            obj.claims = +obj.claims + +obj2.claims;
                            if ($.inArray(obj2.patients, patients) < 0) {
                                patients.push(obj2.patients);
                            }
                            for (var s in obj2.pay_progs) {
                                if (obj.pay_progs[s]) {
                                    obj.pay_progs[s]['claimItemCount'] = obj.pay_progs[s]['claimItemCount'] + obj2.pay_progs[s]['claimItemCount'];
                                    obj.pay_progs[s]['submitted'] = obj.pay_progs[s]['submitted'] + obj2.pay_progs[s]['submitted'];
                                    obj.pay_progs[s]['paid'] = obj.pay_progs[s]['paid'] + obj2.pay_progs[s]['paid'];
                                    obj.pay_progs[s]['claims'] = Object.assign(obj.pay_progs[s]['claims'], obj2.pay_progs[s]['claims']);
                                } else {
                                    obj.pay_progs[s] = obj2.pay_progs[s];
                                }
                            }
                        }
                    }
                    cnt++;
                }
            }
            obj.claimItems = data.length;
            obj.claims = 0;
            obj.patients = patients.length;
            for (var k in obj.pay_progs) {
                obj.pay_progs[k]['claims'] = Object.keys(obj.pay_progs[k]['claims']).length;
                obj.claims = +obj.claims + +obj.pay_progs[k]['claims'];
            }
            pastClaims.results.subtotals = obj;
        } else {
            pastClaims.results.subtotals = {submitted: 0, paid: 0, pay_progs: {}, claimItems: 0};
        }

        // grand total calculation

        var data = $("#past_claims_results_table").DataTable().rows({"search": "applied", "filter": "applied", "page": "all"}).data();
        if (data != null && data.length > 0) {
            var obj = {};
            var cnt = 1;
            var patients = [];
            
            for (var i in data) {
                var s = data[i][0];
                if (typeof s != 'undefined') {

                    var temp = document.createElement('div');
                    temp.innerHTML = s;
                    var id = $(temp).find('.item_id').val();

                    var obj2 = JSON.parse(localStorage.getItem(id));

                    //console.log('result',obj2);

                    if(obj2 != null) {
                     pastClaims.itemArray.push({date_created :obj2.date_created,full_date_created :obj2.full_date_created,submitted :obj2.submitted,paid :obj2.paid,claimItems :obj2.claimItemCount,claims :obj2.claims,patients :obj2.patients});
                    }


                    if (cnt == 1) {
                        obj = obj2;
                    } else {
                        if (obj2 != null) {
                            //date group by
                            obj.submitted = +obj.submitted + +obj2.submitted;
                            obj.paid = +obj.paid + +obj2.paid;
//                            obj.claims = +obj.claims + +obj2.claims;
                            //obj.patients = +obj.patients + +obj2.patients;     
                            if ($.inArray(obj2.patients, patients) < 0) {
                                patients.push(obj2.patients);
                            }

                            for (var s in obj2.pay_progs) {
                                if (obj.pay_progs[s]) {
                                    obj.pay_progs[s]['claimItemCount'] = obj.pay_progs[s]['claimItemCount'] + obj2.pay_progs[s]['claimItemCount'];
                                    obj.pay_progs[s]['submitted'] = obj.pay_progs[s]['submitted'] + obj2.pay_progs[s]['submitted'];
                                    obj.pay_progs[s]['paid'] = obj.pay_progs[s]['paid'] + obj2.pay_progs[s]['paid'];
                                    obj.pay_progs[s]['claims'] = Object.assign(obj.pay_progs[s]['claims'], obj2.pay_progs[s]['claims']);
                                } else {
                                    obj.pay_progs[s] = obj2.pay_progs[s];
                                }
                            }
                        }
                    }
                    cnt++;





                }
            }
            obj.claimItems = data.length;
            obj.claims = 0;
            obj.patients = patients.length;
            for (var k in obj.pay_progs) {
                obj.pay_progs[k]['claims'] = Object.keys(obj.pay_progs[k]['claims']).length;
                obj.claims = +obj.claims + +obj.pay_progs[k]['claims'];
            }
           
            pastClaims.results.totals = obj;
        } else {
            pastClaims.results.totals = {submitted: 0, paid: 0, pay_progs: {}, claimItems: 0};
        }



        var result = [];
        pastClaims.itemArray.reduce(function(res, value) {
          if (!res[value.date_created]) {
            res[value.date_created] = { date_created: value.date_created, full_date_created:value.full_date_created, submitted: 0,paid: 0,claims: [],patients: [],claimItems:0 };
            result.push(res[value.date_created])
          }
          res[value.date_created].submitted += parseFloat(value.submitted);
          res[value.date_created].claimItems += parseFloat(value.claimItems);
          res[value.date_created].paid += parseFloat(value.paid);
     
          if($.inArray(value.claims,res[value.date_created].claims) == -1){
              res[value.date_created].claims.push(value.claims)
          }

          if($.inArray(value.patients,res[value.date_created].patients) == -1){
              res[value.date_created].patients.push(value.patients)
          }
          return res;
        }, {});


       

        // result.sort(function(a,b){
        // var c = new Date(a.full_date_created);
        // var d = new Date(b.full_date_created);
        // return c-d;
        // });

        var category = [];
        var submitted = [];
        var paid = [];
        var claims = [];
        var claimItems = [];
        var patientCount = [];
        var itemD = [];
        var maxamount = [];
        
        for(graphRs in result)
        {
           // console.log('graphRs',graphRs);
            var cat = result[graphRs]['date_created'];
            var feeSubm = result[graphRs]['submitted'];
            var feePaid = result[graphRs]['paid'];
            var claimCount = result[graphRs]['claims'].length;
            var itemCount = result[graphRs]['claimItems'];
            var pCount = result[graphRs]['patients'].length;
            category.push(cat);
            submitted.push(feeSubm);
            paid.push(feePaid);
            claims.push(claimCount);
            claimItems.push(itemCount);
            patientCount.push(pCount);
            maxamount.push(feeSubm,feePaid,claimCount,itemCount,pCount);
        }
        var rs = [];
        rs['category'] = category;
        rs['feeSubm'] = submitted;
        rs['feePaid'] = paid;
        rs['claimCount'] = claims;
        rs['itemCount'] = claimItems;
        rs['patientCount'] = patientCount;

        var maxamt = parseInt(Math.max.apply(Math,maxamount)) + parseInt(1);
        rs['maxamount'] = maxamt;
        //console.log('arr',rs);

        pastClaims.updateGrandTotalGraph(rs);
        //itemD.push(rs);
        //console.log('result',result);  
       

        //console.log('resultresult',submitted);  

        //get grand total calculation report
        //console.log('grandtotal',pastClaims.results.totals);
        pastClaims.clearSubTotalTable();
        pastClaims.results.buildTotalsRow();
    },
    updateGrandTotalGraph:function(rs){

                var categories = rs['category'];
                var feeSubm =  rs['feeSubm'];
                var feePaid = rs['feePaid'];
                var claimCount = rs['claimCount'];
                var itemCount = rs['itemCount'];
                var patientCount = rs['patientCount'];
                var maxamount = rs['maxamount'];
                var resetChart = function(){
                $('#grandtotalchart').highcharts({
                   chart: {
                    type: 'column',
                    height: '40%'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories:categories,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    //max:maxamount,
                    title: {
                        text: 'Amount'
                    }
                },
                tooltip: {
                    // headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    // pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    //     '<td style="padding:0"><b>${point.y:.1f}</b></td></tr>',
                    // footerFormat: '</table>',
                    formatter: function() {
                        var $html = '';
                        $html = '<table>';
                        //console.log('thisthis',this.points[0]);
                       // if (this.points[0].y < 6)
                        $html += '<tr><td colspan="2" style="text-align: center; font-weight: bold;">' + this.points[0].key + '</td></tr>';
                        for(toltipData in this.points)
                        { 
                            if(this.points[toltipData].series.name == 'Fee Submitted' || this.points[toltipData].series.name == 'Fee Paid') {
                             $html += '<tr><td style="color: ' + this.points[toltipData].color + '">' + this.points[toltipData].series.name + ': </td><td><b>$' + this.points[toltipData].y.toFixed(1) + '</b></td></tr>';
                            } else {
                              $html += '<tr><td style="color: ' + this.points[toltipData].color + '">' + this.points[toltipData].series.name + ': </td><td><b>' + this.points[toltipData].y + '</b></td></tr>';
                            }
                        }
                        // else
                        //  return '2nd';
                        $html += '</table>';
                        return $html;
                    },
                    shared: true,
                    useHTML: true
                },
                series: [{
                    name: 'Fee Submitted',
                    data: feeSubm //[49.9, 71.5,49.9, 71.5]

                }, {
                    name: 'Fee Paid',
                    data: feePaid //[83.6, 78.8,49.9, 71.5]

                }, {
                    name: 'Claims',
                    data: claimCount //[100, 78.8,49.9, 71.5]

                }, {
                    name: 'Claim Items',
                    data: itemCount //[20, 78.8]

                }, {
                    name: 'Patients',
                    data: patientCount //[25, 78.8,49.9, 71.5]

                }]
              });
            };
            resetChart();   
    },
    updateDefaultData: function (data = null) {
        //console.log(data);
        if (data) {
            $.ajax({
                type: 'POST',
                url: '/appointment_search/savereportparams',
                data: {type: "past_claims_default_report_cols", data: data},
                success: function (rs) {
                    console.log(rs);
                }
            })
    }
    },
    processDataTableData: function (datatable) {
        var currentPage = pastClaims.dtCurrentPage;
        var maxPages = pastClaims.maxPages;
        var dataSet = [];
        if (currentPage == 1 /*&& pastClaims.resetAll == false*/) {
            pastClaims.request = $.ajax({
                url: pastClaims.actions.search + '?pageNumber=1&size=' + pastClaims.dtCurrentResults + '&' + $('#' + pastClaims.form.formID).serialize(),
                type: 'GET',
                success: function (res) {
                    var obj = JSON.parse(res);

                    if (obj.hasOwnProperty('errors')) {
                        quickClaim.showFormErrors(obj.errors, '#' + pastClaims.form.errorDiv, '#' + pastClaims.form.formID, true);
                        $('#' + pastClaims.messages.error).show();
                        $('#' + pastClaims.messages.text).html('<div class="alert alert-info">The form contains invalid data.</div>').show();
                        return false;
                    }

                    $('#tabs').tabs('enable', 1);
                    $('#tabs').tabs('option', 'active', 1);

                    $('#' + pastClaims.messages.text).html('Processing Results.').show();
                    $('#' + pastClaims.messages.checkmark).show();

                    if (obj.hasOwnProperty('data')) {

                        s = obj.headers;
                        var out = Object.keys(s).map(function(data){
                            return [data,s[data]];
                        });

                        var a = [];
                        for ( var i = 0; i < out.length; i++ ) {
                            if(out[ i ][0] != 0){
                                a.push( out[ i ][0] );
                            }
                        }
                        pastClaims.updateDefaultData(a.toString());


                        pastClaims.data = {};
                        pastClaims.errors = {};
                        if (obj.hasOwnProperty('hlx8lines')) {
                            pastClaims.hlx8lines = obj.hlx8lines;
                        }
                        pastClaims.explCodes = obj.explCodes;

                        if (pastClaims.results.showTotals) {

                            // clear old data
                            pastClaims.clearSubTotalTable();
                            pastClaims.results.subtotals = obj.subtotals;
                            pastClaims.results.totals = obj.totals;
                            pastClaims.results.graphstotals = obj.graphstotals;
                            pastClaims.updateGrandTotalGraph(pastClaims.results.graphstotals);
                        }
                        pastClaims.subtabledata = obj.subtabledata;
                        for (var j in obj.data) {
                            
                            dataSet.push(pastClaims.buildDataTableRow(obj.data[j]));
                            //console.log(dataSet);
                            // var idS = j+2;
                            // $('#past_claims_results_table tr:eq('+idS+')').attr('id','tr_');
                            //console.log(pastClaims.buildDataTableRow(obj.data[j]));
                        }
                        var datatable = $("#" + pastClaims.resultsTable).DataTable();
                      
                        datatable.rows.add(dataSet);
                        //console.log(datatable.rows);
                        
                        datatable.draw();


                        //console.log('graphstotals',pastClaims.results.graphstotals['category']);

                        // var i = 0;
                        // for (var j in obj.data) {
                        //         //console.log(obj.data[j]['id']);
                        //         $("#past_claims_results_table tbody tr:eq("+i+")").attr('id','tr_'+obj.data[j]['id']);
                        //         i++;
                        // }





                        //$('#past_claims_results_table tbody tr:eq(1)').attr('id','tr_');
                        pastClaims.dtCurrentPage++;
                        pastClaims.maxPages = Math.ceil(obj.count / pastClaims.dtCurrentResults);
                        if (pastClaims.maxPages > 1) {
                            var message = "Found total " + obj.count + " records and fetched " + pastClaims.dtCurrentResults + " records, other records are fetching in background.";
                            $.notify(message, {
                                element: 'body',
                                type: "info",
                                allow_dismiss: true,
                                newest_on_top: true,
                                placement: {
                                    from: "top",
                                    align: "right"
                                },
                                delay: 5000,
                                z_index: 99999,
                                mouse_over: 'pause',
                                animate: {
                                    enter: 'animated bounceIn',
                                    exit: 'animated bounceOut'
                                },
                                template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                            <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                            <span data-notify="icon"></span>
                                            <span data-notify="title">{1}</span>
                                            <span data-notify="message">{2}</span>
                                            <div class="progress" data-notify="progressbar">
                                                    <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <a href="{3}" target="{4}" data-notify="url"></a>
                                    </div>`

                            });
                        }
                        //                        if(pastClaims.maxPages == pastClaims.dtCurrentPage){                            
                        $(".selectRsltTbl").select2('destroy');
                        var aa = 0;

                        var apiDt = $("#" + pastClaims.resultsTable).dataTable().api();
                        apiDt.columns().every(function () {

                            var column = this;
                            var columnText = $.trim($(column.header())[0].innerText);

                            if (columnText == 'Error(s)') {
                                $("#" + pastClaims.resultsTable + " thead tr:eq(1) th:eq(" + aa + ")").html('');
                                var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                        .appendTo($("#" + pastClaims.resultsTable + " thead tr:eq(1) th:eq(" + aa + ")")/*$("#" + pastClaims.resultsTable + " thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                                        .on('change', function () {
                                            var val = $.fn.dataTable.util.escapeRegex(
                                                    $(this).val().replace(/<\/?[^>]+(>|$)/g, "")
                                                    );
                                            val =  val.replace(/\\/gi, "");
                                            column
                                                    .search(val ? val : '', true, false)
                                                    .draw();
                                        });

                                column.data().unique().sort().each(function (d, j) {
                                    d = $(d).text();
                                    d = d.replace(/<\/?[^>]+(>|$)/g, "");
                                    if(d != '') {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                    }
                                });
                            } else if (columnText == 'Actions') {
                                $("#" + pastClaims.resultsTable + " thead tr:eq(1) th:eq(" + aa + ")").html('');
                            } else if (aa != 0) {
                                $("#" + pastClaims.resultsTable + " thead tr:eq(1) th:eq(" + aa + ")").html('');

                                var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                        .appendTo($("#" + pastClaims.resultsTable + " thead tr:eq(1) th:eq(" + aa + ")")/*$("#" + pastClaims.resultsTable + " thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                                        .on('change', function () {
                                            var val = $.fn.dataTable.util.escapeRegex(
                                                    $(this).val().replace(/<\/?[^>]+(>|$)/g, "")
                                                    );
                                            val =  val.replace(/\\/gi, "");
                                            column
                                                    .search(val ? val : '', true, false)
                                                    .draw();
                                        });

                                var chkarr = [];
                                column.data().unique().sort().each(function (d, j) {
                                    d = d.replace(/<\/?[^>]+(>|$)/g, "");
                                    if ($.inArray(d, chkarr) < 0) {
                                        if(d != '') {
                                        select.append('<option value="' + d + '">' + d + '</option>');
                                         }
                                        chkarr.push(d);
                                    }

                                });
                            }
                            aa++;
                        });
                        $.fn.dataTableExt.oSort["dateField-desc"] = function (x, y) {
                            var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                            var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                            return (a.diff(b));
                        };

                        $.fn.dataTableExt.oSort["dateField-asc"] = function (x, y) {
                            var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                            var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                            return (b.diff(a));
                        }
//                        }
                        $(".selectRsltTbl").select2({
                            placeholder: "Search",
                            allowClear: true,
                            dropdownAutoWidth: true,
                            width: '98%'
                        });
                        $('#' + pastClaims.resultsTable + ' .select2-arrow').hide();

                        if (pastClaims.results.showTotals) {
                            pastClaims.results.buildTotalsRow();
                        }
                        if ($("#" + pastClaims.resultsTable + " thead tr:eq(1)").is(':visible')) {
                            $("#" + pastClaims.resultsTable + " thead tr:eq(1)").toggle();
                        }
                        if (pastClaims.isAddedActionBtn == true) {
                            pastClaims.results.buildActionButtons();
                            pastClaims.isAddedActionBtn == false;
                        }
                        pastClaims.afterDtDataFinished();
                        pastClaims.results.buildErrorTooltips();
                        pastClaims.updateProgressBar();
                        pastClaims.processDataTableData(datatable);
                    }
                },
                beforeSend: function () {
                    if (pastClaims.request != null) {
                        pastClaims.request.abort();
                    }
                    quickClaim.showOverlayMessage('Searching for claims');
                },
                complete: function () {
                    quickClaim.hideOverlayMessage();


                }
            })
        } else if (currentPage <= maxPages /* && pastClaims.resetAll == false*/) {
            pastClaims.request = $.ajax({
                url: pastClaims.actions.search + '?pageNumber=' + currentPage + '&size=' + pastClaims.dtCurrentResults + '&' + $('#' + pastClaims.form.formID).serialize(),
                type: 'GET',
                success: function (res) {
                    var obj = JSON.parse(res);
                    if (obj.hasOwnProperty('data')) {
                        pastClaims.data = {};
                        pastClaims.errors = {};
                        if (obj.hasOwnProperty('hlx8lines')) {
                            pastClaims.hlx8lines = obj.hlx8lines;
                        }
                        pastClaims.explCodes = obj.explCodes;

                        pastClaims.subtabledata = obj.subtabledata;

                        for (var j in obj.data) {
                            dataSet.push(pastClaims.buildDataTableRow(obj.data[j]));
                        }
                        datatable.rows.add(dataSet);
                        datatable.draw();

                        pastClaims.results.graphstotals = obj.graphstotals;


                        $(".selectRsltTbl").select2('destroy');
                        var aa = 0;
                        var apiDt = $("#" + pastClaims.resultsTable).dataTable().api();
                        apiDt.columns().every(function () {

                            var column = this;
                            var columnText = $.trim($(column.header())[0].innerText);
                            var selectedSearch = $("#" + pastClaims.resultsTable + " thead tr:eq(1) th:eq(" + aa + ")").find('select').val();

                            if (columnText == 'Error(s)') {
                                $("#" + pastClaims.resultsTable + " thead tr:eq(1) th:eq(" + aa + ")").html('');
                                var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                        .appendTo($("#" + pastClaims.resultsTable + " thead tr:eq(1) th:eq(" + aa + ")")/*$("#" + pastClaims.resultsTable + " thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                                        .on('change', function () {
                                            var val = $.fn.dataTable.util.escapeRegex(
                                                    $(this).val()
                                                    );
                                            val =  val.replace(/\\/gi, "");
                                            column
                                                    .search(val ? val : '', true, false)
                                                    .draw();
                                        });

                                column.data().unique().sort().each(function (d, j) {
                                    d = $(d).text();
                                    if(d != '') {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                    }
                                });
                            } else if (columnText == 'Actions') {
                                $("#" + pastClaims.resultsTable + " thead tr:eq(1) th:eq(" + aa + ")").html('');
                            } else if (aa != 0) {
                                $("#" + pastClaims.resultsTable + " thead tr:eq(1) th:eq(" + aa + ")").html('');

                                var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                        .appendTo($("#" + pastClaims.resultsTable + " thead tr:eq(1) th:eq(" + aa + ")")/*$("#" + pastClaims.resultsTable + " thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                                        .on('change', function () {
                                            var val = $.fn.dataTable.util.escapeRegex(
                                                    $(this).val().replace(/<\/?[^>]+(>|$)/g, "")
                                                    );
                                            val =  val.replace(/\\/gi, "");
                                            column
                                                    .search(val ? val : '', true, false)
                                                    .draw();
                                        });
                                var chkarr = [];
                                column.data().unique().sort().each(function (d, j) {
                                    d = d.replace(/<\/?[^>]+(>|$)/g, "");
                                    if ($.inArray(d, chkarr) < 0) {
                                        var selected = (selectedSearch == d) ? 'selected' : '';
                                        if(d != '') {
                                        select.append('<option value="' + d + '" '+selected+'>' + d + '</option>');
                                        }
                                        chkarr.push(d);
                                    }

                                });
                            }
                            aa++;
                        });



//                        if(pastClaims.maxPages == pastClaims.dtCurrentPage){                   
                        
//                        }
                        $(".selectRsltTbl").select2({
                            placeholder: "Search",
                            allowClear: true,
                            dropdownAutoWidth: true,
                            width: '98%'
                        });
                        $('#' + pastClaims.resultsTable + ' .select2-arrow').hide();
                        pastClaims.dtCurrentPage++;
                        pastClaims.afterDtDataFinished();
                        pastClaims.updateProgressBar();
//                        if(pastClaims.resetAll == false){
//                            pastClaims.request.abort();
//                            quickClaim.ajaxLocked = false;
//                            datatable.clear().draw();
//                            
//                            pastClaims.processDataTableData(datatable);
//                        }else{
                        $.fn.dataTableExt.oSort["dateField-desc"] = function (x, y) {
                            var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                            var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                            return (a.diff(b));
                        };

                        $.fn.dataTableExt.oSort["dateField-asc"] = function (x, y) {
                            var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                            var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                            return (b.diff(a));
                        }
                        pastClaims.results.buildErrorTooltips();
                        pastClaims.processDataTableData(datatable);
                       
//                        }                       
                    }
                },
                beforeSend: function () {
                    if (pastClaims.request != null) {
                        pastClaims.request.abort();
                    }
                },
                done: function () {
                    if (pastClaims.resetAll == true) {
                        pastClaims.request.abort();

                    }
                }
            })
        }
//        else {
//            pastClaims.request.abort();
//            quickClaim.ajaxLocked = false;
//            var datatable = $("#" + pastClaims.resultsTable).DataTable();
//            datatable.clear().draw();
//            pastClaims.resetAll = false;
//            $(".ProgressBarMain").css('width', '0%');
//                            pastClaims.dtCurrentPage = 1;
//                            pastClaims.maxPages = 1;
//            pastClaims.processDataTableData();
//        }                                                                                                                                                 
    },
    afterDtDataFinished: function () {
        $(document).on('change', '.checkboxChk', function() {
        //$(".CHKcontainer input[type='checkbox']").change(function () {
            if ($('.CHKcontainer input[type="checkbox"]:checked').length > 0) {
                $(".actionButtonsDiv a").attr('disabled', false);
            } else {
                $(".actionButtonsDiv a").attr('disabled', true);
            }
        //});  
        });                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   

        $('#resultsCheckAll').click(function () {
            var checked = $(this).prop('checked');
            $('#' + pastClaims.resultsTable + ' input[type=checkbox]').prop('checked', checked);
        });
    },
    getErrorTitle: function(e,a){
        var title = '';        
        title += '\n';
                if (pastClaims.explCodes.hasOwnProperty(e)) {
                        title += pastClaims.explCodes[e];
                        if (pastClaims.hlx8lines[a]) {
                            var msg = pastClaims.hlx8lines[a];
                        } else {
                            var msg = '';
                        }
                        if (msg != '') {
                            title += ' - Error Code Message: ' + msg;
                        }
                    } else {
                        title += 'Unknown error code';
                    }
                
                return title = title.replace('?-&gt;', '').replace('&lt;-?', '').replace(/\HX8/g, '');
    },
    buildDataTableRow: function (data) {
        var id = data.id;
        var rowData = [];

        pastClaims.data[id] = {};
        pastClaims.data[id]['claim_id'] = data.claim_id;
        pastClaims.data[id]['patient_id'] = data.patient_id;
        pastClaims.data[id]['item_count'] = data.item_count;
        pastClaims.data[id]['edit'] = data.actions.hasOwnProperty('edit');
        pastClaims.data[id]['resubmit'] = data.actions.hasOwnProperty('resubmit');
        pastClaims.data[id]['submit'] = data.actions.hasOwnProperty('submit');
        pastClaims.data[id]['mark_closed'] = data.actions.hasOwnProperty('mark_closed');
        pastClaims.data[id]['mark_paid'] = data.actions.hasOwnProperty('mark_paid');

        rowData.push('<input type="hidden"  data-item_id="' + id + '" class="claim_id" value="' + data.claim_id + '"/><input type="hidden" data-patient_id="' + data.patient_id + '" data-claim_id="' + data.claim_id + '" class="item_id" value="' + id + '"/><span class="edit_checkbox"><label class="CHKcontainer"><input data-resubmit="' + data.actions.hasOwnProperty('resubmit') + '" data-edit="' + data.actions.hasOwnProperty('edit') + '" data-submit="' + data.actions.hasOwnProperty('submit') + '" data-resubmit="' + data.actions.hasOwnProperty('resubmit') + '" data-mark_closed="' + data.actions.hasOwnProperty('mark_closed') + '" data-mark_paid="' + data.actions.hasOwnProperty('mark_paid') + '" type="checkbox" data-claim_id="' + data.claim_id + '" data-itemid="' + id + '" id="' + id + '" name="claim[' + id + ']" value="' + id + '" class="checkboxChk" checked="checked" /><span class="CHKcheckmark"></span></label></span>');

        setTimeout(function () {
            contextMenu.createButton('actions_' + id);
        }, 500);

        localStorage.setItem(id, JSON.stringify(pastClaims.subtabledata[data.id]));
        if (pastClaims.results.columns == null) {
            pastClaims.initDatatable();
            return;
        }


        for (var a = 0; a < pastClaims.results.columns.length; a++) {
            var className = pastClaims.results.columns[a];
            className = className.replace(' ', '_');
            if (className == 'errors') {
                var e = data['errors'];
                title += '\n';
                if (pastClaims.explCodes.hasOwnProperty(e)) {
                        title += pastClaims.explCodes[e];
                        if (pastClaims.hlx8lines[a]) {
                            var msg = pastClaims.hlx8lines[a];
                        } else {
                            var msg = '';
                        }
                        if (msg != '') {
                            title += ' - Error Code Message: ' + msg;
                            }
                    } else {
                        title += 'Unknown error code';
                    }
                
                title = title.replace('?-&gt;', '').replace('&lt;-?', '').replace(/\HX8/g, '');
var dataError = data['errors'];
var errs = dataError.split(", ");
var err = '';
if(errs.length > 1){
    $.each(errs, function( index, value ) {
        var title = pastClaims.getErrorTitle(value,a);
        err += value != '' ? '<span id="errorTitle_' + id + '" data-errorCode="' + value + '" class="' + className + '" title="'+title+'">' + value + '</span>,' : '';
    });
    
}else{
    var title = pastClaims.getErrorTitle(e,a);
    var err = data['errors'] != '' ? '<span id="errorTitle_' + id + '" data-errorCode="' + data['errors'] + '" class="' + className + '" title="'+title+'">' + data['errors'] + '</span>' : '';
}
// if(errs.length > 1){
//     $.each(errs, function( index, value ) {
//         //var title = pastClaims.getErrorTitle(value,a);
//      err += value != '' ? '<span id="errorTitle_' + id + '" data-errorCode="' + value + '" class="' + className + '" title="'+title+'">' + value + '</span>,' : '';
//      var title += value != '' ? value : '';
//     });
    
// }else{
//    //var title = pastClaims.getErrorTitle(e,a);
//   err = data['errors'] != '' ? '<span id="errorTitle_' + id + '" data-errorCode="' + data['errors'] + '" class="' + className + '" title="'+title+'">' + data['errors'] + '</span>' : '';
//   var title = data['errors'] != '' ? data['errors'] : '';
// }


                

                rowData.push(err);
                if (data['errors']) {
                    pastClaims.errors[id] = data['errors'];
                }
            } else {
               /* console.log(pastClaims.explCodes);
                    console.log(e);
                    console.log(data);*/

                if (data[className]) {
                    var title = typeof data[className] != 'undefined' ? data[className].replace('_', ' ') : '';

                } else {
                    if (className == 'fee_subm') {
                        var title = '$0';
                    } 
                    // else {
                    //     var title = '';
                    // }
                    else if (className == 'fee_paid') {
                        var title = '$0';
                    } else {
                        var title = '';
                    }
                }
                
                rowData.push(title.replace(/<\/?[^>]+(>|$)/g, ""));
            }
        }
        var action = '<img class="actions_button" id="actions_' + id + '" src="' + pastClaims.results.menuHtml + '" />';
        if (data.item_count > 1) {
            action += data.item_count + ' Items';
        }
        rowData.push(action);
        return rowData;
    },
    updateProgressBar: function () {
        var currentPage = pastClaims.dtCurrentPage;
        var maxPages = pastClaims.maxPages;
        currentPage--;
        var percent = (currentPage / maxPages) * 100;
        $(".ProgressBarMain").css('width', percent + '%');
        if (percent == 100) {
            pastClaims.isBckgrndActive = false;
            $(".ProgressBarMain").removeClass('sk-blue');
            $(".ProgressBarMain").addClass('sk-green');
        } else {
            $(".ProgressBarMain").removeClass('sk-green');
            $(".ProgressBarMain").addClass('sk-blue');
        }
    },
    clearSubTotalTable: function () {
        $("#past_claims_totals tbody.PayProgSubtotals th").each(function () {
            $(this).html('');
        })
        $("#past_claims_totals tbody .PayProgSubtotals").html('');
        $("#past_claims_totals tbody.totals th").each(function () {
            $(this).html('');
        })
    }
};



