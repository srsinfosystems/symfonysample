var claimStatusActions = {
    actions: { 
        mark_closed: null,
        mark_paid: null,
        resubmit: null
    },
    baseClass: 'pastClaims',
    claimIDs: new Array(),
    claimItemIDs: new Array(),
    updateStatusRow: null,
    messages: {
        checkmark: 'claimStatusActionsCheckmark',
        error: 'claimStatusActionsErrorIcon',
        spinner: 'claimStatusActionsSpinner',
        spinnerHtml: '', 
        checkmarkHtml: '',
        text: 'claimStatusActionsMessagesText'
    },
    showConfirmations: true,
    totalNewPayments: 0,
    updatedItemsCount: 0,
    getStatus: new Array(),
    getClientId: new Array(),
    getClaimidSNew: new Array(),
    initialize: function () {
        $('#claimStatusActionsTable thead th.statuses').addClass('sorter-false');
        $('#claimStatusActionsTable thead th.message').addClass('sorter-false');
        $('#claimStatusActionsTable thead th:last').append(quickClaim.tablesorterTooltip);

        $('#claimStatusActionsTable').tablesorter({
            theme: 'blue',
            headerTemplate: '{content} {icon}',
            widgets: ['selectRow', 'zebra']
        });
        $('#claimStatusActionsTable .sortTip').tooltip();

        $('#claimStatusActionsDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: $(window).height() - 200,
            modal: true,
            width: '80%'
        });

         $('#markAdminLogin').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
            buttons: {
                "Yes":{
                    click: function () {
                        var modelid = $("#markAdminLogin").data('modelid');
                        pastClaims.confirmReLogin($('#password').val(),modelid);
                        $('#markAdminLogin').dialog('close');
                    },
                    text: 'Login',
                    class: 'btn btn-success administratorlogin'
                },
                "No":{
                    click: function () {
                        $('#markAdminLogin').dialog('close');
                        $('#claimStatusActionsDialog').dialog('close');
                    },
                    text: 'Close',
                    class: 'btn btn-danger'
                }
                        
            }
        });

         $('#markSliDoctor').dialog({
                    resizable: false,
                    modal: true,
                    autoOpen: false,
                    width: 500,
                    buttons: {
                        "Yes":{
                            click: function () {
                                var claim_id = $("#markSliDoctor").data('claim_id');
                                pastClaims.confirmReplaceSli($('#replace_sli_id').val(),claim_id);
                                $('#markSliDoctor').dialog('close');
                            },
                            text: 'Yes',
                            class: 'btn btn-success'
                        },
                        "No":{
                            click: function () {
                                $('#markSliDoctor').dialog('close');
                            },
                            text: 'No',
                            class: 'btn btn-danger'
                        }                        
                    }
                });

            $('#markAdmitdate').dialog({
                    resizable: false,
                    modal: true,
                    autoOpen: false,
                    width: 500,
                    buttons: {
                        "Yes":{
                            click: function () {
                                var claim_id = $("#markAdmitdate").data('claim_id');
                                pastClaims.confirmReplaceAdmitdate($('#replace_admit_date_id').val(),claim_id);
                                $('#markAdmitdate').dialog('close');
                            },
                            text: 'Yes',
                            class: 'btn btn-success'
                        },
                        "No":{
                            click: function () {
                                $('#markSliDoctor').dialog('close');
                            },
                            text: 'No',
                            class: 'btn btn-danger'
                        }                        
                    }
                });

            $('#markLocation').dialog({
                    resizable: false,
                    modal: true,
                    autoOpen: false,
                    width: 500,
                    buttons: {
                        "Yes":{
                            click: function () {
                                if($('#replace_location_id').val() == ' ')  {
                                  sk.skNotify('Select location', 'danger');
                                  return false;
                                }
                                var claim_id = $("#markLocation").data('claim_id');
                                pastClaims.confirmReplaceLocation($('#replace_location_id').val(),claim_id);
                                $('#markLocation').dialog('close');
                            },
                            text: 'Yes',
                            class: 'btn btn-success'
                        },
                        "No":{
                            click: function () {
                                $('#markLocation').dialog('close');
                            },
                            text: 'No',
                            class: 'btn btn-danger'
                        }                        
                    }
                });


        $('#markClosedConfirmation').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
            buttons: {
                "Yes":{
                    click: function () {
                        $('#markClosedConfirmation').dialog('close');
                        claimStatusActions.markClosed.sendAjax();
                    },
                    text: 'Yes',
                    class: 'btn btn-success'
                },
                "No":{
                    click: function () {
                        $('#markClosedConfirmation').dialog('close');
                        $('#claimStatusActionsDialog').dialog('close');
                    },
                    text: 'No',
                    class: 'btn btn-danger'
                }
                        
            }
        });




        $('#rebillConfirmDialog').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
            buttons: {
                "Yes":{
                    click: function () {
                        pastClaims.confirmRebillClick($('#rebill_new_date').val());
                        $('#rebillConfirmDialog').dialog('close');
                    },
                    text: 'Yes',
                    class: 'btn btn-success'
                },
                "No":{
                    click: function () {
                        $('#rebillConfirmDialog').dialog('close');
                    },
                    text: 'No',
                    class: 'btn btn-danger'
                }                        
            }
        });

        

        $('#markReplaceDoctor').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
            buttons: {
                "Yes":{
                    click: function () {
                        if($('#replace_doctor_id').val() == ' ')  {
                          sk.skNotify('Select doctor', 'danger');
                          return false;
                        }
                        var claim_id = $("#markReplaceDoctor").data('claim_id');
                        pastClaims.confirmReplaceDoctor($('#replace_doctor_id').val(),claim_id);
                        $('#markReplaceDoctor').dialog('close');
                    },
                    text: 'Yes',
                    class: 'btn btn-success'
                },
                "No":{
                    click: function () {
                        $('#markReplaceDoctor').dialog('close');
                    },
                    text: 'No',
                    class: 'btn btn-danger'
                }                        
            }
        });


        $('#rebillCompleteDialog').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
            buttons: {
                "Yes":{
                    click: function () {
                        $('#rebillCompleteDialog').dialog('close');
                        if (claimStatusActions.baseClass = 'pastClaims') {
                            var i = 1;
                            for(i=1; i<=2; i++) {
                               $("#find_button").trigger('click');
                            }
                            //$('#' + pastClaims.resultsTable).trigger('update');
                        } else if (claimStatusActions.baseClass == 'details') {
                            details.triggerPageRefresh();
                        }
                    },
                    text: 'Yes',
                    class: 'btn btn-success'
                },
                "No":{
                    click: function () {
                        $('#rebillCompleteDialog').dialog('close');
                    },
                    text: 'No',
                    class: 'btn btn-danger'
                }                        
            }
        });

        $('#actionCompleteDialog').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
            buttons: {
                "Yes":{
                    click: function () {
                        $('#claimStatusActionsDialog').dialog('close');
                        $('#actionCompleteDialog').dialog('close');
                        if (claimStatusActions.baseClass = 'pastClaims') {
                            $("#find_button").trigger('click');
//                          $('#' + pastClaims.resultsTable).trigger('update');
                        } else if (claimStatusActions.baseClass == 'details') {
                            details.triggerPageRefresh();
                        }
                    },
                    text: 'Yes',
                    class: 'btn btn-success'
                },
                "No":{
                    click: function () {
                        var statusThIndex = $('th:contains("Status")').index();
                        var claimStatusCodeThIndex = $('th:contains("Claim status code")').index();
                        var ClientIdArr = claimStatusActions.getClaimidSNew;   
                        //console.log(ClientIdArr); 

                        var getStatus = claimStatusActions.getStatus;  
                        //console.log(getStatus);
                       
                        var getCheckboxVal = claimStatusActions.updateStatusRow;
                        var result = getCheckboxVal.toString().split(',');

                        //console.log(result);

                        $.each(result,function(i,val){
                            if(statusThIndex > 0)
                            {
                            $("#tr_"+val+" td").eq(statusThIndex).html('<b>'+claimStatusActions.getStatus[0].status+'</b>');
                            }
                            if(claimStatusCodeThIndex > 0) { 
                            $("#tr_"+val+" td").eq(claimStatusCodeThIndex).html('<b>'+claimStatusActions.getStatus[0].claim_status_code+'</b>');
                            }                           
                        });
                        claimStatusActions.clearAfterStatusUpdated();

                        $('#claimStatusActionsDialog').dialog('close');
                        $('#actionCompleteDialog').dialog('close');
                    },
                    text: 'No',
                    class: 'btn btn-danger'
                }                        
            }
        });

        $('#markPaidConfirmation').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
             buttons: {
                "Yes":{
                    click: function () {
                         $('#markPaidConfirmation').dialog('close');
                        claimStatusActions.markPaid.sendAjax();
                    },
                    text: 'Yes',
                    class: 'btn btn-success'
                },
                "No":{
                    click: function () {
                        $('#markPaidConfirmation').dialog('close');
                        $('#claimStatusActionsDialog').dialog('close');
                    },
                    text: 'No',
                    class: 'btn btn-danger'
                }                        
            }
        });

        $('#resubmitConfirmation').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
            buttons: {
                "Yes":{
                    click: function () {
                        $('#resubmitConfirmation').dialog('close');
                    claimStatusActions.resubmit.sendAjax();
                    },
                    text: 'Yes',
                    class: 'btn btn-success'
                },
                "No":{
                    click: function () {
                    $('#resubmitConfirmation').dialog('close');
                    $('#claimStatusActionsDialog').dialog('close');
                    },
                    text: 'No',
                    class: 'btn btn-danger'
                }                        
            }
        });

        $('#resubmitCompleteDialog').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
            buttons: {
                "Yes":{
                    click: function () {
                    $('#claimStatusActionsDialog').dialog('close');
                    $('#resubmitCompleteDialog').dialog('close');
                    var ClientIdArr = claimStatusActions.getClientId;
                    if (claimStatusActions.baseClass == 'pastClaims') {
                        pastClaims.handleEditGroupClick(ClientIdArr);
                    } else if (claimStatusActions.baseClass == 'details') {
                        details.handleEditGroupClick(ClientIdArr);
                    }
                    },
                    text: 'Yes',
                    class: 'btn btn-success'
                },
               "No - Reload Search Results":{
                    click: function () {
                        $('#claimStatusActionsDialog').dialog('close');
                        $('#resubmitCompleteDialog').dialog('close');
                        if (claimStatusActions.baseClass == 'pastClaims') {
                             var i = 1;
                             for(i=1; i<=2; i++) {
                                $("#find_button").trigger('click');
                             }
                             //$('#' + pastClaims.resultsTable).trigger('update');
                        } else if (claimStatusActions.baseClass == 'details') {
                            details.triggerPageRefresh();
                        }
                    },
                    text: "No - Reload Search Results",
                    class: 'btn btn-warning'
                },
                "No - Back To Current Results":{
                    click: function () {
                        var statusThIndex = $('th:contains("Status")').index() + 1;
                        var claimStatusCodeThIndex = $('th:contains("Claim status code")').index() + 1;
                        var ClientIdArr = claimStatusActions.getClaimidSNew;    
                        var getStatus = claimStatusActions.getStatus;  

                        var getCheckboxVal = claimStatusActions.updateStatusRow;
                       
                        $.each(getStatus,function(i,val){
                            if(statusThIndex > 1)
                            {
                            //check index positon and add status in status td
                            $(".claim_"+val.claimitemid+" td:nth-child("+statusThIndex+")").html('<b>'+getStatus[0].status+'</b>');
                            //$(".claim_"+val.claimitemid+" td").eq(statusThIndex).html('<b>'+getStatus[0].status+'</b>');
                            }
                            if(claimStatusCodeThIndex > 1) {
                            //check index positon and add claim status code in claim status code td 
                            $(".claim_"+val.claimitemid+" td:nth-child("+claimStatusCodeThIndex+")").html('<b>'+getStatus[0].claim_status_code+'</b>');
                            //$(".claim_"+val.claimitemid+" td").eq(claimStatusCodeThIndex).html('<b>'+getStatus[0].claim_status_code+'</b>');
                            }                           
                        });
                        claimStatusActions.clearAfterStatusUpdated();
                        
                        $('#claimStatusActionsDialog').dialog('close');
                        $('#resubmitCompleteDialog').dialog('close');
                    },
                    text: "No - Back To Current Results",
                    class: 'btn btn-danger'
                }
            }
        });
    },
    clearAfterStatusUpdated()
    {
        claimStatusActions.getStatus  = new Array();
        claimStatusActions.getClientId = new Array();
        claimStatusActions.getClaimidSNew  = new Array();
    },
    markClosed: {
        complete: function () {
            if (claimStatusActions.baseClass == 'details') {
                details.triggerPageRefresh();
            } else {
                $('.updatedItemsCount').text(claimStatusActions.updatedItemsCount);
                $('#actionCompleteDialog').dialog('open');
            }
        },
        openDialog: function (claimItemIDs) {
            claimStatusActions.updateTitle('Mark Claim Items As Closed');

            if (!claimItemIDs.length) {
                quickClaim.showOverlayMessage('No selected Claim Items can be Marked As Closed at this time.');
                setTimeout(function () {
                    quickClaim.hideOverlayMessage();
                }, 2000);
                return;
            }

            claimStatusActions.buildTable(claimItemIDs);
            claimStatusActions.updatedItemsCount = 0;
            claimStatusActions.markClosed.confirmationDialog();
        },
        sendAjax: function () {
            var sendIDs = new Array();
            var extraIDs = new Array();

            for (var a = 0; a < claimStatusActions.claimItemIDs.length; a++) {
                if (a < 500) {
                    sendIDs[a] = claimStatusActions.claimItemIDs[a];

                    $('#claimStatusActionsClaims_' + claimStatusActions.claimItemIDs[a] + ' td.message').text('Marking Claim Item As Closed');
                    $('#claimStatusActionsClaims_' + claimStatusActions.claimItemIDs[a] + ' td.statuses').html($(claimStatusActions.messages.spinnerHtml));
                    $('#claimStatusActionsClaims_' + claimStatusActions.claimItemIDs[a]).addClass('currentlySending');
                } else {
                    extraIDs[extraIDs.length] = claimStatusActions.claimItemIDs[a];
                }
            }
            claimStatusActions.claimItemIDs = extraIDs;
            $.ajax({
                type: 'post',
                dataType: 'json',
                data: 'ids=' + sendIDs.join(','),
                url: claimStatusActions.actions.mark_closed,
                success: function (rs) {
                    for (var claimID in rs.claims) {
                        var message = '<ul>';

                        for (var claimItemID in rs.claims[claimID]) {
                            message += '<li>Claim Item ID ' + claimItemID + ': ' + rs.claims[claimID][claimItemID]['status'] + '</li>';
                        }
                        message += '</ul>';

                        $('#claimStatusActionsClaims_' + claimID + ' .message').html(message);
                        $('#claimStatusActionsClaims_' + claimID + ' .statuses').html(claimStatusActions.messages.checkmarkHtml);
                        $('#claimStatusActionsClaims_' + claimID).removeClass('currentlySending');

                        var myArray = {claimitemid: claimItemID, status: rs.claims[claimID][claimItemID]['status'],claim_status_code: rs.claims[claimID][claimItemID]['claim_status_code']};
                        claimStatusActions.getStatus.push(myArray);
                        claimStatusActions.getClientId.push(claimID);
                        claimStatusActions.getClaimidSNew.push(claimItemID);
                        claimStatusActions.updateStatusRow = sendIDs.join(',');

                        claimStatusActions.markClosed.updateBaseTable(rs.claims[claimID]);
                    }

                    if (rs.hasOwnProperty('updatedItemsCount')) {
                        claimStatusActions.updatedItemsCount += rs['updatedItemsCount'];
                    }

                    if (claimStatusActions.claimItemIDs.length) {
                        claimStatusActions.markClosed.sendAjax();
                    } else {
                        claimStatusActions.markClosed.complete();
                    }
                },
                beforeSend: function () {
                    quickClaim.ajaxLocked = true;
                    $('#claimStatusActionsMessages .icons div').hide();
                    $('#' + claimStatusActions.messages.spinner).show();
                    $('#' + claimStatusActions.messages.text).text('Marking Claim Items As Closed').show();
                },
                complete: function () {
                    quickClaim.ajaxLocked = false;
                    $('#' + claimStatusActions.messages.spinner).hide();
                },
                error: function () {
                    $('#' + claimStatusActions.messages.error).show();
                    $('#' + claimStatusActions.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            });
        },
        confirmationDialog: function () {
            if (claimStatusActions.showConfirmations) {
                $('#markClosedConfirmation').dialog('open');
            } else {
                claimStatusActions.markClosed.sendAjax();
            }
        },
        updateBaseTable: function (data) {
            for (var claimItemID in data) {
                if (claimStatusActions.baseClass == 'pastClaims') {
                    var field = pastClaims.results.rowPrefix + '_' + claimItemID + ' td.claim_status';
                    $('#' + field).html('<b>' + data[claimItemID]['status'] + '</b>');

                    if (pastClaims.data.hasOwnProperty(claimItemID)) {
                        pastClaims.data[claimItemID]['edit'] = data[claimItemID]['actions'].hasOwnProperty('edit');
                        pastClaims.data[claimItemID]['resubmit'] = data[claimItemID]['actions'].hasOwnProperty('resubmit');
                        pastClaims.data[claimItemID]['submit'] = data[claimItemID]['actions'].hasOwnProperty('submit');
                        pastClaims.data[claimItemID]['mark_closed'] = data[claimItemID]['actions'].hasOwnProperty('mark_closed');
                        pastClaims.data[claimItemID]['mark_paid'] = data[claimItemID]['actions'].hasOwnProperty('mark_paid');
                    }
                } else if (claimStatusActions.baseClass == 'details') {
                    details.triggerPageRefresh();
                }
            }
        }
    },
    markPaid: {
        complete: function () {
            if (claimStatusActions.totalNewPayments) {
                var currentSubtotal = $('tr.subtotals th.fee_paid').text();
                if (currentSubtotal) {
                    currentSubtotal = parseFloat(currentSubtotal.replace('$', ''), 10);
                    currentSubtotal += claimStatusActions.totalNewPayments;
                }

                var currentTotal = $('tr.totals th.fee_paid').text();
                if (currentTotal) {
                    currentTotal = parseFloat(currentTotal.replace('$', ''), 10);
                    currentTotal += claimStatusActions.totalNewPayments;
                }

                $('tr.subtotals th.fee_paid').text('$' + number_format(currentSubtotal, 2, '.', ','));
                $('tr.totals th.fee_paid').text('$' + number_format(currentTotal, 2, '.', ','));
            }

            if (claimStatusActions.baseClass == 'pastClaims') {
                $('.updatedItemsCount').text(claimStatusActions.updatedItemsCount);
                $('#actionCompleteDialog').dialog('open');
            }
        },
        openDialog: function (claimItemIDs) {
            claimStatusActions.updateTitle('Mark Claim Items As Paid');

            if (!claimItemIDs.length) {
                quickClaim.showOverlayMessage('No selected Claim Items can be Marked As Paid at this time.');
                setTimeout(function () {
                    quickClaim.hideOverlayMessage();
                }, 2000);
                return;
            }

            claimStatusActions.buildTable(claimItemIDs);
            claimStatusActions.updatedItemsCount = 0;
            claimStatusActions.totalNewPayments = 0;
            claimStatusActions.markPaid.confirmationDialog();
        },
        sendAjax: function () {
            var sendIDs = new Array();
            var extraIDs = new Array();
            
            for (var a = 0; a < claimStatusActions.claimItemIDs.length; a++) {
                //sendIDs1[a] = claimStatusActions.claimItemIDs[a];
                if (a < 500) {
                    sendIDs[a] = claimStatusActions.claimItemIDs[a];
                    $('#claimStatusActionsClaims_' + claimStatusActions.claimItemIDs[a] + ' td.message').text('Marking Claim Item As Paid');
                    $('#claimStatusActionsClaims_' + claimStatusActions.claimItemIDs[a] + ' td.statuses').html($(claimStatusActions.messages.spinnerHtml));
                    $('#claimStatusActionsClaims_' + claimStatusActions.claimItemIDs[a]).addClass('currentlySending');
                } else {
                    extraIDs[extraIDs.length] = claimStatusActions.claimItemIDs[a];
                }
            }

            // console.log(sendIDs);
            // return false;
            claimStatusActions.claimItemIDs = extraIDs;

            $.ajax({
                type: 'post',
                dataType: 'json',
                data: 'ids=' + sendIDs.join(','),
                url: claimStatusActions.actions.mark_paid,
                success: function (rs) {
                    for (var claimID in rs.claims) {
                        var message = '<ul>';


                        for (var claimItemID in rs.claims[claimID]) {
                            message += '<li>Claim Item ID ' + claimItemID + ': ' + rs.claims[claimID][claimItemID]['status'] + '</li>';
                        }
                        message += '</ul>';

                        $('#claimStatusActionsClaims_' + claimID + ' .message').html(message);
                        $('#claimStatusActionsClaims_' + claimID + ' .statuses').html(claimStatusActions.messages.checkmarkHtml);
                        $('#claimStatusActionsClaims_' + claimID).removeClass('currentlySending');

                        var myArray = {claimitemid: claimItemID, status: rs.claims[claimID][claimItemID]['status'],claim_status_code: rs.claims[claimID][claimItemID]['claim_status_code']};
                        

                        claimStatusActions.getStatus.push(myArray);
                        claimStatusActions.getClientId.push(claimID);
                        claimStatusActions.getClaimidSNew.push(claimItemID);
                        claimStatusActions.updateStatusRow = sendIDs.join(',');

                        claimStatusActions.markPaid.updateBaseTable(rs.claims[claimID]);
                    }

                    if (rs.hasOwnProperty('updatedItemsCount')) {
                        claimStatusActions.updatedItemsCount += rs['updatedItemsCount'];
                    }
                    if (rs.hasOwnProperty('totalNewPayments')) {
                        claimStatusActions.totalNewPayments += parseFloat(rs['totalNewPayments'], 10);
                    }

                    if (claimStatusActions.claimItemIDs.length) {
                        claimStatusActions.markPaid.sendAjax();
                    } else {
                        claimStatusActions.markPaid.complete();
                    }
                },
                beforeSend: function () {
                    quickClaim.ajaxLocked = true;
                    $('#claimStatusActionsMessages .icons div').hide();
                    $('#' + claimStatusActions.messages.spinner).show();
                    $('#' + claimStatusActions.messages.text).text('Marking Claim Items As Paid').show();
                },
                complete: function () {
                    quickClaim.ajaxLocked = false;
                    $('#' + claimStatusActions.messages.spinner).hide();
                },
                error: function () {
                    $('#' + claimStatusActions.messages.error).show();
                    $('#' + claimStatusActions.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            });
        },
        confirmationDialog: function () {
            if (claimStatusActions.showConfirmations) {
                $('#markPaidConfirmation').dialog('open');
            } else {
                claimStatusActions.markPaid.sendAjax();
            }
        },
        updateBaseTable: function (data) {
            if (claimStatusActions.baseClass == 'details') {
                details.triggerPageRefresh();
            }
            for (var claimItemID in data) {
                if (claimStatusActions.baseClass == 'pastClaims') {
                    var field = pastClaims.results.rowPrefix + '_' + claimItemID;
                    $('#' + field + ' td.claim_status').html('<b>' + data[claimItemID]['status'] + '</b>');
                    $('#' + field + ' td.fee_paid').html('<b>$' + data[claimItemID]['fee_paid'] + '</b>');

                    if (pastClaims.data.hasOwnProperty(claimItemID)) {
                        pastClaims.data[claimItemID]['edit'] = data[claimItemID]['actions'].hasOwnProperty('edit');
                        pastClaims.data[claimItemID]['resubmit'] = data[claimItemID]['actions'].hasOwnProperty('resubmit');
                        pastClaims.data[claimItemID]['submit'] = data[claimItemID]['actions'].hasOwnProperty('submit');
                        pastClaims.data[claimItemID]['mark_closed'] = data[claimItemID]['actions'].hasOwnProperty('mark_closed');
                        pastClaims.data[claimItemID]['mark_paid'] = data[claimItemID]['actions'].hasOwnProperty('mark_paid');
                    }
                }
            }
        }
    },
    resubmit: {
        complete: function () {
            $('.updatedItemsCount').text(claimStatusActions.updatedItemsCount);
            $('#resubmitCompleteDialog').dialog('open');
        },
        openDialog: function (claimItemIDs) {
            claimStatusActions.updateTitle('Mark Claim Items As Unsubmitted');

            if (!claimItemIDs.length) {
                quickClaim.showOverlayMessage('No selected Claim Items can be Marked As Unsubmitted at this time.');
                setTimeout(function () {
                    quickClaim.hideOverlayMessage();
                }, 2000);
                return;
            }
            claimStatusActions.buildTable(claimItemIDs);
            claimStatusActions.updatedItemsCount = 0;
            claimStatusActions.resubmit.confirmationDialog();
        },
        sendAjax: function () {
     
            var sendIDs = new Array();
            var extraIDs = new Array();

            for (var a = 0; a < claimStatusActions.claimIDs.length; a++) {
                if (a < 500) {
                    sendIDs[a] = claimStatusActions.claimIDs[a];
                    $('#claimStatusActionsClaims_' + claimStatusActions.claimIDs[a] + ' td.message').text('Marking Claim As Unsubmitted');
                    $('#claimStatusActionsClaims_' + claimStatusActions.claimIDs[a] + ' td.statuses').html($(claimStatusActions.messages.spinnerHtml));
                    $('#claimStatusActionsClaims_' + claimStatusActions.claimIDs[a]).addClass('currentlySending');
                } else {
                    extraIDs[extraIDs.length] = claimStatusActions.claimIDs[a];
                }
            }
            claimStatusActions.claimIDs = extraIDs;
            $.ajax({
                type: 'post',
                dataType: 'json',
                data: 'ids=' + sendIDs.join(','),
                url: claimStatusActions.actions.resubmit,
                success: function (rs) {

                    for (var claimID in rs.claims) {
                        var message = '<ul>';

                        for (var claimItemID in rs.claims[claimID]) {
                            message += '<li>Claim Item ID ' + claimItemID + ': ' + rs.claims[claimID][claimItemID]['status'] + '</li>';
                        }
                        message += '</ul>';
                        $('#claimStatusActionsClaims_' + claimID + ' .message').html(message);
                        $('#claimStatusActionsClaims_' + claimID + ' .statuses').html(claimStatusActions.messages.checkmarkHtml);
                        $('#claimStatusActionsClaims_' + claimID).removeClass('currentlySending');
                        //var claimStatusId = [];
                        //console.log(rs.claims[claimID][claimItemID]['status']);
                        var myArray = {claimitemid: claimID, status: rs.claims[claimID][claimItemID]['status'],claim_status_code: rs.claims[claimID][claimItemID]['claim_status_code']};
                       
                        //console.log(myArray);
                        claimStatusActions.getStatus.push(myArray);

                        //console.log(claimStatusActions.getStatus);

                        //console.log(claimStatusActions.getStatus);
                        claimStatusActions.getClientId.push(claimID);
                        claimStatusActions.getClaimidSNew.push(claimItemID);
                       

                        claimStatusActions.resubmit.updateBaseTable(rs.claims[claimID]);
                    }

                    claimStatusActions.updateStatusRow = sendIDs.join(',');
                      

                    if (rs.hasOwnProperty('updatedItemsCount')) {
                        claimStatusActions.updatedItemsCount += rs['updatedItemsCount'];
                    }

                    if (claimStatusActions.claimIDs.length) {
                        claimStatusActions.resubmit.sendAjax();
                    } else {
                        claimStatusActions.resubmit.complete();
                    }
                },
                beforeSend: function () {
                    quickClaim.ajaxLocked = true;
                    $('#claimStatusActionsMessages .icons div').hide();
                    $('#' + claimStatusActions.messages.spinner).show();
                    $('#' + claimStatusActions.messages.text).text('Marking Claim Items As Unsubmitted').show();
                },
                complete: function () {
                    quickClaim.ajaxLocked = false;
                    $('#' + claimStatusActions.messages.spinner).hide();
                },
                error: function () {
                    $('#' + claimStatusActions.messages.error).show();
                    $('#' + claimStatusActions.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            });
        },
        confirmationDialog: function () {
            if (claimStatusActions.showConfirmations) {
                $('#resubmitConfirmation').dialog('open');
            } else {
                claimStatusActions.resubmit.sendAjax();
            }
        },
        updateBaseTable: function (data) {
            for (var claimItemID in data) {
                if (claimStatusActions.baseClass == 'pastClaims') {
                    var field = pastClaims.results.rowPrefix + '_' + claimItemID + ' td.claim_status';
                    $('#' + field).html('<b>' + data[claimItemID]['status'] + '</b>');

                    if (pastClaims.data.hasOwnProperty(claimItemID)) {
                        pastClaims.data[claimItemID]['edit'] = data[claimItemID]['actions'].hasOwnProperty('edit');
                        pastClaims.data[claimItemID]['resubmit'] = data[claimItemID]['actions'].hasOwnProperty('resubmit');
                        pastClaims.data[claimItemID]['submit'] = data[claimItemID]['actions'].hasOwnProperty('submit');
                        pastClaims.data[claimItemID]['mark_closed'] = data[claimItemID]['actions'].hasOwnProperty('mark_closed');
                        pastClaims.data[claimItemID]['mark_paid'] = data[claimItemID]['actions'].hasOwnProperty('mark_paid');
                    }
                } else if (claimStatusActions.baseClass == 'details') {
                    details.triggerPageRefresh();
                }
            }
        }
    },

    buildTable: function (claimItemIDs) {
        claimStatusActions.claimIDs = new Array();
        claimStatusActions.claimItemIDs = new Array(); 
        var idx = 0;
        var acceptableClaimItemIDs = {};

        $('#claimStatusActionsTable tbody tr').remove();

        for (var a = 0; a < claimItemIDs.length; a++) {
            var claimID = claimStatusActions.getClaimId(claimItemIDs[a]);
            var claimItemID = claimItemIDs[a];

            if (!acceptableClaimItemIDs.hasOwnProperty(claimItemID)) {
                acceptableClaimItemIDs[claimItemID] = claimItemID;

                claimStatusActions.claimIDs[idx] = claimID;
                claimStatusActions.claimItemIDs[idx++] = claimItemIDs[a];

                var tr = '<tr id="claimStatusActionsClaims_' + claimItemIDs[a] + '">'
                        + '<td class="statuses">&nbsp;</td>'
                        + '<td class="claimId">' + claimID + '</td>'
                        + '<td class="claimItemID">' + claimItemIDs[a] + '</td>'
                        + '<td class="message">&nbsp;</td>'
                        + '</tr>';

                $('#claimStatusActionsTable tbody').append($(tr));
            }
        }
        $('#claimStatusActionsDialog').dialog('open');
        $('#claimStatusActionsTable').trigger('update');
    },
    getClaimId: function (claimItemID) {
        if (claimStatusActions.baseClass == 'pastClaims') {
            return pastClaims.getClaimId(claimItemID);
        } else if (claimStatusActions.baseClass == 'details') {
            return $('#claimID').text();
        }
    },
    updateTitle: function (message) {
        $('#claimStatusActionsDialog').dialog('option', 'title', message);
        $('#claimStatusActionsTitle').text(message);
    }
};