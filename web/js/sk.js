/* 
 * Author : Sunil Kashyap
 */

var sk = {
    hcmhidemsg: false,
    skdt_page_size: 10,
    print_font_size: 12,
    appt_book_sidemenu_position: 'min',
    NotesIndex : 0,
    checkDate : 1,
    duplicateqscArr : [],
    duplicatedscArr : [],
    duplicateapptypeArr : [],
    duplicatedscArr: [],
    duplicateqscArr: [],
    skNotify: function (msg, type) {
        $.notifyClose();
        $.notify(msg, {
            element: 'body',
            type: type,
            allow_dismiss: true,
            newest_on_top: true,
            placement: {
                from: "top",
                align: "right"
            },
            delay: 5000,
            z_index: 99999,
            mouse_over: 'pause',
            animate: {
                enter: 'animated bounceIn',
                exit: 'animated bounceOut'
            }, 
            template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                            <button type="button" onclick="closeNotify();" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                            <span data-notify="icon"></span>
                            <span data-notify="title">{1}</span>
                            <span data-notify="message">{2}</span>
                            <div class="progress" data-notify="progressbar">
                            <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                            </div>
                            <a href="{3}" target="{4}" data-notify="url"></a>
                        </div>`

        });
    },
    skAjaxParam: {
        url: '',
        type: 'POST',
        data: {}
    },
    setClientParam: function (name, value) {

        $.ajax({
            url: '/appointment_book/SaveClientParameters',
            type: 'POST',
            data: {name: name, value: value},
            success: function (e) {
                // console.log(e);
            }
        })
    },
    getClientParam: function (name, handler) {
        // sk.skAjaxParam.url = '/appointment_book/GetClientParameters';
        // sk.skAjaxParam.data = {name: name};
        // sk.skAjax(function (data) {
        //     handler(data);
        // })
    },
    getJsDate: function (date = null) {
        var newDate;
        switch (quickClaim.dateFormat) {
            case 'd/m/Y':
            case 'dd/mm/yy':
                var dateObj = date.split('/');
                newDate = dateObj[1] + '/' + dateObj[0] + '/' + dateObj[2];
                break;
            case 'Y-m-d':
                var dateObj = date.split('-');
                newDate = dateObj[1] + '/' + dateObj[2] + '/' + dateObj[0];
                break;
            default:
                newDate = date;
                break;
        }
        return new Date(newDate + ' EST');
    },
    datepicker: function (input, multi = false) {

        var startDate = '';
        var patient_dob_limit = $("#id_patient_dob_limit").val();
        if(patient_dob_limit != undefined && patient_dob_limit != '')
        {
            if(patient_dob_limit  == 1 && (input == '#patient_dob' || input == '#claim_dob'))
            {
              startDate  = '-141y';       
            }
        }
        $(input).skDP({
            todayBtn: 'linked',
            autoclose: !multi,
            todayHighlight: true,
            clearBtn: true,
            startDate: startDate,
            multidate: multi,
            format: global_js.getNewDateFormat(),
            assumeNearbyYear: true,
            changeMonth: true,  
            zIndexOffset: 999
        });
        $(input).on('change', function () {
           $(input).blur();
        })
        
        // $(input).on("blur",function () {
        //     //alert('blur');
        //     //event.preventDefault();
        //     // check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
        //     // update the format if it's yyyy-mm-dd
        //     var date = parseDate($(this).val());
        //     if (! isValidDate(date)) {
        //         //create date based on momentjs (we have that)
        //         date = moment().format(sk.getMomentDateFormat().toUpperCase());
        //     }
        //     //alert(i);
        //     if(date == 'empty')
        //     {
        //         date = '';
        //         sk.skNotify('Invalid date', 'danger');
        //     } 
        //     $(this).val(date);
        //     //sk.checkDate++;
        // });

        // $(input).keyup(function(e) {
        //     if(e.keyCode != 8) {
        //         var lengthcheck = $(this).val().length;
        //         if(lengthcheck >= 8)
        //         {
        //             var date = parseDate($(this).val());
        //             if (! isValidDate(date)) {
        //                 //create date based on momentjs (we have that)
        //                 date = moment().format(sk.getMomentDateFormat().toUpperCase());
        //             }
        //             //alert(i);
        //              if(date == 'Invalid date')
        //             {
        //                 date = '';
        //                 sk.skNotify('Invalid date', 'danger');
        //             }
        //             else
        //             {
        //                  $(this).val(date);
        //             }
        //         }
        //         var isValid = false;
        //         var regex = new RegExp("^[0-9?=.*!@#$%^&/*]+$");
        //         isValid = regex.test($(this).val());
        //         if(isValid == false)
        //         {
        //           sk.skNotify('Invalid date', 'danger');
        //           return false;
        //         }
        //     }
        // });

        // var isValidDate = function(value, format) {
        //         format = format || false;
        //         if (format) {
        //         value = parseDate( $(input));
        //         }
        //         var timestamp = Date.parse(value);
        //         return isNaN(timestamp) == false;
        // }
        // var parseDate = function(value) {
        //         var m = moment(value, sk.getMomentDateFormat().toUpperCase()).format(sk.getMomentDateFormat().toUpperCase());
        //         if (m)
        //         return m;
        // }
    },
    pdf: {
        tblRef: '.DataTableAppointmentSearch',
        fontSize: '12',
        firstCol: true,
        lastCol: false,
        orientation: 'portrait',
        headerDates: '',
        title: '',
        fileName: 'pdf',
        action_col_no: 0,
        result_count: 0,
        alreadySpliced:false,
        init: function (tblName) {
            sk.pdf.tblRef = tblName;
            var doc = new jsPDF(sk.pdf.orientation);
            var columns = sk.pdf.buildHeader();
            var data = sk.pdf.buildRows();

            var page = 1;
            var nId = $('th:contains("Notes")').index() - 1;
            var patintN = $('th:contains("Patient notes")').index() - 1;
            //alert(sk.NotesIndex);
            function footer() {
                //console.log(doc.autoTableState);
                if (sk.pdf.orientation == 'portrait') {
                    doc.text('Page ' + page + ' of ' + Math.ceil(sk.pdf.result_count / (37)), 180, 290);
                } else {
                    doc.text('Page ' + page + ' of ' + Math.ceil(sk.pdf.result_count / (24)), 270, 205);
                }
 
                page++;
            }
            var arr = [];
            arr[nId] = {columnWidth:50};
            arr[patintN] = {columnWidth:50};

            var width = doc.internal.pageSize.getWidth();
            var height = doc.internal.pageSize.getHeight();

            doc.autoTable(columns, data, {
                theme: 'grid',
//                styles: {fillColor: [100, 255, 255]},
                columnStyles: arr,
                margin: {top: (sk.pdf.orientation == 'portrait') ? 18 : 25},
                addPageContent: function (data) {
                    if (sk.pdf.orientation == 'portrait') {
                        doc.addImage(sk_const.logo, 'PNG', 150, 5, 50, 10);
                        doc.setFontSize(10);
                        doc.text(sk.pdf.headerDates, width-11, 5, null, null, 'right');
                        //doc.text(sk.pdf.headerDates, 250, 5);
                        footer();
                        doc.setFontSize(18);
                        doc.text(sk.pdf.title + " (" + sk.pdf.result_count + ")", 10, 15);

                        doc.setFontSize(8);
                        doc.text('Printed on:' + moment().format(sk.getMomentDatetimeFormat()), 5, width+80);
                    } else {
                        doc.addImage(sk_const.logo, 'PNG', 190, 5, 100, 18);
                        doc.setFontSize(12);
                        doc.text(sk.pdf.headerDates, 230, 5);
                        //console.log(doc.text(sk.pdf.headerDates, 250, 5));
                        footer();
                        doc.setFontSize(24);
                        doc.text(sk.pdf.title + " (" + sk.pdf.result_count + ")", 10, 20);
                        doc.setFontSize(8);
                        doc.text('Printed on:' + moment().format(sk.getMomentDatetimeFormat()), 5, 205);
                    }
                }
            })
            sk.pdf.fileName = sk.pdf.fileName.replace(/\''/g, '_').toLowerCase();
            doc.save(sk.pdf.fileName + '.pdf');
        },
        buildHeader: function () {
            var header = [];
            var col = 0;
            $(sk.pdf.tblRef).DataTable().columns().every(function () {
                var column = this;
                if ($(column.header())[0].innerText == 'Appointment actions') {
                    sk.pdf.action_col_no = col;
                    console.log('set action col - '+sk.pdf.action_col_no);
                } else {
                    header.push($(column.header())[0].innerText);
                }
                col++;
            });
            if (sk.pdf.firstCol)
                header.shift()
            if (sk.pdf.lastCol)
                header.splice(-1, 1)
            return header;
        },
        buildRows: function () {
            var rowsData = [];
            var rowsData1 = [];
           
            var actionColoumn = $('#appointment_search_report_columns_appointment_actions').prop('checked');
            var rowsData = [];

            var data =  $(sk.pdf.tblRef).DataTable().rows().data();
            var totalTd = $(sk.pdf.tblRef+' tbody td').length / $(sk.pdf.tblRef+' tbody tr').length;

            data.each(function (tr, row) {
            // $(sk.pdf.tblRef+' tbody tr').each(function(row, tr) {
            var count = totalTd;
            var singleRow = [];
            var statusThIndex = $('th:contains("Appointment actions")').index();
            //console.log($(tr));
            var valI = (actionColoumn == true) ? 1 : 0;
            //alert(valI);    
            for (var i=0;i<count;i++){
                //singleRow.push($(tr).find('td:eq('+i+')').text());
                singleRow.push(tr[i]);
                //console.log('allData',$(tr).find('td:eq('+i+')').text());
                // if(actionColoumn == true)
                // {
                //     if(i != 0 || i != statusThIndex) {
                //     //console.log($(tr).find('td:eq('+i+')'));
                //     singleRow.push($(tr).find('td:eq('+i+')').text());
                //     }
                // }
                // else
                // {
                //     singleRow.push($(tr).find('td:eq('+i+')').text());
                // }
            }
           // alert($(tr).find('td:eq(0)').text().splice(2, 1));
            rowsData.push(singleRow);
        })
        

            var allData = rowsData;
            for (var i in allData) {
                if (Array.isArray(allData[i])) {
                       //if(sk.pdf.alreadySpliced == false){
                       var actionColoumn = $('#appointment_search_report_columns_appointment_actions').prop('checked');
                       if(actionColoumn == true) {
                       allData[i].splice(sk.pdf.action_col_no, 1);
                       }
                       //if (sk.pdf.firstCol)
                       //allData[i].shift()
                       //if (sk.pdf.lastCol)
                       //allData[i].splice(-1, 1);
                       //sk.pdf.alreadySpliced = true;
                       //} 
                     
                    //if(allData[i][0] == '')
                    if(allData[i][0].includes("checkbox"))
                    {
                        rowsData1.push(allData[i].slice(1));
                    }  
                    else
                    {
                         rowsData1.push(allData[i]);
                    }
                }
            }
       
            rowsData1 = rowsData1.filter((res)=> {
                return res.length > 0;
            });
            sk.pdf.result_count = rowsData1.length;
            return rowsData1;
        }
    },
    exportToExcel: function(e, tableId){
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#'+tableId).html()));
        e.preventDefault();
    },
    table: {
        getSortData: function (tableId) {
            var colHeader = [];
            $("#" + tableId + " thead th").each(function () {
                if ($(this).attr('data-sksort')) {
                    switch ($(this).attr('data-sksort')) {
                        case 'date':
                            colHeader.push({sType: 'dateField', bSortable: true});
                            break;
                        case 'time':
                            colHeader.push({sType: 'timeField', bSortable: true});
                            break;
                        case 'datetime':
                            colHeader.push({sType: 'datetimeField', bSortable: true});
                            break;
                    }
                } else {
                    colHeader.push({});
                }
            })
            return colHeader;
        },
        init: function (tableId, mode = 'basic', options = {filterFirst: true, buttons: []}) {
           // $.fn.dataTable.moment('DD/MM/YYYY HH:mm');
            var showButtons = options.buttons.length > 0 ? 'B' : '';
            var aoColumns = this.getSortData(tableId);
            var order = [[0, "asc"]]
            if(tableId == 'patient_appointment_history_table'){ 
                order = [[0, "desc"]];
            }
            if(tableId == 'patient_notes_history_results_table'){
                order = [[1, "desc"]];
            }
            if(tableId == 'pdfReportTable'){
                order = [[1, "desc"]];
            }
            var table = $("#" + tableId).DataTable({
                "paging": true,
                "lengthMenu": [10, 25, 50, 100, 200, 250, 500],
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "order": order,
                "info": true,
                aoColumns: aoColumns,
                "autoWidth": false,
                "responsive": false,
                "keys": true,
                "dom": 'Bfrtip',
                "oLanguage": {
                    "sSearch": "",
                    "sLengthMenu": "Per page _MENU_",
                    "oPaginate": {
                        "sNext": "<i class='clip-chevron-right'></i>",
                        "sPrevious": "<i class='clip-chevron-left'></i>"
                    },
                    "sInfo": "_START_ to _END_ rows out of _TOTAL_",
                },
                "sDom": '<"wrapper"<"row"<"col-sm-12" <"col-sm-8 actionButtonsDiv' + tableId + '"> <"col-sm-4 printBtnDiv" ' + showButtons + 'l > <"col-sm-12" <"sk-progress" <"sk-blue indeterminate skl' + tableId + '">><"row well customWell" <"col-sm-4 customSearchInput' + tableId + '" f> <"col-sm-1 customSearchField' + tableId + '">  <"col-sm-7 CustomPagination' + tableId + '"ip> > > >  >rt<"clear">>',
                initComplete: function () {

                    $(".CustomPagination" + tableId).prepend('<div style="float:left"><button data-toggle="tooltip" title="Filters" type="button" class="btn btn-primary filterToggle' + tableId + '"><i class="fa fa-filter"></i></button></div>');

                    $("#" + tableId).find(".entireClickable").click(function () {
                        if ($(this).find('input').prop('checked')) {
                            $(this).find('input').prop('checked', false);
                            $(this).removeClass('highlight');
                        } else {
                            $(this).find('input').prop('checked', true);
                            $(this).addClass('highlight');
                        }
                    })

                    var inputHtml = '<div class="input-group">' +
                            '<input type="text" placeholder="Contains..." data-focus="true" class="form-control DatatableAllRounderSearch' + tableId + '" />' +
                            '<span class="input-group-addon cursorPointer btnClearSearchTxt' + tableId + '"> ' +
                            '<i class="clip-cancel-circle-2 "></i>' +
                            '</span>' +
                            '<span class="input-group-addon cursorPointer"> ' +
                            '<i class="clip-search-2"></i>' +
                            '</span>' +
                            '</div>';

                    $(".customSearchInput" + tableId).html(inputHtml);

                    var searchoptions = $("#" + tableId + " thead tr:eq(0) th");
                    var customfilterinputs = '<tr>';
                    for (var j = 0; j < searchoptions.length; j++) {
                        customfilterinputs += '<th></th>';
                    }
                    customfilterinputs += '</tr>';
                    $("#" + tableId + " thead").append(customfilterinputs);
                    var aa = 0;
                    this.api().columns().every(function () {
                        var column = this;
                        var columnText = $.trim($(column.header())[0].innerText);
                        
                        if ($(column.header())[0].cellIndex != 0 || options.filterFirst == true) {

                            if (($.trim($(column.header())[0].innerText) == 'Description' && tableId == 'explCodeTable')) {
                                $('<input type="text" placeholder="Search" class="form-control btn-squared" />')
                                        .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                        .on('keyup change', function () {
                                            if (column.search() !== this.value) {
                                                column
                                                        .search(this.value)
                                                        .draw();
                                            }
                                        });
                            } else {
                                var select = $('<select class="selectRsltTbl' + tableId + ' '+columnText.replace(/\s/g, '')+' "><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                        .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                        .on('change', function () {
                                            var val = $(this).val().trim();

                                            column.search(val, true, false).draw();
                                        });

                                column.data().unique().sort().each(function (d, j) {
                                 
                                    if (columnText == 'Permissions' && tableId == 'user_type_table') {
                                        var arr = d.trim();
                                        arr = arr.split(',');
                                        for (var i in arr) {
                                            select.append('<option value="' + arr[i] + '">' + arr[i] + '</option>');
                                        }
                                    } else {
                                        if(columnText == 'Doctor Profile' && tableId == 'qsc_table')
                                        {     

                                            column.data().unique().sort().each(function (d, j) {
                                                var arr = d.replace(/\<\/li>/g, '_end').replace(/<\/?[^>]+(>|$)/g, "").trim().replace(/(\r\n|\n|\r)/gm, "").replace(/\                    /g, "");
                                                arr = arr.substring(0, arr.length - 4).split('_end');
                //                                var arr = d.replace(/\<li>/g, '').replace(/\<ul>/g, '').replace(/\<\/ul>/g, '').trim();//.split('</li>');
                //                                arr = arr.substring(0, arr.length - 5).split('</li>');
                                                for (var i in arr) {
                                                    if(arr[i] != '') {
                                                       
                                                        if($.inArray(arr[i], sk.duplicateqscArr) != -1) {
                                                             sk.duplicateqscArr.push(arr[i]);
                                                        } else {
                                                            select.append('<option value="' + arr[i] + '">' + arr[i] + '</option>');
                                                            sk.duplicateqscArr.push(arr[i]);
                                                        }
                                                    }
                                                }

                                            });


                                           //select.append('<option value="' + d + '">' + d + '</option>');  
                                           // var ss = d.split(',  ');
                                           // for(i  in ss)  {   
                                           //      if($.inArray(ss[i], sk.duplicateqscArr) != -1) {
                                           //           sk.duplicateqscArr.push(ss[i]);
                                           //      } else {
                                           //          select.append('<option value="' + ss[i] + '">' + ss[i] + '</option>');
                                           //          sk.duplicateqscArr.push(ss[i]);
                                           //      }
                                           // } 
                                        } 
                                        else if(columnText == 'Doctor Profile' && tableId == 'dsc_table')
                                        {     
                                             column.data().unique().sort().each(function (d, j) {
                                                var arr = d.replace(/\<\/li>/g, '_end').replace(/<\/?[^>]+(>|$)/g, "").trim().replace(/(\r\n|\n|\r)/gm, "").replace(/\                    /g, "");
                                                arr = arr.substring(0, arr.length - 4).split('_end');
                //                                var arr = d.replace(/\<li>/g, '').replace(/\<ul>/g, '').replace(/\<\/ul>/g, '').trim();//.split('</li>');
                //                                arr = arr.substring(0, arr.length - 5).split('</li>');
                                                for (var i in arr) {
                                                    if(arr[i] != '') {

                                                        if($.inArray(arr[i], sk.duplicatedscArr) != -1) {
                                                             sk.duplicatedscArr.push(arr[i]);
                                                        } else {
                                                            select.append('<option value="' + arr[i] + '">' + arr[i] + '</option>');
                                                            sk.duplicatedscArr.push(arr[i]);
                                                        }

                                                        //select.append('<option value="' + arr[i] + '">' + arr[i] + '</option>');
                                                    }
                                                }

                                            });



                                           // var ss = d.split(',  ');
                                           // for(i  in ss)  {   
                                           //      if($.inArray(ss[i], sk.duplicatedscArr) != -1) {
                                           //           sk.duplicatedscArr.push(ss[i]);
                                           //      } else {
                                           //          select.append('<option value="' + ss[i] + '">' + ss[i] + '</option>');
                                           //          sk.duplicatedscArr.push(ss[i]);
                                           //      }
                                           // } 
                                        } 
                                        else if(columnText == 'Doctor Profile' && tableId == 'appointment_type_table')
                                        {       
                                           var ss = d.split(',  ');
                                           for(i  in ss)  {   
                                                if($.inArray(ss[i], sk.duplicateapptypeArr) != -1) {
                                                     sk.duplicateapptypeArr.push(ss[i]);
                                                } else {
                                                    select.append('<option value="' + ss[i] + '">' + ss[i] + '</option>');
                                                    sk.duplicateapptypeArr.push(ss[i]);
                                                }
                                           } 
                                        } else {
                                             if(d != '') {
                                             select.append('<option value="' + d + '">' + d + '</option>');
                                             }
                                        }
                                        //select.append('<option value="' + d + '">' + d + '</option>');
                                    }

                                });
                            }
                        }
                        aa++;
                    });


                    $(".filterToggle" + tableId).click(function () {
                        $("#" + tableId + " thead tr:eq(1)").toggle();
                    });
                    $(".selectRsltTbl" + tableId).select2({
                        placeholder: "Search",
                        allowClear: true,
                        selectOnClose: true,
                        dropdownAutoWidth: true,
                        width: '98%'
                    });
                    $(".custActionBtns" + tableId).detach().appendTo(".actionButtonsDiv" + tableId);
                    $(".actionButtonsDiv" + tableId).attr('style', 'padding-left:0px');
                }
            });

            $(".DatatableAllRounderSearch" + tableId).keyup(function () {
                table.search($(this).val(), true).draw();
            });

            $(".btnClearSearchTxt" + tableId).click(function () {
                $(".DatatableAllRounderSearch" + tableId).val('');
                table.search('', true).draw();
            });
            setTimeout(function () {
                $(".dataTables_length select").select2('destroy');
                $("#" + tableId).find('.select2-arrow').hide();
                $("#" + tableId + " thead tr:eq(1)").toggle();
            }, 200);
            $("#tabs").on("tabsactivate", function (event, ui) {
                if ($("#" + tableId + " thead tr:eq(1)").is(":visible")) {
                    $("#" + tableId + " thead tr:eq(1)").toggle();
                }
            });

            $('div.dataTables_filter input').addClass('form-control');
            $('div.dataTables_filter input').attr('placeholder', 'Search');
            $(".DTsearchlabel").html('<i class="clip-search"></i>');
            $('.dataTables_filter').attr('style', 'width:100%');
            $('.dataTables_filter label').attr('style', 'width:100%');
            $('.dataTables_length select').attr('style', 'font-size: 12px;padding: 5px 3px;background: #fff;');
            $('div.dataTables_filter input').attr('style', 'float:right;width:100%');
            $(".dataTables_wrapper .dataTables_paginate .paginate_button").attr('style', 'background:#fff !important;color:#444444 !important; border-radius: 0;');

            table.on('responsive-display', function (e, datatable, row, showHide, update) {
                $('td ul').attr('style', 'width:100% !important');
                $('td ul').addClass('row');
                $('td ul li').addClass('col-sm-4');
                $(".dropdown-menu li").removeClass('col-sm-4');
            });

            this.initCustomSorting();
        },
        initCustomSorting: function () {
            $.fn.dataTableExt.oSort["dateField-desc"] = function (x, y) {
                var a = moment(x, sk.getMomentDateFormat().toUpperCase());
                var b = moment(y, sk.getMomentDateFormat().toUpperCase());
                return (a.diff(b));
            };

            $.fn.dataTableExt.oSort["dateField-asc"] = function (x, y) {
                var a = moment(x, sk.getMomentDateFormat().toUpperCase());
                var b = moment(y, sk.getMomentDateFormat().toUpperCase());
                return (b.diff(a));
            }

            $.fn.dataTableExt.oSort["datetimeField-desc"] = function (x, y) {
                var a = moment(x, sk.getMomentDatetimeFormat());
                var b = moment(y, sk.getMomentDatetimeFormat());
                return (a.diff(b));
            };

            $.fn.dataTableExt.oSort["datetimeField-asc"] = function (x, y) {
                var a = moment(x, sk.getMomentDatetimeFormat());
                var b = moment(y, sk.getMomentDatetimeFormat());
                return (b.diff(a));
            }
        },
        loader: function (tblid, status = 'stop') {
            if (status == 'start') {
                $(".skl" + tblid).addClass('indeterminate').removeClass('sk-progressfull');
            } else if (status == 'stop') {
                $(".skl" + tblid).addClass('sk-progressfull').removeClass('indeterminate');
        }
        }
    },
    getMomentDateFormat: function () {
        var newFormat = quickClaim.dateFormat;
        switch (quickClaim.dateFormat) {
            case 'd/m/Y':
                newFormat = 'dd/mm/yyyy';
                break;
            case 'dd/mm/yy':
                newFormat = 'dd/mm/yy';
                break;
            case 'm/d/Y':
                newFormat = 'mm/dd/yyyy';
                break;
            case 'Y-m-d':
                newFormat = 'yyyy-mm-dd';
                break;
        }
        return newFormat;
    },
    getMomentDatetimeFormat: function () {
        var newFormat = quickClaim.dateFormat;
        var time = quickClaim.timeFormat.replace('a', 'A').replace('ii', 'mm');
        switch (quickClaim.dateFormat) {
            case 'd/m/Y':
                newFormat = 'DD/MM/YYYY';
                break;
            case 'dd/mm/yy':
                newFormat = 'DD/MM/YY';
                break;
            case 'm/d/Y':
                newFormat = 'MM/DD/YYYY';
                break;
            case 'Y-m-d':
                newFormat = 'YYYY-MM-DD';
                break;
        }
        return newFormat + ' ' + time;
    },
    initRadioTabbled: function () {
        $.fn.radioTabbable = function () {
            var groups = [];

            $(this).each(function () {
                var el = this;
                var thisGroup = groups[el.name] = (groups[el.name] || []);
                thisGroup.push(el);
            });

            $(this).on('keydown', function (e) {
                setTimeout(function () {
                    var el = e.target;
                    var thisGroup = groups[el.name] = (groups[el.name] || []);
                    var indexOfTarget = thisGroup.indexOf(e.target);

                    if (e.keyCode === 9) {
                        if (indexOfTarget < (thisGroup.length - 1) && !(e.shiftKey)) {
                            thisGroup[indexOfTarget + 1].focus();
                        } else if (indexOfTarget > 0 && e.shiftKey) {
                            thisGroup[indexOfTarget - 1].focus();
                        }
                    }
                });
            });
        };
    },
    skAjax: function (handleData) {
        $.ajax({
            url: sk.skAjaxParam.url,
            type: sk.skAjaxParam.type,
            data: sk.skAjaxParam.data,
            success: function (e) {
                handleData(e);
            }
        });
    },
    skOloader: function (handleData) {
        if ($("#spinner").is(':visible')) {
            setTimeout(function () {
                sk.skOloader(handleData)
            }, 200);
        } else {
            handleData(true);
        }
    },
    initBreadcrumsCopy: function () {
        setTimeout(function () {
            $(".SkBreadCrumCopy").click(function () {
                var len = $(".breadcrumbul li").length;
                len--;
                len--;
                var i = 1;
                var cpyData = '';
                $(".breadcrumbul li").each(function () {
                    var text = $(this).text().trim();
                    cpyData += text;
                    if (len >= i) {
                        cpyData += ' \\ ';
                    }
                    i++;
                })
                new ClipboardJS('#copyTriggerBtnBreadcrum');
                $("#copyTextareaBreadCrum").val(cpyData);
                $("#copyTriggerBtnBreadcrum").trigger('click');
                alertify.set('notifier', 'position', 'top-right');
                msg = alertify.success("Breadcrumbs trail copied successfully!", 5);
            })
        }, 500);
    },
    MainMenu: {
        init: function () {
            setTimeout(function () {
                sk.getClientParam('main_menu_pos', function (res) {
                    sk.MainMenu.mainMenu(res);
                });
                if (sk.currentPage() == 'appointment_book') {
                    sk.getClientParam('appt_side_bar', function (res) {
                        sk.MainMenu.mainMenu(res);
                    });
                }
                sk.MainMenu.initButtons();
            }, 500);

        },
        mainMenu: function (status = 'max') {
            switch (status) {
                case 'max':
                    rightSingle();
                    break;
                case 'min':
                    leftSingle();
                    break;
                case 'maxall':
                    rightAll();
                    break;
                case 'minall':
                    leftAll();
                    break;
                case 'appt_min':
                    apptLeft();
                    break;
                case 'appt_max':
                    apptRight();
                    break;
            }
            function leftAll() {
                $('body').addClass('navigation-small1');
                $('body').addClass('navigation-small');
            }
            function rightAll() {
                $('body').removeClass('navigation-small1');
                $('body').removeClass('navigation-small');
            }
            function leftSingle() {
                $('body').addClass('navigation-small');
            }
            function rightSingle() {
                $('body').removeClass('navigation-small');
            }
            function apptLeft() {
                sk.setClientParam('appt_book_sidemenu_position', 'min');
                $('body').addClass('navigation-small1');
//                $("#new_toggler").hover(
//                        function () {
//                            $('body').removeClass("navigation-small1");
//                            if ($('#collapseTwo').css('height') != '0px') {
//                                $("#locationSrch").focus();
//                            }
//                        }, function () {
//                    $('body').addClass("navigation-small1");
//                })
            }
            function apptRight() {
                sk.setClientParam('appt_book_sidemenu_position', 'max');
                $('body').removeClass('navigation-small1');
                $("#new_toggler").unbind('hover');
        }
        },
        initButtons: function () {
            $('.navigation-toggler').bind('click', function () {
                if (!$('body').hasClass('navigation-small')) {
                    $('body').addClass('navigation-small');
                    sk.setClientParam('main_menu_pos', 'min');
                } else {
                    $('body').removeClass('navigation-small');
                    sk.setClientParam('main_menu_pos', 'max');
                }
            })
        }
    },
    currentPage: function () {
        var currentPage = window.location.href;
        currentPage = currentPage.replace('https://', '').replace('http://', '');
        currentPage = currentPage.split('/');
        currentPage = currentPage[1].split('?');
        return currentPage[0];
    },
    init: function () {
        if (!$.fn.skDP && $.fn.datepicker && $.fn.datepicker.noConflict) {
            var datepicker = $.fn.datepicker.noConflict();
            $.fn.skDP = datepicker;
        }
        this.initRadioTabbled();
        this.initBreadcrumsCopy();
        this.MainMenu.init();
    }
}
sk.init();
