var admin_form = {
	actions: {
		save_consult: null,
	},

	consult_length: null,
	ajax_lock: false,
	form_id: 'consultation_form',

	message: {
		checkmark_id: 'mod_checkmark',
		form_errors: 'mod_form_errors',
		error_id: 'mod_error_icon',
		spinner_id: 'mod_spinner',
		text_id: 'mod_message_text'
	},

	modal_id: 'consultation_modal',
	prefix: 'consultation',
	save_button: 'consultation_save',
	delete_button: 'consultation_delete',

	initialize: function() {
		$('#' + this.modal_id).dialog({
			width: 550,
			height: 500,
			autoOpen: false,
			modal: true,
            close: function() {
            	$('#calendar').weekCalendar('removeUnsavedEvents');
        	},
    		open: function() {

    		}
		});

		$('#' + this.prefix + '_start_time_hour').change(function () {
			admin_form.setEndTime();
		});

		$('#' + this.prefix + '_start_time_minute').change(function () {
			admin_form.setEndTime();
		});

		$('#' + this.save_button)
			.button()
			.click(function() {
				admin_form.handleConsultSave(false);
			});

		$('#' + this.delete_button)
		.button()
		.click(function() {
			admin_form.handleConsultSave(true);
		});
	},
	drawForm: function(e, calendar) {
		$('label.error').remove();
		$('.error').removeClass('error');
		$('#' + admin_form.message.form_errors).text('').hide();

		var start = e.start_time.split(':');
		var end = e.end_time.split(':');

		$('#' + this.prefix + '_id').val(e.id);
		$('#' + this.prefix + '_consultation_template_id').val(e.consultation_template_id);
		$('#' + this.prefix + '_date').val(e.start.toString('yyyy-MM-dd'));
		$('#' + this.prefix + '_location_id').val(e.location_id);
		$('#' + this.prefix + '_max_attendees').val(e.max_attendees);
		$('#' + this.prefix + '_start_time_hour').val(parseInt(start[0], 10));
		$('#' + this.prefix + '_start_time_minute').val(start[1]);
		$('#' + this.prefix + '_end_time_hour').val(parseInt(end[0], 10));
		$('#' + this.prefix + '_end_time_minute').val(end[1]);
		$('#' + this.prefix + '_notes').val(e.notes);

		if (end[1] == '00') {
			$('#' + this.prefix + '_end_time_minute').attr('selectedIndex', 1);
		}
		if (start[1] == '00') {
			$('#' + this.prefix + '_start_time_minute').attr('selectedIndex', 1);
		}

		if (e.id) {
			$('#' + this.delete_button).show();
		}
		else {
			$('#' + this.delete_button).hide();
		}

		$('#' + admin_form.message.checkmark_id).hide();
		$('#' + admin_form.message.error_id).hide();
		$('#' + admin_form.message.spinner_id).hide();
		$('#' + admin_form.message.text_id).text('').hide();

		$('#' + this.modal_id).dialog('open');
	},
	handleConsultSave: function(delete_record) {
		if (admin_form.ajax_lock) {
			return;
		}
		var data = $('#' + this.form_id).serialize();

		$('label.error').remove();
		$('.error').removeClass('error');
		$('#' + admin_form.message.form_errors).text('').hide();

		if (delete_record) {
			data += '&delete_record=true'
		}

		$.ajax({
			type: 'post',
			dataType: 'json',
			url: admin_form.actions.save_consult,
			data: data,
			success: function(rs) {
				if (rs.hasOwnProperty('errors')) {
					quickClaim.showFormErrors(rs.errors, '#' + admin_form.message.form_errors, '#' + admin_form.prefix, true);
					$('#' + admin_form.message.error_id).show();
					$('#' + admin_form.message.text_id).text('The form contains invalid data.');

				}
				else {
					$('#' + admin_form.message.checkmark_id).show();
					$('#' + admin_form.message.text_id).text('Successfully saved consultation.  Refreshing.');

					$('#' + admin_calendar.calendar).weekCalendar('refresh');

					var v = setTimeout("$('#' + admin_form.modal_id).dialog('close')",800);
				}
			},
			beforeSend: function() {
				admin_form.ajax_lock = true;
				$('#' + admin_form.message.checkmark_id).hide();
				$('#' + admin_form.message.error_id).hide();
				$('#' + admin_form.message.spinner_id).show();

				$('#' + admin_form.message.text_id).text('Saving Consultation.').show();
			},
			complete: function() {
				admin_form.ajax_lock = false;
				$('#' + admin_form.message.spinner_id).hide();
			},
			error: function() {
				$('#' + admin_form.message.error_id).show();
				$('#' + admin_form.message.text_id).text('An error occurred while contacting the server.');
			}
		});

	},
	setEndTime: function() {
		var start_hr =  $('#' + this.prefix + '_start_time_hour').val();
		var start_min = $('#' + this.prefix + '_start_time_minute').val();

		var hr = parseInt(start_hr, 10);
		var min = start_min ? parseInt(start_min, 10) : 0;

		min += parseInt(this.consult_length, 10);

		hr = hr + Math.floor(min / 60);
		min = min % 60;

		$('#' + this.prefix + '_end_time_hour').val(hr);
		$('#' + this.prefix + '_end_time_minute').val(min);
	}
}