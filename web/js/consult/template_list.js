var template_list = {
	actions: {
		post_schedule: null,
	},

	form_id: 'generate_schedule',
	generate_link_id: 'generate_button',
	generate_modal: 'generate_modal',
	ajax_lock: false,

	message: {
		checkmark_id: 'checkmark',
		form_errors: 'form_errors',
		error_id: 'error_icon',
		spinner_id: 'spinner',
		text_id: 'message_text'
	},

	prefix: 'schedule',
	submit_button: 'submit_button',

	initialize: function() {
		$('#' + this.generate_modal).dialog({
			autoOpen: false,
			height: 550,
			width:  425,
			modal: true,
			close: function() {
				$('#' + template_list.form_id).clearForm();
				template_list.oneOrAll('province', this);
				template_list.oneOrAll('region', this);
			},
			open: function() {
			}
		});

                sk.datepicker('#' + this.prefix + '_start_date');
                sk.datepicker('#' + template_list.prefix + '_end_date');
                var prefix = this.prefix;
		$('#' + this.prefix + '_start_date').on('changeDate', function(e){
                    $('#' + template_list.prefix + '_end_date').skDP('setStartDate', e.date);
                    $('#' + template_list.prefix + '_end_date').skDP('update', $(this).val());
                })
                
                $('#' + template_list.prefix + '_end_date').on('changeDate',function (e) {
                    $('#' + prefix + '_start_date').skDP('setEndDate', e.date);
                });


		$('td#regions input[type=checkbox]').click(function () {
			template_list.oneOrAll('region', this);
		});

		$('td#provinces input[type=checkbox]').click(function () {
			template_list.oneOrAll('province', this);
			template_list.updateRegionList();
		});

		$('#' + this.submit_button)
			.button()
			.click(function () {
				template_list.handleSubmitClick();
			});

		$('#' + this.generate_link_id)
			.button()
			.click(function() {
				template_list.openGenerateModal();
			});

		template_list.oneOrAll('province', this);
		template_list.oneOrAll('region', this);
	},
	addRegionClass: function(id, class_name) {
		form_id = this.prefix + '_regions_' + id;
		$('#' + form_id).parent().addClass(class_name);
	},
	handleSubmitClick: function() {
		if (template_list.ajax_lock) {
			return;
		}
		var data = $('#' + this.form_id).serialize();

		$('label.error').remove();
		$('.error').removeClass('error');
		$('#' + template_list.message.form_errors).text('').hide();

		$.ajax({
			type: 'post',
			dataType: 'json',
			url: template_list.actions.post_schedule,
			data: data,
			success: function(rs) {
				if (rs.hasOwnProperty('errors')) {
					quickClaim.showFormErrors(rs.errors, '#' + template_list.message.form_errors, '#' + template_list.prefix, true);

					$('#' + template_list.message.error_id).show();
					$('#' + template_list.message.text_id).text('The form contains invalid data.');
				}
				else {
					$('#' + template_list.message.checkmark_id).show();
					$('#' + template_list.message.text_id).text('Successfully generated consultations.');
				}
			},
			beforeSend: function() {
				template_list.ajax_lock = true;
				$('#' + template_list.message.checkmark_id).hide();
				$('#' + template_list.message.error_id).hide();
				$('#' + template_list.message.spinner_id).show();

				$('#' + template_list.message.text_id).text('Generating Consultation Schedule').show();
			},
			complete: function() {
				template_list.ajax_lock = false;
				$('#' + template_list.message.spinner_id).hide();
			},
			error: function() {
				$('#' + template_list.message.error_id).show();
				$('#' + template_list.message.text_id).text('An error occurred while contacting the server.');
			}
		});
	},
	oneOrAll: function(field_name, elem) {
		if (elem != null && $(elem).attr('id') == this.prefix + '_' + field_name + 's_all') {
			$('td#' + field_name + 's input[type=checkbox]').prop('checked', false);
			$('#' + this.prefix + '_' + field_name + 's_all').prop('checked', true);
		}
		else if ($('td#' + field_name + 's input[type=checkbox]:checked').size() == 0) {
			$('#' + this.prefix + '_' + field_name + 's_all').prop('checked', true);
		}
		else if ($('td#' + field_name + 's input[type=checkbox]:checked').size() > 1) {
			$('#' + this.prefix + '_' + field_name + 's_all').prop('checked', false);
		}
	},
	openGenerateModal: function() {
		$('#' + this.generate_modal).dialog('open');
	},
	uncheckHidden: function(prefix) {
		$('td#' + prefix + 's input[type=checkbox]:checked').each(function () {
			if ($(this).parent().css('display') == 'none') {
				$(this).prop('checked', false);
			}
		});
	},
	updateRegionList: function() {
		if ($('#provinces input[type=checkbox]:checked').size() == 1 && $('#' + template_list.prefix + '_provinces_all').prop('checked')) {
			$('#regions li').show();
		}
		else {
			$('#regions li').hide();

			$('#provinces input[type=checkbox]:checked').each(function () {
				id = $(this).attr('id').replace('schedule_provinces_', '');
				$('#regions li.province_' + id).show();
			});
		}

		template_list.uncheckHidden('region');
		$('#' + template_list.prefix + '_regions_all').parent().show();

		if ($('#regions input[type=checkbox]:checked').size() == 0) {
			$('#' + template_list.prefix + '_regions_all').prop('checked', true);
		}
	}
};
