var setup = {
    buttons: {
        appt_type_clear: null,
        appt_type_submit: null
    },
    use_complex_appointment_type: false,

    initialize: function () {
        setup.appointment_params.initialize();
        if (setup.use_complex_appointment_type) {
            setup.complex_appointment_type.initialize();
        } else {
            setup.appointment_type.initialize();
        }
        setup.consultation_params.initialize();
        setup.consultation_fee.initialize();
        setup.holiday.initialize();
        setup.stage_client.initialize();
        setup.appt_search.initialize();
        setup.generateEmpty.initialize();
    },
   
    appointment_type: {
        slot_type_normal: null,

        initialize: function () {
            $('#' + setup.buttons.appt_type_clear).click(function () {
                setup.appointment_type.clearForm();
            });
            quickClaim.addColorPicker('appointment_type_colour');

            // $('#appointment_type_colour').farbtastic(function(color) {
            //     console.log('The user has just selected the following color: ' + color);
            //     // setting input value
            // });
            // $('#appointment_type_colour').on('input',
            //     function()
            //     {
            //         console.log($(this).val());
            //     }
            // )
            
            $('#appointment_type_colour').blur(function () {
                setup.appointment_type.updateSampleColours($(this).css('background-color'));
            });
            setup.appointment_type.updateSampleColours($('#appointment_type_colour').val());

            quickClaim.addColorPicker('appointment_type_title_colour');
            $('#appointment_type_title_colour').blur(function () {
                setup.appointment_type.updateTitleColours($(this).css('background-color'));
            });
            setup.appointment_type.updateTitleColours($('#appointment_type_title_colour').val());

            $('#appointment_type_patient_required').closest('div').addClass('patient_required');

            $('#appointment_type_slot_type').change(function () {
                setup.appointment_type.updatePatientRequired($(this).val());
            });
            setup.appointment_type.updatePatientRequired($('#appointment_type_slot_type').val());
        },
        addRowClickHandler: function (row_id, at) {
            $('#appointment_type_' + row_id).on('click', function () {
                $('#stage_2_types').dialog({
                    closeOnEscape: true,
                    autoOpen: false,
                    modal: true,
                    width: '100%',
                    appendTo: "#appointment_type_dialog_parent_div",
                    close: function() {
                       $('#stage_2_types').dialog('destroy');
                       $('#stage_2_types, .validationmsg').hide();
                       $('#appt_type_table_block').show();
                    }
                });
                $('#stage_2_types').dialog('open');    
                $('#appointment_type_table').removeClass('current_edit');
                $('#appointment_type_' + row_id).addClass('current_edit');
                setup.appointment_type.fillInForm(at);
                setup.appointment_type.updatePatientRequired($('#appointment_type_slot_type').val());
            });
        },
        clearForm: function () {
            $('#stage_2_types').clearForm();

            $('#types .form_error_bottom_box').text('').hide();
            $('#types ul.error_list').remove();
            $('#types .error').removeClass('error');
            $('#appointment_type_table tr').removeClass('current_edit');

            $('#appointment_type_colour').val('#FFFFFF');
            $('#appointment_type_title_colour').val('#FFFFFF');
            $.farbtastic('#appointment_type_colour').setColor('#FFFFFF');
            $.farbtastic('#appointment_type_title_colour').setColor('#FFFFFF');
            setup.appointment_type.updateSampleColours($('#appointment_type_colour').val());
            setup.appointment_type.updateTitleColours($('#appointment_type_title_colour').val());
        },
        fillInForm: function (at) {

            $("#submit_claims_doctor_id option:selected").removeAttr("selected");
            $('#btnAllLeft').trigger('click');
            $('#stage_2_types fieldset').removeAttr('style');
            $('#stage_2_types #progressbar li').not(':first').removeClass('active');

            //console.log('at',at);
            setup.appointment_type.clearForm();
            //$("#lstBox1 option").prop("selected", false);
            $('#btnAllLeft').trigger('click');
            $("#lstBox1 option").prop("selected", false);
            for (var x in at) {
                if (x == 'active' || x == 'patient_required' || x == 'allow_double_book') {
                    $('#appointment_type_' + x).prop('checked', at[x]);
                } else {
                    if(x == 'doctor_ids')
                    {   
                        //multiple doctor id selected and move from left to right select box
                        var array = at[x].split(",");
                        for (var i in array){
                            $("#lstBox1 option[value="+array[i]+"]").prop("selected", true);
                            $('#btnRight').trigger('click');
                            $("#submit_claims_doctor_id option[value="+array[i]+"]").prop("selected", true);
                             //alert(array[i]);
                        }
                    } else {
                        $('#appointment_type_' + x).val(at[x]);   
                    }
                }
            }

            if ($('#appointment_type_colour').size()) {
                $.farbtastic('#appointment_type_colour').setColor($('#appointment_type_colour').val());
                $.farbtastic('#appointment_type_title_colour').setColor($('#appointment_type_title_colour').val());
            }

            setup.appointment_type.updateSampleColours($('#appointment_type_colour').val());
            setup.appointment_type.updateTitleColours($('#appointment_type_title_colour').val());
            // $('#appt_type_table_block, .validationmsg').hide();
            // $('#stage_2_types').show();
            quickClaim.focusTopField();
        },
        updatePatientRequired: function (slot_type) {
            if (slot_type == setup.appointment_type.slot_type_normal) {
                $('.patient_required').show();
            } else {
                $('.patient_required').hide();
            }
        },
        updateSampleColours: function (colour) {
            $('#appointment_type_sample .wc-cal-event, #appointment_type_sample_doctor .wc-cal-event').css('background-color', colour);
            $('#appointment_type_sample .wc-cal-event, #appointment_type_sample_doctor .wc-cal-event').css('color', $('#appointment_type_colour').css('color'));
            $('#appointment_type_colour, #appointment_type_sample_doctor').css('color', colour);
        },
        updateTitleColours: function (colour) {
            $('#appointment_type_sample .wc-time, #appointment_type_sample_doctor .wc-time').css('background-color', colour);
            $('#appointment_type_sample .wc-time, #appointment_type_sample_doctor .wc-time').css('color', $('#appointment_type_title_colour').css('color'));
            $('#appointment_type_sample .wc-time, #appointment_type_sample_doctor .wc-time').css('border', '1px solid ' + colour);
            $('#appointment_type_title_colour').css('color', colour);
        }
    },
    complex_appointment_type: {
        initialize: function () {
            $('#' + setup.buttons.appt_type_clear).click(function () {
                setup.complex_appointment_type.clearForm();
            });

            $('#add_row_button').button();
            $('#add_row_button').click(function () {
                setup.complex_appointment_type.addRow();
            });

            quickClaim.addColorPicker('appointment_type_colour');


            $('#appointment_type_colour').blur(function () {
                setup.appointment_type.updateSampleColours($(this).css('background-color'));
            });
            // setup.appointment_type.updateSampleColours($('#appointment_type_colour').val());

            quickClaim.addColorPicker('appointment_type_title_colour');
            $('#appointment_type_title_colour').blur(function () {
                setup.appointment_type.updateTitleColours($(this).css('background-color'));
            });
            setup.appointment_type.updateTitleColours($('#appointment_type_title_colour').val());

            $('#appointment_type_patient_required').parent('td').parent('div').addClass('patient_required');

            $('#appointment_type_slot_type').change(function () {
                setup.appointment_type.updatePatientRequired($(this).val());
            });
            setup.appointment_type.updatePatientRequired($('#appointment_type_slot_type').val());

            $('#appointment_province_table th label').each(function () {
                $(this).text($(this).text().replace(' 1', ''));
            });



        },
        addRowClickHandler: function (row_id, at) {
            $('#appointment_type_' + row_id).click(function () {
                $('#appointment_type_table').removeClass('current_edit');
                $('#appointment_type_' + row_id).addClass('current_edit');

                setup.complex_appointment_type.fillInForm(at);
                setup.appointment_type.updatePatientRequired($('#appointment_type_slot_type').val());
            });
        },
        fillInForm: function (at) {
            setup.complex_appointment_type.clearForm();
            for (var a in at.provinces) {
                if (a != 0) {
                    setup.complex_appointment_type.addRow();
                }
            }

            for (var x in at) {
                if (x == 'provinces') {

                    for (var a in at.provinces)
                    {
                        for (var b in at.provinces[a]) {
                            if (b == 'active' || b == 'require_actual_times' || b == 'allow_double_book') {
                                $('#appointment_type_' + a + '_' + b).prop('checked', at.provinces[a][b]);
                            } else {
                                $('#appointment_type_' + a + '_' + b).val(at.provinces[a][b]);
                            }
                        }

                    }

                } else if (x == 'active' || x == 'patient_required') {
                    $('#appointment_type_' + x).prop('checked', at[x]);
                } else {
                    $('#appointment_type_' + x).val(at[x]);
                }
            }

            quickClaim.focusTopField();
        },
        clearForm: function () {
            $('#stage_2_types').clearForm();

            $('#types .form_error_bottom_box').text('').hide();
            $('#types ul.error_list').remove();
            $('#types .error').removeClass('error');
            $('#appointment_type_table tr').removeClass('current_edit');

            $('.form_row').each(function () {
                var a = $(this).attr('id').replace('form_row_', '');

                if (a != '0') {
                    $(this).remove();
                    $('#appointment_type_' + a + '_id').remove();
                }
            });
        },
        addRow: function () {
            var new_row_id = $('.form_row').size();
            var new_row = $('#form_row_0').clone();

            $('#add_row_button', new_row).remove();
            $('.add_button_div', new_row).removeClass('add_button_div');
            $('.count', new_row).text(parseInt(new_row_id, 10) + 1);
            $('input,select', new_row).each(function () {
                var new_id = $(this).attr('id').replace('0', new_row_id);
                var new_name = $(this).attr('name').replace('0', new_row_id);

                if ($(this).attr('type') != 'checkbox') {
                    $(this).val('');
                }
                $(this).attr('id', new_id);
                $(this).attr('name', new_name);
                if ($(this).prop('checked')) {
                    $(this).prop('checked', false);
                }
            });

            var new_hidden_id = $('#appointment_type_0_id').clone();

            $(new_hidden_id).val('');
            $(new_hidden_id).attr('id', $(new_hidden_id).attr('id').replace('0', new_row_id));
            $(new_hidden_id).attr('name', $(new_hidden_id).attr('name').replace('0', new_row_id));

            $(new_row).attr('id', $(new_row).attr('id').replace('0', new_row_id));

            $('#appointment_province_table').append($(new_row));
            $('#appointment_type_0_id').parent('div').append($(new_hidden_id));
        }
    },
    consultation_fee: {
        initialize: function () {
            sk.datepicker('#consultation_fee_start_date');
            sk.datepicker('#consultation_fee_end_date');
            
            $('#consultation_fee_start_date').on('changeDate',function (e) {
                $('#consultation_fee_end_date').skDP('setStartDate', e.date);
            });

            $('#consultation_fee_end_date').on('changeDate',function (e) {
                $('#consultation_fee_start_date').skDP('setEndDate', e.date);
            });
            

            $('#consultation_fee_consultation_total').blur(function () {
                setup.consultation_fee.calculateTotals($(this).val());
            });

            $('#st4_clear').click(function () {
                $('#stage_4_fees').clearForm();

                $('.form_error_bottom_box').text('').hide();
                $('ul.error_list').remove();
                $('.error').removeClass('error');
                $('#fees_table tr').removeClass('current_edit');
            });

            $('.money').blur(function () {
                if ($(this).val()) {
                    $(this).val(number_format($(this).val(), 2));
                }
            });
        },

        addRowClickHandler: function (row_id, consultation_fee) {
            $('#consultation_fee_' + row_id).click(function () {
                $('#fees_table tr').removeClass('current_edit');
                $('#consultation_fee_' + row_id).addClass('current_edit');
                setup.consultation_fee.fillInForm(consultation_fee);
            });
        },
        calculateTotals: function (total) {
            total = parseFloat(total, 10);
            if (!isNaN(total)) {
                $('#consultation_fee_student_total').val(number_format(total * .75, 2));
                $('#consultation_fee_child_total').val(number_format(total * .5, 2));
            }
        },
        fillInForm: function (fee) {
            for (var x in fee) {
                $('#consultation_fee_' + x).val(fee[x]);
            }

            $('.form_error_bottom_box').text('').hide();
            $('ul.error_list').remove();
            $('.error').removeClass('error');

            quickClaim.focusTopField();
        }
    },
    consultation_params: {
        status_code_reasons: {},

        initialize: function () {
            $('#consultation_setup_consultation_appointment_title').change(function () {
                setup.consultation_params.updateSampleTitle($(this).val());
            });
            if ($('#consultation_setup_consultation_appointment_title').size()) {
                setup.consultation_params.updateSampleTitle($('#consultation_setup_consultation_appointment_title').val());
            }

            quickClaim.addColorPicker('consultation_setup_consultation_colour');
            $('#consultation_setup_consultation_colour').blur(function () {
                setup.consultation_params.updateColours($(this).val());
            });
            setup.consultation_params.updateColours($('#consultation_setup_consultation_colour').val());

            quickClaim.addColorPicker('consultation_setup_consultation_title_bar_colour');
            $('#consultation_setup_consultation_title_bar_colour').blur(function () {
                setup.consultation_params.updateTitleColours($(this).val());
            });
            setup.consultation_params.updateTitleColours($('#consultation_setup_consultation_title_bar_colour').val());

            $('#consultation_setup_consultation_no_show_patient_status').change(function () {
                setup.consultation_params.updateReasonCodes();
            });

            $('#consultation_setup_consultation_patient_status').change(function () {
                setup.consultation_params.updateReasonCodes();
            });

            $('#consultation_setup_consultation_cancel_patient_status').change(function () {
                setup.consultation_params.updateReasonCodes();
            });
        },

        addStatusReasonCode: function (status_id, reason_code_id) {
            if (!this.status_code_reasons[status_id]) {
                this.status_code_reasons[status_id] = [];
            }
            var length = this.status_code_reasons[status_id].length;
            this.status_code_reasons[status_id][length] = reason_code_id;
        },
        updateSampleTitle: function (title_string) {
            title_string = title_string.replace('%patient_count%', '3');
            title_string = title_string.replace('%max_patients%', '5');
            title_string = title_string.replace('%patient_list%', 'PATIENT A; PATIENT B; PATIENT C');
            $('#consultation_sample .wc-cal-event .wc-title').html(title_string);
        },
        updateReasonCodes: function () {
            var no_show_status_id = $('#consultation_setup_consultation_no_show_patient_status').val();
            var no_show_reason_id = $('#consultation_setup_consultation_no_show_reason_code').val();

            var default_status_id = $('#consultation_setup_consultation_patient_status').val();
            var default_reason_id = $('#consultation_setup_consultation_patient_reason_code').val();

            var cancel_status_id = $('#consultation_setup_consultation_cancel_patient_status').val();
            var cancel_reason_id = $('#consultation_setup_consultation_cancel_reason_code').val();

            var reset_no_show = true;
            var reset_cancel = true;
            var reset_default = true;

            $('#consultation_setup_consultation_no_show_reason_code option').each(function () {
                if ($(this).val()) {
                    $(this).attr('disabled', true);
                }
            });

            $('#consultation_setup_consultation_cancel_reason_code option').each(function () {
                if ($(this).val()) {
                    $(this).attr('disabled', true);
                }
            });

            $('#consultation_setup_consultation_patient_reason_code option').each(function () {
                if ($(this).val()) {
                    $(this).attr('disabled', true);
                }
            });

            if (this.status_code_reasons[no_show_status_id]) {
                var codes = this.status_code_reasons[no_show_status_id];
                for (var a in codes) {
                    $('#consultation_setup_consultation_no_show_reason_code option[value="' + codes[a] + '"]').attr('disabled', false);
                    if (codes[a] == no_show_reason_id) {
                        reset_no_show = false;
                    }
                }
            }
            if (this.status_code_reasons[cancel_status_id]) {
                var codes = this.status_code_reasons[cancel_status_id];
                for (var a in codes) {
                    $('#consultation_setup_consultation_cancel_reason_code option[value="' + codes[a] + '"]').attr('disabled', false);
                    if (codes[a] == cancel_reason_id) {
                        reset_cancel = false;
                    }
                }
            }
            if (this.status_code_reasons[default_status_id]) {
                var codes = this.status_code_reasons[default_status_id];
                for (var a in codes) {
                    $('#consultation_setup_consultation_patient_reason_code option[value="' + codes[a] + '"]').attr('disabled', false);
                    if (codes[a] == default_reason_id) {
                        reset_default = false;
                    }
                }
            }

            if (reset_no_show) {
                $('#consultation_setup_consultation_no_show_reason_code').val('');
            }
            if (reset_cancel) {
                $('#consultation_setup_consultation_cancel_reason_code').val('');
            }
            if (reset_default) {
                $('#consultation_setup_consultation_patient_reason_code').val('');
            }
        },
        updateColours: function (colour) {
            $('#consultation_sample .wc-cal-event').css('background-color', colour);
            $('#consultation_sample .wc-cal-event').css('color', $('#consultation_setup_consultation_colour').css('color'));
        },
        updateTitleColours: function (colour) {
            $('#consultation_sample .wc-time').css('background-color', colour);
            $('#consultation_sample .wc-time').css('color', $('#consultation_setup_consultation_title_bar_colour').css('color'));
            $('#consultation_sample .wc-time').css('border', '1px solid ' + colour);
        }
    },
    appointment_params: {
        initialize: function () {
            $('#appointment_setup_appointment_title_format').change(function () {
                setup.appointment_params.updateSampleTitle($(this).val());
            });
            if ($('#appointment_setup_appointment_title_format').size()) {
                setup.appointment_params.updateSampleTitle($('#appointment_setup_appointment_title_format').val());
            }

            $('#appointment_setup_appointment_show_end_times').closest('div').addClass('show_end_times');
            $('#appointment_setup_appointment_hide_start_times').click(function () {
                setup.appointment_params.toggleAppointmentBookTimes($(this).prop('checked'));
            });
            setup.appointment_params.toggleAppointmentBookTimes($('#appointment_setup_appointment_hide_start_times').prop('checked'));
        },

        toggleAppointmentBookTimes: function (checked) {
            if (checked) {
                $('.show_end_times').hide();
                $('#appointment_setup_appointment_show_end_times').prop('checked', false);
            } else {
                $('.show_end_times').show();
            }
        },
        updateSampleTitle: function (title_string) {
            title_string = title_string.replace('%appointment_type%', 'CHECKUP');
            title_string = title_string.replace('%patient_name%', 'JACOB SMITH');
            title_string = title_string.replace('%appointment_id%', '13324');
            title_string = title_string.replace('%patient_phone%', '(555) 444-3333');
            title_string = title_string.replace('%patient_age%', '28');
            title_string = title_string.replace('%patient_sex%', 'F');
            title_string = title_string.replace('%patient_dob%', '1983/04/23');
            title_string = title_string.replace('%patient_id%', '12212');
            title_string = title_string.replace('%current_stage%', 'BOOKED');
            title_string = title_string.replace('%status%', 'BILLED');
            title_string = title_string.replace('%patient_fname%', 'JACOB');
            title_string = title_string.replace('%patient_lname%', 'SMITH');
            title_string = title_string.replace('%notes%', 'Needs vacc **** Cancelled appt 1/2/2015');
            title_string = title_string.replace('%patient_notes%', 'Needs vacc *');
            title_string = title_string.replace('%chart_number%', '13324');

            $('#appointment_param_sample .wc-cal-event .wc-title').html(title_string);
        }
    },
    holiday: {
        initialize: function () {
            $('#holiday_button_clear').click(function () {
                setup.holiday.clearForm();
            });

            sk.datepicker('#holiday_date');
            sk.datepicker('#holiday_consult_date');
            
            $('#holiday_date').on('changeDate', function(){
                $('#holiday_consult_date').skDP('update', $(this).val());
            });
        },
        clearForm: function () {
            $('#holiday_table tr.current_edit').removeClass('current_edit');
            $('#holiday_form').clearForm();

            $('#holidays .form_error_bottom_box').text('').hide();
            $('#holidays .error_list').remove();
            $('#holidays .error').removeClass('error');

            $('#holidays .form_error_bottom_box').text('').hide();
        },
        fillInForm: function (data) {
            this.clearForm();

            var prefix = '#holiday_';

            for (var key in data) {
                if ($(prefix + key)) {
                    if ($(prefix + key).attr('type') == 'checkbox') {
                        $(prefix + key).prop('checked', data[key]);
                    } else {
                        $(prefix + key).val(data[key]);
                    }
                }
            }
            quickClaim.focusTopField();
        },
        addRowClickHandler: function (row_id, holiday) {
            $('#holiday_' + row_id).click(function () {

                setup.holiday.fillInForm(holiday);
                $('#holiday_' + row_id).addClass('current_edit');
            });
        }
    },
    stage_client: {
        initialize: function () {
            $('#stage_client_button_clear').click(function () {
                setup.stage_client.clearForm();
            });
        },
        addRowClickHandler: function (row_id, stage_client) {
            $('#stage_client_' + row_id).click(function () {

                setup.stage_client.fillInForm(stage_client);
                $('#stage_client_' + row_id).addClass('current_edit');
            });
        },
        clearForm: function () {
            $('#stage_client_form input[type=checkbox]').attr('disabled', false);
            $('#stage_client_table tr.current_edit').removeClass('current_edit');
            $('#stage_client_form').clearForm();

            $('#stages .form_error_bottom_box').text('').hide();
            $('#stages .error_list').remove();
            $('#stages .error').removeClass('error');

            $('#stages .form_error_bottom_box').text('').hide();
        },
        fillInForm: function (data) {
            this.clearForm();

            var prefix = '#stage_client_';
            for (var key in data) {
                if ($(prefix + key)) {

                    if (key == 'stage_following') {
                        if (data['id']) {
                            $(prefix + key + '_' + data['id']).attr('disabled', 'disabled');
                        }
                        for (var a in data[key]) {
                            $(prefix + key + '_' + a).prop('checked', 'checked');
                        }
                    } else if ($(prefix + key).attr('type') == 'checkbox') {
                        $(prefix + key).prop('checked', data[key]);
                    }  else if (prefix + key == '#stage_client_appointment_status' || prefix + key == '#stage_client_go_to_appointment_status') {
                         $(prefix + key).val(data[key]).trigger('change.select2');
                    } else {
                        $(prefix + key).val(data[key]);
                    }
                }
            }

            quickClaim.focusTopField();
        }
    },
    appt_search: {
        initialize: function () {
            quickClaim.setupToggleAll('appointment_search_default_statuses');
            quickClaim.setupToggleAll('default_claim_status');
            $('#appointment_search_setup_appointment_search_report_header').closest('div').before('<tr><td colspan="2">&nbsp;</td></tr>');
            $('#appointment_search_setup_appointment_search_date_end').closest('div').after('<tr><td colspan="2">&nbsp;</td></tr>');
        }
    },
    generateEmpty: {
        initialize: function () {
            $('#generate_blanks_generate_blank_appointment_type_id').closest('div').addClass('secondary');
            $('#generate_blanks_generate_blank_fname').closest('div').addClass('secondary');
            $('#generate_blanks_generate_blank_lname').closest('div').addClass('secondary');

            var tr = '<tr class="secondary"><td colspan="2">&nbsp;</td></tr>';

            $('#generate_blanks_generate_blank_appointment_type_id').closest('div').before($(tr));
            $('#generate_blanks_generate_blank_fname').closest('div').before($(tr));

            $('#generate_blanks_generate_blank_appointments').click(function () {
                setup.generateEmpty.toggleSecondaryFormItems();
            });
            setup.generateEmpty.toggleSecondaryFormItems();
        },
        toggleSecondaryFormItems: function () {
            if ($('#generate_blanks_generate_blank_appointments').prop('checked')) {
                $('#stage9GenerateEmptyAppointments tr.secondary').show();
            } else {
                $('#stage9GenerateEmptyAppointments tr.secondary').hide();
            }
        }
    }
};