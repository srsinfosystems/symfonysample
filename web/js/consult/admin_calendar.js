var admin_calendar = {
	actions: {
		pull_events: null
	},

	calendar: 'calendar',

	message: {
		checkmark_id: 'checkmark',
		error_id: 'error_icon',
		spinner_id: 'spinner',
		text_id: 'messages_text'
	},

	defaults: {
		draggable : function(calEvent, element) {
			return false;
		},
		resizable : function(calEvent, element) {
			return false;
		},

		defaultFreeBusy: {free: true},
        displayFreeBusys : false,
//		businessHours : {start: 6, end: 18, limitDisplay : true},
		daysToShow: 1,
		switchDisplay: {'1 day': '1', '3 days': '3', 'work week': '5', 'week': '7', 'month': '30'},

		headerSeparator: '  ',
		useShortDayNames: true,
		timeslotsPerHour: 4,
		timeslotHeight: 20,

        timeSeparator: '-',
        timeFormat: 'g:ia',
        alwaysShowEndTime: true,
        hideStartTime: false,
        timelineShowMinutes: true,

		allowCalEventOverlap : true,
		overlapEventsSeparate: true,
		defaultEventLength: 6,

		showAsSeparateUser: false,
		separateUsersDisplay: {'1': true, '3': true, '5': true, '7': true, '30': false},

		lastDate: $.cookie('current_date'),
		users: [],
		user_map: [],

		height: function($calendar) {
			return $(window).height() - $('#header').outerHeight(true) - $('#title_bar').outerHeight(true) - 4;
		},

		data: function(start, end, callback) {
			admin_calendar.pullConsultationData(start, end, callback);
		},

		eventClick : function(calEvent) {
	       admin_form.drawForm(calEvent, this.element);
		},

		eventNew : function(calEvent, $event, FreeBusyManager, calendar) {
			calEvent.location_id = admin_calendar.defaults.user_map[calEvent.userId];
			calEvent.start_time = calEvent.start.toString('H:mm');
			calEvent.end_time = calEvent.end.toString('H:mm');
			calEvent.notes = '';
			calEvent.max_attendees = 1;

			admin_form.drawForm(calEvent, calendar);
		},

		eventRender : function(calEvent, $event) {
			if(calEvent.end.getTime() < new Date().getTime()) {
				$event.css("backgroundColor", "#aaa");
				$event.find(".wc-time").css({backgroundColor: "#999", border:"1px solid #888"});
			}
		},

		getUserId : function(user, index, calendar) {
			if (!user) {
				return index;
			}

			for (var a in this.users) {
				if (user == this.users[a]) {
					return a;
				}
			}

			for (var a in admin_calendar.defaults.user_map) {
				if (user == admin_calendar.defaults.user_map[a]) {
					return a;
				}
			}

			return index;
		}
	},

	initialize: function() {

	},

	setViewList: function(views) {
		var rs = {};

		for (var a in admin_calendar.defaults.switchDisplay) {
			if (jQuery.inArray(admin_calendar.defaults.switchDisplay[a], views) != -1) {
				rs[a] = admin_calendar.defaults.switchDisplay[a];
			}
		}

		admin_calendar.defaults.switchDisplay = rs;
	},

	pullConsultationData: function(s, e, c) {
		var activeLocations = admin_sidebar.getActiveLocations();
		activeLocations = activeLocations.join(',');

		var params = 'start=' + s.toString('yyyy-MM-dd')
			+ '&end=' + e.toString('yyyy-MM-dd')
			+ '&active_locations=' + activeLocations
			+ '&days_to_show=' + $('#calendar').weekCalendar('option', 'daysToShow')
			+ '&timeslots_per_hour=' + $('#calendar').weekCalendar('option', 'timeslotsPerHour')
			+ '&minical_date=' + admin_sidebar.getMiniCalDate();

 		$.ajax({
			type: 'post',
			dataType: 'json',
			url: admin_calendar.actions.pull_events,
			data: params,
			success: function(rs) {
				if (rs.hasOwnProperty('users')) {
					users = new Array();
					user_map = new Array();
					count = 0;
					for (var i in rs.users) {
						users[count] = rs.users[i];
						user_map[count] = i;
						count++;
					}

					admin_calendar.defaults.users = users;
					admin_calendar.defaults.user_map = user_map;
					$('#calendar').weekCalendar('option', 'users', users);
				}

				e = rs.data.events ? rs.data.events : [];

				for (var a in e) {
					rs.data.events[a].start = Date.parse(e[a].start);
					rs.data.events[a].end = Date.parse(e[a].end);
					rs.data.events[a].userId = admin_calendar.defaults.getUserId(e[a].userId);
				}

				rs.data.freeebusys = [];
				c(rs.data);

				admin_sidebar.minical_dates = rs.month_counts;
				$('#' + admin_sidebar.minical).datepick('option', 'onDate', adminSidebarUpdateMinical);
				$('#' + admin_calendar.message.text_id).hide();
			},
			beforeSend: function() {
				$('#' + admin_calendar.message.checkmark_id).hide();
				$('#' + admin_calendar.message.error_id).hide();
				$('#' + admin_calendar.message.spinner_id).show();

				$('#' + admin_calendar.message.text_id).text('Requesting Consultation Data').show();
			},
			complete: function() {
				$('#' + admin_calendar.message.spinner_id).hide();
			},
			error: function() {
				$('#' + admin_calendar.message.error_id).show();
				$('#' + admin_calendar.message.text_id).text('An error occurred while contacting the server.');
			}
		});
	}
}