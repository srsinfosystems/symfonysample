var admin_sidebar = {

	minical: 'minical',
	minical_dates: [],

	initialize: function() {
		$("#provinces input.province_select").click(function() {
			admin_sidebar.setActiveProvince($(this).val());
		});

		$('#provinces li .province_icon').click(function () {
			admin_sidebar.toggleProvince($(this));
		});

		$('#provinces input[name="region_select"]').click(function () {
			admin_sidebar.setActiveRegion($(this).val());
		});

		$('#provinces .region_icon').click(function () {
			admin_sidebar.toggleRegion($(this));
		});

		$('#provinces input[type=checkbox]').click(function () {
			admin_sidebar.setActiveLocation();
		});

		$('#regions_button')
			.button()
			.click(function() {
				$('#calendar').weekCalendar('refresh');
			});

		$('#' + this.minical).datepick({
			dateFormat: quickClaim.dateFormat,
			renderer: $.datepick.themeRollerRenderer,
			changeMonth: true,
			selectDefaultDate: true,
			onChangeMonthYear: function(year, month) {
				minical_date = year + '-' + month + '-01';

				$(this).datepick('setDate', Date.parse(minical_date));
                                console.log(Date.parse(minical_date));
                                $(".weekDatepick").datepicker('setDate', Date.parse(minical_date));
			},
			onSelect: function(dates) {
				$('#calendar').weekCalendar('gotoDate', dates[0]);
			},
			onDate: adminSidebarUpdateMinical
		});
	},
	getMiniCalDate: function() {
		var minical_date = $('#' + admin_sidebar.minical + ' .datepick-month-year').val();
		if (minical_date) {
			minical_date = minical_date.substring(minical_date.indexOf('/') + 1) + '-' + minical_date.substring(0, minical_date.indexOf('/')) + '-01';
		}
		else {
			minical_date = '';
		}

		return minical_date;
	},
	getActiveLocations: function() {
		var rs = Array();
		var a = 0;

		$('.location_select').each(function () {
			if (admin_sidebar.isActiveLocation($(this).val())) {
				rs[a++] = $(this).val();
			}
		});

		return rs;
	},
	isActiveLocation: function(e) {
		if ($('#location_select_' + e + ':checked').size()) {
			return true;
		}

		var region_id = $('#location_select_' + e).parent('li').parent('ul').attr('id').replace('locations_', '');
		if ($('#region_select_' + region_id + ':checked').size()) {
			return $('#locations_' + region_id + ' input:checked').size() == 0;
		}

		var prov = $('#location_select_' + e).parent('li').parent('ul').parent('li').attr('class').replace('province_', '');
		if ($('#province_select_' + prov + ':checked').size()) {
			return $('#regions_' + prov + ' input.region_select:checked').size() == 0;
		}

		return false;
	},
	setActiveLocation: function(location) {
	},
	setActiveProvince: function(prov) {
		if (prov) {
			var checked = $('#province_select_' + prov).prop('checked');
			$('ul#regions_' + prov + ' li input.region_select').each(function () {
				$(this).prop('checked', checked);

				admin_sidebar.setActiveRegion($(this).val());
			});
		}
	},
	setActiveRegion: function(region_id) {
		if (region_id) {
			var checked = $('#region_select_' + region_id).prop('checked');
			$('ul#locations_' + region_id + ' li input.location_select').each(function () {
				$(this).prop('checked', checked);
				admin_sidebar.setActiveLocation($(this).val());
			});
		}
	},
	setDefaultLocations: function(x) {
		if (!x) {
			return;
		}
		var locations = (x).split(',');

		for (var a in locations) {
			var loc_id = locations[a];

			if ($('#location_select_' + loc_id).size()) {
				var prov_code =  $('#location_select_' + loc_id).parent('li').parent('ul').parent('li').attr('class').replace('province_', '');
				var region_id = $('#location_select_' + loc_id).parent('li').parent('ul').attr('id').replace('locations_', '');

				$('#location_select_' + loc_id).prop('checked', true);
				admin_sidebar.toggleProvince($('.province_icon', $('#province_select_' + prov_code).parent()), 1);
				admin_sidebar.toggleRegion($('.region_icon', $('#region_select_' + region_id).parent()), 1);
			}
		}
	},
	toggleProvince: function(icon, open) {
		if ($(icon).hasClass('ui-icon-plusthick') || open == 1) {
			$(icon).removeClass('ui-icon-plusthick');
			$(icon).addClass('ui-icon-minusthick');
			$('ul.regions', $(icon).parent()).show();
		}
		else {
			$(icon).removeClass('ui-icon-minusthick');
			$(icon).addClass('ui-icon-plusthick');
			$('ul.regions', $(icon).parent()).hide();
		}
	},
	toggleRegion: function(icon, open) {
		if ($(icon).hasClass('ui-icon-plusthick') || open == 1) {
			$(icon).removeClass('ui-icon-plusthick');
			$(icon).addClass('ui-icon-minusthick');
			$('ul.locations', $(icon).parent()).show();
		}
		else {
			$(icon).removeClass('ui-icon-minusthick');
			$(icon).addClass('ui-icon-plusthick');
			$('ul.locations', $(icon).parent()).hide();
		}
	}
};

function adminSidebarUpdateMinical (date, inMonth) {
	if (inMonth) {
		if ($.inArray(date.toString('yyyy-MM-dd'), admin_sidebar.minical_dates) != -1) {
			return {dateClass: 'active_date'};
		}
		return {dateClass: 'inactive_date'};
	}
	return {};
}
