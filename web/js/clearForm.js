/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var clearForm = {
    trigger: 'btnClearForm',
    formalise: '',
    default_source: 'txtDefaultSource',
    formId: null,
    default: null,

    process: function (source, type, data, key) {
        if ($(source).data('datepicker')) {
            // check input is date or not
            if (data != '') {
                $(source).val(data).skDP('setDate', new Date(global_js.getFormattedDate(data)));
            }
        } else if ($(source).is(':radio') || $(source).is(':checkbox')) {

            if (data instanceof Array) {
                if ($.inArray('all', data) >= 0) {
                    $(source).prop('checked', false);
                    $(source + "[value='all']").prop('checked', true);
                } else {
                    $(source).each(function () {
                        if ($.inArray($(this).val(), data) >= 0) {
                            $(this).prop('checked', true).trigger('change');
                        } else {
                            $(this).prop('checked', false);
                        }
                    })
                }
            } else {
                $(source).each(function () {
                    $(this).val() == data ? $(this).prop('checked', true).trigger('change') : ''
                })
            }
        } else {
            if (data instanceof Array) {
                console.log('undefined element');
            } else {

                $(source).val(data);
            }
        }
    },
    clear: function () {
        $("#" + this.formId + ' input[type="text"]').val('');
        for (var i in this.default) {
            if ($("input[name='" + this.formalise + "[" + i + "]']").length > 0) {
                this.process("input[name='" + this.formalise + "[" + i + "]']", 'string', this.default[i], i);
            } else if ($("input[name='" + this.formalise + "[" + i + "][]']").length > 0) {
                this.process("input[name='" + this.formalise + "[" + i + "][]']", 'array', this.default[i], i)
            }
            /*
             if(this.default[i] instanceof Array){
             console.log('Array')
             console.log(this.default[i]);
             } else {
             console.log('String')
             console.log(this.default[i]);
             }*/

        }

        $('input:checkbox[name="appointment_search[province_code][]"]').prop( "checked", false );
        $('input:checkbox[name="appointment_search[province_code][]"][value="all"]').prop('checked', true);
        $('input:checkbox[name="appointment_search[province_code][]"]').trigger('change'); 
        $('input:checkbox[name="appointment_search[province_code][]"][value="all"]').trigger('click'); 
        $('input:checkbox[name="appointment_search[province_code][]"][value="all"]').prop('checked', true);

        $('input:checkbox[name="appointment_search[region_id][]"]').prop( "checked", false );
        $('input:checkbox[name="appointment_search[region_id][]"][value="all"]').prop('checked', true);
        $('input:checkbox[name="appointment_search[region_id][]"]').trigger('change'); 
        $('input:checkbox[name="appointment_search[region_id][]"][value="all"]').trigger('click'); 
        $('input:checkbox[name="appointment_search[region_id][]"][value="all"]').prop('checked', true);

        $('input:checkbox[name="appointment_search[location_id][]"]').prop( "checked", false );
        $('input:checkbox[name="appointment_search[location_id][]"][value="' + appt_search.location_id + '"]').prop('checked', true);
        $('input:checkbox[name="appointment_search[location_id][]"]').trigger('change'); 
        $('input:checkbox[name="appointment_search[location_id][]"][value="' + appt_search.location_id + '"]').trigger('click'); 
        $('input:checkbox[name="appointment_search[location_id][]"][value="' + appt_search.location_id + '"]').prop('checked', true);
    

    },
    init: function () {

        this.default = JSON.parse($("#" + this.default_source).val());

        $("#" + this.trigger).click(function () {
            clearForm.clear();
        })
    }

}

