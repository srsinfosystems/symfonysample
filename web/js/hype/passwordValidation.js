$(document).ready(function () {
    password_validation.init();
});

var password_validation = {
    init: function () {
        $('#password_new_password1').keyup(function () {
            var isValid = password_validation.check_password($('#password_new_password1').val());
            console.log(isValid);

            if (isValid) {
                $("#changepasswordbtn").attr('disabled', false);
            } else {
                $("#changepasswordbtn").attr('disabled', true);
            }
        });
        $('#profile_confirmation_password_1').keyup(function () {
            var isValid = password_validation.check_password($('#profile_confirmation_password_1').val());

        });
    },
    check_password: function (password) {
        var isValid = true;
        if (password.match(/([A-Z])/)) {
            $('#uppercase span').css('text-decoration', 'line-through');
            $("#uppercase i").show();
        } else {
            isValid = false;
            $("#uppercase i").hide();
            $('#uppercase span').css('text-decoration', 'none');
        }
        if (password.match(/([0-9])/)) {
            $('#number span').css('text-decoration', 'line-through');
            $("#number i").show();
        } else {
            isValid = false;
            $("#number i").hide();
            $('#number span').css('text-decoration', 'none');
        }
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) {
            $('#symbol span').css('text-decoration', 'line-through');
            $("#symbol i").show();
        } else {
            isValid = false;
            $("#symbol i").hide();
            $('#symbol span').css('text-decoration', 'none');
        }
        if (password.length > 7) {
            $('#length span').css('text-decoration', 'line-through');
            $("#length i").show();
        } else {
            isValid = false;
            $("#length i").hide();
            $('#length span').css('text-decoration', 'none');
        }




        var strength = 0;
        if (password.length > 7)
            strength++;
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) {
            strength += 1;
        }
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        password_validation.set_result(strength);

        return isValid;
    },
    set_result: function (strength) {
        if (strength < 2) {
            $('#password_new_password1').css('border', '1px solid #ff0000');
            $('#errors').html('Weak password');
            $('#save').prop('disabled', true);
        } else if (strength < 3) {
            $('#password_new_password1').css('border', '1px solid #AA9539');
            $('#errors').html('Medium password');
            $('#save').prop('disabled', false);
        } else if (strength == 4) {
            $('#password_new_password1').css('border', '1px solid #2E882E');
            $('#errors').html('Great password');
            $('#save').prop('disabled', false);
        }
    }
};