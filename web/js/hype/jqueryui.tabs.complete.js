(function() {

var ui_tabs_load = $.ui.tabs.prototype.load;
var ui_dialog_open = $.ui.dialog.prototype.open;

$.extend($.ui.tabs.prototype, {
    load: function() {
        var self = this;
        ui_tabs_load.apply(this, arguments);
        this.postShow(this, arguments);
    },

	postShow: function(ui, args) {
		if (this.options.postShow) {
			this.options.postShow(ui, args);
		}
	}
});

$.extend($.ui.dialog.prototype, {
    open: function() {
        var self = this;
        this.preOpen(this, arguments);
        ui_dialog_open.apply(this, arguments);
        this.postOpen(this, arguments);
    },

	preOpen: function(ui, args) {
		if (this.options.preOpen) {
			this.options.preOpen(ui, args);
		}
	},
	postOpen: function(ui, args) {
		if (this.options.postOpen) {
			this.options.postOpen(ui, args);
		}
	}
});

})(jQuery);

