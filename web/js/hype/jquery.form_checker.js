(function( $ ) {
	var methods = {
		init: function( options ) {
			defaults = {
				original_values: {},
				tabs_id: null,
				tab_index: null,
				modal_id: null,
				submit_clicked: false,
				error_message: 'You have unsaved form values',
				ignore_new_fields: false
			};

			return this.each(function() {
				var parameters = $.extend(defaults, options);
				var $this = $(this);
				data = $this.data('formCheck');
				parameters = $.extend(data, parameters);

				if (parameters.tabs_id != null) {
					$('#' + parameters.tabs_id).bind('tabsselect', function() {
						current_tab = $('#' + parameters.tabs_id).tabs('option', 'selected');
						if (current_tab == parameters.tab_index) {
							return $('#' + $this.attr('id')).formCheck('tabSelect');
						}
					});
				}

				if (parameters.modal_id != null) {
					$('#' + parameters.modal_id).bind('dialogbeforeclose', function(e, ui) {
						return $('#' + $this.attr('id')).formCheck('modalClose');
					});
				}

				if (!data) {
					$(this).data('formCheck', parameters);
				}

				$(this).formCheck('setupForm', true);

				$('#' + $this.attr('id')).submit(function() {
					parameters.submit_clicked = true;
				});

				$(window).bind('beforeunload', function() {
					if (parameters.modal_id) {
						if (parameters.modal_id == quickClaim.modals.currentModal()) {
							rs = $('#' + $this.attr('id')).formCheck('isChanged');
							if (rs != null) { return rs; }
						}
					}
					if (parameters.tabs_id) {
						current_tab = $('#' + parameters.tabs_id).tabs('option', 'selected');

						if (parameters.submit_clicked == false && current_tab == parameters.tab_index) {
							rs = $('#' + $this.attr('id')).formCheck('isChanged');
							if (rs != null) { return rs; }
						}
					}
					if (parameters.submit_clicked == false) {
						rs = $('#' + $this.attr('id')).formCheck('isChanged');
						if (rs != null) { return rs; }
					}

					parameters.submit_clicked = false;
				});
			});
		},

		isChanged: function() {
			var $this = $(this);
			var data = $this.data('formCheck');
			var form_id = $($this).attr('id');
			var changed_fields = '';

			$('#' + form_id + ' textarea').each(function() {
				key = $(this).attr('id') ? $(this).attr('id') : $(this).attr('name');
				val = $(this).val();

				if (data.original_values[key] != val) {
					if (val || !data.ignore_new_fields) {
						changed_fields += key + "\n";
					}
				}
			});

			$('#' + form_id + ' select').each(function() {
				key = $(this).attr('id') ? $(this).attr('id') : $(this).attr('name');
				val = $(this).val();

				if (data.original_values[key] != val) {
					if (val || !data.ignore_new_fields) {
						changed_fields += key + "\n";
					}
				}
			});

			$('#' + form_id + ' input').each(function() {
				switch ($(this).attr('type')) {
					case 'button':
						break;
					case 'radio':
					case 'checkbox':
						key = $(this).attr('id') ? $(this).attr('id') : $(this).attr('name');
						val = $(this).prop('checked');

						if (data.original_values[key] != val) {
							if (val || !data.ignore_new_fields) {
								changed_fields += key + "\n";
							}
						}
						break;
					case 'text':
					case 'password':
					case 'file':
					default:
						key = $(this).attr('id') ? $(this).attr('id') : $(this).attr('name');
						val = $(this).val();

						if (data.original_values[key] != val) {
							if (val || !data.ignore_new_fields) {
								changed_fields += key + "\n";
							}
						}
						break;
				}
			});

			for (var v in data.original_values) {
				if ($('#' + v).size() == 0) {
					changed_fields += v + "\n";
				}
			}

			if (changed_fields == '') {
				return null;
			}

			if (data.error_message) {
				return data.error_message + "\n\n*WARNING: Pressing 'OK' will NOT save your changes!";
			}

			return changed_fields + "\n\n*WARNING: Pressing 'OK' will NOT save your changes!";
		},

		setupForm: function (reset_fields) {
			return this.each(function() {
				var $this = $(this);
				var data = $this.data('formCheck');
				var form_id = $($this).attr('id');

				if (reset_fields) {
					if (data == undefined) {
						data = {};
					}
					data.original_values = {};
				}

				$('#' + form_id + ' textarea').each(function() {
					key = $(this).attr('id') ? $(this).attr('id') : $(this).attr('name');
					val = $(this).val();

					data.original_values[key] = val;
				});

				$('#' + form_id + ' select').each(function() {
					key = $(this).attr('id') ? $(this).attr('id') : $(this).attr('name');
					val = $(this).val();

					data.original_values[key] = val;
				});

				$('#' + form_id + ' input').each(function() {
					switch ($(this).attr('type')) {
						case 'radio':
						case 'checkbox':
							key = $(this).attr('id') ? $(this).attr('id') : $(this).attr('name');
							val = $(this).prop('checked');

							data.original_values[key] = val;
							break;
						case 'text':
						case 'password':
						case 'file':
						default:
							key = $(this).attr('id') ? $(this).attr('id') : $(this).attr('name');
							val = $(this).val();

							data.original_values[key] = val;
							break;
					}
				});

				$(this).data('formCheck', data);
			});
		},

		updateField: function (field_id) {
			var $this = $(this);
			var data = $this.data('formCheck');
			var form_id = $($this).attr('id');

			data.original_values[field_id] = $('#' + field_id).val();

			$(this).data('formCheck', data);
		},

		tabSelect: function () {
			changed = $(this).formCheck('isChanged');

			if (changed) {
				text = "Are you sure you want to navigate away from this tab?" + "\n\n"
					+ changed + "\n\n"
					+ "Press OK to continue to the next tab, or Cancel to stay on the current tab";

				return confirm(text);
			}

			return true;
		},

		modalClose: function() {
			changed = $(this).formCheck('isChanged');

			if (changed) {
				text = "Are you sure you want to close this dialog?" + "\n\n"
					+ changed + "\n\n"
					+ "Press OK to close the dialog, or Cancel to stay on the dialog";

				return confirm(text);
			}

			return true;
		}
	};

	$.fn.formCheck = function( method ) {
		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.formCheck' );
		}
	};

})( jQuery );
