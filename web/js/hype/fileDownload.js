var hypeFileDownload = {
	downloadTimer: null,
	attempts: 30,
	returnFunction: null,
	tokenName: 'token',

	setReturnFunction: function(x) {
		hypeFileDownload.returnFunction = x; 
	},
	getCookie: function(name) {
		var parts = document.cookie.split(name + '=');

		if (parts.length == 2) {
			return parts.pop().split(';').shift();
		}
	},

	expireCookie: function(cName) {
		document.cookie = encodeURIComponent(cName) + '=; expires=' + new Date(0).toUTCString();
	},

	setFormToken: function() {
		var downloadToken = new Date().getTime();
		if ($('#batchPDFDownloadToken').size()) {
			$("#batchPDFDownloadToken").value(downloadToken);
		}
		return downloadToken;
	},

	blockResubmit: function() {
		var downloadToken = hypeFileDownload.setFormToken();
		hypeFileDownload.attempts = 30;

		hypeFileDownload.downloadTimer = window.setInterval(function() {
			var token = hypeFileDownload.getCookie(hypeFileDownload.tokenName);

			if((token == downloadToken) || (hypeFileDownload.attempts <= 0)) {
				hypeFileDownload.unblockSubmit();
			}

			hypeFileDownload.attempts--;
		}, 1000);
		
		return downloadToken;
	},

	unblockSubmit: function() {
		window.clearInterval(hypeFileDownload.downloadTimer);
		hypeFileDownload.expireCookie(hypeFileDownload.tokenName);
		if (hypeFileDownload.returnFunction != null) {
			hypeFileDownload.returnFunction();
		}
	}
}