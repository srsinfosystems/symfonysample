/* 
 * Sunil Kashyap - 03-07-18
 */


var srchChkTag = {
    mainDiv: 'srchCols',
    selectId: 'searchSelectCols',
    select: null,
    form_name: 'appointment_search',

    initChkbox: function() {
        $("#"+srchChkTag.mainDiv+" input[type='checkbox']").click(function(){
            var value = $(this).val();
            var name = $(this).attr('name');

            if(value == 'all' && $(this).is(':checked')){
                $('input[name="'+name+'"]').prop('checked', false);
                $('input[name="'+name+'"][value="all"]').prop('checked', true);
            } else {
                $('input[name="'+name+'"][value="all"]').prop('checked', false);
            }
            if($('input[name="'+name+'"]:checked').length == 0) {
                $('input[name="'+name+'"][value="all"]').prop('checked', true);
            }
            srchChkTag.toggleSelect();
        })
        //check all accept all
        $("#"+srchChkTag.mainDiv+" input[type='checkbox']:checked").each(function(){
            // if($(this).val() != 'all'){
            //     $(this).prop('checked', true);
            // }
        })
        srchChkTag.toggleSelect();
    },
    initSelect: function () {
        
        srchChkTag.select = $("#" + srchChkTag.selectId).select2({
            multiple: true,
            placeholder: "Select search options...",
            data: srchChkTag.buildOptions(),
            width: '100%'
        })

        srchChkTag.select.on('change', function (e) {
            srchChkTag.onChangeSelect(e);
        })
        
        

    },
    buildOptions: function () {
        var data = [];
        var chkGrp = [];
        var childData = {};
         var i = 0;
        $("#" + srchChkTag.mainDiv + " li").each(function () {
            var grp = $(this).find('input').attr('name').replace(srchChkTag.form_name, '').replace(/\[/g, '').replace(/\]/g, '');
            if ($.inArray(grp, chkGrp) > -1) {
                if (grp in childData) {
                    childData[grp][i] = {
                        id: $(this).find('input').attr('id'),
                        text: $(this).find('label').text()
                    };
                    i++;
                } else {             
                    i = 0;
                    childData[grp] = [{
                        id: $(this).find('input').attr('id'),
                        text: $(this).find('label').text()
                    }];
                    i++;
                }
            } else {
                data.push({
                    id: grp,
                    text: $('label[for="' + $(this).find('input').attr('id').replace('_all', '') + '"]').text() + ' - All'
                })
            }
            chkGrp.push(grp);
        })
        
        for ( var i in data){
            if(data[i]['id'] in childData){
                data[i]['children'] = childData[data[i]['id']];
            }
        }
        
        return data;
    },
    onChangeSelect: function(e){
        if (e.added) {
            $("#"+e.added.id).prop('checked', true);
        } else if (e.removed) {
            $("#"+e.removed.id).prop('checked', false);
        }
    },
    toggleSelect: function(){
        var data = [];
        $("#"+srchChkTag.mainDiv+" input[type='checkbox']:checked").each(function(){
            if ($(this).val() == 'all') {
                data.push($(this).attr('name').replace(srchChkTag.form_name, '').replace(/\[/g, '').replace(/\]/g, ''));
            } else {
                data.push($(this).attr('id'));
            }
        })
        srchChkTag.select.select2('val', data);
    },
    wait: function (delay = 500, callback = 'init') {
        setTimeout(function () {
            srchChkTag.callback();
        }, delay)
    },
    init: function () {
        // wait till loader completes fetching data
        if ($("#spinner").is(':visible')) {
            srchChkTag.wait();
        } else {
            srchChkTag.initSelect();
            srchChkTag.initChkbox();
        }
    }
};