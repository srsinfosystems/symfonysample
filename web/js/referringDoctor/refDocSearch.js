var refDocSearch = {
    parentInterface: '',

    initialize: function () {
        $('#search_ref_doc').button();
        $('#search_ref_doc').click(function () {
            if (refDocSearch.parentInterface == 'patientEdit') {
                patient_edit.activeRefDocPrefix = 'ref_doc';
            }
            $('#referring_doctor_dialog').dialog('open');
        });

        $('#search_fam_doc').button();
        $('#search_fam_doc').click(function () {
            if (refDocSearch.parentInterface == 'patientEdit') {
                patient_edit.activeRefDocPrefix = 'fam_doc';
            }
            $('#referring_doctor_dialog').dialog('open');
        });

        $('#referring_doctor_dialog').dialog({
            autoOpen: false,
            height: $(window).height() - 300,
            modal: true,
            width: $(window).width() - 500,
            open: function () {
                if (!referringDoctorList.initialized) {
                    referringDoctorList.initializePager();
                }else{
                    $("#referringDoctorSearchButton").trigger('click');
                }
            }
        });
    }
};
