var referringDoctorUploadForm = {
    actions: {
        upload: null
    },
    messageID: 'referringDoctorUploadMessage',
    parent: 'referringDoctorList',

    initialize: function () {
        $('#uploadReferringDoctorFileButton')
            .button()
            .click(function () {
                $('#referringDoctorFileUploadDialog').dialog('open');
            });

        $('#referringDoctorUploadButton')
            .button()
            .click(function () {
                referringDoctorUploadForm.submitForm();
            });

        $('#referringDoctorUploadCloseDialogButton')
            .button()
            .click(function () {
                $('#referringDoctorFileUploadDialog').dialog('close');
            });

        $('#referringDoctorFileUploadDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
//            height: 400,
            modal: true,
            width: 800
        });
    },

    showDialog: function () {
        $('#referringDoctorFileUploadDialog').dialog('open');
    },

    submitForm: function () {
        var formData = new FormData($("#referringDoctorUploadForm")[0]);
        formData.append('parent', referringDoctorUploadForm.parent);

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: referringDoctorUploadForm.actions.upload,
            data: formData,

            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function (rs) {
                if (rs.hasOwnProperty('success')) {
                    $('#referringDoctorFileUploadDialog').dialog('close');
                    quickClaim.hideAjaxMessage(referringDoctorUploadForm.messageID);

                    if (referringDoctorUploadForm.parent == 'import') {
                        quickClaim.hideAjaxMessage(dataImport.messageID);
                    }
                }
                else if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#referringDoctorsUploadErrorDiv', '#referring_doctor_upload', true);
                    var message = 'The form contains invalid data.';

                    quickClaim.showAjaxMessage(referringDoctorUploadForm.messageID, 'error', message);
                    if (referringDoctorUploadForm.parent == 'import') {
                        quickClaim.showAjaxMessage(dataImport.messageID, 'error', message);
                    }
                }

                if (rs.hasOwnProperty('batchJobs') && referringDoctorUploadForm.parent == 'referringDoctorList') {
                    referringDoctorList.displayBatchJobDetails(rs.batchJobs);
                }

                if (rs.hasOwnProperty('importData') && explCodesUploadForm.parent == 'import') {
                    $('#tabs').tabs('option', 'active', 0);
                    for (var a in rs['importData']) {
                        dataImport.loadTableRow(rs['importData'][a]);
                    }
                }
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                var message = 'Uploading .XLS File';

                if (referringDoctorUploadForm.parent == 'referringDoctorList') {
                    $('#' + referringDoctorList.messages.spinner).show();
                    $('#' + referringDoctorList.messages.text).text(message).show();
                }
                else if (referringDoctorUploadForm.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'spin', message);
                }

                quickClaim.showAjaxMessage(referringDoctorUploadForm.messageID, 'spin', message);
                quickClaim.showOverlayMessage(message);
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();

                if (referringDoctorUploadForm.parent == 'referringDoctorList') {
                    $('#' + referringDoctorList.messages.spinner).hide();
                }
            },
            error: function () {
                quickClaim.ajaxLocked = false;
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(referringDoctorUploadForm.messageID, 'error', message);

                if (referringDoctorUploadForm.parent == 'referringDoctorList') {
                    $('#' + referringDoctorList.messages.error).show();
                    $('#' + referringDoctorList.messages.text).text(message).show();
                }
                else if (referringDoctorUploadForm.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'error', message);
                }
            }

        });
    }
};