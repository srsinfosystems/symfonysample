var referringDoctorForm = {
    actions: {
        deleteReferringDoctor: null,
        save: null,
        label: null
    },
    messages: {
        error: 'referringDoctorEditErrorIcon',
        spinner: 'referringDoctorEditSpinner',
        checkmark: 'referringDoctorEditCheckmark',
        text: 'referringDoctorEditMessageText',
        spinnerHtml: '',
        checkmarkHtml: '',
        errorHtml: ''
    },
    paginationSize: 1000,
    paginationSort: [],
    parentInterface: null,
    editTimestamp: null,

    printLabel: function () {
        var dataURL = 'type=refaddr&refid=' + $('#referring_doctor_id').val();

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: referringDoctorForm.actions.label,
            data: dataURL,
            success: function (rs) {
                if (rs.hasOwnProperty('error')) {
                    alert(rs['error']);
                    return;
                }

                labelXML = rs;

                if (labelXML) {
                    var label = dymo.label.framework.openLabelXml(labelXML['xml']);
                    var label_objects = label.getObjectNames();
                    var refdoc = labelXML['refdoc'];

                    for (obj in label_objects) {
                        if (referringDoctorForm.contains(label.getObjectNames(), label_objects[obj])) {
                            var ele = label_objects[obj];
                            var new_text = refdoc[ele];
                            if (new_text) {
                                label.setObjectText(label_objects[obj], new_text);
                            }
                        }
                    }

                    // slect printer to print on
                    // for simplicity sake just use the first LabelWriter printer
                    var printers = dymo.label.framework.getPrinters();
                    if (printers.length == 0)
                        throw "No DYMO printers are installed. Install DYMO printers.";

                    var printerName = "";
                    for (var i = 0; i < printers.length; ++i) {
                        var printer = printers[i];
                        if (printer.printerType == "LabelWriterPrinter") {
                            printerName = printer.name;
                            break;
                        }
                    }
                    if (printerName == "")
                        throw "No LabelWriter printers found. Install LabelWriter printer";

                    // finally print the label
                    label.print(printerName);
                }
            },
            error: function (rs) {
                alert('An error has occured: ' + rs);
            }
        });
    },
    contains: function (a, obj) {
        for (var i = 0; i < a.length; i++) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    },
    initialize: function () {
        
        $('#referringDoctorLabelButton').button();
        $('#referringDoctorLabelButton').click(function () {
            referringDoctorForm.printLabel();
        });

        $('#createReferringDoctorButton').button();
        $('#createReferringDoctorButton').click(function () {
             referringDoctorForm.edit({});
             $('#referring_doctor_salutation').val('Dr.');
        });

        $('#referringDoctorSaveButton').button();
        $('#referringDoctorSaveButton').click(function () {
            referringDoctorForm.saveReferringDoctor();
        });

        $('#referringDoctorCloseDialogButton').button();
        $('#referringDoctorCloseDialogButton').click(function () {
            $('#referringDoctorEditFormDialog').dialog('close');
        });

        $('#deleteReferringDoctorButton').button();
        $('#deleteReferringDoctorButton').click(function () {
            referringDoctorForm.deleteReferringDoctor();
        });

        $('#referringDoctorEditFormDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
//			height: 600,
            modal: true,
            width: 800,
            open: function () {
                var d = new Date();
                $("#referringDoctorEditForm .cliponeErrorInput").removeClass('cliponeErrorInput');
                referringDoctorForm.editTimestamp = d.getTime();
            },
            close: function () {
                $('#deleteReferringDoctorButton').show();
                $('#deleteReferringDoctorButton').button('enable');

                referringDoctorForm.clearForm();
                if (referringDoctorForm.parentInterface == 'patientEdit') {
                    patient_edit.fillInRefDoc({id: '', autocomplete: '', provider_num: ''});
                } else if (referringDoctorForm.parentInterface == 'claimEdit') {
                    claim.fillInRefDoc({id: '', autocomplete: '', provider_num: ''});
                }
            }
        });

        $('#referring_doctor_postal_code').blur(function () {
            quickClaim.formatPostal('#' + $(this).attr('id'));
        });
        $('#referring_doctor_phone').blur(function () {
            quickClaim.formatPhone('#' + $(this).attr('id'));
        });
        $('#referring_doctor_fax').blur(function () {
            quickClaim.formatPhone('#' + $(this).attr('id'));
        });

        $('#referring_doctor_fname').closest('tr').before('<tr><td colspan="2">&nbsp;</td></tr>');

        $('#referring_doctor_provider_num').keyup(function (e) {
            var key = e.key;

            if (key.length == 1) {
                if ($(this).val().length >= 6) {
                    $('#referring_doctor_lname').focus();
                }
            }
        });
    },

    edit: function (data) {
        referringDoctorForm.fillForm(data);
        $('#referringDoctorEditFormDialog').dialog('open');
    },
    deleteReferringDoctor: function () {
        if (quickClaim.ajaxLocked) {
            return;
        }
        if (referringDoctorForm.parentInterface != 'referringDoctorList' && !referringDoctorForm.actions.deleteReferringDoctor) {
            return;
        }

        referringDoctorForm.clearErrors();
        var data = 'id=' + $('#referringDoctorEditForm #referring_doctor_id').val();

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: referringDoctorForm.actions.deleteReferringDoctor,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('success')) {
                    $('#referringDoctorMessages .icon').hide();
                    $('#referringDoctorSpinner').show();
                    $('#referringDoctorMessageText').text('Deleted Referring Doctor');
                    $('#referringDoctorEditFormDialog').dialog('close');
                    $('#referringDoctorTable').trigger('pageSet.pager');
                    
                    console.log('test');
                } else {
                    $('#' + referringDoctorForm.messages.error).show();
                    $('#' + referringDoctorForm.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            },
            beforeSend: function (rs) {
                quickClaim.ajaxLocked = true;

                $('#referringDoctorMessages .icons div').hide();
                if (referringDoctorForm.parentInterface == 'referringDoctorList') {
                    $('#' + referringDoctorList.messages.spinner).show();
                    $('#' + referringDoctorList.messages.text).text('Deleting Referring Doctor').show();
                }

                $('#referringDoctorEditMessages .icons div').hide();
                $('#' + referringDoctorForm.messages.spinner).show();
                $('#' + referringDoctorForm.messages.text).text('Deleting Referring Doctor').show();
            },
            complete: function (rs) {
                quickClaim.ajaxLocked = false;

                if (referringDoctorForm.parentInterface == 'referringDoctorList') {
                    $('#' + referringDoctorList.messages.spinner).hide();
                }
                $('#' + referringDoctorForm.messages.spinner).hide();
            },
            error: function (rs) {
                quickClaim.ajaxLocked = false;

                $('#' + referringDoctorForm.messages.error).show();
                $('#' + referringDoctorForm.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();

                if (referringDoctorForm.parentInterface == 'referringDoctorList') {
                    $('#' + referringDoctorList.messages.error).show();
                    $('#' + referringDoctorList.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            }
        });
    },
    saveReferringDoctor: function () {
        if (quickClaim.ajaxLocked) {
            return;
        }

        referringDoctorForm.clearErrors();
        var data = $('#referringDoctorEditForm').serialize();
        
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: referringDoctorForm.actions.save,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('success')) {
                    if (referringDoctorForm.parentInterface == 'referringDoctorList') {
                        $('#referringDoctorEditFormDialog').dialog('close');
                        $('#referringDoctorTable').trigger('pageSet.pager');
                    } else if (referringDoctorForm.parentInterface == 'patientEdit') {
                        if (rs.hasOwnProperty('data') && rs.data.hasOwnProperty('0')) {
                            $('#referringDoctorEditFormDialog').dialog('close');
                            patient_edit.fillInRefDoc(rs.data[0]);
                        }
                    } else if (referringDoctorForm.parentInterface == 'claimEdit') {
                        if (rs.hasOwnProperty('data') && rs.data.hasOwnProperty('0')) {
                            $('#referringDoctorEditFormDialog').dialog('close');
                            claim.fillInRefDoc(rs.data[0]);
                        }
                    }
                } else if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#referringDoctorErrorDiv', '#referring_doctor', true);

                    var ers = 'The form contains invalid data.<ul style="padding:0px">';
                    for(var i in rs.errors ){
                        ers += '<li style="list-style-type:none">' + rs.errors[i]['label'] + ': ' + rs.errors[i]['message'] + '</li>';
                    }
                    ers += '</ul>';
                    $('#' + referringDoctorForm.messages.error).show();
                    $('#' + referringDoctorForm.messages.text).html(ers).show();
                }
            },
            beforeSend: function (rs) {
                quickClaim.ajaxLocked = true;

                $(".cliponeErrorInput").removeClass('cliponeErrorInput');
                $('#referringDoctorMessages .icons div').hide();
                if (referringDoctorForm.parentInterface == 'referringDoctorList') {
                    $('#' + referringDoctorList.messages.spinner).show();
                    $('#' + referringDoctorList.messages.text).text('Saving Referring Doctor').show();
                }

                $('#referringDoctorEditMessages .icons div').hide();
                $('#' + referringDoctorForm.messages.spinner).show();
                $('#' + referringDoctorForm.messages.text).text('Saving Referring Doctor').show();
            },
            complete: function (rs) {
                quickClaim.ajaxLocked = false;

                if (referringDoctorForm.parentInterface == 'referringDoctorList') {
                    $('#' + referringDoctorList.messages.spinner).hide();
                }
                $('#' + referringDoctorForm.messages.spinner).hide();
            },
            error: function (rs) {
                quickClaim.ajaxLocked = false;

                $('#' + referringDoctorForm.messages.error).show();
                $('#' + referringDoctorForm.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();

                if (referringDoctorForm.parentInterface == 'referringDoctorList') {
                    $('#' + referringDoctorList.messages.error).show();
                    $('#' + referringDoctorList.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            }
        });
    },
    clearErrors: function () {
        $('#referringDoctorEditForm #referringDoctorErrorDiv').text('').hide();
        $('#referringDoctorEditForm label.error').remove();
        $('#referringDoctorEditForm .error').removeClass('error');
    },
    clearForm: function () {
        referringDoctorForm.clearErrors();
        $('#referringDoctorTable tr.current_edit').removeClass('current_edit');

        $('#referringDoctorEditMessages .icons div').hide();
        $('#' + referringDoctorForm.messages.text).text('').hide();
        $('#referring_doctor_spec_code_id').val('').select2().trigger('change');
        $('#referring_doctor_province').val('').select2().trigger('change');
        $('#referringDoctorEditForm').clearForm();
    },
    fillForm: function (data) {
        this.clearForm();
        $('#referringDoctorTable tr#refDocRow_' + data.id).addClass('current_edit');

        var prefix = '#referring_doctor_';
        for (var key in data) {
            if (key == 'active') {
                $(prefix + key).prop('checked', data[key] == 'Y');
            } else if(prefix + key == '#referring_doctor_spec_code_id')
            {
               $('#referring_doctor_spec_code_id').val(data[key]).select2().trigger('change');
            } else if(prefix + key == '#referring_doctor_province')
            {
               $('#referring_doctor_province').val(data[key]).select2().trigger('change');
            } 
            else if ($(prefix + key)) {
                //alert(data['salutation']);
                $(prefix + key).val(data[key]);   
            }
        }

        if($('#referring_doctor_salutation').val() == '')
        {
         $('#referring_doctor_salutation').val('Dr.');
        }
       
        if (data.id) {
            if (data.actions.hasOwnProperty('delete')) {
                $('#deleteReferringDoctorButton').button('enable');
            } else {
                $('#deleteReferringDoctorButton').button('disable');
            }
        } else {
            $('#referring_doctor_active').prop('checked', true);
            $('#deleteReferringDoctorButton').hide();
        }

        $('#referringDoctorEditFormDialog').dialog('open');

        if ($('#referring_doctor_provider_num').val().length >= 6) {
            $('#referring_doctor_lname').focus();
        } else {
            quickClaim.focusTopField('referringDoctorEditForm');
        }
    }
}
