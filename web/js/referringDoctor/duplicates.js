var referringDoctorDuplicates = {
	actions: {
		duplicatesList: '',
		save: '',
		triggerDuplicateSearch: '',
		removeFromDuplicates: ''
	},
	buttonText: '',
	headers: [],
	currentMasterID: null,
	currentMergeID: null,
	data: {},
		
	initialize: function() {
		referringDoctorDuplicates.buttonText = $('#showDuplicateReferringDoctorButton').val();
		
		$('#showDuplicateReferringDoctorButton').button();
		$('#showDuplicateReferringDoctorButton').click(function() {
			$('#referringDoctorsDuplicatesDialog').dialog('open');
		});
		
		$('#mergeSelectionButton').button();
		$('#mergeSelectionButton').click(function() {
			referringDoctorDuplicates.loadConfirmationForm();
			
			$('#editDuplicate').show();
			$('#referringDoctorsDuplicatesSelection').hide();
			
		});

        $('#printTablebutton').button();
        $('#printTablebutton').click(function() {
            $('#referringDoctorTable').printElement({
                overrideElementCSS:['css/jquery-tablesorter1.css','css/jquery-tablesorter/jquery.tablesorter.pager.css','css/jquery-tablesorter/theme.blue.css']
            });
        });

		$('#searchForDuplicateReferringDoctorsButton').button();
		$('#searchForDuplicateReferringDoctorsButton').click(function() {
			$('#duplicateSearcchDialog').dialog('open');
		});
		
		$('#searchForDuplicatesExecuteButton').button();
		$('#searchForDuplicatesExecuteButton').click(function() {
			referringDoctorDuplicates.triggerDuplicateSearch();
		});

		$('#duplicateSearcchDialog').dialog({
			closeOnEscape: true,
			autoOpen: false,
			height: 400,
			modal: true,
			width:  800
		});
		
		$('#referringDoctorsDuplicatesDialog').dialog({
			closeOnEscape: true,
			autoOpen: false,
			modal: true,
			width: $(window).width() - 100,
			height: $(window).height() - 100,
			open: function() {
				referringDoctorDuplicates.loadDuplicates();
				referringDoctorDuplicates.currentMasterID = null;
				referringDoctorDuplicates.currentMergeID = null;

				$('#mergeSelectionButton').button('disable');
				
				$('#duplicateReferringDoctorTable tr.master td .text').text('');
				$('#duplicateReferringDoctorTable tr td.master .form input').val('');
				$('#duplicateReferringDoctorTable tr td.master .form select').val('');
				$('#duplicateReferringDoctorTable tr td.merged').text('');

				$('#editDuplicate').hide();
				$('#referringDoctorsDuplicatesSelection').show();				
			},
			close: function() {
				$('#referringDoctorsDuplicatesTable tbody tr').remove();
			}
		});

		var idx = 0;
		$('#referringDoctorsDuplicatesTable thead tr th').each(function() {
			referringDoctorDuplicates.headers[idx] = $(this).prop('class');
			idx++;
		});

		$('#referringDoctorsDuplicatesTable thead th.actions').addClass('sorter-false');
		$('#referringDoctorsDuplicatesTable thead th.select').addClass('sorter-false');
		$('#referringDoctorsDuplicatesTable thead th.select2').addClass('sorter-false');

		$('#referringDoctorsDuplicatesTable').tablesorter({
			theme: 'blue',
			headerTemplate : '{content} {icon}',
			widgets: ['selectRow', 'zebra', 'pager'],
			sortList: [[4, 0]]
		});
		
		$('#referringDoctorsDuplicatesTable').tablesorterPager({
			container: $('.referringDoctorDuplicatesPager'),
			size: 10,
			cssPageSize: '.pagesize',
		});
		
		$('#duplicateReferringDoctorTable tr td.merged').click(function() {
			var className = $(this).closest('tr').prop('class');
			var value = $(this).text();
			
			if (className == 'spec_code') {
				var merged = referringDoctorDuplicates.data[referringDoctorDuplicates.currentMergeID];
				$('#mergeReferringDoctorConfirmationForm #merge_referring_doctor_spec_code_id').val(merged['spec_code_id']);
			}
			else if (className == 'province') {
				$('#duplicateReferringDoctorTable #merge_referring_doctor_province').val(value);
			}
			else if (className != 'id') {
				$('#duplicateReferringDoctorTable tr.' + className + ' td.master .form input').val(value);
			}
		});
		
		$('#duplicateCancelMerge').button();
		$('#duplicateCancelMerge').click(function() {
			$('#editDuplicate').hide();
			$('#referringDoctorsDuplicatesSelection').show();			
		});
		
		$('#duplicateReferringDoctorMergeSubmit').button();
		$('#duplicateReferringDoctorMergeSubmit').click(function() {
			referringDoctorDuplicates.mergeRecords();
		});
	},
	initializeRadioButtons: function() {
		$('#referringDoctorsDuplicatesTable tr td.select input[type=radio]').click(function() {
			var value = $(this).val();
			referringDoctorDuplicates.currentMasterID = value;
			
			if (referringDoctorDuplicates.currentMasterID && referringDoctorDuplicates.currentMergeID && referringDoctorDuplicates.currentMasterID != referringDoctorDuplicates.currentMergeID) {
				$('#mergeSelectionButton').button('enable');
			}
			else {
				$('#mergeSelectionButton').button('disable');
			}
		});

		$('#referringDoctorsDuplicatesTable tr td.select2 input[type=radio]').click(function() {
			var value = $(this).val();
			referringDoctorDuplicates.currentMergeID = value;
			
			if (referringDoctorDuplicates.currentMasterID && referringDoctorDuplicates.currentMergeID && referringDoctorDuplicates.currentMasterID != referringDoctorDuplicates.currentMergeID) {
				$('#mergeSelectionButton').button('enable');
			}
			else {
				$('#mergeSelectionButton').button('disable');
			}
		});
		
		$('#referringDoctorsDuplicatesTable tr input.notADuplicate').button();
		$('#referringDoctorsDuplicatesTable tr input.notADuplicate').click(function() {
			var id = $(this).prop('id').replace('removeFromDuplicatesButton_', '');
			referringDoctorDuplicates.removeFromDuplicates(id);
		});
	},
	clearErrors: function() {
		$('#mergeReferringDoctorConfirmationForm #mergeReferringDoctorErrorDiv').text('').hide();
		$('#mergeReferringDoctorConfirmationForm label.error').remove();
		$('#mergeReferringDoctorConfirmationForm .error').removeClass('error');
	},
	clearForm: function() {
		referringDoctorDuplicates.clearErrors();
		$('#mergeReferringDoctorConfirmationForm').clearForm();
	},
	hideIndexPageButton: function() {
		$('#showDuplicateReferringDoctorButton').hide();
	},
	showIndexPageButton: function(duplicateCount) {
		$('#showDuplicateReferringDoctorButton').val(duplicateCount + ' ' + referringDoctorDuplicates.buttonText);
		$('#showDuplicateReferringDoctorButton').show();
	},
	loadConfirmationForm: function() {
		var master = referringDoctorDuplicates.data[referringDoctorDuplicates.currentMasterID];
		var merged = referringDoctorDuplicates.data[referringDoctorDuplicates.currentMergeID];
		
		for (var a in master) {
			$('#duplicateReferringDoctorTable tr.' + a + ' td.master .form input').val(master[a]);
		}
		
		for (var a in merged) {
			$('#duplicateReferringDoctorTable tr.' + a + ' td.merged').text(merged[a]);
		}

		$('#duplicateReferringDoctorTable tr.id td.master .text').text(master['id']);
		$('#duplicateReferringDoctorTable tr.id td.merged').text(merged['id']);
		
		$('#mergeReferringDoctorConfirmationForm #merge_referring_doctor_master_id').val(master['id']);
		$('#mergeReferringDoctorConfirmationForm #merge_referring_doctor_merge_id').val(merged['id']);
		$('#mergeReferringDoctorConfirmationForm #merge_referring_doctor_spec_code_id').val(master['spec_code_id']);
		$('#mergeReferringDoctorConfirmationForm #merge_referring_doctor_province').val(master['province']);
	},
	loadDuplicates: function() {
		var data = '';

		$.ajax({
			type: 'post',
			dataType: 'json',
			data: data,
			url: referringDoctorDuplicates.actions.duplicatesList,
			success: function(rs) {
				if (rs.hasOwnProperty('data')) {
					referringDoctorDuplicates.data = {};
					for (var a in rs.data) {
						referringDoctorDuplicates.data[rs.data[a]['id']] = rs.data[a];
						referringDoctorDuplicates.createTableRow(rs.data[a]);
					}
					$('#referringDoctorsDuplicatesTable').trigger('update');
					
					referringDoctorDuplicates.initializeRadioButtons();
					
				}
			},
			beforeSend: function() {
				$('#referringDoctorDuplicateMessages .icons div').hide();
				$('#referringDoctorDuplicateSpinner').show();
				$('#referringDoctorDuplicateMessageText').text('Retrieving possible duplicates list').show();
			},
			complete: function() {
				$('#referringDoctorDuplicateSpinner').hide();
			},
			error: function() {
				$('#referringDoctorDuplicateErrorIcon').show();
				$('#referringDoctorDuplicateMessageText').text('An error occurred while contacting the server.  Please refresh the page and try again.').show();
			}
		});
	},
	mergeRecords: function() {
		if (quickClaim.ajaxLocked) {
			return;
		}
		
		referringDoctorDuplicates.clearErrors();
		var data = $('#mergeReferringDoctorConfirmationForm').serialize()
			+ '&id=' + referringDoctorDuplicates.currentMasterID;
		
		$.ajax({
			type: 'post',
			dataType: 'json',
			url: referringDoctorDuplicates.actions.save,
			data: data,
			success: function(rs) {
				if (rs.hasOwnProperty('errors')) {
					quickClaim.showFormErrors(rs.errors, '#mergeReferringDoctorErrorDiv', '#merge_referring_doctor', true);

					$('#referringDoctorDuplicateErrorIcon').show();
					$('#referringDoctorDuplicateMessageText').text('The form contains invalid data.').show();
				}
				else {
					$('#mergeSelectionButton').button('disable');

					$('#editDuplicate').hide();
					$('#referringDoctorsDuplicatesSelection').show();				

					$('#duplicateRow_' + referringDoctorDuplicates.currentMasterID).remove();
					$('#duplicateRow_' + referringDoctorDuplicates.currentMergeID).remove();

					referringDoctorDuplicates.currentMasterID = null;
					referringDoctorDuplicates.currentMergeID = null;
					
					$('#referringDoctorsDuplicatesTable').trigger('update');
					$('#referringDoctorTable').trigger('update');
					
					$('#referringDoctorDuplicateMessageText').text('Merged Referring Doctors');
					$('#referringDoctorDuplicateCheckmark').show();
				}
			},
			beforeSend: function(rs) {
				quickClaim.ajaxLocked = true;
				
				$('#referringDoctorDuplicateMessages .icons div').hide();
				$('#referringDoctorDuplicateSpinner').show();
				$('#referringDoctorDuplicateMessageText').text('Merging Referring Doctors').show();

				quickClaim.showOverlayMessage('Merging Referring Doctors. This may take a while.');
			},
			complete: function(rs) {
				quickClaim.ajaxLocked = false;
				
				$('#referringDoctorDuplicateSpinner').hide();
				quickClaim.hideOverlayMessage(); 
			},
			error: function(rs) {
				quickClaim.ajaxLocked = false;

				$('#referringDoctorDuplicateErrorIcon').show();
				$('#referringDoctorDuplicateMessageText').text('An error occurred while contacting the server. Please refresh the page and try again.').show();
			}
		});
	},
	removeFromDuplicates: function(id) {
		if (quickClaim.ajaxLocked) {
			return;
		}

		var data = 'id=' + id;

		$.ajax({
			type: 'post',
			dataType: 'json',
			data: data,
			url: referringDoctorDuplicates.actions.removeFromDuplicates,
			success: function(rs) {
				if (rs.hasOwnProperty('removedIDs')) {
					for (var a in rs.removedIDs) {
						$('#duplicateRow_' + rs.removedIDs[a]).remove();
					}
					$('#referringDoctorsDuplicatesTable').trigger('update');
					
					$('#referringDoctorDuplicateMessageText').text('Removed Duplicate(s)');
					$('#referringDoctorDuplicateCheckmark').show();
					
				}
			},
			beforeSend: function() {
				$('#referringDoctorDuplicateMessages .icons div').hide();
				$('#referringDoctorDuplicateSpinner').show();
				$('#referringDoctorDuplicateMessageText').text('Removing Duplicates').show();
				quickClaim.showOverlayMessage('Removing Duplicate Referring Doctors.');
			},
			complete: function() {
				$('#referringDoctorDuplicateSpinner').hide();
				quickClaim.hideOverlayMessage(); 
			},
			error: function() {
				$('#referringDoctorDuplicateErrorIcon').show();
				$('#referringDoctorDuplicateMessageText').text('An error occurred while contacting the server.  Please refresh the page and try again.').show();
			}
		});
		
	},
	triggerDuplicateSearch: function() {
		if (quickClaim.ajaxLocked) {
			return;
		}
		
		var data = '';
		
		$.ajax({
			type: 'post',
			dataType: 'json',
			url: referringDoctorDuplicates.actions.triggerDuplicateSearch,
			data: data,
			success: function(rs) {
				if (rs.hasOwnProperty('batchJobs')) {
					referringDoctorList.displayBatchJobDetails(rs.batchJobs);
				}
			},
			beforeSend: function(rs) {
				quickClaim.ajaxLocked = true;
				
				$('#duplicateSearcchDialog').dialog('close');

				// get batch process information
				
//				$('#referringDoctorDuplicateMessages .icons div').hide();
//				$('#referringDoctorDuplicateSpinner').show();
//				$('#referringDoctorDuplicateMessageText').text('Merging Referring Doctors').show();
//
//				quickClaim.showOverlayMessage('Merging Referring Doctors. This may take a while.');
			},
			complete: function(rs) {
				quickClaim.ajaxLocked = false;
				
//				$('#referringDoctorDuplicateSpinner').hide();
//				quickClaim.hideOverlayMessage(); 
			},
			error: function(rs) {
				quickClaim.ajaxLocked = false;

				$('#referringDoctorDuplicateErrorIcon').show();
				$('#referringDoctorDuplicateMessageText').text('An error occurred while contacting the server. Please refresh the page and try again.').show();
			}
		});
		
	},
	createTableRow: function(data) {
		var tr = '<tr id="duplicateRow_' + data.id + '">';
		for (var a in referringDoctorDuplicates.headers) {
			tr += '<td class="' + referringDoctorDuplicates.headers[a] + '">';
			if (referringDoctorDuplicates.headers[a] == 'select') {
				tr += '<input type="radio" name="merge_referring_doctor[master]" id="merge_referring_doctor_master_' + data.id + '" value="' + data.id + '" />';
			}
			else if (referringDoctorDuplicates.headers[a] == 'select2') {
				tr += '<input type="radio" name="merge_referring_doctor[merge]" id="merge_referring_doctor_merge_' + data.id + '" value="' + data.id + '" />';
			}
			else if (referringDoctorDuplicates.headers[a] == 'removeFromuplicatesList') {
				tr += '<input type="button" id="removeFromDuplicatesButton_' + data.id + '" value="Not A Duplicate" class="ui-button-inline notADuplicate" />';
			}
			else {
				tr += data[referringDoctorDuplicates.headers[a]];
			}
			tr += '</td>';
		}
		tr += '</tr>';
		
		$('#referringDoctorsDuplicatesTable tbody').append($(tr));
	}
};