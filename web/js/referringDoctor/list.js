var referringDoctorList = {
    actions: {
        referringDoctorTable: null
    },
    // TODO: phase out old style of messaging
    messages: {
        error: 'referringDoctorErrorIcon',
        spinner: 'referringDoctorSpinner',
        checkmark: 'referringDoctorCheckmark',
        text: 'referringDoctorMessageText',
        spinnerHtml: '',
        checkmarkHtml: '',
        errorHtml: ''
    },
    referringDoctors: {},
    parentInterface: '',

    initialized: false,
    pagerColumns: {},
    paginationSize: 25,
    paginationSort: [],

    initialize: function () {
        if (!referringDoctorList.initialized) {
            $('#referringDoctorSearchButton').button();
            $('#referringDoctorResetButton').button();

            $('#referringDoctorSearchButton').click(function () {
//                $(".custActionBtnsreferringDoctorTable").remove();
                $('<div/>', {class: 'custActionBtnsreferringDoctorTable'}).appendTo('body');
                $(".actionButtonsDivreferringDoctorTable").detach().appendTo(".custActionBtnsreferringDoctorTable");
//                var custbtns = $(".actionButtonsDivreferringDoctorTable").html();
//                $('body').append('<div class="custActionBtnsreferringDoctorTable">'+custbtns+'</div>');
//              $(".custActionBtnsreferringDoctorTable").html($(".actionButtonsDivreferringDoctorTable").html());
                $("#referringDoctorTable").dataTable().fnDestroy();
                $("#referringDoctorTable thead tr:eq(1)").remove();
                $('#referringDoctorTable').trigger('pageSet.pager');
                setTimeout(function(){
                    if($(".custActionBtnsreferringDoctorTable").length > 1){
                        $(".custActionBtnsreferringDoctorTable:eq(0)").remove();
                    }
                },200);
                
            });

            $('#referringDoctorResetButton').click(function () {
                $('#referringDoctorSearchForm').clearForm();
                $(".custActionBtnsreferringDoctorTable").html($(".actionButtonsDivreferringDoctorTable").html());
                $("#referringDoctorTable").dataTable().fnDestroy();
                $("#referringDoctorTable thead tr:eq(1)").remove();
                $('#referringDoctorTable').trigger('pageSet.pager');
            });

            if (referringDoctorList.parentInterface == 'referringDoctorList') {
                referringDoctorList.initializePager();
            }
        }
    },
    initializePager: function () {
        referringDoctorList.initialized = true;
        $('#referringDoctorTable thead th').addClass('sorter-false');
        $('#referringDoctorTable thead th:last').append(quickClaim.tablesorterTooltip);
        $('#referringDoctorTable').tablesorter({
            theme: 'blue',
            headerTemplate: '{content} {icon}',
            widgets: []
        });
        $('#referringDoctorTable .sortTip').tooltip();

        $('#referringDoctorTable').tablesorterPager({
            container: $('.referringDoctorPager'),
            size: 100000,
            cssPageSize: '.pagesize',
            ajaxUrl: referringDoctorList.actions.referringDoctorTable + '?{sortList:sort}&pageNumber={page}&size={size}',
            customAjaxUrl: function (table, url) {
                console.log(url);
                return url + '&' + $('#referringDoctorSearchForm').serialize();
            },
            ajaxObject: {
                dataType: 'json',
                complete: function () {
                    quickClaim.ajaxLocked = false;
                    $('#' + referringDoctorList.messages.spinner).hide();

                    for (var a in referringDoctorList.referringDoctors) {
                        if (referringDoctorList.referringDoctors[a].hasOwnProperty('actions') && referringDoctorList.referringDoctors[a].actions.hasOwnProperty('edit')) {
                            $('tr#refDocRow_' + referringDoctorList.referringDoctors[a].id).click(function () {
                                referringDoctorList.handleAction($(this).prop('id'));
                            });
                        }
                    }
                    var tableId = 'referringDoctorTable';
                    global_js.customFooterDataTable(tableId, true);
     
                },
                beforeSend: function () {
                    //$(".custActionBtnsreferringDoctorTable").html($(".actionButtonsDivreferringDoctorTable").html());
                    $("#referringDoctorTable").dataTable().fnDestroy();
                    $("#referringDoctorTable thead tr:eq(1)").remove();
                    
                    quickClaim.ajaxLocked = true;
                    $('#referringDoctorMessages .icons div').hide();
                    $('#' + referringDoctorList.messages.spinner).show();
                    $('#' + referringDoctorList.messages.text).text('Requesting Referring Doctors').show();
                },
                error: function () {
                    $('#' + referringDoctorList.messages.error).show();
                    $('#' + referringDoctorList.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            },
            ajaxProcessing: function (rs) {
                quickClaim.hideOverlayMessage(1);

                $('#' + referringDoctorList.messages.text).text('Received Referring Doctors list.').show();
                $('#' + referringDoctorList.messages.checkmark).show();

                if (rs.hasOwnProperty('batchJobs')) {
                    referringDoctorList.displayBatchJobDetails(rs.batchJobs);
                }

                if (rs.hasOwnProperty('possibleDuplicates') && rs.possibleDuplicates) {
                    if (referringDoctorList.parentInterface != 'claimEdit' && referringDoctorList.parentInterface != 'patientEdit') {
                        referringDoctorDuplicates.showIndexPageButton(rs.possibleDuplicates);
                    }
                } else {
                    if (referringDoctorList.parentInterface != 'claimEdit' && referringDoctorList.parentInterface != 'patientEdit') {
                        referringDoctorDuplicates.hideIndexPageButton();
                    }
                }

                if (rs.hasOwnProperty('data')) {
                    referringDoctorList.pagerColumns = rs.headers;
                    referringDoctorList.referringDoctors = {};
                    var data = rs.data;
                    var rows = '';

                    for (var a in data)
                    {
                        referringDoctorList.referringDoctors[data[a].id] = data[a];
                        rows += '<tr id="refDocRow_' + data[a].id + '">';
                        rows += referringDoctorList.getReferringDoctorTableRow(data[a], data[a].id);
                        rows += '</tr>' + "\n";
                    }

                    return [rs.count, $(rows)];
                }
            }
        });
    },

    displayBatchJobDetails: function (data) {
        if (data.job_count == 0) {
            $('.batchJobStatusDiv').hide();
            return;
        }

        $('.batchJobStatusDiv .batchCount').text(data.job_count);
        $('.batchJobStatusDiv ul li').remove();

        for (var a in data.jobs) {
            var li = '<li><b>' + data.jobs[a].document_name + ':</b> ' + data.jobs[a].status + '</li>';

            $('.batchJobStatusDiv ul').append($(li));
        }

        $('.batchJobStatusDiv').show();
    },
    getReferringDoctorTableRow: function (data, id) {
        var rs = '<td></td>';

        for (var b in referringDoctorList.pagerColumns)
        {
            if (b == 'actions') {
                rs += '<td class="' + b + '">' + data[b] + '</td>';
            } else {
                rs += '<td class="' + b + '">' + data[b] + '</td>';
            }
        }
        return rs;
    },
    handleAction: function (buttonID) {
        buttonID = buttonID.replace('refDocRow_', '');
        var id = buttonID.substring(buttonID.indexOf('_') + 1, buttonID.length);

        if (referringDoctorList.parentInterface == 'patientEdit') {
            var data = referringDoctorList.referringDoctors[id];
            var rs = {id: data.id, autocomplete: data.autocomplete, provider_num: data.provider_num};

            patient_edit.fillInRefDoc(rs);
            $('#referring_doctor_dialog').dialog('close');
        } else if (referringDoctorList.parentInterface == 'claimEdit') {
            var data = referringDoctorList.referringDoctors[id];
            var rs = {id: data.id, autocomplete: data.autocomplete, provider_num: data.provider_num};

            claim.fillInRefDoc(rs);
            $('#referring_doctor_dialog').dialog('close');
        } else {
            referringDoctorForm.edit(referringDoctorList.referringDoctors[id]);
        }
    }
}