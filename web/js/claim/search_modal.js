var search_modal = {
	current_diag_code: null,
	current_service_code: null,
	item_target: null,
	prefix: 'claim_claim_items_',

	handleDiagCodeClick: function(val) {
		quickClaim.modals.removeModal('diag_code_dialog');

		var count = this.current_diag_code.length;
		for (var a = 0; a < this.current_diag_code.length; a++) {
			$('#' + this.prefix + this.current_diag_code[a] + '_diag_code').val(val);
		}

		if (count == 0) {
			$('#' + this.prefix + '0_diag_code').val(val);
		}
	},

	handleDiagCodeSearch: function(count) {
		this.current_diag_code = count;

		quickClaim.modals.addModal('diag_code_dialog');
		diagCodeSearchOpenHandler();
	},

	handleServiceCodeClick: function(val) {
		quickClaim.modals.removeModal('service_code_dialog');
		count = this.current_service_code.replace('claim_claim_items_', '').replace('_service_code', '');

		$('#' + this.current_service_code).val(val);
		this.item_target.serviceCodeLookup(this.current_service_code, count);

		$('#' + this.current_service_code.replace('service_code', 'diag_code')).focus();
	},

	handleServiceCodeSearch: function(count) {
		this.current_service_code = 'claim_claim_items_' + count + '_service_code';
		quickClaim.modals.addModal('service_code_dialog');
		serviceCodeSearchOpenHandler();
	}
};