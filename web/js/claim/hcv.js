var claimHCV;

claimHCV = {
    actions: {
        hcv: null,
        enabled: false
    },
    values: {
        health_num: '',
        version_code: ''
    },

    healthCard: 'claim_health_num',
    versionCode: 'claim_version_code',
    patientID: 'claim_patient_id',
    firstName: 'claim_fname',
    lastName: 'claim_lname',
    sex: 'claim_sex',
    dob: 'claim_dob',

    initialize: function () {
        var date = new Date();
        Date.prototype.yyyymmdd = function (char) {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = this.getDate().toString();
            return yyyy + char + (mm[1] ? mm : "0" + mm[0]) + char + (dd[1] ? dd : "0" + dd[0]); // padding
        };

        Date.prototype.mmddyyyy = function (char) {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = this.getDate().toString();

            return (mm[1] ? mm : "0" + mm[0]) + char + (dd[1] ? dd : "0" + dd[0]) + char + yyyy;
        }

        Date.prototype.ddmmyyyy = function (char) {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = this.getDate().toString();

            return (dd[1] ? dd : "0" + dd[0]) + char + (mm[1] ? mm : "0" + mm[0]) + char + yyyy;
        } 
    }, 
    validate: function (buttonClicked,isvalidatepopup = true) {
        
        if (quickClaim.hcvLocked || !claimHCV.actions.enabled) {
            return;
        }
        var valuesChanged = false;
        var hcn = $('#' + claimHCV.healthCard).val().trim();
        var vc = $('#' + claimHCV.versionCode).val().toUpperCase().trim();
        var patient_id = $('#' + claimHCV.patientID).val();
        if(hcn.length <= 9){
            if(isvalidatepopup == 'false') {
            $('.smallvalidatemessage').addClass('text-danger');
            $('.smallvalidatemessage').html('Invalid number of characters for healthcard');
            }  else {
             sk.skNotify('Invalid number of characters for healthcard', 'danger');   
            }
            return;
        }
        if (!hcn && !buttonClicked) {
            return;
        }

        if (hcn != claimHCV.values.health_num || vc != claimHCV.values.version_code) {
            valuesChanged = true;
            claimHCV.values = {health_num: hcn, version_code: vc};
        }

        if (buttonClicked || valuesChanged) {
            var data = 'hcn=' + hcn + '&vc=' + vc + '&patient_id=' + patient_id;
            claimHCV.clearResults();

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: claimHCV.actions.hcv,
                timeout: 5000,
                data: data,
                success: function (rs) {

//                    only for testing
//                    var rs = {};
//                    rs.mod_ten = true;
//                    rs.status = 200;
//                    rs.response_message = 'SKDROID Ontario HC# pattern test.';
//                    rs.class_name = 'hcv_fail';
                    if(rs == null){
                        claimHCV.handleError500Response(rs,isvalidatepopup);
                        return false;
                    }
                    
                    if (rs.hasOwnProperty('code') && rs.code == 500) {
                        claimHCV.handleError500Response(rs,isvalidatepopup);
                    } else {
                        if (rs.hasOwnProperty('mod_ten')) {
                            claimHCV.handleModTenResponse(rs,isvalidatepopup);
                        } else {
                            claim.values = {health_num: hcn, version_code: vc};
                            claimHCV.handleHCVResponse(rs,isvalidatepopup);
                        }
                    }

                },
                beforeSend: function () {
                    quickClaim.hcvLocked = true;
                    $('#checkmark').hide();
                    $('#error_icon').hide();
                    $('#spinner').show();

                    $('#messages_text').text('Validating Health Card').show();
                },
                complete: function () {
                    quickClaim.hcvLocked = false;
                    $('#spinner').hide();
                    $("#claim_sex").trigger('change');   
                },
                error: function () {
                    $('#error_icon').show();
                    $('#messages_text').text('An error occurred while contacting the server.');
                }
            });
        }


    },
    clearResults: function () {
        $('#hcv_results').removeClass('hcv_pass').removeClass('hcv_fail').removeClass('hcv_warn').removeClass('hcv_mod10_pass').text('');
        $('#fee_results').removeClass('hcv_pass').removeClass('hcv_fail').removeClass('hcv_warn').removeClass('hcv_mod10_pass').text('');
        $('#health_card_check_row').hide();
        $('#fee_service_check_row').hide();
        $('[id=fee_details]').remove();
    },
    handleError500Response: function (rs, isvalidatepopup) { 
        if(isvalidatepopup == 'false') {
            $('.smallvalidatemessage').addClass('text-danger');
            $('.smallvalidatemessage').html('HCV is down at the moment. Service should be restored shortly');
        } else {
            $('.smallvalidatemessage').html('');
            $.notifyClose();
            $.notify('HCV is down at the moment. Service should be restored shortly.', {
                        element: 'body',
                        type: "info",
                        allow_dismiss: true,
                        newest_on_top: true,
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        delay: 5000,
                        z_index: 99999,
                        animate: {
                            enter: 'animated bounceIn',
                            exit: 'animated bounceOut'
                        },
                        mouse_over: 'pause',
                        template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                    <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                    <span data-notify="icon"></span>
                                    <span data-notify="title">{1}</span>
                                    <span data-notify="message">{2}</span>
                                    <div class="progress" data-notify="progressbar">
                                            <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                    </div>
                                    <a href="{3}" target="{4}" data-notify="url"></a>
                            </div>`

                    }); 
        }
        
    },
    handleModTenResponse: function (rs, isvalidatepopup) {   
        if(isvalidatepopup == 'false') {
            $('.smallvalidatemessage').addClass('text-warning');
            $('.smallvalidatemessage').html(rs['response_message']);
        } else {
            $('.smallvalidatemessage').html('');
        var date = new Date();
        if (rs.hasOwnProperty('response_message')) {
            $('#hcv_results').removeClass('hcv_mod10_pass').removeClass('hcv_fail').removeClass('hcv_success')
                .text(moment(new Date()).format(global_js.getNewMomentFormat()) + ': ' + rs['response_message']);
        }
        $('#messages_text').text('Received message.').show();
        $('#health_card_check_row').removeClass('hcv_fail').removeClass('hcv_success').removeClass('hcv_pass').addClass(rs.class_name).show();
        $.notifyClose();
        $.notify(rs['response_message'], {
                    element: 'body',
                    type: "warning",
                    allow_dismiss: true,
                    newest_on_top: true,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    delay: 5000,
                    z_index: 99999,
                    mouse_over: 'pause',
                    animate: {
                        enter: 'animated bounceIn',
                        exit: 'animated bounceOut'
                    },
                    template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                <span data-notify="icon"></span>
                                <span data-notify="title">{1}</span>
                                <span data-notify="message">{2}</span>
                                <div class="progress" data-notify="progressbar">
                                <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                </div>
                                <a href="{3}" target="{4}" data-notify="url"></a>
                        </div>`

                });
        }
    },
    handleHCVResponse: function (rs, isvalidatepopup) {
        function AutofillForm() {
            if (person['firstName'] && person['lastName'] && person['gender']) {
                if (rs.hasOwnProperty('swipe_replace') && rs['swipe_replace']) {
                    $('#claim_fname').val(person['firstName']);
                }
                $('#claim_lname').val(person['lastName']);
                $('#claim_sex').val(person['gender'] == 'F' ? 'F' : 'M');
                $('#patient_province').val("ON");
            }
        }

        function setDates() {
            var dateOfBirth = new Date(person.dateOfBirth);
            if (person.expiryDate)
                var expiryDate = new Date(person.expiryDate);
            $('#claim_dob').skDP('setDate', dateOfBirth);
            
            if (expiryDate) {
                $('#claim_hcn_exp_year').skDP('update', expiryDate).skDP('fill');
            }
            quickClaim.calculateAge(new Date(person.dateOfBirth), 'age');
        }

        function setHTMLBar() {
            var date = new Date();

            $('#health_card_check_row').removeClass('hcv_fail').removeClass('hcv_pass').addClass(rs.class_name).show();
            $('#hcv_results').html('<span>' + date.toString(quickClaim.getParseDateFormat(global_js.getNewDateFormat())) + ': ' + rs.response_message + '</span>');
        }

        function handleFeeServiceDetails() {
            var new_content = '';
            var actual_content = '';
            if (rs.hasOwnProperty('fee_service_details')) {
                message += '<hr>';
                for (var index = 0; index < rs['fee_service_details'].length; index++) {
                    actual_content += '<tr id="fee_details"><td>&nbsp;</td><td class="' + rs.class_name + '">';
                    for (var a in rs['fee_service_details'][index]) {

                        if (!rs['fee_service_details'][index][a])
                            continue;
                        if (a == 'feeServiceCode') {
                            console.log(rs['fee_service_details'][index]['feeServiceDate']);
                            if(rs['fee_service_details'][index]['feeServiceDate'] != null)
                            {
                             rs['fee_service_details'][index][a] = '<b>'+rs['fee_service_details'][index][a].substr(0, 4)+'</b>';   
                            } else {
                             rs['fee_service_details'][index][a] = rs['fee_service_details'][index][a].substr(0, 4);
                            }
                        } else if (a == 'feeServiceDate') {
                            rs['fee_service_details'][index][a] = '<b>'+new Date(rs['fee_service_details'][index][a]).toDateString()+'</b>';
                        }

                        new_content += rs['fee_service_details'][index][a] + ' ';
                        message += rs['fee_service_details'][index][a] + ' ';
                    }
                    message += '<br>';
                    actual_content += new_content + '</td></tr>';
                    new_content = '';
                    $('#hcv_validation').after(actual_content);
                    actual_content = '';
                }
            }
        }

        function buildResponseMsg() {
            var message = rs.response_action + ' <br><b>' + rs.response_message + '</b>';
            var patient_dob = new Date(person['dateOfBirth'])
            var patient_expires;
            message = person['lastName'] + ', ' + person['firstName'] + ' <br>';
 
            if (person['expiryDate'] != null) {
                patient_expires = new Date(person['expiryDate']);
                message += rs.response_code + ' : ' + rs.response_action + ' <br>DOB: ' + patient_dob.toDateString() + ' expires: ' + patient_expires.toDateString();
            } else {
                patient_expires = true;
                message += rs.response_code + ' : ' + rs.response_action + ' <br>DOB: ' + patient_dob.toDateString();
            }
            return message;
        }
  
        function alertUser() {
            if (claim.tour_running) {
                return;
            }

            if (rs.class_name == 'hcv_pass') {
                 if(isvalidatepopup == 'false') {
                    $('.smallvalidatemessage').addClass('text-warning');
                    $('.smallvalidatemessage').html(rs.response_message);
                    return false;
                }
                $('.smallvalidatemessage').html(''); 
                $.notifyClose();
                $.notify(message, {
                            element: 'body',
                            type: "success",
                            allow_dismiss: true,
                            newest_on_top: true,
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            delay: 10000,
                            z_index: 99999,
                            mouse_over: 'pause',
                            animate: {
                                enter: 'animated bounceIn',
                                exit: 'animated bounceOut'
                            },
                            template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                            <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                            <span data-notify="icon"></span>
                                            <span data-notify="title">{1}</span>
                                            <span data-notify="message">{2}</span>
                                            <div class="progress" data-notify="progressbar">
                                                    <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <a href="{3}" target="{4}" data-notify="url"></a>
                                    </div>`

                                });
            }
        }

        function validResponseCode(code) {
            switch (code) {
                case '50':
                case '51':
                case '52':
                case '53':
                case '54':
                case '55':
                    return true;
                case '05':
                case '10':
                case '15':
                case '20':
                case '25':
                case '60':
                case '65':
                case '70':
                case '75':
                case '80':
                case '83':
                case '90':
                case '99':
                case '9A':
                case '9B':
                case '9C':
                case '9D':
                case '9E':
                case '9F':
                case '9G':
                case '9H':
                case '9I':
                case '9J':
                case '9K':
                case '9L':
                case '9M':
                    return false;
                default:
                    return false;
            }
        }

        if (rs.hasOwnProperty('results') && rs['results'][0]) {
            var person = rs['results'][0];

            if (person && validResponseCode(person.responseCode)) {
                $('#messages_text').text('Received message.').show();

                AutofillForm();

                setDates();

                if (rs.hasOwnProperty('response_message')) {
                    setHTMLBar();

                    var message = buildResponseMsg();
                    handleFeeServiceDetails();
                    alertUser();
                }
            } else {

                setHTMLBar();
                AutofillForm();
                setDates();
                var message = rs.response_action + ' <br><b>' + rs.response_message + '</b>';

                if(isvalidatepopup == 'false') {
                    $('.smallvalidatemessage').addClass('text-warning');
                    $('.smallvalidatemessage').html(rs.response_message);
                    return false;
                }
                $('.smallvalidatemessage').html('');
                $.notifyClose();
                $.notify(message, {
                            element: 'body',
                            type: "danger",
                            allow_dismiss: true,
                            newest_on_top: true,
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            delay: 10000,
                            z_index: 99999,
                            mouse_over: 'pause',
                            animate: {
                                enter: 'animated bounceIn',
                                exit: 'animated bounceOut'
                            },
                            template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                            <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                            <span data-notify="icon"></span>
                                            <span data-notify="title">{1}</span>
                                            <span data-notify="message">{2}</span>
                                            <div class="progress" data-notify="progressbar">
                                                    <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <a href="{3}" target="{4}" data-notify="url"></a>
                                    </div>`

                                });
            }
        }

        $('#health_card_check_row').show();
        $('#fee_service_check_row').show();
    },
    showJGrowlMessage: function (message, cssClass) {
        $.jGrowl(message, {life: 10000, group: cssClass});
    }
};
