var refdoc_cache = [];
var claim;

claim = {
    transfer_patient_data_to_claim_form: false,
    always_show_temporary_button: false,
    auto_fill_last_code: false,
    can_create: true,
    canCreatePatients: true, // Required for Patient Search
    from_temporary_form: false,
    initialized: false,
    location_provinces: null,
    new_direct_autocheck_payment: false,
    no_results_yes_clicked: false,
    update_existing_claims: true,
    search_cache: {},
    refDocSelected: false,
    showDirectCodesForThirdParty: false,
    skipPatientSearch: false,
    tab_index: null,
    enable_hcv: false,
    edit_mode: false,
    event_holder: null,
    autocomplete_open: false,
    tour_running: false,
    patientHistory: {
        patientID: null,
        resultColumns: []
    },
    updatePatientAlert: false,

    actions: {
        cancel_loop: null,
        claim_edit: null,
        claim_save: null,
        history: null,
        pdf: null,
        next_in_loop: null,
        patientHistory: null,
        patient_profile: null,
        autocomplete_refdoc: null,
        service_code_lookup: null,
        temporary_delete: null,
        update_patient: null,
        down_arrow_option: null,
        load_last_five_services: null,
        compare_autofill: null
    },

    autofill: {
        data: {},
        default_location_id: null,
        default_province: null,
        enabled: false,
        fill_service_date: true,

        setData: function () {
            sk.skOloader(function (e) {
            setTimeout(function () {
                if($("#claim_health_num").val() == '')
                {
                  $("#claim_health_num").focus();      
                }
                else {
                $("#claim_claim_items_0_service_code").focus();
                }
                // if (is_new) {
                //     $("#claim_health_num").focus();
                // } else {
                //     $("#claim_claim_items_0_service_code").focus();
                // }
             }, 200)
             })
             // if (!$('#claim_patient_id').val()) {
             //        $('#claim_health_num').focus();
             //  } 
            //$('#claim_doctor_id').val(claim.claims_default_doctor).trigger('change.select2');
            var $province = $('#claim_province');
            var $locationID = $('#claim_location_id');

            if (!$province.val()) {
                $province.val(claim.autofill.default_province).trigger('change.select2');
            }

            if (!$locationID.val()) {
                $locationID.val(claim.autofill.default_location_id).trigger('change.select2');
            }

            var $serviceDate = $('#' + claim.items.form_prefix + '0_service_date');

            if (!claim.edit_mode) 
            {
              claim.items.claimLockData(); 
            }

            if (claim.autofill.enabled) {

                var $doctorID = $('#claim_doctor_id');
                var $facilityNum = $('#claim_facility_num');
                var $facilityDescription = $('#claim_facility_description');
                var $facilityID = $('#claim_facility_id');
                var $sli = $('#claim_sli');
                var $claim_type = $('#claim_pay_prog');

                //alert(!claim.edit_mode);

                //if(claim.transfer_patient_data_to_claim_form == 'false') {

                // 
                //     if (claim.autofill.data.doctor_id && claim.autofill.data.hasOwnProperty('doctor_id')) {
                //         $doctorID.val(claim.autofill.data.doctor_id).trigger('change.select2');
                //     }
                // }
                //check condition if claim form not edit mode
                if($facilityDescription.val() == ' --  -- () []')
                {
                   $facilityDescription.val('');
                }



                if (!claim.edit_mode) 
                {
                    //alert(claim.autofill.data.doctor_id);
                    if (!$doctorID.val() && claim.autofill.data.hasOwnProperty('doctor_id')) 
                    {
                       $doctorID.val(claim.autofill.data.doctor_id).trigger('change.select2');
                    }

                    
                    if(claim.transfer_patient_data_to_claim_form == 'false')
                    {
                        if (!$facilityNum.val() && claim.autofill.data.hasOwnProperty('facility_num')) {
                            $facilityNum.val(claim.autofill.data.facility_num);
                        }   
                        if (!$facilityDescription.val() && claim.autofill.data.hasOwnProperty('facility_description')) {
                            $facilityDescription.val(claim.autofill.data.facility_description);
                            $("#claim_admit_date").show();
                        } 
                        // else {
                        //     if (!claim.autofill.data.facility_description && !claim.edit_mode) {
                        //         claim.facility.lookup(claim.doctor.facilities[$doctorID.val()]);
                        //     }
                        // }
                        if (!$facilityID.val() && claim.autofill.data.hasOwnProperty('facility_id')) {
                            $facilityID.val(claim.autofill.data.facility_id);
                        }
                    }
                    ///}
                    if (!$sli.val() && claim.autofill.data.hasOwnProperty('sli')) {
                        $sli.val(claim.autofill.data.sli);
                    }
                    if (!$locationID.val() && claim.autofill.data.hasOwnProperty('location_id')) {
                        $locationID.val(claim.autofill.data.location_id).trigger('change.select2');
                    }
                    if (!$claim_type.val() && claim.autofill.data.hasOwnProperty('claim_type')) {

                      $claim_type.val(claim.autofill.data.claim_type).trigger('change.select2');
                    }

                }
            }
            if (claim.autofill.useServiceDate() && !$serviceDate.val() && claim.autofill.data.hasOwnProperty('last_service_date')) {

                $('#' + claim.items.form_prefix + '0_service_date').skDP('setDate', sk.getJsDate(claim.autofill.data.last_service_date));
            }

            if (claim.transfer_patient_data_to_claim_form == 'false' && !claim.autofill.enabled)
            {
                if(claim.is_default_doctor == true)
                {
                   $('#claim_doctor_id').val(claim.claims_default_doctor).trigger('change.select2');
                }
            }

            // if($('#claim_patient_id').val() == '' && claim.claims_default_doctor != '')
            // {   
            //     $('#claim_doctor_id').val(claim.claims_default_doctor).trigger('change.select2');
            //     //alert(claim.doctor.facilities[claim.claims_default_doctor]);
            //     //alert(claim.doctor.facilities[claim.claims_default_doctor]);
            // }
            
            $('#claim_note_row').hide();

            $("#claim_note_checkbox").change(function () {
                $('#claim_item_table tbody').css('height','');
                if (this.checked) {
                    $('#claim_note_row').show();
                    $('#claim_item_table tbody').css('max-height', 'calc(100vh - 510px)');
                } else {
                    $('#claim_note_row').hide();
                    $('#claim_item_table tbody').css('max-height', 'calc(100vh - 380px)');
                }
            });


            if (claim.autofill.data.doctor_id != localStorage.getItem('doc_id')) {
                $('#claim_doctor_id').css('background-color', '#ffcc66');
            } else {
                $('#claim_doctor_id').css('background-color', '#FFFFFF');
            }

            if (claim.autofill.data.sli != localStorage.getItem('SLI')) {
                $('#claim_sli').css('background-color', '#ffcc66');
            } else {
                $('#claim_sli').css('background-color', '#FFFFFF');
            }

            if (claim.autofill.data.facility_num != localStorage.getItem('facility_num')) {
                $('#facility_num').css('background-color', '#ffcc66');
                $('#claim_facility_id').css('background-color', '#ffcc66');
                $('#claim_facility_description').css('background-color', '#ffcc66');
            } else {
                $('#facility_num').css('background-color', '#FFFFFF');
                $('#claim_facility_id').css('background-color', '#FFFFFF');
                $('#claim_facility_description').css('background-color', '#FFFFFF');
            }

            
            // if(!$facilityDescription.val())
            // {
            //     $("#claim_admit_date").val('');
            // }
        },
        useServiceDate: function () {
            return claim.autofill.fill_service_date && !claim.from_temporary_form;
        }
    },
    calculate: {
        amountRemaining: function () {
            var sum = 0;

            $('#claim_item_table').find('tbody input.fee_subm').each(function () {
                var val = parseFloat($(this).val());
                if (val) {
                    sum += val;
                }
            });
            if ($('#claim_pay_prog').val() == 'DIRECT' && sum) {
                sum += claim.calculate.taxes();
            }
            sum = sum - parseFloat($('.total_paid').text(), 10) - parseFloat($('#claim_total_paid').val(), 10);
            $('.remaining_amount').text('$' + number_format(sum, 2));
        },
        dateAndPrefixSubtotal: function (row, date, prefixes) {
            var subtotal = 0;
            for (var a = row - 1; a >= 0; a--) {
                if ($('#claim_row_' + a).size() && $('#claim_claim_items_' + a + '_service_date').val() == date) {
                    var prefix = $('#claim_claim_items_' + a + '_service_code').val().charAt(0);
                    var idx = jQuery.inArray(prefix, prefixes);
                    if (idx != -1) {
                        subtotal += parseFloat($('#claim_claim_items_' + a + '_fee_subm').val());
                    }
                }
            }
            return number_format(subtotal, 2);
        },
        dateSubtotal: function (row, date, all_claims) {
            var subtotal = 0;
            for (var a = row - 1; a >= 0; a--) {
                if ($('#claim_row_' + a).size() && $('#claim_claim_items_' + a + '_service_date').val() == date) {
                    subtotal += parseFloat($('#claim_claim_items_' + a + '_fee_subm').val());

                    if (!all_claims) {
                        return number_format(subtotal, 2);
                    }
                }

            }
            return number_format(subtotal, 2);
        },
        datePercentageSubsetSubtotal: function (row, date, bypass) {
            // calculate everything below the code in the table until another premium code is reached
            var subtotal = 0;
            for (var a = row - 1; a >= 0; a--) {
                if ($('#claim_row_' + a).size() && $('#claim_claim_items_' + a + '_service_date').val() == date) {
                    if (!claim.services.isPremiumCode($('#claim_claim_items_' + a + '_service_code').val())) {
                        subtotal += parseFloat($('#claim_claim_items_' + a + '_fee_subm').val());
                    } else if (!bypass) {
                        return number_format(subtotal, 2);
                    }
                }
            }
            return number_format(subtotal, 2);
        },
        numberOfUnitsSubtotal: function (row, date) {
            var currentNumberOfUnits = $('#claim_claim_items_' + row + '_num_serv').val();

            if (currentNumberOfUnits) {
                return currentNumberOfUnits;
            }

            // calculate everything below the code in the table until another premium code is reached
            var subtotal = 0;
            for (var a = row - 1; a >= 0; a--) {
                if ($('#claim_row_' + a).size() && $('#claim_claim_items_' + a + '_service_date').val() == date) {
                    if (!claim.services.isPremiumCode($('#claim_claim_items_' + a + '_service_code').val())) {
                        subtotal += parseInt($('#claim_claim_items_' + a + '_num_serv').val(), 10);
                    } else {
                        return subtotal
                    }
                }
            }
            return subtotal;
        },
        premiumCodes: function (reset = 0) {
            var row_count = claim.items.row_count;
            var types = claim.services.code_types;
            for (var a = 0; a <= claim.items.row_count; a++) {
                var $serviceCode = $('#claim_claim_items_' + a + '_service_code');
                var $numServices = $('#claim_claim_items_' + a + '_num_serv');
                var $serviceDate = $('#claim_claim_items_' + a + '_service_date');

                var isNewRow = !$('#claim_claim_items_' + a + '_id').val() || (claim.update_existing_claims);

                if ($('#claim_row_' + a).size() && isNewRow) {
                    //  alert(a);
                    var service_code = $serviceCode.val();
                    var cache_item = null;
                    var num_serv = $numServices.val();

                    if (!parseInt(num_serv, 10)) {
                        num_serv = 1;
                    }

                    if (claim.services.cache.hasOwnProperty(service_code)) {
                        cache_item = claim.services.cache[service_code];
                    } else if (claim.services.cache.hasOwnProperty(service_code.substr(0, 4))) {
                        cache_item = claim.services.cache[service_code.substr(0, 4)];
                    }

                    var sc_type = null;
                    var date = $serviceDate.val() ? $serviceDate.val() : claim.items.previousServiceDate();
                    var fee = null;
                    var modifier = null;

                    if (cache_item != null && cache_item.hasOwnProperty('sc_type')) {
                        sc_type = cache_item['sc_type'];
                    }
                    if (cache_item == null || sc_type == types['normal'] || sc_type == types['do_not_change']) {
                    } else if (sc_type == types['base_percentage_increase']) {
                        var previousRowNum = a - 1;
                        num_serv = claim.calculate.numberOfUnitsSubtotal(a, date);

                        modifier = cache_item['premium_info'] * num_serv;
                        fee = claim.services.determineBaseFee(cache_item, service_code.substr(4, 1));
                        if (fee != $('#claim_claim_items_' + a + '_base_fee').val() && $('#claim_claim_items_' + a + '_base_fee').val() != '') {
                            fee = $('#claim_claim_items_' + a + '_base_fee').val();
                        }
                        $('#claim_claim_items_' + a + '_base_fee').val(fee);
                        $('#claim_claim_items_' + a + '_modifier').val(modifier);
                        $('#claim_claim_items_' + a + '_fee_subm').val(number_format(fee * modifier / 100, 2));
                        $numServices.val(num_serv);
                        $('#claim_claim_items_' + a + '_service_date').val(date);
                    } else if (sc_type == types['percentage_all']) {
                        bypass = $('#prem_bypass').prop('checked');
                        fee = claim.calculate.datePercentageSubsetSubtotal(a, date, bypass);
                        //alert(fee);
                        //todo replace flag
                        if (true && reset == 0) {
                            if ($('#claim_claim_items_' + a + '_base_fee').val() != 0) {
                                fee = $('#claim_claim_items_' + a + '_base_fee').val();
                            }
                        }

                        $('#claim_claim_items_' + a + '_base_fee').val(fee);
                        $('#claim_claim_items_' + a + '_modifier').val(cache_item['fee_general'] * 100);
                        $('#claim_claim_items_' + a + '_fee_subm').val(number_format(fee * cache_item['fee_general'], 2));
                        $numServices.val(num_serv);
                        $('#claim_claim_items_' + a + '_service_date').val(date);

                    } else if (sc_type == types['percentage_one']) {
                        fee = claim.calculate.dateSubtotal(a, date, false);

                        $('#claim_claim_items_' + a + '_base_fee').val(fee);
                        $('#claim_claim_items_' + a + '_modifier').val(cache_item['fee_general'] * 100);
                        $('#claim_claim_items_' + a + '_fee_subm').val(number_format(fee * cache_item['fee_general'], 2));
                        $numServices.val(num_serv);
                        $('#claim_claim_items_' + a + '_service_date').val(date);
                    } else if (sc_type == types['percentage_prefix']) {
                        fee = claim.calculate.dateAndPrefixSubtotal(a, date, cache_item['premium_info']);

                        $('#claim_claim_items_' + a + '_base_fee').val(fee);
                        $('#claim_claim_items_' + a + '_modifier').val(cache_item['fee_general'] * 100);
                        $('#claim_claim_items_' + a + '_fee_subm').val(number_format(fee * cache_item['fee_general'], 2));
                        $numServices.val(num_serv);
                        $('#claim_claim_items_' + a + '_service_date').val(date);
                    } else if (sc_type == types['premium_age']) {
                        var age = quickClaim.calculateAge($('#claim_dob').skDP('getDate'));
                        modifier = claim.services.determineModifier();
                        fee = cache_item['fee_general'];

                        for (var idx in cache_item['premium_info']) {
                            var record = cache_item['premium_info'][idx];

                            if (age[0] >= record[0] && age[1] <= record[1]) {
                                fee = record[2];
                            }
                        }

                        $('#claim_claim_items_' + a + '_base_fee').val(fee);
                        $('#claim_claim_items_' + a + '_modifier').val(modifier);
                        $('#claim_claim_items_' + a + '_fee_subm').val(number_format(fee, 2));
                        $numServices.val(num_serv);
                        $('#claim_claim_items_' + a + '_service_date').val(date);

                    } else {

                        alert('unhandled service code type');
                    }
                }
            }
        },
        taxes: function () {
            var taxable_totals = {};
            var taxation_amount = 0;
            var c = 0;
            var b;

            for (var a in claim.services.tax_cache) {
                taxable_totals[a] = 0;
            }

            for (c = 0; c <= claim.items.row_count; c++) {
                var sc = $('#claim_claim_items_' + c + '_service_code').val();
                var amount = parseFloat($('#claim_claim_items_' + c + '_fee_subm').val());

                if (amount && sc && claim.services.cache.hasOwnProperty(sc)) {
                    for (b in claim.services.cache[sc].taxes) {
                        taxable_totals[claim.services.cache[sc].taxes[b]] += amount;
                    }
                }
            }

            for (b in taxable_totals) {
                if (taxable_totals[b]) {
                    var total = number_format(taxable_totals[b] * claim.services.tax_cache[b].rate, 2, '.', '');
                    taxation_amount += parseFloat(total);
                    $('.total_billed.tax_' + b).show();
                    $('.taxable_amount_' + b).text('$' + total);

                    $('#claim_tax_' + b).val(total);
                }
            }

            return taxation_amount;
        },
        totalBilled: function (update_total_paid) {
            var sum = 0;
            var $paid = $('#claim_total_paid');

            $('#claim_item_table').find('input.fee_subm').each(function () {
                var val = parseFloat($(this).val());
                if (val) {
                    sum += val;
                }
            });

            $('.taxable_amounts').text('$' + number_format(0, 2));
            $('.total_billed.taxes').hide();
            $('.widget_tax').val('');

            if ($('#claim_pay_prog').val() == 'DIRECT' && sum) {
                sum += claim.calculate.taxes();
            }

            $('.billed_subtotal').text('$' + number_format(sum, 2));
            $('#claim_total_billed').val(number_format(sum, 2, '.', ''));

            sum = sum - parseFloat($('.total_paid').text(), 10);

            $('.outstanding_subtotal').text('$' + number_format(sum, 2));

            if ((update_total_paid) && $('#claim_record_direct_payment').prop('checked')) {
                $paid.val(number_format(sum, 2));
            }

            $('.remaining_amount').text('$' + number_format(sum - $paid.val(), 2));
            $('.claim_subtotal').text('$' + number_format(sum, 2));
        }
    },
    doctor: {
        bill_as_ana: null,
        facilities: {},
        slis: {},
        spec_codes: {},
        current_spec_code: '',
        current_modifier: '',
        modifiers: {},

        isAnaesthesiologist: function () {
            var spec_code = claim.doctor.spec_codes[$('#claim_doctor_id').val()];
            return (spec_code == '01' || spec_code == '00') ? true : false;
        },
        isBillAsAna: function () {
            return  claim.doctor.bill_as_ana[$('#claim_doctor_id').val()];
        },
        isSpecialist: function () {
            var spec_code = claim.doctor.spec_codes[$('#claim_doctor_id').val()];

            return !(spec_code == '12' || spec_code == '' || spec_code == '00');
        }
    },
    calculateMinutes: function (value) {
        var service_code = '';
        var baseunits, $baseUnits;
        //FF fix for no srcElement
        if (!claim.event_holder.srcElement) {
            var id = claim.event_holder.currentTarget.id;
            id = id.substr(-10, 1);
            service_code = $('#claim_claim_items_' + id + '_service_code');
            baseunits = parseInt($('#' + claim.event_holder.currentTarget.id).val());
            $baseUnits = $('#' + claim.event_holder.currentTarget.id);
        } else {
            service_code = claim.event_holder.srcElement.parentElement.parentElement.getElementsByClassName('service_code')[1];
            baseunits = parseInt($('#' + claim.event_holder.srcElement.id).val());
            $baseUnits = $('#' + claim.event_holder.srcElement.id);
        }

        service_code = $(service_code).val();
        var service_code_ending = service_code.slice(service_code.lastIndexOf(), service_code.length);
        var mins = value;

        if (service_code_ending == 'b' || service_code_ending == 'B') {
            //doctor assist
            if (mins <= 60) {
                baseunits += Math.ceil(mins / 15);
            } else if (mins >= 60) {
                baseunits += Math.ceil(mins / 15);
            } else if (mins >= 150) {
                baseunits += Math.ceil(mins / 15);
            }
            $baseUnits.val(baseunits);
        } else if (service_code_ending == 'c' || service_code_ending == 'C') {
            //anaesthesiologist
            if (mins <= 60) {
                baseunits += Math.ceil(mins / 15);
            } else if (mins > 60 && mins < 90) {
                baseunits += Math.ceil(mins / 15);
            } else if (mins >= 90) {
                baseunits += Math.ceil(mins / 15);
            }
            $baseUnits.val(baseunits);
        }
    },
    items: {
        row_count: 0,
        multiselect_service_date: false,
        canShowFeePaid: false,
        form_prefix: 'claim_claim_items_',
        autocomplete: {
            actions: {
                retrieve_codes: '/claim/autoCompleteCodes',
                retrieve_diag: '/claim/autoCompleteDiags'
            },
            diagCodeOptions: {
                delay: 300,
                minLength: 2,
                position: {my: 'left top', at: 'left bottom'},
                open: function (event, ui) {
                    claim.autocomplete_open = true;
                    $('.ui-helper-hidden-accessible').remove();

                    event.stopImmediatePropagation();
                },
                close: function (event, ui) {
                    claim.autocomplete_open = false;
                    $('.ui-helper-hidden-accessible').remove();

                    event.stopImmediatePropagation();
                },
                select: function (event, ui) {
                    $('.ui-helper-hidden-accessible').remove();
                    $('#' + event.target.id).val(ui.item.value);
                    $('.claim_item_row .diag_code').autocomplete('close');
                    claim.autocomplete_open = true;
                    event.stopImmediatePropagation();
                    return false;
                },
                focus: function (event, ui) {
                    $('#' + event.target.id).val(ui.item.value);
                    return false;
                },
                source: function (req, res) {
                    var matches = req.term.match(/\d+/g);

                    if (req.term.length > 2 && matches != null) {
                        $('.claim_item_row .diag_code').autocomplete('close');
                        claim.autocomplete_open = false;
                        return;
                    }

                    if (lscache) {
                        var result = lscache.get(req.term + '_diag');

                        if (result) {
                            res(result);
                        } else {

                            $.ajax({
                                dataType: 'json',
                                type: 'get',
                                data: 'code=' + req.term,
                                url: claim.items.autocomplete.actions.retrieve_diag,
                                cache: true,
                                success: function (rs) {
                                    if (typeof rs != "object")
                                        return;

                                    var result_set = [];

                                    for (var code in rs) {
                                        if (rs[code].description.includes('defined'))
                                            return;
                                        result_set[code] = {label: rs[code].diag_code + ': ' + rs[code].description, value: rs[code].diag_code};
                                    }
                                    res(result_set);
                                    lscache.set(req.term + '_diag', result_set, 360000);
                                }
                            });
                        }
                    }
                }
            },
            options: {
                delay: 200,
                minLength: 2,
                position: {my: 'left top', at: 'left bottom'},
                open: function (event, ui) {
                    //$('#results').css('top', $('.ui-autocomplete').height() + 8);
                    claim.autocomplete_open = true;
                    $('.ui-helper-hidden-accessible').remove();

                    event.stopImmediatePropagation();
                },
                close: function (event, ui) {
                    //$('#results').css('top', 0);
                    claim.autocomplete_open = false;
                    $('.ui-helper-hidden-accessible').remove();

                    event.stopImmediatePropagation();
                },
                select: function (event, ui) {
                    $('.ui-helper-hidden-accessible').remove();
                    $('#' + event.target.id).val(ui.item.value);
                    $('.claim_item_row .service_code').autocomplete('close');
                    claim.autocomplete_open = true;
                    var currentStage = event.target.id.match(/\d+/);
                    var nextStage = +currentStage[0] + 1;

                    $('#' + event.target.id.replace(currentStage[0], nextStage)).focus();
                    event.stopImmediatePropagation();

                    return false;
                },
                focus: function (event, ui) {
                    $('#' + event.target.id).val(ui.item.value);

                    return false;
                },
                source: function (req, res) {
                    if (lscache) {
                        var result = lscache.get(req.term + '_serv');
                        //console.log(result);
                        if (result) {
                            res(result);
                        } else {
                            $.ajax({
                                dataType: 'json',
                                type: 'get',
                                data: 'code=' + req.term,
                                url: claim.items.autocomplete.actions.retrieve_codes,
                                cache: true,
                                success: function (rs) {
                                    if (typeof rs != "object")
                                        return;
                                    var result_set = [];

                                    for (var code in rs) {
                                        result_set[code] = {label: rs[code].service_code + ': ' + rs[code].description, value: rs[code].service_code};
                                    }

                                    res(result_set);
                                    lscache.set(req.term + '_serv', result_set, 360000);
                                }
                            });
                        }
                    }
                }
            },
            init: function () {
                //ticket 3045, not sure why it was here but...
                //if (window.location.search.length == 0) {
                $('.claim_item_row .diag_code').autocomplete(claim.items.autocomplete.diagCodeOptions);
                $('.claim_item_row .service_code').autocomplete(claim.items.autocomplete.options);
                $('.ui-helper-hidden-accessible').remove();
                //}
            }
        },
        initialize: function (c) {
            // if (claim.autofill.enabled) {
            //      if (claim.autofill.data.hasOwnProperty('facility_description')) {
            //             var $facilityDescription = $('#claim_facility_description');
            //             if(claim.autofill.data.facility_description != "" && !claim.edit_mode)
            //             {
            //             $facilityDescription.val(claim.autofill.data.facility_description);
            //             }
            //      }
            // }

            var fee;
            var $serviceDate = $('#' + '' + this.form_prefix + c + '_service_date');
            var $baseFee = $('#' + this.form_prefix + c + '_base_fee');
            var $baseUnits = $('#' + this.form_prefix + c + '_num_serv');
            var $feeSub = $('#' + this.form_prefix + c + '_fee_subm');
            var $feePaid = $('#' + this.form_prefix + c + '_fee_paid');
            var $firstServiceDate = $('#' + this.form_prefix + '0_service_date');
            var $firstServiceCode = $('#' + this.form_prefix + c + '_service_code');

            if (claim.items.multiselect_service_date) {

                sk.datepicker('#' + '' + this.form_prefix + c + '_service_date', true);
                //$('#' + '' + this.form_prefix + c + '_service_date').skDP('setDate', sk.getJsDate($('#' + '' + this.form_prefix + '0_service_date').val()))
                $('#' + '' + this.form_prefix + c + '_service_date').on('hide', function () {
                    claim.items.handleServiceDate(c, $('#' + '' + claim.items.form_prefix + c + '_service_date').skDP('getDates'));
                })

            } else {
                sk.datepicker('#' + '' + this.form_prefix + c + '_service_date');

                $serviceDate.on('changeDate', function () {
                    claim.items.handleServiceDate(c, $('#' + '' + claim.items.form_prefix + c + '_service_date').skDP('getDate'));
                })

            }

            $('#' + this.form_prefix + c + '_num_serv').dblclick(function (e) {
                claim.event_holder = e;
                $("#time_dialog").dialog("option", "appendTo", "#claim_item_table");
                $("#time_dialog").dialog('open');


            });

            $('#' + this.form_prefix + c + '_num_serv').on('click touch', function (e) {
                claim.event_holder = e;
                $("#time_dialog").dialog("option", "appendTo", "#claim_item_table");
                $("#time_dialog").dialog('open');
            });

            $('#clear_' + c)
                    .hover(function () {
                        $(this).css('cursor', 'pointer');
                    })
                    .click(function () {
                        claim.items.clearRow(this.id.replace('clear_', ''));
                        claim.items.row_count = claim.items.row_count - 1;
                        $('#claim_recalculate').show();

                    });
            $('#' + this.form_prefix + c + '_service_code').blur(function (e) {
                if ($(this).val()) {
                    claim.services.handleCodeLookup(c);
                }
            });

            $('#claim_recalculate').button().click(function(){
                    claim.items.formatFeeSubm(c);
                    claim.services.updateBaseFees();
                    //claim.items.checkMultipleServices();
                    // claim.calculate.totalBilled(false);
                    claim.calculate.totalBilled(true);
                    claim.handlePayProgChange();
                    claim.calculate.premiumCodes(1);
            });

            $('#' + this.form_prefix + c + '_diag_code').blur(function (e) {
                claim.services.fillDiagCode(c);
            });

            $baseFee.blur(function (e) {
                claim.items.checkNumServices(c);
            });

            $('#' + this.form_prefix + c + '_num_serv').blur(function (e) {
                claim.items.checkNumServices(c);
            });

            $('#' + this.form_prefix + c + '_modifier').blur(function (e) {
                claim.items.checkNumServices(c);
            });

            $feeSub.blur(function (e) {
                claim.items.formatFeeSubm(c);
                claim.calculate.totalBilled(true);
                claim.calculate.premiumCodes();
            });

            $('#claim_record_direct_payment').change(function () {
                if ($('#claim_record_direct_payment').prop('checked')) {
                    claim.calculate.totalBilled(true);
                } else {
                    $('#claim_total_paid').val('0.00');
                    $('.remaining_amount').text($('.billed_subtotal').text());
                    claim.calculate.totalBilled(true);
                }
            });

            if ($feeSub.val()) {
                fee = parseFloat($feeSub.val());
                $feeSub.val(number_format(fee, 2, '.', ''));
            }

            if ($baseFee.val()) {
                fee = parseFloat($baseFee.val());
                $baseFee.val(number_format(fee, 2, '.', ''));
            }

            if ($feePaid.val()) {
                fee = parseFloat($feePaid.val());
                $feePaid.val(number_format(fee, 2, '.', ''));
            }

            claim.calculate.totalBilled(true);
            claim.items.formatFeeSubm(c);

            if (!$firstServiceDate.val() && claim.autofill.useServiceDate()) {
                $firstServiceDate.val($.datepick.formatDate(quickClaim.dateFormat, new Date()));
            }

            this.checkRowCount();
            claim.items.autocomplete.init();


            if ($("#claim_item_table tbody").height() > 204) {
            }

            $('.insertRow').off('click');
            $('.insertRow').click(function(){
                    var $table = $('#claim_item_table');
                    var old_count = 0;
                    if (claim.items.row_count > 2 && claim.items.row_count < 20) {
                        $("html, body").animate({scrollTop: "430px"});
                    }
                    var clonedRow = $table.find('tbody tr#claim_row_0').clone();

                    $(clonedRow).attr('id', 'claim_row_' + claim.items.row_count);
                    $(clonedRow).removeClass('discounted_row');

                    clonedRow = claim.items.setupNewRowIds(clonedRow);
                   
                    var addId = this.id.split('_');
                    $(clonedRow).clone().insertAfter('tbody tr#claim_row_'+addId[1]);
 
                    claim.items.updateClaimTableSeq();

                    //$table.find('tbody').append($(clonedRow));
                    $('label.error', $(clonedRow)).remove();

                    claim.items.drawNewHiddenRowItem('id');
                    claim.items.drawNewHiddenRowItem('discounted');
                    claim.items.drawNewHiddenRowItem('original_fee');

                    //$('.row_count b', clonedRow).text(claim.items.row_count + 1);
                    $('#claim_claim_items_' + claim.items.row_count + '_service_code').attr('title', '');

                    $('label.error', $(clonedRow)).remove();
                    $('.error', $(clonedRow)).removeClass('error');
                    $('.error_codes', $(clonedRow)).text('');
                    $('.error_codes', $(clonedRow)).attr('title', '');
                    $('.original_fee_text', $(clonedRow)).remove();

                    $('.payments .text', $(clonedRow)).text('');
                    $('.payments .field input', $(clonedRow)).val('');
                    $('.payments .field', $(clonedRow)).hide();
                    $('.payments .text', $(clonedRow)).show();
                        
                   
                    var blurEvent = parseInt(addId[1])+parseInt(1);
                    claim.items.initialize(blurEvent); 
                    //claim.items.initialize(claim.items.row_count);
                    claim.items.row_count++;
                    $('.scrollable_div').animate({scrollTop: $('.scrollable_div').prop("scrollHeight")}, 500);
                    $('#claim_item_table tbody').animate({scrollTop: $('#claim_item_table tbody').prop("scrollHeight")}, 200);

                    var windowHeight = window.innerHeight;
                    var tableRowHeight = $('#claim_item_table tbody tr').length * 31;
                    if ((tableRowHeight + 380) > windowHeight) {
                        $('#claim_item_table tbody').css('height', 'calc(100vh - 380px)');
                    }
                    $('#claim_item_table tbody').css('max-height', tableRowHeight * 2);
                    $('#claim_recalculate').show();
            });


//            $('#claim_item_table tbody ').animate({scrollTop: $('#claim_item_table tbody').prop("scrollHeight")}, $('#claim_item_table tbody').height());
//            $("#quick_service_codes_div span").click(function () {
//                if (($('.scrollable_div').height()) > 300) {
//                    $('.scrollable_div').addClass('scrollerTable');
//                    $(window).scroll(function () {
//                        if ($(this).scrollTop() >= 300) {
//                            $('.scrollable_div').addClass('fixed-header');
//                            $('.bottomTable').addClass('fixed-footer');
//                            $('#quick_service_codes_div').addClass('fixed-footer1');
//                            $('.scrollable_div #claim_item_table thead').addClass('fixed-header');
//                        }
//                        if ($(this).scrollTop() < 100) {
//                            $('.scrollable_div').removeClass('fixed-header');
//                            $('.bottomTable').removeClass('fixed-footer');
//                            $('#quick_service_codes_div').removeClass('fixed-footer1');
//                            $('.scrollable_div #claim_item_table thead').removeClass('fixed-header');
//                        }
//                    });
//                }
//            })

//            $('.scrollable_div').mouseup(function (e) {
//                $('.scrollable_div #claim_item_table tbody td input').keypress(function () {
//                    /*Added for sticky header*/
//
//                    if (($('.scrollable_div').height()) > 300) {
//                        $('.scrollable_div').addClass('scrollerTable');
//                        $(window).scroll(function () {
//                            if ($(this).scrollTop() >= 300) {
//                                $('.scrollable_div').addClass('fixed-header');
//                                $('.bottomTable').addClass('fixed-footer');
//                                $('#quick_service_codes_div').addClass('fixed-footer1');
//                                $('.scrollable_div #claim_item_table thead').addClass('fixed-header');
//                            }
//                            if ($(this).scrollTop() < 100) {
//                                $('.scrollable_div').removeClass('fixed-header');
//                                $('.bottomTable').removeClass('fixed-footer');
//                                $('#quick_service_codes_div').removeClass('fixed-footer1');
//                                $('.scrollable_div #claim_item_table thead').removeClass('fixed-header');
//                            }
//                        });
//                    }
//
//                });
//
//
//                if ($(window).scrollTop() < 200) {
//                    $('.scrollable_div #claim_item_table').removeClass('fixed-header');
//                }
//                if (typeof (Storage) !== "undefined") {
//                    // Code for localStorage/sessionStorage.
//                    localStorage.setItem('default_height', $('.scrollable_div').height());
//                } else {
//                    // Sorry! No Web Storage support..
//                }
//
//
//
//            });

            if (localStorage.default_height) {
//                $('.scrollable_div').height(localStorage.default_height);
            }

            if (claim.direct_billing) {
                $("#claim_pay_prog option[value='HCP - P']").remove();
                $("#claim_pay_prog option[value='HCP - S']").remove();
                $("#claim_pay_prog option[value='RMB']").remove();
                $("#claim_pay_prog option[value='WCB']").remove();
                $("#claim_pay_prog option[value='THIRD PARTY']").remove();
            }

            $('#claim_split_claim').change(function () {
                if ($('#claim_split_claim').prop('checked')) {
                    $('#split_doctor_row').show();
                    localStorage.setItem('show_split', true);
                } else {
                    $('#split_doctor_row').hide();
                    localStorage.setItem('show_split', false);
                }
            });

            if (localStorage.getItem('show_split') == 'true') {
                $('#claim_split_claim').prop('checked', true);
                $('#split_doctor_row').show();
            }
            claim.items.claimLock();
        },
        updateClaimTableSeq:function(){
            var i=0;
            $('#claim_item_table tbody tr').each(function() {
                var srno = i+1
                $(this).find(".row_count b").html(srno);
                $(this).attr('id','claim_row_'+i);
                $(this).find(".service_code input").attr('id','claim_claim_items_'+i+'_service_code');
                $(this).find(".service_code input").attr('name','claim[claim_items]['+i+'][service_code]');

                $(this).find(".diag_code input").attr('id','claim_claim_items_'+i+'_diag_code');
                $(this).find(".diag_code input").attr('name','claim[claim_items]['+i+'][diag_code]');

                $(this).find(".base_fee input").attr('id','claim_claim_items_'+i+'_base_fee');
                $(this).find(".base_fee input").attr('name','claim[claim_items]['+i+'][base_fee]');

                $(this).find(".num_serv input").attr('id','claim_claim_items_'+i+'_num_serv');
                $(this).find(".num_serv input").attr('name','claim[claim_items]['+i+'][num_serv]');

                $(this).find(".modifier input").attr('id','claim_claim_items_'+i+'_modifier');
                $(this).find(".modifier input").attr('name','claim[claim_items]['+i+'][modifier]');

                $(this).find(".fee_subm input").attr('id','claim_claim_items_'+i+'_fee_subm');
                $(this).find(".fee_subm input").attr('name','claim[claim_items]['+i+'][fee_subm]');

                $(this).find(".service_date input").attr('id','claim_claim_items_'+i+'_service_date');
                $(this).find(".service_date input").attr('name','claim[claim_items]['+i+'][service_date]');

                $(this).find(".fee_paid input").attr('id','claim_claim_items_'+i+'_fee_paid');
                $(this).find(".fee_paid input").attr('name','claim[claim_items]['+i+'][fee_paid]');

                $(this).find(".insertRow").attr('id','add_'+i+'');
                $(this).find(".removeRow").attr('id','clear_'+i+'');
                i++;
            });
        },
        claimLockData:function(){
                $('.claim_unlock').show(); 
                $('.claim_lock').hide(); 
                if(claim.items.claimlockdata) {
                        var claimLock = Object.values(claim.items.claimlockdata);
                        $(claimLock).each(function( index, value ) {
                            if(value['is_lock']  == 1)
                            {  
                              $('#'+value['lock_primary_id']).attr('id',value['lock_key']); 
                              $('.'+value['lock_key']).show(); 
                              $('.'+value['lock_primary_id']+''+'_unlock').hide(); 

                              var valueLock = value['lock_value'];
                              if( typeof valueLock != 'undefined' || valueLock != null ){
                              //if (valueLock  != "undefined")
                              //{
                                 valueLock = value['lock_value'];
                              } else {
                                 valueLock = '';
                              }
                              //alert(value['lock_primary_id']);
                              if(value['lock_primary_id'] == 'claim_manual_review')
                              {     
                                if(valueLock == 'true') {
                                      //alert(1);
                                      $(".claim_manual_review").prop('checked',true);  
                                } else {
                                      //alert(2);
                                      $(".claim_manual_review").prop('checked',false);     
                                }
                              } else if(value['lock_primary_id'] == 'claim_facility_description')  {
                                    //alert(value['lock_value']);
                                    var str = valueLock;
                                    if( typeof str != 'undefined' || str != null ){
                                    //if(str != "undefined") {
                                           var splitFacNum = str.split(" ")[0];
                                            $('#claim_facility_num').val(splitFacNum);
                                            $('#'+value['lock_key']).val(valueLock);
                                    }
                              } else if(value['lock_primary_id'] == 'claim_ref_doc_description')  {
                                    //alert(value['lock_value']);
                                    var str = valueLock;
                                    if( typeof str != 'undefined' || str != null ){
                                    var splitFacNum = str.split(" ")[0];
                                    $('#claim_ref_doc_num').val(splitFacNum);
                                    $('#'+value['lock_key']).val(valueLock);
                                    }
                              }
                              else {
                                   ///alert(value['lock_primary_id']+'-'+value['lock_key']);
                                   $('#'+value['lock_key']).val(valueLock);
                              }
                            } 
                            else 
                            {
                               $('.'+value['lock_primary_id']+''+'_unlock').show(); 
                               $('.'+value['lock_key']).hide(); 
                               $('#'+value['lock_key']).attr('id',value['lock_primary_id']);
                            }
                        });


                 } else {
                     $('.claim_unlock').show(); 
                     $('.claim_lock').hide(); 
                 }
        },
        claimLock:function(){
            $('.claim_lock, .claim_unlock').click(function(){
                var primary_id = $(this).attr('data-primary-id');
                var data_field = $(this).attr('data-field');
                var is_lock = $(this).attr('data-lock');
                if(is_lock == 1) {

                    if(primary_id == 'claim_doctor_id') {
                        if($('#'+primary_id).val() == '') {
                            //alert('Billing md is required');
                            $('.validatelock').css('border','1px solid #a94442');
                            //$('#'+primary_id).addClass('cliponeErrorInput');
                            return false;
                        } 
                        else {
                            $('.validatelock').css('border','');
                            //$('#'+primary_id).removeClass('cliponeErrorInput');
                        }
                    }
                }
                //alert(is_lock);
                //alert($(this).attr('data-lock'));
                if(primary_id == 'claim_manual_review') {
                    //alert($(".claim_manual_review").is(':checked'));
                   // $(".claim_manual_review").prop('checked',true);     
                    var value = $(".claim_manual_review").is(':checked');
                    } else {
                    var value = $('#'+$(this).attr('data-replace-with')).val();
                }
                //alert(value);
                
                var data = 'data_field=' + data_field +'&'+'is_lock=' + is_lock+'&'+'value=' + value+'&'+'primary_id=' + primary_id;
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: claim.actions.claimlock,
                    data: data,
                    success: function (rs) {
                        // $('#spinner').hide();
                        // $('.service_code_spinner').removeClass('service_code_spinner');
                        claim.items.claimlockdata = rs;
                        claim.items.claimLockData();
                         $('#'+$(this).attr('data-replace-with')).attr('id', $(this).attr('data-replace-to'));  
                        if (rs.hasOwnProperty('data')) {
                            $('#messages_text').text('').hide();
                        } else {
                            $('#error_icon').show();
                            $('#messages_text').text('No Lock Data found').show();
                        }
                    },
                    beforeSend: function (rs) {
                        $('#checkmark').hide();
                        $('#error_icon').hide();
                        $('#spinner').show();
                        if(is_lock == 1)
                        {
                          $('#messages_text').text('Locking...').show();  
                        } else {
                          $('#messages_text').text('Unlocking...').show();  
                        }
                    },
                    complete: function (rs) {
                        $('#spinner').hide();
                    },
                    error: function (rs) {
                        $('#error_icon').show();
                        $('#messages_text').text('An error occurred while contacting the server.');
                    }
                });
            })
        },
        checkMultipleServices: function () {
            var $table = $('#claim_item_table');
            var table_rows = [];
            var c = 0;

            $table.find('tbody tr').each(function () {
                var ns_1 = $('.num_serv input', $(this)).val();

                if (ns_1 && parseInt(ns_1, 10) > 99) {
                    claim.items.splitServicesUp($(this).prop('id').replace('claim_row_', ''));
                    return claim.items.checkMultipleServices();
                }
            });

            $table.find('tbody tr').each(function () {
                table_rows[c] = $(this);
                c++;
            });

            for (var a = 0; a < table_rows.length - 1; a++) {
                var row_a = $(table_rows[a]).attr('id');

                if ($('#' + row_a).size()) {
                    var sc_1 = $('.service_code input', table_rows[a]).val();
                    var dc_1 = $('.diag_code input', table_rows[a]).val();
                    var sd_1 = $('.service_date input', table_rows[a]).val();
                    var ns_1 = parseInt($('.num_serv input', table_rows[a]).val(), 10);

                    if (sc_1 || dc_1 || sd_1) {
                        for (var b = a + 1; b < table_rows.length; b++) {
                            var row_b = $(table_rows[b]).attr('id');
                            if ($('#' + row_b).size()) {
                                var sc_2 = $('.service_code input', table_rows[b]).val();
                                var dc_2 = $('.diag_code input', table_rows[b]).val();
                                var sd_2 = $('.service_date input', table_rows[b]).val();
                                var ns_2 = parseInt($('.num_serv input', table_rows[b]).val(), 10);

                                if (sc_2 == sc_1 && sd_2 == sd_1 && (ns_2 + ns_1 < 99)) {
                                    if (dc_1 == dc_2) {
                                        claim.items.mergeRows(row_a, row_b);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        },
        checkNumServices: function (c) {
            var $numServices = $('#' + this.form_prefix + c + '_num_serv');
            var $baseFee = $('#' + this.form_prefix + c + '_base_fee');
            var $modifier = $('#' + this.form_prefix + c + '_modifier');
            var $feeSubm = $('#' + this.form_prefix + c + '_fee_subm');

            var num_serv = parseInt($numServices.val(), 10);
            var base_fee = parseFloat($baseFee.val());
            var modifier = parseInt($modifier.val(), 10);
            var doctorModifier = parseInt(claim.services.determineModifier(), 10);

            if (isNaN(num_serv)) {
                num_serv = 1;
            }
            if (!num_serv && base_fee) {
                num_serv = 1;
            }
            if (!modifier) {
                modifier = doctorModifier;
            }
            if (num_serv * doctorModifier != modifier && (modifier % doctorModifier == 0)) {
                modifier = num_serv * doctorModifier;
            }

            $numServices.val(num_serv);
            $modifier.val(modifier);
            $feeSubm.val(number_format(base_fee * modifier / 100, 2, '.', ''));

            claim.calculate.totalBilled(true);
            claim.calculate.premiumCodes();
        },
        checkRowCount: function () {
            if (!claim.items.isLastRowEmpty()) {
                claim.items.drawNewRow();
            }
        },
        clearRow: function (row_count) {

            var $row = $('#claim_row_' + row_count);
            if (row_count == '0') {
                $('#claim_row_' + row_count + ' input').each(function () {
                    if ($(this).attr('type') != 'button') {
                        $(this).val('');
                    }
                });
                $('#' + this.form_prefix + row_count + '_base_fee').val('');
                $('#' + this.form_prefix + row_count + '_id').val('');
                $('#' + this.form_prefix + row_count + '_status').val('');
                $('#' + this.form_prefix + row_count + '_discounted').val('');
                $('#' + this.form_prefix + row_count + '_original_fee').val('');
                $('#' + this.form_prefix + row_count + '_service_date').skDP('setDate', new Date());

                $row.removeClass('discounted_row');
                $row.find('.original_fee_text').remove();
                $row.find('.error_codes').text('');
                $row.find('.payments .text').text('');
                $row.find('.payments .field input').val('');
            } else {
                $row.remove();
                $('#' + this.form_prefix + row_count + '_base_fee').remove();
                $('#' + this.form_prefix + row_count + '_id').remove();
                $('#' + this.form_prefix + row_count + '_status').remove();
                $('#' + this.form_prefix + row_count + '_discounted').remove();
                $('#' + this.form_prefix + row_count + '_original_fee').remove();
            }

            claim.calculate.totalBilled(true);
            claim.calculate.premiumCodes();
        },
        drawNewHiddenRowItem: function (name) {
            var $original = $('#' + this.form_prefix + '0_' + name);

            var field = $original.clone();

            $(field).attr('id', $(field).attr('id').replace('_0_', '_' + claim.items.row_count + '_'));
            $(field).attr('name', $(field).attr('name').replace('[0]', '[' + claim.items.row_count + ']'));
            $(field).val('');

            $original.parent().append(field);
        },
        drawNewRow: function () {
            var $table = $('#claim_item_table');
            var old_count = 0;
            if (claim.items.row_count > 2 && claim.items.row_count < 20) {
                $("html, body").animate({scrollTop: "430px"});
//                $("html, body").animate({scrollTop: $(document).height() * 0.4});
            }

            var clonedRow = $table.find('tbody tr#claim_row_0').clone();
            $(clonedRow).attr('id', 'claim_row_' + claim.items.row_count);
            $(clonedRow).removeClass('discounted_row');

            clonedRow = claim.items.setupNewRowIds(clonedRow);
            $table.find('tbody').append($(clonedRow));

            $('label.error', $(clonedRow)).remove();

            this.drawNewHiddenRowItem('id');
            this.drawNewHiddenRowItem('discounted');
            this.drawNewHiddenRowItem('original_fee');

            //$('.row_count b', clonedRow).text(claim.items.row_count + 1);
            claim.items.updateClaimTableSeq();
            $('#claim_claim_items_' + claim.items.row_count + '_service_code').attr('title', '');

            $('label.error', $(clonedRow)).remove();
            $('.error', $(clonedRow)).removeClass('error');
            $('.error_codes', $(clonedRow)).text('');
            $('.error_codes', $(clonedRow)).attr('title', '');
            $('.original_fee_text', $(clonedRow)).remove();

            $('.payments .text', $(clonedRow)).text('');
            $('.payments .field input', $(clonedRow)).val('');
            $('.payments .field', $(clonedRow)).hide();
            $('.payments .text', $(clonedRow)).show();

            claim.items.initialize(claim.items.row_count);
            claim.items.row_count++;
            $('.scrollable_div').animate({scrollTop: $('.scrollable_div').prop("scrollHeight")}, 500);
//            var target = $('#claim_item_table tbody');
            $('#claim_item_table tbody').animate({scrollTop: $('#claim_item_table tbody').prop("scrollHeight")}, 200);

            var windowHeight = window.innerHeight;
            var tableRowHeight = $('#claim_item_table tbody tr').length * 31;
            if ((tableRowHeight + 380) > windowHeight) {
                $('#claim_item_table tbody').css('height', 'calc(100vh - 380px)');
            }
            $('#claim_item_table tbody').css('max-height', tableRowHeight * 2);
            $('#claim_recalculate').show();
//            $('#claim_item_table tbody').css('height', 'calc(100vh - 380px)');
//                target.scrollTop = target.scrollHeight;
        },
        formatFeeSubm: function (c) {
            var $field = $('#' + claim.items.form_prefix + c + '_fee_subm');
            var value = parseFloat($field.val());
            if (value) {
                $field.val(number_format(value, 2));
            }
        },
        getEmptyDiagCodes: function () {
            var rows = [];

            for (var a = 0; a <= claim.items.row_count; a++) {
                if (!claim.items.isRowEmpty(a) && !$('#' + claim.items.form_prefix + a + '_diag_code').val()) {
                    rows[rows.length] = a;
                }
            }
            return rows;
        },
        getFirstEmptyRow: function (a) {
            if (a == null) {
                a = 0;
            }

            for (; a <= claim.items.row_count; a++) {
                if ($('#claim_row_' + a).size()) {
                    if (claim.items.isRowEmpty(a)) {
                        if (a >= claim.items.row_count) {
                            claim.items.drawNewRow();
                        }
                        return a;
                    }
                }
            }
            claim.items.drawNewRow();
            return a;
        },
        handleServiceDate: function (row_id, dates) {
            if (dates != null) {
                if (dates.length > 1) {
                    dates.sort(quickClaim.sortDate);
                    $('#' + claim.items.form_prefix + row_id + '_service_date').skDP('update', $.datepick.formatDate(quickClaim.dateFormat, dates[0]));

                    claim.items.checkRowCount();
                    for (var a = 1; a < dates.length; a++) {
                        var new_id_idx = claim.items.getFirstEmptyRow(row_id);

                        $('#claim_row_' + row_id + ' input').each(function () {
                            var new_field = this.id.replace('_' + row_id + '_', '_' + new_id_idx + '_');
                            $('#' + new_field).val($(this).val());
                        });
                        $('#' + claim.items.form_prefix + new_id_idx + '_service_date').skDP('update', $.datepick.formatDate(quickClaim.dateFormat, dates[a]));
                        claim.items.checkRowCount();
                    }
                }
            } else {
                $('#' + claim.items.form_prefix + row_id + '_service_date').skDP('setDate', new Date());

            }

            claim.calculate.premiumCodes();
        },
        isLastRowEmpty: function () {
            var id = $('tr.claim_item_row:last').attr('id').replace('claim_row_', '');
            return this.isRowEmpty(id);
        },
        isRowEmpty: function (a) {
            var rs = true;
            $('#claim_row_' + a + ' input').each(function () {
                if ($(this).attr('type') != 'button' && $(this).val() && $(this).attr('id') != 'claim_claim_items_' + a + '_service_date' && $(this).attr('id') != 'claim_claim_items_' + a + '_modifier') {
                    rs = false;
                }
            });
            return rs;
        },
        lastServiceDate: function () {
            if (claim.autofill.useServiceDate()) {
                return claim.items.previousServiceDate();
            }
            return null;
        },
        previousServiceDate: function () {
            for (var a = claim.items.row_count; a >= 0; a--) {
                var $serviceDate = $('#' + this.form_prefix + a + '_service_date');
                if ($serviceDate.size()) {
                    if (a > 0) {
                        var v = moment($('#' + this.form_prefix + (a - 1) + '_service_date').skDP('getDate')).format(global_js.getNewMomentFormat());
                    } else {
                        var v = moment($('#' + this.form_prefix + a + '_service_date').skDP('getDate')).format(global_js.getNewMomentFormat());
                    }
                    if(claim.appointment_search == true) {
                        v = $('#claim_claim_items_0_service_date').val();
                    }
                    if (v != 'Invalid date') {
                        return v;
                    }
                }
            }
            return null;
        },
        mergeRows: function (a, b) {
            var $rowA = $('#' + a);
            var $rowB = $('#' + b);

            var ns_1 = parseFloat($rowA.find('.num_serv input').val());
            var ns_2 = parseFloat($rowB.find('.num_serv input').val());

            var tb_1 = parseFloat($rowA.find('.fee_subm input').val());
            var tb_2 = parseFloat($rowB.find('.fee_subm input').val());

            var mod_1 = parseInt($rowA.find('.modifier input').val());
            var mod_2 = parseInt($rowB.find('.modifier input').val());

            var row_number = b.replace('claim_row_', '');

            $rowA.find('.num_serv input').val(ns_1 + ns_2);
            $rowA.find('.fee_subm input').val(number_format(tb_1 + tb_2, 2));
            $rowA.find('.modifier input').val(mod_1 + mod_2);

            claim.items.clearRow(row_number);
        },
        rowDown: function (count) {
            for (count++; count < claim.items.row_count; count++) {
                var $serviceCode = $('#' + claim.items.form_prefix + count + '_service_code');
                if ($serviceCode.size()) {
                    $serviceCode.focus();
                    return;
                }
            }
        },
        rowUp: function (count) {
            for (count--; count >= 0; count--) {
                var $serviceCode = $('#' + claim.items.form_prefix + count + '_service_code');
                if ($serviceCode.size()) {
                    $serviceCode.focus();
                    return;
                }
            }
        },
        setupNewRowIds: function (row) {
            var old = 0;
            $('input', row).each(function (e) {
                var id = this.id.replace('_' + old + '_', '_' + claim.items.row_count + '_');
                var name = $(this).attr('name').replace('[' + old + ']', '[' + claim.items.row_count + ']');

                $(this).attr('id', id);
                $(this).attr('name', name);
                if ($(this).attr('type') != 'button') {
                    $(this).val('');
                }
            });

            $('.hasDatepick', row).each(function () {
                $(this).removeClass('hasDatepick');
            });
            $('#clear_' + old, row).attr('id', 'clear_' + claim.items.row_count);
            $('#add_' + old, row).attr('id', 'add_' + claim.items.row_count);
            return row;
        },
        splitServicesUp: function (row) {
            claim.items.drawNewRow();
            var new_row = claim.items.getFirstEmptyRow(row);

            var $originalNumServ = $('#claim_claim_items_' + row + '_num_serv');
            var $originalFeeSubm = $('#claim_claim_items_' + row + '_fee_subm');
            var $originalModifier = $('#claim_claim_items_' + row + '_modifier');
            var $originalServiceCode = $('#claim_claim_items_' + row + '_service_code');
            var $originalDiagCode = $('#claim_claim_items_' + row + '_diag_code');
            var $originalBaseFee = $('#claim_claim_items_' + row + '_base_fee');
            var $originalServiceDate = $('#claim_claim_items_' + row + '_service_date');
            var $originalStatus = $('#claim_claim_items_' + row + '_status');
            var $originalDiscounted = $('#claim_claim_items_' + row + '_discounted');
            var $originalOriginalFee = $('#claim_claim_items_' + row + '_original_fee');

            var $newNumServ = $('#claim_claim_items_' + new_row + '_num_serv');
            var $newFeeSubm = $('#claim_claim_items_' + new_row + '_fee_subm');
            var $newModifier = $('#claim_claim_items_' + new_row + '_modifier');
            var $newServiceCode = $('#claim_claim_items_' + new_row + '_service_code');
            var $newDiagCode = $('#claim_claim_items_' + new_row + '_diag_code');
            var $newBaseFee = $('#claim_claim_items_' + new_row + '_base_fee');
            var $newServiceDate = $('#claim_claim_items_' + new_row + '_service_date');
            var $newStatus = $('#claim_claim_items_' + new_row + '_status');
            var $newDiscounted = $('#claim_claim_items_' + new_row + '_discounted');
            var $newOriginalFee = $('#claim_claim_items_' + new_row + '_original_fee');

            var ns = parseInt($originalNumServ.val(), 10);
            var fee_total = parseFloat($originalFeeSubm.val());
            var modifier = parseInt($originalModifier.val(), 10);

            var ns_2 = ns - 99;
            var ns_1 = ns - ns_2;

            var total_2 = (ns_2 / ns) * fee_total;
            var total_1 = (ns_1 / ns) * fee_total;

            var mod_2 = (ns_2 / ns) * modifier;
            var mod_1 = (ns_1 / ns) * modifier;

            $newServiceCode.val($originalServiceCode.val());
            $newDiagCode.val($originalDiagCode.val());
            $newBaseFee.val($originalBaseFee.val());
            $newServiceDate.val($originalServiceDate.val());
            $newStatus.val($originalStatus.val());
            $newDiscounted.val($originalDiscounted.val());
            $newOriginalFee.val($originalOriginalFee.val());

            $originalNumServ.val(ns_1);
            $newNumServ.val(ns_2);

            $originalFeeSubm.val(number_format(total_1, 2, '.', ''));
            $newFeeSubm.val(number_format(total_2, 2, '.', ''));

            $originalModifier.val(parseInt(mod_1));
            $newModifier.val(parseInt(mod_2));

        },
        insertErrorCodes: function (error_codes) {
            if (error_codes.hasOwnProperty('item_errors')) {
                for (var a in error_codes['item_errors']) {
                    var text = '';
                    var title = '';

                    for (var b in error_codes['item_errors'][a]) {
                        if (text != '') {
                            text += ', ';
                        }
                        if (title != '') {
                            title += '  ';
                        }

                        text += error_codes['item_errors'][a][b];
                        title += error_codes['item_errors'][a][b] + ': ' + error_codes['error_codes'][error_codes['item_errors'][a][b]];
                    }
                    $('#claim_row_' + a).find('.error_codes')
                            .text(text + '')
                            .attr('title', title);
                }
            }
        },
        insertPayments: function (payments) {
            for (var a in payments) {
                var $row = $('#claim_row_' + a);

                $row.find('.payments .text').text(payments[a] ? ('$' + payments[a]) : '');
                $row.find('.payments .field input').val(payments[a]);

                if (claim.items.canShowFeePaid && $('#claim_claim_items_' + a + '_id').val()) {
                    $row.find('.payments .field').show();
                    $row.find('.payments .text').hide();
                } else {
                    $row.find('.payments .field').hide();
                    $row.find('.payments .text').show();
                }
            }
        }
    },
    services: {
        cache: {},
        quick_codes: {},

        code_types: {},
        discounts: {},
        tax_cache: {},

        default_suffix: 'A',
        fill_down_diag_code: true,

        determineBaseFee: function (service_code, mode) {
            if (!service_code) {
                return null;
            }
            var fee = 0;

            if (mode == 'A' || mode == '') {
                if (claim.doctor.isSpecialist()) {
                    fee = service_code['fee_specialist'];
                    if (!parseFloat(fee, 10)) {
                        fee = service_code['fee_general'];
                    }
                } else {
                    fee = service_code['fee_general'];
                    if (!parseFloat(fee, 10)) {
                        fee = service_code['fee_specialist'];
                    }
                }
            } else if (mode == 'B') {
                fee = service_code['fee_assistant'];
            } else if (mode == 'C') {
                if (claim.doctor.isAnaesthesiologist()) {
                    fee = service_code['fee_ana'];
                    if (!parseFloat(fee, 10)) {
                        fee = service_code['fee_non_ana'];
                    }
                } else {
                    fee = service_code['fee_specialist'];

                    if (!parseFloat(fee)) {
                        fee = service_code['fee_ana'];
                    }
                }
            }
            return fee;
        },

        determineFee: function (service, row) {
            var $diagCode = $('#claim_claim_items_' + row + '_diag_code');
            var $serviceCode = $('#claim_claim_items_' + row + '_service_code');
            var $baseFee = $('#claim_claim_items_' + row + '_base_fee');
            var $numServ = $('#claim_claim_items_' + row + '_num_serv');
            var $feeSubm = $('#claim_claim_items_' + row + '_fee_subm');
            var $modifier = $('#claim_claim_items_' + row + '_modifier');
            var $discounted = $('#claim_claim_items_' + row + '_discounted');
            var $originalFee = $('#claim_claim_items_' + row + '_original_fee');
            var $serviceDate = $('#claim_claim_items_' + row + '_service_date');

            if (service.hasOwnProperty('is_direct') && service['is_direct']) {
                service.mode = '';
            } else if (service.hasOwnProperty('service_code') && service.service_code.length == '5') {
                service.mode = service.service_code.charAt(4);
                service.service_code = service.service_code.substr(0, 4);
            }

            var service_code = claim.services.cache[service.service_code];
            var diag_code = $diagCode.val();



            if (service.facility) {
                claim.facility.fillInFacility(service.facility)
            }
            
            if (service.sli) 
            {
                $('#claim_sli').val(service.sli);
            }

            if (!diag_code) {
                diag_code = service.diag_code;
            }
            if (!diag_code) {
                diag_code = claim.services.getPreviousDiagCode();
            }

            if (!(service.mode == 'A' || service.mode == 'B' || service.mode == 'C')) {
                service.mode = claim.services.default_suffix;
            }
            if (service['is_direct']) {
                service.mode = '';
            }

            $serviceCode.val(service.service_code + service.mode);
            $serviceCode.closest('tr').removeClass('discounted_row');
            $serviceCode.attr('title', service.description);

            if (service_code['sc_type'] == claim.services.code_types['normal'] || service_code['sc_type'] == claim.services.code_types['do_not_change']) {
                var modifier = claim.services.determineModifier();
                var fee = $baseFee.val();
                var newBaseFee = claim.services.determineBaseFee(service_code, service.mode);
                if (parseFloat(newBaseFee)) {
                    if (fee == '' || fee == 0.00 || fee != newBaseFee) {
                        fee = newBaseFee;
                        //$serviceCode.select();
                    }
                }

                var units = claim.services.determineUnits(service_code, service.mode);
                var percentage = modifier * units;

                if ($numServ.val()) {
                    units = $numServ.val();
                } else if (service.num_serv) {
                    units = service.num_serv;
                }

                if (service.hasOwnProperty('percentage') && service.percentage != 100) {
                    percentage = service.percentage;
                }

                $feeSubm.val(number_format(fee * percentage / 100, 2, '.', ''));
                $baseFee.val(fee);

                $numServ.val(units);
                $modifier.val(percentage);
                if (diag_code) {
                    $diagCode.val(diag_code);
                }
                $discounted.val('');
                $originalFee.val('');
                $('.original_fee_text', $('#claim_row_' + row)).remove();
                if (!$serviceDate.val() && claim.autofill.useServiceDate()) {
                    $serviceDate.skDP('setDate', claim.items.lastServiceDate());
                } else if (!$serviceDate.val()) {
                    $serviceDate.skDP('setDate', claim.items.previousServiceDate(row));
                }

                if (service_code['is_direct'] && claim.services.discounts.hasOwnProperty(service.service_code)) {
                    var dateCreated = $('#claim_date_created').val();
                    var idx = claim.services.discounts[service.service_code];

                    var date = new Date();
                    date = date.toString('yyyy-MM-dd');
                    if (dateCreated) {
                        date = dateCreated;
                    }

                    for (var a in idx) {
                        if (idx[a]['start_date'] <= date && date <= idx[a]['end_date']) {
                            var original_fee = fee;
                            var text = '<div class="original_fee_text">Original cost: $' + number_format(original_fee, 2, '.', ',') + '</div>';
                            fee = fee - (idx[a].rate * fee);

                            $feeSubm.val(number_format(fee * units, 2, '.', ''));
                            $baseFee.val(number_format(fee * units, 2, '.', ','));
                            $discounted.val(1);
                            $originalFee.val(original_fee);

                            $feeSubm.parent().append($(text));
                            $baseFee.closest('tr').addClass('discounted_row');
                            break;
                        }
                    }
                }
            } else {
                if (diag_code) {
                    $diagCode.val(diag_code);
                }
                claim.calculate.premiumCodes();
            }
            claim.items.checkRowCount(row);
        },
        determineUnits: function (service_code, mode) {
            var units = 1;
            var fee = null;

            if (mode == 'B') {
                units = service_code['units_assistant'];
            } else if (mode == 'C') {
                if (!claim.doctor.isAnaesthesiologist() && !claim.doctor.isBillAsAna()) {
                    fee = service_code['fee_non_ana'];
                    units = service_code['units_non_ana'];

                    if (!parseFloat(fee, 10)) {
                        units = service_code['units_ana'];
                    }
                } else {
                    fee = service_code['fee_ana'];
                    units = service_code['units_ana'];

                    if (!parseFloat(fee, 10)) {
                        units = service_code['units_non_ana'];
                    }
                }
            }
            return units;
        },
        determineModifier: function () {
            var doctorID = $('#claim_doctor_id').val();
            var modifier = 100;

            if (claim.doctor.modifiers.hasOwnProperty(doctorID)) {
                modifier = claim.doctor.modifiers[doctorID];
            }
            return modifier;
        },
        directCodeLookup: function (id) {
            var first_row = claim.items.getFirstEmptyRow();
            $('#claim_claim_items_' + first_row + '_diag_code').focus();

            if (claim.services.cache.hasOwnProperty(id)) {
                return claim.services.determineFee(claim.services.cache[id], first_row);
            }
        },
        handleCodeLookup: function (id) {
            var service_code = $('#claim_claim_items_' + id + '_service_code').val().toUpperCase();

            if (!service_code) {
                return;
            }   
            var services = {};
            var is_quick_code = claim.services.isQuickCode(service_code);
            if (is_quick_code) {
                services = claim.services.getQuickCode(service_code);
            } else {

                services = claim.services.getServiceCode(service_code, id);
            }

            for (var a in services) {
                if (services[a].service_code) {
                    claim.services.determineFee(services[a], id);
                    id = claim.items.getFirstEmptyRow(id);
                }
            }
            claim.calculate.totalBilled(true);
            $('#claim_claim_items_' + id + '_service_date').skDP('update');
        },
        handleQuickCodeClick: function (code) {

            if (!claim.services.quick_codes.hasOwnProperty(code)) {
                return false;
            }
            var first_row = claim.items.getFirstEmptyRow();
            $('#claim_claim_items_' + first_row + '_service_code').val(code);
            claim.services.handleCodeLookup(first_row);
            var focusRow = $("#claim_item_table tbody tr").length - 2;
            $("#claim_claim_items_" + focusRow + "_diag_code").focus();
        },
        isInCache: function (service_code) {
            return claim.services.cache.hasOwnProperty(service_code);
        },
        isPercentageAllCode: function (service_code) {
            if (service_code.length > 4) {
                service_code = service_code.substring(0, 4);
            }
            if (claim.services.cache.hasOwnProperty(service_code)) {
                var serv = claim.services.cache[service_code];

                if (serv['sc_type'] == claim.services.code_types['percentage_all']) {
                    return true;
                }
                if (serv['sc_type'] == claim.services.code_types['percentage_one']) {
                    return true;
                }
                if (serv['sc_type'] == claim.services.code_types['percentage_prefix']) {
                    return true;
                }
            }
            return false;
        },
        isPremiumCode: function (service_code) {
            if (service_code.length > 4) {
                service_code = service_code.substring(0, 4);
            }
            if (claim.services.cache.hasOwnProperty(service_code)) {
                var serv = claim.services.cache[service_code];

                if (serv['sc_type'] == claim.services.code_types['normal']) {
                    return false;
                }
                if (serv['sc_type'] == claim.services.code_types['do_not_change']) {
                    return false;
                }
            }
            return true;
        },
        isQuickCode: function (service_code) {
            return claim.services.quick_codes.hasOwnProperty(service_code);
        },
        fillDiagCode: function (row_num) {
            if (row_num == null) {
                row_num = 0;
            }
            var a;
            var diag_code = $('#claim_claim_items_' + row_num + '_diag_code').val();

            for (a = row_num + 1; a <= claim.items.row_count; a++) {
                var $diagCode = $('#claim_claim_items_' + a + '_diag_code');
                if ($diagCode.size() && !claim.items.isRowEmpty(a)) {
                    if (!$diagCode.val()) {
                        $diagCode.val(diag_code);
                    } else if (!diag_code) {
                        diag_code = $diagCode.val();
                    }
                }
            }
        },
        padServiceCode: function (sc) {
            sc = $.trim(sc);
            var mode = '';

            if (sc.length > 1) {
                mode = sc.charAt(sc.length - 1);
                if (mode == 'A' || mode == 'B' || mode == 'C') {
                    sc = sc.substr(0, sc.length - 1);
                } else {
                    mode = '';
                }
            }
            if (sc.length < 4) {
                while (sc.length != 4) {
                    sc = sc.charAt(0) + '0' + sc.substr(1, sc.length);
                }
            }

            return {code: sc, mode: mode};
        },
        getPreviousDiagCode: function () {
            var rs = '';
            if (claim.services.fill_down_diag_code) {
                var prev_row = null;
                var count = claim.items.row_count;// - 1;

                for (count--; count >= 0; count--) {
                    if ($('#' + claim.items.form_prefix + count + '_service_code').size() && $('#' + claim.items.form_prefix + count + '_diag_code').val()) {
                        prev_row = count;
                        count = 0;
                    }
                }

                var diag_code = $('#' + claim.items.form_prefix + prev_row + '_diag_code').val();
                if (prev_row != null && diag_code) {
                    return diag_code;
                }
            }
            return rs;
        },
        getQuickCode: function (service_code) {
            var rs = {};

            var service = claim.services.quick_codes[service_code];
            for (var a in service['codes']) {
                rs[a] = {
                    service_code: service['codes'][a],
                    percentage: service['percentages'][a],
                    num_serv: service['num_servs'][a],
                    diag_code: service.diag_code,
                    sli: service.sli,
                    facility: service.facility
                };
            }

            return rs;
        },
        getServiceCode: function (service_code, row_num) {
            var services = {};
            service_code = claim.services.padServiceCode(service_code);

            var mode = service_code.mode;
            service_code = service_code.code;

            if (claim.services.isInCache(service_code)) {
                services[0] = claim.services.cache[service_code];

                var ns = parseInt($('#claim_claim_items_' + row_num + '_num_serv').val());
                var per = parseInt($('#claim_claim_items_' + row_num + '_modifier').val());
                if (!per) {
                    per = 100;
                }

                services[0].percentage = per;
                services[0].mode = mode;
                services[0].num_serv = ns;
                return services;
            }

            claim.services.retrieveServiceCode(service_code, row_num);
        },
        retrieveServiceCode: function (service_code, row_num) {
            if (quickClaim.serviceCodeLocked) {
                return;
            }
            var data = 'service_code=' + service_code;

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: claim.actions.service_code_lookup,
                data: data,
                success: function (rs) {
                    if (rs.hasOwnProperty('data')) {
                        $('#messages_text').text('').hide();

                        if (!rs.data.length) {
                            claim.services.cache[service_code] = {};
                        }

                        for (var a in rs.data) {
                            claim.services.cache[rs.data[a].service_code] = rs.data[a];

                        }
                        if (row_num || row_num === 0) {
                            claim.services.handleCodeLookup(row_num);
                        }
                    } else {
                        $('#error_icon').show();
                        $('#messages_text').text('No Service Code Data found').show();
                    }
                },
                beforeSend: function (rs) {
                    quickClaim.serviceCodeLocked = true;
                    $('#checkmark').hide();
                    $('#error_icon').hide();
                    $('#spinner').show();

                    $('#messages_text').text('Retrieving Service Code Data').show();
                    $('#claim_claim_items_' + row_num + '_service_code').addClass('service_code_spinner');
                },
                complete: function (rs) {
                    quickClaim.serviceCodeLocked = false;
                    $('#spinner').hide();
                    $('.service_code_spinner').removeClass('service_code_spinner');
                },
                error: function (rs) {
                    $('#error_icon').show();
                    $('#messages_text').text('An error occurred while contacting the server.');
                }
            });
        },
        serviceCodeLookup: function (id, row_num) {
            claim.services.handleCodeLookup(row_num);
        },
        updateModifiers: function (oldModifier) {
            var row_num;
            for (row_num = 0; row_num <= claim.items.row_count; row_num++) {
                if ($('#claim_row_' + row_num).size() && $('#claim_claim_items_' + row_num + '_service_code').val()) {
                    var $modifier = $('#claim_claim_items_' + row_num + '_modifier');

                    var currentModifier = parseFloat($modifier.val());
                    var numServices = parseFloat($('#claim_claim_items_' + row_num + '_num_serv').val());
                    var doctorModifier = claim.services.determineModifier();

                    if ((currentModifier / numServices) == oldModifier) {
                        var newModifier = doctorModifier * numServices;

                        var baseFee = $('#claim_claim_items_' + row_num + '_base_fee').val();

                        $modifier.val(newModifier);
                        $('#claim_claim_items_' + row_num + '_fee_subm').val(number_format(baseFee * newModifier / 100, 2));
                    }
                }
            }
        },
        updateBaseFees: function () {
            var row_number;
            for (row_number = 0; row_number <= claim.items.row_count; row_number++) {
                var service = $('#claim_claim_items_' + row_number + '_service_code').val();

                if ($('#claim_row_' + row_number).size() && service) {
                    var modifier = parseFloat($('#claim_claim_items_' + row_number + '_modifier').val());

                    var mode = service.charAt(service.length - 1);
                    service = service.substr(0, service.length - 1);
                    if (!claim.services.cache.hasOwnProperty(service)) {
                        claim.services.retrieveServiceCode(service);
                    }

                    if (claim.services.cache.hasOwnProperty(service)) {
                        var service_code = claim.services.cache[service];
                        if (service_code['sc_type'] == claim.services.code_types['normal'] || service_code['sc_type'] == claim.services.code_types['do_not_change']) {
                            var fee = $('#claim_claim_items_' + row_number + '_base_fee').val();
                            var newBaseFee = claim.services.determineBaseFee(claim.services.cache[service], mode);
                            if (parseFloat(newBaseFee, 10)) {
                                fee = newBaseFee;
                            }

                            if (fee) {
                                $('#claim_claim_items_' + row_number + '_base_fee').val(number_format(fee, 2));
                                $('#claim_claim_items_' + row_number + '_fee_subm').val(number_format(fee * modifier / 100, 2));
                            }
                        } else {
                            claim.calculate.premiumCodes();
                        }
                    }
                }
            }
        },
        updateTitles: function () {
            var row_number;
            for (row_number = 0; row_number <= claim.items.row_count; row_number++) {
                if ($('#claim_row_' + row_number).size()) {
                    var service = $('#claim_claim_items_' + row_number + '_service_code').val();
                    var service_no_mode = service.substring(0, service.length - 1);

                    if (claim.services.cache.hasOwnProperty(service)) {
                        $('#claim_claim_items_' + row_number + '_service_code').attr('title', claim.services.cache[service].description);
                    } else if (claim.services.cache.hasOwnProperty(service_no_mode)) {
                        $('#claim_claim_items_' + row_number + '_service_code').attr('title', claim.services.cache[service_no_mode].description);
                    }
                }
            }
        }
    },

    facility: {
        actions: {
            lookup: null
        },
        cache: [],
        data: {},
        ajaxLock: false,

        initialize: function () {
            for (var a in claim.facility.data) {
                claim.facility.cache[claim.facility.cache.length] = claim.facility.data[a].toString;
            }

            facilitiesList.isDialog = true;
            var $description = $('#claim_facility_description, #claim_facility_description_lock');

            $description.autocomplete({
                minLength: 1,
                select: function (event, ui) {
                    var facilityNum = ui.item.label.substring(0, 4);
                    claim.facility.fillInFacility(facilityNum);
                    $("#claim_admit_date").show();
                    event.stopPropagation();
                },
                source: claim.facility.cache
                //source: function (request, response) {
                //     $.ajax({
                //         url: '/setup_ohip/autocomplete',
                //         dataType: 'json',
                //         data: request,
                //         success: function (data) {
                //             refdoc_cache[request.term] = data;
                //             response(data);
                //         }
                //     });
                //}
            });

            $description.blur(function () {
                // var facilityNum = $(this).val().substring(0, 4);
                // if ($(this).val() != '') {
                //     $("#claim_admit_date").show();
                // } else {
                //     $("#claim_admit_date").hide();
                // }

                //claim.facility.fillInFacility(facilityNum);
            });

            $('#facility_num_dialog').dialog({
                closeOnEscape: true,
                autoOpen: false,
                modal: true,
                height: $(window).height() - 300,
                width: $(window).width() - 500,
                open: function () {
                    facilitiesList.initialize();
                    var tableId = 'facilitiesTable';
                    if ($.fn.DataTable.isDataTable("#" + tableId)) {
                        $("#facilitiesSearchButton").trigger('click');
                    }
                }
            });

            $('#search_facility')
                    .button()
                    .click(function () {
                        $('#facility_num_dialog').dialog('open');
                    });
        },
        getDorctorQSC: function(doctor_id)
        {
            $("#quick_service_codes_div span").each(function(){
                var doctor_ids = $(this).attr('doctor-ids');
                var doctorArrs = doctor_ids.split(',');
                if($.inArray(doctor_id, doctorArrs) !== -1)
                {
                    if(doctor_id != '' && doctor_id != undefined && doctor_id != null)
                    {
                         $(this).show();
                         console.log('doctor_ids',doctorArrs);
                    }
                   
                } else {
                    $(this).hide();
                }
            }); 
        },
        getDorctorDSC: function(doctor_id)
        {
            $("#direct_service_codes_div span").each(function(){
                var doctor_ids = $(this).attr('doctor-ids');
                var doctorArrs = doctor_ids.split(',');
                if($.inArray(doctor_id, doctorArrs) !== -1)
                {
                    if(doctor_id != '' && doctor_id != undefined && doctor_id != null)
                    {
                        $(this).show();
                        console.log('doctor_ids',doctorArrs);
                    }
                } else {
                    $(this).hide();
                }
            }); 
        },
        fillInFacility: function (facilityNum, next_step = true) {
            if (claim.facility.data.hasOwnProperty(facilityNum)) {

                var d = claim.facility.data[facilityNum];
                $('#claim_facility_num').val(d.facility_num);
                $('#claim_facility_description').val(d.toString);

                claim.facility.lookup(facilityNum,next_step);
                //$('#claim_facility_id').val(d.facility_id);
                //$('#claim_facility_description').val(d.toString);
                $('#facility_num_dialog').dialog('close');
                //$("#claim_admit_date").show();
            } else {
                claim.facility.lookup(facilityNum,next_step);
            }
        },

        lookup: function (facilityNum,next_step = true) {
            if (claim.facility.ajaxLock) {
                return false;
            }
            var data = 'facilityNum=' + facilityNum;
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: claim.facility.actions.lookup,
                data: data,
                success: function (rs) {
                    if (rs.hasOwnProperty('data') && rs.data.hasOwnProperty('0')) {
                            claim.facility.data[rs.data[0].facility_num] = rs.data[0];
                            claim.facility.fillInFacility(rs.data[0].facility_num);
                    } else {
                        $("#claim_admit_date").val('');
                    }

                    if(next_step == true) {
                         $("#claim_admit_date").val('');
                    }
                   
                    // if (rs.hasOwnProperty('data') && rs.data.hasOwnProperty('0')) {
                    //         claim.facility.data[rs.data[0].facility_num] = rs.data[0];
                    //         claim.facility.fillInFacility(rs.data[0].facility_num);
                    //     }

                    // if (claim.transfer_patient_data_to_claim_form == 'true')  {
                    //     if($('#claim_facility_description').val() == '')
                    //     {
                    //         if (rs.hasOwnProperty('data') && rs.data.hasOwnProperty('0')) {
                    //                 claim.facility.data[$('#claim_facility_num').val()] = rs.data[0];
                    //                 claim.facility.fillInFacility($('#claim_facility_num').val());
                    //         }   
                    //     }
                    // } else {
                    //     if (rs.hasOwnProperty('data') && rs.data.hasOwnProperty('0')) {
                    //         claim.facility.data[rs.data[0].facility_num] = rs.data[0];
                    //         claim.facility.fillInFacility(rs.data[0].facility_num);
                    //     } 
                    // }
                    /* else {
                    $('#claim_facility_num').val($('#claim_facility_description').val());
                    //$('#claim_facility_id').val('');
                    //$('#claim_facility_description').val('');
                    //$("#claim_admit_date").hide();
                    }*/
                },
                beforeSend: function (rs) {
                    claim.facility.ajaxLock = true;
                },
                complete: function (rs) {
                    if (claim.skipPatientSearch) {
                       //$('#claim_health_num').focus();
                    } else {
                        $('#claim_claim_items_0_service_code').focus();
                    }
                    claim.facility.ajaxLock = false;
                },
                error: function (rs) {
                }
            });
        }
    },

    UpdatePatientData: function (data) {
        $.ajax({
            type: 'POST',
            url: '/patient/updatePatientFromClaim',
            data: data,
            success: function (rs) {
                if (claim.updatePatientAlert) {
                    claim.updatePatientAlert = false;
                    var result = JSON.parse(rs);
                    if (result.hasOwnProperty('success')) {
                        $.notify('Updated patient profile.', {
                            element: 'body',
                            type: "success",
                            allow_dismiss: true,
                            newest_on_top: true,
                            placement: {
                                from: "top",
                                align: "left"
                            },
                            delay: 3000,
                            z_index: 99999,
                            mouse_over: 'pause',
                            animate: {
                                enter: 'animated bounceIn',
                                exit: 'animated bounceOut'
                            },
                            template: `<div data-notify="container" class="col-xs-11 col-sm-2 alert alert-{0}" role="alert">
                                                    <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                                    <span data-notify="icon"></span>
                                                    <span data-notify="title">{1}</span>
                                                    <span data-notify="message">{2}</span>
                                                    <div class="progress" data-notify="progressbar">
                                                            <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                                    </div>
                                                    <a href="{3}" target="{4}" data-notify="url"></a>
                                            </div>`

                        });
                    } else if (result.hasOwnProperty('error')) {
                        $.notify('Error updating patient profile.', {
                            element: 'body',
                            type: "danger",
                            allow_dismiss: true,
                            newest_on_top: true,
                            placement: {
                                from: "top",
                                align: "left"
                            },
                            delay: 3000,
                            z_index: 99999,
                            mouse_over: 'pause',
                            animate: {
                                enter: 'animated bounceIn',
                                exit: 'animated bounceOut'
                            },
                            template: `<div data-notify="container" class="col-xs-11 col-sm-2 alert alert-{0}" role="alert">
                                                    <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                                    <span data-notify="icon"></span>
                                                    <span data-notify="title">{1}</span>
                                                    <span data-notify="message">{2}</span>
                                                    <div class="progress" data-notify="progressbar">
                                                            <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                                    </div>
                                                    <a href="{3}" target="{4}" data-notify="url"></a>
                                            </div>`

                        });
                    }
                    setTimeout(function () {
                        claim.updatePatientAlert = true;
                    }, 5000);
                }
            }
        });
    },
    UpdatePatient: function (dataURL) {
        $.ajax({
            type: 'POST',
            url: claim.actions.update_patient,
            data: dataURL,
            success: function (rs) {
                if (claim.updatePatientAlert) {
                    claim.updatePatientAlert = false;
                    var result = JSON.parse(rs);
                    if (result.hasOwnProperty('success')) {

                        $.notify('Updated patient profile with new version code.', {
                            element: 'body',
                            type: "success",
                            allow_dismiss: true,
                            newest_on_top: true,
                            placement: {
                                from: "top",
                                align: "left"
                            },
                            delay: 3000,
                            z_index: 99999,
                            mouse_over: 'pause',
                            animate: {
                                enter: 'animated bounceIn',
                                exit: 'animated bounceOut'
                            },
                            template: `<div data-notify="container" class="col-xs-11 col-sm-2 alert alert-{0}" role="alert">
                                                    <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                                    <span data-notify="icon"></span>
                                                    <span data-notify="title">{1}</span>
                                                    <span data-notify="message">{2}</span>
                                                    <div class="progress" data-notify="progressbar">
                                                            <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                                    </div>
                                                    <a href="{3}" target="{4}" data-notify="url"></a>
                                            </div>`

                        });
                    } else if (result.hasOwnProperty('error')) {

                        $.notify('Error updating patient profile with new version code.', {
                            element: 'body',
                            type: "error",
                            allow_dismiss: true,
                            newest_on_top: true,
                            placement: {
                                from: "top",
                                align: "left"
                            },
                            delay: 3000,
                            z_index: 99999,
                            mouse_over: 'pause',
                            animate: {
                                enter: 'animated bounceIn',
                                exit: 'animated bounceOut'
                            },
                            template: `<div data-notify="container" class="col-xs-11 col-sm-2 alert alert-{0}" role="alert">
                                                    <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                                    <span data-notify="icon"></span>
                                                    <span data-notify="title">{1}</span>
                                                    <span data-notify="message">{2}</span>
                                                    <div class="progress" data-notify="progressbar">
                                                            <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                                    </div>
                                                    <a href="{3}" target="{4}" data-notify="url"></a>
                                            </div>`

                        });
                    }
                    setTimeout(function () {
                        claim.updatePatientAlert = true;
                    }, 5000);
                }
            }
        });
    },
    compareToAutofill: function () {
        var doc_id = $('#claim_doctor_id').val();
        var loc_id = $('#claim_location_id').val();
        var sli = $('#claim_sli').val();

        if (claim.autofill.data.doctor_id != "" && doc_id != "" && doc_id != claim.autofill.data.doctor_id) {
            return "You are saving a claim with a different doctor than the default, continue?";
        }

        if (claim.autofill.data.location_id != "" && loc_id != "" && loc_id != claim.autofill.data.location_id) {
            return "You are saving a claim with a different location than the default, continue?";
        }

        if (claim.autofill.data.sli != "" && sli != "" && sli != claim.autofill.data.sli) {
            return "You are saving a claim with a different SLI than the default, continue?";
        }
        return "";
    },
    initialize: function () {
        $('#tabs').tabs('disable', 2);

        setTimeout(function () {
            claim.updatePatientAlert = true;
        }, 500)
        $('#health_card_check_row').hide();

        sk.datepicker('#claim_dob');
        $('#claim_dob').skDP('setEndDate', new Date());

        $('#claim_dob').on('blur', function (e) {
            if ($('#claim_dob').val() != '') {
                quickClaim.calculateAge($('#claim_dob').skDP('getDate'), 'age');
            }
        });

        $('#claim_dob').on('clearDate', function (e) {
            quickClaim.calculateAge(null, 'age');

        });
        $('#claim_dob').on('changeDate', function (evt) {
            quickClaim.calculateAge($('#claim_dob').skDP('getDate'), 'age');
        });

        sk.datepicker('#claim_admit_date');

        /*$('#claim_admit_date').skDP('setStartDate', new Date(new Date().setMonth(new Date().getMonth() - 6)));*/
        $('#claim_admit_date').skDP('setEndDate', new Date());

        $('#claim_admit_date').on('changeDate', function (evt) {
            var date = $('#claim_admit_date').skDP('getDate');
            var d = new Date();
            var year = d.getFullYear();
            var month = d.getMonth();
            var day = d.getDate();
            year = +year + +40;
            var c = new Date(year, month, day);
            if (date > c) {
                $("#claim_admit_date").addClass('sk-border-deep-orange');
            } else {
                $("#claim_admit_date").removeClass('sk-border-deep-orange');
            }
        });

        $('#claim_location_id').change(function () {
            claim.determineClaimType();
        });
        $('#claim_health_num').blur(function () {
            var value = $(this).val();
            if (value.length > 10) {
                $(this).addClass('sk-border-red');
            } else {
                $(this).removeClass('sk-border-red');
            }
            claim.determineClaimType();
        });
        $('#claim_province').change(function () {
            claim.determineClaimType();
        });

        $('#claim_pay_prog').change(function () {
            claim.handlePayProgChange();
        });
        $('input:radio[name="claim[payment_method]"]').click(function () {
            claim.handlePayProgChange();
        });
        $('input:radio[name="claim[card_type]"]').click(function () {
            claim.handlePayProgChange();
        });

        $('#claim_total_paid').blur(function () {
            var val = isNaN(parseFloat($(this).val(), 10)) ? 0 : parseFloat($(this).val(), 10);
            $(this).val(number_format(val, 2));

            claim.calculate.amountRemaining();
        });
        $('#claim_doctor_id').change(function () {
            var current_doctor = $('#claim_doctor_id').val();
            if (claim.doctor.facilities[current_doctor]) {


                var facilityNum = claim.doctor.facilities[current_doctor];
                var facilityNum1 = [];
                var facilityNum1 = facilityNum;

                //console.log(facilityNum1);
              
                var d = claim.facility.data;
                // console.log(d);

                // var imageList = Object.keys(d).map(function (key) { return d[key]; });
                    
                // var index1 = imageList.map(function (img) { return img.facility_num; }).indexOf(facilityNum);
                // console.log(index1);
                //var d = claim.facility.data[facilityNum];
                ////console.log(d[facilityNum1]);
                if(typeof d[facilityNum1] != 'undefined' && current_doctor != '') {
                $('#claim_facility_description').val(d[facilityNum1].toString);
                } else {
                $('#claim_facility_description').val('');
                }
                //$('#claim_facility_description').val(claim.doctor.facilities[current_doctor]);
                claim.facility.fillInFacility(claim.doctor.facilities[current_doctor]);
            } else {
                $('#claim_facility_description').val("");
                claim.facility.fillInFacility("");
                //$("#claim_admit_date").hide();
            }
            $('#claim_sli').val(claim.doctor.slis[current_doctor]);

            if (claim.services.determineModifier() != claim.doctor.current_modifier) {
                claim.services.updateModifiers(claim.doctor.current_modifier);
            }
            claim.doctor.current_modifier = claim.services.determineModifier();
            claim.facility.getDorctorQSC(current_doctor);
            claim.facility.getDorctorDSC(current_doctor);
            claim.services.updateBaseFees();
            claim.doctor.current_spec_code = claim.doctor.spec_codes[current_doctor];
        });
        claim.doctor.current_modifier = claim.services.determineModifier();

        $('#claim_button_clear')
                .button()
                .click(function () {
                    claim.clearForm(true);
                })
                .tooltip();
       
        $('#claim_button_save')
                .button()
                .click(function () {

                    var date = $('#claim_admit_date').skDP('getDate');
                    var d = new Date();
                    var year = d.getFullYear();
                    var month = d.getMonth();
                    var day = d.getDate();
                    year = +year + +40;
                    var c = new Date(year, month, day);
                    if (date > c) {
                        $("#claim_admit_date").addClass('sk-border-deep-orange');
                        $('#form_message')
                                .html('Admit Date is invalid.')
                                .addClass('alert alert-danger')
                                .show();
                        return;
                    } else {
                        $("#claim_admit_date").removeClass('sk-border-deep-orange');
                        $('#form_message').hide();
                    }

                    if (claim.actions.compare_autofill) {
                        var err = claim.compareToAutofill();

                        if (err != "") {
                            alertify.confirm('Confirm', err, function () {
                                claim.saveClaim();
                            }, function () {
                                $.notify('Cancel.', {
                                    element: 'body',
                                    type: "danger",
                                    allow_dismiss: true,
                                    newest_on_top: true,
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    delay: 1000,
                                    z_index: 99999,
                                    mouse_over: 'pause',
                                    animate: {
                                        enter: 'animated bounceIn',
                                        exit: 'animated bounceOut'
                                    },
                                    template: `<div data-notify="container" class="col-xs-11 col-sm-2 alert alert-{0}" role="alert">
                                                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                                <span data-notify="icon"></span>
                                                <span data-notify="title">{1}</span>
                                                <span data-notify="message">{2}</span>
                                                <div class="progress" data-notify="progressbar">
                                                        <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                                </div>
                                                <a href="{3}" target="{4}" data-notify="url"></a>
                                        </div>`

                                });
                            });
                        } else {
                            claim.saveClaim();
                        }
                    } else {
                        claim.saveClaim();
                    }
                })
                .tooltip();

       $('#patient_profile_button')
                .button()
                .on('contextmenu', function (e) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    return false;
                })
                .on('mousedown', function (e) {
                    e.preventDefault();
                    e.stopImmediatePropagation();

                    var rightClicked = (e.button == 2);
                    if ($('#claim_patient_id').val()) {
                        if (rightClicked) {
                            window.open(claim.actions.patient_profile + '?id=' + $('#claim_patient_id').val(), '_blank');
                            return false;
                        }
                        window.location = claim.actions.patient_profile + '?id=' + $('#claim_patient_id').val();
                    }
                    return false;
                }); 

        if ($('#claim_patient_id').val()) {
            // $('#patient_profile_button').removeAttr('disabled');//.show();
            $('#patient_profile_button').show();
            $('#tabs').tabs('enable', 2);
        }

        $('#claim_button_delete_temporary').button();
        $('#claim_button_delete_temporary').click(function () {
            $('#delete_temporary_confirm').dialog('open');
        });
        if ($('#claim_temporary_form_id').val()) {
            $('#claim_button_delete_temporary').show();
        }

        $('#service_code_dialog_button').button({
            icons: false,
            text: false
        });
        $('#service_code_dialog_button').css('float', 'right');
        $('#service_code_dialog_button').click(function () {
            search_modal.handleServiceCodeSearch(claim.items.getFirstEmptyRow());
            return false;
        });

        $('#sksrs_service_code_dialog_button').click(function () {
            search_modal.handleServiceCodeSearch(claim.items.getFirstEmptyRow());
            return false;
        });

        $('#diag_code_dialog_button').button({
            icons: {primary: 'ui-icon-search'},
            text: false
        });
        $('#diag_code_dialog_button').css('float', 'right');
        $('#diag_code_dialog_button').click(function () {
            search_modal.handleDiagCodeSearch(claim.items.getEmptyDiagCodes());
            return false;
        });
        $('#sksrs_diag_code_dialog_button').click(function () {
            search_modal.handleDiagCodeSearch(claim.items.getEmptyDiagCodes());
            return false;
        });

        claim.setupRefdocAutocomplete();

        if (!($('#direct_payments').find('tbody tr').size())) {
            $('#direct_payment_history').hide();
        } else {
            $('#direct_payments').tablesorter1({
                widgets: ['zebra', 'hover']
            });
        }

        if (!$('#claim_patient_id').val()) {
            $('#claim_province').val(claim.autofill.default_province);
        }
        if (!$('#claim_location_id').val()) {
            $('#claim_location_id').val(claim.autofill.default_location_id);
        }

        for (var a = 0; a < claim.items.row_count; a++) {
            claim.items.initialize(a);
        }

        $('#claim_history_button').button();
        $('#claim_history_button').click(function () {
            claim.handleHistoryClick();
        });

        $('#download_history_csv').button();
        $('#download_history_csv').click(function () {
            claim.handleHistoryCSVClick();
        });

        $('#claim_history_log_dialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            minWidth: 800,
            minHeight: 250,
        });

        claim.determineClaimType();
        claim.handlePayProgChange();
        claim.calculate.totalBilled(true);
        claim.autofill.setData();

        $('#next_appointment').mousedown(function () {
            claim.handleNextInLoopRequest();
            return false;
        });

        $('#cancel_appointments').mousedown(function () {
            claim.handleCancelLoopRequest();
            return false;
        });

        claim.services.updateTitles();

        $('#delete_temporary_confirm').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 400,
            buttons: {
                'Yes': function () {
                    $(this).dialog('close');
                    claim.deleteTemporaryData();
                },
                'No': function () {
                    $(this).dialog('close');
                }
            }
        });
        $('#claim_button_delete_temporary').tooltip();

        $('#claim_health_num').keydown(function (e) {
            if (claim.enable_hcv) {
                if (e.which == 40) { 
                    claimHCV.validate(false);
                }
            }
        });

        // $('#claim_version_code').blur(function () {
        //     if (claim.enable_hcv) {
        //         claimHCV.validate(false);
        //     }

        //     var new_version_code = $('#claim_version_code').val();
        //     var patient_id = $('#claim_patient_id').val();
        //     var dataURL = 'version_code=' + new_version_code + '&patientid=' + patient_id;

        //     if (patient_id) {
        //         claim.UpdatePatient(dataURL);
        //     }

        // });

        var hasChanged = false;

        $('#claim_lname, #claim_fname, #claim_patient_number').on('keyup', function () {
            hasChanged = true;
        })

        $('#claim_lname, #claim_fname, #claim_patient_number').blur(function () {
            var field = $(this).attr('id').replace('claim_', '');
            var data = $(this).val();
            var patient_id = $('#claim_patient_id').val();
            var dataURL = 'field=' + field + '&data=' + data + '&patientid=' + patient_id;
            if (patient_id && hasChanged) {
                claim.UpdatePatientData(dataURL);
                hasChanged = false;
            }
        });

        //#claim_dob, 
        $('#claim_hcn_exp_year').on('changeDate', function () {
            var field = $(this).attr('id').replace('claim_', '');
            var date = $(this).skDP('getDate');
            if (date != null) {
                var data = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
                var patient_id = $('#claim_patient_id').val();
                var dataURL = 'field=' + field + '&data=' + data + '&patientid=' + patient_id;
            }

            if (patient_id) {
                claim.UpdatePatientData(dataURL);
            }
        });

        //#claim_dob, 
        $('#claim_hcn_exp_year').on('clearDate', function () {
            var field = $(this).attr('id').replace('claim_', '');
            var data = null;
            var patient_id = $('#claim_patient_id').val();
            var dataURL = 'field=' + field + '&data=' + data + '&patientid=' + patient_id;

            if (patient_id) {
                claim.UpdatePatientData(dataURL);
            }
        });


        $('#claim_sex').on('change', function () {
            var field = $(this).attr('id').replace('claim_', '');
            var data = $(this).val();
            var patient_id = $('#claim_patient_id').val();
            var dataURL = 'field=' + field + '&data=' + data + '&patientid=' + patient_id;
            if (patient_id) {
                claim.UpdatePatientData(dataURL);
            }
        }); 



        var d = new Date();
        var newDate = d.toString('yyyy-MM-dd');
        if(claim.items.hcv_last_checked == newDate)
        {
           claimHCV.validate(false,'false');
        }
        else
        {
            claimHCV.validate(false);
        }
        // if ($('#claim_health_num').val()) {
        //     if (claim.enable_hcv) {
        //         claimHCV.validate(false);
        //     }
        // }

        if ($('input.hl7_partner_id').size()) {
            $('input.hl7_partner_id:last').closest('tr').after($('<tr class="small_row"><td colspan="2">&nbsp;</td></tr>'));
        }

        if ($('#claim_id').val()) {
            $('#claim_history_button').show();
        }

        $('#no_results_dialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            width: 700
        });

        $('#no_results_create').button();
        $('#no_results_create').tooltip();
        $('#no_results_create').click(function (e) {
            claim.no_results_yes_clicked = true;
            $('#no_results_dialog').dialog('close');
            patient_search.handleOneResult(patient_search.edit_target.search_cache, true);
        });

        $('#no_results_cancel').button().tooltip().click(function () {
            $('#no_results_dialog').dialog('close');
        });

        $('#validateButton').button();
        $('#validateButton').click(function () {
//            if (claim.enable_hcv) {

            claimHCV.validate(true);

//            }
        });

        claim.handleDiscountedFieldSetup();
        $('#patientHistoryTable').find('thead th').each(function () {
            claim.patientHistory.resultColumns[claim.patientHistory.resultColumns.length] = $(this).attr('class');
        });

        $('#patientHistoryTable').tablesorter({
            theme: 'blue',
            widgets: ['zebra']
        });

        if (claim.always_show_temporary_button) {
            $('#temporary_save_row').show();
        }

        $("#time_dialog").dialog({
            autoOpen: false
        });

        var fromTimeDial = $('#fromtime').timePicker({
            'twelve_hour': true,
            'enable_buttons': false,
            'autohide': true,
            'positon': 'top'
        });

        var toTimeDial = $('#totime').timePicker({
            'twelve_hour': true,
            'enable_buttons': false,
            'autohide': true,
            'positon': 'top'
        });


        $('#calculateMinutes').button();
        $('#calculateMinutes').click(function (e) {

            var manual = $('#minutesManualText').val();
            if (manual > 0) {
                claim.calculateMinutes(manual);
                $("#time_dialog").dialog('close');
                return;
            }

            var from = $('#fromtime').val();
            var to = $('#totime').val();

            var fromTime = new Date("01/01/2007 " + from);

            if (to.includes('AM')) {
                var toTime = new Date("01/01/2007 " + to);
            } else {
                var toTime = new Date("01/01/2007 " + to);
            }

            var mins = toTime - fromTime;
            mins = mins / 60 / 1000;

            claim.calculateMinutes(mins);
            $("#time_dialog").dialog('close');
        });

        if ($("#claim_note").val() != '') {
            $('#claim_note_checkbox').prop('checked', true);
        }
        if ($('#claim_note_checkbox').prop('checked')) {
            $('#claim_note_row').show();
            setTimeout(function () {
                $('#claim_item_table tbody').animate({scrollTop: $('#claim_item_table tbody').prop("scrollHeight")}, 200);
            }, 200);

        }

        var patient_id_href = window.location.href.indexOf('patient_id') != -1;
        if (claim.actions.load_last_five_services && patient_id_href) {
            claim.requestPatientHistory();
        }

        setup.events.setupTourButtonClaims();

        sk.datepicker('#claim_claim_items_0_service_date');




        if (claim.edit_mode == false && claim.appointment_search == false) {
             if (claim.autofill.enabled  && claim.autofill.fill_service_date == false) {
                 $('#' + claim.items.form_prefix + '0_service_date').skDP('setDate', sk.getJsDate(claim.autofill.data.last_service_date));
             } else {
            $('#claim_claim_items_0_service_date').skDP('setDate', new Date());
             }
        } else {
            $('#claim_dob').skDP('setDate');
        }
        claim.facility.initialize();
        claim.ClearField();
        if ($("#claim_admit_date").skDP('getDate') != null) {
            $("#claim_admit_date").show();
        }
        if ($('#tabs').tabs('option', 'active') == claim.tab_index) {
            if (claim.skipPatientSearch && !claim.edit_mode) {
                $('input[data-focus="true"]').attr('data-focus', false);
                //$('#claim_claim_items_0_service_code').focus();
                $('#claim_health_num').focus();
            } else {
                $('input[data-focus="true"]').attr('data-focus', false);
                $('#claim_claim_items_0_service_code').focus();
                //$('#claim_health_num').focus();
            }
        }
        claim.initialized = true;
        $("#claim_ref_doc_description").keydown((e) => {
            if (e.key == 'Tab') {
                var $select = $("#claim_doctor_id").select2();
            }

            if (e.key == 'ArrowUp') {
                $('#claim_claim_items_0_service_code').focus();
            }
        })

        claim.facility.getDorctorQSC($('#claim_doctor_id').val());
        claim.facility.getDorctorDSC($('#claim_doctor_id').val());
    },

    clearForm: function (buttonUsed) {
        //var current_doctor = $('#claim_doctor_id').val();
        this.from_temporary_form = false;
        quickClaim.clearMessages('form_message', true);
        $('#claim_form').clearForm('form_message');
        $('#claim_form input, select').removeClass('cliponeErrorInput');
        $('#age').html('');
        if ($("#claimPatientName")) {
            $("#claimPatientName").html('');
        }

        $("#claim_dob").skDP('setDate', null);

        $('#tabs').tabs('disable', 2);
        $('#patientHistoryTable').find('tbody tr').remove();
        if ($("#patientHistoryTableShort")) {
            if ($.fn.DataTable.isDataTable('#patientHistoryTableShort')) {
                $("#patientHistoryTableShort").DataTable().clear().draw();
            }
        }
        patient_search.handleClearPress();

        if (!claim.skipPatientSearch) {
            $('#tabs').tabs('option', 'active', 0);
            $('#patient_search_patient_number').focus();
        } else {
            $('#claim_health_num').focus();
        }

        $('#direct_payment_history').hide();
        $('#direct_payments').find('tbody tr').remove();
        $('#direct_payments').find('tfoot span').text('');
        $('#direct_payments').find('.claim_total').text(number_format(0, 2));
        $('#direct_payments').find('.total_paid').text(number_format(0, 2));
        $('#direct_payments').find('.total_owing').text(number_format(0, 2));
        $('#health_card_check_row').hide();
        claimHCV.values = {health_num: '', version_code: ''};

        $('tr.claim_item_row').each(function () {
            var id = $(this).attr('id').replace('claim_row_', '');
            if (id != '0') {
                claim.items.clearRow(id);
            }
        });
        $('#claim_claim_items_0_status').val('0');
        $('.discounted_row').removeClass('discounted_row');
        $('.original_fee_text').remove();

        $('.taxable_amounts').text('');
        $('.taxes').hide();
        $('#claim_item_table').find('tbody .error_codes').text('');

        if (!claim.items.canShowFeePaid || buttonUsed) {
            $('#claim_item_table').find('tbody .payments').text('');
        }

        if (!claim.always_show_temporary_button) {
            $('#temporary_save_row').hide();
        }

        $('#override_save_row').hide();

        claim.items.row_count = 1;

        claim.handlePayProgChange();
        claim.autofill.setData();
        claim.edit_mode = false;

        if (!claim.edit_mode) 
        {
          claim.items.claimLockData();
        }
        $('#results_text_multi_parent').css('display','none');
        $('#appt_current_text').text('');
        window.history.pushState("Claims", "Claims - HYPE Medical", window.location.origin+"/claim");

        //$('#patient_profile_button').attr('disabled');//.hide();
        $('#patient_profile_button').hide();

        if (!claim.can_create) {
            $('.cannot_create').show().addClass('error');
            $('#claim_button_save').remove();
            $('#claim_button_clear').remove();
        }
        $('#claim_history_button').hide();
        $('#claim_id_div').text('');
        $('#claim_pay_prog').val('').trigger('change.select2');
        $("#new_tab select").each(function () {
            $(this).trigger('change.select2');
        });

        if (claim.autofill.enabled && claim.autofill.fill_service_date == false) {
            $('#claim_claim_items_0_service_date').val(claim.autofill.data.last_service_date);
        }

        // $('#claim_doctor_id option[value='+current_doctor+']').attr('selected','selected');
        // if($("#claim_facility_description").val() == '' || current_doctor != '') { 
        //     var facilityNum = claim.doctor.facilities[current_doctor];
        //     var d = claim.facility.data;
        //     //var d = claim.facility.data[facilityNum];
        //    if(typeof d[facilityNum] != 'undefined' && current_doctor != '') {
        //     $('#claim_facility_description').val(d[facilityNum].toString);
        //     } else {
        //     $('#claim_facility_description').val('');
        //     }   
        // }
       
    },
    deleteTemporaryData: function () {
        if (quickClaim.ajaxLocked) {
            return;
        }

        if ($('#claim_temporary_form_id').val()) {
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: claim.actions.temporary_delete,
                data: 'id=' + $('#claim_temporary_form_id').val(),
                success: function (rs) {
                    if (rs.status == true) {
                        if (rs.hasOwnProperty('next_claim')) {
                            claim.handleNextInLoopResults(rs);
                        } else {
                            claim.clearForm(false);

                            $('#tabs').tabs('option', 'active', 0);
                            $('#success_message_text').html('Deleted temporary data.');
                            $('#success_message').show();

                            $('#messages_text').text('').hide();
                        }
                    } else {
                        $('#error_icon').show();
                        $('#messages_text').text('An error occurred while trying to delete temporary data.');
                    }
                },
                beforeSend: function (rs) {
                    quickClaim.ajaxLocked = true;
                    $('#checkmark').hide();
                    $('#error_icon').hide();
                    $('#spinner').show();

                    $('#messages_text').text('Deleting Temporary Data').show();
                },
                complete: function (rs) {
                    quickClaim.ajaxLocked = false;
                    $('#spinner').hide();
                },
                error: function (rs) {
                    $('#error_icon').show();
                    $('#messages_text').text('An error occurred while contacting the server.');
                }
            });
        }
    },
    determineClaimType: function () {
        if ($('#claim_id').val()) {
            return claim.handlePayProgChange();
        }

        var hcn = $('#claim_health_num').val();
        var loc_id = $('#claim_location_id').val();
        var prov = $('#claim_province').val();
        var loc_prov = claim.location_provinces[loc_id];

        if (loc_prov != 'ON' && loc_id) {
            alert('We currently do not support creating claims for locations outside of Ontario');
        } else if (!hcn && $('#claim_patient_id').val()) {
            $('#claim_pay_prog').val('DIRECT');
        } else if (prov != 'ON') {
            $('#claim_pay_prog').val('RMB');
        } else {
            $('#claim_pay_prog').val('HCP - P');
        }
        claim.handlePayProgChange();

    },
    handleDiscountedFieldSetup: function () {
        $('.discounted_field').each(function () {
            if ($(this).val() == '1') {
                var row = $(this).attr('id').replace('claim_claim_items_', '').replace('_discounted', '');

                $('#claim_row_' + row).addClass('discounted_row');
                var original_fee = $('#claim_claim_items_' + row + '_original_fee').val();

                var text = '<div class="original_fee_text">Original cost: $' + number_format(original_fee, 2, '.', ',') + '</div>';
                $('#claim_claim_items_' + row + '_fee_subm').parent().append($(text));
            }
        });
    },
    handleHistoryClick: function () {
        if (quickClaim.ajaxLocked) {
            return;
        }
        var url = claim.actions.history + '?id=' + $('#claim_id').val() + '&format=json';

        $.ajax({
            type: 'get',
            dataType: 'json',
            url: url,
            data: data,
            success: function (rs) {

                var div = '';
                for (var a in rs) {
                    div += '<div class="date">' + rs[a]['event_date'] + ' &nbsp;&nbsp; ' + rs[a]['username'] + '</div>';
                    div += '<ul>';

                    for (var b in rs[a].data) {
                        div += '<li class="event">';
                        div += '<div class="type">' + rs[a].data[b]['type_str'] + ' [' + rs[a].data[b]['version_table_str'] + ' ID: ' + rs[a].data[b]['active_id'] + ']</div>';
                        div += '<div class="event_description">' + rs[a].data[b]['action_description'] + '</div>';
                        div += '</li>';
                    }

                    div += '</ul>';
                }

                $('#claim_history_log_list').html($(div));
                $('#claim_history_log_dialog').dialog('open');
            },
            beforeSend: function (rs) {
                quickClaim.ajaxLocked = true;
                $('#checkmark').hide();
                $('#error_icon').hide();
                $('#spinner').show();

                $('#messages_text').text('Retrieving Claim History').show();
            },
            complete: function (rs) {
                quickClaim.ajaxLocked = false;
                $('#spinner').hide();
            },
            error: function (rs) {
                $('#error_icon').show();
                $('#messages_text').text('An error occurred while contacting the server.');
            }
        });
    },
    handleHistoryCSVClick: function () {
        var url = claim.actions.history + '?id=' + $('#claim_id').val() + '&format=csv';
        claim.setupIFrame();
        claim.setupHiddenForm([], url, '');
    },
    handleKeyPress: function (event) {
        var element_id = $(document.activeElement).attr('id');
        var closest_table = $(document.activeElement).closest('table').attr('id');
        var item_row = (closest_table == 'claim_item_table') ? $(document.activeElement).closest('tr').attr('id') : null;
        var row_number = null;

        if (!claim.always_show_temporary_button) {
            $('#temporary_save_row').hide();
        }

        $('#override_save_row').hide();
        $('#claim_save_temporary').prop('checked', false);
        $('#claim_save_anyways').prop('checked', false);

        // up pressed in claim items table
        if (closest_table == 'claim_item_table' && event.which == '38' && !claim.autocomplete_open) {
            row_number = item_row.replace('claim_row_', '');

            if (element_id && element_id == 'claim_claim_items_' + row_number + '_diag_code') {
                claim.services.fillDiagCode();
            }
            claim.items.rowUp(row_number);
        }

        // up pressed in claim items table
        if (!$('.ui-autocomplete.ui-widget:visible').length && ((closest_table != 'claim_item_table' && event.which == '38') || (closest_table != 'claim_item_table' && event.which == '40'))) {

            if (claim.actions.down_arrow_option) {
                if (event.currentTarget.activeElement.id == 'claim_health_num' || event.target.id == "claim_version_code" || event.srcElement.id == "claim_version_code" || event.target.id == 'claim_health_num' || event.srcElement.id == "claim_health_num") {
                    $('#claim_ref_doc_description').focus();
                } else {
                    $("#claim_item_table input:first").focus();
                }
            } else {
                $("#claim_item_table input:first").focus();
            }
        }

        // down pressed in claim items table
        else if (closest_table == 'claim_item_table' && event.which == '40' && !claim.autocomplete_open) {
            row_number = item_row.replace('claim_row_', '');

            if (element_id && element_id == 'claim_claim_items_' + row_number + '_diag_code') {
                claim.services.fillDiagCode();
            }
            claim.items.rowDown(row_number);

        } else if (event.which == '40' && closest_table == 'details_table' && document.activeElement.tagName == 'INPUT' && document.activeElement.id != 'claim_ref_doc_description' && document.activeElement.id != 'claim_facility_description') {

            $('#claim_claim_items_0_service_code').focus();
            if (claim.actions.down_arrow_option) {
                if (event.target.id == "claim_version_code" || event.srcElement.id == "claim_version_code") {
                    $('#claim_ref_doc_description').focus();
                }
            }
        }

        // non-enter keypress in claim item table
        else if (closest_table == 'claim_item_table' && event.which != '13' && !claim.autocomplete_open) {
            row_number = item_row.replace('claim_row_', '');
            claim.items.checkRowCount(row_number);
        }
        // enter keypress anywhere
        else if (event.which == '13') {
            if (claim.autocomplete_open) {
                claim.autocomplete_open = false;
                return;
            }

            if (global_js.enter == false || $('#delete_temporary_confirm').dialog('isOpen') || $('#referring_doctor_dialog').dialog('isOpen') || $('#search_results_dialog').dialog('isOpen') || $('#service_code_dialog').dialog('isOpen') || $('#diag_code_dialog').dialog('isOpen')) {
                global_js.enter = true;
                return;
            }

            if (element_id == 'referring_doctor_provider_num') {
                return;
            }

            if (element_id == 'claim_note') {
                return;
            }

            if (element_id == 'claim_facility_description') {
                return;
            }

            if (element_id && element_id.indexOf('service_code') != -1) {
                claim.services.handleCodeLookup(item_row.replace('claim_row_', ''));
            }

            if (event.target.className == 'ajs-input') {
                return;
            }

            if ($('#' + patient_search.results_dialog).dialog("isOpen")) {
                return;
            }

            if (patient_search.selectedIndex != 0) {
                patient_search.selectedIndex = 0;
                return;
            }

            claim.saveClaim();
        }
    },
    handleNextInLoopRequest: function () {
        if (quickClaim.ajaxLocked) {
            return;
        }
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: claim.actions.next_in_loop,
            data: '',
            success: function (rs) {
                quickClaim.clearMessages('form_message', true);
                if (rs.hasOwnProperty('next_claim')) {
                    claim.handleNextInLoopResults(rs);

                    $('#messages_text').text('').show();
                } else {
                    $('#from_appointments_count').text('');

                    claim.clearForm(true);
                    $('#tabs').tabs('option', 'active', 0);
                    $('#messages_text').text('').show();
                }
            },
            beforeSend: function (rs) {
                quickClaim.ajaxLocked = true;
                $('#checkmark').hide();
                $('#error_icon').hide();
                $('#spinner').show();

                $('#messages_text').text('Requesting Next Item').show();
            },
            complete: function (rs) {
                quickClaim.ajaxLocked = false;
                $('#spinner').hide();
            },
            error: function (rs) {
                $('#error_icon').show();
                $('#messages_text').text('An error occurred while contacting the server.');
            }
        });
    },
    handleCancelLoopRequest: function () {

        $.ajax({
            type: 'get',
            url: claim.actions.cancel_loop,
            success: function (rs) {
                quickClaim.clearMessages('form_message', true);

                $('#tabs').tabs('option', 'active', 0);
                $('#messages_text').text('').show();

                location.reload();
            },
            beforeSend: function (rs) {
                quickClaim.ajaxLocked = true;
                $('#checkmark').hide();
                $('#error_icon').hide();
                $('#spinner').show();

                $('#messages_text').text('Cancelling').show();
            },
            complete: function (rs) {
                quickClaim.ajaxLocked = false;
                $('#spinner').hide();
            },
            error: function (rs) {
                $('#error_icon').show();
                $('#messages_text').text('An error occurred while contacting the server.');
            }
        });

    },
    alertNext: function () {
        $('#next_appointment').mousedown(function () {
            claim.handleNextInLoopRequest();
            return false;
        });

        $('#cancel_appointments').mousedown(function () {
            claim.handleCancelLoopRequest();
            return false;
        });
    },
    handleNextInLoopResults: function (rs) {
        
        var count;
        var b;
        var row_number = null;
        claim.loadData({});

        for (var a in rs['next_claim']) {
            if (a == 'claim_items') {
                for (row_number in rs['next_claim'].claim_items) {
                    count = claim.items.getFirstEmptyRow();

                    if (count >= claim.items.row_count) {
                        claim.items.drawNewRow();
                        count = claim.items.getFirstEmptyRow();
                    }

                    for (b in rs['next_claim'].claim_items[row_number]) {
                        $('#claim_claim_items_' + count + '_' + b).val(rs['next_claim'].claim_items[row_number][b]);
                    }
                }

                count = claim.items.getFirstEmptyRow();
                if (count > claim.items.row_count) {
                    claim.items.drawNewRow();
                }
            } else if (a == 'taxes') {
                for (b in rs['next_claim'].taxes) {
                    var tax = rs['next_claim'].taxes[b];
                    $('.tax_' + b + ' td').text('$' + tax);
                    $('.tax_' + b).show();

                    var classes = $('.tax_' + b).attr('class').split(' ');

                    for (var c in classes) {
                        if (!(classes[c] == 'taxes' || classes[c] == 'total_billed' || classes[c] == 'tax_' + b)) {
                            $('#claim_' + classes[c]).val(tax);
                        }
                    }

                }
            } else if (a == 'direct_payments') {
                for (b in rs['next_claim']['direct_payments']) {
                    var row = '<tr>'
                            + '<td class="date">' + rs['next_claim']['direct_payments'][b].date + '</td>'
                            + '<td class="payment_method">' + rs['next_claim']['direct_payments'][b].payment_method + '</td>'
                            + '<td class="amount_paid">' + rs['next_claim']['direct_payments'][b]['amount_paid'] + '</td>'
                            + '</tr>';

                    $(row).appendTo('#direct_payments tbody');
                }
                $('#direct_payment_history').show();
                if (b) {
                    $('#direct_payments').tablesorter1({
                        widgets: ['zebra', 'hover']
                    });
                }
            } else if (a == 'amt_billed') {
                $('#direct_payments').find('.claim_total').text(number_format(rs['next_claim'][a], 2));
            } else if (a == 'amt_paid') {
                $('#direct_payments').find('.total_paid').text(number_format(rs['next_claim'][a], 2));
            } else if (a == 'amt_remaining') {
                $('#direct_payments').find('.total_owing').text(number_format(rs['next_claim'][a], 2));
            } else {
                $('#claim_' + a).val(rs['next_claim'][a]);
                if ($('#claim_' + a).is('select')) {
                    $('#claim_' + a).trigger('change.select2');
                }
                if (a == 'facility_description') {
                    claim.facility.fillInFacility(rs['next_claim'][a],false);
                }
            }
        }

        if (rs.hasOwnProperty('error_codes')) {
            claim.items.insertErrorCodes(rs['error_codes']);
        }
        if (rs.hasOwnProperty('payments')) {
            claim.items.insertPayments(rs['payments']);
        }

        if (rs.hasOwnProperty('doctor_string')) {
            claim.checkForInactiveDoctor(rs['next_claim']['doctor_id'], rs['doctor_string']);
        }

        claim.handlePayProgChange();
        claim.calculate.premiumCodes();

        var current = parseInt($('#appt_count_current').text(), 10);
        $('#appt_count_current').text(current + 1);

        var ClaumId = "[Claim ID "+rs['next_claim'].id+"]";
        $('#claimPatientName').text(ClaumId);
        $('#appt_current_text').text(rs['next_claim_information']);

        //claim.alertNext();
        claim.services.updateTitles();

        //if (claim.enable_hcv) {
        claimHCV.validate(false);
        //}

        $('#claim_claim_items_0_service_code').focus();

        if ($('#claim_id').val()) {
            $('#claim_history_button').show();
        }
        if (!$('#claim_patient_id').val()) {
            //$('#patient_profile_button').attr('disabled');//.hide();
            $('#patient_profile_button').hide();
        } else {
            $('#tabs').tabs('enable', 2);
            //$('#patient_profile_button').removeAttr('disabled');//.show();
            $('#patient_profile_button').show();
        }
        claim.handleDiscountedFieldSetup();
    },
    handlePayProgChange: function () {
        var pay_prog = $('#claim_pay_prog').val();
        var advanced = false;
        //alert();
        $('#quick_service_codes_div').show();
        $('#direct_service_codes_div').hide();
        $('#direct_service_codes_div spam').hide();
        $('.direct_row').hide();
        $('.third_party_row').hide();

        $('.card_type').hide();
        $('.other_card').hide();
        $('.cheque_number').hide();
        $('.transaction_number').hide();
        if (pay_prog == 'THIRD PARTY') {
            $('.third_party_row').show();

            if (claim.showDirectCodesForThirdParty) {
                $('#quick_service_codes_div').hide();
                $('#direct_service_codes_div').show();
                 claim.facility.getDorctorQSC($('#claim_doctor_id').val());
            }
        } else if (pay_prog == 'DIRECT') {
            advanced = true;
            $('#quick_service_codes_div').hide();
            $('#direct_service_codes_div').show();
            claim.facility.getDorctorQSC($('#claim_doctor_id').val());
            $('.direct_row').show();

            var autocheck_payment = !$('#claim_id').val() && claim.new_direct_autocheck_payment;
            $('#claim_record_direct_payment').prop('checked', autocheck_payment);
        }

        

        if (advanced) {
            var method = $('input:radio[name="claim[payment_method]"]:checked').val();
            var card_type = $('input:radio[name="claim[card_type]"]:checked').val();

            if (!method) {
                $('#claim_payment_method_CASH').prop('checked', true);
                method = 'CASH';
            } else if (method == 'CHEQUE') {
                $('.cheque_number').show();
            } else if (method == 'CREDIT') {
                $('.card_type').show();
                $('.transaction_number').show();

                if (card_type == 'OTHER') {
                    $('.other_card').show();
                }
            } else if (method == 'DEBIT') {
                $('.transaction_number').show();
            }
        } else {
            $('#claim_payment_method').val(null);
            $('.transaction_number').show();
        }
        $('#claim_pay_prog').trigger('change.select2');
    },
    handlePdfClick: function (id, type) {
        claim.setupIFrame();
        claim.setupHiddenForm([], claim.actions.pdf + '?id=' + id + '&type=' + type, '');
    },
    handleNoSearchResultDialog: function () {
        $('#no_results_dialog').dialog('open');
    },
    handleClearPress: function () {
        claim.clearForm(false);
    },
    loadData: function (data, is_new) {
        $("#claim_note_checkbox").trigger('change');
        claim.patientHistory.patientID = null;
        if (data.hasOwnProperty('hcn_num')) {
            data.health_num = data.hcn_num;
        }
        claim.clearForm(false);
        $('.jGrowl-notification').trigger('jGrowl.close');

        $('#tabs').tabs('option', 'active', 1);

        //console.log(data);
        quickClaim.formFill(data, '#claim_', 'search_results_dialog');
        var claim_facility_num = data.facility_num;

        var d = new Date();
        var newDate = d.toString('yyyy-MM-dd');
        
        
        if(data.hcv_last_checked == newDate)
        {
            if ($('#claim_health_num').val()) {
                //if (claim.enable_hcv) {
                claimHCV.validate(false,'false');
                //}
            } 
        }
        else
        {
            if ($('#claim_health_num').val()) {
                //if (claim.enable_hcv) {
                claimHCV.validate(false);
                //}
            } 
        }

        //console.log(claim.doctor.facilities);
        //doctor selected as par facility num wise
        // $.each(claim.doctor.facilities, function(key, value) { 
        //     if(value == data.facility_num)
        //     {
        //        $('#claim_doctor_id').val(key).trigger('change.select2'); 
        //     }
        // });

         // else {
         //       $('#claim_doctor_id').val('').trigger('change.select2');
         //    }

        // var current_doctor = $('#claim_doctor_id').val();
        // if($("#claim_facility_description").val() == '') { 
        // var facilityNum = claim.doctor.facilities[current_doctor];
        // var d = claim.facility.data;
        // var d = claim.facility.data[facilityNum];
        //    if(typeof d[facilityNum] != 'undefined') {
        //     $('#claim_facility_description').val(d[facilityNum].toString);
        //     $('#claim_sli').val(claim.doctor.slis[current_doctor]);
        //     } 
        // }

        //alert(claim.transfer_patient_data_to_claim_form);
        //console.log(claim.transfer_patient_data_to_claim_form);
        if (claim.transfer_patient_data_to_claim_form == 'false')  {
            $('#claim_doctor_id').val('');
            $("#claim_facility_description").val('');
            $("#claim_facility_id").val('');
            $("#claim_facility_num").val('');
            $("#claim_admit_date").val('');
            $("#claim_ref_doc_description").val('');
        }

        if (data['facility_description'] != '') {
            $("#claim_admit_date").show();
        }
        if (!$('#claim_patient_id').val()) {
            $('#tabs').tabs('disable', 2);
            $('#claim_province').val(claim.autofill.default_province);
            //$('#patient_profile_button').attr('disabled');//.hide();
            $('#patient_profile_button').hide();
        } else {
            $('#tabs').tabs('enable', 2);
            //$('#patient_profile_button').removeAttr('disabled');//.show();
            $('#patient_profile_button').show();
            $('#tabs').tabs();
        }

        if (!$('#claim_temporary_form_id').val()) {
            if (!$('#claim_location_id').val()) {
                $('#claim_location_id').val(claim.autofill.default_location_id);
            }
          claim.autofill.setData();
        }


        //#3821 saving a claim for a brand new patient
        //console.log(claim.doctor.facilities);
        //alert(claim_facility_num);
        if (claim.transfer_patient_data_to_claim_form == 'true')  
        {   
            if(claim_facility_num == $('#claim_facility_num').val())
            {    
                if($('#claim_doctor_id').val == '') {   
                    var nofacnum = 1;
                    $.each(claim.doctor.facilities, function(key, value) { 
                        if(value == data.facility_num) {
                           nofacnum++;
                           $('#claim_doctor_id').val(key).trigger('change.select2'); 
                        }
                    });
                    if(nofacnum == 1) {
                        $('#claim_doctor_id').val('').trigger('change.select2');
                    }
                }
                
            }
        }

        claim.determineClaimType();
        claim.handlePayProgChange();

        if ($('#claim_temporary_form_id').val()) {
            $('#claim_button_delete_temporary').show();
        }

        // if ($('#claim_health_num').val()) {
        //     //if (claim.enable_hcv) {
        //     claimHCV.validate(false);
        //     //}
        // }

        claim.handleDiscountedFieldSetup();

        if (!$('#claim_patient_id').val()) {
           $('#claim_health_num').focus();
        } else {
            $('#' + claim.items.form_prefix + '0_service_code').focus();
        }

        if (data.hasOwnProperty('doctor_id')) {
            claim.checkForInactiveDoctor(data.doctor_id);
        }

        if (claim.auto_fill_last_code && data.health_num) {
            claim.requestPatientHistory();
        }

        if (claim.actions.load_last_five_services && data.health_num) {
            claim.requestPatientHistory();
        }

        if (localStorage.getItem('show_split') == 'true') {
            $('#claim_split_claim').prop('checked', true);
            $('#split_doctor_row').show();
        }

        // if(patient_search.default_admit_date != '' && typeof d[facilityNum] != 'undefined') {
        // $('#claim_admit_date').val(patient_search.default_admit_date); 
        // }

        return false;

    },

    checkForInactiveDoctor: function (doctor_id, doctor_string) {
        if (doctor_id && !$('#claim_doctor_id').val()) {
            var errorJSON = {
                'doctor_id': {
                    'label': 'Doctor Profile',
                    'message': 'Doctor Profile ' + doctor_string + ' is inactive. Activate this profile or change the settings to include inactive doctors before editing this claim.'
                }
            };
            quickClaim.showFormErrors(errorJSON, '#form_message', '#claim', true);
        }

    },

    requestPatientHistory: function () {
        var patientID = $('#claim_patient_id').val();
        var data = 'patient_id=' + patientID;

        if (!patientID) {
            return;
        }

        if (patientID == claim.patientHistory.patientID) {
            return;
        }

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: claim.actions.patientHistory,
            data: data,
            success: function (rs) {
                claim.patientHistory.patientID = patientID;
                if (rs.hasOwnProperty('service') && rs['service'].hasOwnProperty('data')) {

                    $('#patientHistoryTableShort').find('tbody').empty();
                    $('#patientHistoryTable').find('tbody').empty();

                    var lastService = rs['service'].data[0];

                    if (lastService) {
                        if (claim.auto_fill_last_code) {
                            $('#claim_claim_items_0_service_code').val(lastService.service_code);
                            $('#claim_claim_items_0_diag_code').val(lastService.diag_code);
                            $('#claim_claim_items_0_num_serv').val(lastService.num_serv);
                        }

                        claim.services.retrieveServiceCode(lastService.service_code, 0);

                        $('#checkmark').show();
                        $('#messages_text').text(rs['service'].data.length + ' services retrieved.').show();

                        claim.patientHistory.resultColumns = ['doctor_name', 'service_code', 'num_serv', 'diag_code', 'service_date', 'charge_to', 'claim_status', 'fee_subm', 'fee_paid', 'errors', 'ref_doc', 'account_num', 'claim_id', 'facility_num', 'admit_date'];

                        for (var a in rs['service'].data) {
                            var className = rs['service'].data[a].claim_id == ($('#claim_id').val()) ? 'currentClaim' : '';
                            var tr = '<tr id="' + rs['service'].data[a].id + '" class="' + className + '">';
                            for (var idx in claim.patientHistory.resultColumns) {
                                tr += '<td class="' + claim.patientHistory.resultColumns[idx] + '">';
                                if (rs['service'].data[a].hasOwnProperty(claim.patientHistory.resultColumns[idx])) {
                                    tr += rs['service'].data[a][claim.patientHistory.resultColumns[idx]];
                                }
                                tr += ' </td>';
                            }
                            tr += '</tr>';
                            $('#patientHistoryTable').find('tbody').append($(tr));
                        }

                        if (claim.actions.load_last_five_services) {
                            var length = (rs['service'].data.length > 5 ? 5 : rs['service'].data.length);
                            for (var i = 0; i < length; i++) {
                                var className = rs['service'].data[i].claim_id == ($('#claim_id').val()) ? 'currentClaim' : '';
                                var tr = '<tr id="' + rs['service'].data[i].id + '" class="' + className + '">';
                                for (var idx in claim.patientHistory.resultColumns) {
                                    tr += '<td class="' + claim.patientHistory.resultColumns[idx] + '">';
                                    if (rs['service'].data[i].hasOwnProperty(claim.patientHistory.resultColumns[idx])) {
                                        tr += rs['service'].data[i][claim.patientHistory.resultColumns[idx]];
                                    }
                                    tr += ' </td>';
                                }
                                tr += '</tr>';
                                $('#patientHistoryTableShort').find('tbody').append($(tr));
                            }
                            $('#patientHistoryTableShort').trigger('update');
                        }

                        $('#patientHistoryTable').trigger('update');
                        if (rs['service'].data.length == 0) {
                            if ($("#quick_service_codes_div span").length > 35) {
                                $('#claim_item_table tbody').css('max-height', 'calc(100vh - 620px)');
                            } else {
                                $('#claim_item_table tbody').css('max-height', 'calc(100vh - 590px)');
                            }
                        } else if (rs['service'].data.length == 2) {
                            if ($("#quick_service_codes_div span").length > 35) {
                                $('#claim_item_table tbody').css('max-height', 'calc(100vh - 620px)');
                            } else {
                                $('#claim_item_table tbody').css('max-height', 'calc(100vh - 590px)');
                            }
                        }
                    }
                }
            },
            beforeSend: function (rs) {
                $("#patientHistoryTableShort").dataTable().fnDestroy();
                $("#patientHistoryTable").dataTable().fnDestroy();
                $("#patientHistoryTableShort thead tr:eq(1)").remove();
                $("#patientHistoryTable thead tr:eq(1)").remove();
                quickClaim.ajaxLocked = true;
                $('#checkmark').hide();
                $('#error_icon').hide();
                $('#spinner').show();

                $('#messages_text').text('Requesting Patient Service History').show();
            },
            complete: function (rs) {
                quickClaim.ajaxLocked = false;
                $('#spinner').hide();
                $('#' + claim.items.form_prefix + '0_service_code').select();
                if (!$.fn.DataTable.isDataTable('#patientHistoryTableShort')) {
                    global_js.simpleDatatable('patientHistoryTableShort');
                }
                if (!$.fn.DataTable.isDataTable('#patientHistoryTable')) {
                    global_js.customFooterDataTableWithOptions('patientHistoryTable', true);
                }
                if (claim.actions.load_last_five_services) {
                    if ($("#quick_service_codes_div span").length > 35) {
                        $('#claim_item_table tbody').css('max-height', 'calc(100vh - 535px)');
                    } else {
                        $('#claim_item_table tbody').css('max-height', 'calc(100vh - 515px)');
                    }
                    $('#claim_claim_items_0_service_code').focus();
                }

            },
            error: function (rs) {
                quickClaim.ajaxLocked = false;
                $('#error_icon').show();
                $('#messages_text').text('An error occurred while contacting the server.');
            }
        });
    },
    saveClaim: function () {
        if (quickClaim.ajaxLocked) {
            return;
        }
        $("#s2id_claim_doctor_id a").removeAttr('style');
        claim.items.checkMultipleServices();
        claim.calculate.totalBilled(false);

        var data = $('#claim_form').serialize();
        if (!$('#claim_save_temporary').prop('checked') && !$('#claim_id').val()) {
            if (!claim.always_show_temporary_button) {
                $('#temporary_save_row').hide();
            }

            $('#claim_save_temporary').prop('checked', false);
        }
        if (!$('#claim_save_anyways').prop('checked') && !$('#claim_id').val()) {
            $('#override_save_row').hide();
            $('#claim_save_anyways').prop('checked', false);
        }

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: claim.actions.claim_save,
            data: data,
            success: function (rs) {
                quickClaim.clearMessages('form_message', true);

                if (rs.hasOwnProperty('data') || rs.hasOwnProperty('temporary_id')) {
                    $('#results_text').text('').hide();

                    if ($('#claim_doctor_id').val() == claim.autofill.data.doctor_id) {
                        localStorage.setItem('doc_id', $('#claim_doctor_id').val());
                        localStorage.setItem('SLI', $('#claim_sli').val());

                        localStorage.setItem('facility_num', $('#claim_facility_num').val());
                        localStorage.setItem('facility_id', $('#claim_facility_id').val());
                        localStorage.setItem('facility_description', $('#claim_facility_description').val());
                    }

                    //
                    //localStorage.setItem('doc_id', $('#claim_doctor_id').val());

                    claim.autofill.data.doctor_id = $('#claim_doctor_id').val();
                    claim.autofill.data.facility_num = $('#claim_facility_num').val();
                    claim.autofill.data.facility_id = $('#claim_facility_id').val();
                    claim.autofill.data.facility_description = $('#claim_facility_description').val();
                    claim.autofill.data.sli = $('#claim_sli').val();
                    claim.autofill.data.last_service_date = claim.items.lastServiceDate();
                    claim.autofill.data.location_id = $('#claim_location_id').val();
                    claim.autofill.data.claim_type = $('#claim_pay_prog').val();

                    $('#claim_patient_doctor_id').val(rs.data[0]['doctor_id']);

                    //console.log('rs',rs.data[0]['doctor_id']);

                    if (rs.hasOwnProperty('next_claim')) {
                        claim.handleNextInLoopResults(rs);
                        claim.writeSuccessMessage(rs, 'results_text_multi', 'results_text_multi_parent');
                    } else {
                        $('#from_appointments_count').text('');
                        claim.clearForm(false);
                        if (claim.skipPatientSearch) {
                            claim.writeSuccessMessage(rs, 'results_text_multi', 'results_text_multi_parent');
                            $('#claim_health_num').focus();
                        } else {
                            claim.writeSuccessMessage(rs, 'success_message_text', 'success_message');                               
                            $('#tabs').tabs('option', 'active', 0);
                        }
                    }
                    window.history.pushState("Claims", "Claims - HYPE Medical", window.location.origin+"/claim");
                } else if (rs.hasOwnProperty('errors')) {
                    $('input').removeClass('cliponeErrorInput');
                    quickClaim.showFormErrors(rs.errors, '#form_message', '#claim', true);

                    var errors = rs.errors;
                    var lgn = 1;
                    var is_focus = false;
                    for (var e in errors) {
                        if (e == '_global_') {
                            $("#items_table tbody tr:eq(0) td:eq(0) input").addClass('cliponeErrorInput');
//                            $("#items_table tbody tr:eq(0) td:eq(0) input").attr('tabIndex', errors.length + 1);
                        } else {

                            if ($("#claim_" + e).is('select')) {
//                                $("#s2id_claim_" + e).prop('tabindex', lgn);
//                                        .on('focus', function(){
//                                    $("#claim_sex").select2('open');
//                                });
//                                $("#s2id_claim_"+e).on('focus', function(){

//                                    $(this).find('a').trigger('click');
//                                })
//                                    $("#claim_"+e).select2('open');
//                                });
                            } else {
                                if (is_focus == false) {
                                    $("#claim_" + e).focus();
                                    is_focus = true;
                                }

                            }
                            lgn++;
                        }
                    }
                    $('input[tabIndex="1"]').focus();

                    $('#temporary_save_row').show();
                    if (rs.hasOwnProperty('can_save_anyways') && rs['can_save_anyways']) {
                        $('#override_save_row').show();
                    } else {
                        $('#override_save_row').hide();
                    }

                    $('#error_icon').show();
                    $('#messages_text').text('Form Errors were detected').show();
                    var error_height = $("#form_message").height();
                    if (error_height < 200) {
                        error_height = error_height + 450;
                        $('#claim_item_table tbody').css('max-height', 'calc(100vh - ' + error_height + 'px)');
                    }

                }
            },
            beforeSend: function (rs) {
                quickClaim.ajaxLocked = true;
                $('#checkmark').hide();
                $('#error_icon').hide();
                $('#spinner').show();

                $('#messages_text').text('Saving Claim Data').show();
            },
            complete: function (rs) {
                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
                $('#spinner').hide();
            },
            error: function (rs) {
                quickClaim.ajaxLocked = false;
                $('#error_icon').show();
                $('#messages_text').text('An error occurred while contacting the server.');
            }
        });
    },
    writeSuccessMessage: function (data, textDiv, textDivParent) {
        if (data.hasOwnProperty('temporary_id')) {
            $('#' + textDiv).html('Saved data in temporary format. Access temporary data through the Dashboard.');
        } else {
            var text = 'Saved claim '
                    + '<a href="' + claim.actions.claim_edit + '?id=' + data.data[0]['id'] + ' " target="_blank"><b>' + data.data[0]['id'] + ' (' + data.data[0]['lname'] + ')</b></a>.  '
                    + '<br />PID: ' + data.data[0]['patient_id']
                    + '<br />Chart Number: ' + data.data[0]['patient_number'];

            for (var a in data.data[0]['actions']) {
                if (data.data[0]['actions'][a] == 'details_pdf') {
                    text += '<br /><b><a href="#" id="detailsPdf">Details PDF</a> &nbsp;';
                } else if (data.data[0]['actions'][a] == 'invoice_pdf') {
                    text += '<br /><a href="#" id="invoicePdf">Invoice PDF</a> &nbsp;';
                } else if (data.data[0]['actions'][a] == 'receipt_pdf') {
                    text += '<br /><a href="#" id="receiptPdf">Receipt PDF</a> &nbsp;';
                }
            }
            text += '<br /><a href="#" id="manualPdf">Manual Review PDF</a> &nbsp;';
            text += '<br /><a href="#" id="newClaimForPatient">New Claim For This Patient</a> &nbsp;</b>';
            text += '<input class="btn btn-primary" id="label_custom_template_claim_button" type="button" value="Custom Template" />';

            $('#' + textDiv).html(text);



            $('#detailsPdf').click(function () {
                claim.handlePdfClick(data.data[0]['id'], 'details');
            });
            $('#invoicePdf').click(function () {
                claim.handlePdfClick(data.data[0]['id'], 'invoice');
            });
            $('#receiptPdf').click(function () {
                claim.handlePdfClick(data.data[0]['id'], 'receipt');
            });
            $('#manualPdf').click(function () {
                claim.handlePdfClick(data.data[0]['id'], 'manual');
            });
            $('#newClaimForPatient').click(function () {
                claim.loadData({
                    patient_number: data.data[0]['patient_number'],
                    patient_id: data.data[0]['patient_id'],
                    lname: data.data[0]['lname'],
                    fname: data.data[0]['fname'],
                    health_num: data.data[0]['health_num'],
                    version_code: data.data[0]['version_code'],
                    dob: data.data[0]['dob'],
                    sex: data.data[0]['sex'],
                    province: data.data[0]['province'],
                    admit_date: data.data[0]['admit_date'],
                    ref_doc_id: data.data[0]['ref_doc_id'],
                    ref_doc_description: data.data[0]['ref_doc_num'],
                    ref_doc_num: data.data[0]['ref_doc_num']
                });
            });
        }

        $('#checkmark').hide();
        $('#' + textDivParent).show();
    },
    setFocus: function () {
    },
    setupRefdocAutocomplete: function () {
        $('#claim_ref_doc_description, #claim_ref_doc_description_lock').autocomplete({
            minLength: 2,

            select: function (event, ui) {
                if (ui.item.label == 'Create A New Referring Doctor') {
                    var value = ui.item.provider_num;
                    var alphasRE = new RegExp(/[a-z\-]/i);
                    var tokens = value.split(' ');

                    ui.item.provider_num = '';

                    for (var a in tokens) {
                        if (tokens[a].match(alphasRE)) {
                            if (!ui.item.hasOwnProperty('lname')) {
                                ui.item.lname = tokens[a];
                            } else if (!ui.item.hasOwnProperty('fname')) {
                                ui.item.fname = tokens[a];
                            } else {
                                ui.item.fname += ' ' + tokens[a];
                            }
                        } else {
                            ui.item.provider_num += tokens[a];
                        }
                    }

                    event.stopPropagation();
                    referringDoctorForm.edit(ui.item);
                } else {
                    claim.refDocSelected = true;
                    $('#claim_ref_doc_id').val(ui.item.id);
                    $('#claim_ref_doc_description').val(ui.item.label);
                    $('#claim_ref_doc_num').val(ui.item.provider_num);
                    event.stopPropagation();
                }
            },

            source: function (request, response) {
                if (request.term in refdoc_cache) {
                    response(refdoc_cache[request.term]);
                }

                $.ajax({
                    url: claim.actions.autocomplete_refdoc,
                    dataType: 'json',
                    data: request,
                    success: function (data) {
                        refdoc_cache[request.term] = data;
                        response(data);
                    }
                });
            }
        });
        $('#claim_ref_doc_description').blur(function () {
            if (!claim.refDocSelected) {
                if (!$(this).val()) {
                    $('#claim_ref_doc_id').val('');
                    $('#claim_ref_doc_description').val('');
                    $('#claim_ref_doc_num').val('');
                } else if ($('#claim_ref_doc_num').val() != $('#claim_ref_doc_description').val().substring(0, 6)) {
                    $('#claim_ref_doc_num').val($(this).val());
                    $('#claim_ref_doc_id').val('');

                }
            }

            claim.refDocSelected = false;
        });
    },
    fillInRefDoc: function (data) {
        $('#claim_doctor_id').focus();
        $('#claim_ref_doc_id').val(data.id);
        $('#claim_ref_doc_description').val(data.autocomplete);
        $('#claim_ref_doc_num').val(data.provider_num);
    },

    setupIFrame: function () {
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    setupHiddenForm: function (fields, action, prefix) {
        setTimeout(function () {
            var $div = $('#hiddenDownloader');

            var a = null;
            var frame_body = $div.contents().find('body');
            var form = '<form id="hidden_form" action="' + action + '" method="POST">';

            for (a in fields) {
                var name = fields[a].substring(0, fields[a].indexOf('='));
                var value = fields[a].substring(fields[a].indexOf('=') + 1);

                name = name.replace(past_claims.form_prefix, prefix);
                form += '<input type="text" value="' + value + '" id="' + name + '" name="' + name + '" />';
            }
            form += '</form>';
            frame_body.append($(form));
            $div.contents().find('#hidden_form').submit();
        });
    },
    ClearField: function () {
        $(".btnClearTxt").click(function () {
            var id = $(this).attr('data-id');
            var idlock = $(this).attr('data-lock-id');
            if (id == 'claim_facility_description' && idlock == 'claim_facility_description_lock') {
                //$("#claim_admit_date").skDP('setDate', null).hide();
                $("#claim_admit_date").val('');
                $("#claim_facility_num").val('');
                $("#claim_facility_id").val('');

                $("#claim_facility_num_lock").val('');
                $("#claim_facility_id_lock").val('');
            }
            if (id == 'claim_ref_doc_description' && idlock == 'claim_ref_doc_description') {
                $("#claim_ref_doc_id").val('');
                $("#claim_ref_doc_num").val('');
            }
            $("#" + id).val('');
        })
    }
};
