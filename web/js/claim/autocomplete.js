/**
 * Created by Scott on 10/7/2015.
 */
var autocomplete = {
    actions: {
        retrieve_codes: '/claim/autoCompleteCodes',
        retrieve_diag: '/claim/autoCompleteDiags'
    },
    diagCodeOptions: {
        delay: 300,
        minLength: 2,
        position: { my: 'left top', at: 'left bottom'},
        open: function(event, ui) {
            claim.autocomplete_open = true;
            $('.ui-helper-hidden-accessible').remove();

            event.stopImmediatePropagation();
        },
        close: function (event, ui) {
            claim.autocomplete_open = false;
            $('.ui-helper-hidden-accessible').remove();

            event.stopImmediatePropagation();
        },
        select: function( event, ui ) {
            $('.ui-helper-hidden-accessible').remove();
            $('#' + event.target.id).val(ui.item.value);
            $('.claim_item_row .diag_code').autocomplete('close');
            claim.autocomplete_open = true;

            event.stopImmediatePropagation();

            return false;
        },
        focus: function( event, ui ) {
            $('#' + event.target.id).val(ui.item.value);

            return false;
        },
        source: function (req, res) {
            var matches = req.term.match(/\d+/g);

            if (req.term.length > 2 && matches != null) {
                $('.claim_item_row .diag_code').autocomplete('close');
                claim.autocomplete_open = false;
                return;
            }

            if (lscache) {
                var result = lscache.get(req.term + '_diag');

                if (result) {
                    res(result);
                } else {

                    $.ajax({
                        dataType: 'json',
                        type: 'get',
                        data: 'code=' + req.term,
                        url: autocomplete.actions.retrieve_diag,
                        cache: true,
                        success: function (rs) {
                            if (typeof rs != "object") return;

                            var result_set = [];

                            for (code in rs) {
                                if (rs[code].description.includes('defined')) return;

                                result_set[code] = { label: rs[code].diag_code + ': ' + rs[code].description, value: rs[code].diag_code } ;
                            }

                            res(result_set);
                            lscache.set(req.term + '_diag', result_set, 360000);
                        }
                    });
                }
            }
        }
    },
    options: {
        delay: 200,
        minLength: 2,
        position: { my: 'left top', at: 'left bottom'},
        open: function(event, ui) {
            //$('#results').css('top', $('.ui-autocomplete').height() + 8);
            claim.autocomplete_open = true;
            $('.ui-helper-hidden-accessible').remove();

            event.stopImmediatePropagation();
        },
        close: function (event, ui) {
            //$('#results').css('top', 0);
            claim.autocomplete_open = false;
            $('.ui-helper-hidden-accessible').remove();

            event.stopImmediatePropagation();
        },
        select: function( event, ui ) {
            $('.ui-helper-hidden-accessible').remove();
            $('#' + event.target.id).val(ui.item.value);
            $('.claim_item_row .service_code').autocomplete('close');
            claim.autocomplete_open = true;
            var currentStage = event.target.id.match(/\d+/);
            var nextStage = +currentStage[0] + 1;
            
           $('#' + event.target.id.replace(currentStage[0], nextStage)).focus();
            event.stopImmediatePropagation();

            return false;
        },
        focus: function( event, ui ) {
            $('#' + event.target.id).val(ui.item.value);

            return false;
        },
        source: function (req, res) {
            if (lscache) {
                var result = lscache.get(req.term + '_serv');

                if (result) {
                    res(result);
                } else {
                    //var matches = req.term.match(/\d+/g);
                    //if (matches != null) {
                    //    return;
                    //}

                    $.ajax({
                        dataType: 'json',
                        type: 'get',
                        data: 'code=' + req.term,
                        url: autocomplete.actions.retrieve_codes,
                        cache: true,
                        success: function (rs) {
                            if (typeof rs != "object") return;
                            var result_set = [];

                            for (code in rs) {
                                result_set[code] = { label: rs[code].service_code + ': ' + rs[code].description, value: rs[code].service_code } ;
                            }

                            res(result_set);
                            lscache.set(req.term + '_serv', result_set, 360000);
                        }
                    });
                }
            }
        }
    },
    init: function () {
        //ticket 3045, not sure why it was here but...
        //if (window.location.search.length == 0) {
            $('.claim_item_row .diag_code').autocomplete(autocomplete.diagCodeOptions);
            $('.claim_item_row .service_code').autocomplete(autocomplete.options);
            $('.ui-helper-hidden-accessible').remove();
        //}
    }
};


