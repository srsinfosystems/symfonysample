var details = {
    actions: {
        index: '',
        resubmit: '',
        submit: '',
        invoice_pdf: '',
        ohip_pdf: '',
        history_csv: '',
        mark_paid: '',
        mark_closed: '',
        delete_claim: '',
        reverse_direct_payment: ''
    },

    initialize: function () {
        if ($('#items_table tbody tr').size()) {
            $('#items_table').tablesorter1({
                widgets: ['zebra', 'hover']
            });
        }
        if ($('#direct_payments tobdy tr').size()) {
            $('#direct_payments').tablesorter1({
                widgets: ['zebra', 'hover']
            });
        }
        if ($('#invoice_payments tobdy tr').size()) {
            $('#invoice_payments').tablesorter1({
                widgets: ['zebra', 'hover']
            });
        }

        if ($('#claim_history_log_table tbody tr').size()) {
            $('#claim_history_log_table').tablesorter1({
                widgets: ['zebra', 'hover'],
                headers: {
                    0: {
                        sorter: 'sortDate-dmyHia'
                    }
                }
            });
        }

        if ($('#taxes_table tbody tr').size()) {
            $('#taxes_table').tablesorter1({
                widgets: ['zebra', 'hover']
            });
        }

        $('#edit_button').button();
        $('#edit_button').click(function () {
            window.location = details.actions.index;
        });

        $('.resubmit_button').button();
        $('.resubmit_button').click(function () {
            window.location = details.actions.resubmit;
        });

        $('#submit_button').button();
        $('#submit_button').click(function () {
            window.location = details.actions.submit;
        });

        $('#invoice_pdf').button();
        $('#invoice_pdf').click(function () {
            details.setupHiddenDownloader(details.actions.invoice_pdf + '?type=details');
        });

        $('#invoice_pdf_invoice').button();
        $('#invoice_pdf_invoice').click(function () {
            details.setupHiddenDownloader(details.actions.invoice_pdf + '?type=invoice');
        });

        $('#invoice_pdf_receipt').button();
        $('#invoice_pdf_receipt').click(function () {
            details.setupHiddenDownloader(details.actions.invoice_pdf + '?type=receipt');
        });

        $('#ohip_pdf').button();
        $('#ohip_pdf').click(function () {
            details.setupHiddenDownloader(details.actions.ohip_pdf);
        });

        $('#download_history_csv').button();
        $('#download_history_csv').click(function () {
            details.setupHiddenDownloader(details.actions.history_csv);
        });

        $('#history_button').button();
        $('#history_button').click(function () {
            $('#claim_history_log_dialog').dialog('open');
        });

        $('.mark_closed').button();
        $('.mark_closed').click(function () {
            var itemID = $(this).prop('id').replace('mark_closed_', '');
            claimStatusActions.markClosed.openDialog(new Array(itemID));
        });

        $('.mark_paid').button();
        $('.mark_paid').click(function () {
            var itemID = $(this).prop('id').replace('mark_paid_', '');
            claimStatusActions.markPaid.openDialog(new Array(itemID));
        });

        $('#delete_button').button();
        $('#delete_button').click(function () {
            $('#delete_confirm').dialog('open');
        });

        $("#delete_confirm").dialog({
            resizable: false,
            height: 200,
            modal: true,
            autoOpen: false,
            buttons: [
                {
                    text: "Delete Claim",
                    "class": 'btn btn-success btn-squared',
                    click: function () {
                        window.location = details.actions.delete_claim;
                    }
                }, {
                    text: "Cancel",
                    "class": 'btn btn-danger btn-squared',
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ]

        });

        $('#claim_history_log_dialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            minWidth:800,
            minHeight:250,
            modal: true,

        });

        $('#reverse_payment').button();
        $('#reverse_payment').click(function () {
            $('#refund_dialog').dialog('open');
        });

        $('#refund_dialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: $(window).height() - 100,
            modal: true,
            width: $(window).width() - 100
        });

        $('#refund_payment_cheque_number').closest('tr').addClass('cheque_num');
        $('.cheque_num').hide();

        $('#refund_payment_card_type_VISA').closest('tr').addClass('card_type');
        $('.card_type').hide();

        $('#refund_payment_other_card').closest('tr').addClass('other_card');
        $('.other_card').hide();

        $('#refund_payment_transaction_number').closest('tr').addClass('transaction_num');
        $('.transaction_num').hide();

        $('#refund_payment_total_paid').closest('tr').before($('<tr><td colspan="2">&nbsp;</td></tr>'));

        $('input[name="refund_payment[payment_method]"]').click(function () {
            var val = $(this).val();

            $('.cheque_num').hide();
            $('.card_type').hide();
            $('.transaction_num').hide();

            if (val == 'CASH') {
            } else if (val == 'CHEQUE') {
                $('.cheque_num').show();
            } else if (val == 'CREDIT') {
                $('.card_type').show();
                $('.transaction_num').show();
            } else if (val == 'DEBIT') {
                $('.transaction_num').show();
            }
        });

        $('input[name="refund_payment[card_type]"]').click(function () {
            var val = $(this).val();

            $('.other_card').hide();

            if (val == 'OTHER') {
                $('.other_card').show();
            }
        });
    },
    setupHiddenDownloader: function (url) {
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" style="width: 0; height: 0; border: none; display: none;"></iframe>');

        setTimeout(function () {
            var frame_body = $('#hiddenDownloader').contents().find('body');
            var form = '<form id="hidden_form" action="' + url + '" method="POST">';
            form += '</form>';
            frame_body.append($(form));
            $('#hiddenDownloader').contents().find('#hidden_form').submit();
        });
    },
    triggerPageRefresh: function () {
        location.reload();
    }
}