/**
 * Created by Scott on 8/12/2016.
 */
var mcedt = {
    actions: {
        info_url: null
    }, 
    doctors: null,
    initialize: function () {
        mcedt.doctors = JSON.parse(mcedt.doctors);

        var html = '';
        for (var key in mcedt.doctors) {
            if (mcedt.doctors.hasOwnProperty(key)) {
                html += '<option value="' + key + '">' + mcedt.doctors[key] + '</option>';
            }
        }

        $('#doctor_select').append(html);

        $('#pull_list_btn').button();

        $('#pull_list_btn').click(function (ev) {
            
            var dataURL = 'doc_id=' + $('#doctor_select').val() + '&status=' + $('#res_status').val() + '&type=' + $('#res_type').val();

            $.ajax({
                type: 'post',
                data: dataURL,
                url: mcedt.actions.info_url,
                success: function (rs) {
                    var resultSet = JSON.parse(rs);
                    if (resultSet) {
                        var html = '';
                        for (var result in resultSet.data) {
                            html += '<tr> <td>' + resultSet.data[result]['description'] + '</td> <td>' + resultSet.data[result]['resourceID'] + '</td> <td>'
                                    + resultSet.data[result]['status'] + '</td> <td>' + Date(resultSet.data[result]['createTimestamp']) + '</td> <td>' + Date(resultSet.data[result]['modifyTimestamp']) + '</td> </tr>';

                        }
                        $('#mcedt_table').append(html);
                        
                    }else{
                        $.notify('No Results Found', {
                            element: 'body',
                            type: "danger",
                            allow_dismiss: true,
                            newest_on_top: true,
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            delay: 5000,
                            z_index: 99999,
                            mouse_over: 'pause',
                            animate: {
                                enter: 'animated bounceIn',
                                exit: 'animated bounceOut'
                            },
                            template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                            <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                            <span data-notify="icon"></span>
                                            <span data-notify="title">{1}</span>
                                            <span data-notify="message">{2}</span>
                                            <div class="progress" data-notify="progressbar">
                                                    <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <a href="{3}" target="{4}" data-notify="url"></a>
                                    </div>`

                                });
                    }
                    global_js.customFooterDataTable('mcedt_table_main', true);
                },
                beforeSend: function () {
                    $("#mcedt_table_main").dataTable().fnDestroy();
                    $("#mcedt_table_main thead tr:eq(1)").remove();
                    $("#spinner").attr('style','display:block');
                    $("#messages_text").text("Searching List of MCEDT");
                },
                complete: function () {
                    $("#spinner").attr('style','display:none');
                $("#messages_text").text("");
                },
                error: function () {

                }
            });
        });
    }
}