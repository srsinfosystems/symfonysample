var consultation = {
	actions: {
		save: null
	},
	consult_length: 0,
	ajax_lock: false,

	prefix: 'consultation',
	row_prefix: 'consultations',
	row_count: 0,

	initialize: function() {
		consultation.row_count = $('#form_table tbody tr.form_row').size();

		$('#save_schedule')
			.button()
			.click(function() {
				consultation.handleSaveClick();
			});
	},
	initializeRow: function(c) {
                sk.datepicker('#' + consultation.prefix + '_' + consultation.row_prefix + '_' + c + '_start_date');
                sk.datepicker('#' + consultation.prefix + '_' + consultation.row_prefix + '_' + c + '_end_date');
                
		$('#' + consultation.prefix + '_' + consultation.row_prefix + '_' + c + '_start_date').on('changeDate', function(e){
                    $('#' + consultation.prefix + '_' + consultation.row_prefix + '_' + c + '_end_date').skDP('setStartDate', e.date);
                    $('#' + consultation.prefix + '_' + consultation.row_prefix + '_' + c + '_end_date').skDP('update', $(this).val());
                }).blur(function(e) {
			consultation.getWeekday(c);
		});
		consultation.getWeekday(c);

		$('#' + consultation.prefix + '_' + consultation.row_prefix + '_' + c + '_end_date').blur(function(e) {
                    $('#' + consultation.prefix + '_' + consultation.row_prefix + '_' + c + '_start_date').skDP('setEndDate', e.date);
		});

		var prefix = consultation.prefix + '_' + consultation.row_prefix + '_' + c;
		$('#' + prefix + '_start_date').blur(function () {
			consultation.checkForLastRow(c);
		});
		$('#' + prefix + '_max_attendees').change(function () {
			consultation.checkForLastRow(c);
		});
		$('#' + prefix + '_start_time_hour').change(function () {
			consultation.checkForLastRow(c);
			consultation.setEndTime(c);
		});
		$('#' + prefix + '_start_time_minute').change(function () {
			consultation.checkForLastRow(c);
			consultation.setEndTime(c);
		});
		$('#' + prefix + '_end_time_hour').change(function () {
			consultation.checkForLastRow(c);
		});
		$('#' + prefix + '_end_time_minute').change(function () {
			consultation.checkForLastRow(c);
		});
		$('#' + prefix + '_recurrence_type').change(function () {
			consultation.checkForLastRow(c);
		});
		$('#' + prefix + '_end_date').change(function () {
			consultation.checkForLastRow(c);
		});

		$('#clear_' + c).click(function() {
			consultation.deleteRow(c);
		});
	},
	setEndTime: function(row_num) {
		var start_hr =  $('#' + consultation.prefix + '_' + consultation.row_prefix + '_' + row_num + '_start_time_hour').val();
		var start_min = $('#' + consultation.prefix + '_' + consultation.row_prefix + '_' + row_num + '_start_time_minute').val();

		var hr = parseInt(start_hr, 10);
		var min = start_min ? parseInt(start_min, 10) : 0;

		min += parseInt(consultation.consult_length, 10);

		hr = hr + Math.floor(min / 60);
		min = min % 60;

		$('#' + consultation.prefix + '_' + consultation.row_prefix + '_' + row_num + '_end_time_hour').val(hr);
		$('#' + consultation.prefix + '_' + consultation.row_prefix + '_' + row_num + '_end_time_minute').val(min);
	},
	deleteRow: function(row_num) {
		var num_rows = $('#form_table tbody tr').size();

		if (num_rows > 1) {
			$('#form_row_' + row_num).remove();
			$('#' + this.prefix + '_' + this.row_prefix + '_' + row_num + '_id').remove();
		}
	},
	handleSaveClick: function() {
		if (consultation.ajax_lock) {
			return;
		}
		$('#consultation_form :input.error').removeClass('error');
		$('#error_message').hide();
		$('#error_message').text('');

		var data = $('#consultation_form').serialize();

		$.ajax({
			type: 'post',
			dataType: 'json',
			url: consultation.actions.save,
			data: data,
			success: function(rs) {
				if (rs.hasOwnProperty('errors')) {
					quickClaim.showFormErrors(rs.errors, '#error_message', '#general_schedule_form', true);
				}
				else if (rs.hasOwnProperty('success')) {
					$('#success_message_text').text('Saved Consultation Template.  Refreshing the page');
					$('#spinner').show();
					$('#success_message').show();

					setTimeout("location.reload(true);", 2000);
				}
			},
			beforeSend: function() {
				consultation.ajax_lock = true;
				$('#spinner').show();
				$('#spinner_text').text('Saving consultation templates');
			},
			complete: function() {
				consultation.ajax_lock = false;
				$('#spinner').hide();
				$('#spinner_text').text('');
			}
		});

	},
	checkForLastRow: function(row_num) {
		var last_row = $('#form_table tbody tr:last').attr('id');
		last_row = last_row.replace('form_row_', '');

		if (row_num != last_row) {
			return false;
		}
		consultation.insertRow();
	},
	insertRow: function() {
		var old_count = 0;

		var clonedRow = $("#form_table tbody tr#form_row_0").clone();
		$(clonedRow).attr('id', 'form_row_' + consultation.row_count);
		$('.row_num', $(clonedRow)).text(consultation.row_count + 1);

		clonedRow = consultation.setupNewRowIds(clonedRow, old_count);
		$("#form_table tbody").append($(clonedRow));
		consultation.drawNewHiddenRowItem('id', old_count);

		$('select', $(clonedRow)).each(function() {
			$(this).attr('selectedIndex', 0);
		});
		$('input', $(clonedRow)).each(function() {
			$(this).val('');
		});

		consultation.initializeRow(consultation.row_count);
	    consultation.row_count++;
	},
	getWeekday: function(row_num) {
		var dow = '';
		var date = $('#' + consultation.prefix + '_' + consultation.row_prefix + '_' + row_num + '_start_date').val();
		date = Date.parseExact(date, quickClaim.getParseDateFormat(quickClaim.dateFormat));

		if (date) {
			dow = date.toString('ddd');
		}
		$('#row_' + row_num + '_weekday').text(dow);
	},
	setupNewRowIds: function(row, old) {
		$(":input", row).each(function(e) {
			id = this.id.replace('_' + old + '_', '_' + consultation.row_count + '_');
			$(this).attr('id', id);
			name = $(this).attr('name').replace('[' + old + ']', '[' + consultation.row_count + ']');
			$(this).attr('name', name);
			$(this).val('');
		});
		$('.hasDatepick', row).each(function() {
			$(this).removeClass('hasDatepick');
		});
		$('span', row).each(function(e) {
			id = this.id.replace('_' + old + '_', '_' + consultation.row_count + '_');
			$(this).attr('id', id);
			$(this).val('');
		});
		return row;
	},
	drawNewHiddenRowItem: function(name, old) {
		old = 0;
		field = $("#" + consultation.prefix + '_' + consultation.row_prefix + '_' + old + '_' + name).clone();

		$(field).attr('id', $(field).attr('id').replace('_' + old + '_', '_' + consultation.row_count + '_'));
		$(field).attr('name', $(field).attr('name').replace('[' + old + ']', '[' + consultation.row_count + ']'));
		$(field).val('');

		$("#" + consultation.prefix + '_' + consultation.row_prefix + '_' + old + '_' + name).parent().append(field);
	}
};