var card_swipe = {
	actions: {
		appt_search: null,
		hcn_search: null
	},
	reader: null,
	replace_first_name: true,
	results_modal: 'search_results_dialog',
	merge_modal: 'hcn_merge_dialog',

	initialize: function() {
		this.reader = new CardReader();
		this.reader.observe($(document));

		this.reader.cardRead(function (value) {
			card_swipe.handleCardRead(value);
		});
	},

	handleCardRead: function(value) {
		var tracks = value.split('^');

		var hcn_num = value.substr(7,10);
		var name = tracks[1];
		var expiry_date = tracks[2].substr(0, 4);
		var sex = tracks[2].substr(7, 1);
		var dob = tracks[2].substr(8, 8);
		var version_code = tracks[2].substr(16, 2);

		name = name.split('/');
		var fname = name[1];
		var lname = name[0];

		var data = 'hcn[health_num]=' + hcn_num + '&'
			+ 'hcn[fname]=' + fname + '&'
			+ 'hcn[lname]=' + lname + '&'
			+ 'hcn[expiry_date]=' + expiry_date + '&'
			+ 'hcn[sex]=' + sex + '&'
			+ 'hcn[dob]=' + dob + '&'
			+ 'hcn[version_code]=' + version_code;

        if ($('#patient_hcn_version_code').val() != version_code) {
            $('#patient_hcn_version_code').val(version_code);
        }

		$.post(card_swipe.actions.hcn_search, data, function(rs) {
			if (rs.data.length == 1) {
				patient_edit.loadData(rs.data[0]);
				switchTabs('patient_edit');
			}
			else {
				patient_edit.loadData(rs.hcn_details);
				switchTabs('patient_edit');
			}
//			else {
//				quickClaim.modals.addModal(card_swipe.results_modal);
//				card_swipe.drawSearchResultsTable(rs.header, rs.data, rs.hcn_details);
//				card_swipe.drawHcnDetails(rs.hcn_details, rs.header);
//			}
		}, 'json');
	}
}