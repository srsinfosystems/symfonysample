var card_swipe = {
    actions: {
        appt_search: null,
        hcn_search: null
    },
    reader: null,
    replace_first_name: true,
    results_modal: 'search_results_dialog',
    merge_modal: 'hcn_merge_dialog',

    initialize: function () {

        card_swipe.reader = new CardReader();
        card_swipe.reader.observe($(document));

        card_swipe.reader.cardRead(function (value) {
            sk.hcmhidemsg = true;
            if (patient.is_modal) {
                if (quickClaim.modals.currentModal() == patient.form_dialog) {
                    card_swipe.handleCardRead(value);
                } else {
                    if (quickClaim.modals.currentModal() == null) {
                        card_swipe.handleCardReadApptBook(value);
                    }
                    return false;
                }
            } else if (value) {
                card_swipe.handleCardRead(value);
            }
        });
    },
    drawHcnDetails: function (details, header) {
        table = '<h4>Health Card Details [click to create new patient record]</h4>';
        table += '<table id="hcn_details" class="tablesorter1">';

        table += '<thead><tr>';
        for (var a in details) {
            title = a;
            if (header.hasOwnProperty(a)) {
                title = header[a];
            }

            table += '<th>' + title + '</th>';
        }
        table += '</tr></thead>';

        table += '<tbody><tr id="hcn_new_patient">';
        for (var a in details) {
            table += '<td>' + details[a] + '</td>';
        }

        table += '</tr></tbody></table>';
        $('#' + card_swipe.results_modal).prepend($(table));

        if ($('#hcn_details tbody tr').size()) {
            $('#hcn_details').tablesorter1({
                widgets: ['zebra', 'hover']
            });
        }

        $('#hcn_new_patient').click(function () {
            $('#' + patient.form_id).clearForm(patient.error_message);
            quickClaim.formFill(details, '#patient_', '', '#' + patient.error_message, 'Loaded Patient Data');
            patient.switchMode(2);
            if (quickClaim.modals.currentModal() == card_swipe.results_modal) {
                quickClaim.modals.removeModal(card_swipe.results_modal);
            }
            $('patient_address').focus();
        });
    },
    drawSearchResultsTable: function (header, data, card_data) {
        table = '<h4>Closest Matches [click for merging options]</h4>';
        table += '<table class="tablesorter1" id="hcn_results_table">' + '<thead><tr>';

        for (var key in header) {
            if (header.hasOwnProperty(key)) {
                table += '<th>' + header[key] + '</th>';
            }
        }
        table += '</thead></tr>' + '<tbody></tbody></table>';
        $('#' + card_swipe.results_modal).html(table);

        for (var key in data) {
            row = '<tr id="hcn_result_' + key + '">';
            for (var key2 in header) {
                var d = data[key][key2] == null ? ' ' : data[key][key2];
                row += '<td class="' + key2 + '">' + d + '</td>';
            }
            row += '</tr>';
            $(row).appendTo($('#' + card_swipe.results_modal + ' table tbody'));

            $('#hcn_result_' + key).click(function () {
                k = this.id.replace('hcn_result_', '');

                data[k].dob = card_data.dob;
                if (card_data.hcn_exp_year) {
                    data[k].hcn_exp_year = card_data.hcn_exp_year;
                }
                data[k].hcn_num = card_data.hcn_num;
                data[k].hcn_version_code = card_data.hcn_version_code;
                data[k].lname = card_data.lname;
                data[k].sex = card_data.sex;
                if (card_swipe.replace_first_name) {
                    data[k].fname = card_data.fname;
                }

                quickClaim.formFill(data[k], '#patient_', '', '#' + patient.error_message, 'Loaded Patient Data');
                patient.switchMode(3);
                if (quickClaim.modals.currentModal() == card_swipe.results_modal) {
                    quickClaim.modals.removeModal(card_swipe.results_modal);
                }
                $('patient_address').focus();

            });
        }

        if ($('#hcn_results_table tbody tr').size()) {
            $('#hcn_results_table').tablesorter1({
                widgets: ['zebra', 'hover']
            });
        }
    },
    handleCardReadApptBook: function (value) {
        var tracks = value.split('^');
        var hcn_num = value.substr(7, 10);
        var name = tracks[1].split('/');
        var fname = name[1];
        var lname = name[0];

        var data = 'hcn[health_num]=' + hcn_num + '&'
                + 'hcn[fname]=' + fname + '&'
                + 'hcn[lname]=' + lname;
    },
    handleCardRead: function (value) {
        var tracks = value.split('^');

        var hcn_num = value.substr(7, 10);
        var name = tracks[1];
        var expiry_date = tracks[2].substr(0, 4);
        var sex = tracks[2].substr(7, 1);
        var dob = tracks[2].substr(8, 8);
        var version_code = tracks[2].substr(16, 2);
        var status = 0;

        name = name.split('/');
        var fname = name[1];
        var lname = name[0];

        var data = 'hcn[health_num]=' + hcn_num + '&'
                + 'hcn[fname]=' + fname + '&'
                + 'hcn[lname]=' + lname + '&'
                + 'hcn[expiry_date]=' + expiry_date + '&'
                + 'hcn[sex]=' + sex + '&'
                + 'hcn[dob]=' + dob + '&'
                + 'hcn[version_code]=' + version_code + '&'
                + 'hcn[status_text]=' + status;
        patient_search.is_cardswiped = true;
        sk.hcmhidemsg = true;
        $.post(card_swipe.actions.hcn_search, data, function (rs) {
            
            
            if (rs.data.length == 1) {
                quickClaim.formFill(rs.data[0], '#patient_', '', '#' + patient.error_message, 'Loaded Patient Data');
                patient.setStatus(rs.data[0].status);
                patient.switchMode(3);
            } else if (rs.data.length == 0) {
                quickClaim.formFill(rs.hcn_details, '#patient_', '', '#' + patient.error_message, 'Loaded Patient Data');
                patient.switchMode(3);
            } else {
                quickClaim.modals.addModal(card_swipe.results_modal);
                card_swipe.drawSearchResultsTable(rs.header, rs.data, rs.hcn_details);
                card_swipe.drawHcnDetails(rs.hcn_details, rs.header);
            }
            setTimeout(function(){sk.hcmhidemsg = false;},5000);
        }, 'json');
    }
};