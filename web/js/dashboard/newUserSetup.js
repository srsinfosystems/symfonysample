/**
 * Created by Scott on 4/28/2017.
 */

var setup = {
    variables: {
        doctorCount: 0,
        claimCount: false,
        hasCreatedClaim: false,
        claimTour: new Tour({
            onStart: function (tour) {
                claim.tour_running = true;
            },
            onEnd: function (tour) {
                claim.tour_running = false;
            },
            steps: [{
                backdrop: true,
                element: "#srsTourTab",
                placement: 'bottom',
                title: "Search",
                content: "Search for a patient to begin with.",
                duration: 10000,
                onNext: function (tour) {
                    $('#tabs').tabs('option', 'active', 1);
                }
            }, {
                backdrop: true,
                element: "#claim_health_num",
                title: "HCV",
                content: "You can also create a new patient from health card validation, it will pull the patient data from OHIP!",
                duration: 10000,
                onShow: function (tour) {
                    $('#claim_health_num').val('2431731831');
                    $('#validateButton').click();
                }
            }, {
                backdrop: true,
                element: "#claim_pay_prog",
                title: "Pay Program",
                content: "Select which method of payment you'd like here, we support third party and direct!",
                duration: 10000
            }, {
                backdrop: true,
                element: "#claim_doctor_id",
                title: "Doctor",
                content: "Select which doctor performed this service, the facility and SLI will automatically be filled in from the doctor profile.",
                duration: 10000
            }, {
                backdrop: true,
                backdropContainer: 'body',
                element: "#claim_claim_items_0_service_code",
                title: "Service Codes",
                content: "Finally, here is where you enter your service codes, there are a lot of shortcuts to make this process easier for you.",
                duration: 10000
            }, {
                backdrop: true,
                element: "#claim_claim_items_0_service_code",
                title: "Service Codes",
                content: "To begin, we'll enter A7 into the service code field and then press TAB. Hype Medical will automatically load your code details.<br> You can also type any word from the description to bring up a list of matching codes!",
                duration: 10000,
                onShow: function (tour) {
                    $('#claim_claim_items_0_service_code').val('A7');
                    claim.services.handleCodeLookup(0);
                }
            }, {
                backdrop: true,
                element: "#claim_claim_items_0_diag_code",
                title: "Diagnostic Codes",
                content: "Next let's fill out the diagnostic code...",
                duration: 10000,
                onShown: function (tour) {
                    $('#claim_claim_items_0_diag_code').val('foot');
                }
            }, {
                backdrop: true,
                element: ".ui-menu-item :first",
                title: "Diagnostic Codes",
                content: "...and our intelligent autocomplete will suggest some possible options for you. You can hover any of the options to see what diagnostic code it is and click to select.",
                duration: 10000,
                onShown: function (tour) {
                    $('#claim_claim_items_0_diag_code').autocomplete('search', 'foot');
                },
                onShow: function (tour) {
                    $('#claim_claim_items_0_diag_code').autocomplete('search', 'foot');
                },
                onNext: function (tour) {
                    $('#claim_claim_items_0_diag_code').val(754);
                }
            }, {
                backdrop: true,
                element: "#claim_button_save",
                title: "Saving",
                content: "That's all you need! Simply hit save and the claim will be waiting to go to OHIP on your command. Check out the status of your claims at 'OHIP Files' under the Claims menu.",
                duration: 10000
            }]
        }),
        tour: new Tour({
            onEnd: function(tour) {
                setup.actions.promptRedirectForDoctor();
            },
            steps: [{
                backdrop: true,
                element: "#patient",
                title: "Patient",
                content: "This is the patient module, create and modify patient profiles here.",
                duration: 10000
            }, {
                backdrop: true,
                element: "#appointment_book",
                title: "Appointment Book",
                content: "Here is our Appointment Book, schedule existing patients here. You can also create appointments from the Patient module.",
                duration: 10000
            }, {
                backdrop: true,
                element: "#claim",
                title: "Claims",
                content: "the most important part of Hype Medical, claims. Create new claims from here or from any of the other modules.",
                duration: 10000
            }, {
                backdrop: true,
                element: "#doctor",
                title: "Doctor Setup",
                content: "Add or edit your doctor profiles here, also contains the MOH information.",
                duration: 10000
            }, {
                backdrop: true,
                element: "#sys_admin",
                title: "System Settings",
                content: "Here is where you can modify the many hundred settings Hype Medical has, leave the defaults or customize the software to work exactly how you want it to.",
                duration: 10000
            }, {
                backdrop: true,
                element: "#reports_widget",
                title: "Quick Reports",
                content: "Here are quick reports, they will tell you how your practice is doing and remind you of any missed opportunities.",
                duration: 10000
            }, {
                backdrop: true,
                placement: 'left',
                element: "#create_claims_widget",
                title: "Bill Appointment Widget",
                content: "Here you can quickly find out if you have any outstanding appointments that still need to be billed.",
                duration: 10000
            }, {
                backdrop: true,
                element: "#tabs",
                title: "Doctor Setup",
                content: "Welcome to the doctor module, create your doctor profile to start using Hype Medical!",
                duration: 10000,
                path: '/provider/edit'

            }]
        })
    },
    actions: {
        //redirect
        promptNewDoctor: function() {
            alertify.prompt( 'Add a Doctor', 'Welcome to Hype Medical! Please enter the billing number of your primary doctor', '000000',
                function(evt, value) {
                    $.notify('You entered: ' + value, {
                            element: 'body',
                            type: "success",
                            allow_dismiss: true,
                            newest_on_top: true,
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            delay: 10000,
                            z_index: 99999,
                            mouse_over: 'pause',
                            animate: {
                                enter: 'animated bounceIn',
                                exit: 'animated bounceOut'
                            },
                            template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                            <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                            <span data-notify="icon"></span>
                                            <span data-notify="title">{1}</span>
                                            <span data-notify="message">{2}</span>
                                            <div class="progress" data-notify="progressbar">
                                                    <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <a href="{3}" target="{4}" data-notify="url"></a>
                                    </div>`

                                });
                }, function() {
                    $.notify('Canceled new doctor setup', {
                            element: 'body',
                            type: "danger",
                            allow_dismiss: true,
                            newest_on_top: true,
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            delay: 10000,
                            z_index: 99999,
                            mouse_over: 'pause',
                            animate: {
                                enter: 'animated bounceIn',
                                exit: 'animated bounceOut'
                            },
                            template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                            <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                            <span data-notify="icon"></span>
                                            <span data-notify="title">{1}</span>
                                            <span data-notify="message">{2}</span>
                                            <div class="progress" data-notify="progressbar">
                                                    <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <a href="{3}" target="{4}" data-notify="url"></a>
                                    </div>`

                                });
                });
        },
        promptRedirectForDoctor: function() {
            alertify.confirm('Add a Doctor', '<h3>Welcome to Hype Medical!</h3> <br> In order to make a claim you need to create a doctor, would you like to do that now?',
                function(){
                    window.location.href = "/provider/edit";
                },
                function(){
                    localStorage.setItem('firstTimeUser', true);
                    $.notify('Click Doctor Profiles to add a doctor later!', {
                            element: 'body',
                            type: "danger",
                            allow_dismiss: true,
                            newest_on_top: true,
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            delay: 5000,
                            z_index: 99999,
                            mouse_over: 'pause',
                            animate: {
                                enter: 'animated bounceIn',
                                exit: 'animated bounceOut'
                            },
                            template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                            <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                            <span data-notify="icon"></span>
                                            <span data-notify="title">{1}</span>
                                            <span data-notify="message">{2}</span>
                                            <div class="progress" data-notify="progressbar">
                                                    <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <a href="{3}" target="{4}" data-notify="url"></a>
                                    </div>`

                                });
                });
        }
    },
    events: {
        init: function() {
            $( document ).ready(function() {
                if (setup.variables.doctorCount == 0 && localStorage.getItem('firstTimeUser') != "true") {
                    setup.variables.tour.init();
                    setup.variables.tour.start();
                }
            });
        },
        setupTourButtonClaims: function() {
            setup.variables.claimTour.init();

            $('#tourBtn').click(function() {
                $('#tabs').tabs('option', 'active', 0);

                setup.variables.claimTour.end();
                setup.variables.claimTour.restart();
            });
        }
    }
}
