var unacknowledgedClaims = {
    tblId: 'unacknowledgedClaimsTable',
    actions: {
        search: null
    },
    initialize: function() {
        $('#unacknowledged_claims_report').click(function() {
            unacknowledgedClaims.handleRequest(false);
        });

        $('#unacknowledgedunacknowledgedClaimsSelectAllRadio').button();
        $('#unacknowledgedClaimsSelectAllRadio').click(function() {
            unacknowledgedClaims.selectAllRadios();
        });

        $('#unacknowledgedClaimsClear').button();
        $('#unacknowledgedClaimsClear').click(function () {
            $('#unacknowledgedClaimsForm').clearForm();
        });

        $('#unacknowledgedClaimsDialog').dialog({
            autoOpen: false,
            closeOnEscape: true,
            modal: true,
            height: $(window).height() - 200,
            width: '80%',
            preOpen: function() {
                $('#unacknowledged_claims__start_date').datepick('disable');
            },
            postOpen: function() {
                $('#unacknowledged_claims__start_date').datepick('enable');
                $('#unacknowledgedClaimsTable').focus();
            }
        });
    },
    buildResultsNextStatusCell: function(data) {
        var id = data.appointment_id;
        var radios = '';

        for (var a in data.statuses) {
            var field_id = 'update_' + id + '_' + a;
            var selected = (a == data.appt_status) ? 'checked="checked"' : '';
            radios += '<input type="radio" value="' + a + '" id="' + field_id + '" name="update[' + id + ']" ' + selected + ' />';
            radios += ' <label for="' + field_id + '">' + data.statuses[a] + '</label> &nbsp;';
        }

        return radios;
    },
    selectAllRadios: function () {
        var radio_btns = $('#unacknowledgedClaimsTable input:radio:not(:checked)');
        $.each(radio_btns, function(key, val) {
            $(val).prop('checked', true);
        });
    },   
    
    initDataTable: function (rs) {
        var header = [];
        for (var i in rs.headers) {
            if(i == 'date_created'){
                header.push({title: rs.headers[i], sType : 'rank'});
            } else if (i == 'service_date') {
                header.push({title: rs.headers[i], sType : 'dateField'});
            } else {
                header.push({title: rs.headers[i]});
            }            
        }

        var tableId = unacknowledgedClaims.tblId;
        var table = $("#" + tableId).DataTable({
            "paging": true,
            "lengthMenu": [10, 25, 50, 100, 200, 250, 500],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": false,
            "select": false,
            "keys": true,
            "dom": 'Bfrtip',
            "aoColumns": header,
            "oLanguage": {
                "sSearch": "",
                "sLengthMenu": "Per page _MENU_",
                "oPaginate": {
                    "sNext": "<i class='clip-chevron-right'></i>",
                    "sPrevious": "<i class='clip-chevron-left'></i>"
                },
                "sInfo": "_START_ to _END_ rows out of _TOTAL_",
            },
            "sDom": '<"wrapper"<"row"<"col-sm-12" <"col-sm-8 actionButtonsDiv' + tableId + '"> <"col-sm-4 printBtnDiv" l > <"col-sm-12" <"sk-progress" <"sk-blue indeterminate skl' + tableId + '">><"row well customWell" <"col-sm-4 customSearchInput' + tableId + '" f> <"col-sm-1 customSearchField' + tableId + '">  <"col-sm-7 CustomPagination' + tableId + '"ip> > > >  >rt<"clear">>',
            initComplete: function () {
                $(".CustomPagination" + tableId).prepend('<div style="float:left"><button data-toggle="tooltip" title="Filters" type="button" class="btn btn-default filterToggle' + tableId + '"><i class="fa fa-filter"></i></button></div>');

                $("#" + tableId).find(".entireClickable").click(function () {
                    if ($(this).find('input').prop('checked')) {
                        $(this).find('input').prop('checked', false);
                        $(this).removeClass('highlight');
                    } else {
                        $(this).find('input').prop('checked', true);
                        $(this).addClass('highlight');
                    }
                })

                var inputHtml = '<div class="input-group">' +
                        '<input type="text" placeholder="Contains..." data-focus="true" class="form-control DatatableAllRounderSearch' + tableId + '" />' +
                        '<span class="input-group-addon cursorPointer btnClearSearchTxt' + tableId + '"> ' +
                        '<i class="clip-cancel-circle-2 "></i>' +
                        '</span>' +
                        '<span class="input-group-addon cursorPointer"> ' +
                        '<i class="clip-search-2"></i>' +
                        '</span>' +
                        '</div>';

                $(".customSearchInput" + tableId).html(inputHtml);

                global_js.triggerFocusField();

                var searchoptions = $("#" + tableId + " thead tr:eq(0) th");
                var customfilterinputs = '<tr>';
                for (var j = 0; j < searchoptions.length; j++) {
                    customfilterinputs += '<th></th>';
                }
                customfilterinputs += '</tr>';
                $("#" + tableId + " thead").append(customfilterinputs);
                var aa = 0;
                this.api().columns().every(function () {
                    var column = this;
                    var columnText = $.trim($(column.header())[0].innerText);

                    if ($(column.header())[0].cellIndex != 0 || 1 == 1) {

                        if (($.trim($(column.header())[0].innerText) == 'Description' && tableId == 'explCodeTable')) {
                            $('<input type="text" placeholder="Search" class="form-control btn-squared" />')
                                    .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                    .on('keyup change', function () {
                                        if (column.search() !== this.value) {
                                            column
                                                    .search(this.value)
                                                    .draw();
                                        }
                                    });
                        } else {
                            var select = $('<select class="selectRsltTbl' + tableId + '"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                    .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                    .on('change', function () {
                                        var val = $(this).val().trim();
                                        column.search(val, true, false).draw();
                                    });

                            column.data().unique().sort().each(function (d, j) {
                                if (columnText == 'Permissions' && tableId == 'user_type_table') {
                                    var arr = d.trim();
                                    arr = arr.split(',');
                                    for (var i in arr) {
                                        select.append('<option value="' + arr[i] + '">' + arr[i] + '</option>');
                                    }
                                } else {
                                    if(d != '') {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                    }
                                }

                            });
                        }

                    }
                    aa++;
                });


                $(".filterToggle" + tableId).click(function () {
                    $("#" + tableId + " thead tr:eq(1)").toggle();
                });
                $(".selectRsltTbl" + tableId).select2({
                    placeholder: "Search",
                    allowClear: true,
                    dropdownAutoWidth: true,
//                    maximumInputLength: 20,
                    width: '98%'
                });
                $(".custActionBtns" + tableId).detach().appendTo(".actionButtonsDiv" + tableId);
                $(".actionButtonsDiv" + tableId).attr('style', 'padding-left:0px');
            }
        });

        $(".DatatableAllRounderSearch" + tableId).keyup(function () {
            table.search($(this).val(), true).draw();
        });

        $(".btnClearSearchTxt" + tableId).click(function () {
            $(".DatatableAllRounderSearch" + tableId).val('');
            table.search('', true).draw();
        });
        setTimeout(function () {
            $(".dataTables_length select").select2('destroy');
            $("#" + tableId).find('.select2-arrow').hide();
            $("#" + tableId + " thead tr:eq(1)").toggle();
        }, 200);


        $('div.dataTables_filter input').addClass('form-control');
        $('div.dataTables_filter input').attr('placeholder', 'Search');
        $(".DTsearchlabel").html('<i class="clip-search"></i>');
        $('.dataTables_filter').attr('style', 'width:100%');
        $('.dataTables_filter label').attr('style', 'width:100%');
        $('.dataTables_length select').attr('style', 'font-size: 12px;padding: 5px 3px;background: #fff;');
        $('div.dataTables_filter input').attr('style', 'float:right;width:100%');
        $(".dataTables_wrapper .dataTables_paginate .paginate_button").attr('style', 'background:#fff !important;color:#444444 !important; border-radius: 0;');


        unacknowledgedClaims.fillDataTable(table, rs);


    },
    fillDataTable: function (table, data) {
        var tableData = [];
        for (var i = 0; i < data.data.length; i++) {
            var rowData = [];
            for (var j in data.headers) {
                if (data.data[i]) {
                    if (j == 'actions') {
                        rowData.push(unacknowledgedClaims.buildResultsNextStatusCell(data.data[i]));
                    } else if (j == 'date_created') {
                        rowData.push(moment(data.data[i][j]).format(global_js.getNewMomentFormatDateTime()));
                    } else {
                        rowData.push(data.data[i][j]);
                    }
                }
            }

            tableData.push(rowData);
        }
        console.log('tableData',tableData);
        table.clear().draw();
        table.rows.add(tableData);
        table.draw();

        global_js.datatableFilterInit(unacknowledgedClaims.tblId);
    },
    handleRequest: function(update_data) {
       
        if (quickClaim.ajaxLocked) { return; }

        var location_id = ($('#location_id').val() != undefined || $('#location_id').val() != null) ? $('#location_id').val() : '';
        var data = $('#unacknowledgedClaimsForm').serialize() + '&location_id=' + location_id;
        global_js.dtLoader(unacknowledgedClaims.tblId, 'start')
        $.ajax({
            type: 'post',
            url: unacknowledgedClaims.actions.search,
            dataType: 'json',
            data: data,
            success: function(rs) {

                if (rs.hasOwnProperty('data')) {


                    if (!$.fn.DataTable.isDataTable('#'+unacknowledgedClaims.tblId)) {
                        unacknowledgedClaims.initDataTable(rs);
                    } else {
                        var table = $("#"+unacknowledgedClaims.tblId).DataTable();
                        unacknowledgedClaims.fillDataTable(table, rs);
                    }
                    $('#unacknowledgedClaimsDialog').dialog('open');

                    if (rs.data.lenth > 0) {
                        $('#unacknowledgedClaimsSubmit').show();
                    }

                    $('#unacknowledged_claims_total_dialog, #unacknowledgedClaimsCount .bignum').text(rs.total_count);
                    $('#unacknowledged_claims_result_count').text(rs.data.length);

                    if (rs.updated_count) {
                        $('#quick_reports_messages_text, #unacknowledged_claims_messages_text').text('Updated ' + rs.updated_count + ' appointments. Received ' + row_count + ' results needing follow-up.');
                    }
                    else {
                        $('#quick_reports_messages_text, #unacknowledged_claims_messages_text').text('Received ' + rs.data.length + ' results.');
                    }
                    $('#quick_reports_checkmark, #follow_up_appts_checkmark').show();
                }
                
            },
            beforeSend: function() {
                quickClaim.ajaxLocked = true;
                $('#quick_reports_messages .icons div, #follow_up_appts_messages .icons div').hide();
                $('#quick_reports_spinner, #follow_up_appts_spinner').show();

                $('#quick_reports_messages_text, #follow_up_appts_messages_text').text('Requesting unacknowledged Claims Report');
            },
            complete: function() {
                quickClaim.ajaxLocked = false;
                $('#quick_reports_spinner, #follow_up_appts_spinner').hide();
            },
            error: function() {
                $('#quick_reports_error_icon, #follow_up_appts_error_icon').show();
                $('#quick_reports_messages_text, #follow_up_appts_messages_text').text('An error occurred while contacting the server.  Please refresh the page and try again.');
            }
        });
    }
};
