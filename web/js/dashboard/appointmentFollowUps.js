var appointmentFollowUp = {
    actions: {
        search: null,
        attended_search: null
    },
    scope: {
        series: []
    },
    initialize: function () {
        $('#appointmentAttendedDialog').dialog({
            autoOpen: false,
            closeOnEscape: true,
            modal: true,
            height: $(window).height() - 200,
            width: '80%',
            postOpen: function () {
                $('#appointmentAttendedTable').focus();
            }
        });

        sk.datepicker('#attended_appointment_start_date');


        $('#followUpAppointmentDialog').dialog({
            autoOpen: false,
            closeOnEscape: true,
            modal: true,
            height: $(window).height() - 200,
            width: '80%',
            postOpen: function () {
                $('#appointmentFollowUpsTable').focus();
            }
        });

        $('#appointmentFollowUpCount').click(function () {
            appointmentFollowUp.handleRequest(false);
        });

        $('#appointmentAttendedCount').click(function () {
            appointmentFollowUp.handleAttendedRequest(false);
        });

        $('#appointmentSelectAllRadio').button();
        $('#appointmentSelectAllRadio').click(function () {
            appointmentFollowUp.selectAllRadios();
        });

        $('#appointmentAttendedSelectAllRadio').button();
        $('#appointmentAttendedSelectAllRadio').click(function () {
            appointmentFollowUp.selectAllAttendedRadios();
        });

        $('#appointmentAttendedSubmit').button();
        $('#appointmentAttendedSubmit').click(function () {
            appointmentFollowUp.handleAttendedRequest(true);
        });

        $('#appointmentFollowUpSubmit').button();
        $('#appointmentFollowUpSubmit').click(function () {
            appointmentFollowUp.handleRequest(true);
        });

        $('#appointmentFollowUpRefineSubmit').button();
        $('#appointmentFollowUpRefineSubmit').click(function () {
            appointmentFollowUp.handleRequest(false);
        });


        $('#appointmentFollowUpRefineClear').click(function () {
            $('#followUpAppointmentsForm').clearForm();
            $("#follow_up_refine select").each(function () {
                $(this).trigger('change');
            })
            appointmentFollowUp.handleRequest(false);
        });

        $('#appointmentAttendedRefineSubmit').button();
        $('#appointmentAttendedRefineSubmit').click(function () {
            appointmentFollowUp.handleAttendedRequest(false);
        });

        $('#appointmentAttendedRefineClear').button();
        $('#appointmentAttendedRefineClear').click(function () {
            $('#attendedAppointmentsForm').clearForm();
            $("#attendedAppointmentsForm select").each(function () {
                $(this).trigger('change');
            })
            appointmentFollowUp.handleAttendedRequest(false);
        });

        $('#location_id').on('change',function(){
            appointmentFollowUp.buildLocationWiseCount(this.value);
        })


        sk.datepicker('#follow_up_appointment_start_date');

        sk.datepicker('#attended_appointment_start_date');

        sk.datepicker('#unresolved_claims__start_date');

    },
    buildLocationWiseCount: function(val) {
        $('#follow_up_appointment_location_id option[value='+val+']').attr("selected","selected");
        $('#attended_appointment_location_id option[value='+val+']').attr("selected","selected");
        $('#todays_appointments_location_id option[value='+val+']').attr("selected","selected");
        $.ajax({
            type: 'post',
            url: appointmentFollowUp.actions.dashboardlocationwisecount,
            dataType: 'json',
            data: {location_id:val},
            success: function (rs) {
                $('#dashboard_follow_up_count').text(rs.follow_up_count);
                $('#dashboard_orphan_count').text(rs.orphan_count);
                $('#dashboard_follow_up_appointment_count').text(rs.follow_up_appointment_count);
                $('#dashboard_attended_appointment_count').text(rs.attended_appointment_count);
                $('#dashboard_todays_appointment_count').text(rs.todays_appointment_count);
                $('#dashboard_unresolved_claims_count').text(rs.unresolved_claims_count );
                $('#dashboard_unacknowledged_claims_count').text(rs.unacknowledged_claims_count);
                $('#dashboard_adjudication_claims_count').text(rs.adjudication_claims_count);
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
            },
            error: function () {
            }
        });


    },
    buildResultsDataRow: function (data, columns) {
        var id = data.appointment_id;
        var tr = '<tr id="' + id + '">';
        for (var class_name in columns) {
            var text = class_name == 'actions' ? '' : data[class_name];
            tr += '<td class="' + class_name + '">' + text + '</td>';
        }
        tr += '</tr>';
        $(tr).appendTo($('#appointmentFollowUpsTable tbody'));
    },
    buildAttendedResultsDataRow: function (data, columns) {
        var id = data.appointment_id;
        var tr = '<tr id="' + id + '">';
        for (var class_name in columns) {
            var text = class_name == 'actions' ? '' : data[class_name];
            tr += '<td class="' + class_name + '">' + text + '</td>';
        }
        tr += '</tr>';
        $(tr).appendTo($('#appointmentAttendedTable tbody'));
    },
    buildResultsHeaderRow: function (headers) {
        var tr = '<tr>';
        for (var class_name in headers) {
            tr += '<th class="' + class_name + '">' + headers[class_name] + '</th>';
        }
        tr += '</tr>';
        return tr;
    },
    buildResultsNextStatusCell: function (data) {
        var id = data.appointment_id;
        var radios = '';

        for (var a in data.statuses) {
            var field_id = 'update_' + id + '_' + a;
            var selected = (a == data.appt_status) ? 'checked="checked"' : '';
            radios += '<input type="radio" value="' + a + '" id="' + field_id + '" name="update[' + id + ']" ' + selected + ' />';
            radios += ' <label for="' + field_id + '">' + data.statuses[a] + '</label> &nbsp;';
        }

        return radios;
    },
    buildAttendedResultsNextStatusCell: function (data) {
        var id = data.appointment_id;
        var radios = '';

        for (var a in data.statuses) {
            var field_id = 'update_' + id + '_' + a;
            var selected = (a == data.appt_status) ? 'checked="checked"' : '';
            radios += '<input type="radio" value="' + a + '" id="' + field_id + '" name="update[' + id + ']" ' + selected + ' />';
            radios += ' <label for="' + field_id + '">' + data.statuses[a] + '</label> &nbsp;';
        }

        return radios;
    },
    selectAllRadios: function () {
        var radio_btns = $('#appointmentFollowUpsTable input:radio:not(:checked)');
        $.each(radio_btns, function (key, val) {
            $(val).prop('checked', true);
        });
    },
    selectAllAttendedRadios: function () {
        var radio_btns = $('#appointmentAttendedTable input:radio:not(:checked)');
        $.each(radio_btns, function (key, val) {
            $(val).prop('checked', true);
        });
    },
    buildChart: function () {
        new Chartist.Bar('.ct-chart', {
            labels: ['Patient reminders', 'Appt rescheduling', 'Appt reminders', 'Appt not billed', 'Scheduled today', 'Unresolved claims'],
            series: appointmentFollowUp.scope.series
        }, {
            distributeSeries: true
        });
    },
    initDataTable: function (rs) {
        var header = [];
        for (var i in rs.headers) {       
            console.log(i); 
            if(i == 'start_date'){
                header.push({title: rs.headers[i], sType: "dateTimeField"});
            } else {
                header.push({title: rs.headers[i], bSortable: true});
            }
            
        }

        var tableId = 'appointmentFollowUpsTable';
        var table = $("#" + tableId).DataTable({
            "paging": true,
            "lengthMenu": [10, 25, 50, 100, 200, 250, 500],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": false,
            "select": false,
            "keys": true,
            "dom": 'Bfrtip',
//            "columns": header,
            "aoColumns": header,
            "oLanguage": {
                "sSearch": "",
                "sLengthMenu": "Per page _MENU_",
                "oPaginate": {
                    "sNext": "<i class='clip-chevron-right'></i>",
                    "sPrevious": "<i class='clip-chevron-left'></i>"
                },
                "sInfo": "_START_ to _END_ rows out of _TOTAL_",
            },
            "sDom": '<"wrapper"<"row"<"col-sm-12" <"col-sm-8 actionButtonsDiv' + tableId + '"> <"col-sm-4 printBtnDiv" l > <"col-sm-12" <"sk-progress" <"sk-blue indeterminate skl' + tableId + '">><"row well customWell" <"col-sm-4 customSearchInput' + tableId + '" f> <"col-sm-1 customSearchField' + tableId + '">  <"col-sm-7 CustomPagination' + tableId + '"ip> > > >  >rt<"clear">>',
            initComplete: function () {
                $(".CustomPagination" + tableId).prepend('<div style="float:left"><button data-toggle="tooltip" title="Filters" type="button" class="btn btn-default filterToggle' + tableId + '"><i class="fa fa-filter"></i></button></div>');

                $("#" + tableId).find(".entireClickable").click(function () {
                    if ($(this).find('input').prop('checked')) {
                        $(this).find('input').prop('checked', false);
                        $(this).removeClass('highlight');
                    } else {
                        $(this).find('input').prop('checked', true);
                        $(this).addClass('highlight');
                    }
                })

                var inputHtml = '<div class="input-group">' +
                        '<input type="text" placeholder="Contains..." data-focus="true" class="form-control DatatableAllRounderSearch' + tableId + '" />' +
                        '<span class="input-group-addon cursorPointer btnClearSearchTxt' + tableId + '"> ' +
                        '<i class="clip-cancel-circle-2 "></i>' +
                        '</span>' +
                        '<span class="input-group-addon cursorPointer"> ' +
                        '<i class="clip-search-2"></i>' +
                        '</span>' +
                        '</div>';

                $(".customSearchInput" + tableId).html(inputHtml);

                global_js.triggerFocusField();

                var searchoptions = $("#" + tableId + " thead tr:eq(0) th");
                var customfilterinputs = '<tr>';
                for (var j = 0; j < searchoptions.length; j++) {
                    customfilterinputs += '<th></th>';
                }
                customfilterinputs += '</tr>';
                $("#" + tableId + " thead").append(customfilterinputs);
                var aa = 0;
                this.api().columns().every(function () {
                    var column = this;
                    var columnText = $.trim($(column.header())[0].innerText);

                    if ($(column.header())[0].cellIndex != 0 || 1 == 1) {

                        if (($.trim($(column.header())[0].innerText) == 'Description' && tableId == 'explCodeTable')) {
                            $('<input type="text" placeholder="Search" class="form-control btn-squared" />')
                                    .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                    .on('keyup change', function () {
                                        if (column.search() !== this.value) {
                                            column
                                                    .search(this.value)
                                                    .draw();
                                        }
                                    });
                        } else {
                            var select = $('<select class="selectRsltTbl' + tableId + '"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                    .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                    .on('change', function () {
                                        var val = $(this).val().trim();
                                        column.search(val, true, false).draw();
                                    });

                            column.data().unique().sort().each(function (d, j) {
                                if (columnText == 'Permissions' && tableId == 'user_type_table') {
                                    var arr = d.trim();
                                    arr = arr.split(',');
                                    for (var i in arr) {
                                        select.append('<option value="' + arr[i] + '">' + arr[i] + '</option>');
                                    }
                                } else {
                                    if(d != '') {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                    }
                                }

                            });
                        }

                    }
                    aa++;
                });


                $(".filterToggle" + tableId).click(function () {
                    $("#" + tableId + " thead tr:eq(1)").toggle();
                });
                $("#" + tableId + ' select').select2({
                    placeholder: "Search",
                    allowClear: true,
                    dropdownAutoWidth: true,
//                    maximumInputLength: 20,
                    width: '98%'
                });
                $(".custActionBtns" + tableId).detach().appendTo(".actionButtonsDiv" + tableId);
                $(".actionButtonsDiv" + tableId).attr('style', 'padding-left:0px');
            }
        });

        $(".DatatableAllRounderSearch" + tableId).keyup(function () {
            table.search($(this).val(), true).draw();
        });

        $(".btnClearSearchTxt" + tableId).click(function () {
            $(".DatatableAllRounderSearch" + tableId).val('');
            table.search('', true).draw();
        });
        setTimeout(function () {
            $(".dataTables_length select").select2('destroy');
            $("#" + tableId).find('.select2-arrow').hide();
            $("#" + tableId + " thead tr:eq(1)").toggle();
        }, 200);


        $('div.dataTables_filter input').addClass('form-control');
        $('div.dataTables_filter input').attr('placeholder', 'Search');
        $(".DTsearchlabel").html('<i class="clip-search"></i>');
        $('.dataTables_filter').attr('style', 'width:100%');
        $('.dataTables_filter label').attr('style', 'width:100%');
        $('.dataTables_length select').attr('style', 'font-size: 12px;padding: 5px 3px;background: #fff;');
        $('div.dataTables_filter input').attr('style', 'float:right;width:100%');
        $(".dataTables_wrapper .dataTables_paginate .paginate_button").attr('style', 'background:#fff !important;color:#444444 !important; border-radius: 0;');


        appointmentFollowUp.fillDataTable(table, rs);

    },
    fillDataTable: function (table, data) {
        var tableData = [];
        for (var i = 0; i < data.data.length; i++) {
            var rowData = [];
            for (var j in data.headers) {
                if (data.data[i]) {
                    if (j == 'actions') {
                        rowData.push(appointmentFollowUp.buildResultsNextStatusCell(data.data[i]));
                    } else {
                        rowData.push(data.data[i][j]);
                    }
                }
            }
            tableData.push(rowData);
        }
        table.clear().draw();
        table.rows.add(tableData);
        table.draw();
        
        global_js.datatableFilterInit('appointmentFollowUpsTable');
    },
    handleRequest: function (update_data) {
        if (quickClaim.ajaxLocked) {
            return;
        }
        global_js.dtLoader('appointmentFollowUpsTable', 'start');

        var data = $('#followUpAppointmentsForm').serialize();
        if (update_data) {
            data += '&' + $('#follow_up_update_form').serialize();
        }

        $.ajax({
            type: 'post',
            url: appointmentFollowUp.actions.search,
            dataType: 'json',
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('data')) {

                    if (!$.fn.DataTable.isDataTable('#appointmentFollowUpsTable')) {
                        appointmentFollowUp.initDataTable(rs);
                    } else {
                        var table = $("#appointmentFollowUpsTable").DataTable();
                        console.log(rs);
                      
                        appointmentFollowUp.fillDataTable(table, rs);
                    }

                    $('#followUpAppointmentDialog').dialog('open');


                    $('#follow_up_appt_total_dialog, #appointmentFollowUpCount .bignum').text(rs.total_count);

                    if (rs.updated_count) {
                        $('#quick_reports_messages_text, #follow_up_appts_messages_text').text('Updated ' + rs.updated_count + ' appointments. Received ' + rs.data.length + ' results needing follow-up.');
                    } else {
                        $('#quick_reports_messages_text, #follow_up_appts_messages_text').text('Received ' + rs.data.length + ' results.');
                    }
                    $('#quick_reports_checkmark, #follow_up_appts_checkmark').show();

                }
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                $('#quick_reports_messages .icons div, #follow_up_appts_messages .icons div').hide();
                $('#quick_reports_spinner, #follow_up_appts_spinner').show();

                $('#quick_reports_messages_text, #follow_up_appts_messages_text').text('Requesting Appointment Follow Up Report');
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                $('#quick_reports_spinner, #follow_up_appts_spinner').hide();
            },
            error: function () {
                $('#quick_reports_error_icon, #follow_up_appts_error_icon').show();
                $('#quick_reports_messages_text, #follow_up_appts_messages_text').text('An error occurred while contacting the server.  Please refresh the page and try again.');
            }
        });
    },

    initAttendedRequestDataTable: function (rs) {
        var header = [];
        for (var i in rs.headers) {
            if(i == 'start_date') {
                header.push({title: rs.headers[i], sType: "dateTimeField"});
            } else {
                header.push({title: rs.headers[i]});
            }
            
        }

        var tableId = 'appointmentAttendedTable';
        var table = $("#" + tableId).DataTable({
            "paging": true,
            "lengthMenu": [10, 25, 50, 100, 200, 250, 500],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": false,
            "select": false,
            "keys": true,
            "dom": 'Bfrtip',
            "columns": header,
            "oLanguage": {
                "sSearch": "",
                "sLengthMenu": "Per page _MENU_",
                "oPaginate": {
                    "sNext": "<i class='clip-chevron-right'></i>",
                    "sPrevious": "<i class='clip-chevron-left'></i>"
                },
                "sInfo": "_START_ to _END_ rows out of _TOTAL_",
            },
            "sDom": '<"wrapper"<"row"<"col-sm-12" <"col-sm-8 actionButtonsDiv' + tableId + '"> <"col-sm-4 printBtnDiv" l > <"col-sm-12" <"sk-progress" <"sk-blue indeterminate skl' + tableId + '">><"row well customWell" <"col-sm-4 customSearchInput' + tableId + '" f> <"col-sm-1 customSearchField' + tableId + '">  <"col-sm-7 CustomPagination' + tableId + '"ip> > > >  >rt<"clear">>',
            initComplete: function () {
                $(".CustomPagination" + tableId).prepend('<div style="float:left"><button data-toggle="tooltip" title="Filters" type="button" class="btn btn-default filterToggle' + tableId + '"><i class="fa fa-filter"></i></button></div>');

                $("#" + tableId).find(".entireClickable").click(function () {
                    if ($(this).find('input').prop('checked')) {
                        $(this).find('input').prop('checked', false);
                        $(this).removeClass('highlight');
                    } else {
                        $(this).find('input').prop('checked', true);
                        $(this).addClass('highlight');
                    }
                })

                var inputHtml = '<div class="input-group">' +
                        '<input type="text" placeholder="Contains..." data-focus="true" class="form-control DatatableAllRounderSearch' + tableId + '" />' +
                        '<span class="input-group-addon cursorPointer btnClearSearchTxt' + tableId + '"> ' +
                        '<i class="clip-cancel-circle-2 "></i>' +
                        '</span>' +
                        '<span class="input-group-addon cursorPointer"> ' +
                        '<i class="clip-search-2"></i>' +
                        '</span>' +
                        '</div>';

                $(".customSearchInput" + tableId).html(inputHtml);

                global_js.triggerFocusField();

                var searchoptions = $("#" + tableId + " thead tr:eq(0) th");
                var customfilterinputs = '<tr>';
                for (var j = 0; j < searchoptions.length; j++) {
                    customfilterinputs += '<th></th>';
                }
                customfilterinputs += '</tr>';
                $("#" + tableId + " thead").append(customfilterinputs);
                var aa = 0;
                this.api().columns().every(function () {
                    var column = this;
                    var columnText = $.trim($(column.header())[0].innerText);

                    if ($(column.header())[0].cellIndex != 0 || 1 == 1) {

                        if (($.trim($(column.header())[0].innerText) == 'Description' && tableId == 'explCodeTable')) {
                            $('<input type="text" placeholder="Search" class="form-control btn-squared" />')
                                    .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                    .on('keyup change', function () {
                                        if (column.search() !== this.value) {
                                            column
                                                    .search(this.value)
                                                    .draw();
                                        }
                                    });
                        } else {
                            var select = $('<select class="selectRsltTbl' + tableId + '"><option value=""></option><option value="">All</option><option value="" disabled>No '+columnText+'</option></select>')
                                    .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                    .on('change', function () {
                                        var val = $(this).val().trim();
                                        column
                                                .search(val)
                                                .draw();
                                    });

                            column.data().unique().sort().each(function (d, j) {
                                if (columnText == 'Permissions' && tableId == 'user_type_table') {
                                    var arr = d.trim();
                                    arr = arr.split(',');
                                    for (var i in arr) {
                                        select.append('<option value="' + arr[i] + '">' + arr[i] + '</option>');
                                    }
                                } else {
                                    if(d != '') {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                    }
                                }

                            });
                        }

                    }
                    aa++;
                });


                $(".filterToggle" + tableId).click(function () {
                    $("#" + tableId + " thead tr:eq(1)").toggle();
                });
                $("#" + tableId + ' select').select2({
                    placeholder: "Search",
                    allowClear: true,
                    dropdownAutoWidth: true,
//                    maximumInputLength: 20,
                    width: '98%'
                });
                $(".custActionBtns" + tableId).detach().appendTo(".actionButtonsDiv" + tableId);
                $(".actionButtonsDiv" + tableId).attr('style', 'padding-left:0px');
            }
        });

        $(".DatatableAllRounderSearch" + tableId).keyup(function () {
            table.search($(this).val(), true).draw();
        });

        $(".btnClearSearchTxt" + tableId).click(function () {
            $(".DatatableAllRounderSearch" + tableId).val('');
            table.search('', true).draw();
        });
        setTimeout(function () {
            $(".dataTables_length select").select2('destroy');
            $("#" + tableId).find('.select2-arrow').hide();
            $("#" + tableId + " thead tr:eq(1)").toggle();
        }, 200);


        $('div.dataTables_filter input').addClass('form-control');
        $('div.dataTables_filter input').attr('placeholder', 'Search');
        $(".DTsearchlabel").html('<i class="clip-search"></i>');
        $('.dataTables_filter').attr('style', 'width:100%');
        $('.dataTables_filter label').attr('style', 'width:100%');
        $('.dataTables_length select').attr('style', 'font-size: 12px;padding: 5px 3px;background: #fff;');
        $('div.dataTables_filter input').attr('style', 'float:right;width:100%');
        $(".dataTables_wrapper .dataTables_paginate .paginate_button").attr('style', 'background:#fff !important;color:#444444 !important; border-radius: 0;');


        appointmentFollowUp.fillAttendedRequestDataTable(table, rs);


    },
    fillAttendedRequestDataTable: function (table, data) {
        var tableId = 'appointmentAttendedTable';
        var tableData = [];
        global_js.dtLoader(tableId, 'stop');
        for (var i = 0; i < data.data.length; i++) {
            var rowData = [];
            for (var j in data.headers) {
                if (data.data[i]) {
                    if (j == 'actions') {
                        rowData.push(appointmentFollowUp.buildAttendedResultsNextStatusCell(data.data[i]));
                    } else {
                        rowData.push(data.data[i][j]);
                    }
                }
            }
            tableData.push(rowData);
        }
        table.clear().draw();
        table.rows.add(tableData);
        table.draw();
        var aa = 0;
        
        var apiDt = $("#" + tableId).dataTable().api();
        apiDt.columns().every(function () {
            var column = this;

//            if (aa != 0) {
            $("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")").html('');
            if (($.trim($(column.header())[0].innerText) == 'Date & Time')) {
                var date = $('<select data-width="60" class="selectRsltTbl' + tableId + '"><option value=""></option></select>')
                        .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                        .on('change', function () {
                            var val = $(this).val().trim();
                            column
                                    .search(val)
                                    .draw();
                        });

                var time = $('<select data-width="40" class="selectRsltTbl' + tableId + '"><option value=""></option></select>')
                        .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                        .on('change', function () {
                            var val = $(this).val().trim();
                            column
                                    .search(val)
                                    .draw();
                        });

                if (column.search() != ''){
                    var search = column.search();
                    var srch = search.trim();
                    srch = srch.split(' ');
                }
                
                column.data().unique().sort().each(function (d, j) {
                    var arr = d.trim();
                    arr = arr.split(' ');
                    if(typeof srch != 'undefined' && (srch[0] == arr[0] || srch[1] == arr[1])){
                        date.append('<option selected value="' + arr[0] + '">' + arr[0] + '</option>');
                        time.append('<option selected value="' + arr[1] + '">' + arr[1] + '</option>');
                    } else {
                        date.append('<option value="' + arr[0] + '">' + arr[0] + '</option>');
                        time.append('<option value="' + arr[1] + '">' + arr[1] + '</option>');
                    }
                    

                });

            } else if (($.trim($(column.header())[0].innerText) != 'Next Status')) {
                var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="" disabled>No '+columnText+'</option></select>')
                        .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")")/*$("#" + pastClaims.resultsTable + " thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                    );

                            column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                        });

               column.data().unique().sort().each(function (d, j) {
                    if (column.search() != ''){
                        var search = column.search().slice(1,-1).replace(/\\/g, "");
                        console.log(search);
                        if(search == d){
                            select.append('<option selected value="' + d + '">' + d + '</option>');
                        } else {    
                            select.append('<option value="' + d + '">' + d + '</option>');
                        }
                    } else {
                        if(d != '') {
                        select.append('<option value="' + d + '">' + d + '</option>');
                        }
                    }
                    
                });
            }
            aa++;
        });

        $("#" + tableId + " select").each(function () {

            var width = $(this).attr('data-width') ? $(this).attr('data-width') : '98';
            $(this).select2({
                placeholder: "Search",
                allowClear: true,
                dropdownAutoWidth: true,
                width: width + '%'
            });
        })
        $('#' + tableId + ' .select2-arrow').hide();
        global_js.dtLoader(tableId, 'stop');
    },
    handleAttendedRequest: function (update_data) {
        if (quickClaim.ajaxLocked) {
            return;
        }

        var s_date = $('#attended_appointment_start_date').val();
//        if (s_date != undefined || s_date != null || s_date != "") {
//            var from = s_date.split("/");
//            if (from != "") {
//                var f = new Date(from[2], from[1], from[0]);
//                var date_string = f.getFullYear() + "-" + f.getMonth() + "-" + f.getDate();
//                $('#attended_appointment_start_date').val(date_string);
//            }
//        }

        var data = $('#attendedAppointmentsForm').serialize();
        if (update_data) {
            data += '&' + $('#attended_update_form').serialize();
        }
        console.log(data);

        $.ajax({
            type: 'post',
            url: appointmentFollowUp.actions.attended_search,
            dataType: 'json',
            data: data,
            success: function (rs) {
                
                if (rs.hasOwnProperty('data')) {
console.log(rs);

                    if (!$.fn.DataTable.isDataTable('#appointmentAttendedTable')) {
                        console.log('in init datatable');
                        appointmentFollowUp.initAttendedRequestDataTable(rs);
                    } else {
                        console.log('in update datatable')
                        var table = $("#appointmentAttendedTable").DataTable();
                        appointmentFollowUp.fillAttendedRequestDataTable(table, rs);
                    }


                    $('#appointmentAttendedSubmit').hide();

                    $('#attendedAppointmentDialog').dialog({
                        closeOnEscape: true,
                        modal: true,
                        height: $(window).height() - 200,
                        width: '80%',
                    });



                    if (rs.data.length > 1) {
                        $('#appointmentAttendedSubmit').show();
                    }


                    $('#attended_appt_total_dialog, #appointmentAttendedCount .bignum').text(rs.total_count);
                    $('#attended_appt_result_count').text(rs.data.length);

                    if (rs.updated_count) {
                        $('#quick_reports_messages_text, #attended_appts_messages_text').text('Updated ' + rs.updated_count + ' appointments. Received ' + row_count + ' results needing billing.');
                    } else {
                        $('#quick_reports_messages_text, #attended_appts_messages_text').text('Received ' + rs.data.length + ' results.');
                    }
                    $('#quick_reports_checkmark, #attended_appts_checkmark').show();
                }

            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                $('#quick_reports_messages .icons div, #follow_up_appts_messages .icons div').hide();
                $('#quick_reports_spinner, #follow_up_appts_spinner').show();

                $('#quick_reports_messages_text, #follow_up_appts_messages_text').text('Requesting Appointment Attended Report');
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                $('#quick_reports_spinner, #follow_up_appts_spinner').hide();
            },
            error: function () {
                $('#quick_reports_error_icon, #follow_up_appts_error_icon').show();
                $('#quick_reports_messages_text, #follow_up_appts_messages_text').text('An error occurred while contacting the server.  Please refresh the page and try again.');
            }
        });
    }
};
