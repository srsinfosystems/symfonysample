var todaysAppointments = {
    actions: {
        search: null,
        visit_pdf: null
    },
    tblId: 'todays_appts_report_results',

    initialize: function () {
        $('#appointment_count_report').click(function () {
            todaysAppointments.handleRequest(false);
        });

        $('#todays_appointments_dialog').dialog({
            autoOpen: false,
            closeOnEscape: true,
            modal: true,
            height: $(window).height() - 200,
            width: '80%'
        });

        $('#todays_appointment_refine_button').click(function () {
            todaysAppointments.handleRequest(true);
        });

        $('#todays_appointment_clear_button').click(function () {
            $('#todays_appointment_refine_search').clearForm();
            $("#todays_appointment_refine_search select").each(function () {
                $(this).trigger('change');
            })
            todaysAppointments.handleRequest(true);
        });
    },
    handleRequest: function (refine_search) {
        if (quickClaim.ajaxLocked) {
            return;
        }
        global_js.dtLoader(todaysAppointments.tblId, 'start');
        var data = $('#todays_appointment_refine_search').serialize();

        $.ajax({
            type: 'post',
            url: todaysAppointments.actions.search,
            dataType: 'json',
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('data')) {
                    if (!$.fn.DataTable.isDataTable('#' + todaysAppointments.tblId)) {
                        todaysAppointments.initDataTable(rs);
                    } else {
                        var table = $("#" + todaysAppointments.tblId).DataTable();
                        todaysAppointments.fillDataTable(table, rs);
                    }
                    $('#todays_appointments_dialog').dialog('open');
                    $('#todays_appts_result_count').text($(rs.data).size());

                    $('#quick_reports_messages_text, #todays_appts_messages_text').text('Received ' + $(rs.data).size() + ' results.');
                    $('#quick_reports_checkmark, #todays_appts_checkmark').show();

                }
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                $('#quick_reports_messages .icons div, #todays_appts_messages .icons div').hide();
                $('#quick_reports_spinner, #todays_appts_spinner').show();

                $('#quick_reports_messages_text, #todays_appts_messages_text').text("Requesting Today's Appointments Report");
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                $('#quick_reports_spinner, #todays_appts_spinner').hide();
            },
            error: function () {
                $('#quick_reports_error_icon, #todays_appts_error_icon').show();
                $('#quick_reports_messages_text, #todays_appts_messages_text').text('An error occurred while contacting the server.  Please refresh the page and try again.');
            }
        });
    },

    initDataTable: function (rs) {
        var header = [];
        for (var i in rs.headers) {
            if(i == 'start_time') {
                header.push({title: rs.headers[i], sType: "timeField"});
            } else {
                header.push({title: rs.headers[i]});
            }
            
        }

        var tableId = todaysAppointments.tblId;
        var table = $("#" + tableId).DataTable({
            "paging": true,
            "lengthMenu": [10, 25, 50, 100, 200, 250, 500],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": false,
            "select": false,
            "keys": true,
            "dom": 'Bfrtip',
            "columns": header,
            "oLanguage": {
                "sSearch": "",
                "sLengthMenu": "Per page _MENU_",
                "oPaginate": {
                    "sNext": "<i class='clip-chevron-right'></i>",
                    "sPrevious": "<i class='clip-chevron-left'></i>"
                },
                "sInfo": "_START_ to _END_ rows out of _TOTAL_",
            },
            "sDom": '<"wrapper"<"row"<"col-sm-12" <"col-sm-8 actionButtonsDiv' + tableId + '"> <"col-sm-4 printBtnDiv" l > <"col-sm-12" <"sk-progress" <"sk-blue indeterminate skl' + tableId + '">><"row well customWell" <"col-sm-4 customSearchInput' + tableId + '" f> <"col-sm-1 customSearchField' + tableId + '">  <"col-sm-7 CustomPagination' + tableId + '"ip> > > >  >rt<"clear">>',
            initComplete: function () {
                $(".CustomPagination" + tableId).prepend('<div style="float:left"><button data-toggle="tooltip" title="Filters" type="button" class="btn btn-default filterToggle' + tableId + '"><i class="fa fa-filter"></i></button></div>');

                $("#" + tableId).find(".entireClickable").click(function () {
                    if ($(this).find('input').prop('checked')) {
                        $(this).find('input').prop('checked', false);
                        $(this).removeClass('highlight');
                    } else {
                        $(this).find('input').prop('checked', true);
                        $(this).addClass('highlight');
                    }
                })

                var inputHtml = '<div class="input-group">' +
                        '<input type="text" placeholder="Contains..." data-focus="true" class="form-control DatatableAllRounderSearch' + tableId + '" />' +
                        '<span class="input-group-addon cursorPointer btnClearSearchTxt' + tableId + '"> ' +
                        '<i class="clip-cancel-circle-2 "></i>' +
                        '</span>' +
                        '<span class="input-group-addon cursorPointer"> ' +
                        '<i class="clip-search-2"></i>' +
                        '</span>' +
                        '</div>';

                $(".customSearchInput" + tableId).html(inputHtml);

                global_js.triggerFocusField();

                var searchoptions = $("#" + tableId + " thead tr:eq(0) th");
                var customfilterinputs = '<tr>';
                for (var j = 0; j < searchoptions.length; j++) {
                    customfilterinputs += '<th></th>';
                }
                customfilterinputs += '</tr>';
                $("#" + tableId + " thead").append(customfilterinputs);
                var aa = 0;
                this.api().columns().every(function () {
                    var column = this;
                    var columnText = $.trim($(column.header())[0].innerText);

                    if ($(column.header())[0].cellIndex != 0 || 1 == 1) {

                        if (($.trim($(column.header())[0].innerText) == 'Description' && tableId == 'explCodeTable')) {
                            $('<input type="text" placeholder="Search" class="form-control btn-squared" />')
                                    .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                    .on('keyup change', function () {
                                        if (column.search() !== this.value) {
                                            column
                                                    .search(this.value)
                                                    .draw();
                                        }
                                    });
                        } else {
                            var select = $('<select class="selectRsltTbl' + tableId + '"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                    .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                    .on('change', function () {
                                        var val = $(this).val().trim();
                                        column.search(val, true, false).draw();
                                    });

                            column.data().unique().sort().each(function (d, j) {
                                if (columnText == 'Permissions' && tableId == 'user_type_table') {
                                    var arr = d.trim();
                                    arr = arr.split(',');
                                    for (var i in arr) {
                                        select.append('<option value="' + arr[i] + '">' + arr[i] + '</option>');
                                    }
                                } else {
                                    if(d != '') {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                    }
                                }

                            });
                        }

                    }
                    aa++;
                });


                $(".filterToggle" + tableId).click(function () {
                    $("#" + tableId + " thead tr:eq(1)").toggle();
                });
                $(".selectRsltTbl" + tableId).select2({
                    placeholder: "Search",
                    allowClear: true,
                    dropdownAutoWidth: true,
//                    maximumInputLength: 20,
                    width: '98%'
                });
                $(".custActionBtns" + tableId).detach().appendTo(".actionButtonsDiv" + tableId);
                $(".actionButtonsDiv" + tableId).attr('style', 'padding-left:0px');
            }
        });

        $(".DatatableAllRounderSearch" + tableId).keyup(function () {
            table.search($(this).val(), true).draw();
        });

        $(".btnClearSearchTxt" + tableId).click(function () {
            $(".DatatableAllRounderSearch" + tableId).val('');
            table.search('', true).draw();
        });
        setTimeout(function () {
            $(".dataTables_length select").select2('destroy');
            $("#" + tableId).find('.select2-arrow').hide();
            $("#" + tableId + " thead tr:eq(1)").toggle();
        }, 200);


        $('div.dataTables_filter input').addClass('form-control');
        $('div.dataTables_filter input').attr('placeholder', 'Search');
        $(".DTsearchlabel").html('<i class="clip-search"></i>');
        $('.dataTables_filter').attr('style', 'width:100%');
        $('.dataTables_filter label').attr('style', 'width:100%');
        $('.dataTables_length select').attr('style', 'font-size: 12px;padding: 5px 3px;background: #fff;');
        $('div.dataTables_filter input').attr('style', 'float:right;width:100%');
        $(".dataTables_wrapper .dataTables_paginate .paginate_button").attr('style', 'background:#fff !important;color:#444444 !important; border-radius: 0;');


        todaysAppointments.fillDataTable(table, rs);


    },
    fillDataTable: function (table, data) {
        var tableData = [];
        for (var i = 0; i < data.data.length; i++) {
            var rowData = [];
            for (var j in data.headers) {
                if (data.data[i]) {
                    if (j == 'actions') {
                        rowData.push(todaysAppointments.buildResultsActionsCell(data.data[i]));
                    } else {
                        rowData.push(data.data[i][j]);
                    }
                }
            }
            tableData.push(rowData);
        }
        table.clear().draw();
        table.rows.add(tableData);
        table.draw();
        $('#' + todaysAppointments.tblId + '.actionsBtns').on('click', function () {
            var mode = $(this).attr('id').replace('_' + id, '');
            var appointment_id = $(this).attr('data-appointment_id');
            var patient_id = $(this).attr('data-patient_id');
            var id = $(this).attr('data-id');
            todaysAppointments.handleActionClick(mode, appointment_id, patient_id, id);
        });


        global_js.datatableFilterInit(todaysAppointments.tblId);
    },

    buildResultsActionsCell: function (data, actions) {
        var id = data.appointment_id;
        var actions = '';
        for (var a in actions) {
            var td = '<td class="' + actions[a] + '">';
            var button_id = actions[a] + '_' + id;
            var action = actions[a];

            if (data.actions.hasOwnProperty(actions[a])) {
                td += '<input class="btn btn-blue actionsBtns" data-id="' + data.id + '" data-patient_id="' + data.patient_id + '" data-appointment_id="' + data.appointment_id + '" type="button" id="' + button_id + '" value="' + actions[a].toUpperCase().replace('_', ' ') + '" />';
            } else {
                td += '&nbsp;';
            }
            td += '';
            actions += td;
        }
        return actions;
    },
    buildResultsHeaderRow: function (headers) {
        var tr = '<tr>';
        for (var class_name in headers) {
            tr += '<th class="' + class_name + '">' + headers[class_name] + '</th>';
        }
        tr += '</tr>';
        return tr;
    },
    handleActionClick: function (mode, appointment_id, patient_id, attendee_id) {
        if (mode == 'visit_pdf') {
            return todaysAppointments.getVisitSheetPDF(attendee_id);
        }

        $('#todays_appointment_book_patient_id').val(patient_id);
        $('#todays_appointment_book_appointment_id').val(appointment_id);
        $('#todays_appointment_book_mode').val(mode);

        $('#todays_appts_form').submit();
    },
    appointmentVisitDownloadComplete: function () {
        quickClaim.hideOverlayMessage();
        $('#quick_reports_spinner, #todays_appts_spinner').hide();
    },
    getVisitSheetPDF: function (attendeeID)
    {
        var url = todaysAppointments.actions.visit_sheet + '?appointmentAttendeeID=' + attendeeID + '&AppointmentVisitSheetToken=' + hypeFileDownload.blockResubmit();

        hypeFileDownload.tokenName = "AppointmentVisitSheetToken";
        hypeFileDownload.setReturnFunction(todaysAppointments.appointmentVisitDownloadComplete);

        $('#quick_reports_checkmark, #todays_appts_checkmark').hide();
        $('#quick_reports_error_icon, #todays_appts_error_icon').hide();
        $('#quick_reports_spinner, #todays_appts_spinner').show();

        $('#quick_reports_messages_text, #todays_appts_messages_text').text('Downloading Appointment Visit Sheet').show();
        quickClaim.showOverlayMessage('Downloading Appointment Visit Sheet');

        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
    }
};