var billAppointments = {
	actions: {
		search: null,
		setupBillings: null
	},

	initialize: function() {
		
                sk.datepicker('#bill_appointments_start_date');
                
                $('#bill_appointments_start_date').on('close', function () {
                    $('#bill_appointments_start_date').datepick('option', 'defaultDate', $(this).val());
                })
                
		$('#bill_appointments_start_date').on('changeDate',function(e) { 
                    $('#bill_appointments_end_date').skDP('setStartDate', e.date);
                });

                 sk.datepicker('#bill_appointments_end_date');
                 
		$('#bill_appointments_end_date').on('changeDate',function(e) { 
                    $('#bill_appointments_start_date').skDP('setEndDate', e.date);
                });

		$('#billAppointmentsButton').button();
		$('#billAppointmentsButton').click(function() {
			billAppointments.handleSearchClick();
		});

		$('#billAppointmentsDialog').dialog({
			autoOpen: false,
			closeOnEscape: true,
			modal: true,
			height: $(window).height() - 100,
			width: $(window).width() - 200
		});
	},
	handleSearchClick: function() {
		var params = $('#create_claims_form').serialize();

		$.ajax({
			type: 'post',
			dataType: 'json',
			url: billAppointments.actions.search,
			data: params,
			success: function(rs) {
				if (rs.hasOwnProperty('error')) {
					$('#bill_appointments_error_icon').show();
					$('#bill_appointments_messages_text').text(rs.error).show();
				}
				else {
					$('#billAppointmentsDialog').dialog('open');
					billAppointments.drawBillAppointmentsTable(rs, 'billAppointmentsDialog', '');
					$('#bill_appointments_messages_text').text('').hide();
				}
			},
			beforeSend: function() {
				$('#bill_appointments_checkmark').hide();
				$('#bill_appointments_error_icon').hide();
				$('#bill_appointments_spinner').show();

				$('#bill_appointments_messages_text').text('Retrieving Billable Appointments').show();
			},
			complete: function() {
				$('#bill_appointments_spinner').hide();
			},
			error: function() {
				$('#bill_appointments_error_icon').show();
				$('#bill_appointments_messages_text').text('An error occurred while contacting the server.');
			}
		});
	},
	drawBillAppointmentsTable: function(data, div_id, action_url) {
		var table = '<table class="tablesorter1 table table-hover" id="' + div_id + '_results_table">' + '<thead><tr>';
		var header = data.header;
		var colspan = 1;

		$('#' + div_id).html('');
		table += '<th>&nbsp;</th>';
		for (var key in header) {
			if (header.hasOwnProperty(key)) {

                if (header[key] === "Appointment Date") {
                    table += '<th class="sorter-shortDate dateFormat-ddmmyyyy">' + header[key] + '</th>';
                    colspan++;
                } else {
                    table += '<th>' + header[key] + '</th>';
                    colspan++;
                }
			}
		}
		table += '</tr></thead><tbody></tbody></table>';
		$('#' + div_id).html(table);

		var data = data.data;
		for (var key in data) {
			row = '<tr id="bill_appointments_' + key + '" style="visibility:visible">';
			row += '<td><input type="checkbox" id="bill_appointment_' + data[key]['appointment_attendee_id'] + '" name="bill_appointment[' + data[key]['appointment_attendee_id'] + ']" value="' + data[key]['appointment_attendee_id'] + '" checked="checked" /></td>';
			for (var key2 in header) {
				var d = data[key][key2] == null ? ' ' : data[key][key2];
				if(key2 == 'start_date') {
				row += '<td class="' + key2 + '" data-order="'+data[key]['start_date_sort']+'">' + d + '</td>';
				} else {
				row += '<td class="' + key2 + '">' + d + '</td>';	
				}
			}
			row += '</tr>';
			$(row).appendTo($('#' + div_id + ' table tbody'));
		}

		var footer = '<button onclick="$(\'#create_claims_button\').trigger(\'click\')" class="btn btn-success btn-squared" type="button"><i class="clip-file-plus"></i> Create Claims</button><input style="display:none" class="btn btn-primary btn-squared" type="submit" value="Create Claims" id="create_claims_button" />';
		$(footer).appendTo($('#' + div_id));

		if (data && data.length) {
			$('#' + div_id + '_results_table').tablesorter1({
				widgets: ['hover', 'zebra']
			});
		}

		$('#create_claims_button').button();
		$('#create_claims_button').click(function() {
			var ids = '';
			$('#billAppointmentsDialog_results_table input:checked').each(function() {
				if (ids.length) {
					ids += ',';
				}
				ids += $(this).val();
			});
			
			var html = '<form id="billable_appointments_results_form" method="POST" action="' + billAppointments.actions.setupBillings + '">'
				+ '<input type="hidden" value="' + ids + '" id="ids" name="ids" /></form>';
			
			$(html).appendTo($('#' + div_id));
			$('#billable_appointments_results_form').submit();
		});
	}
};
