var hl7_inbound = {
    actions: {
        acknowledgeByIds: null,
        acknowledgeGroup: null,
        details: null,
        duplicate: null,
        form_error_data: null,
        processed: null
    },
    details_key: null, // set when requesting the details screen -> is the id of the table row requesting the details

    initialize: function () {
        $('#hl7_inbound_processed_successfully').addClass('pointer');
        $('#hl7_inbound_duplicate_claims').addClass('pointer');
        $('#hl7_inbound_form_errors').addClass('pointer');

        $('#hl7_inbound_processed_successfully').click(function () {
            hl7_inbound.loadProcessedReport();
        });
        $('#hl7_inbound_duplicate_claims').click(function () {
            hl7_inbound.loadDuplicateReport();
        });
        $('#hl7_inbound_form_errors').click(function () {
            hl7_inbound.loadFormErrorReport();
        });

        $('#acknowledge_all_details').button();
        $('#acknowledge_all_details').click(function () {
            hl7_inbound.acknowledgeAllGroupMessages();
        });

        $('#hl7_processed_dialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            width: '80%',
            height: $(window).height() - 200
        });

        $('#hl7_details_dialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            width: '80%',
            height: $(window).height() - 200
        });

        $('#hl7_duplicate_dialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            width: '80%',
            height: $(window).height() - 200
        });

        $('#hl7_form_error_dialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            width: '80%',
            height: $(window).height() - 200
        });

//		hl7_inbound_fatal_errors
//		hl7_inbound_outbound_messages
        groupedContextMenu.initialize();
        detailContextMenu.initialize();
        errorDetailContextMenu.initialize();
    },

    acknowledgeIndividualMessage: function (ids) {
        var params = 'ids=' + ids + '&key=' + hl7_inbound.details_key;

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: hl7_inbound.actions.acknowledgeByIds,
            data: params,
            success: function (rs) {
                $('#' + ids).remove();
                $('.tablesorter1').trigger("update");
                $('.tablesorter1').trigger('applyWidgets');

                hl7_inbound.buildProcessedReport(rs);

                for (var a in rs['counts']) {
                    var status = rs['counts'][a]['status_name'];
                    status = status.replace(/ /g, '_').toLowerCase();
                    if ($('#hl7_inbound_' + status).size()) {
                        $('#hl7_inbound_' + status + ' .bignum').text(number_format(rs['counts'][a]['status_count'], 0, '.', ','));
                    }
                }

                $('#hl7_details_messages_text').text('').hide();
                $('#hl7_details_checkmark').show();
            },
            beforeSend: function () {
                $('#hl7_details_error_icon').hide();
                $('#hl7_details_checkmark').hide();
                $('#hl7_details_spinner').show();

                $('#hl7_details_messages_text').text('Acknowledging HL7 Details.').show();
            },
            complete: function () {
                $('#hl7_details_spinner').hide();
            },
            error: function () {
                $('#hl7_details_error_icon').show();
                $('#hl7_details_messages_text').text('An error occurred while contacting the server.');
            }
        });
    },
    acknowledgeAllGroupMessages: function () {
        ids = '';

        $('#hl7_details_table tbody tr').each(function () {
            if (ids.length) {
                ids += ',';
            }

            ids += $(this).prop('id');
        });

        var params = 'ids=' + ids + '&key=' + hl7_inbound.details_key;

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: hl7_inbound.actions.acknowledgeByIds,
            data: params,
            success: function (rs) {
                hl7_inbound.buildProcessedReport(rs);

                for (var a in rs['counts']) {
                    var status = rs['counts'][a]['status_name'];
                    status = status.replace(/ /g, '_').toLowerCase();
                    if ($('#hl7_inbound_' + status).size()) {
                        $('#hl7_inbound_' + status + ' .bignum').text(number_format(rs['counts'][a]['status_count'], 0, '.', ','));
                    }
                }

                $('#hl7_details_messages_text').text('').hide();
                $('#hl7_details_checkmark').show();
                $('#hl7_details_dialog').dialog('close');
            },
            beforeSend: function () {
                $('#hl7_details_error_icon').hide();
                $('#hl7_details_checkmark').hide();
                $('#hl7_details_spinner').show();

                $('#hl7_details_messages_text').text('Acknowledging HL7 Details.').show();
            },
            complete: function () {
                $('#hl7_details_spinner').hide();
            },
            error: function () {
                $('#hl7_details_error_icon').show();
                $('#hl7_details_messages_text').text('An error occurred while contacting the server.');
            }
        });
    },
    acknowledgeMessages: function (key) {
        var params = 'key=' + key;

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: hl7_inbound.actions.acknowledgeGroup,
            data: params,
            success: function (rs) {
                $('#' + rs['key']).remove();
                $('.tablesorter1').trigger("update");
                $('.tablesorter1').trigger('applyWidgets');

                for (var a in rs['counts']) {
                    var status = rs['counts'][a]['status_name'];
                    status = status.replace(/ /g, '_').toLowerCase();
                    if ($('#hl7_inbound_' + status).size()) {
                        $('#hl7_inbound_' + status + ' .bignum').text(number_format(rs['counts'][a]['status_count'], 0, '.', ','));
                    }
                }

                $('#hl7_processed_messages_text').text('').hide();
                $('#hl7_processed_checkmark').show();
            },
            beforeSend: function () {
                $('#hl7_processed_error_icon').hide();
                $('#hl7_processed_checkmark').hide();
                $('#hl7_processed_spinner').show();

                $('#hl7_processed_messages_text').text('Acknowledging HL7 Details.').show();
            },
            complete: function () {
                $('#hl7_processed_spinner').hide();
            },
            error: function () {
                $('#hl7_processed_error_icon').show();
                $('#hl7_processed_messages_text').text('An error occurred while contacting the server.');
            }
        });
    },
    buildDetailsReport: function (data) {
        $('#hl7_details_table').remove();

        var a = null;
        var table = '<table id="hl7_details_table" class="table table-hover table-striped"><thead></thead><tbody></tbody></table>';

        $('#hl7_details_data').append(table);

        var row = '<tr>';

        // build header
        for (var a in data.headers) {
            row += '<th class="' + a + '">' + data.headers[a] + '</th>';
        }
        row += '<th class="actions">Actions</th>';
        row += '</tr>';

        $('#hl7_details_table thead').append(row);

        // build rows
        for (var a in data.data) {
            var row = '<tr id="' + a + '">';
            for (var b in data.headers) {
                if (b == 'log_text') {
                    data.data[a][b] = '<div><pre>' + data.data[a][b] + '</pre></div>';
                }
                row += '<td class="' + b + '">' + data.data[a][b] + '</td>';
            }
            row += '<td class="context_menu actions">'
                    + '<button class="detail_actions_button" id="detail_actions_' + a + '" class="ui-button-inline">Menu</button>&nbsp;&nbsp;'
                    + '</td>'
                    + '</tr>';

            $('#hl7_details_table tbody').append(row);
            detailContextMenu.createButton('detail_actions_' + a);
        }

        if (a != null) {
            $('#hl7_details_table').tablesorter1({
                widgets: ['hover', 'zebra']
            });
        }
    },
    buildErrorDetailsReport: function (data) {
        $('#hl7_details_table').remove();

        var a = null;
        var table = '<table id="hl7_details_table" class="table table-hover table-striped"><thead></thead><tbody></tbody></table>';

        $('#hl7_details_data').append(table);

        var row = '<tr>';

        // build header
        for (var a in data.headers) {
            row += '<th class="' + a + '">' + data.headers[a] + '</th>';
        }
        row += '<th class="actions">Actions</th>';
        row += '</tr>';

        $('#hl7_details_table thead').append(row);

        // build rows
        for (var a in data.data) {
            var row = '<tr id="' + a + '">';
            for (var b in data.headers) {
                if (b == 'log_text' || b == 'error_messages') {
                    data.data[a][b] = '<div><pre>' + data.data[a][b] + '</pre></div>';
                }
                row += '<td class="' + b + '">' + data.data[a][b] + '</td>';
            }
            row += '<td class="context_menu actions">'
                    + '<button class="error_detail_actions_button" id="detail_actions_' + a + '" class="ui-button-inline">Menu</button>&nbsp;&nbsp;'
                    + '</td>'
                    + '</tr>';

            $('#hl7_details_table tbody').append(row);
            errorDetailContextMenu.createButton('detail_actions_' + a);
        }

        if (a != null) {
            $('#hl7_details_table').tablesorter1({
                widgets: ['hover', 'zebra']
            });
        }
    },
    buildDuplicateReport: function (data) {
        $('#hl7_duplicate_table').remove();

        var a = null;
        var table = '<table id="hl7_duplicate_table" class="table table-hover table-striped"><thead></thead><tbody></tbody></table>';

        $('#hl7_duplicate_data').append(table);

        var row = '<tr>';

        // build header
        for (var a in data.headers) {
            row += '<th class="' + a + '">' + data.headers[a] + '</th>';
        }
        row += '<th class="actions">Actions</th>';
        row += '</tr>';

        $('#hl7_duplicate_table thead').append(row);

        // build rows
        for (var a in data.data) {
            var row = '<tr id="' + a + '">';
            for (var b in data.headers) {
                row += '<td class="' + b + '">' + data.data[a][b] + '</td>';
            }

            row += '<td class="context_menu actions">'
                    + '<button class="group_actions_button" id="actions_' + a + '" class="ui-button-inline">Menu</button>&nbsp;&nbsp;'
                    + '</td>'
                    + '</tr>';

            $('#hl7_duplicate_table tbody').append(row);
            groupedContextMenu.createButton('actions_' + a);
        }

        if (a != null) {
            $('#hl7_duplicate_table').tablesorter1({
                widgets: ['hover', 'zebra']
            });
        }
    },
    buildFormErrorReport: function (data) {
        $('#hl7_form_error_table').remove();

        var a = null;
        var table = '<table id="hl7_form_error_table" class="table table-hover table-striped"><thead></thead><tbody></tbody></table>';

        $('#hl7_form_error_data').append(table);

        var row = '<tr>';

        // build header
        for (var a in data.headers) {
            row += '<th class="' + a + '">' + data.headers[a] + '</th>';
        }
        row += '<th class="actions">Actions</th>';
        row += '</tr>';

        $('#hl7_form_error_table thead').append(row);

        // build rows
        for (var a in data.data) {
            var row = '<tr id="' + a + '">';
            for (var b in data.headers) {
                row += '<td class="' + b + '">' + data.data[a][b] + '</td>';
            }

            row += '<td class="context_menu actions">'
                    + '<button class="group_actions_button" id="actions_' + a + '" class="ui-button-inline">Menu</button>&nbsp;&nbsp;'
                    + '</td>'
                    + '</tr>';

            $('#hl7_form_error_table tbody').append(row);
            groupedContextMenu.createButton('actions_' + a);
        }

        if (a != null) {
            $('#hl7_form_error_table').tablesorter1({
                widgets: ['hover', 'zebra']
            });
        }
    },
    buildProcessedReport: function (data) {
        $('#hl7_processed_table').remove();

        var a = null;
        var table = '<table id="hl7_processed_table" class="table table-hover table-striped"><thead></thead><tbody></tbody></table>';

        $('#hl7_processed_data').append(table);

        var row = '<tr>';

        // build header
        for (var a in data.headers) {
            row += '<th class="' + a + '">' + data.headers[a] + '</th>';
        }
        row += '<th class="actions">Actions</th>';
        row += '</tr>';

        $('#hl7_processed_table thead').append(row);

        // build rows
        for (var a in data.data) {
            var row = '<tr id="' + a + '">';
            for (var b in data.headers) {
                row += '<td class="' + b + '">' + data.data[a][b] + '</td>';
            }

            row += '<td class="context_menu actions">'
                    + '<button class="group_actions_button" id="actions_' + a + '" class="ui-button-inline">Menu</button>&nbsp;&nbsp;'
                    + '</td>'
                    + '</tr>';

            $('#hl7_processed_table tbody').append(row);
            groupedContextMenu.createButton('actions_' + a);
        }

        if (a != null) {
            $('#hl7_processed_table').tablesorter1({
                widgets: ['hover', 'zebra']
            });
        }
        
    },
    loadDetails: function (key) {
        var params = 'key=' + key;
        hl7_inbound.details_key = key;

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: hl7_inbound.actions.details,
            data: params,
            success: function (rs) {
                if (rs.hasOwnProperty('report_type') && rs.report_type == 'errors') {
                    hl7_inbound.buildErrorDetailsReport(rs);
                    $('#hl7_details_dialog').dialog('open');
                } else {
                    hl7_inbound.buildDetailsReport(rs);
                    $('#hl7_details_dialog').dialog('open');
                }

                $('#hl7_processed_messages_text').text('').hide();
                $('#hl7_processed_checkmark').show();
            },
            beforeSend: function () {
                $('#hl7_processed_error_icon').hide();
                $('#hl7_processed_checkmark').hide();
                $('#hl7_processed_spinner').show();

                $('#hl7_processed_messages_text').text('Retrieving HL7 Details Report.').show();
            },
            complete: function () {
                $('#hl7_processed_spinner').hide();
            },
            error: function () {
                $('#hl7_processed_error_icon').show();
                $('#hl7_processed_messages_text').text('An error occurred while contacting the server.');
            }
        });
    },
    loadDuplicateReport: function () {
        var params = '';

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: hl7_inbound.actions.duplicate,
            data: params,
            success: function (rs) {
                hl7_inbound.buildDuplicateReport(rs);
                $('#hl7_duplicate_dialog').dialog('open');

                $('#hl7_messages_text').text('').hide();
                $('#hl7_checkmark').show();
            },
            beforeSend: function () {
                $('#hl7_error_icon').hide();
                $('#hl7_checkmark').hide();
                $('#hl7_spinner').show();

                $('#hl7_messages_text').text('Retrieving HL7 Duplicate Messages Report.').show();
            },
            complete: function () {
                $('#hl7_spinner').hide();
            },
            error: function () {
                $('#hl7_error_icon').show();
                $('#hl7_messages_text').text('An error occurred while contacting the server.');
            }
        });
    },
    loadFormErrorReport: function () {
        var params = '';

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: hl7_inbound.actions.form_error_data,
            data: params,
            success: function (rs) {
                hl7_inbound.buildFormErrorReport(rs);
                $('#hl7_form_error_dialog').dialog('open');

                $('#hl7_messages_text').text('').hide();
                $('#hl7_checkmark').show();
            },
            beforeSend: function () {
                $('#hl7_error_icon').hide();
                $('#hl7_checkmark').hide();
                $('#hl7_spinner').show();

                $('#hl7_messages_text').text('Retrieving HL7 Form Error Report.').show();
            },
            complete: function () {
                $('#hl7_spinner').hide();
            },
            error: function () {
                $('#hl7_error_icon').show();
                $('#hl7_messages_text').text('An error occurred while contacting the server.');
            }
        });
    },
    loadProcessedReport: function () {
        var params = '';

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: hl7_inbound.actions.processed,
            data: params,
            success: function (rs) {
                hl7_inbound.buildProcessedReport(rs);
                $('#hl7_processed_dialog').dialog('open');

                $('#hl7_messages_text').text('').hide();
                $('#hl7_checkmark').show();
            },
            beforeSend: function () {
                $('#hl7_error_icon').hide();
                $('#hl7_checkmark').hide();
                $('#hl7_spinner').show();

                $('#hl7_messages_text').text('Retrieving HL7 Processed Messages Report.').show();
            },
            complete: function () {
                $('#hl7_spinner').hide();
            },
            error: function () {
                $('#hl7_error_icon').show();
                $('#hl7_messages_text').text('An error occurred while contacting the server.');
            }
        });
    }
};

var detailContextMenu = {
    active: {
        claimDetails: true,
        acknowedge: true
    },
    actions: {
        claimDetails: null
    },

    initialize: function () {
        var parameters = {
            selector: '.detail_actions_button',
            trigger: 'hover',
            autoHide: true,
            className: 'detailContextMenu',
            items: {
                claimDetails: {
                    name: 'Claim Details',
                    callback: function (key, options, event) {
                        var row_id = detailContextMenu.getTriggeredItem(options);
                        var claim_id = $('#' + row_id + ' .claim_id').text();
                        if (!claim_id) {
                            var claim_id = $('#' + row_id + ' .duplicate_claim_id').text();
                        }

                        var url = detailContextMenu.actions.claimDetails + '?id=' + claim_id;
                        if (quickClaim.isRightClick(event)) {
                            window.open(url, 'details');
                        } else {
                            window.location = url;
                        }
                    }
                },
                acknowledge: {
                    name: 'Acknowledge Message',
                    callback: function (key, options, event) {
                        var key = detailContextMenu.getTriggeredItem(options);
                        hl7_inbound.acknowledgeIndividualMessage(key);
                    }
                }
            }
        };

        for (var a in this.active) {
            if (!this.active[a]) {
                delete parameters.items[a];
            }
        }

        if (!$.isEmptyObject(parameters.items)) {
            $.contextMenu(parameters);
        }
    },
    createButton: function (id) {
        $('#' + id).button({
            icons: {
                primary: "ui-icon-triangle-1-s"
            },
            text: false
        });
    },
    getTriggeredItem: function (options) {
        var id = $(options.$trigger).attr('id');
        if (id) {
            return id.replace('detail_actions_', '');
        }
        return null;
    }
};

var errorDetailContextMenu = {
    active: {
        claimDetails: true,
        acknowedge: true
    },
    actions: {
        editClaim: null
    },

    initialize: function () {
        var parameters = {
            selector: '.error_detail_actions_button',
            trigger: 'hover',
            autoHide: true,
            className: 'errorDetailContextMenu',
            items: {
                claimDetails: {
                    name: 'Edit Temporary Form',
                    callback: function (key, options, event) {
                        var row_id = errorDetailContextMenu.getTriggeredItem(options);
                        var claim_id = $('#' + row_id + ' .temporary_form_id').text();

                        var url = errorDetailContextMenu.actions.editClaim + '?id=' + claim_id;
                        if (quickClaim.isRightClick(event)) {
                            window.open(url, 'details');
                        } else {
                            window.location = url;
                        }
                    }
                },
                acknowledge: {
                    name: 'Acknowledge Message',
                    callback: function (key, options, event) {
                        var key = errorDetailContextMenu.getTriggeredItem(options);
                        hl7_inbound.acknowledgeIndividualMessage(key);
                    }
                }
            }
        };

        for (var a in this.active) {
            if (!this.active[a]) {
                delete parameters.items[a];
            }
        }

        if (!$.isEmptyObject(parameters.items)) {
            $.contextMenu(parameters);
        }
    },
    createButton: function (id) {
        $('#' + id).button({
            icons: {
                primary: "ui-icon-triangle-1-s"
            },
            text: false
        });
    },
    getTriggeredItem: function (options) {
        var id = $(options.$trigger).attr('id');
        if (id) {
            return id.replace('detail_actions_', '');
        }
        return null;
    }
};

var groupedContextMenu = {
    active: {
        details: true,
        acknowedge: true
    },

    initialize: function () {
        var parameters = {
            selector: '.group_actions_button',
            trigger: 'hover',
            autoHide: true,
            className: 'groupedContextMenu',
            items: {
                details: {
                    name: 'Details',
                    callback: function (key, options, event) {
                        var key = groupedContextMenu.getTriggeredItem(options);
                        hl7_inbound.loadDetails(key);
                    }
                },
                acknowledge: {
                    name: 'Acknowledge Messages',
                    callback: function (key, options, event) {
                        var key = groupedContextMenu.getTriggeredItem(options);
                        hl7_inbound.acknowledgeMessages(key);
                    }
                }
            }
        };

        for (var a in this.active) {
            if (!this.active[a]) {
                delete parameters.items[a];
            }
        }

        if (!$.isEmptyObject(parameters.items)) {
            $.contextMenu(parameters);
        }
    },
    createButton: function (id) {
        $('#' + id).button({
            icons: {
                primary: "ui-icon-triangle-1-s"
            },
            text: false
        });
    },
    getTriggeredItem: function (options) {
        var id = $(options.$trigger).attr('id');
        if (id) {
            return id.replace('actions_', '');
        }
        return null;
    }
};