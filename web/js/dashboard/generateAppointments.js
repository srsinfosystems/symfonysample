var generate = {
    actions: {
        deletePastGeneratedAppointments: null,
        validate: null
    },

    initialize: function () {
        $('#generateAppointmentsButton').button();
        $('#generateAppointmentsButton').click(function () {
            generate.sendGenerate();
        });

        $('#deleteGeneratedAppointments').button();
        $('#deleteGeneratedAppointments').click(function () {
            window.location = generate.actions.deletePastGeneratedAppointments;
        });

        sk.datepicker('#generate_date');
    },
    clearErrors: function () {
        $('#generateAppointmentsErrorMessages').removeClass('ui-state-error').removeClass('ui-state-highlight').text('').hide();
        $('#generateBlankAppointmentsForm label.error').remove();
        $('#generateBlankAppointmentsForm input.error').removeClass('error');
        $('#generateBlankAppointmentsForm select.error').removeClass('error');
    },
    sendGenerate: function () {
        if (quickClaim.ajaxLocked) {
            return;
        }

        generate.clearErrors();

        var data = $('#generateBlankAppointmentsForm').serialize();

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: generate.actions.validate,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#generateAppointmentsErrorMessages', '#generate', true);

                    $('#generateAppointments_error_icon').show();
                    $('#generateAppointments_messages_text').text('The form contains invalid data.').show();
                } else {
                    $('#generateAppointments_checkmark').show();
                    $('#generateAppointments_messages_text').text(rs.messages).show();
                }
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                $('#generateAppointments .icons div').hide();
                $('#generateAppointments_spinner').show();
                $('#generateAppointments_messages_text').text('Validating Form Values').show();
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                $('#generateAppointments_spinner').hide();
            },
            error: function () {
                $('#generateAppointments_error_icon').show();
                $('#generateAppointments_messages_text').text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });
    }
}