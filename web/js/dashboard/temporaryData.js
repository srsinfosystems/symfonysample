/**
 * Created by Scott on 1/17/2017.
 */

var temporaryForm = {
    actions: {
        pullData: null,
        deleteAll: null
    },
    fields: {
        isAdmin: false
    },
    methods: {
        deleteData: function(ids) {
            quickClaim.showOverlayMessage('Deleting Claims . . . ')
             $.ajax({
                 type: 'post',
                 url: temporaryForm.actions.deleteAll,
                 dataType: 'json',
                 data: 'ids=' + ids,
                 success: function(rs) {

                     location.reload();

                 },
                 beforeSend: function() {

                 },
                 complete: function() {

                 },
                 error: function() {

                 }
             });
        },
        pullData: function(userid) {
            $.ajax({
                type: 'post',
                url: temporaryForm.actions.pullData,
                dataType: 'json',
                data: "user=" + userid,
                success: function(rs) {
                    $('.temporary_list').empty();
                    if (rs.length > 0) {

                        var html = '';
                        for (var i = 0; i < rs.length; i++) {
                            html += '<li id="temporary_item_"' + rs[i].temporary_form_id + '">';

                            html += '<a href="/claim/temporary_form/id/' + rs[i].temporary_form_id + '">';
                            html += 'Claim Data: <br> ' + rs[i].fname + ' ' + rs[i].lname + '<br> Services: &nbsp;';

                            for (var x = 0; x < rs[i].claim_items.length; x++) {
                                html += 'Code: ' + rs[i].claim_items[x].service_code + ', ' + rs[i].claim_items[x].service_date + '&nbsp; | ';
                            }

                            html += '</li>';
                        }

                        $('.temporary_list').append(html);
                    }

                },
                beforeSend: function() {

                },
                complete: function() {

                },
                error: function() {

                }
            });
        },
        init: function() {
            $('.search_terms').first().focus();

            if (window.location.pathname.indexOf('temp') != 1) {
                this.pullData($('#filter_user').val());
            }

            $('#filter_user').change(function () {
                temporaryForm.methods.pullData($('#filter_user').val());
            });

            $('#delete_selected').click(function() {
                var selectedIds = [];

                $('.temporary_select:checkbox:checked').each(function( i ) {
                    var temp = $(this).attr('data-rowid');
                    selectedIds.push(temp);
                });

                if (selectedIds.length > 10) {
                    alertify.confirm('Confirm', 'You\'re about to delete 10+ claims, continue?', function(){
                            temporaryForm.methods.deleteData(selectedIds);
                        },
                        function(){

                        });
                } else {
                    temporaryForm.methods.deleteData(selectedIds);
                }
            });
        }
    }
}