var errorReportItems = {
	actions: {
		getItem: null
	},
	lineNumbers: {
		HX1: null,
		HXH: null,
		HXT: null
	},

	initialize: function() {
		$('#errorReportItemCount').click(function () {
			errorReportItems.handleRequest();
		});

		$('#errorReportItemDialog').dialog({
			autoOpen: false,
			closeOnEscape: true,
			modal: true,
			height: $(window).height() - 100,
			width: $(window).width() - 100
		});
		
		$('#errorReportItemHX1').keyup(function() {
			errorReportItems.parseHX1();
		});
		$('#errorReportItemHXH').keyup(function() {
			errorReportItems.parseHXH();
		});
		$('#errorReportItemHXT').keyup(function() {
			errorReportItems.parseHXT();
		});
		
		$('#erroReportSaveButton').button();
		$('#erroReportSaveButton').click(function() {
			errorReportItems.handleRequest();
		});
		
		$('#errorReportNextRecordButton').button();
		$('#errorReportNextRecordButton').click(function() {
			errorReportItems.handleRequest(true);
		});
	},
	
	parseHX1: function() {
		var line = $('#errorReportItemHX1').val();
		
		$('#HX1GroupNum').text(line.substring(23, 27));
		$('#HX1ProviderNum').text(line.substring(27, 33));
		$('#HX1SpecCode').text(line.substring(33, 35));
	},
	parseHXH: function() {
		var line = $('#errorReportItemHXH').val();

		$('#HXHHealth').text(line.substring(3, 13));
		$('#HXHVersionCode').text(line.substring(13,15));
		$('#HXHPatientDob').text(line.substring(15, 23));
		$('#HXHAcctNum').text(line.substring(23, 31));
		$('#HXHPayProg').text(line.substring(31, 35));
		$('#HXHRefDoc').text(line.substring(35, 41));
		$('#HXHFacilityNum').text(line.substring(41, 45));
		$('#HXHAdmitDate').text(line.substring(45, 53));
		$('#HXHRefLab').text(line.substring(53, 57));
		$('#HXHSLI').text(line.substring(57, 61));
		$('#HXHExplCodes').text(line.substring(64, 67) + '--' + line.substring(67, 70) + '--' + line.substring(70, 73) + '--' + line.substring(73, 76) + '--' + line.substring(76, 79));
	},
	parseHXT: function() {
		var line = $('#errorReportItemHXT').val();
		
		$('#HXTServiceCode').text(line.substring(3, 8));
		$('#HXTDiagCode').text(line.substring(26, 30));
		$('#HXTFeeSubm').text(line.substring(10, 14) + '.' + line.substring(14, 16));
		$('#HXTServiceDate').text(line.substring(18, 26));
		$('#HXTExplCodes').text(line.substring(64, 67) + '--' + line.substring(67, 70) + '--' + line.substring(70, 73) + '--' + line.substring(73, 76) + '--' + line.substring(76, 79));
	},
	handleRequest: function(skipSave) {
		if (quickClaim.ajaxLocked) { return; }

		$('#errorReportText pre').text('');
		var data = $('#errorReportItemForm').serialize();
	
		if (skipSave) {
			data += '&skipSave=true';
		}
		
		$.ajax({
			type: 'post',
			url: errorReportItems.actions.getItem,
			dataType: 'json',
			data: data,
			success: function(rs) {
				errorReportItems.clearDisplay();
				if (rs.hasOwnProperty('data')) {
					$('#errorReportItemDialog').dialog('open');
					
					$('#errorReportItemID').val(rs.data.id);
					$('#errorReportItemIDText').text(rs.data.id);
					$('#errorReportItemHX1').val(rs.data.HX1Line);
					$('#errorReportItemHXH').val(rs.data.HXHLine);
					$('#errorReportItemHXT').val(rs.data.HXTLine);
					$('#errorReportFileName').text(rs.data.fileName);
					
					errorReportItems.lineNumbers.HX1 = rs.data.HX1LineNum;
					errorReportItems.lineNumbers.HXH = rs.data.HXHLineNum;
					errorReportItems.lineNumbers.HXT = rs.data.HXTLineNum;
					
					$('#error_reports_messages_text, #unparsable_messages_text').text('');
					$('#error_reports_checkmark, #unparsable_checkmark').show();
				}

				errorReportItems.parseHX1();
				errorReportItems.parseHXH();
				errorReportItems.parseHXT();

				if (rs.hasOwnProperty('fileText')) {
					errorReportItems.buildFileTextDiv(rs.fileText);
				}
				
			},
			beforeSend: function() {
				quickClaim.ajaxLocked = true;
				$('#error_reports_messages .icons div, #unparsable_messages .icons div').hide();
				$('#error_reports_spinner, #unparsable_spinner').show();
	
				$('#error_reports_messages_text, #unparsable_messages_text').text('Requesting Unparsable Error Report Item');
			},
			complete: function() {
				quickClaim.ajaxLocked = false;
				$('#error_reports_spinner, #unparsable_spinner').hide();
			},
			error: function() {
				$('#error_reports_error_icon, #unparsable_error_icon').show();
				$('#error_reports_messages_text, #unparsable_messages_text').text('An error occurred while contacting the server.  Please refresh the page and try again.');
			}
		});
	},
	
	clearDisplay: function() {
		$('#errorReportItemID').val('');
		$('#errorReportItemIDText').text('');
		$('#errorReportItemHX1').val('');
		$('#errorReportItemHXH').val('');
		$('#errorReportItemHXT').val('');
		$('#errorReportFileName').text('');
		
		errorReportItems.lineNumbers.HX1 = null;
		errorReportItems.lineNumbers.HXH = null;
		errorReportItems.lineNumbers.HXT = null;
	},
	buildFileTextDiv: function (text) {
		textFile = text.split("\r\n");
		textBlock = '';
		lineNum = 1;
		for (var a in textFile)	{
			className = ' class=""';
			if (lineNum == errorReportItems.lineNumbers.HX1) {
				className = ' class="HX1"';
			}
			else if (lineNum == errorReportItems.lineNumbers.HXH) {
				className = ' class="HXH"';
			}
			else if (lineNum == errorReportItems.lineNumbers.HXT) {
				className = ' class="HXT"';
			}
			
			textBlock += '<code' + className + '>' + textFile[a] + '</code>' + "\n";
			lineNum++;
		}
		$('#errorReportText pre').html(textBlock);
		
		errorReportItems.scrollToHXH();
	},
	scrollToHXH: function() {
		$('#errorReportText').delay(500).animate({scrollTop: $('#errorReportText code.HXH').position().top }, 1000);
	}
};