var quickClaim = {
    ajaxLocked: false,
    hcvLocked: false,
    serviceCodeLocked: false,
    contextMenuImage: '',
    dateFormat: null,
    overlayTimer: null,
    timeFormat: null,
    dateTimeFormat: null,
    skipToHCN: false,
    tablesorterTooltip: null,
    useAlphaPatientNumbers: false,
    displayWarning: false,
    warningMessage: 'There will be a brief service disruption today between 12 and 12:30 pm. You may experience delays or downtime. We apologize for this inconvenience.',

    checkPatientNumber: function (e, lname, hcn) {
        val = $(e).val();
        re = new RegExp(/[a-z]/i);

        if (val.match(re) && !quickClaim.useAlphaPatientNumbers) {
            $(lname).val(val);
            $(lname).setToEnd();
            $(e).val('');
        } else if (val.length > 7 && quickClaim.skipToHCN) {
            $(hcn).val(val);
            $(hcn).setToEnd();
            $(e).val('');
        }
    },
    focusTopField: function (form_id) {
        var scope = $(document);
        if (form_id) {
            var scope = $('#' + form_id);
        }
        // get any visible fields with an error class
        var errors = $('input:checkbox.error, input:password.error, input:radio.error, textarea.error, select.error, input:text.error', scope);
        var td_errors = $('td.error input:text, td.error input:password, td.error input:checkbox, td.error input:radio, td.error textarea, td.error select', scope);
        var visible = $('input:text:visible, input:password:visible, textarea:visible, select:visible', scope);

        if (errors.size()) {
            $(errors[0]).focus();
            $(errors[0]).setToEnd();
            return;
        }
        if (td_errors.size()) {
            $(td_errors[0]).focus();
            $(td_errors[0]).setToEnd();
            return;
        }
        if (visible.size()) {
            $(visible[0]).focus();
            $(visible[0]).setToEnd();
            return;
        }
    },
    formatDateFieldNew: function (field) {
        var val = $('#' + field).val();
        val = val.replace(/[\/-]/g, '');

        if (!val) {
            return;
        }

        var format = global_js.getNewDateFormat();
        var rs = '';
        var idx = 0;

        for (var a = 0; a < format.length; a++) {
            if ((format[a] == '/' || format[a] == '-')) {
                rs += format[a];
            } else if (val.length > idx) {
                rs += val[idx];
                idx++;
            }
        }
        $('#' + field).val(rs);
    },
    formatDateField: function (field) {
        var val = $('#' + field).val();
        val = val.replace(/[\/-]/g, '');

        if (!val) {
            return;
        }

        var format = quickClaim.dateFormat;
        var rs = '';
        var idx = 0;

        for (var a = 0; a < format.length; a++) {
            if ((format[a] == '/' || format[a] == '-')) {
                rs += format[a];
            } else if (val.length > idx) {
                rs += val[idx];
                idx++;
            }
        }
        $('#' + field).val(rs);
    },
    formFill: function (data, form_prefix, dialog_div, message_div, message) {
        for (var key in data) {
            if ($(form_prefix + key)) {
                if ($(form_prefix + key).attr('type') == 'checkbox') {
                    $(form_prefix + key).prop('checked', data[key]);
                } else if (key == 'dob' || key == 'hcn_exp_year' || key == 'admit_date') {
                    if (data[key] != '') {
                        if (key == 'dob') {
                            $(form_prefix + key).skDP('setDate', new Date(global_js.getFormattedDate(data[key])));
                        } else {
                            if (global_js.getFormattedDate(data[key]) != "Invalid date") {
                                //alert();
                                $(form_prefix + key).val(data[key]);
                                //$(form_prefix + key).skDP('setDate', new Date(global_js.getFormattedDate(data[key])));
                            }
                        }
                    }
                } else {
                    if ($(form_prefix + key).data('datepicker')) {
                        $(form_prefix + key).skDP('setDate', new Date(global_js.getFormattedDate(data[key])));
                    } else {
                        $(form_prefix + key).val(data[key]);
                    }
                    if ($(form_prefix + key).is('select')) {
                        $(form_prefix + key).trigger('change.select2');
                    }
                }
            }
        }
        if (dialog_div) {
            quickClaim.modals.removeModal(dialog_div);
        }

        if (message_div && message) {
            $(message_div)
                    .html(message)
                    .addClass('ui-state-highlight')
                    .show();
        }

        if ($('#patient_dob').val()) {
            quickClaim.calculateAge($('#patient_dob').skDP('getDate'), 'age');
        } else if ($('#claim_dob').val()) {
            quickClaim.calculateAge($('#claim_dob').skDP('getDate'), 'age');
        }

        if ($("#claimPatientName")) {
            $("#claimPatientName").text(data['fname'] + ' ' + data['lname']);
        }
    },

    handleScroll: function () {
        $('#overlayMessage .message').css('top', $(window).scrollTop() + 'px');
    },
    isRightClick: function (event) {
        if (event.hasOwnProperty('which')) {
            return event.which == 3;
        }
        return false;
    },

    setupDatePicker: function (id) {
        $('#' + id).datepick({
            dateFormat: quickClaim.dateFormat,
            changeMonth: false,
            renderer: $.datepick.themeRollerRenderer
        })
        .blur(function (e) {
            quickClaim.formatDateField(this.id);
        });
    },
    showFormErrors: function (errors, div_name, form_prefix, unselectable) {
        var message = '';
        var unselectable = unselectable ? ' UNSELECTABLE="on" ' : '';
        for (var key in errors) {
            if (key == '_global_') {
                if (errors[key].hasOwnProperty(message)) {
                    message += errors[key].message;
                } else {
                    message += errors[key];
                }
            } else {
                if (message.length) {
                    message += '<br />';
                }
                message += '<b' + unselectable + '>' + errors[key].label + '</b>: ' + errors[key].message;

                var name = (form_prefix + '[' + key + ']').replace('#', '');
                if (name == 'claim[doctor_id_0]' || name == 'claim[doctor_id_1]' || key == 'doctor_id') {
                    $("#s2id_claim_doctor_id a").attr('style', 'border: 1px solid #a94442 !important');

                } else if ($(form_prefix + '_' + key).size() && $(form_prefix + '_' + key).attr('type') != 'hidden') {
                    $(form_prefix + '_' + key).addClass('cliponeErrorInput');

                    field = form_prefix + '_' + key;
                    field = field.replace('#', '');

                    $('#' + field).tooltip({'trigger': 'hover', 'title': errors[key].message});//.attr('title', errors[key].message);
                } else if ($('[name="' + name + '"]').size() && $(form_prefix + '_' + key).attr('type') != 'hidden') {
                    name = '[name="' + name + '"]';
                    $(name).addClass('cliponeErrorInput');

                    field = form_prefix + '_' + key;
                    field = field.replace('#', '');

                    $(name + ':first').tooltip({'trigger': 'hover', 'title': errors[key].message});//.attr('title', errors[key].message);
                } else if ($('input.' + key).size() && $(form_prefix + '_' + key).attr('type') != 'hidden') {
                    field = form_prefix + '_' + key;
                    $('input.' + key + ':first').tooltip({'trigger': 'hover', 'title': errors[key].message});//.attr('title', errors[key].message);
                } else {
                    var new_key = key.substr(0, key.lastIndexOf('_'));
                    if ($(form_prefix + '_' + new_key).size() && $(form_prefix + '_' + new_key).attr('type') != 'hidden') {
                        $(form_prefix + '_' + new_key).addClass('cliponeErrorInput');

                        field = form_prefix + '_' + new_key;
                        field = field.replace('#', '');
                        $('#' + field).tooltip({'trigger': 'hover', 'title': errors[key].message});//.attr('title', errors[key].message);
                    }
                }
            }
        }

        setTimeout(function () {
            if (message.length > 0 && sk.hcmhidemsg == false) {
                if (sk.currentPage() == 'patient' && message == 'At least one of the following fields is required: ID / Chart Number, Date Of Birth, Health Card Number, First Name, Last Name, Sex, Phone, Email, Notes.') {
                    if ($("#tabs").tabs('option', 'active') == 1) {

                    } else {
                        sk.skNotify(message, 'danger');
                        $(div_name)
                                .html(message)
                                .addClass('alert alert-danger')
                                .show();
                    }
                } else {
                    sk.skNotify(message, 'danger');
                    $(div_name)
                            .html(message)
                            .addClass('alert alert-danger')
                            .show();
                    $(div_name).fadeIn('fast').delay(6500).fadeOut('fast');
                }
            }
        }, 300);
    },
    strpos: function (haystack, needle, offset) {
        var i = (haystack + '').indexOf(needle, (offset || 0));
        return i === -1 ? false : i;
    },

    modals: {
        current_modal: new Array(),
        addModal: function (type) {
            $('#' + type).dialog('open');
            this.current_modal.push(type);
        },
        pop: function (type) {
            var v = this.current_modal.pop();
        },
        removeModal: function (type) {
            var v = this.current_modal.pop();
            if (v == type) {
                $('#' + type).dialog('close');
            }
        },
        currentModal: function () {
            var v = this.current_modal.pop();
            this.current_modal.push(v);
            if (v) {
                return v;
            }
            return null;
        }
    },
    buildBulletedList: function (arr, list_type, list_options, li_options) {
        if (!li_options) {
            li_options = '';
        }
        if (!list_options) {
            list_options = '';
        }
        if (!list_type) {
            list_type = 'ul';
        }

        var list = '<' + list_type + ' ' + list_options + '>';

        for (var a in arr) {
            list += '<li ' + li_options + '>' + arr[a] + '</li>';
        }

        list += '</' + list_type + '>';

        return list;

    },
    // include_blank: 0: no;  1: if list length > 1;  2: always
    buildOptionsString: function (arr, include_blank) {
        var opts = '', count = 0;
        for (var a in arr) {
            opts += '<option value="' + a + '">' + arr[a] + '</option>';
            count++;
        }
        if (include_blank == 2 || (include_blank == 1 && count > 1)) {
            opts = '<option value=""></option>' + opts;
        }
        return opts;
    },
    buildRadioList: function (arr, name) {
        var opts = Array();
        var id = name.replace(']', '_').replace('[', '_');
        var count = 0;

        for (var a in arr) {
            opts[count] = '<input type="radio" id="' + id + a + '" name="' + name + '" value="' + a + '" />'
                    + '<label for="' + id + a + '">' + arr[a] + '</label>';
            count++;
        }
        return opts;
    },

    // sunil age calculation
    calculateAge: function (dob, widget_id) {
        // alert("hii");
        if (dob == null) {
            if (widget_id) {
                $('#' + widget_id).html('###Y ##M').show();
            }
            return;
        }
        var a = moment();
        var b = moment(dob);

        var years = a.diff(b, 'year');
        b.add(years, 'years');

        var months = a.diff(b, 'months');
        b.add(months, 'months');

//        var weeks = a.diff(b, 'weeks');
//        b.add(weeks, 'weeks');

        var days = a.diff(b, 'days');

        var age = [];

        // years
        if (years < 100) {
            age.push('0' + years + 'Y');
        } else {
            age.push(years + 'Y');
        }

        // months
        if (months < 10) {
            age.push('0' + months + 'M');
        } else {
            age.push(months + 'M');
        }

        // weeks
//        if (weeks > 0) {
//            age.push(weeks + 'W');
//        }

        // days
        if (days < 10) {
            age.push('0' + days + 'D');
        } else {
            age.push(days + 'D');
        }

        /*if (weeks > 9 && weeks < 13 && years == 0) {
         if (months < 10) {
         age.push('0' + months + 'M');
         } else {
         age.push(months + 'M');
         }
         
         if (weeks > 0)
         age.push(weeks + 'W');
         var days = a.diff(b, 'days');
         
         if (days < 7 && weeks == 0) {
         age.push(days + 'D');
         } else {
         b.add(weeks, 'weeks');
         age.push(days  + 'D');
         }
         }else if (weeks > 9 && weeks < 13 && years == 0) {
         var months = a.diff(b, 'months');
         b.add(months, 'months');
         var days = a.diff(b, 'days');
         
         if (months < 10) {
         age.push('0' + months + 'M');
         } else {
         age.push(months + 'M');
         }
         }*/

        if (widget_id) {
            $('#' + widget_id).html(age.join(' ')).show();
        }
        return;

        if (dob == '' || dob == null) {
            $('#' + widget_id).html('').hide();
            return;
        }
        var today = new Date();
        var birthDate = dob;

        var y = [today.getFullYear(), birthDate.getFullYear()],
                ydiff = y[0] - y[1],
                ydiff = y[0] - y[1],
                m = [today.getMonth(), birthDate.getMonth()],
                mdiff = m[0] - m[1],
                d = [today.getDate(), birthDate.getDate()],
                ddiff = d[0] - d[1];
        if (mdiff < 0 || (mdiff === 0 && ddiff < 0))
            --ydiff;
        if (mdiff < 0)
            mdiff += 12;
        if (ddiff < 0) {
            birthDate.setMonth(m[1] + 1, 0);
            ddiff = birthDate.getDate() - d[1] + d[0];
            --mdiff;
        }

        if (ydiff > 0) {
            var zero = '';
            if (ydiff < 10) {
                zero = '00';
            } else if (ydiff < 100) {
                zero = '0'
            } else {
                zero = '';
            }
            age.push(zero + ydiff + 'Y' + (ydiff > 1 ? ' ' : ' '));
        }
        if (mdiff > 0) {
            var zero = '';
            if (mdiff < 10) {
                zero = '0'
            }
            age.push(zero + mdiff + 'M' + (mdiff > 1 ? '' : ''));
        } else {
            age.push('00M');
        }

        if (widget_id) {
            $('#' + widget_id).html(age.join(' ')).show();
        }
        return;
    },
    clearMessages: function (message_div, clear_errors) {
        $('#' + message_div)
                .removeClass('alert alert-danger')
                .removeClass('ui-state-highlight')
                .html('')
                .hide();

        if (clear_errors) {
            $('label.error').remove();
            $('.error').removeClass('error');
            $('label.warning').remove();
            $('.warning').removeClass('warning');
        }
    },
    drawSearchResultsTable: function (data, header, id_name, div_name, id_prefix, form_prefix, message_div, message) {
        table_id = div_name.substr(1);
        table = '<table class="tablesorter1" id="' + table_id + '_results_table">' + '<thead><tr>';

        for (var key in header) {
            if (header.hasOwnProperty(key)) {
                table += '<th>' + header[key] + '</th>';
            }
        }
        table += '</tr></thead></table>';
        $(div_name).html(table);

        for (var key in data) {
            row = '<tr id="' + id_prefix + key + '">';
            for (var key2 in header) {
                var d = data[key][key2] == null ? ' ' : data[key][key2];
                row += '<td class="' + key2 + '">' + d + '</td>';
            }
            row += '</tr>';
            $(row).appendTo($(div_name + ' table'));

            $('#' + id_prefix + key).click(function () {
                k = this.id.replace(id_prefix, '');

                quickClaim.formFill(data[k], form_prefix, div_name, message_div, message);
            });
        }

        if ($('#' + table_id + '_results_table tbody tr').size()) {
            $('#' + table_id + '_results_table').tablesorter1({
                widgets: ['zebra', 'hover']
            });
        }
    },
    drawToolTip: function (target, tooltip) {
        $(target).attr('title', tooltip);
        $(target).tooltip();
    },
    formatDate: function (y, m, d) {
        if (y.length != 4) {
            y = '20' + y;
        }

        var rs = quickClaim.dateFormat;
        rs = rs.replace('dd', d);
        rs = rs.replace('mm', m);
        rs = rs.replace('yyyy', y);

        return rs;
    },
    formatPhone: function (f) {
        var value = quickClaim.stripNonNumeric($(f).val());

        if (value.length < 10) {
            rs = $(f).val();
        } else if (value.charAt(0) == '1') {
            rs = value.substr(0, 1) + '-' + value.substr(1, 3) + '-' + value.substr(4, 3) + '-' + value.substr(7, 4);

            if (value.length > 11) {
                rs += ' x ' + value.substr(11);
            }
        } else {
            rs = '(' + value.substr(0, 3) + ') ' + value.substr(3, 3) + '-' + value.substr(6, 4);
            if (value.length > 10) {
                rs += ' x ' + value.substr(10);
            }
        }
        $(f).val(rs);

    },
    formatPostal: function (f) {
        var value = $(f).val();

        value = value.replace(' ', '');

        if (value.length == 6) {
            value = value.substr(0, 3) + ' ' + value.substr(3, 3);
            $(f).val(value.toUpperCase());
        }
    },
    getParseDateFormat: function (format) {
        if (!format) {
            format = quickClaim.dateFormat;
        }
        format = format.replace('mm', 'MM');
        return format;
    },
    replace: function (str, rx) {
        return str.replace(rx, '');
    },
    setupToggleAll: function (className) {
        $('.' + className + ' input').click(function () {
            var count = $('.' + className + ' input:checked').size();
            if ($(this).val() == 'all' && $(this).prop('checked')) {
                $('.' + className + ' input:checked').each(function () {
                    if ($(this).val() != 'all') {
                        $(this).prop('checked', false);
                    }
                });
            } else if (count == 0) {
                $('.' + className + ' input[value=all]').prop('checked', true);
            } else if (count > 1) {
                $('.' + className + ' input[value=all]').prop('checked', false);
            }
        });
    },
    hideOverlayMessage: function (timeout) {
        if (timeout == null) {
            timeout = 1000;
        }
        $('#overlayMessage').fadeOut();
        quickClaim.overlayTimer = setTimeout(function () {
            $('#overlayMessage').remove();
        }, timeout);
        $("#spinner").fadeOut();
    },
    showOverlayMessage: function (message) {
        window.clearTimeout(quickClaim.overlayTimer);

        $("#messages_text").text(message);
        $("#spinner").fadeIn();

//		if ($('#overlayMessage').size() && $('#overlayMessage h3').is(':visible')) {
//			$('#overlayMessage h3').text(message);
//		}
//		else {
//			var height = $(window).height() / 2.5;
//			height = $(window).scrollTop() + height;
//
//			var overlay = '<div id="overlayMessage">'
//				+ '<div class="ui-widget-overlay"></div>'
//				+ '<div style="position: absolute; font-size: 1.25em; width: 40%; left: 30%; top: ' + height + 'px; padding: 20px;" class="message ui-widget ui-widget-content ui-corner-all">'
//				+ '<h3 style="text-align: center">' + message + '</h3></div>'
//				+ '</div>';
//
//			$(overlay).hide().appendTo('body');
//			$('#overlayMessage').fadeIn();
//		}
    },
    /**
     * @param divID - ID of div (without #)
     * @param icon - 'spin', 'error', 'success'
     * @param messageText, text string to put in messages text field
     */
    showAjaxMessage: function (divID, icon, messageText) {
        var div = $('#' + divID);

        div.find('.icons div').hide();
        div.find('.spinner').hide();
        div.find('.checkmark').hide();
        div.find('.error_icon').hide();

        if (messageText != null) {
            div.find('.text')
                    .text(messageText)
                    .show();
        }

        if (icon == 'spin') {
            div.find('.spinner').show();
        } else if (icon == 'error') {
            div.find('.error_icon').show();
        } else if (icon == 'success') {
            div.find('.checkmark').show();
        }


        div.show();
    },
    /**
     * @param divID - ID of div (without #)
     */
    hideAjaxMessage: function (divID) {
        $('#' + divID).hide();
    },

    setupSelectAllCheckbox: function (div_name, id, name) {
        var first_checkbox = $('#' + div_name + ' ul li input[type=checkbox]');
        var checkbox = '<li><input id="' + id + '_all" type="checkbox" name="' + name + '_all"><label for="' + id + '_all"> ALL</label></li>';

        if ($(first_checkbox).size()) {
            $(first_checkbox[0]).parent('li').parent('ul').prepend($(checkbox));
        } else {
            $('#' + div_name + ' ul').prepend($(checkbox));
        }

        $('#' + id + '_all').click(function () {
            var checked = $('#' + id + '_all').prop('checked');
            $('#' + div_name + ' input').prop('checked', checked);
        });
    },
    sortDate: function (a, b) {
        if (a > b)
            return 1;
        if (a < b)
            return -1;
        return 0;
    },
    stripNonNumeric: function (str) {
        return quickClaim.replace(str, /[^0-9]/g);
    },
    isHexColor: function (value) {
        if ((/[^#a-fA-F0-9]/g).test(value)) {
            return false;
        }
        if (value.lastIndexOf("#") !== 0) {
            // If # in any position but 0.
            return false;
        }
        if (value.length !== 4 && value.length !== 7) {
            return false;
        }
        return true;
    },
    addColorPicker: function (id, def_colour) {
        if (!$('#' + id).size()) {
            return;
        }
        $('#' + id).addClass('colorPicker');

        if (!$('#' + id).val()) {
            $('#' + id).val(def_colour ? def_colour : '#ffffff');
        }

        $('body').click(function (e) {
            if ($(e.target).is("input.colorPicker.focus")) {
                return;
            }
            $("div.picker-on").removeClass("picker-on");
            $("#picker").remove();
            $("input.focus, select.focus").removeClass("focus");
        });

        $('#' + id).change(function () {
            $(this).trigger("validate");
        });

        $('#' + id).click(function (e) {
            $(this).addClass('focus');
            $("#picker").remove();
            $("div.picker-on").removeClass("picker-on");
            $(this).after("<div id=\"picker\"></div>").parent().addClass("picker-on");
            $("#picker").farbtastic(this);
            e.preventDefault();
        });

        $('#' + id).on('validate', function () {
            if (quickClaim.isHexColor($(this).val())) {
                $(this).removeClass("state-error");
            } else {
                $(this).addClass("state-error");
            }
        });

        $('#' + id).trigger("validate");
        $('#' + id).farbtastic('#' + id);
    },

    highlightVersionDifferences: function (tableID) {
        if ($('#' + tableID + ' tbody tr').size() > 1) {

            var baseRow = null;
            $('#' + tableID + ' tbody tr').each(function () {
                if (baseRow) {
                    $('td', $(this)).each(function () {
                        var className = $(this).attr('class');
                        var baseRowText = $('.' + className, baseRow).text();
                        var thisRow = $(this).text();

                        if (baseRowText != thisRow) {
                            $('.' + className, baseRow).addClass('highlightDifferences');
                        }
                    })

                }
                baseRow = $(this);
            });
        }
    }
};

$('document').ready(function () {
    if (quickClaim.displayWarning) {
        $.notify(quickClaim.warningMessage, {
            element: 'body',
            type: "warning",
            allow_dismiss: true,
            newest_on_top: true,
            placement: {
                from: "top",
                align: "right"
            },
            delay: 10000,
            z_index: 99999,
            mouse_over: 'pause',
            animate: {
                enter: 'animated bounceIn',
                exit: 'animated bounceOut'
            },
            template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                            <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                            <span data-notify="icon"></span>
                                            <span data-notify="title">{1}</span>
                                            <span data-notify="message">{2}</span>
                                            <div class="progress" data-notify="progressbar">
                                                    <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <a href="{3}" target="{4}" data-notify="url"></a>
                                    </div>`

        });

    }
});


function closeNotify()
{
     $('#form_message')
                .removeClass('ui-state-error').removeClass('ui-state-highlight')
                .html('').hide();
     $('.error').removeClass('error');
}


$.fn.clearForm = function (error_div) {
    if (error_div) {
        $('#' + error_div).removeClass('alert alert-danger').removeClass('ui-state-highlight').html('').hide();
        $(' .error', this).removeClass('error');
    }

    return this.each(function () {
        var type = this.type,
                tag = this.tagName.toLowerCase();

        if (tag == 'form')
            return $(':input', this).clearForm();

        if (quickClaim.strpos(this.id, '_csrf_token')) {
//			// do nothing
        } else if (type == 'text' || type == 'password' || tag == 'textarea') {
            this.value = '';
        } else if (type == 'checkbox' || type == 'radio')
            this.checked = false;
        else if (tag == 'select')
            this.selectedIndex = 0;
        else if (type == 'hidden')
            this.value = '';
    });
};

function number_format(number, decimals, dec_point, thousands_sep) {
    // http://kevin.vanzonneveld.net
    var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? '' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}


$.maxZIndex = $.fn.maxZIndex = function (opt) {
    var def = {inc: 10, group: "*"};
    $.extend(def, opt);
    var zmax = 0;
    $(def.group).each(function () {
        var cur = parseInt($(this).css('z-index'), 10);
        zmax = cur > zmax ? cur : zmax;
    });
    if (!this.jquery)
        return zmax;

    return this.each(function () {
        zmax += def.inc;
        $(this).css("z-index", zmax);
    });
};

$.fn.setToEnd = function () {
    if ($(this).val()) {
        var length = $(this).val().length;
        $(this).selectRange(length, length);
    }
};

$.fn.selectRange = function (start, end) {
    return this.each(function () {
        if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    });
};
