var patient_search = {
    actions: {
        search: null,
        hcn_search: null,
        retrieve: null,
        walkinAppointments: null,
        healthcard: null,
    },
    buttons: {
        search: 'search_button_patient',
        clear: 'search_button_clear',
        create: 'search_button_create'
    },
    messages: {
        checkmark: 'checkmark',
        error: 'error_icon',
        spinner: 'spinner',
        text: 'messages_text'
    },
    conformToLeave: "0",
    newValueFlag: "0",
    oldValue: "",
    is_cardswiped: false,
    is_processing: false,
    fields: [],
    message_div: 'search_message',
    prefix_search: 'patient_search',
    form_id: 'search_form',
    mode: 'patient_search',
    results_dialog: 'search_results_dialog',
    results_table: 'search_results_dialog_table',
    tab_index: null,
    currentLocation: null,
    currentMatchType: null,
    last_field: null,
    edit_target: null,
    extra_search_params: null,
    showResultsDialogOnZeroResults: false,
    skipToHealthCardNumber: true,
    alwaysShowResultsDialog: false,
    selectedIndex: 0,
    doctorIdForWalkIn: null,
    prefix: 'patient',
    clientID: 0,


    initialize: function () {
        
        $("#patient_search_sex_F").attr('tabindex', '8');
        $("#patient_search_sex_M").attr('tabindex', '8');
        $("#patient_search_match_type_exact").attr('tabindex', '9');
        $("#patient_search_match_type_wildcard").attr('tabindex', '9');
        patient_search.clientID = 0;
        $(function(){
            $('.radiobutton').radioTabbable();
        });
        keyHandling.PatientSearch();
        //$('#claim_pay_prog option:first').attr('selected', true).trigger('change');

        var form = document.querySelector('form');
        Mousetrap(form).bind('esc', function (e) {
            if (e.preventDefault) {
                e.preventDefault();
            } else {
                // internet explorer
                e.returnValue = false;
            }
            patient_search.handleClearPress();
            return false;
        });

        $('#patient_search_patient_number, #patient_search_hcn_num').keyup(function(e) {
             var regex = new RegExp("^[a-zA-Z]+$");
             var searchKey = $(this).val();
             isValid = regex.test(searchKey);

             a = searchKey.split('');
             for (var i = 0; i < a.length; i++) {
             if (a[i] <= 'A' || a[i] >= 'z') delete a[i];
             }
             var st = a.join('');
             if(isValid === true)
             { 
                  //Check condition only for number
                  var preletter = $(this).val();
                  $(this).val('');
                  $('#patient_search_lname').focus();
                  $('#patient_search_lname').val(preletter);
             }
             else if(st != '')
             {
                  //check if alphanumeric valid exist
                  $(this).val('');
                  $('#patient_search_lname').focus();
                  $('#patient_search_lname').val(st);
             }
             else {      
                    if($(this).val().length > 8)
                    {
                        //check condition if numeric value greate then Length of 8 
                        var preletter = $(this).val();
                        $(this).val('');     
                        $('#patient_search_hcn_num').focus();
                        $('#patient_search_hcn_num').val(preletter);
                    }
             }
        });
        
        // Mousetrap(form).bind('enter', function (e) {
        //     if (!patient_search.is_processing && !patient_search.is_cardswiped) {
        //         patient_search.is_cardswiped = false;
        //         e.preventDefault();
        //         console.log('Patient Search Js');
        //         patient_search.handleSearchPress(e, false);
        //     }
        //     return false;
        // });

        $('#' + this.buttons.search).button();
        $('#' + this.buttons.search).click(function (e) {
            patient_search.is_cardswiped = false;
            if ($('#' + patient_search.results_dialog).dialog("isOpen")) {
                return;
            }
            patient_search.handleSearchPress(e, false);
            return false;
        });

        $('#' + this.buttons.clear).button();
        $('#' + this.buttons.clear).click(function () {
            patient_search.handleClearPress();
            $('#search_form #patient_search_patient_number').focus();
            return false;
        });

        $('#' + this.buttons.create).button();
        $('#' + this.buttons.create).click(function () {
            patient_search.handleCreatePress();
            return false;
        });


        $('#label_custom_template_button').button();
        $('#label_custom_template_button').click(function (event) {
            patient_search.healthcard = $('#' + patient_search.prefix+ '_hcn_num').val();

            if (!$('#patient_doctor_id').val()) {
                $('#patient_doctor_id').css('border-left', '1px solid red');
                return;
            }
            var doctorId = $('#patient_doctor_id').val();
            var radio = $('#template_radio').val();

            if (!radio) {
                $.ajax({
                    type: 'post',
                    url: patient_search.actions.template,
                    dataType: 'json',
                    success: function (rs) {
                        patient_search.clientID = rs['clientID'];
                        rs['templates'].forEach(addRadio);

                        function addRadio(element, index, array) {
                            $('#label_custom_template_button').after('<p></p><input type="radio" id="template_radio" name="template" value="' + element + '">' + element);
                        }
                    }
                });
            } else if (patient_search.clientID[0]) {
                var dataVar = 'patient_id=' + $('#' + patient_search.prefix + '_id').val() + '&template=' + $('#template_radio:checked').val() + '&healthcard=' + patient_search.healthcard + '&doctor_id=' + doctorId;
                $.ajax({
                    type: 'post',
                    url: patient_search.actions.replace,
                    data: dataVar,
                    success: function (rs) {
                        var url = 'https://' + rs;
                        window.open(url, '_blank');
                    }
                });
            }
        });

        //Create template button clone with success message
        $('#label_custom_template_claim_button').button();
        $('#label_custom_template_claim_button').live('click',function (event) {
            
            patient_search.healthcard = $('#' + patient_search.prefix + '_hcn_num').val();

            if (!$('#claim_patient_doctor_id').val()) {
                $('#claim_patient_doctor_id').css('border-left', '1px solid red');
                return;
            }
            var doctorId = $('#claim_patient_doctor_id').val();
            var radio = $('#template_radio').val();

            if (!radio) {
                $.ajax({
                    type: 'post',
                    url: patient_search.actions.template,
                    dataType: 'json',
                    success: function (rs) {
                        patient_search.clientID = rs['clientID'];
                        rs['templates'].forEach(addRadio);

                        function addRadio(element, index, array) {
                            $('#label_custom_template_claim_button').after('<p></p><input type="radio" id="template_radio" name="template" value="' + element + '">' + element);
                        }
                    }
                });
            } else if (patient_search.clientID[0]) {

                var dataVar = 'patient_id=' + $('#' + patient_search.prefix + '_id').val() + '&template=' + $('#template_radio:checked').val() + '&healthcard=' + patient_search.healthcard + '&doctor_id=' + doctorId;

                $.ajax({
                    type: 'post',
                    url: patient_search.actions.replace,
                    data: dataVar,
                    success: function (rs) {
                        var url = 'https://' + rs;
                        window.open(url, '_blank');
                    }
                });
            }
        });


        //Appointment custom buttton
        $('#label_custom_template_appointment_button').button();
        $('#label_custom_template_appointment_button').click(function (event) {
            patient_search.healthcard = $('#' + patient_search.prefix + '_hcn_num').val();
            
            if (!$('#appointment_doctor_id').val()) {
                $('#appointment_doctor_id').css('border-left', '1px solid red');
                return;
            }
            var doctorId = $('#appointment_doctor_id').val();
            var radio = $('#template_radio').val();
            if (!radio) {
                $.ajax({
                    type: 'post',
                    url: patient_search.actions.template,
                    dataType: 'json',
                    success: function (rs) {
                        patient_edit.clientID = rs['clientID'];
                        rs['templates'].forEach(addRadio);

                        function addRadio(element, index, array) {
                            $('#label_custom_template_appointment_button').after('<p></p><input type="radio" id="template_radio" name="template" value="' + element + '">' + element);
                        }
                    }
                });
            } else if (patient_search.clientID[0]) {

                var dataVar = 'patient_id=' + $('#' + patient_search.prefix + '_id').val() + '&template=' + $('#template_radio:checked').val() + '&healthcard=' + patient_search.healthcard + '&doctor_id=' + doctorId;

                $.ajax({
                    type: 'post',
                    url: patient_search.actions.replace,
                    data: dataVar,
                    success: function (rs) {
                        var url = 'https://' + rs;
                        window.open(url, '_blank');
                    }
                });
            }
        });


        var date = new Date();
        var yearsRange = (date.getFullYear() - 100) + ':' + date.getFullYear();

        sk.datepicker('#' + this.prefix_search + '_dob');

        if ($('#' + this.prefix_search + '_patient_number').size()) {
            $('#' + this.prefix_search + '_patient_number').keyup(function (e) {
                if (!patient_search.is_processing) {
                    quickClaim.checkPatientNumber(
                            '#' + patient_search.prefix_search + '_patient_number',
                            '#' + patient_search.prefix_search + '_lname',
                            '#' + patient_search.prefix_search + '_hcn_num');
                }
            });
        }

        $('#' + this.prefix_search + '_phone').blur(function (e) {
            quickClaim.formatPhone('#' + patient_search.prefix_search + '_phone');
        });
 
        $('#' + this.results_dialog).dialog({
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            width: 1200,
            height: 600,
            open: function () {
                hideTooltips();
            }
        });

        $('#' + this.buttons.search).tooltip();
        $('#' + this.buttons.clear).tooltip();

        $('#' + this.form_id).submit(function () {
            return false;
        });
        $('.new_patient').click(function (e) {
            $('.new_patient').hide();
            patient_search.handleOneResult(patient_search.edit_target.search_cache, true);
            return false;
        });

        $('#create_from_search').click(function () {
            patient_search.handleOneResult(patient_search.edit_target.search_cache, true);
            $(this).hide();
        });
        
        $("#patient_hcn_num, #patient_search_hcn_num").blur(function(){
            var value = $(this).val();
            if(value.length > 10){
                $(this).addClass('sk-border-red');
            } else {
                $(this).removeClass('sk-border-red');
            }
        });
    },

    clearMessages: function () {
        $('#' + patient_search.message_div)
                .text('')
                .removeClass('alert alert-danger')
                .removeClass('alert alert-danger');
    },
    handleClearPress: function () {
        $('#' + patient_search.form_id).clearForm();
        $('#' + patient_search.prefix_search + '_location_id').val(this.currentLocation);
        //alert('#' + patient_search.prefix_search + '_match_type_' + this.currentMatchType);
        $('#' + patient_search.prefix_search + '_match_type_' + this.currentMatchType).prop('checked', true);
        $('#patient_hcn_num').focus();
        return false;
    },
    setCurrentLoc: function (l) {
        patient_search.currentLocation = l;
    },
    setCurrentMatchType: function (v) {
        patient_search.currentMatchType = v;
    },
    handleCreatePress: function () {
        var data = patient_search.getPatientSearchTerms();
        patient_search.handleOneResult(data, true);
    },
    handleOneResult: function (data, is_new) {
        edit_target = patient_search.edit_target;

        if (data.hasOwnProperty('truncated_dataset')) {
            edit_target.handleClearPress();
            $('#' + patient_search.results_dialog).dialog('close');

            var data = 'patient_id=' + data.id
                    + '&return_type=json'
                    + '&mode=' + patient_search.mode;

            $.ajax({
                type: 'post',
                dataType: 'json',
                data: data,
                url: patient_search.actions.retrieve,
                success: function (rs) {
                   

                    var serialized = JSON.stringify(rs.data[0]);  
                    console.log(rs.data[0]);                  
                    workflowActions.handleHealthCardEntry('patient update');
                    if(rs.doctors){
                        patient_search.doctorIdForWalkIn = rs.doctors[0]['id'];
                    }
                    if (!rs.hasOwnProperty('data')) {
                        $('#' + patient_search.messages.error).show();
                        $('#' + patient_search.messages.text).html('An error occurred when performing this search.  Please refresh the page and try again.').show();
                    } else {
                        //$('#oldallvlue').val(serialized);
                        var res = edit_target.loadData(rs.data[0]);
                        var PatientOldForm = $('#patient_form').serialize();
                        $('#oldpatientdata').val(PatientOldForm);
                        $('#tabs').tabs('option', 'disabled', []);

                        if (res !== false) {
                            switchTabs(edit_target.tab_id);
                            patient_search.handleClearPress();

                            $('#' + patient_search.messages.checkmark).show();
                            $('#' + patient_search.messages.text).html('Loaded patient data.').show();
                            patient_search.selectedIndex = 0;
                        }
                        patient_search.conformToLeave = "1";

                    }

                    $('#todaysAppointments').empty();
                    $.ajax({
                        type: 'post',
                        dataType: 'html',
                        data: 'patient_id=' + rs.data[0]['id'],
                        url: patient_search.actions.walkinAppointments,
                        success: function (result) {
                            // console.log(result);
                            $('#todaysAppointments').html(result);
                        }
                    });                    
                },
                beforeSend: function () {
                    patient_search.is_processing = true;
                    $('#messages .icons div').hide();
                    $('#' + patient_search.messages.spinner).show();

                    $('#' + patient_search.messages.text).html('Retrieving additional patient data...').show();
                },
                complete: function () {
                    patient_search.is_processing = false;
                    $('#' + patient_search.messages.spinner).hide();
                    $("#patient_form select").each(function () {
                        $(this).trigger('change');
                    });
                },
                error: function () { 
                    patient_search.is_processing = false;
                    $('#' + patient_search.messages.error).show();
                    $('#' + patient_search.messages.text).html('<div class="alert alert-info">An error occurred while contacting the server.  Please refresh the page and try again.</div>').show();
                }
            });
            return false;
        } else {
            $('#' + patient_search.results_dialog).dialog('close');
            edit_target.handleClearPress();
            var res = edit_target.loadData(data, is_new);

            if (res !== false) {
                switchTabs(edit_target.tab_id);
                patient_search.handleClearPress();
                console.log('here');
                if (is_new) {
                    $('#patient_lname').focus();
                }
            }
        }
    },
    handleSearchPress: function (event) {
        if (patient_search.is_processing == true) {
            return false;
        }
        if ($('#' + patient_search.results_dialog).dialog('isOpen')) {
            if (patient_search.edit_target.canCreatePatients) {
                return patient_search.handleNewPatient();
            } else {
                patient_search.clearMessages();
                $('#' + patient_search.messages.error).show();
                $('#' + patient_search.messages.text).text('You cannot create a new patient.').show();
                return;
            }
        }

        var ignored_fields = ['button_patient', 'button_clear', 'notes'];
        if (event) {
            element = $(event.currentTarget.activeElement).attr('id');

            for (var a in ignored_fields) {
                if (element == patient_search.prefix_search + '_' + ignored_fields[a]) {
                    return false;
                }
            }
        }
        patient_search.clearMessages();
        if (alertify) {
            alertify.dismissAll();
        }
        var data = $('#' + patient_search.form_id).serialize();
        data += '&return_type=json';
        data += '&mode=' + patient_search.mode;
        data += '&prefix=' + patient_search.prefix_search;

        var cache = patient_search.getPatientSearchTerms();

        patient_search.edit_target.search_cache = cache;

        $('#' + patient_search.results_table + ' table').remove();
        $('#create_from_search').hide();
        $('.new_patient').show();
        $.ajax({
            type: 'post', 
            dataType: 'json',
            url: patient_search.actions.search,
            data: data,
            success: function (rs) { 
                // console.log(rs);
                // return false;
//                if(typeof rs['data'][0] == 'undefined'){
//                    quickClaim.showFormErrors(rs.errors, '#' + patient_search.message_div, '#' + patient_search.form_id, true);
//                    quickClaim.focusTopField();
//
//                    $('#' + patient_search.messages.error).show();
//                    $('#' + patient_search.messages.text).html('<div class="alert alert-info">The form contains invalid data.</div>').show();
//
//                }else{
//                    if(rs['data'][0]['sex'] != ''){
//                        $('#s2id_claim_sex .select2-choice').find('.select2-chosen').html(rs['data'][0]['sex']);
//                    } else {
//                        $('#s2id_claim_sex .select2-choice').find('.select2-chosen').html('[Sex]');
//                    }
//                    if (rs['data'][0]['claim_type'] != '' && rs['data'][0]['claim_type'] != null) {
//                        $('#s2id_claim_pay_prog .select2-choice').find('.select2-chosen').html(rs['data'][0]['claim_type']);
//                    } else {
//                        $('#s2id_claim_pay_prog .select2-choice').find('.select2-chosen').html('HCP-P');
//                    }
//                }
                if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#' + patient_search.message_div, '#' + patient_search.form_id, true);
                    quickClaim.focusTopField();

                    $('#' + patient_search.messages.error).show();
                    $('#' + patient_search.messages.text).html('<div class="alert alert-info">The form contains invalid data.</div>').show();
                } else if (!rs.hasOwnProperty('data')) {
                    $('#' + patient_search.messages.error).show();
                    $('#' + patient_search.messages.text).html('<div class="alert alert-info">An error occurred when performing this search.  Please refresh the page and try again.</div>').show();
                } else if (rs.data.length == 0) {
                    if (!patient_search.edit_target.canCreatePatients) {
                        $('#' + patient_search.messages.error).show();
                        $('#' + patient_search.messages.text).html('<div class="alert alert-info">Received no search results. Try a different search.</div>').show();
                    } else {

                        $('#' + patient_search.messages.error).show();
                        $('#' + patient_search.messages.text).html('<div class="alert alert-info">Received no search results.</div>').show();
                        patient_search.edit_target.handleNoSearchResultDialog();
                    }
                } else if (rs.data.length == 1 && !patient_search.alwaysShowResultsDialog) {
                    $('#' + patient_search.messages.checkmark).show();
                    $('#' + patient_search.messages.text).html('<div class="alert alert-info">Received one search result</div>').show();
                    patient_search.conformToLeave = "1";
                    $('#todaysAppointments').empty();
                    $.ajax({
                        type: 'post',
                        dataType: 'html',
                        data: 'patient_id=' + rs.data[0]['id'],
                        url: patient_search.actions.walkinAppointments,
                        success: function (result) {
                            // console.log(result);
                            $('#todaysAppointments').html(result);
                        }
                    });
                   
                    patient_search.handleOneResult(rs.data[0], false);

                    if (patient_search.edit_target.require_search && patient_search.edit_target.canCreatePatients) {
                        $('#create_from_search').show();
                    } else if (patient_search.edit_target.canCreatePatients) {
                        $('#messages_create').css('display', 'inline-flex');
                        var t = setTimeout("$('#messages_create').hide();", 6000);
                    }
                } else {
                    $('#' + patient_search.messages.checkmark).show();
                    $('#' + patient_search.messages.text).html('<div class="alert alert-info">Received ' + rs.data.length + ' search results.</div>').show();
                    patient_search.handleSearchResultDialog(rs.data, rs.header);
                }

                return false;
            },
            beforeSend: function () {
                patient_search.is_processing = true;
                $('#messages .icons div').hide();
                $('#' + patient_search.messages.checkmark).hide();
                $('#' + patient_search.messages.error).hide();
                $('#' + patient_search.messages.spinner).show();
                $('#' + patient_search.messages.text).html('Searching for patients').show();
                $('#messages_create').hide();
            },
            complete: function () {
                patient_search.is_processing = false;
                $('#' + patient_search.messages.spinner).hide();
                $("#patient_form select").each(function () {
                    $(this).trigger('change');
                });
            },
            error: function () {
                patient_search.is_processing = false;
                $('#' + patient_search.messages.error).show();
                $('#' + patient_search.messages.text).html('<div class="alert alert-info">An error occurred while contacting the server.  Please refresh the page and try again.</div>').show();

            }
        });

        return false;
    },
    handleSearchResultDialog: function (data, header) {
        $('#' + patient_search.results_dialog).dialog('open');

        $('#search_results_count').text(data.length + ' results found').show();

        if (data.length) {
            var table = '<table class="table sk-datatable" id="' + patient_search.results_table + '_results_table"><thead><tr>';
            var custButton = `<div class="custActionBtnssearch_results_dialog_table_results_table">
                <a href="javascript:void(0)" class="new_patient btn btn-success btn-squared" style="font-size:14px;color:#fff;"><i class="clip-file-plus"></i> Create a new patient from your search terms.</a>
            </div>`;
            for (var key in header) {
                if (header.hasOwnProperty(key)) {
                     if(key == 'dob'){
 
                        table += '<th class="sorter-false" data-sksort="date">' + header[key] + '</th>';
 
                    } else {
 
                        table += '<th class="sorter-false">' + header[key] + '</th>';
 
                    }
 
                }
            }
            table += '</thead></tr>' + '<tbody></tbody><tfoot><tr>';
            for (var key in header) {
                if (header.hasOwnProperty(key)) {
                    table += '<th></th>';
                }
            }
            table += '</tr></tfoot></table>';

            //table += custButton;

            $('#' + patient_search.results_table).html($(table));

            for (var key in data) {
                var class_name = data[key].hasOwnProperty('row_class') ? data[key]['row_class'] : '';
                var row = '<tr id="search_result_' + key + '" class="' + class_name + ' clickable_row">';

                for (var key2 in header) {
                    var d = data[key][key2] == null ? ' ' : data[key][key2];
                    row += '<td class="' + key2 + '">' + d + '</td>';
                }

                row += '</tr>';
                $(row).appendTo($('#' + patient_search.results_table + ' table tbody'));

                $('#search_result_' + key).click(function () {
                    k = this.id.replace('search_result_', '');
                    patient_search.handleOneResult(data[k], false);
                });

                localStorage.setItem('patient_search_' + key, JSON.stringify(data[key]));

            }

//            $('#' + patient_search.results_table + '_results_table').tablesorter1({
//                tabIndex: true,
//                widgets: ['zebra', 'hover']
//            });

            $('#' + patient_search.results_table + '_results_table tbody tr').attr('tabindex', 0);

            $('#search_result_0').focus();

            // make arrows keys change row focus
            $('body').on('keydown', function (e) {
                if (e.which == 13) {
                    var indexElement = $('#' + patient_search.results_dialog).dialog('isOpen');
                    if (indexElement && !patient_search.is_processing) {
                        var id = $("#" + patient_search.results_table + '_results_table tbody .selected').attr('id');
                        var index = $("#" + patient_search.results_table + '_results_table tbody .selected').attr('id');//$(':focus').attr('id');
                        index = index.substr(14);
                        if (index) {
                            patient_search.selectedIndex = index;
                            if (data[index]) {
                                patient_search.handleOneResult(JSON.parse(localStorage.getItem('patient_search_' + index)), false);
                            }
                        }
                    }
                }
            });
        } else {
            $('#' + patient_search.results_table + '_table').html('');
        }
        global_js.customFooterDataTableWithOptions(patient_search.results_table + '_results_table', true);
        //sk.table.init(patient_search.results_table + '_results_table');
        //sk.table.loader(patient_search.results_table + '_results_table');
        var table = $("#" + patient_search.results_table + '_results_table').DataTable();
        table
                .on('key-focus', function (e, datatable, cell) {
                    datatable.rows().deselect();
                    datatable.row(cell.index().row).select();
                });
        table.row(0).select();
        table.cell(':eq(0)').focus();

    },
    getPatientSearchTerms: function () {
        var cache = {};
        if (patient_search.extra_search_params == 'mode=appointment_book') {
            cache['mode'] = 'appointment_book';
        }
        $('#' + patient_search.form_id + ' input, #' + patient_search.form_id + ' select').each(function () {
            if ($(this).attr('class') != 'select2-input' && $(this).attr('id') != patient_search.prefix_search + '__csrf_token' && $(this).attr('type') != 'button' && $(this).attr('id') != patient_search.prefix_search + '_patient_number') {

                if ($(this).attr('id') == 's2id_patient_search_sex') {
                    cache[$(this).attr('id').replace(patient_search.prefix_search + '_', '')] = $(this).select2("val");
                } else {
                    cache[$(this).attr('id').replace(patient_search.prefix_search + '_', '')] = $(this).val();
                }
            }
        });
        return cache;
    }
};
