var merge = {
	actions: {
		merge: null
	},
	search_patient_number: null,
	patient_data: {
		one: {},
		two: {}
	},

	initialize: function() {
		$('#merge_into_1').button();
		$('#merge_into_2').button();
		$('#patient_1_search').button();
		$('#patient_2_search').button();

		$('#search').dialog({
			closeOnEscape: false,
			autoOpen: false,
			height: 650,
			modal: true,
			width:  '70%'
		});

		$('#patient_1_search').click(function() {
			merge.search_patient_number = 1;
			merge.patient_data.one = {};

			merge.handleClearPress();
			merge.toggleMergeButtons();
			$('#search').dialog('open');
		});

		$('#patient_2_search').click(function() {
			merge.search_patient_number = 2;
			merge.patient_data.two = {};

			merge.handleClearPress();
			merge.toggleMergeButtons();
			$('#search').dialog('open');
		});

		$('#merge_into_1').click(function() {
			merge.sendMergeRequest('one');
		});
		$('#merge_into_2').click(function() {
			merge.sendMergeRequest('two');
		});

		merge.toggleMergeButtons();
	},
	handleClearPress: function() {
		$('.patient_' + merge.search_patient_number + '_prop').text('');
	},
	loadData: function(data, isNew) {
		var num = merge.search_patient_number;
		var id = num == 1 ? 'one' : 'two';

		merge.patient_data[id] = data;

		for (var a in data)	{
			if ($('#patient_' + num + '_' + a).size()) {
				$('#patient_' + num + '_' + a).text(data[a])
			}

		}
		merge.toggleMergeButtons();
		$('#search').dialog('close');
	},
	sendMergeRequest: function(primary) {
		var deleteId = null;
		var mergeId = null;

		if (primary == 'one') {
			mergeId = merge.patient_data['one'].id;
			deleteId = merge.patient_data['two'].id;
		}
		else {
			mergeId = merge.patient_data['two'].id;
			deleteId = merge.patient_data['one'].id;
		}

		if (mergeId == deleteId) {
			alert('Patient Records are the same');
			return;
		}

		var data = 'deleteRecord=' + deleteId + '&masterRecord=' + mergeId;

		$.ajax({
			type: 'post',
			url: merge.actions.merge,
			dataType: "json",
			data: data,
			success: function(rs) {
				if (rs.hasOwnProperty('error')) {
					$('#error_icon').show();
					$('#messages_text').html('<div class="alert alert-info">'+rs.error+'</div>');
					alert(rs.error);
					return;
				}
				else {
					merge.patient_data.one = {};
					merge.search_patient_number = 1;
					merge.handleClearPress();

					merge.patient_data.two = {};
					merge.search_patient_number = 2;
					merge.handleClearPress();

					merge.toggleMergeButtons();

					$('#checkmark').show();
					$('#messages_text').html('<div class="alert alert-info">Patient Records Merged Successfully.</div>');
				}
			},
			beforeSend: function() {
				$('#messages .icons div').hide()
				$('#spinner').show();

				$('#messages_text').html('<div class="alert alert-info">Merging Patient Records.</div>');
			},
			complete: function() {
				$('#spinner').hide();
			},
			error: function() {
				$('#error_icon').show();
				$('#messages_text').html('<div class="alert alert-info">An error occurred while contacting the server.  Please refresh the page and try again.</div>');
			}

		});
	},
	toggleMergeButtons: function() {
		if (merge.patient_data.one.hasOwnProperty('id') && merge.patient_data.two.hasOwnProperty('id')) {
			$('#merge_into_1').show();
			$('#merge_into_2').show();
		}
		else {
			$('#merge_into_1').hide();
			$('#merge_into_2').hide();
		}
	}
};