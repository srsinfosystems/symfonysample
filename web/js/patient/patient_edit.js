
var patient_edit = {
    actions: {
        address_label: null,
        autocomplete_refdoc: null,
        hcv: null,
        hcv_enabled: null,
        save: null,
        hcv_reset: null,
        versionHistory: null,
        template: null,
        healthcard: null,
        labels: null,
        update_hcv_status: null
    },
    copy_with_hcv: false,
    activeRefDocPrefix: null,
    ajax_lock: false,
    appointment_form_prefix: null,
    appointment_form_id: null,
    bookable_patient_statuses: null,
    default_patient_status: null,
    defaultPatientType: null,
    followUpDate: '',
    form_id: 'patient_form',
    hcv_status: 'status_text',
    message: 'edit_message',
    prefix: 'patient',
    require_search: false,
    search_cache: new Array(),
    canCreatePatients: true,
    showMohProvince: false,
    clientID: 0,
    hcn_status: null,

    tab_index: null,
    tab_id: null,
    tab_default: 'Edit Patient',
    tab_edit: 'Patient Info',

    versionsLoaded: false,

    warning_fields: {'Sex': 'sex', 'Date Of Birth': 'dob', 'Referring Doctor': 'ref_doc_description', 'Health Card Number': 'hcn_num'},
    warning_past_dates: {'Expiry Date (should be future date)': 'hcn_exp_year'},
    warning_list: 'warning_list',
    warning_made: false,
    warning_modal: 'warning_dialog',
    warning_button_yes: 'warning_continue',
    warning_button_no: 'warning_stop',

    reason_codes: {},
    locations_provinces: {},

    currentLocation: null,
    original_status_id: null,

    default_title: '',
    default_province: '',

    facility: {
        actions: {
            lookup: null
        },
        cache: [],
        data: {},
        ajaxLock: false,

        initialize: function () {
            for (var a in patient_edit.facility.data) {
                patient_edit.facility.cache[patient_edit.facility.cache.length] = patient_edit.facility.data[a].toString;
            }

            facilitiesList.isDialog = true;
            facilitiesList.dialogParent = 'patient_edit';

            var $description = $('#patient_facility_description');

            $description.autocomplete({
                minLength: 1,
                select: function (event, ui) {
                    var facilityNum = ui.item.label.substring(0, 4);

                    patient_edit.facility.fillInFacility(facilityNum);

                    event.stopPropagation();
                },
                source: patient_edit.facility.cache
            });

            $description.blur(function () {
                var facilityNum = $(this).val().substring(0, 4);
                patient_edit.facility.fillInFacility(facilityNum);
            });

            $('#facility_num_dialog').dialog({
                closeOnEscape: true,
                modal: true,
                autoOpen: false,
                height: $(window).height() - 100,
                width: $(window).width() - 200,
                open: function () {
                    facilitiesList.initialize();
                }
            });

            $('#facility_num_dialog1').dialog({
                closeOnEscape: true,
                modal: true,
                autoOpen: false,
                height: $(window).height() - 100,
                width: $(window).width() - 200,
                open: function () {
                    facilitiesList.initialize();
                }
            });

            $('#facility_num_dialog2').dialog({
                closeOnEscape: true,
                modal: true,
                autoOpen: true,
                height: $(window).height() - 100,
                width: $(window).width() - 200,
                open: function () {
                    facilitiesList.initialize();
                }
            });



            $('#search_facility')
                    .button()
                    .click(function () {
                        $('#facility_num_dialog').dialog('open');
                    });

            $('#search_fam_doc')
                    .button()
                    .click(function () {
                        $('#facility_num_dialog1').dialog('open');
                    });

            $('#search_ref_doc')
                    .button()
                    .click(function () {
                        $('#facility_num_dialog2').dialog('open');
                    });

        },

        fillInFacility: function (facilityNum) {
            if (patient_edit.facility.data.hasOwnProperty(facilityNum)) {
                var d = patient_edit.facility.data[facilityNum];

                $('#patient_facility_num').val(d.facility_num);
                $('#patient_facility_id').val(d.facility_id);
                $('#patient_facility_description').val(d.toString);

                $('#facility_num_dialog').dialog('close');
                $('#facility_num_dialog1').dialog('close');
                $('#facility_num_dialog2').dialog('close');
            } else {
                patient_edit.facility.lookup(facilityNum);
            }
        },

        lookup: function (facilityNum) {
            if (patient_edit.facility.ajaxLock) {
                return false;
            }

            var data = 'facilityNum=' + facilityNum;

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: patient_edit.facility.actions.lookup,
                data: data,
                success: function (rs) {
                    if (rs.hasOwnProperty('data') && rs.data.hasOwnProperty('0')) {
                        patient_edit.facility.data[rs.data[0].facility_num] = rs.data[0];
                        patient_edit.facility.fillInFacility(rs.data[0].facility_num);
                    } else {
                        $('#patient_facility_num').val($('#patient_facility_description').val());
                        $('#patient_facility_id').val('');
                    }
                },
                beforeSend: function (rs) {
                    patient_edit.facility.ajaxLock = true;
                },
                complete: function (rs) {
                    patient_edit.facility.ajaxLock = false;
                },
                error: function (rs) {
                }
            });
        }
    },

    initialize: function () {
        patient_edit.setupRefDocAutocomplete('ref_doc');
        patient_edit.setupRefDocAutocomplete('fam_doc');

        patient_edit.clientID = 0;

        var preloadTab = patient_edit.getParameterByName('tab', location.href);

        if (preloadTab) {
            $('#tabs').tabs('option', 'active', preloadTab);
        }

        var date = new Date();

        Date.prototype.yyyymmdd = function () {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = this.getDate().toString();
            return yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]); // padding
        };

        Date.prototype.mmddyyyy = function () {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = this.getDate().toString();

            return (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + '/' + yyyy;
        }

        Date.prototype.ddmmyyyy = function () {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = this.getDate().toString();

            return (dd[1] ? dd : "0" + dd[0]) + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + yyyy;
        }

        var yearsRange = (date.getFullYear() - 100) + ':' + date.getFullYear();

        sk.datepicker('#' + patient_edit.prefix + '_dob');

        $('#' + patient_edit.prefix + '_dob').on('changeDate', function (e) {
            if ($('#' + patient_edit.prefix + '_dob').val() != '') {
                quickClaim.calculateAge($('#' + patient_edit.prefix + '_dob').skDP('getDate'), 'age');
            }
        });

        $('#' + patient_edit.prefix + '_dob').on('blur', function (e) {
            if ($('#' + patient_edit.prefix + '_dob').val() != '') {
                quickClaim.calculateAge($('#' + patient_edit.prefix + '_dob').skDP('getDate'), 'age');
            }
        });

        $('#' + patient_edit.prefix + '_dob').on('clearDate', function (e) {
            quickClaim.calculateAge(null, 'age');

        });



        sk.datepicker('#' + patient_edit.prefix + '_hcn_exp_year');

        sk.datepicker('#' + patient_edit.prefix + '_admit_date');

        sk.datepicker('#' + patient_edit.prefix + '_follow_up_date');

        $('#' + this.prefix + '_cellphone').blur(function (e) {
            quickClaim.formatPhone('#' + patient_edit.prefix + '_cellphone');
        });

        $('#' + this.prefix + '_hphone').blur(function (e) {
            quickClaim.formatPhone('#' + patient_edit.prefix + '_hphone');
        });

        $('#' + this.prefix + '_wphone').blur(function (e) {
            quickClaim.formatPhone('#' + patient_edit.prefix + '_wphone');
        });

        $('#' + this.prefix + '_postal_code').blur(function (e) {
            quickClaim.formatPostal('#' + patient_edit.prefix + '_postal_code');
        });

        $('#' + this.prefix + '_dob').blur(function () {
//            quickClaim.calculateAge($('#' + patient_edit.prefix + '_dob').skDP('getDate'), 'age');
        });

        $('#' + this.prefix + '_patient_number').keydown(function (e) {
            quickClaim.checkPatientNumber('#' + patient_edit.prefix + '_patient_number', '#' + patient_edit.prefix + '_lname', '#' + patient_edit.prefix + '_hcn_num');
        });

        $('#' + this.prefix + '_patient_status_id').change(function (e) {
            patient_edit.setupReasonCodes();
        });

        $('#' + this.prefix + '_hcn_version_code').change(function (e) {
            //patient_edit.handleHcvReset();
        });

        $('#' + this.prefix + '_hcn_num').change(function (e) {
            //patient_edit.handleHcvReset();
        });

        //Follow up first click
        $('#' + this.prefix + '_follow_up').click(function () {
            patient_edit.updateFollowUpDate();
        });

        if (patient_edit.showMohProvince) {
            $('#' + this.prefix + '_province').change(function (e) {
                if (!$('#' + patient_edit.prefix + '_moh_province').val()) {
                    $('#' + patient_edit.prefix + '_moh_province').val($(this).val());
                }
            });
        }

        $('#' + this.prefix + '_patient_type_id').change(function () {
            patient_edit.togglePatientTypeMessages();
        });

        $('#' + patient_edit.warning_modal).dialog({

            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            position: {my: "center center", at: "center center"},
            height: 300,
            width: 700,
            postOpen: function () {
                $('#patient_id').focus();
                $('#warning_dialog').focus();
                $('#warning_continue').addClass('ui-state-hover');
            },
            open: function (event, ui) {
                $('html, body').animate({
                    scrollTop: $(".ui-dialog-titlebar").offset().top
                }, 500);
            }
        });

        $('#no_results_dialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            width: 700
        });

        $('#patient_save').button()
                .tooltip()
                .click(function (e) {
                    patient_edit.handleEnterPress();
                    return false;
                });

        $('#patient_clear, #add_new_patient, #add_next_patient').button()
                .tooltip()
                .click(function (e) {
                    patient_edit.handleClearPress();
                    return false;
                });

        /*$('#add_new_patient').button()
                .tooltip()
                .click(function (e) {
                    patient_edit.handleNewPatientPress();
                    return false;
                });*/
                

        $('#create_claim').button()
                .tooltip()
                .click(function (e) {
                    if (!$('#' + patient_edit.prefix + '_id').val()) {
                        alert('no active patient.');
                        return false;
                    }
                    window.location = patient_edit.actions.claim_edit + "/patient_id/" + $('#' + patient_edit.prefix + '_id').val();
                    return false;
                });

        $('#hcv_button').tooltip()
                .click(function (e) {
                    patient_edit.handleHcvPress();
                    return false;
                });

        $('#' + patient_edit.warning_button_yes).button()
                .tooltip()
                .click(function (e) {
                    patient_edit.handleWarningButtonYes();
                    return false;
                });

        $('#' + patient_edit.warning_button_no).button()
                .tooltip()
                .click(function (e) {
                    patient_edit.handleWarningButtonNo();
                    return false;
                });

        $('#create_appointment').button()
                .tooltip()
                .click(function () {
                    if (!$('#' + patient_edit.prefix + '_id').val()) {
                        alert('no active patient.');
                        return false;
                    }

                    if ($('#' + patient_edit.prefix + '_patient_status_id').size()) {
                        if (jQuery.inArray(parseInt($('#' + patient_edit.prefix + '_patient_status_id').val(), 10), patient_edit.bookable_patient_statuses) === -1) {
                            alert('You cannot book appointments for patients with this status.');
                            return false;
                        }
                    }

                    $('#' + patient_edit.appointment_form_prefix + '_patient_id').val($('#' + patient_edit.prefix + '_id').val());
                    $('#' + patient_edit.appointment_form_prefix + '_appointment_id').val('');
                    $('#' + patient_edit.appointment_form_prefix + '_mode').val('create');
                    $('#' + patient_edit.appointment_form_id).submit();
                });

        $('#no_results_create').button()
                .tooltip()
                .click(function () {
                    $('#no_results_dialog').dialog('close');
                    patient_search.handleOneResult(patient_search.edit_target.search_cache, true);
                });

        $('#no_results_cancel').button()
                .tooltip()
                .click(function () {
                    $('#no_results_dialog').dialog('close');
                });

        this.setHCVStatus();

        this.setupLocationWatcher();
        this.setupReasonCodes();

        this.setupPrintLinks();

        $('.patient_status_row').hide();
        $('.future_appointment_check').hide();

        if (!this.canCreatePatients) {
            $('.new_patient').remove();
            this.tab_default = 'Patient Details';
        }

        if (!$('#patient_id').val()) {
            $('#patient_patient_status_id').val(patient_edit.default_patient_status);
            patient_edit.loadData({});
        }

        if (!$('#patient_lname').val()) {
            // patient_edit.handleHcvPress();
        }

        patient_edit.loadPreviousPatients();


        patient_edit.facility.initialize();
        patient_edit.ClearField();
        patient_edit.copyMenuInit();

        $('#hcn_changed_warning').dialog({
            closeOnEscape: true,
            modal: true,
            autoOpen: false,
            height: 250,
            width: 500,
            buttons: [
                {
                    text: "Yes",
                    "class": 'btn btn-success',
                    click: function () {
                        $('#hcn_changed_warning').dialog('close');
                        patient_edit.handleSaveRequest();
                    }
                },
                {
                    text: "No",
                    "class": 'btn btn-danger',
                    click: function () {
                        $('#hcn_changed_warning').dialog('close');
                    }
                },
                {
                    text: "Save without deleting previous patient",
                    "class": 'btn btn-primary',
                    click: function () {
                        $('#hcn_changed_warning').dialog('close');

                        patient_edit.handlenewPatient();
                    }
                }
            ]
        });

    },
    handlenewPatient: function () {
        
        patient_edit.handleCopyEvent('full');

        patient_edit.clearMessages();
        patient_edit.clearWarnings();
        patient_edit.warning_made = false;

        $('#' + patient_edit.form_id).clearForm(patient_edit.message);

        $('#create_edit_title').text(patient_edit.tab_default);
        $('#patient_name').text(patient_edit.default_title);
        $('#age').text('');

        $('#' + patient_edit.hcv_status).text('').removeClass('error-text').removeClass('success-text');
        $("#hcv_results").html('');
        patient_edit.setOriginalLocId('');
        patient_edit.clearVersions();
        patient_edit.versionsLoaded = false;

        patient_history.clearData();
        patient_edit.copy_with_hcv = true;
        patient_edit.handlePasteEvent('full');

        patient_edit.handleSaveRequest();
    },
    ClearField: function () {
        $(".btnClearTxt").click(function () {
            var id = $(this).attr('data-id');
            $("#" + id).val('');
            if (id == 'patient_facility_description') {
                $("#patient_facility_num").val('');
                $("#patient_admit_date").skDP('setDate', null);
            }
        })
    },
    loadPreviousPatients: function () {
        lscache.flush();
    },
    getParameterByName: function (name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
        if (!results)
            return null;
        if (!results[2])
            return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    clearWarnings: function () {
        $('label.warning').remove();
        $('.warning').removeClass('warning');
        $('#warning_list').html('');
        $(".cliponeErrorInput").removeClass('cliponeErrorInput');
    },
    getCurrentAddress: function () {
        return $('#' + this.prefix + '_address').val()
                + ', '
                + $('#' + this.prefix + '_city').val()
                + ', '
                + $('#' + this.prefix + '_province').val()
                + ', '
                + $('#' + this.prefix + '_postal_code').val();
    },
    hasWarnings: function () {
        if (patient_edit.warning_made == true) {
            return true;
        } else {
            var warns = false;
            var today = Date.today();

            for (var idx in patient_edit.warning_fields) {
                if (!$('#' + patient_edit.prefix + '_' + idx).val()) {
                    warns = true;
                }
            }

            for (var a in patient_edit.warning_past_dates) {
                var val = Date.parseExact($('#' + patient_edit.prefix + '_' + patient_edit.warning_past_dates[a]).val(), quickClaim.getParseDateFormat(quickClaim.dateFormat));

                if (val && val < today) {
                    warns = true;
                }
            }
            return warns;
        }
    },
    makeWarnings: function () {
        patient_edit.clearWarnings();

        var message = 'Suggested';
        for (var idx in patient_edit.warning_fields) {
            var field = patient_edit.prefix + '_' + idx;
            if (!$('#' + field).val()) {
                $('#' + field).addClass('warning');
                $('#' + field).attr('data-toggle', 'tooltip');
                $('#' + field).attr('title', message);
//                if (field == 'patient_ref_doc_description') {
//                    
//                    $('#' + field).closest('.form-group').append($('<label class="warning help-block" style="color:#ff6b6b" for="' + field + '">' + message + '</label>'));
//                } else {
//                    $('#' + field).parent().append($('<label class="warning help-block" style="color:#ff6b6b" for="' + field + '">' + message + '</label>'));
//                }
                $('#warning_list').append($('<li>' + patient_edit.warning_fields[idx] + '</li>'));
            }
        }

        var today = Date.today();
        message = 'Past Date';
        for (var a in patient_edit.warning_past_dates) {
            field = patient_edit.prefix + '_' + patient_edit.warning_past_dates[a];
            var val = Date.parseExact($('#' + field).val(), quickClaim.getParseDateFormat(quickClaim.dateFormat));

            if (val && val < today) {
                $('#' + field).addClass('warning');
                if (field == 'patient_ref_doc_description') {
                    // $('#' + field).closest('.form-group').append($('<label class="warning help-block" style="color:#ff6b6b" for="' + field + '">' + message + '</label>'));
                } else {
                    $('#' + field).parent().append($('<label class="warning help-block" style="color:#ff6b6b" for="' + field + '">' + message + '</label>'));
                }
                $('#warning_list').append($('<li>' + a + '</li>'));
            }
        }

        $('#' + patient_edit.warning_modal).dialog('open');
        patient_edit.warning_made = true;
    },
    handleNewPatientPress: function () {
        patient_edit.clearMessages();
        patient_edit.clearWarnings();
        patient_edit.warning_made = false;

        $('#' + patient_edit.form_id).clearForm(patient_edit.message);

        $('#create_edit_title').text(patient_edit.tab_default);
        $('#patient_name').text(patient_edit.default_title);
        $('#age').text('');
        $("#patient_dob").skDP('setDate', null);

        $('#' + patient_edit.hcv_status).text('').removeClass('error-text').removeClass('success-text');
        $("#hcv_results").html('');
        patient_edit.setOriginalLocId('');
        patient_edit.clearVersions();
        patient_edit.versionsLoaded = false;

        patient_history.clearData();
        $('#patient_form select').each(function () {
            $(this).trigger('change.select2');
        })
        $(".pastPatients").html('');
        if (patient_edit.require_search) {
            $('#tabs').tabs('option', 'disabled', [1, 2, 3, 4, 5, 6]);
        }
    },
    handelUnsavePopup: function(event) {
        if($('#patient_form').serialize()!=$('#oldpatientdata').val() && $('#oldpatientdata').val() != '') {
            location.reload();
            event.preventDefault();
            return false;
        } else {
            return true;
        }
    },
    handleClearPress: function () {
        patient_edit.handelUnsavePopup();
        $('#oldpatientdata').val('');
        patient_edit.clearMessages();
        patient_edit.clearWarnings();
        patient_edit.warning_made = false;

        $('#' + patient_edit.form_id).clearForm(patient_edit.message);

        $('#create_edit_title').text(patient_edit.tab_default);
        $('#patient_name').text(patient_edit.default_title);
        $('#age').text('');
        $("#patient_dob").skDP('setDate', null);

        $('#' + patient_edit.hcv_status).text('').removeClass('error-text').removeClass('success-text');
        $("#hcv_results").html('');
        patient_edit.setOriginalLocId('');
        patient_edit.clearVersions();
        patient_edit.versionsLoaded = false;

        patient_history.clearData();
 
        $('#patient_location_id').val(patient_edit.currentLocation);
        
        $('#tabs').tabs('option', 'active', 0);
        $('#patient_form select').each(function () {
            $(this).trigger('change.select2');
        })
        $(".pastPatients").html('');
        if (patient_edit.require_search) {
            $('#tabs').tabs('option', 'disabled', [1, 2, 3, 4, 5, 6]);
        }
    },
    handleEnterPress: function (event) {
        var ignored_fields = ['notes', /*'ref_doc_description', 'fam_doc_description'*/];
        if (event) {
            var element = $(event.currentTarget.activeElement).attr('id');

            if (element == 'referring_doctor_provider_num') {
                return false;
            }

            for (var a in ignored_fields) {
                if (element == patient_edit.prefix + '_' + ignored_fields[a]) {
                    return false;
                }
            }
        }
        if (quickClaim.modals.currentModal() == patient_edit.warning_modal) {
            return patient_edit.handleWarningButtonYes();
        }

        if ($("#oldHCNCheck").val() != '') {
            if ($("#oldHCNCheck").val() != $("#patient_hcn_num").val()) {
                var name = $("#patient_lname").val() + ' ' + $("#patient_fname").val();
                $(".patientNameHCN").html(name);
                $('#hcn_changed_warning').dialog('open');
                return false;
            }
        }

        patient_edit.handleSaveRequest();
        return false;
    },
    clearMessages: function () {
        $('#' + patient_edit.message)
                .text('')
                .removeClass('alert alert-danger')
                .removeClass('ui-state-highlight');

        $('label.error').remove();
        $('input.error').removeClass('error');
        $('select.error').removeClass('error');
        $('textarea.error').removeClass('error');
    },
    handleHcvReset: function () {
        if (patient_edit.ajax_lock) {
            return;
        }

//      if ($('#' + this.prefix + '_id').val() + 0 > 0) {
        var data = 'id=' + $('#' + this.prefix + '_id').val() + '&';
        data += 'hcn_num=' + $('#' + this.prefix + '_hcn_num').val() + '&';
        data += 'vc=' + $('#' + this.prefix + '_hcn_version_code').val() + '&';

        $('input.duplicate_error').removeClass('error').removeClass('duplicate_error');
        $('label.duplicate_error').remove();
        $('#duplicate_trigger').remove();

        $.ajax({
            type: 'post',
            url: patient_edit.actions.hcv_reset,
            dataType: "json",
            data: data,
            success: function (rs) {
                $('#status_text').html('');
                if (rs.hasOwnProperty('duplicate_patient_id') && rs.duplicate_patient_id) {
                    patient_edit.duplicate.showDuplicateError(rs.duplicate_patient);
                }
            },
            beforeSend: function () {
                patient_edit.ajax_lock = true;
            },
            complete: function () {
                patient_edit.ajax_lock = false;
            }
        });
//      }
    },
    handleSaveRequest: function () {
        if (patient_edit.ajax_lock) {
            return;
        }
        patient_edit.clearMessages();

        var has_warnings = patient_edit.hasWarnings();

        if (has_warnings && !patient_edit.warning_made) {
            patient_edit.makeWarnings();
            return false;
        } else {
            patient_edit.clearWarnings();
            patient_edit.makeWarnings();
            $('#' + patient_edit.warning_modal).dialog('close');
        }

        var disabled = $('#' + patient_edit.form_id + ' :disabled');
        disabled.attr('disabled', false);

        var data = $('#' + patient_edit.form_id).serialize();
        disabled.attr('disabled', true);

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: patient_edit.actions.save,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('errors')) {
                    patient_edit.clearWarnings();
                    quickClaim.showFormErrors(rs.errors, '#' + patient_edit.message, '#' + patient_edit.prefix, true);
                    quickClaim.focusTopField();

                    $('#error_icon').show();
                    $('#messages_text').text('The form contains invalid data.');

                    if (rs.hasOwnProperty('show_override_appointment_check') && rs.show_override_appointment_check) {
                        $('.future_appointment_check').show();
                    }
                } else {
                    patient_edit.loadData(rs.data[0]);
                    patient_search.conformToLeave = "0";
                    var PatientOldForm = $('#patient_form').serialize();
                    $('#oldpatientdata').val(PatientOldForm);
                    $('#tabs').tabs('option', 'disabled', []);
                    $('#checkmark').show();
                    $('#messages_text').html('Saved <a href="' + patient_edit.actions.patient_edit + '?id=' + rs.data[0]['id'] + '"><b>' + rs.data[0]['fname'] + ' ' + rs.data[0]['lname'] + '</b>.</a>');

                    $('#edit_success_message_text').html('Saved <a href="' + patient_edit.actions.patient_edit + '?id=' + rs.data[0]['id'] + '"><b>' + rs.data[0]['fname'] + ' ' + rs.data[0]['lname'] + '</b>.</a>');
                    $('#edit_success_message').show();
                    $("#msgDivSk").html('Patient Saved Successfully.').addClass('alert alert-success sk-margin-top btn-squared');
                    $('#patient_notes').val('');
 
                    $.notify('Patient Saved Successfully.', {
                        element: 'body',
                        type: "success",
                        allow_dismiss: true,
                        newest_on_top: true,
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        delay: 10000,
                        z_index: 99999,
                        mouse_over: 'pause',
                        animate: {
                            enter: 'animated bounceIn',
                            exit: 'animated bounceOut'
                        },
                        template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                            <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                            <span data-notify="icon"></span>
                                            <span data-notify="title">{1}</span>
                                            <span data-notify="message">{2}</span>
                                            <div class="progress" data-notify="progressbar">
                                                    <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <a href="{3}" target="{4}" data-notify="url"></a>
                                    </div>`

                    });
                    var v = setTimeout("$('#edit_success_message').hide();", 5000);
                }
            },
            beforeSend: function () {
                patient_edit.ajax_lock = true;
                $('#messages').find('.icons div').hide();
                $('#spinner').show();

                $('#messages_text').text('Saving patient record.... ');
            },
            complete: function () {
                patient_edit.ajax_lock = false;
                $('#spinner').hide();
            },
            error: function () {
                $('#error_icon').show();
                $('#messages_text').text('An error occurred while contacting the server.  Please refresh the page and try again.');
            }
        });
    },
    HandleHCVResponse: function (rs,isvalidatepopup) {
        
        function AutofillForm() {
            if (person['firstName'] && person['lastName'] && person['gender']) {
                if (rs.hasOwnProperty('swipe_replace') && rs['swipe_replace']) {
                    $('#patient_fname').val(person['firstName']);
                }
                $('#patient_lname').val(person['lastName']);
                $('#patient_sex').val(person['gender'] == 'F' ? 'F' : 'M');
                //$('#patient_province').val("ON");
                $('#patient_moh_province').val("ON");
            }
        }

        function setDates() {
            if (person.dateOfBirth != '') {

                if (person.dateOfBirth != null) {
                    var dateOfBirth = new Date(person.dateOfBirth);
                    quickClaim.calculateAge(dateOfBirth, 'age');
                    $('#patient_dob').skDP('setDate', dateOfBirth);
                }

                if (person.expiryDate != null) {
                    var expiryDate = new Date(person.expiryDate);
                    $('#patient_hcn_exp_year').skDP('setDate', expiryDate);
                }
            }
        }

        function setHTMLBar() {
            var date = new Date();
            patient_edit.setHCVStatus('<span>' + date.toString(quickClaim.getParseDateFormat(quickClaim.dateFormat)) + ': ' + rs.response_message + '</span>');
            $('#' + patient_edit.hcv_status + ' span').addClass(rs.class_name);
            $('#' + patient_edit.hcv_status).removeClass("error-text").removeClass("success-text");
        }

        function handleFeeServiceDetails() {
            var new_content = '';
            var actual_content = '';
            if (rs.hasOwnProperty('fee_service_details')) {
                message += '<hr>';
                for (var index = 0; index < rs['fee_service_details'].length; index++) {
                    actual_content += '<tr id="fee_details"><td>&nbsp;</td><td class="' + rs.class_name + '">';
                    for (var a in rs['fee_service_details'][index]) {

                        
                        if (!rs['fee_service_details'][index][a])
                            continue;
                        if (a == 'feeServiceCode') {
                            
                            if(rs['fee_service_details'][index]['feeServiceDate'] != null)
                            {
                             rs['fee_service_details'][index][a] = '<b>'+rs['fee_service_details'][index][a].substr(0, 4)+'</b>';   
                            } else {
                             rs['fee_service_details'][index][a] = rs['fee_service_details'][index][a].substr(0, 4);
                            }
                        } else if (a == 'feeServiceDate') {
                            rs['fee_service_details'][index][a] = '<b>'+new Date(rs['fee_service_details'][index][a]).toDateString()+'</b>';
                        }

                        new_content += rs['fee_service_details'][index][a] + ' ';
                        message += rs['fee_service_details'][index][a] + ' ';
                    }
                    message += '<br>';
                    actual_content += new_content + '</td></tr>';
                    new_content = '';
                    $('#hcv_validation').after(actual_content);
                    actual_content = '';
                }
            }
        }


        //please

        function buildResponseMsg() {
            var message = rs.response_action + ' <br><b>' + rs.response_message + '</b>';
            var patient_dob = new Date(person['dateOfBirth'])
            var patient_expires;
            message = person['lastName'] + ', ' + person['firstName'] + '<br>';

            if (person['expiryDate'] != null) {
                patient_expires = new Date(person['expiryDate']);
                message += rs.response_code + ' : ' + rs.response_action + ' <br>DOB: ' + patient_dob.toDateString() + ' expires: ' + patient_expires.toDateString();
            } else {
                patient_expires = true;
                message += rs.response_code + ' : ' + rs.response_action + ' <br>DOB: ' + patient_dob.toDateString();
            }
            return message;
        }

        function alertUser() {
            if (rs.class_name == 'hcv_pass') {
                if(isvalidatepopup == 'false')
                {
                     $('#hcv_results').removeClass('hcvError');
                     $('#hcv_results').addClass('hcvSuccess');
                     $('#hcv_results').html('<span>'+message+'</span>');
                     return false;
                }

                $.notifyClose();
                $.notify(message, {
                    element: 'body',
                    type: "success",
                    allow_dismiss: true,
                    newest_on_top: true,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    delay: 10000,
                    z_index: 99999,
                    mouse_over: 'pause',
                    animate: {
                        enter: 'animated bounceIn',
                        exit: 'animated bounceOut'
                    },
                    template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                            <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                            <span data-notify="icon"></span>
                                            <span data-notify="title">{1}</span>
                                            <span data-notify="message">{2}</span>
                                            <div class="progress" data-notify="progressbar">
                                                    <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <a href="{3}" target="{4}" data-notify="url"></a>
                                    </div>`

                });

                $('.ajs-message').hover(function () {
                    msg.delay(0);
                });
            }
        }

        function validResponseCode(code) {
            switch (code) {
                case '50':
                case '51':
                case '52':
                case '53':
                case '54':
                case '55':
                    return true;
                case '05':
                case '10':
                case '15':
                case '20':
                case '25':
                case '60':
                case '65':
                case '70':
                case '75':
                case '80':
                case '83':
                case '90':
                case '99':
                case '9A':
                case '9B':
                case '9C':
                case '9D':
                case '9E':
                case '9F':
                case '9G':
                case '9H':
                case '9I':
                case '9J':
                case '9K':
                case '9L':
                case '9M':
                    return false;
                default:
                    return false;
            }
        }

        if (rs.hasOwnProperty('results') && rs['results'][0]) {
            var person = rs['results'][0];

            if (person && validResponseCode(person.responseCode)) {
                var dataURL = 'patientid=' + $('#' + patient_edit.prefix + '_id').val();
                dataURL += '&status=' + person.responseCode;

                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: patient_edit.actions.update_hcv_status,
                    data: dataURL,
                    success: function (rs) {


                    }
                });

                $('#messages_text').text('Received message.').show();

                AutofillForm();

                setDates();

                if (rs.hasOwnProperty('response_message')) {
                    setHTMLBar();

                    $('#hcv_results').removeClass('hcvError');
                    $('#hcv_results').addClass('hcvSuccess');
                    var date = new Date();
                    $('#hcv_results').html('<span>' + date.toString(quickClaim.getParseDateFormat(global_js.getNewDateFormat())) + ': ' + rs.response_message + '</span>');
                    var message = buildResponseMsg();
                    handleFeeServiceDetails();
                    alertUser();
                }
            } else {
                var dataURL = 'patientid=' + $('#' + patient_edit.prefix + '_id').val();
                dataURL += '&status=' + person.responseCode;

                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: patient_edit.actions.update_hcv_status,
                    data: dataURL,
                    success: function (rs) {
                    }
                });

                setHTMLBar();
                AutofillForm();
                setDates();
                var message = rs.response_action + ' <br><b>' + rs.response_message + '</b>';
                $('#hcv_results').removeClass('hcvSuccess');
                $('#hcv_results').addClass('hcvError');
                var date = new Date();
                $('#hcv_results').html('<span>' + date.toString(quickClaim.getParseDateFormat(global_js.getNewDateFormat())) + ': ' + rs.response_message + '</span>');
                if(isvalidatepopup != 'false')
                {
                    $.notifyClose();
                    $.notify(message, {
                        element: 'body',
                        type: "danger",
                        allow_dismiss: true,
                        newest_on_top: true,
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        delay: 10000,
                        z_index: 99999,
                        mouse_over: 'pause',
                        animate: {
                            enter: 'animated bounceIn',
                            exit: 'animated bounceOut'
                        },
                        template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                                <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                                <span data-notify="icon"></span>
                                                <span data-notify="title">{1}</span>
                                                <span data-notify="message">{2}</span>
                                                <div class="progress" data-notify="progressbar">
                                                        <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                                </div>
                                                <a href="{3}" target="{4}" data-notify="url"></a>
                                        </div>`

                    });
                }
            }
        }
    },
    handleHcvPress: function (isvalidatepopup = true) {

        if ($('#' + patient_edit.prefix + '_hcn_num').val().length > 9) {
            var data = 'hcn=' + $('#' + patient_edit.prefix + '_hcn_num').val();
            data += '&vc=' + $('#' + patient_edit.prefix + '_hcn_version_code').val();
            data += '&id=' + $('#' + patient_edit.prefix + '_id').val();

            $('[id=fee_details]').remove();

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: patient_edit.actions.hcv,
                data: data,
                timeout: 15000,
                success: function (rs) {
                    //Check 500 errors
                    if (rs == null) {
                        patient_edit.handleError500Response(rs,isvalidatepopup);
                        return false;
                    }
                    if (rs.hasOwnProperty('code') && rs.code == 500) {
                        patient_edit.handleError500Response(rs,isvalidatepopup);
                    } else {
                        if (rs.hasOwnProperty('mod_ten') && rs['mod_ten']) {
                            patient_edit.handleModTenResponse(rs,isvalidatepopup);
                        } else {
                            patient_edit.HandleHCVResponse(rs,isvalidatepopup);
                        }
                    }
                },
                beforeSend: function () {
                    patient_edit.ajax_lock = true;
                    $('#messages .icons div').hide();
                    $('#spinner').show();

                    $('#messages_text').text('Requesting validation...');
                },
                complete: function () {
                    patient_edit.ajax_lock = false;
                    $('#spinner').hide();
                    $('#checkmark').show();
                    $('#error_icon').hide();
                    $('#messages_text').text('Received HCV response');
                    $("#patient_form select").each(function () {
                        $(this).trigger('change.select2');
                    })
                },
                error: function () {
                    $('#error_icon').show();
                    $('#messages_text').text('An error occurred while contacting the server.  Please refresh the page and try again.');
                }
            });
        } else {
            if(isvalidatepopup == 'false')
            {
                 $('#hcv_results').removeClass('hcvSuccess');
                 $('#hcv_results').addClass('hcvError');
                 $('#hcv_results').html('<span>Invalid number of characters for healthcard</span>');
            }
            else
            {
                sk.skNotify('Invalid number of characters for healthcard', 'danger');
            }
        }
    },
    handleError500Response: function (rs,isvalidatepopup) {
        if(isvalidatepopup == 'false')
        {
             $('#hcv_results').removeClass('hcvSuccess');
             $('#hcv_results').addClass('hcvError');
             $('#hcv_results').html('<span>HCV is down at the moment. Service should be restored shortly</span>');
             return false;
        }
        $.notify('HCV is down at the moment. Service should be restored shortly.', {
            element: 'body',
            type: "warning",
            allow_dismiss: true,
            newest_on_top: true,
            placement: {
                from: "top",
                align: "right"
            },
            delay: 10000,
            z_index: 99999,
            mouse_over: 'pause',
            animate: {
                enter: 'animated bounceIn',
                exit: 'animated bounceOut'
            },
            template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                            <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                            <span data-notify="icon"></span>
                                            <span data-notify="title">{1}</span>
                                            <span data-notify="message">{2}</span>
                                            <div class="progress" data-notify="progressbar">
                                                    <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <a href="{3}" target="{4}" data-notify="url"></a>
                                    </div>`

        });

    },
    handleModTenResponse: function (rs,isvalidatepopup) {

        
        var date = new Date();

        if (rs.hasOwnProperty('response_message')) {
            $('#status_text').removeClass('hcv_mod10_pass').removeClass('hcv_fail').removeClass('hcv_success')
                    .text(date.toString(quickClaim.getParseDateFormat()) + ': ' + rs['response_message'])
                    .addClass(rs.class_name);
        }
        $('#messages_text').text('Received message.').show();
        $('#health_card_check_row').show();

        if(isvalidatepopup == 'false')
        {
             $('#hcv_results').removeClass('hcvSuccess');
             $('#hcv_results').addClass('hcvError');
             $('#hcv_results').html('<span>'+rs['response_message']+'</span>');
             return false;
        }

        $.notify(rs['response_message'], {
            element: 'body',
            type: "warning",
            allow_dismiss: true,
            newest_on_top: true,
            placement: {
                from: "top",
                align: "right"
            },
            delay: 10000,
            z_index: 99999,
            mouse_over: 'pause',
            animate: {
                enter: 'animated bounceIn',
                exit: 'animated bounceOut'
            },
            template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                            <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                            <span data-notify="icon"></span>
                                            <span data-notify="title">{1}</span>
                                            <span data-notify="message">{2}</span>
                                            <div class="progress" data-notify="progressbar">
                                                    <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <a href="{3}" target="{4}" data-notify="url"></a>
                                    </div>`

        });

    },
    loadData: function (data, is_new) {
        if (data.full_name && !lscache.get('prevpatient_' + data.id)) {
            lscache.set('prevpatient_' + data.id, data, 60);
            $('.pastPatients').html('<span id="' + data.id + '" class="pp patient_widget"></span>');

            switch (parseInt(data.id.substr(data.id.length - 1))) {
                case 0:
                    $('#' + data.id).addClass('red');
                    break;

                case 1:
                    $('#' + data.id).addClass('yellow');
                    break;

                case 2:
                    $('#' + data.id).addClass('blue');
                    break;

                case 3:
                    $('#' + data.id).addClass('green');
                    break;

                case 4:
                    $('#' + data.id).addClass('purple');
                    break;

                case 5:
                    $('#' + data.id).addClass('red');
                    break;

                case 6:
                    $('#' + data.id).addClass('yellow');
                    break;

                case 7:
                    $('#' + data.id).addClass('blue');
                    break;

                case 8:
                    $('#' + data.id).addClass('green');
                    break;

                case 9:
                    $('#' + data.id).addClass('purple');
                    break;

                default:
                    $('#' + data.id).addClass('red');
                    break;
            }

            $('#' + data.id).click(function () {
                patient_edit.loadData(lscache.get('prevpatient_' + this.id), false);
            });

            $('#' + data.id).html(data.full_name);

            var d = new Date();
            var newDate = d.toString('yyyy-MM-dd');

            if(data.hcv_last_checked == newDate)
            {
                patient_edit.handleHcvPress('false');
            } else {
                patient_edit.handleHcvPress();
            }
        }

        if (is_new || (patient_edit.require_search && data.hasOwnProperty('id'))) {
            $('#tabs').tabs('option', 'disabled', []);
        }

        patient_edit.clearMessages();
        patient_edit.clearWarnings();
        patient_edit.warning_made = false;

        $('#patient_follow_up').prop('checked', false);
        $('#patient_follow_up_date').val('');

       

        quickClaim.formFill(data, '#' + patient_edit.prefix + '_', null, '#' + patient_edit.message);
//        if(data.hcn_num){
        $("#oldHCNCheck").val(data.hcn_num);
//        }
        if (data.hasOwnProperty('dob')) {
 //            $('#' + patient_edit.prefix + '_hcn_exp_year').skDP('setStartDate', moment(data.dob).toDate());
        }
        $('#create_edit_title').text(data['id'] ? patient_edit.tab_edit : patient_edit.tab_default);

        patient_edit.setOriginalLocId(data.location_id);

        if (!data.hasOwnProperty('id')) {
            if (data.location_id) {
                $('#patient_location_id').val(data.location_id);
            } else {
                $('#patient_location_id').val(this.currentLocation);
            }
        }

        if (data.hasOwnProperty('full_name')) {
            $('#patient_name').text(data.full_name);
        }

        patient_edit.setHCVStatus(data.status);
        patient_edit.clearVersions();
        patient_edit.versionsLoaded = false;

        if (data.id) {
            patient_history.handleServiceHistory(data.service);
            patient_history.handleNotesHistory(data.note_history);
            patient_history.handleAppointmentHistory(data.appointment_history);


        } else {
            $('#patient_province').val(patient_edit.default_province);
            $('#patient_moh_province').val(patient_edit.default_province);
        }

        if (data.patient_status_id) {
            patient_edit.original_status_id = data.patient_status_id;
        } else {
            $('#patient_patient_status_id').val(patient_edit.default_patient_status);
        }

        if (!data.patient_type_id) {
            $('#patient_patient_type_id').val(patient_edit.defaultPatientType);
        }

        $('.patient_status_row').hide();
        $('.future_appointment_check').hide();

        patient_edit.togglePatientTypeMessages();



        return true;
    },
    setOriginalLocId: function (v) {
        if (!v) {
            v = '';
        }
        patient_edit.original_loc_id = v;
    },
    setHCVStatus: function (status) {
        if (status) {
            $('#' + patient_edit.hcv_status).html(status);
        } else {
            status = $('#' + patient_edit.hcv_status).text();
        }

        $('#' + patient_edit.hcv_status).removeClass("error-text");
        $('#' + patient_edit.hcv_status).removeClass("success-text");
        $('#' + patient_edit.hcv_status + ' span').removeClass("error-text");
        $('#' + patient_edit.hcv_status + ' span').removeClass("success-text");

        if (status) {
            if (status.substr(0, 7) == 'Invalid') {
                $('#' + patient_edit.hcv_status).addClass("error-text");
            } else {
                $('#' + patient_edit.hcv_status).addClass("success-text");
            }
        }
    },
    handleWarningButtonYes: function () {
        patient_edit.handleSaveRequest();
    },
    handleWarningButtonNo: function () {
        $('#' + patient_edit.warning_modal).dialog('close');
        patient_edit.warning_made = false;
    },
    setupLocationWatcher: function () {
        $('#' + patient_edit.prefix + '_location_id').change(function () {
            if (patient_edit.locations_provinces[$(this).val()]) {
                if($('#patient_province').val() == 0)
                {
                $('#' + patient_edit.prefix + '_province').val(patient_edit.locations_provinces[$(this).val()]);
                }
            }
        });
    },
    setupRefDocAutocomplete: function (prefix) {
        if ($('#' + patient_edit.prefix + '_' + prefix + '_description').size()) {
            $('#' + patient_edit.prefix + '_' + prefix + '_description').autocomplete({
                minLength: 2,
                select: function (event, ui) {
                    if (ui.item.label == 'Create A New Referring Doctor') {
                        patient_edit.activeRefDocPrefix = prefix;

                        var value = ui.item.provider_num;
                        var alphasRE = new RegExp(/[a-z\-]/i);
                        var tokens = value.split(' ');

                        ui.item.provider_num = '';

                        for (var a in tokens) {
                            if (tokens[a].match(alphasRE)) {
                                if (!ui.item.hasOwnProperty('lname')) {
                                    ui.item.lname = tokens[a];
                                } else if (!ui.item.hasOwnProperty('fname')) {
                                    ui.item.fname = tokens[a];
                                } else {
                                    ui.item.fname += ' ' + tokens[a];
                                }
                            } else {
                                ui.item.provider_num += tokens[a];
                            }
                        }

                        referringDoctorForm.edit(ui.item);
                    } else {
                        $('#' + patient_edit.prefix + '_' + prefix + '_id').val(ui.item.id);
                        $('#' + patient_edit.prefix + '_' + prefix + '_description').val(ui.item.label);
                        $('#' + patient_edit.prefix + '_' + prefix + '_num').val(ui.item.provider_num);
                    }
                },
                source: function (request, response) {
                    if (request.term in refdoc_cache) {
                        response(refdoc_cache[request.term]);
                    }

                    $.ajax({
                        url: patient_edit.actions.autocomplete_refdoc,
                        dataType: "json",
                        data: request,
                        success: function (data) {
                            refdoc_cache[request.term] = data;
                            response(data);
                        }
                    });
                }
            });
        }
    },
    fillInRefDoc: function (data) {
        $('#' + patient_edit.prefix + '_' + patient_edit.activeRefDocPrefix + '_id').val(data.id);
        $('#' + patient_edit.prefix + '_' + patient_edit.activeRefDocPrefix + '_description').val(data.autocomplete);
        $('#' + patient_edit.prefix + '_' + patient_edit.activeRefDocPrefix + '_num').val(data.provider_num);
        $('#referral_form_ref_doc').val(data.id);
    },
    setCurrentLoc: function (l) {
        patient_edit.currentLocation = l;
    },
    setupReasonCodes: function () {
        if (($('.status_row .select2-chosen').text() == 'INACTIVE') || (($('.status_row .select2-chosen').text() == 'PIMENTEL'))) {
            $('.' + 'patient_status_row').remove();

            var s = '<label class="col-sm-3 control-label paddingwithright fs12 labelTop reasonblock patient_status_row">Reason</label>'
                    + '<div class="col-lg-9 col-sm-9 col-xs-12 noPadding reasonblock patient_status_row"><select name="' + this.prefix + '[reason_code_id]" id="' + this.prefix + '_reason_code_id" class="form-control">';
            for (var a in patient_edit.reason_codes) {
                if ($('#' + this.prefix + '_patient_status_id').val() == patient_edit.reason_codes[a].status_id) {
                    s += '<option value="' + patient_edit.reason_codes[a].id + '">' + patient_edit.reason_codes[a].name + '</option>';
                }
            }
            s += '</select></div>';
            $('#' + 'status_row').append($(s));
            $('#status_row').show();
        } else {
            $('#status_row').hide();
        }
    },
    setupPrintLinks: function () {

        //DYMO BABY

        $('#dymo_label_address_button').button();
        $('#dymo_label_address_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            $('#dymo_label_address_button').prop('disabled', true);
            patient_edit.printNumberofCopy('address');
            //patient_edit.PrintLabel('address');

            $('#dymo_label_address_button').prop('disabled', false);
        });

        $('#dymo_label_address_center_button').button();
        $('#dymo_label_address_center_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            $('#dymo_label_address_center_button').prop('disabled', true);

            patient_edit.printNumberofCopy('address_center');
            //patient_edit.PrintLabel('address_center');

            $('#dymo_label_address_center_button').prop('disabled', false);
        });

        $('#dymo_label_dob_button').button();
        $('#dymo_label_dob_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }
            $('#dymo_label_dob_button').prop('disabled', true);

            patient_edit.printNumberofCopy('dob');
            //patient_edit.PrintLabel('dob');

            $('#dymo_label_dob_button').prop('disabled', false);
        });

        $('#dymo_label_name_button').button();
        $('#dymo_label_name_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            $('#dymo_label_name_button').prop('disabled', true);

            patient_edit.printNumberofCopy('name');
            //patient_edit.PrintLabel('name');


            $('#dymo_label_name_button').prop('disabled', false);
        });

       

        $('#dymo_label_chart_button').button();
        $('#dymo_label_chart_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            $('#dymo_label_chart_button').prop('disabled', true);

            patient_edit.printNumberofCopy('chart');
            //patient_edit.PrintLabel('chart');


            $('#dymo_label_chart_button').prop('disabled', false);
        });

        $('#dymo_label_chart_refdoc_button').button();
        $('#dymo_label_chart_refdoc_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            $('#dymo_label_chart_refdoc_button').prop('disabled', true);


            patient_edit.printNumberofCopy('chartwrefdoc');
            //patient_edit.PrintLabel('chartwrefdoc');


            $('#dymo_label_chart_refdoc_button').prop('disabled', false);
        });

        $('#dymo_label_ref_dr_button').button();
        $('#dymo_label_ref_dr_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            $('#dymo_label_ref_dr_button').prop('disabled', true);

            patient_edit.printNumberofCopy('refdr');
            //patient_edit.PrintLabel('refdr');


            $('#dymo_label_ref_dr_button').prop('disabled', false);
        });

        $('#dymo_label_ref_dr_button_2').button();
        $('#dymo_label_ref_dr_button_2').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            if (!$('#referral_form_ref_doc').val()) {
                $('#referring_doctor_dialog').dialog('open');
                return false;
            }

            $('#dymo_label_ref_dr_button').prop('disabled', true);

            var dataURL = 'ref_doc=' + $('#referral_form_ref_doc').val() + '&type=ref_addr';

            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: patient_edit.actions.labels2,
                data: dataURL,
                success: function (rs) {
                    if (rs.hasOwnProperty('error')) {
                        alert(rs['error']);
                        return;
                    }

                    labelXML = rs;

                    if (labelXML) {
                        var label = dymo.label.framework.openLabelXml(labelXML['xml']);
                        var label_objects = label.getObjectNames();
                        var refdoc = labelXML['refdoc'];

                        for (obj in label_objects) {
                            if (patient_edit.contains(label.getObjectNames(), label_objects[obj])) {
                                var ele = label_objects[obj];
                                var new_text = refdoc[ele];
                                if (new_text) {
                                    label.setObjectText(label_objects[obj], new_text);
                                }
                            }
                        }

                        // slect printer to print on
                        // for simplicity sake just use the first LabelWriter printer
                        var printers = dymo.label.framework.getPrinters();
                        if (printers.length == 0)
                            throw "No DYMO printers are installed. Install DYMO printers.";

                        var printerName = "";
                        for (var i = 0; i < printers.length; ++i) {
                            var printer = printers[i];
                            if (printer.printerType == "LabelWriterPrinter") {
                                printerName = printer.name;
                                break;
                            }
                        }
                        if (printerName == "")
                            throw "No LabelWriter printers found. Install LabelWriter printer";

                        // finally print the label
                        label.print(printerName);
                    }
                },
                error: function (rs) {
                    alert('An error has occured: ' + rs);
                }
            });


            $('#dymo_label_ref_dr_button').prop('disabled', false);
            $('#referral_form_ref_doc').val('');
        });

        $('#dymo_label_famdoc_addr').button();
        $('#dymo_label_famdoc_addr').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            $('#dymo_label_famdoc_addr').prop('disabled', true);


            //patient_edit.PrintLabel('famaddr');
            patient_edit.printNumberofCopy('famaddr');


            $('#dymo_label_famdoc_addr').prop('disabled', false);
        });

        $('#dymo_label_refdoc_addr').button();
        $('#dymo_label_refdoc_addr').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            $('#dymo_label_refdoc_addr').prop('disabled', true);

            patient_edit.printNumberofCopy('refaddr');
            //patient_edit.PrintLabel('refaddr');

            $('#dymo_label_refdoc_addr').prop('disabled', false);
        });


         $('#dymo_label_name_mirrored_button').button();
        $('#dymo_label_name_mirrored_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            $('#dymo_label_name_mirrored_button').prop('disabled', true);

            patient_edit.printNumberofCopy('namemirror');
            //patient_edit.PrintLabel('namemirror');


            $('#dymo_label_name_mirrored_button').prop('disabled', false);
        });


        $('#dymo_label_mirrored_2').button();
        $('#dymo_label_mirrored_2').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }
 
            $('#dymo_label_mirrored_2').prop('disabled', true);

            patient_edit.printNumberofCopy('mirror2');
            //patient_edit.PrintLabel('mirror2');


            $('#dymo_label_mirrored_2').prop('disabled', false);
        });



        //label_custom_template_button
        $('#label_address_button').button();
        $('#label_address_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            var url = patient_edit.actions.address_label + "?type=address&patient_id=" + $('#' + patient_edit.prefix + '_id').val();
            patient_edit.setupIframe(url);
        });

        // $('#label_custom_template_button').button();
        // $('#label_custom_template_button').click(function (event) {
        //     patient_edit.healthcard = $('#' + patient_edit.prefix + '_hcn_num').val();


        //     if (!$('#patient_doctor_id').val()) {
        //         $('#patient_doctor_id').css('border-left', '1px solid red');
        //         return;
        //     }
        //     var doctorId = $('#patient_doctor_id').val();

        //     var radio = $('#template_radio').val();

        //     if (!radio) {
        //         $.ajax({
        //             type: 'post',
        //             url: patient_edit.actions.template,
        //             dataType: 'json',
        //             success: function (rs) {
        //                 patient_edit.clientID = rs['clientID'];
        //                 rs['templates'].forEach(addRadio);

        //                 function addRadio(element, index, array) {
        //                     $('#label_custom_template_button').after('<p></p><input type="radio" id="template_radio" name="template" value="' + element + '">' + element);
        //                 }
        //             }
        //         });
        //     } else if (patient_edit.clientID[0]) {

        //         var dataVar = 'patient_id=' + $('#' + patient_edit.prefix + '_id').val() + '&template=' + $('#template_radio:checked').val() + '&healthcard=' + patient_edit.healthcard + '&doctor_id=' + doctorId;

        //         $.ajax({
        //             type: 'post',
        //             url: patient_edit.actions.replace,
        //             data: dataVar,
        //             success: function (rs) {
        //                 var url = 'https://' + rs;
        //                 window.open(url, '_blank');
        //             }
        //         });
        //     }
        // });


        $('#label_dob_button').button();
        $('#label_dob_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            var url = patient_edit.actions.address_label + "?type=dob&patient_id=" + $('#' + patient_edit.prefix + '_id').val();
            patient_edit.setupIframe(url);
        });

        $('#label_direct_address_button').button();
        $('#label_direct_address_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }
            if (!$('#direct_print_select').val()) {
                alert("no printer selected");
                return false;
            }

            var data = "direct=true&type=address&patient_id=" + $('#' + patient_edit.prefix + '_id').val() + '&printer=' + $('#direct_print_select').val();
            $.ajax({
                type: 'post',
                url: patient_edit.actions.address_label,
                dataType: "json",
                data: data,
                success: function (rs) {
                    $('#success_icon').show();
                    $('#messages_text').text('Created Print Request');
                }
            });
        });

        $('#label_direct_dob_button').button();
        $('#label_direct_dob_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }
            if (!$('#direct_print_select').val()) {
                alert("no printer selected");
                return false;
            }

            var data = "direct=true&type=dob&patient_id=" + $('#' + patient_edit.prefix + '_id').val() + '&printer=' + $('#direct_print_select').val();
            $.ajax({
                type: 'post',
                url: patient_edit.actions.address_label,
                dataType: "json",
                data: data,
                success: function (rs) {
                    $('#success_icon').show();
                    $('#messages_text').text('Created Print Request');
                },
                beforeSend: function () {
                    patient_edit.ajax_lock = true;
                    $('#messages .icons div').hide();
                    $('#spinner').show();

                    $('#messages_text').text('Direct Printing DOB Label ');
                },
                complete: function () {
                    patient_edit.ajax_lock = false;
                    $('#spinner').hide();
                },
                error: function () {
                    $('#error_icon').show();
                    $('#messages_text').text('An error occurred while contacting the server.  Please refresh the page and try again.');
                }
            });
        });

        $('#label_name_button').button();
        $('#label_name_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            var url = patient_edit.actions.address_label + "?type=name&patient_id=" + $('#' + patient_edit.prefix + '_id').val();
            patient_edit.setupIframe(url);
        });

        $('#label_direct_name_button').button();
        $('#label_direct_name_button').click(function () {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }
            if (!$('#direct_print_select').val()) {
                alert("no printer selected");
                return false;
            }

            var data = "direct=true&type=name&patient_id=" + $('#' + patient_edit.prefix + '_id').val() + '&printer=' + $('#direct_print_select').val();
            $.ajax({
                type: 'post',
                url: patient_edit.actions.address_label,
                dataType: "json",
                data: data,
                success: function (rs) {
                    $('#success_icon').show();
                    $('#messages_text').text('Created Print Request');
                },
                beforeSend: function () {
                    patient_edit.ajax_lock = true;
                    $('#messages .icons div').hide();
                    $('#spinner').show();

                    $('#messages_text').text('Direct Printing Name Label ');
                },
                complete: function () {
                    patient_edit.ajax_lock = false;
                    $('#spinner').hide();
                },
                error: function () {
                    $('#error_icon').show();
                    $('#messages_text').text('An error occurred while contacting the server.  Please refresh the page and try again.');
                }
            });
        });

        $('#label_chart_button').button();
        $('#label_chart_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            var url = patient_edit.actions.address_label + "?type=chart&patient_id=" + $('#' + patient_edit.prefix + '_id').val();
            patient_edit.setupIframe(url);
        });

        $('#label_chart_refdoc_button')
                .button()
                .click(function (event) {
                    if (!$('#' + patient_edit.prefix + '_id').val()) {
                        alert('no active patient.');
                        return false;
                    }

                    var url = patient_edit.actions.address_label + "?type=chart_refdoc&patient_id=" + $('#' + patient_edit.prefix + '_id').val();
                    patient_edit.setupIframe(url);
                });


        $('#label_direct_chart_button').button();
        $('#label_direct_chart_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }
            if (!$('#direct_print_select').val()) {
                alert("no printer selected");
                return false;
            }

            var data = "direct=true&type=chart&patient_id=" + $('#' + patient_edit.prefix + '_id').val() + '&printer=' + $('#direct_print_select').val();

            $.ajax({
                type: 'post',
                url: patient_edit.actions.address_label,
                dataType: "json",
                data: data,
                success: function (rs) {
                    $('#success_icon').show();
                    $('#messages_text').text('Created Print Request');
                },
                beforeSend: function () {
                    patient_edit.ajax_lock = true;
                    $('#messages .icons div').hide();
                    $('#spinner').show();

                    $('#messages_text').text('Direct Printing Chart Label ');
                },
                complete: function () {
                    patient_edit.ajax_lock = false;
                    $('#spinner').hide();
                },
                error: function () {
                    $('#error_icon').show();
                    $('#messages_text').text('An error occurred while contacting the server.  Please refresh the page and try again.');
                }
            });
        });

        $('#label_direct_chart_refdoc_button').button();
        $('#label_direct_chart_refdoc_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }
            if (!$('#direct_print_select').val()) {
                alert("no printer selected");
                return false;
            }

            var data = "direct=true&type=chart_refdoc&patient_id=" + $('#' + patient_edit.prefix + '_id').val() + '&printer=' + $('#direct_print_select').val();
            $.ajax({
                type: 'post',
                url: patient_edit.actions.address_label,
                dataType: "json",
                data: data,
                success: function (rs) {
                    $('#success_icon').show();
                    $('#messages_text').text('Created Print Request');
                },
                beforeSend: function () {
                    patient_edit.ajax_lock = true;
                    $('#messages .icons div').hide();
                    $('#spinner').show();

                    $('#messages_text').text('Direct Printing Chart Label w RefDoc');
                },
                complete: function () {
                    patient_edit.ajax_lock = false;
                    $('#spinner').hide();
                },
                error: function () {
                    $('#error_icon').show();
                    $('#messages_text').text('An error occurred while contacting the server.  Please refresh the page and try again.');
                }
            });
        });

        $('#label_ref_dr_button').button();
        $('#label_ref_dr_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }
            if (!$('#' + patient_edit.prefix + '_ref_doc_id').val()) {
                alert('no referring doctor record.');
                return false;
            }

            var url = patient_edit.actions.refdoc_label + "?addr_id=" + $('#' + patient_edit.prefix + '_ref_doc_id').val();
            patient_edit.setupIframe(url);
        });

        $('#label_direct_ref_dr_button').button();
        $('#label_direct_ref_dr_button').click(function () {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }
            if (!$('#' + patient_edit.prefix + '_ref_doc_id').val()) {
                alert('no referring doctor record.');
                return false;
            }
            if (!$('#direct_print_select').val()) {
                alert("no printer selected");
                return false;
            }

            var data = "direct=true&addr_id=" + $('#' + patient_edit.prefix + '_ref_doc_id').val() + '&printer=' + $('#direct_print_select').val();
            $.ajax({
                type: 'post',
                url: patient_edit.actions.refdoc_label,
                dataType: "json",
                data: data,
                success: function (rs) {
                    $('#success_icon').show();
                    $('#messages_text').text('Created Print Request');
                },
                beforeSend: function () {
                    patient_edit.ajax_lock = true;
                    $('#messages .icons div').hide();
                    $('#spinner').show();

                    $('#messages_text').text('Direct Printing Referring Doctor Label ');
                },
                complete: function () {
                    patient_edit.ajax_lock = false;
                    $('#spinner').hide();
                },
                error: function () {
                    $('#error_icon').show();
                    $('#messages_text').text('An error occurred while contacting the server.  Please refresh the page and try again.');
                }
            });
        });

        $('#general_info_button').button();
        $('#general_info_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            var url = patient_edit.actions.pdf + "?type=genInfo&id=" + $('#' + patient_edit.prefix + '_id').val();
            patient_edit.setupIframe(url);
        });

        $('#claim_history_button').button();
        $('#claim_history_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            var url = patient_edit.actions.pdf + "?type=claim_history&id=" + $('#' + patient_edit.prefix + '_id').val();
            patient_edit.setupIframe(url);
        });

        $('#appointment_history_button').button();
        $('#appointment_history_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            var url = patient_edit.actions.pdf + "?type=appointment_history&id=" + $('#' + patient_edit.prefix + '_id').val();
            patient_edit.setupIframe(url);
        });


        $('#referral_form_button').button();
        $('#referral_form_button').click(function (event) {
            if (!$('#' + patient_edit.prefix + '_id').val()) {
                alert('no active patient.');
                return false;
            }

            if (!$('#referral_form_ref_doc').val()) {
                $('#referring_doctor_dialog').dialog('open');
                return false;
            }

            var url = patient_edit.actions.pdf + "?type=referral_form&id=" + $('#' + patient_edit.prefix + '_id').val() + "&ref_id=" + $('#referral_form_ref_doc').val() + "&doc_id=" + $('#patient_doctor_id').val();
            patient_edit.setupIframe(url);
            $('#referral_form_ref_doc').val('');
        });

    },
    PrintLabel: function (s,noCopy = null) {
        var patient_id = $('#' + patient_edit.prefix + '_id').val();
        var dataURL = 'patient_id=' + patient_id;

        $.ajax({
            type: 'GET',
            data: dataURL,
            url: patient_edit.actions.patient_info,
            success: function (rs) {

                if (rs.hasOwnProperty('error')) {
                    alert(rs['error']);
                    return;
                }

                var patientObj = JSON.parse(rs);

                var patientid = patientObj.id;
                var patientaddr = patientObj.address;
                var patienthomephone = patientObj.hphone;
                var patientcellphone = patientObj.cellphone;
                var patientdob = patientObj.dob;
                var patientgender = patientObj.sex;
                var patientname = patientObj.lname;
                var patientfname = patientObj.fname;

                var patientlfname = patientname + ', ' + patientfname;

                var patientchart = patientObj.patient_number;
                var patientcity = patientObj.city;
                var patientpostalcode = patientObj.postal_code;
                var patientprovince = patientObj.province;
                var patienthcn = patientObj.health_num;
                var patientversion = patientObj.hcn_version_code;

                var patientophone = patientObj.ophone;
                var patientwphone = patientObj.wphone;

                var patienthcvstatus = patientObj.hcn_last_status;

                var patientrefdocprovider = '';
                var patientrefdocname = '';
                var patientrefdoccity = '';
                var patientrefdocaddr = '';
                var patientrefdocpost = '';

                var patientfamdocprovider = '';
                var patientfamdocname = '';
                var patientfamdoccity = '';
                var patientfamdocaddr = '';
                var patientfamdocpost = '';


                var famdocarr = $('#patient_fam_doc_description').val().split('[');

                if (famdocarr.length == 1) {
                    var famdocarr2 = famdocarr[0].split('-');

                    patientfamdocprovider = famdocarr2[0];
                    patientfamdocname = famdocarr2[1];
                    patientfamdoccity = famdocarr2[2];
                    patientfamdocpost = famdocarr2[3];
                }

                if (famdocarr.length == 2) {
                    //first half is prov name city postal
                    //second half is addr

                    var famdocarr2 = famdocarr[0].split('-');

                    patientfamdocprovider = famdocarr2[0];
                    patientfamdocname = famdocarr2[1];
                    patientfamdoccity = famdocarr2[2];
                    patientfamdocpost = famdocarr2[3];

                    patientfamdocaddr = famdocarr[1].replace(']', '');
                }


                var refdocarr = $('#patient_ref_doc_description').val().split('[');

                if (refdocarr.length == 1) {
                    var refdocarr2 = refdocarr[0].split('-');

                    patientrefdocprovider = refdocarr2[0];
                    patientrefdocname = refdocarr2[1];
                    patientrefdoccity = refdocarr2[2];
                    patientrefdocpost = refdocarr2[3];
                }

                if (refdocarr.length == 2) {
                    //first half is prov name city postal
                    //second half is addr

                    var refdocarr2 = refdocarr[0].split('-');

                    patientrefdocprovider = refdocarr2[0];
                    patientrefdocname = refdocarr2[1];
                    patientrefdoccity = refdocarr2[2];
                    patientrefdocpost = refdocarr2[3];

                    patientrefdocaddr = refdocarr[1].replace(']', '');
                }

                if (patienthomephone) {
                    patienthomephone = patienthomephone.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
                }

                if (patientcellphone) {
                    patientcellphone = patientcellphone.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
                }

                if (patientophone) {
                    patientophone = patientophone.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
                }

                if (patientwphone) {
                    patientwphone = patientwphone.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
                }


                var patient = {
                    id: patientid,
                    name: patientname,
                    fname: patientfname,
                    addr: patientaddr,
                    hphone: patienthomephone,
                    cphone: patientcellphone,
                    ophone: patientophone,
                    wphone: patientwphone,
                    dob: patientdob,
                    gender: patientgender,
                    chart: patientchart,
                    city: patientcity,
                    post: patientpostalcode,
                    prov: patientprovince,
                    hcn: patienthcn,
                    vers: patientversion,
                    status: patienthcvstatus,
                    lfname: patientlfname,
                    refdocprov: patientrefdocprovider,
                    refdocname: patientrefdocname,
                    refdoccity: patientrefdoccity,
                    refdocaddr: patientrefdocaddr,
                    refdocpost: patientrefdocpost,
                    famdocprov: patientfamdocprovider,
                    famdocname: patientfamdocname,
                    famdoccity: patientfamdoccity,
                    famdocaddr: patientfamdocaddr,
                    famdocpost: patientfamdocpost,
                    refdoc: patientrefdocname + ' ' + patientrefdocprovider,
                    famdoc: patientfamdocname + ' ' + patientfamdocprovider
                };

                if (patient.refdoc.indexOf('undefined') == 0) {
                    patient.refdoc = '';
                }
                if (patient.famdoc.indexOf('undefined') == 0) {
                    patient.famdoc = '';
                }

                var labelXML;
                var dataURL = 'type=' + s;

                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: patient_edit.actions.labels,
                    data: dataURL,
                    success: function (rs) {
                        if (rs.hasOwnProperty('error')) {
                            alert(rs['error']);
                            return;
                        }

                        labelXML = rs;

                        if (labelXML) {
                            var label = dymo.label.framework.openLabelXml(labelXML);

                            if (patient) {

                                var label_objects = label.getObjectNames();

                                for (obj in label_objects) {
                                    if (patient_edit.contains(label.getObjectNames(), label_objects[obj])) {
                                        var ele = label_objects[obj];
                                        var new_text = patient[ele.toLowerCase()];
                                        
                                        if (new_text) {
                                            label.setObjectText(label_objects[obj], new_text);
                                        }
                                    }


                                }
                            }

                            // select printer to print on
                            // for simplicity sake just use the first LabelWriter printer
                            var printers = dymo.label.framework.getPrinters();
                            if (printers.length == 0)
                                throw "No DYMO printers are installed. Install DYMO printers.";

                            var printerName = "";
                            for (var i = 0; i < printers.length; ++i) {
                                var printer = printers[i];
                                if (printer.printerType == "LabelWriterPrinter") {
                                    printerName = printer.name;
                                    break;
                                }
                            }

                            if (printerName == "")
                                throw "No LabelWriter printers found. Install LabelWriter printer";

                            // finally print the label
                            var i;
                            for(i = 0; i < noCopy; i++)
                            {
                                label.print(printerName);
                            }
                        }
                    },
                    error: function (rs) {
                        alert('An error has occured: ' + rs);
                    }
                });
            }
        });
    },
    contains: function (a, obj) {
        for (var i = 0; i < a.length; i++) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    },
    setupIframe: function (action) {
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" style="width: 0; height: 0; border: none; display: none;" src="' + action + '"></iframe>');
    },
    togglePatientTypeMessages: function () {
        var val = $('#' + this.prefix + '_patient_type_id').val();

        $('.patient_type_msg').hide();
        if ($('#patient_type_message_' + val).size()) {
            $('#patient_type_message_' + val).show();
            $('.patient_type_message').show();
        } else {
            $('.patient_type_message').hide();
        }
    },
    updateFollowUpDate: function () {
        if ($('#' + this.prefix + '_follow_up:checked').prop('checked')) {
            $('#' + this.prefix + '_follow_up_date').val(this.followUpDate);
        } else {
            $('#' + this.prefix + '_follow_up_date').val('');
        }
    },
    handleNoSearchResultDialog: function () {
        $('#no_results_dialog').dialog('open');
    },
    duplicate: {
        row_prefix: 'dup_patient_',
        initialized: false,

        initialize: function () {
            $('#duplicate_patient_dialog').dialog({
                closeOnEscape: false,
                autoOpen: false,
                height: $(window).height() - 100,
                modal: true,
                width: $(window).width() - 100,
                close: function (e) {
                    if (quickClaim.modals.currentModal() == $(this).attr('id')) {
                        quickClaim.modals.pop($(this).attr('id'));
                    }
                },
                open: function () {
                    hideTooltips();
                }
            });
        },

        showDuplicateError: function (duplicate) {
            var label = '<label for="' + patient_edit.prefix + '_hcn_num" class="error duplicate_error">Duplicate Health Card Found</label>';

            $('#' + patient_edit.prefix + '_hcn_num').parent().append($(label));
            $('#' + patient_edit.prefix + '_hcn_num').addClass('error');
            $('#' + patient_edit.prefix + '_hcn_num').addClass('duplicate_error');

//          $('#duplicate_trigger').click(function () {
//              patient_edit.duplicate.clearTable();
//              patient_edit.duplicate.showDialog(duplicate);
//          });
        },

        clearTable: function () {
            $('#duplicate_table tbody tr td.column1').text('');
            $('#duplicate_table tbody tr td.column2').text('');
        },

        showDialog: function (dup) {
            var rp = this.row_prefix;

            if (!this.initialized) {
                this.initialize();
                this.initialized = true;
            }

            $('#duplicate_table tr').each(function () {
                var field_name = $(this).attr('id').replace(rp, '');
                $('.column2', $(this)).text(dup[field_name]);
            });

            $('#duplicate_patient_dialog').dialog('open');
        }
    },
    loadVersionHistory: function () {
        if (patient_edit.versionsLoaded) {
            return;
        }

        var data = 'id=' + $('#patient_id').val();

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: patient_edit.actions.versionHistory,
            data: data,
            success: function (rs) {

                if (rs.hasOwnProperty('data')) {
                    patient_edit.versionsLoaded = true;

                    var table = '<table class="table sk-datatable" id="versionHistoryTable"><thead><tr>';
                    for (var col in rs.headers) {
                        table += '<th>' + rs.headers[col] + '</th>';
                    }
                    table += '</tr></thead><tbody></tbody><tfoot><tr>';
                    for (var col in rs.headers) {
                        table += '<th></th>';
                    }
                    table += '</tr></tfoot></table>';
                    $('#versionsDiv').html(table);

                    for (var index in rs.data) {
                        var row_id = rs.data[index]['version'];
                        var row = '<tr id="version_' + row_id + '">';

                        for (var column in rs.headers) {
                            var text = rs.data[index][column];
                            row += '<td class="' + column + '">' + text + '</td>';
                        }

                        row += '</tr>';
                        $(row).appendTo($('#versionHistoryTable tbody'))
                    }

                    if ($('#versionHistoryTable tbody tr').size()) {
                        $('#versionHistoryTable').tablesorter1({
                        });


                        quickClaim.highlightVersionDifferences('versionHistoryTable');
                    }
                }
                var tableId = 'versionHistoryTable';
                global_js.customFooterDataTableWithOptions(tableId, true, [], false);
            },
            beforeSend: function () {
                patient_edit.ajax_lock = true;
                $('#messages .icons div').hide();
                $('#spinner').show();

                $('#messages_text').text('Retrieving Version Records');
            },
            complete: function () {
                patient_edit.ajax_lock = false;
                $('#spinner').hide();
            },
            error: function () {
                $('#error_icon').show();
                $('#messages_text').text('An error occurred while contacting the server.  Please refresh the page and try again.');
            }
        });


    },
    clearVersions: function () {
        $('#versionHistoryTable').remove();
    },
    copyMenuInit: function () {
        var parameters = {
            selector: '.patientCopyBtn',
            trigger: 'hover',
            autoHide: true,
            className: 'claims_menu',
            callback: function (key, options, event) {
            },
            items: {
                name: {
                    name: 'Copy To Clipboard: Patient Name',
                    callback: function (key, options, event) {
                        patient_edit.handleCopyEvent(key);
                    }
                },
                address: {
                    name: 'Copy To Clipboard: Patient Address',
                    icon: 'resubmit',
                    callback: function (key, options, event) {
                        patient_edit.handleCopyEvent(key);
                    }
                },
                nameaddress: {
                    name: 'Copy To Clipboard: Patient Name & Address',
                    icon: 'submit',
                    callback: function (key, options, event) {
                        patient_edit.handleCopyEvent(key);
                    }
                },
                phone: {
                    name: 'Copy To Clipboard: Name, Addr., Phone Num.',
                    icon: 'mark_closed',
                    callback: function (key, options, event) {
                        patient_edit.handleCopyEvent(key);
                    }
                },
                sep1: '----',
                full: {
                    name: 'Copy To Clipboard: Full Patient Info',
                    icon: 'mark_closed',
                    callback: function (key, options, event) {
                        patient_edit.handleCopyEvent(key);
                    }
                },
                sep2: '----',
                paste: {
                    name: 'Paste Patient Data from Clipboard',
                    icon: 'mark_paid',
                    callback: function (key, options, event) {
                        patient_edit.handlePasteEvent(key);
                    }
                }
            }
        };

        if (!$.isEmptyObject(parameters.items)) {
            $.contextMenu(parameters);
        }

    },
    handleCopyEvent: function (key) {
        var copyData = '';
        var copyJson = {};
        switch (key) {
            case 'name':
                copyData = $("#patient_lname").val() + ' ' + $("#patient_fname").val();
                copyJson = {
                    patient_lname: $("#patient_lname").val(),
                    patient_fname: $("#patient_fname").val()
                };
                break;
            case 'address':
                var provinceData = $("#patient_province").select2('data');
                copyData = $("#patient_email").val() + '\n' + $("#patient_address").val() + ',\n' + $("#patient_city").val() + ',\n' + $("#patient_postal_code").val() + ',\n' + provinceData.text + '.\n';
                copyJson = {
                    patient_address: $("#patient_address").val(),
                    patient_city: $("#patient_city").val(),
                    patient_province: provinceData,
                    patient_postal_code: $("#patient_postal_code").val(),
                    patient_email: $("#patient_email").val()
                };
                break;
            case 'nameaddress':
                var name = $("#patient_lname").val() + ' ' + $("#patient_fname").val();
                var provinceData = $("#patient_province").select2('data');
                copyData = name + '\n' + $("#patient_email").val() + '\n' + $("#patient_address").val() + ',\n' + $("#patient_city").val() + ',\n' + $("#patient_postal_code").val() + ',\n' + provinceData.text + '.\n';
                copyJson = {
                    patient_lname: $("#patient_lname").val(),
                    patient_fname: $("#patient_fname").val(),
                    patient_email: $("#patient_email").val(),
                    patient_address: $("#patient_address").val(),
                    patient_city: $("#patient_city").val(),
                    patient_province: provinceData,
                    patient_postal_code: $("#patient_postal_code").val()
                };
                break;
            case 'phone':
                var name = $("#patient_lname").val() + ' ' + $("#patient_fname").val();
                var provinceData = $("#patient_province").select2('data');
                copyData = name + '\n' + $("#patient_email").val() + '\n' + $("#patient_address").val() + ',\n' + $("#patient_city").val() + ',\n' + $("#patient_postal_code").val() + ',\n' + provinceData.text + '.\nHome Phone: ' + $("#patient_hphone").val() + '\nCell Phone: ' + $("#patient_cellphone").val() + '\nWork Phone: ' + $("#patient_wphone").val();
                copyJson = {
                    patient_lname: $("#patient_lname").val(),
                    patient_fname: $("#patient_fname").val(),
                    patient_email: $("#patient_email").val(),
                    patient_address: $("#patient_address").val(),
                    patient_city: $("#patient_city").val(),
                    patient_province: provinceData,
                    patient_postal_code: $("#patient_postal_code").val(),
                    patient_hphone: $("#patient_hphone").val(),
                    patient_cellphone: $("#patient_cellphone").val(),
                    patient_wphone: $("#patient_wphone").val()
                };
                break;
            case 'full':
                var name = $("#patient_lname").val() + ' ' + $("#patient_fname").val();
                var provinceData = $("#patient_province").select2('data');
                var sex = $("#patient_sex").select2('data');
                var location = $("#patient_location_id").select2('data');
                var type = $("#patient_patient_type_id").select2('data');
                var status = $("#patient_patient_status_id").select2('data');
                var reason = $("#patient_reason_code_id").val() != '' ? $("#patient_reason_code_id").val() : '';

                copyData = 'Chart Number: ' + $("#patient_patient_number").val() + '\n\n';
                copyData += 'Name:' + name + '\n' +
                        'Sex: ' + sex.text + '\n' +
                        'DOB: ' + $("#patient_dob").val() + '\n\n' +
                        'Address: ' + $("#patient_address").val() + ',' + $("#patient_city").val() + ',' + $("#patient_postal_code").val() + ',' + provinceData.text + '.\n' +
                        'Email: ' + $("#patient_email").val() + '\n' +
                        'Home Phone: ' + $("#patient_hphone").val() + '\n' +
                        'Cell Phone: ' + $("#patient_cellphone").val() + '\n' +
                        'Work Phone: ' + $("#patient_wphone").val() + '\n' +
                        'Other Phone: ' + $("#patient_other_phone").val() + '\n\n' +
                        'Health Card:' + $("#patient_hcn_num").val() + '   ' + $("#patient_hcn_version_code").val() + '\n' +
                        'Expiry Date:' + $("#patient_hcn_exp_year").val() + '\n\n' +
                        'Referring MD: ' + $("#patient_ref_doc_description").val() + '\n' +
                        'Facility Code: ' + $("#patient_fam_doc_description").val() + '\n' +
                        'Family MD: ' + $("#patient_facility_description").val() + '\n' +
                        'Admit Date: ' + $("#patient_admit_date").val() + '\n' +
                        'Family MD: ' + $("#patient_facility_description").val() + '\n\n' +
                        'Location: ' + location.text + '\n' +
                        'Patient Type: ' + type.text + '\n' +
                        'Patient Status: ' + status.text + '\n' +
                        'Reason: ' + reason + '\n' +
                        'Followup Date: ' + $("#patient_follow_up_date").val() +
                        ''
                        ;
                copyJson = {
                    patient_patient_number: $("#patient_patient_number").val(),
                    patient_hcn_num: $("#patient_hcn_num").val(),
                    patient_hcn_version_code: $("#patient_hcn_version_code").val(),
                    patient_hcn_exp_year: $("#patient_hcn_exp_year").val(),
                    patient_sex: sex,
                    patient_dob: $("#patient_dob").val(),
                    patient_ref_doc_description: $("#patient_ref_doc_description").val(),
                    patient_fam_doc_description: $("#patient_fam_doc_description").val(),
                    patient_facility_description: $("#patient_facility_description").val(),
                    patient_admit_date: $("#patient_admit_date").val(),
                    patient_location_id: location,
                    patient_patient_type_id: type,
                    patient_patient_status_id: status,
                    patient_reason_code_id: $("#patient_reason_code_id").val(),
                    patient_email: $("#patient_email").val(),
                    patient_other_phone: $("#patient_other_phone").val(),
                    patient_follow_up: $("#patient_follow_up").val(),
                    patient_follow_up_date: $("#patient_follow_up_date").val(),
                    patient_notes: $("#patient_notes").val(),
                    patient_fam_doc_id: $("#patient_fam_doc_id").val(),
                    patient_fam_doc_num: $("#patient_fam_doc_num").val(),
                    patient_ref_doc_id: $("#patient_ref_doc_id").val(),
                    patient_ref_doc_num: $("#patient_ref_doc_num").val(),
                    patient_facility_num: $("#patient_facility_num").val(),
                    patient_lname: $("#patient_lname").val(),
                    patient_fname: $("#patient_fname").val(),
                    patient_address: $("#patient_address").val(),
                    patient_city: $("#patient_city").val(),
                    patient_province: provinceData,
                    patient_postal_code: $("#patient_postal_code").val(),
                    patient_hphone: $("#patient_hphone").val(),
                    patient_cellphone: $("#patient_cellphone").val(),
                    patient_wphone: $("#patient_wphone").val()
                };
                break;
            default:
                copyData = '';
                copyJson = {};
                break;
        }
        new ClipboardJS('#copyTriggerBtn');
        localStorage.setItem('copy_patient_data', JSON.stringify(copyJson));
        $("#copyTextareaPatient").val(copyData);
        $("#copyTriggerBtn").trigger('click');

        $.notify("Data Copied successfully!", {
            element: 'body',
            type: "success",
            allow_dismiss: true,
            newest_on_top: true,
            placement: {
                from: "top",
                align: "right"
            },
            delay: 5000,
            z_index: 99999,
            mouse_over: 'pause',
            animate: {
                enter: 'animated bounceIn',
                exit: 'animated bounceOut'
            },
            template: `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                            <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                            <span data-notify="icon"></span>
                                            <span data-notify="title">{1}</span>
                                            <span data-notify="message">{2}</span>
                                            <div class="progress" data-notify="progressbar">
                                                    <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                            <a href="{3}" target="{4}" data-notify="url"></a>
                                    </div>`

        });
    },
    handlePasteEvent: function (key) {
        var data = JSON.parse(localStorage.getItem('copy_patient_data'));
        for (var i in data) {
            if (patient_edit.copy_with_hcv) {
                if (i != 'patient_patient_number' /*&& i != 'patient_hcn_num' && i != 'patient_hcn_version_code'*/) {
                    if ($("#" + i).is('select')) {
                        $("#" + i).val(data[i].id).trigger('change.select2');
                    } else if ($("#" + i).is(':checkbox')) {
//                    $("#" + i).prop('checked', true)
                    } else {
                        $("#" + i).val(data[i]);
                    }
                }
            } else {
                if (i != 'patient_patient_number' && i != 'patient_hcn_num' && i != 'patient_hcn_version_code') {
                    if ($("#" + i).is('select')) {
                        $("#" + i).val(data[i].id).trigger('change.select2');
                    } else if ($("#" + i).is(':checkbox')) {
//                    $("#" + i).prop('checked', true)
                    } else {
                        $("#" + i).val(data[i]);
                    }
                }
            }

        }
        patient_edit.copy_with_hcv = false;
    },
    printNumberofCopy: function(action){
            printLabelString = action.replace('dymo','');
            if(patient_edit.canPrintCopies == true){
            $('.background_left').css('background','none');
            //Print number of copy code
            $("#print_number_of_copy_button").attr("disabled", false);
            $("#print_number_of_copy").on( "keypress", function(event) {

                    if (event.which == 13 && !event.shiftKey) {
                        event.preventDefault();
                        $("#print_number_of_copy_button").trigger('click');
                    }

            });
            $('#dymo_print_number_of_copy_dialog').dialog({
                autoOpen: true,
                closeOnEscape: false,
                modal: true,
                position: {my: 'top', at: 'top+100'},
                minHeight: 350,
                width: 500
            });
            $('#print_number_of_copy').focus();
            var count = 1;
            $("#print_number_of_copy_button").click(function(){
                    var noCopy = $("#print_number_of_copy").val().trim();
                    if(count == 1) {
                      if(noCopy == '') {
                        alert('Please enter valid number');
                        return false;
                      }
                      patient_edit.PrintLabel(printLabelString,noCopy);
                      $("#print_number_of_copy_button").attr("disabled", true); 
                      $('#print_number_of_copy').val(1); 
                      $('#dymo_print_number_of_copy_dialog').dialog('close');
                    }
                    count++;
             });
            }
            else
            {
                patient_edit.PrintLabel(printLabelString,1);
            }
    }
};

