var patient_history = {
    actions: {
        action_pdf: null,
        edit: null,
        delete_claim: false,
        submit: null,
        resubmit: null,
        details: null,
        visit_pdf: null
    },

    buttons: {},

    fields: [],
    message_div: 'patient_service',
    prefix: 'service',
    results_dialog: 'patient service',
    notes_tab: 5,
    todo_button_yes: 'todo_continue',
    todo_button_no: 'todo_stop',
    todo_dialog: 'follow_up_dialog',
    appointment_actions: null,
    data: {},
    rowPrefix: 'claimItemRow',
    selectedIDs: new Array(),

    initialize: function () {
        $('#' + this.todo_dialog).dialog({
            closeOnEscape: false,
            autoOpen: false,
            modal: true,
            height: 300,
            width: 600,
            close: function (e) {
                if (quickClaim.modals.currentModal() == $(this).attr('id')) {
                    quickClaim.modals.pop($(this).attr('id'));
                }
            }
        });

         //New add more option mark as paid, mark as submit, mark as unsubmit and mark item as close code start
         $('#claimStatusActionsDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: $(window).height() - 200,
            modal: true,
            width: '80%'
        });
            
        $('#markClosedConfirmation').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
            buttons: {
                "Yes":{
                    click: function () {
                        $('#markClosedConfirmation').dialog('close');
                        claimStatusActions.markClosed.sendAjax();
                    },
                    text: 'Yes',
                    class: 'btn btn-success'
                },
                "No":{
                    click: function () {
                        $('#markClosedConfirmation').dialog('close');
                        $('#claimStatusActionsDialog').dialog('close');
                    },
                    text: 'No',
                    class: 'btn btn-danger'
                }
                        
            }
        });

        $('#rebillConfirmDialog').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
            buttons: {
                "Yes":{
                    click: function () {
                        pastClaims.confirmRebillClick($('#rebill_new_date').val());
                        $('#rebillConfirmDialog').dialog('close');
                    },
                    text: 'Yes',
                    class: 'btn btn-success'
                },
                "No":{
                    click: function () {
                        $('#rebillConfirmDialog').dialog('close');
                    },
                    text: 'No',
                    class: 'btn btn-danger'
                }                        
            }
        });

        $('#rebillCompleteDialog').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
            buttons: {
                "Yes":{
                    click: function () {
                        $('#rebillCompleteDialog').dialog('close');
                        if (claimStatusActions.baseClass = 'pastClaims') {
                            var patient_id = $('#patient_id').val();
                            $('#patient_search_patient_number').val(patient_id);
                            $("#search_button_patient").trigger('click');
                        } else if (claimStatusActions.baseClass == 'details') {
                            details.triggerPageRefresh();
                        }
                    },
                    text: 'Yes',
                    class: 'btn btn-success'
                },
                "No":{
                    click: function () {
                        $('#rebillCompleteDialog').dialog('close');
                    },
                    text: 'No',
                    class: 'btn btn-danger'
                }                        
            }
        });

        $('#actionCompleteDialog').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
            buttons: {
                "Yes":{
                    click: function () {
                        $('#claimStatusActionsDialog').dialog('close');
                        $('#actionCompleteDialog').dialog('close');
                        if (claimStatusActions.baseClass = 'pastClaims') {
                            var patient_id = $('#patient_id').val();
                            $('#patient_search_patient_number').val(patient_id);
                            $("#search_button_patient").trigger('click');
                        } else if (claimStatusActions.baseClass == 'details') {
                            details.triggerPageRefresh();
                        }
                    },
                    text: 'Yes',
                    class: 'btn btn-success'
                },
                "No":{
                    click: function () {
                        var statusThIndex = $('#patient_service_history_results_table th:contains("Status")').index();
                        var claimStatusActionstusCodeThIndex = $('th:contains("Claim status code")').index();
                        var ClientIdArr = claimStatusActions.getClaimidSNew;   
                        var getStatus = claimStatusActions.getStatus;  
                        var getCheckboxVal = claimStatusActions.updateStatusRow;
                        var result = getCheckboxVal.toString().split(',');
                        $.each(result,function(i,val){
                            if(statusThIndex > 0)
                            {
                            $(".claim_one_"+val+" td").eq(statusThIndex).html('<b>'+claimStatusActions.getStatus[0].status+'</b>');
                            }
                        });
                        claimStatusActions.clearAfterStatusUpdated();

                        $('#claimStatusActionsDialog').dialog('close');
                        $('#actionCompleteDialog').dialog('close');
                    },
                    text: 'No',
                    class: 'btn btn-danger'
                }                        
            }
        });

        $('#markPaidConfirmation').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
             buttons: {
                "Yes":{
                    click: function () {
                         $('#markPaidConfirmation').dialog('close');
                        claimStatusActions.markPaid.sendAjax();
                    },
                    text: 'Yes',
                    class: 'btn btn-success'
                },
                "No":{
                    click: function () {
                        $('#markPaidConfirmation').dialog('close');
                        $('#claimStatusActionsDialog').dialog('close');
                    },
                    text: 'No',
                    class: 'btn btn-danger'
                }                        
            }
        });

        $('#resubmitConfirmation').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
            buttons: {
                "Yes":{
                    click: function () {
                        $('#resubmitConfirmation').dialog('close');
                    claimStatusActions.resubmit.sendAjax();
                    },
                    text: 'Yes',
                    class: 'btn btn-success'
                },
                "No":{
                    click: function () {
                    $('#resubmitConfirmation').dialog('close');
                    $('#claimStatusActionsDialog').dialog('close');
                    },
                    text: 'No',
                    class: 'btn btn-danger'
                }                        
            }
        });

        $('#resubmitCompleteDialog').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 500,
            buttons: {
                "Yes":{
                    click: function () {
                    $('#claimStatusActionsDialog').dialog('close');
                    $('#resubmitCompleteDialog').dialog('close');
                    var ClientIdArr = claimStatusActions.getClientId;
                    patient_history.handleEditGroupClick(ClientIdArr);
                    },
                    text: 'Yes',
                    class: 'btn btn-success'
                },
               "No - Reload Search Results":{
                    click: function () {
                        $('#claimStatusActionsDialog').dialog('close');
                        $('#resubmitCompleteDialog').dialog('close');
                        if (claimStatusActions.baseClass == 'pastClaims') {
                            var patient_id = $('#patient_id').val();
                            $('#patient_search_patient_number').val(patient_id);
                            $("#search_button_patient").trigger('click');
                        } else if (claimStatusActions.baseClass == 'details') {
                            details.triggerPageRefresh();
                        }
                    },
                    text: "No - Reload Search Results",
                    class: 'btn btn-warning'
                },
                "No - Back To Current Results":{
                    click: function () {
                        var statusThIndex = $('#patient_service_history_results_table th:contains("Status")').index() + 1;
                        //var claimStatusCodeThIndex = $('th:contains("Claim status code")').index() + 1;
                        var ClientIdArr = claimStatusActions.getClaimidSNew;    
                        var getStatus = claimStatusActions.getStatus;  

                        var getCheckboxVal = claimStatusActions.updateStatusRow;
                       
                        $.each(getStatus,function(i,val){
                            if(statusThIndex > 1)
                            {
                            $(".claim_"+val.claimitemid+" td:nth-child("+statusThIndex+")").html('<b>'+getStatus[0].status+'</b>');
                            }                         
                        });
                        claimStatusActions.clearAfterStatusUpdated();
                        
                        $('#claimStatusActionsDialog').dialog('close');
                        $('#resubmitCompleteDialog').dialog('close');
                    },
                    text: "No - Back To Current Results",
                    class: 'btn btn-danger'
                }
            }
        });

        $('#batch_creation_dialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            minWidth:800,
            minHeight:250,
            height: $(window).height() - 200,
            modal: true,
            width: '80%',
            close: function () {
                batch_create.resetValues();
            }
        });

        $('#batchCreationConfirmationDialog').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 300,
            buttons: [
                {
                    text: "Download Summary PDF",
                    "class": 'btn btn-teal btn-squared',
                    click: function () {
                        $('#batchCreationConfirmationDialog').dialog('close');
                        $('#batch_creation_dialog').dialog('close');
                        batch_create.downloadPDFSummary();
                    }
                },{
                    text: "Close",
                    "class": 'btn btn-danger btn-squared',
                    click: function () {
                        var patient_id = $('#patient_id').val();
                        $('#patient_search_patient_number').val(patient_id);
                        $("#search_button_patient").trigger('click');
                        $('#batchCreationConfirmationDialog').dialog('close');
                        $('#batch_creation_dialog').dialog('close');
                    }
                }
            ]
        });

        //Code end for add more option





        $('#' + this.todo_button_yes).button();
        $('#' + this.todo_button_yes).tooltip();
        $('#' + this.todo_button_yes).click(function () {
            patient_history.handleFollowUpEnterPress();
            return false;
        });

        $('#' + this.todo_button_no).button();
        $('#' + this.todo_button_no).tooltip();
        $('#' + this.todo_button_no).click(function () {
            $('#' + patient_history.todo_dialog).dialog('close');
        });

        
    },
    handleFollowUpEnterPress: function () {
        if (patient_edit.ajax_lock) {
            return;
        }
        var data = $('#follow_up_form').serialize();

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: patient_history.actions.follow_up,
            data: data,
            success: function (rs)
            {
                var tableId = 'patient_notes_history_results_table';
                if ($.fn.DataTable.isDataTable('#' + tableId)) {
                    $("#" + tableId).dataTable().fnDestroy();
                    $("#" + tableId + " thead tr:eq(1)").remove();
                }
                $('#patient_notes_history tbody tr').each(function () {
                    if ($('td.note', $(this)).text() == $('#follow_up_completion_notes').val()) {
                        $(this).removeClass('incomplete').addClass('complete');
                        jQuery(this).find('.details').find('b').html('Complete');
                    }
                    ;
                });
                var tableId = 'patient_notes_history_results_table';
                if (!$.fn.DataTable.isDataTable('#patient_notes_history_results_table')) {
                    global_js.customFooterDataTableWithOptions(tableId, true);
                }


                $('#' + patient_history.todo_dialog).dialog('close');

            },
            beforeSend: function () {
                var tableId = 'patient_notes_history_results_table';
                if ($.fn.DataTable.isDataTable('#' + tableId)) {
                    $("#" + tableId).dataTable().fnDestroy();
                    $("#" + tableId + " thead tr:eq(1)").remove();
                }
                patient_edit.ajax_lock = true;
                $('#spinner').show();
                $('#messages_text').text('Updating To-Do');
            },
            complete: function () {
                patient_edit.ajax_lock = false;
                $('#spinner').hide();
                $('#messages_text').text('');
            }
        });

    },
    clearData: function () {
        $('#claim_tab').text('Services');
        $('#notes_tab').text('Notes');
        $('#appointment_tab').text('Appointments');

        $('#patient_service_history table').remove();
        $('#patient_notes_history table').remove();
        $('#patient_appointment_history table').remove();

        $('#top_notes thead').remove();
        $('#top_notes tbody').remove();
    },
    drawNotesTable: function (data, header, table_id) {
        var table = '<table style="width:100%" class="sk-datatable table table-hover table-stripped" id="' + table_id + '_results_table">' + '<thead><tr>';

        for (var key in header) {
            if (header.hasOwnProperty(key)) {
                if(key == 'date'){
                    table += '<th data-sksort="datetime" class="sorter-false">' + header[key] + '</th>';
                } else {
                    table += '<th class="sorter-false">' + header[key] + '</th>';
                }
                
            }
        }
        //~ table += '</tr></thead>' + '</table>';
        table += '</tr></thead>';

        table += '</table>';
        $('#' + table_id).html(table);

 

        for (var key in data) {
            //console.log('data',data[key]['timestamp']);
            row = '<tr id="note_row_' + key + '" class="' + data[key].row_class + '">';
            for (var key2 in header) {
                var d = data[key][key2] == null ? ' ' : data[key][key2];
                row += '<td class="' + key2 + '">' + d + '</td>';
                
            }
            row += '</tr>';
            $(row).appendTo($('#' + table_id + ' table'));

            $('#note_row_' + key + '.incomplete').click(function () {
                k = this.id.replace('note_row_', '');

                patient_history.handleFollowUpClick(data[k]);
            });
        }

        var tableId = 'patient_notes_history_results_table';
        sk.table.init(tableId);
        sk.table.loader(tableId, 'stop');
//        global_js.customFooterDataTableWithOptions(tableId, true, [], false);
    },
    handleFollowUpClick: function (data) {
        $('#follow_up_completion_notes').val(data.note);
        $('#follow_up_id').val(data.id);
        quickClaim.modals.addModal(this.todo_dialog);
    },
    handleServiceHistory: function (rs) {
        //alert(rs.total.claim_max_date);
        /*console.log(rs.data[0]['claim_id']);*/
        
        if(rs.total.claim_total == 0) 
        {
            $('#claim_tab').html('Services (' + rs.total.claim_total + ') <br> ');
            $('#claim_tab').css('line-height','32px');
        }   
        else
        {
            $('#claim_tab').html('Services (' + rs.total.claim_total + ') <br> '+rs.total.claim_max_date+'');
             $('#claim_tab').css('line-height','16px');
        }

        var table = '<table width="100%" cellpadding="0" class="sk-datatable table table-hover table-striped" id="patient_service_history_results_table"><thead><tr>';
        var test = 1;
        for (var column in rs.header) {
            if (column == 'actions') {
                table += '<th class="sorter-false">' + rs.header[column] + '</th>';
            } else {
                if (test == 1) {
                    table += '<th class="sorter-false">' + rs.header[column] + '</th>';
                    test++;
                } else {
                    table += '<th class="sorter-false">' + rs.header[column] + '</th>';
                }
            }
        }
        table += '</tr></thead><tbody></tbody><tfoot><tr>';

        for (var column in rs.header) {
            table += '<th></th>';
        }

        table += '</tr></tfoot></table>';
        $('#patient_service_history').html(table);

        for (var index in rs.data) {
            var row_id = rs.data[index]['id'];
            var claim_item_id = rs.data[index]['id'];
            var claim_id = rs.data[index]['claim_id'];
            var row = '<tr id="' + row_id + '" class="claim_'+ claim_id +' claim_one_'+ row_id +'">';

            for (var column in rs.header) {
                var text = rs.data[index][column];

                if (column == 'actions') {

                    var resubmitclass = (!rs.data[index]['actions'].hasOwnProperty('resubmit')) ? 'disableservicecontext' : '';
                    var submitclass = (!rs.data[index]['actions'].hasOwnProperty('submit')) ? 'disableservicecontext' : '';
                    var mark_paidclass = (!rs.data[index]['actions'].hasOwnProperty('mark_paid')) ? 'disableservicecontext' : '';
                    var mark_closedclass = (!rs.data[index]['actions'].hasOwnProperty('mark_closed')) ? 'disableservicecontext' : '';

                    row += '<td>';

                    row += '<div class="btn-group">' +
                            '<a href="#" data-toggle="dropdown" style="color:#FFF" class="btn btn-primary btn-squared dropdown-toggle">' +
                            '<i class="clip-paperplane"></i> Action <span class="caret"></span>' +
                            '</a>' +
                            '<ul class="dropdown-menu" role="menu">';
                    if (rs.data[index]['actions'].hasOwnProperty('edit')) {
                        row += '<li role="presentation" class="action_edit">' +
                                '<a href="javascript:void(0)" onclick="claimEdit(event)" tabindex="-1" role="menuitem" id="edit_' + row_id + '" class="edit">' +
                                'Edit' +
                                '</a>' +
                                '</li>';
                    }

                    if (rs.data[index]['actions'].hasOwnProperty('details')) {
                        row += '<li role="presentation" class="action_details">' +
                                '<a href="javascript:void(0)" onclick="claimDetails(event)" tabindex="-1" class="details" id="details_' + row_id + '">' +
                                'Details' +
                                ' </a>' +
                                ' </li>';
                    }
                
                    if(patient_history.actions.delete_claim == 'true'){
                        row += '<li role="presentation" class="action_delete">'+
                            '<a href="javascript:void(0)" onclick="claimDelete(event, '+ rs.data[index]['claim_id'] +','+row_id+')" tabindex="-1" class="delete" id="delete_'+ row_id + '">'+
                            'Delete All Items & Claim'+
                            '</a>'+
                            '</li>';
                        }else{
                            row += '<li role="presentation" class="action_delete">'+
                            '<a href="javascript:void(0)" tabindex="-1" class="delete" id="delete_'+ row_id + '" style="color:#ccc;">'+
                            'Delete All Items & Claim'+
                            '</a>'+
                            '</li>';
                        }

                    if (rs.data[index]['actions'].hasOwnProperty('pdf')) {
                        row += '<li role="presentation" class="action_pdf">' +
                                '<a href="javascript:void(0)" onclick="claimPDF(event)" tabindex="-1" class="pdf" id="pdf_' + row_id + '">' +
                                'PDF' +
                                ' </a>' +
                                ' </li>';
                    }

                    row += '<li role="presentation" class="action_details">' +
                            '<a href="javascript:void(0)" class="'+resubmitclass+'" onclick="patient_history.MarkasUnsubmitted(event, '+ rs.data[index]['claim_id'] +','+row_id+');">' +
                            'Mark Claim as Unsubmitted' +
                            ' </a>' +
                            ' </li>';

                    row += '<li role="presentation" class="action_details">' +
                            '<a href="javascript:void(0)"  class="'+submitclass+'" onclick="patient_history.MarkasSubmitted(event, '+ rs.data[index]['claim_id'] +','+row_id+');">' +
                            'Submit Claim' +
                            ' </a>' +
                            ' </li>';

                    row += '<li role="presentation" class="action_details">' +
                            '<a href="javascript:void(0)" class="'+mark_paidclass+'"  onclick="patient_history.MarkasPaid(event, '+ rs.data[index]['claim_id'] +','+row_id+');" >' +
                            'Mark Item as Paid' +
                            ' </a>' +
                            ' </li>';

                    row += '<li role="presentation" class="action_details">' +
                            '<a href="javascript:void(0)" class="'+mark_closedclass+'" onclick="patient_history.MarkasClosed(event, '+ rs.data[index]['claim_id'] +','+row_id+');" >' +
                            'Mark Item as Closed' +
                            ' </a>' +
                            ' </li>';

                    if (rs.data[index]['charge_to'] == 'DIRECT' || rs.data[index]['charge_to'] == 'THIRD PARTY') {
                        var inv_url = patient_history.actions.action_pdf + "?id=" + claim_id + "&type=invoice";
                        row += '<li role="presentation" class="action_pdf"><a href="' + inv_url + '" tabindex="-1" target="_blank"> Invoice PDF </a></li>';

                        var rec_url = patient_history.actions.action_pdf + "?id=" + claim_id + "&type=receipt";
                        row += '<li role="presentation" class="action_pdf"><a href="' + rec_url + '" tabindex="-1" target="_blank"> Receipt PDF </a></li>';

                        var man_url = patient_history.actions.action_pdf + "?id=" + claim_id + "&type=manual";
                        row += '<li role="presentation" class="action_pdf"><a href="' + man_url + '" tabindex="-1" target="_blank"> Manual PDF </a></li>';
                    }
                    row += '</ul>' +
                            '</div></td>';

                } else {
                    if(column == 'fee_subm' && text == '')
                    {
                       text = '$0';
                    } 
                    else if(column == 'fee_paid' && text == '')
                    {
                       text = '$0';
                    }
                    row += '<td class="' + column + '">' + text + '</td>';
                }
            }


            row += '</tr>';
            $(row).appendTo($('#patient_service_history_results_table tbody'));
            $('tr#' + row_id + ' input[type=button]').button().addClass('ui-button-inline');


            $('#edit_' + row_id).on('click', function () {
                var k = $(this).attr('id').replace('edit_', '');
                var claim_id = $('tr#' + k + ' td.claim_id').text();
                window.location = patient_history.actions.edit + '?id=' + claim_id;
            });
            $('#pdf_' + row_id).click(function () {
                var k = $(this).attr('id').replace('pdf_', '');
                var claim_id = $('tr#' + k + ' td.claim_id').text();

                patient_history.setupIframe();
                patient_history.setupHiddenForm(new Array(), patient_history.actions.action_pdf + '?id=' + claim_id, '');
            });
            $('#details_' + row_id).click(function () {
                var k = $(this).attr('id').replace('details_', '');
                var claim_id = $('tr#' + k + ' td.claim_id').text();

                window.location = patient_history.actions.details + '?id=' + claim_id;
            });
        }

        if (rs.total.hasOwnProperty('total_subm')) {
            totals = '<tfoot><tr>'
                    + '<td>' + rs.total.claim_total + ' Claim Items</td>'
                    + '<td>' + rs.total.subm + '</td>'
                    + '<td>' + rs.total.paid + '</td></tr></tfoot>';
            $(totals).appendTo($('#patient_service_history table'));
        }

        /*if ($('#patient_service_history_results_table tbody tr').size()) {
            $('#patient_service_history_results_table').tablesorter({
                widgets: [],
                headers: {
                    12: {
                        sorter: 'sortDate-dmyHia'
                    }
                }
            });
        }*/
        if (!$.fn.DataTable.isDataTable('#' + tableId)) {
            var tableId = 'patient_service_history_results_table';
            global_js.customFooterDataTableWithOptions(tableId, true, [], false);
        }
    },
    MarkasPaid:function(event,claim_id,item_id) {
        // var obj2 = JSON.parse(localStorage.getItem(item_id));
        // if (obj2['pay_progs']['DIRECT']) {
        //     pastClaims.handleEditClick(pastClaims.getClaimId(item_id), item_id, quickClaim.isRightClick(event));
        // } else {
        //     patient_history.handleMarkPaidClick([item_id]);
        // }
        patient_history.handleMarkPaidClick([item_id]);
    }, 
    handleMarkPaidClick: function (claimItemIDs) {
        patient_history.selectedIDs = claimItemIDs;
        claimStatusActions.markPaid.openDialog(claimItemIDs);
    },
    MarkasClosed:function(event,claim_id,item_id){
        patient_history.handleMarkClosedClick([item_id]); 
    },
    handleMarkClosedClick: function (claimItemIDs) {
        patient_history.selectedIDs = claimItemIDs;
        claimStatusActions.markClosed.openDialog(claimItemIDs);
    },
    MarkasUnsubmitted:function(event,claim_id,item_id){
        patient_history.handleUnsubmitClick([claim_id]);
    },
    handleUnsubmitClick: function (claimItemIDs) {
        patient_history.selectedIDs = claimItemIDs;
        claimStatusActions.resubmit.openDialog(claimItemIDs);
    },
    MarkasSubmitted:function(event,claim_id,item_id){
        patient_history.handleSubmitClick([claim_id]);
    },
    handleSubmitClick: function (claimItemIDs) {
         if (!claimItemIDs.length) {
            quickClaim.showOverlayMessage('No Submittable Claims are selected.');
            setTimeout(function () {
                quickClaim.hideOverlayMessage();
            }, 2000);
            return;
        }
        var data = 'claim_ids=' + claimItemIDs.join(',');
        batch_create.handleDoctorLocationCountRequest(data);
        $('#batch_creation_dialog').on('dialogclose', function (event) {
            $('#unsubmitted_search').trigger('click');
            $('#unsubmitted_search').trigger('click');
        });
    },
    handleEditGroupClick: function (ids) {
        quickClaim.showOverlayMessage('Loading Claim Edit Interface');
        var ClientIdsJoin = ids.join(',');
        $("#inputBulkEdit").val(ClientIdsJoin);
        $("#pastClaimBulkEditForm").submit();
    },
    confirmRebillClick: function (date) {
        var claimIDs = pastClaims.results.getActionClaimIDs('resubmit');
        var claimItemIDs = pastClaims.results.getActionClaimItemIDs('resubmit');

        $.ajax({
            type: 'post',
            dataType: 'json',
            data: 'c_ids=' + claimIDs.join(',') + '&ids=' + claimItemIDs.join(',') + '&date=' + date,
            url: pastClaims.actions.changeItems,
            success: function (rs) {
                $('#rebillCompleteDialog').dialog('open');

                var html = '<ul>';
                if (rs.res) {
                    for (var msg in rs.res) {
                        html += '<li>' + rs.res[msg] + '</li>';
                    }
                }
                
                if (rs.error) {
                    html += '</ul><h4>Errors</h4><ul>';
                    for (var err in rs.error) {
                        html += '<li>' + 'Claim for ' + rs.error[err] + '</li>';
                    }
                    html += '</ul>';
                }
                $('.rebillCount').html(html);
            },
            beforeSend: function () {
                quickClaim.showOverlayMessage('Cloning selected claims');
            },
            complete: function () {
                quickClaim.hideOverlayMessage();
            }
        });
    },
    handleNotesHistory: function (rs) {
        patient_history.drawNotesTable(rs.data, rs.header, 'patient_notes_history');
        //$('#notes_tab').text('Notes (' + rs.data.length + ')');

        if(rs.data.length == 0) { 
        $('#notes_tab').html('Notes (' + rs.data.length + ')');
        $('#notes_tab').css('line-height','32px');
        } else
        {
           $('#notes_tab').html('Notes (' + rs.data.length + ') <br> '+rs.total.notes_max_date+'');  
           $('#notes_tab').css('line-height','16px'); 
        }

        $('#tmpsearch, #tmpID, #history_tab, #ui-id-1').css('line-height','32px');

        var s;
        s = '<thead><tr>'
                + '<th>Type</th>'
                + '<th>Note</th>'
                + '<th>Details</th>'
                + '<th>Date</th>'
                + '</tr></thead>';

        $('#top_notes thead').remove();
        $('#top_notes tbody').remove();
        $('#top_notes tfoot').remove();
        $('#top_notes').append($(s));


        var count = 0;
        for (var n in rs.data) {
            s = '<tr class="' + rs.data[n].row_class + '">'
                    + '<td>' + rs.data[n].type + '</td>'
                    + '<td>';

            s += rs.data[n].note.substring(0, 80);
            if ((rs.data[n].note).length > 80) {
                s += '...';
            }
            count++;

            s += '</td>'
                    + '<td>' + rs.data[n].details + '</td>'
                    + '<td>' + rs.data[n].date + '</td>'
                    + '</tr>';

            $('#top_notes').append(s);

            if (count == 3) {
                break;
            }
        }

        $('#top_notes tbody').click(function (e) {
            switchTabs('top_notes');
            return false;
        });
    },
     handleAppointmentHistory: function (rs) {
        if (!patient_history.appointment_actions) {
            patient_history.appointment_actions = rs.actions;
        }

        if(rs.data.length == 0){ 
            $('#appointment_tab').html('Appointments (' + rs.data.length + ') <br> ');
            $('#appointment_tab').css('line-height','32px');
        }
        else
        {
            $('#appointment_tab').html('Appointments (' + rs.data.length + ') <br> '+rs.total.appointment_max_date+'');
             $('#appointment_tab').css('line-height','16px'); 
        }


        if (rs && rs.hasOwnProperty('data')) {
            patient_history.drawAppointmentTable(rs.data, rs.header, rs.actions);

            for (var a in rs.data) {
                for (var b in rs.data[a].actions) {
                    if (b == 'cancel') {
                        patient_history.addAppointmentCancel(a, rs.data[a].appointment_id);
                    } else if (b == 'reschedule') {
                        patient_history.addAppointmentReschedule(a, rs.data[a].appointment_id);
                    } else if (b == 'edit') {
                        patient_history.addAppointmentEdit(a, rs.data[a].appointment_id);
                    } else if (b == 'reverse') {
                        patient_history.addAppointmentReverse(a, rs.data[a].appointment_id);
                    } else if (b == 'claim') {
                        patient_history.addCreateAClaim(a, rs.data[a].appointment_attendee_appointment_id);
                    } else if (b == 'visit_pdf') {
                        patient_history.addVisitPDF(a, rs.data[a].appointment_attendee_id);
                    } else if (b == 'profile') {
                        patient_history.gotoprofile(a, rs.data[a].appointment_attendee_id);
                    }
                }
            }
        }
    },
    gotoprofile: function (row_id, appt_id) {
        $('#patient_appointment_history_table #appointment_' + row_id + ' input[type=button].profile').button()
            .click(function () {
                $("#tabs").tabs("option", "active", 1);
                return false;
            });
    },
    addAppointmentCancel: function (row_id, appt_id) {
        $('#patient_appointment_history_table #appointment_' + row_id + ' input[type=button].cancel').button()
                .click(function () {
                    patient_history.handleAppointmentCancel(appt_id);
                    return false;
                });
    },
    addAppointmentReschedule: function (row_id, appt_id) {
        $('#patient_appointment_history_table #appointment_' + row_id + ' input[type=button].reschedule').button()
                .click(function () {
                    patient_history.handleAppointmentReschedule(appt_id);
                    return false;
                });
    },
    addAppointmentEdit: function (row_id, appt_id) {
        $('#patient_appointment_history_table #appointment_' + row_id + ' input[type=button].edit').button()
                .click(function () {
                    patient_history.handleAppointmentEdit(appt_id);
                    return false;
                });
    },
    addAppointmentReverse: function (row_id, appt_id) {
        $('#patient_appointment_history_table #appointment_' + row_id + ' input[type=button].reverse').button()
                .click(function () {
                    patient_history.handleAppointmentReverse(appt_id, row_id);
                    return false;
                });
    },
    addCreateAClaim: function (row_id, appointment_attendee_id) {
        $('#patient_appointment_history_table #appointment_' + row_id + ' input[type=button].claim').button();
        $('#patient_appointment_history_table #appointment_' + row_id + ' input[type=button].claim').click(function () {
            patient_history.handleAppointmentClaim(appointment_attendee_id);
            return false;
        });
    },
    addVisitPDF: function (row_id, appointment_attendee_id) {
        $('#patient_appointment_history_table #appointment_' + row_id + ' input[type=button].visit_pdf').button();
        $('#patient_appointment_history_table #appointment_' + row_id + ' input[type=button].visit_pdf').click(function () {
            patient_history.handleAppointmentVisitPdf(appointment_attendee_id);
            return false;
        });
    },
    handleAppointmentCancel: function (id) {
        $('#' + patient_edit.appointment_form_prefix + '_patient_id').val($('#' + patient_edit.prefix + '_id').val());
        $('#' + patient_edit.appointment_form_prefix + '_appointment_id').val(id);
        $('#' + patient_edit.appointment_form_prefix + '_mode').val('cancel');
        $('#' + patient_edit.appointment_form_id).submit();
    },
    handleAppointmentEdit: function (id) {
        $('#' + patient_edit.appointment_form_prefix + '_patient_id').val($('#' + patient_edit.prefix + '_id').val());
        $('#' + patient_edit.appointment_form_prefix + '_appointment_id').val(id);
        $('#' + patient_edit.appointment_form_prefix + '_mode').val('edit');
        $('#' + patient_edit.appointment_form_id).submit();
    },
    handleAppointmentClaim: function (id) {
        var url = patient_edit.actions.claim + '/index/appointment_attendee_id/' + id;
        window.location = url;
    },
    handleAppointmentReschedule: function (id) {
        $('#' + patient_edit.appointment_form_prefix + '_patient_id').val($('#' + patient_edit.prefix + '_id').val());
        $('#' + patient_edit.appointment_form_prefix + '_appointment_id').val(id);
        $('#' + patient_edit.appointment_form_prefix + '_mode').val('reschedule');
        $('#' + patient_edit.appointment_form_id).submit();
    },
    appointmentVisitDownloadComplete: function () {
        quickClaim.hideOverlayMessage();
        $('#spinner').hide();
    },
    handleAppointmentVisitPdf: function (id) {
        hypeFileDownload.tokenName = "AppointmentVisitSheetToken";
        hypeFileDownload.setReturnFunction(patient_history.appointmentVisitDownloadComplete);

        var url = patient_history.actions.visit_pdf + '?appointmentAttendeeID=' + id + '&AppointmentVisitSheetToken=' + hypeFileDownload.blockResubmit();

        $('#checkmark').hide();
        $('#error_icon').hide();
        $('#spinner').show();
        $('#messages_text').text('Downloading Appointment Visit Sheet').show();
        quickClaim.showOverlayMessage('Downloading Appointment Visit Sheet');

        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    handleAppointmentReverse: function (id, row_id) {
        var params = 'appointment[id]=' + id
                + '&appointment[patient_id]=' + $('#' + patient_edit.prefix + '_id').val()
                + '&mode=patient_profile';

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: patient_history.actions.reverse,
            data: params,
            success: function (rs) {
                if (rs.hasOwnProperty('data')) {
                    var data = rs.data[0];

                    $('#patient_appointment_history_table #appointment_' + row_id + ' td').each(function () {
                        var c = $(this).attr('class');
                        if (data.hasOwnProperty(c)) {
                            $(this).text(data[c]);
                        } else if (patient_history.appointment_actions.hasOwnProperty(c) && data['actions'].hasOwnProperty(c)) {
                            var action = rs['actions'][c];
                            $(this).text('');
                            $(this).html('<input type="button" id="' + action + '_' + row_id + '" class="ui-widget ui-button-inline ' + action + '" value="' + action.toUpperCase() + '" />');
                        } else {
                            $(this).html('&nbsp;');
                        }
                    });

                    for (var a in data.actions) {
                        if (data.actions[a] == 'cancel') {
                            patient_history.addAppointmentCancel(row_id, data.id);
                        } else if (data.actions[a] == 'reschedule') {
                            patient_history.addAppointmentReschedule(row_id, data.id);
                        } else if (data.actions[a] == 'edit') {
                            patient_history.addAppointmentEdit(row_id, data.id);
                        } else if (data.actions[a] == 'reverse') {
                            patient_history.addAppointmentReverse(row_id, data.id);
                        } else if (data.actions[a] == 'claim') {
                            patient_history.addCreateAClaim(row_id, data.appointment_attendee_id);
                        } else if (data.actions[a] == 'visit_pdf') {
                            patient_history.addVisitPDF(row_id, data.appointment_attendee_id);
                        }
                    }
                } else {
                    $('#error_icon').show();
                    $('#messages_text').text('An error occurred at the server.');
                }
            },
            beforeSend: function () {
                $('#checkmark').hide();
                $('#error_icon').hide();
                $('#spinner').show();
                $('#messages_text').text('Reversing Appointment Status').show();
            },
            complete: function () {
                $('#spinner').hide();
            },
            error: function () {
                $('#error_icon').show();
                $('#messages_text').text('An error occurred while contacting the server.').show();
            }
        });
    },
    setupIframe: function () {
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    setupHiddenForm: function (fields, action, prefix) {
        setTimeout(function () {
            var frame_body = $('#hiddenDownloader').contents().find('body');
            var form = '<form id="hidden_form" action="' + action + '" method="POST">';

            for (var a in fields) {
                var name = fields[a].substring(0, fields[a].indexOf('='));
                var value = fields[a].substring(fields[a].indexOf('=') + 1);

                name = name.replace(past_claims.form_prefix, prefix);
                form += '<input type="text" value="' + value + '" id="' + name + '" name="' + name + '" />';
            }
            form += '</form>';
            frame_body.append($(form));
            $('#hiddenDownloader').contents().find('#hidden_form').submit();
        });
    },
    drawAppointmentTable: function (data, header, actions) {
        var table = '<table style="width:100%" class="sk-datatable table-striped table table-hover" id="patient_appointment_history_table"><thead><tr>';
        for (var key in header) {
            if (header.hasOwnProperty(key)) {
                if (key == 'actions_text') {
                    table += '<th class="sorter-false">' + header[key] + '</th>';
                } else {
                    if(key == 'date_time'){
                        table += '<th data-sksort="datetime" class="sorter-false">' + header[key] + '</th>';
                    } else {
                        table += '<th class="sorter-false">' + header[key] + '</th>';
                    }                    
                }
            }
        }
        table += '</tr></thead><tbody></tbody><tfoot><tr>';
        for (var key in header) {
            if (header.hasOwnProperty(key)) {
                table += '<th></th>';
            }
        }
        table += '</tr></tfoot></table>';
        $('#patient_appointment_history').html(table);

        for (var key in data) {
            var row = '<tr id="appointment_' + key + '">';
            for (var key2 in header) {
                if (key2 == 'actions_text') {
                    row += '<td><div class="btn-group" style="width:160px;">' +
                            '<a href="#" data-toggle="dropdown" style="color:#FFF" class="btn btn-primary btn-squared dropdown-toggle">' +
                            '<i class="clip-paperplane"></i> Action <span class="caret"></span>' +
                            '</a>' +
                            '<ul class="dropdown-menu" role="menu">';
                    for (var a in data[key]['actions']) {

                        row += '<li role="presentation" class="action_edit">' +
                                '<input style="border:0px" type="button" id="' + a + '_' + key + '" class="btn-block btn btn-squared btn-default sk-txt-left ' + a + '" value="' + data[key]['actions'][a].toUpperCase() + '" />' +
                                '</li>';

                    }
                    row += '</ul>' +
                            '</div></td>';
                } else {
                    var d = data[key][key2] == null ? ' ' : data[key][key2];
                    row += '<td class="' + key2 + '">' + d + '</td>';
                }
            }
            row += '</tr>';
            $(row).appendTo($('#patient_appointment_history_table tbody'));
        }

        var tableId = 'patient_appointment_history_table';
        sk.table.init(tableId);
        sk.table.loader(tableId, 'stop');
//        global_js.customFooterDataTableWithOptions(tableId, true, [], false);

    }
}
