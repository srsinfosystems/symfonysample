var facilityForm = {
    actions: {
        save: null
    },

    messageID: 'facilityEditMessages',

    initialize: function () {
        $('#createFacilityButton')
            .button()
            .click(function () {
                facilityForm.editFacility({});
            });

        $('#facilitySaveButton')
            .button()
            .click(function () {
                facilityForm.saveFacility();
            });

        $('#facilityCloseDialogButton')
            .button()
            .click(function () {
                $('#facilityEditFormDialog').dialog('close');
            });

        $('#facilityEditFormDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 400,
            modal: true,
            width: 800,
            close: function () {
                facilityForm.clearForm();
            }
        });
    },

    editFacility: function (data) {
        facilityForm.fillForm(data);
        $('#facilityEditFormDialog').dialog('open');
    },
    saveFacility: function () {
        if (quickClaim.ajaxLocked) {
            return;
        }

        facilityForm.clearFormErrors();
        var data = $('#facilityEditForm').serialize();

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: facilityForm.actions.save,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('success')) {
                    $('#facilityEditFormDialog').dialog('close');
                    $('#facilitiesTable').trigger('pageSet.pager');

                    quickClaim.showAjaxMessage(facilitiesList.messageID, 'success', null);
                }
                else if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#facilitiesErrorDiv', '#facility', true);
                    var message = 'The form contains invalid data.';

                    quickClaim.showAjaxMessage(facilityForm.messageID, 'error', message);
                    quickClaim.showAjaxMessage(facilitiesList.messageID, 'error', message);
                }
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                var message = 'Saving Facility';

                quickClaim.showAjaxMessage(facilityForm.messageID, 'spin', message);
                quickClaim.showAjaxMessage(facilitiesList.messageID, 'spin', message);
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
            },
            error: function () {
                quickClaim.ajaxLocked = false;
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                quickClaim.showAjaxMessage(facilityForm.messageID, 'error', message);
                quickClaim.showAjaxMessage(facilitiesList.messageID, 'error', message);
            }
        });
    },
    clearFormErrors: function() {
        var $form = $('#facilityEditForm');
        $form.find('#facilitiesErrorDiv').text('').hide();
        $form.find('label.error').remove();
        $form.find('.error').removeClass('error');

    },
    clearForm: function () {
        facilityForm.clearFormErrors();
        $('#facilitiesTable').find('tr.current_edit').removeClass('current_edit');

        quickClaim.hideAjaxMessage(facilityForm.messageID);
        $('#facilityEditForm').clearForm();
    },
    fillForm: function (data) {
        this.clearForm();

        $('#facilitiesTable').find('tr#facilityRow_' + data.id).addClass('current_edit');

        var prefix = '#facility_';

        for (var key in data) {
            if ($(prefix + key)) {
                $(prefix + key).val(data[key]);
            }
        }
        quickClaim.focusTopField();
    }
};
