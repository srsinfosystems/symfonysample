var serviceCodeUploadForm = {
    actions: {
        upload: null
    },
    messageID: 'serviceCodeFileUploadMessage',
    parent: 'serviceCodeList',

    initialize: function () {
        $('#uploadServiceCodeFileButton')
            .button()
            .click(function () {
                $('#serviceCodeFileUploadDialog').dialog('open');
            });

        $('#serviceCodeUploadButton')
            .button()
            .click(function () {
                serviceCodeUploadForm.submitForm();
            });

        $('#serviceCodeUploadCloseDialogButton')
            .button()
            .click(function () {
                $('#serviceCodeFileUploadDialog').dialog('close');
            });

        $('#serviceCodeFileUploadDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 400,
            modal: true,
            width: 800
        });
    },

    showDialog: function () {
        $('#serviceCodeFileUploadDialog').dialog('open');
    },

    submitForm: function () {
        var formData = new FormData($("#serviceCodeUploadForm")[0]);
        formData.append('parent', serviceCodeUploadForm.parent);

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: serviceCodeUploadForm.actions.upload,
            data: formData,

            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function (rs) {
                if (rs.hasOwnProperty('success')) {
                    $('#serviceCodeFileUploadDialog').dialog('close');

                    if (serviceCodeUploadForm.parent == 'import') {
                        quickClaim.hideAjaxMessage(dataImport.messageID);
                    }
                    quickClaim.hideAjaxMessage(serviceCodeUploadForm.messageID);

                }
                else if (rs.hasOwnProperty('errors')) {
                    var errorMessage = 'The form contains invalid data.';

                    quickClaim.showFormErrors(rs.errors, '#serviceCodesUploadErrorDiv', '#expl_codes_upload', true);
                    quickClaim.showAjaxMessage(serviceCodeUploadForm.messageID, 'error', errorMessage);
                    if (serviceCodeUploadForm.parent == 'import') {
                        quickClaim.showAjaxMessage(dataImport.messageID, 'error', errorMessage);
                    }
                }

                if (rs.hasOwnProperty('batchJobs') && serviceCodeUploadForm.parent == 'serviceCodeList') {
                    serviceCodeList.displayBatchJobDetails(rs['batchJobs']);
                }

                if (rs.hasOwnProperty('importData') && serviceCodeUploadForm.parent == 'import') {
                    $('#tabs').tabs('option', 'active', 0);
                    for (var a in rs['importData']) {
                        dataImport.loadTableRow(rs['importData'][a]);
                    }
                }
            },
            beforeSend: function () {
                var message = 'Uploading .XLS File';
                quickClaim.ajaxLocked = true;

                quickClaim.showAjaxMessage(serviceCodeUploadForm.messageID, 'spin', message);
                if (serviceCodeUploadForm.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'spin', message);
                }
                else if (serviceCodeUploadForm.parent == 'serviceCodeList') {
                    $('#' + serviceCodeList.messages.spinner).show();
                    $('#' + serviceCodeList.messages.text).text(message).show();
                }
                quickClaim.showOverlayMessage(message);
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
            },
            error: function () {
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';
                quickClaim.ajaxLocked = false;

                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(serviceCodeUploadForm.messageID, 'error', message);
                if (serviceCodeUploadForm.parent == 'serviceCodeList') {
                    quickClaim.showAjaxMessage(serviceCodeList.messageID, 'error', message);
                }
                else if (serviceCodeUploadForm.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'error', message);
                }
            }
        });
    }
};