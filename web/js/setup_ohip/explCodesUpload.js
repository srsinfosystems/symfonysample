var explCodesUploadForm = {
    actions: {
        upload: null
    },
    messageID: 'explCodeFileUploadMessage',
    parent: 'explCodesList',

    initialize: function () {
        $('#uploadExplCodeFileButton')
            .button()
            .click(function () {
                $('#explCodeFileUploadDialog').dialog('open');
            });

        $('#explCodeUploadButton')
            .button()
            .click(function () {
                explCodesUploadForm.submitForm();
            });

        $('#explCodeUploadCloseDialogButton')
            .button()
            .click(function () {
                $('#explCodeFileUploadDialog').dialog('close');
            });

        $('#explCodeFileUploadDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 400,
            modal: true,
            width: 800
        });
    },

    showDialog: function () {
        $('#explCodeFileUploadDialog').dialog('open');
    },

    submitForm: function () {
        var formData = new FormData($("#explCodeUploadForm")[0]);

        if (explCodesUploadForm.parent == 'import') {
            formData.append('parent', explCodesUploadForm.parent);
        }

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: explCodesUploadForm.actions.upload,
            data: formData,

            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function (rs) {
                if (rs.hasOwnProperty('success')) {
                    $('#explCodeFileUploadDialog').dialog('close');

                    if (explCodesUploadForm.parent == 'explCodesList') {
                        quickClaim.hideAjaxMessage(explCodesList.messageID)
                    }
                    else if (explCodesUploadForm.parent == 'import') {
                        quickClaim.hideAjaxMessage(dataImport.messageID);
                    }
                    quickClaim.hideAjaxMessage(explCodesUploadForm.messageID);
                }
                else if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#explCodesUploadErrorDiv', '#expl_codes_upload', true);

                    var errorMessage = 'The form contains invalid data';
                    quickClaim.showAjaxMessage(explCodesUploadForm.messageID, 'error', errorMessage);
                    if (explCodesUploadForm.parent == 'explCodesList') {
                        quickClaim.showAjaxMessage(explCodesList.messageID, 'error', errorMessage);
                    }
                    else if (explCodesUploadForm.parent == 'import') {
                        quickClaim.showAjaxMessage(dataImport.messageID, 'error', errorMessage);
                    }
                }

                if (rs.hasOwnProperty('batchJobs') && explCodesUploadForm.parent == 'explCodesList') {
                    explCodesList.displayBatchJobDetails(rs['batchJobs']);
                }

                if (rs.hasOwnProperty('importData') && explCodesUploadForm.parent == 'import') {
                    $('#tabs').tabs('option', 'active', 0);
                    for (var a in rs['importData']) {
                        dataImport.loadTableRow(rs['importData'][a]);
                    }
                }
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                var message = 'Uploading .XLS File';

                quickClaim.showAjaxMessage(explCodesUploadForm.messageID, 'spin', message);
                if (explCodesUploadForm.parent == 'explCodesList') {
                    quickClaim.showAjaxMessage(explCodesList.messageID, 'spin', message);
                }
                else if (explCodesUploadForm.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'spin', message);
                }
                quickClaim.showOverlayMessage(message);
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
            },
            error: function () {
                quickClaim.ajaxLocked = false;
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(explCodesUploadForm.messageID, 'error', message);

                if (explCodesUploadForm.parent == 'explCodesList') {
                    quickClaim.showAjaxMessage(explCodesList.messageID, 'error', message);
                }
                else if (explCodesUploadForm.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'error', message);
                }
            }

        });
    }
};