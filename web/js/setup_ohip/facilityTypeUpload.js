var facilityTypeUpload = {
    actions: {
        upload: null
    },
    messageID: 'facilityTypeUploadMessages',
    parent: 'facilityTypeList',

    initialize: function() {
        $('#uploadFacilityTypeFileButton')
            .button()
            .click(function() {
                $('#facilityTypeFileUploadDialog').dialog('open');
            });

        $('#facilityTypeUploadButton')
            .button()
            .click(function() {
                facilityTypeUpload.submitForm();
            });

        $('#facilityTypeUploadCloseDialogButton')
            .button()
            .click(function() {
                $('#facilityTypeFileUploadDialog').dialog('close');
            });

        $('#facilityTypeFileUploadDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 400,
            modal: true,
            width:  800
        });
    },
    showDialog: function () {
        $('#facilityTypeFileUploadDialog').dialog('open');
    },
    submitForm: function() {
        var formData = new FormData($("#facilityTypeUploadForm")[0]);
        formData.append('parent', facilityTypeUpload.parent);

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: facilityTypeUpload.actions.upload,
            data: formData,

            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function(rs) {
                if (rs.hasOwnProperty('success')) {
                    $('#facilityTypeFileUploadDialog').dialog('close');
                    if (facilityTypeUpload.parent == 'facilityTypeList') {
                        quickClaim.hideAjaxMessage(facilityTypeList.messageID);
                    }
                    else if (facilityTypeUpload.parent == 'import') {
                        quickClaim.hideAjaxMessage(dataImport.messageID);
                    }
                }
                else if (rs.hasOwnProperty('errors')) {
                    var errorMessage = 'The form contains invalid data';

                    quickClaim.showAjaxMessage(facilityTypeUpload.messageID, 'error', errorMessage);
                    if (facilityTypeUpload.parent == 'facilityTypeList') {
                        quickClaim.showAjaxMessage(facilityTypeList.messageID, 'error', errorMessage);
                    }
                    else if (facilityTypeUpload.parent == 'import') {
                        quickClaim.showAjaxMessage(dataImport.messageID, 'error', errorMessage);
                    }

                    quickClaim.showFormErrors(rs.errors, '#facilityTypesUploadErrorDiv', '#facility_type_upload', true);
                }

                if (rs.hasOwnProperty('batchJobs')) {
                    if (facilityTypeUpload.parent == 'facilityTypeList') {
                        facilityTypeList.displayBatchJobDetails(rs.batchJobs);
                    }
                }

                if (rs.hasOwnProperty('importData') && explCodesUploadForm.parent == 'import') {
                    $('#tabs').tabs('option', 'active', 0);
                    for (var a in rs['importData']) {
                        dataImport.loadTableRow(rs['importData'][a]);
                    }
                }
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                var message = 'Uploading .XLS File';

                quickClaim.showAjaxMessage(facilityTypeUpload.messageID, 'spin', message);
                if (facilityTypeUpload.parent == 'facilityTypeList') {
                    quickClaim.showAjaxMessage(facilityTypeList.messageID, 'spin', message);
                }
                else if (facilityTypeUpload.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'spin', message);
                }
                quickClaim.showOverlayMessage(message);
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
            },
            error: function () {
                quickClaim.ajaxLocked = false;
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(facilityTypeUpload.messageID, 'error', message);
                if (facilityTypeUpload.parent == 'facilityTypeList') {
                    quickClaim.showAjaxMessage(facilityTypeList.messageID, 'error', message);
                }
                else if (facilityTypeUpload.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'error', message);
                }
            }
        });
    }
};