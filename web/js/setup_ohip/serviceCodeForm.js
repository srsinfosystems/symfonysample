var serviceCodeForm = {
	actions: {
		save: null
	},
	messages: {
		error: 'serviceCodeEditErrorIcon',
		spinner: 'serviceCodeEditSpinner',
		checkmark: 'serviceCodeEditCheckmark',
		text: 'serviceCodeEditMessageText',
		spinnerHtml: '',
		checkmarkHtml: '',
		errorHtml: ''
	},

	initialize: function() {
		$('#createServiceCodeButton')
            .button()
            .click(function() {
                serviceCodeForm.editServiceCode({});
            });

		$('#serviceCodeSaveButton')
            .button()
            .click(function() {
                serviceCodeForm.saveServiceCode();
            });

		$('#serviceCodeCloseDialogButton')
            .button()
            .click(function() {
                $('#serviceCodeEditFormDialog').dialog('close');
            });

        $('#service_code_fee_general').blur(function() {
            var val = $(this).val().replace(',', '');
            val = parseFloat(val);
           $(this).val(number_format(val, 2, '.', ','));
        });
        $('#service_code_fee_assistant').blur(function() {
            var val = $(this).val().replace(',', '');
            val = parseFloat(val);
            $(this).val(number_format(val, 2, '.', ','));
        });
        $('#service_code_fee_specialist').blur(function() {
            var val = $(this).val().replace(',', '');
            val = parseFloat(val);
            $(this).val(number_format(val, 2, '.', ','));
        });
        $('#service_code_fee_ana').blur(function() {
            var val = $(this).val().replace(',', '');
            val = parseFloat(val);
            $(this).val(number_format(val, 2, '.', ','));
        });
        $('#service_code_fee_non_ana').blur(function() {
            var val = $(this).val().replace(',', '');
            val = parseFloat(val);
            $(this).val(number_format(val, 2, '.', ','));
        });

		$('#serviceCodeEditFormDialog').dialog({
			closeOnEscape: true,
			autoOpen: false,			
			modal: true,
			width:  1000,
			close: function() {
				serviceCodeForm.clearForm();
			}
		});
	},

	editServiceCode: function(data) {
		serviceCodeForm.fillForm(data);
		$('#serviceCodeEditFormDialog').dialog('open');
	},
	saveServiceCode: function() {
		if (quickClaim.ajaxLocked) {
			return;
		}

        serviceCodeForm.clearErrors();
		var data = $('#serviceCodeEditForm').serialize();

 		$.ajax({
			type: 'post',
			dataType: 'json',
			url: serviceCodeForm.actions.save,
			data: data,
			success: function(rs) {
				if (rs.hasOwnProperty('success')) {
					$('#serviceCodeEditFormDialog').dialog('close');
					$('#serviceCodeTable').trigger('pageSet.pager');
				}
				else if (rs.hasOwnProperty('errors')) {
					quickClaim.showFormErrors(rs.errors, '#serviceCodeErrorDiv', '#service_code', true);

					$('#' + serviceCodeForm.messages.error).show();
					$('#' + serviceCodeForm.messages.text).text('The form contains invalid data.').show();
				}
			},
			beforeSend: function(rs) {
				quickClaim.ajaxLocked = true;

				$('#serviceCodeMessages .icons div').hide();
				$('#' + serviceCodeList.messages.spinner).show();
				$('#' + serviceCodeList.messages.text).text('Saving Service Code').show();

				$('#serviceCodeEditMessages .icons div').hide();
				$('#' + serviceCodeForm.messages.spinner).show();
				$('#' + serviceCodeForm.messages.text).text('Saving Service Code').show();
			},
			complete: function(rs) {
				quickClaim.ajaxLocked = false;

				$('#' + serviceCodeList.messages.spinner).hide();
				$('#' + serviceCodeForm.messages.spinner).hide();
			},
			error: function(rs) {
				quickClaim.ajaxLocked = false;

				$('#' + serviceCodeForm.messages.error).show();
				$('#' + serviceCodeForm.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();

				$('#' + serviceCodeList.messages.error).show();
				$('#' + serviceCodeList.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
			}
		});
	},
	clearErrors: function() {
        $('#serviceCodeEditForm #serviceCodeErrorDiv').text('').hide();
        $('#serviceCodeEditForm label.error').remove();
        $('#serviceCodeEditForm .error').removeClass('error');
    },
    clearForm: function() {
		$('#serviceCodeTable tr.current_edit').removeClass('current_edit');
		$('#serviceCodeEditForm #serviceCodeErrorDiv').text('').hide();
		$('#serviceCodeEditForm label.error').remove();
		$('#serviceCodeEditForm .error').removeClass('error');

		$('#serviceCodeEditMessages .icons div').hide();
		$('#' + serviceCodeForm.messages.text).text('').hide();

		$('#serviceCodeEditForm').clearForm();
	},
	fillForm: function(data) {
		this.clearForm();

		$('#serviceCodeTable tr#serviceCodeRow_' + data.id).addClass('current_edit');

		var prefix = '#service_code_';

		for (var key in data) {
			if (key == 'active' || key == 'req_diag_code' || key == 'req_ref_doctor' || key == 'req_facility_num' || key == 'req_admit_date') {
				$(prefix + key).prop('checked', (data[key] == 'Y'));
			}
            else if (key == 'fee_general' || key == 'fee_specialist' || key == 'fee_assistant' || key == 'fee_ana' || key == 'fee_non_ana') {
                $(prefix + key).val(data[key].replace('$', ''));
            }
			else if ($(prefix + key)) {
				$(prefix + key).val(data[key]);
			}
		}

        if (!$(prefix + 'id').val()) {
            $(prefix + 'units_assistant').val(1);
            $(prefix + 'units_ana').val(1);
            $(prefix + 'units_non_ana').val(1);

            $(prefix + 'fee_general').val('0.00');
            $(prefix + 'fee_assistant').val('0.00');
            $(prefix + 'fee_specialist').val('0.00');
            $(prefix + 'fee_ana').val('0.00');
            $(prefix + 'fee_non_ana').val('0.00');

            // fill in units and fees with 1 and 0.00
        }

		quickClaim.focusTopField();
	}
}
