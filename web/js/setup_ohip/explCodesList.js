var explCodesList = {
    actions: {
        explCodeTable: null
    },
    // TODO: switch message display structure
    messageID: 'explCodesListMessage',
    explCodes: {},

    initialized: false,
    pagerColumns: {},
    paginationSize: 25,
    paginationSort: [],

    initialize: function () {
        if (!explCodesList.initialized) {
            $('#explCodesSearchButton')
                    .button()
                    .click(function () {
                        $("#explCodeTable thead tr:eq(1)").remove();
                        $('#explCodeTable').trigger('pageSet.pager');
                    });

            $('#explCodesResetButton')
                    .button()
                    .click(function () {
                        $('#explCodesSearchForm').clearForm();
                    });

            explCodesList.initialized = true;
            explCodesList.initializePager();
        }
    },
    initializePager: function () {
        var $table = $('#explCodeTable');

        $table.find('thead th.actions').addClass('sorter-false');
        $table.find('thead th:last').append(quickClaim.tablesorterTooltip);
        $table.tablesorter({
            theme: 'blue',
            headerTemplate: '{content} {icon}',
            widgets: ['selectRow', 'zebra', 'pager'],
            sortList: explCodesList.paginationSort
        });
        $table.find('.sortTip').tooltip();

        $table.tablesorterPager({
            container: $('.explCodesPager'),
            size: 500,
            cssPageSize: '.pagesize',
            ajaxUrl: explCodesList.actions.explCodeTable + '?{sortList:sort}}',
            customAjaxUrl: function (table, url) {
                return url + '&' + $('#explCodesSearchForm').serialize();
            },
            ajaxObject: {
                dataType: 'json',
                complete: function () {
                    quickClaim.ajaxLocked = false;
                    quickClaim.hideOverlayMessage();

                    for (var a in explCodesList.explCodes) {
                        if (explCodesList.explCodes[a].hasOwnProperty('actions') && explCodesList.explCodes[a].actions.hasOwnProperty('edit')) {
                            $('tr#explCodeRow_' + explCodesList.explCodes[a].id).click(function () {
                                explCodesList.handleAction($(this).prop('id'));
                            });
                        }
                    }
                    
                    global_js.customFooterDataTable("explCodeTable", true);
                    
                },
                beforeSend: function () {
                    $("#explCodeTable").dataTable().fnDestroy();
                    $("#explCodeTable thead tr:eq(1)").remove();
                    var message = 'Requesting Error / Expl Codes';

                    quickClaim.ajaxLocked = true;
                    quickClaim.showAjaxMessage(explCodesList.messageID, 'spin', message);
                    quickClaim.showOverlayMessage(message);
                },
                error: function () {
                    var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                    quickClaim.ajaxLocked = false;

                    quickClaim.showAjaxMessage(explCodesList.messageID, 'error', message);
                    quickClaim.hideOverlayMessage(explCodesList.messageID);

                }
            },
            ajaxProcessing: function (rs) {
                quickClaim.hideOverlayMessage(1);

                var message = 'Received Error Codes list.';
                quickClaim.showAjaxMessage(explCodesList.messageID, 'success', message);

                if (rs.hasOwnProperty('batchJobs')) {
                    explCodesList.displayBatchJobDetails(rs['batchJobs']);
                }

                if (rs.hasOwnProperty('data')) {
                    explCodesList.pagerColumns = rs.headers;
                    explCodesList.explCodes = {};
                    var data = rs.data;
                    var rows = '';

                    for (var a in data)
                    {
                        explCodesList.explCodes[data[a].id] = data[a];
                        rows += '<tr id="explCodeRow_' + data[a].id + '">';
                        rows += explCodesList.getExplCodeTableRow(data[a]);
                        rows += '</tr>' + "\n";
                    }

                    return [rs.count, $(rows)];
                }
            }
        });
    },

    displayBatchJobDetails: function (data) {
        var $div = $('.batchJobStatusDiv');

        if (data['job_count'] == 0) {
            $div.hide();
            return;
        }

        $div.find('.batchCount').text(data['job_count']);
        $div.find('ul li').remove();

        for (var a in data['jobs']) {
            var li = '<li><b>' + data['jobs'][a]['document_name'] + ':</b> ' + data['jobs'][a].status + '</li>';

            $div.find('ul').append($(li));
        }

        $div.show();
    },
    getExplCodeTableRow: function (data) {
        var rs = '';

        for (var b in explCodesList.pagerColumns)
        {
            if (b == 'description') {
                rs += '<td style="max-width:600px !important;" class="' + b + '"><pre>' + data[b] + '</pre></td>';
            } else if (b != 'actions') {
                rs += '<td class="' + b + '">' + data[b] + '</td>';
            }
        }
        return rs;
    },
    handleAction: function (buttonID) {
        buttonID = buttonID.replace('explCodeEdit_', '');
        var id = buttonID.substring(buttonID.indexOf('_') + 1, buttonID.length);

        explCodesForm.editExplCode(explCodesList.explCodes[id]);
    }


};