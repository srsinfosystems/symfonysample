var serviceCodeList = {
    actions: {
        serviceCodeTable: null
    },
    messages: {
        error: 'serviceCodeErrorIcon',
        spinner: 'serviceCodeSpinner',
        checkmark: 'serviceCodeCheckmark',
        text: 'serviceCodeMessageText',
        spinnerHtml: '',
        checkmarkHtml: '',
        errorHtml: ''
    },
    serviceCodes: {},

    initialized: false,
    pagerColumns: {},
    paginationSize: 25,
    paginationSort: [],

    initialize: function () {
        if (!serviceCodeList.initialized) {
            $('#serviceCodeSearchButton').button();
            $('#serviceCodeResetButton').button();

            $('#serviceCodeSearchButton').click(function () {
                $("#serviceCodeTable thead tr:eq(1)").remove();
                $('#serviceCodeTable').trigger('pageSet.pager');
            });

            $('#serviceCodeResetButton').click(function () {
                $('#serviceCodeSearchForm').clearForm();
            });

            serviceCodeList.initialized = true;
            serviceCodeList.initializePager();
        }
    },
    initializePager: function () {
        $('#serviceCodeTable thead th.actions').addClass('sorter-false');
        $('#serviceCodeTable thead th:last').append(quickClaim.tablesorterTooltip);
        $('#serviceCodeTable').tablesorter({
            theme: 'blue',
            headerTemplate: '{content} {icon}',
            widgets: ['selectRow', 'zebra', 'pager'],
            sortList: serviceCodeList.paginationSort
        });
        //$('#serviceCodeTable .sortTip').tooltip();

        $('#serviceCodeTable').tablesorterPager({
            container: $('.serviceCodePager'),
            size: serviceCodeList.paginationSize,
            cssPageSize: '.pagesize',
            ajaxUrl: serviceCodeList.actions.serviceCodeTable + '?{sortList:sort}&pageNumber={page}&size={size}',
            customAjaxUrl: function (table, url) {
                return url + '&' + $('#serviceCodeSearchForm').serialize();
            },
            ajaxObject: {
                dataType: 'json',
                complete: function () {
                    quickClaim.ajaxLocked = false;
                    $('#' + serviceCodeList.messages.spinner).hide();

                    for (var a in serviceCodeList.serviceCodes) {
                        if (serviceCodeList.serviceCodes[a].hasOwnProperty('actions') && serviceCodeList.serviceCodes[a].actions.hasOwnProperty('edit')) {
                            $('tr#serviceCodeRow_' + serviceCodeList.serviceCodes[a].id).click(function () {
                                serviceCodeList.handleAction($(this).prop('id'));
                            });
                        }
                    }



                    var tableId = 'serviceCodeTable';
                    global_js.customFooterDataTable(tableId);
                },
                beforeSend: function () {
                    $("#serviceCodeTable").dataTable().fnDestroy();
                     $("#serviceCodeTable thead tr:eq(1)").remove();
                    quickClaim.ajaxLocked = true;
                    $('#serviceCodeMessages .icons div').hide();
                    $('#' + serviceCodeList.messages.spinner).show();
                    $('#' + serviceCodeList.messages.text).text('Requesting Service Codes').show();
                },
                error: function () {
                    $('#' + serviceCodeList.messages.error).show();
                    $('#' + serviceCodeList.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            },
            ajaxProcessing: function (rs) {
                quickClaim.hideOverlayMessage(1);

                $('#' + serviceCodeList.messages.text).text('Received Service Codes list.').show();
                $('#' + serviceCodeList.messages.checkmark).show();

                if (rs.hasOwnProperty('batchJobs')) {
                    serviceCodeList.displayBatchJobDetails(rs.batchJobs);
                }

                if (rs.hasOwnProperty('data')) {
                    serviceCodeList.pagerColumns = rs.headers;
                    serviceCodeList.serviceCodes = {};
                    var data = rs.data;
                    var rows = '';

                    for (var a in data)
                    {
                        serviceCodeList.serviceCodes[data[a].id] = data[a];
                        rows += '<tr id="serviceCodeRow_' + data[a].id + '">';
                        rows += serviceCodeList.getServiceCodeTableRow(data[a], data[a].id);
                        rows += '</tr>' + "\n";
                    }

                    return [rs.count, $(rows)];
                }
            }
        });
    },

    displayBatchJobDetails: function (data) {
        if (data.job_count == 0) {
            $('.batchJobStatusDiv').hide();
            return;
        }

        $('.batchJobStatusDiv .batchCount').text(data.job_count);
        $('.batchJobStatusDiv ul li').remove();

        for (var a in data.jobs) {
            var li = '<li><b>' + data.jobs[a].document_name + ':</b> ' + data.jobs[a].status + '</li>';

            $('.batchJobStatusDiv ul').append($(li));
        }

        $('.batchJobStatusDiv').show();
    },
    getServiceCodeTableRow: function (data, id) {
        var rs = '';

        for (var b in serviceCodeList.pagerColumns)
        {
            if (b == 'description') {
                rs += '<td class="' + b + '">' + data[b] + '</td>';
            } else if (b != 'actions') {
                rs += '<td class="' + b + '">' + data[b] + '</td>';
            }
        }
        return rs;
    },
    handleAction: function (buttonID) {
        buttonID = buttonID.replace('serviceCodeEdit_', '');
        var id = buttonID.substring(buttonID.indexOf('_') + 1, buttonID.length);

        serviceCodeForm.editServiceCode(serviceCodeList.serviceCodes[id]);
    }
};