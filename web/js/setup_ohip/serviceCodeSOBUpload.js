var serviceCodeSOBUploadForm = {
    actions: {
        upload: null
    },
    messageID: 'serviceCodeSOBUploadMessages',
    parent: 'serviceCodeList',

    initialize: function () {
        $('#uploadSOBFileButton')
            .button()
            .click(function () {
                $('#serviceCodeSOBFileUploadDialog').dialog('open');
            });

        $('#serviceCodeSOBUploadButton')
            .button()
            .click(function () {
                serviceCodeSOBUploadForm.submitForm();
            });

        $('#serviceCodeSOBUploadCloseDialogButton')
            .button()
            .click(function () {
                $('#serviceCodeSOBFileUploadDialog').dialog('close');
            });

        $('#serviceCodeSOBFileUploadDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 400,
            modal: true,
            width: 800
        });
    },
    showDialog: function () {
        $('#serviceCodeSOBFileUploadDialog').dialog('open');
    },
    submitForm: function () {
        var formData = new FormData($("#serviceCodeSOBUploadForm")[0]);
        formData.append('parent', serviceCodeSOBUploadForm.parent);

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: serviceCodeSOBUploadForm.actions.upload,
            data: formData,

            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function (rs) {
                if (rs.hasOwnProperty('success')) {
                    $('#serviceCodeSOBFileUploadDialog').dialog('close');

                    if (serviceCodeSOBUploadForm.parent == 'import') {
                        quickClaim.hideAjaxMessage(dataImport.messageID);
                    }
                    quickClaim.hideAjaxMessage(serviceCodeSOBUploadForm.messageID);
                }
                else if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#serviceCodeSOBUploadErrorDiv', '#service_code_upload', true);

                    var message = 'The form contains invalid data';
                    quickClaim.showAjaxMessage(serviceCodeSOBUploadForm.messageID, 'error', message);
                    if (serviceCodeSOBUploadForm.parent == 'import') {
                        quickClaim.showAjaxMessage(dataImport.messageID, 'error', message);
                    }
                }

                if (rs.hasOwnProperty('batchJobs') && serviceCodeSOBUploadForm.parent == 'serviceCodeList') {
                    serviceCodeList.displayBatchJobDetails(rs.batchJobs);
                }

                if (rs.hasOwnProperty('importData') && serviceCodeSOBUploadForm.parent == 'import') {
                    $('#tabs').tabs('option', 'active', 0);
                    for (var a in rs['importData']) {
                        dataImport.loadTableRow(rs['importData'][a]);
                    }
                }
            },
            beforeSend: function () {
                var message = 'Uploading SOB File';
                quickClaim.ajaxLocked = true;

                quickClaim.showAjaxMessage(serviceCodeSOBUploadForm.messageID, 'spin', message);
                if (serviceCodeSOBUploadForm.parent == 'serviceCodeList') {
                    $('#serviceCodesMessages').find('.icons div').hide();
                    $('#' + serviceCodeList.messages.spinner).show();
                    $('#' + serviceCodeList.messages.text).text('Uploading SOB File').show();
                }
                else if (serviceCodeSOBUploadForm.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'spin', message);
                }
                quickClaim.showOverlayMessage(message);
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
            },
            error: function () {
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';
                quickClaim.ajaxLocked = false;

                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(serviceCodeSOBUploadForm.messageID, 'error', message);
                if (serviceCodeSOBUploadForm.parent == 'serviceCodeList') {
                    $('#' + serviceCodeList.messages.error).show();
                    $('#' + serviceCodeList.messages.text).text(message).show();
                }
                else if (serviceCodeSOBUploadForm.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'error', message);
                }
            }
        });
    }
};