var facilityTypeList = {
    actions: {
        table: null
    },
    data: {},

    initialized: false,
    pagerColumns: {id: 'ID', code: 'Code', name: 'Name'},
    paginationSize: 25,
    paginationSort: [],
    messageID: 'facilityTypesMessages',

    initialize: function () {
        if (!facilityTypeList.initialized) {
            facilityTypeList.initialized = true;
            facilityTypeList.initializePager();
        }
    },
    initializePager: function () {
        $('#facilityTypesTable thead th:last').append(quickClaim.tablesorterTooltip);
        $('#facilityTypesTable').tablesorter({
            theme: 'blue',
            headers: {
                0: {sorter: false},
                1: {sorter: false},
                2: {sorter: false}
            },
            headerTemplate: '{content} {icon}',
            widgets: ['selectRow', 'zebra', 'pager'],
            sortList: facilityTypeList.paginationSort
        });
        $('#facilityTypesTable .sortTip').tooltip();

        $('#facilityTypesTable').tablesorterPager({
            container: $('.facilityTypesPager'),
            size: 100,
            cssPageSize: '.pagesize',
            ajaxUrl: facilityTypeList.actions.table + '?{sortList:sort}&pageNumber={page}&size={size}',
            ajaxObject: {
                dataType: 'json',
                complete: function () {
                    quickClaim.ajaxLocked = false;

                    for (var a in facilityTypeList.data) {
                        $('tr#facilityTypeRow_' + facilityTypeList.data[a].id).click(function () {
                            facilityTypeList.handleAction($(this).prop('id'));
                        });
                    }
                    
                    
                    var tableId = 'facilityTypesTable';
                    global_js.customFooterDataTable(tableId);
                },
                beforeSend: function () {
                    $("#facilityTypesTable").dataTable().fnDestroy();
                    $("#facilityTypesTable thead tr:eq(1)").remove();
                    var message = 'Requesting Facility Types';

                    quickClaim.ajaxLocked = true;
                    quickClaim.showAjaxMessage(facilityTypeList.messageID, 'spin', message);
                    quickClaim.showOverlayMessage(message);
                },
                error: function () {
                    var message = 'An error occurred while contacting the server. Please refresh the page and try again.'
                    quickClaim.ajaxLocked = false;

                    setTimeout(function () {
                        quickClaim.hideOverlayMessage();
                    }, 2000);
                    quickClaim.showAjaxMessage(facilityTypeList.messageID, 'error', message);
                }
            },
            ajaxProcessing: function (rs) {
                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(facilityTypeList.messageID, 'success', 'Received Facility Types List');

                if (rs.hasOwnProperty('batchJobs')) {
                    facilityTypeList.displayBatchJobDetails(rs.batchJobs);
                }

                if (rs.hasOwnProperty('data')) {
                    facilityTypeList.data = {};
                    var data = rs.data;
                    var rows = '';

                    for (var a in data)
                    {
                        facilityTypeList.data[data[a].id] = data[a];
                        rows += '<tr id="facilityTypeRow_' + data[a].id + '">';
                        rows += facilityTypeList.getFacilityTypeTableRow(data[a], data[a].id);
                        rows += '</tr>' + "\n";
                    }

                    return [rs.count, $(rows)];
                }
            }
        });
    },

    displayBatchJobDetails: function (data) {
        if (data.job_count == 0) {
            $('.batchJobStatusDiv').hide();
            return;
        }

        $('.batchJobStatusDiv .batchCount').text(data.job_count);
        $('.batchJobStatusDiv ul li').remove();

        for (var a in data.jobs) {
            var li = '<li><b>' + data.jobs[a].document_name + ':</b> ' + data.jobs[a].status + '</li>';

            $('.batchJobStatusDiv ul').append($(li));
        }

        $('.batchJobStatusDiv').show();
    },
    getFacilityTypeTableRow: function (data, id) {
        var rs = '';

        for (var b in facilityTypeList.pagerColumns)
        {
            rs += '<td class="' + b + '">' + data[b] + '</td>';
        }
        return rs;
    },
    handleAction: function (buttonID) {
        buttonID = buttonID.replace('facilityTypeRow_', '');
        var id = buttonID.substring(buttonID.indexOf('_') + 1, buttonID.length);

        facilityTypeForm.editFacilityType(facilityTypeList.data[id]);
    }
};