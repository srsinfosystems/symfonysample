var facilityTypeForm = {
    actions: {
        save: null
    },
    messageID: 'facilityTypeEditMessages',

    initialize: function() {
        $('#createFacilityTypeButton')
            .button()
            .click(function() {
                facilityTypeForm.editFacilityType({});
            });

        $('#facilityTypeSaveButton')
            .button()
            .click(function() {
            facilityTypeForm.saveFacilityType();
        });

        $('#facilityTypeCloseDialogButton')
            .button()
            .click(function() {
                $('#facilityTypeEditFormDialog').dialog('close');
            });

        $('#facilityTypeEditFormDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 400,
            modal: true,
            width:  800,
            close: function() {
                facilityTypeForm.clearForm();
            }
        });
    },

    editFacilityType: function(data) {
        facilityTypeForm.fillForm(data);
        $('#facilityTypeEditFormDialog').dialog('open');
    },
    saveFacilityType: function() {
        if (quickClaim.ajaxLocked) {
            return;
        }

        var data = $('#facilityTypeEditForm').serialize();

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: facilityTypeForm.actions.save,
            data: data,
            success: function(rs) {
                if (rs.hasOwnProperty('success')) {
                    $('#facilityTypeEditFormDialog').dialog('close');
                    $('#facilityTypesTable').trigger('pageSet.pager');

                    quickClaim.showAjaxMessage(facilityTypeList.messageID, 'success', null);
                }
                else if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#facilityTypesErrorDiv', '#facility_type', true);
                    var message = 'The form contains invalid data.';

                    quickClaim.showAjaxMessage(facilityTypeForm.messageID, 'error', message);
                    quickClaim.showAjaxMessage(facilityTypeList.messageID, 'error', message);
                }
            },
            beforeSend: function(rs) {
                quickClaim.ajaxLocked = true;
                var message = 'Saving Facility Type';

                quickClaim.showAjaxMessage(facilityTypeForm.messageID, 'spin', message);
                quickClaim.showAjaxMessage(facilityTypeList.messageID, 'spin', message);
            },
            complete: function(rs) {
                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
            },
            error: function(rs) {
                quickClaim.ajaxLocked = false;
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                quickClaim.showAjaxMessage(facilityTypeList.messageID, 'error', message);
                quickClaim.showAjaxMessage(facilityTypeForm.messageID, 'error', message);
                quickClaim.hideOverlayMessage();
            }
        });
    },
    clearForm: function() {
        var $form = $('#facilityTypeEditForm');

        $('#facilityTypesTable').find('tr.current_edit').removeClass('current_edit');
        $form.find('#facilityTypesErrorDiv').text('').hide();
        $form.find('label.error').remove();
        $form.find('.error').removeClass('error');
        $form.clearForm();

        quickClaim.hideAjaxMessage('facilityTypesMessages');
        quickClaim.hideAjaxMessage('facilityTypeEditMessages');
    },
    fillForm: function(data) {
        this.clearForm();

        $('#facilityTypesTable tr#facilityTypeRow_' + data.id).addClass('current_edit');

        var prefix = '#facility_type_';

        for (var key in data) {
            if ($(prefix + key)) {
                $(prefix + key).val(data[key]);
            }
        }
        quickClaim.focusTopField();
    }
};
