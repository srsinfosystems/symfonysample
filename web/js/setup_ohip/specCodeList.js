var specCodeList = {
    actions: {
        specCodeTable: null
    },
    // TODO: redo messaging system in javascript
    messages: {
        error: 'specCodeErrorIcon',
        spinner: 'specCodeSpinner',
        checkmark: 'specCodeCheckmark',
        text: 'specCodeMessageText',
        spinnerHtml: '',
        checkmarkHtml: '',
        errorHtml: ''
    },
    specCodes: {},

    initialized: false,
    pagerColumns: {},
    paginationSize: 25,
    paginationSort: [],

    initialize: function () {
        if (!specCodeList.initialized) {
            $('#specCodeSearchButton')
                    .button()
                    .click(function () {
                        
                $("#specCodeTable thead tr:eq(1)").remove();
                        $('#specCodeTable').trigger('pageSet.pager');
                    });

            $('#specCodeResetButton')
                    .button()
                    .click(function () {
                        $('#specCodeSearchForm').clearForm();
                    });

            specCodeList.initialized = true;
            specCodeList.initializePager();
        }
    },
    initializePager: function () {
        var $table = $('#specCodeTable');
        $table.find('thead th.actions').addClass('sorter-false');
        $table.find('thead th:last').append(quickClaim.tablesorterTooltip);
        $table.tablesorter({
            theme: 'blue',
            headerTemplate: '{content} {icon}',
            widgets: ['selectRow', 'zebra', 'pager'],
            sortList: specCodeList.paginationSort
        });
        $table.find('.sortTip').tooltip();

        $table.tablesorterPager({
            container: $('.specCodePager'),
            size: specCodeList.paginationSize,
            cssPageSize: '.pagesize',
            ajaxUrl: specCodeList.actions.specCodeTable + '?{sortList:sort}&pageNumber={page}&size={size}',
            customAjaxUrl: function (table, url) {
                return url + '&' + $('#specCodeSearchForm').serialize();
            },
            ajaxObject: {
                dataType: 'json',
                complete: function () {
                    quickClaim.ajaxLocked = false;
                    $('#' + specCodeList.messages.spinner).hide();

                    for (var a in specCodeList.specCodes) {
                        if (specCodeList.specCodes[a].hasOwnProperty('actions') && specCodeList.specCodes[a].actions.hasOwnProperty('edit')) {
                            $('tr#specCodeRow_' + specCodeList.specCodes[a].id).click(function () {
                                specCodeList.handleAction($(this).prop('id'));
                            });
                        }
                    }
                    
                    var tableId = 'specCodeTable';
                    global_js.customFooterDataTable(tableId, true);
                    
                },
                beforeSend: function () {
                    $("#specCodeTable").dataTable().fnDestroy();
                    $("#specCodeTable thead tr:eq(1)").remove();
                    quickClaim.ajaxLocked = true;
                    $('#specCodeMessages').find('.icons div').hide();
                    $('#' + specCodeList.messages.spinner).show();
                    $('#' + specCodeList.messages.text).text('Requesting Specialty Codes').show();
                },
                error: function () {
                    $('#' + specCodeList.messages.error).show();
                    $('#' + specCodeList.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            },
            ajaxProcessing: function (rs) {
                quickClaim.hideOverlayMessage(1);

                $('#' + specCodeList.messages.text).text('Received Error Codes list.').show();
                $('#' + specCodeList.messages.checkmark).show();

                if (rs.hasOwnProperty('batchJobs')) {
                    specCodeList.displayBatchJobDetails(rs['batchJobs']);
                }

                if (rs.hasOwnProperty('data')) {
                    specCodeList.pagerColumns = rs.headers;
                    specCodeList.specCodes = {};
                    var data = rs.data;
                    var rows = '';

                    for (var a in data)
                    {
                        specCodeList.specCodes[data[a].id] = data[a];
                        rows += '<tr id="specCodeRow_' + data[a].id + '">';
                        rows += specCodeList.getSpecCodeTableRow(data[a]);
                        rows += '</tr>' + "\n";
                    }

                    return [rs.count, $(rows)];
                }
            }
        });
    },

    displayBatchJobDetails: function (data) {
        var $div = $('.batchJobStatusDiv');

        if (data['job_count'] == 0) {
            $div.hide();
            return;
        }

        $('.batchJobStatusDiv .batchCount').text(data['job_count']);
        $('.batchJobStatusDiv ul li').remove();

        for (var a in data['jobs']) {
            var li = '<li><b>' + data['jobs'][a]['document_name'] + ':</b> ' + data['jobs'][a].status + '</li>';

            $('.batchJobStatusDiv ul').append($(li));
        }

        $div.show();
    },
    getSpecCodeTableRow: function (data) {
        var rs = '';

        for (var b in specCodeList.pagerColumns)
        {
            if (b == 'description') {
                rs += '<td class="' + b + '">' + data[b] + '</td>';
            } else if (b != 'actions') {
                rs += '<td class="' + b + '">' + data[b] + '</td>';
            }
        }
        return rs;
    },
    handleAction: function (buttonID) {
        buttonID = buttonID.replace('specCodeEdit_', '');
        var id = buttonID.substring(buttonID.indexOf('_') + 1, buttonID.length);

        specCodeForm.editSpecCode(specCodeList.specCodes[id]);
    }
};