var specCodeUploadForm = {
    actions: {
        upload: null
    },
    messageID: 'specCodeUploadMessages',
    parent: 'specCodeList',

    initialize: function () {
        $('#uploadSpecCodeFileButton')
            .button()
            .click(function () {
                $('#specCodeFileUploadDialog').dialog('open');
            });

        $('#specCodeUploadButton')
            .button()
            .click(function () {
                specCodeUploadForm.submitForm();
            });

        $('#specCodeUploadCloseDialogButton')
            .button()
            .click(function () {
                $('#specCodeFileUploadDialog').dialog('close');
            });

        $('#specCodeFileUploadDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 400,
            modal: true,
            width: 800
        });
    },

    showDialog: function () {
        $('#specCodeFileUploadDialog').dialog('open');
    },

    submitForm: function () {
        var formData = new FormData($("#specCodeUploadForm")[0]);
        formData.append('parent', specCodeUploadForm.parent);

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: specCodeUploadForm.actions.upload,
            data: formData,

            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function (rs) {
                if (rs.hasOwnProperty('success')) {
                    $('#specCodeFileUploadDialog').dialog('close');

                    if (specCodeUploadForm.parent == 'specCodeList') {
                    }
                    else if (specCodeUploadForm.parent == 'import') {
                        quickClaim.hideAjaxMessage(dataImport.messageID);
                    }
                    quickClaim.hideAjaxMessage(specCodeUploadForm.messageID);
                }
                else if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#specCodeUploadErrorDiv', '#spec_codes_upload', true);

                    var errorMessage = 'The form contains invalid data';
                    quickClaim.showAjaxMessage(specCodeUploadForm.messageID, 'error', errorMessage);
                    if (specCodeUploadForm.parent == 'specCodeList') {
                    }
                    else if (specCodeUploadForm.parent == 'import') {
                        quickClaim.showAjaxMessage(dataImport.messageID, 'error', errorMessage);
                    }
                }

                if (rs.hasOwnProperty('batchJobs') && specCodeUploadForm.parent == 'specCodeList') {
                    specCodeList.displayBatchJobDetails(rs.batchJobs);
                }

                if (rs.hasOwnProperty('importData') && specCodeUploadForm.parent == 'import') {
                    $('#tabs').tabs('option', 'active', 0);
                    for (var a in rs['importData']) {
                        dataImport.loadTableRow(rs['importData'][a]);
                    }
                }
            },
            beforeSend: function () {
                var message = 'Uploading .XLS File';
                quickClaim.ajaxLocked = true;

                quickClaim.showAjaxMessage(specCodeUploadForm.messageID, 'spin', message);
                if (specCodeUploadForm.parent == 'specCodeList') {
                    $('#specCodeMessages').find('.icons div').hide();
                    $('#' + specCodeList.messages.spinner).show();
                    $('#' + specCodeList.messages.text).text(message).show();
                }
                else if (specCodeUploadForm.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'spin', message);
                }
                quickClaim.showOverlayMessage(message);
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
            },
            error: function () {
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';
                quickClaim.ajaxLocked = false;

                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(specCodeUploadForm.messageID, 'error', message);
                if (specCodeUploadForm.parent == 'specCodeList') {
                    $('#' + specCodeList.messages.error).show();
                    $('#' + specCodeList.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
                else if (specCodeUploadForm.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'error', message);
                }
            }

        });
    }
};