var specCodeForm = {
	actions: {
		save: null
	},
	messages: {
		error: 'specCodeEditErrorIcon',
		spinner: 'specCodeEditSpinner',
		checkmark: 'specCodeEditCheckmark',
		text: 'specCodeEditMessageText',
		spinnerHtml: '',
		checkmarkHtml: '',
		errorHtml: ''
	},
		
	initialize: function() {
		$('#createSpecCodeButton').button();
		$('#createSpecCodeButton').click(function() {
			specCodeForm.editSpecCode({});
		});
		
		$('#specCodeSaveButton').button();
		$('#specCodeSaveButton').click(function() {
			specCodeForm.saveSpecCode();
		});
		
		$('#specCodeCloseDialogButton').button();
		$('#specCodeCloseDialogButton').click(function() {
			$('#specCodeEditFormDialog').dialog('close');
		});
		
		$('#specCodeEditFormDialog').dialog({
			closeOnEscape: true,
			autoOpen: false,
			height: 400,
			modal: true,
			width:  800,
			close: function() {
				specCodeForm.clearForm();
			}
		});
	},
	
	editSpecCode: function(data) {
		specCodeForm.fillForm(data);
		$('#specCodeEditFormDialog').dialog('open');
	},
	saveSpecCode: function() {
		if (quickClaim.ajaxLocked) {
			return;
		}

		var data = $('#specCodeEditForm').serialize();
		
 		$.ajax({
			type: 'post',
			dataType: 'json',
			url: specCodeForm.actions.save,
			data: data,
			success: function(rs) {
				if (rs.hasOwnProperty('success')) {
					$('#specCodeEditFormDialog').dialog('close');
					$('#specCodeTable').trigger('pageSet.pager');
				}
				else if (rs.hasOwnProperty('errors')) {
					quickClaim.showFormErrors(rs.errors, '#specCodeErrorDiv', '#expl_code', true);

					$('#' + specCodeForm.messages.error).show();
					$('#' + specCodeForm.messages.text).text('The form contains invalid data.').show();
				}
			},
			beforeSend: function(rs) {
				quickClaim.ajaxLocked = true;
				
				$('#specCodeMessages .icons div').hide();
				$('#' + specCodeList.messages.spinner).show();
				$('#' + specCodeList.messages.text).text('Saving Specialty Code').show();
				
				$('#specCodeEditMessages .icons div').hide();
				$('#' + specCodeForm.messages.spinner).show();
				$('#' + specCodeForm.messages.text).text('Saving Specialty Code').show();
			},
			complete: function(rs) {
				quickClaim.ajaxLocked = false;
				
				$('#' + specCodeList.messages.spinner).hide();
				$('#' + specCodeForm.messages.spinner).hide();
			},
			error: function(rs) {
				quickClaim.ajaxLocked = false;

				$('#' + specCodeForm.messages.error).show();
				$('#' + specCodeForm.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
				
				$('#' + specCodeList.messages.error).show();
				$('#' + specCodeList.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
			}
		});
	},
	clearForm: function() {
		$('#specCodeTable tr.current_edit').removeClass('current_edit');
		$('#specCodeEditForm #specCodeErrorDiv').text('').hide();
		$('#specCodeEditForm label.error').remove();
		$('#specCodeEditForm .error').removeClass('error');

		$('#specCodeEditMessages .icons div').hide();
		$('#' + specCodeForm.messages.text).text('').hide();
		
		$('#specCodeEditForm').clearForm();
	},
	fillForm: function(data) {
		this.clearForm();
		
		$('#specCodeTable tr#specCodeRow_' + data.id).addClass('current_edit');

		var prefix = '#spec_code_';

		for (var key in data) {
			if (key == 'active') {
				$(prefix + key).prop('checked', (data[key] == 'Y'));
			}
			else if ($(prefix + key)) {
				$(prefix + key).val(data[key]);
			}
		}
		quickClaim.focusTopField();
	}
}
