var facilitiesList = {
    actions: {
        facilitiesTable: null
    },
    facilities: {},
    isDialog: false,
    dialogParent: 'claim',

    initialized: false,
    pagerColumns: {},
    paginationSize: 25,
    paginationSort: [],
    messageID: 'facilitiesMessages',

    initialize: function () {
        if (!facilitiesList.initialized) {
            $('#facilitiesSearchButton')
                    .button()
                    .click(function () {
                        $("#facilitiesTable").dataTable().fnDestroy();
                        $("#facilitiesTable thead tr:eq(1)").remove();
                
                        $('#facilitiesTable').trigger('pageSet.pager');
                    });

            $('#facilitiesResetButton')
                    .button()
                    .click(function () {
                        
                        $('#facilitiesSearchForm').clearForm();
                        $("#facilitiesSearchForm select").trigger('change');
                        $("#facilitiesSearchButton").trigger('click');
                    });
            facilitiesList.initialized = true;
            facilitiesList.initializePager();
        }
    },
    initializePager: function () {
        var $table = $('#facilitiesTable');

        $table.find('thead th:last').append(quickClaim.tablesorterTooltip);
        $table.tablesorter({
            theme: 'blue',
            headerTemplate: '{content} {icon}',
            widgets: ['selectRow', 'zebra', 'pager'],
            sortList: facilitiesList.paginationSort
        });
        $table.find('.sortTip').tooltip();

        $table.tablesorterPager({
            container: $('.facilitiesPager'),
            size: facilitiesList.paginationSize,
            cssPageSize: '.pagesize',
            ajaxUrl: facilitiesList.actions.facilitiesTable + '?{sortList:sort}&pageNumber={page}&size={size}',
            customAjaxUrl: function (table, url) {
                return url + '&' + $('#facilitiesSearchForm').serialize();
            },
            ajaxObject: {
                dataType: 'json',
                complete: function () {
                    quickClaim.ajaxLocked = false;

                    for (var a in facilitiesList.facilities) {
                        $('tr#facilityRow_' + facilitiesList.facilities[a].id).click(function () {
                            facilitiesList.handleAction($(this).prop('id'));
                            $("#claim_admit_date").show();
                        });
                    }

                    var tableId = 'facilitiesTable';
                    global_js.customFooterDataTableWithOptions(tableId, true);
                    var table = $("#"+tableId).DataTable();
             table
                   .on('key-focus', function (e, datatable, cell) {
                      datatable.rows().deselect();
                      datatable.row( cell.index().row ).select();
                    });
                    table.row(0).select();
                    table.cell( ':eq(0)' ).focus();
                    
        Mousetrap.bind('enter', function(e) {
            console.log('enter clicked');
            global_js.enter = false;
            $("#"+$("#"+tableId + ' tbody .selected').attr('id')).trigger('click');
        })
                },
                beforeSend: function () {
                    $("#facilitiesTable").dataTable().fnDestroy();
                    $("#facilitiesTable thead tr:eq(1)").remove();
                    quickClaim.ajaxLocked = true;

                    var message = 'Requesting Facilities';
                    quickClaim.showAjaxMessage(facilitiesList.messageID, 'spin', message);
                    quickClaim.showOverlayMessage(message);
                },
                error: function () {
                    quickClaim.ajaxLocked = false;

                    var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                    quickClaim.hideOverlayMessage();
                    quickClaim.showAjaxMessage(facilitiesList.messageID, 'error', message);
                }
            },
            ajaxProcessing: function (rs) {
                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(facilitiesList.messageID, 'success', 'Received Facilities list');

                if (rs.hasOwnProperty('batchJobs')) {
                    facilitiesList.displayBatchJobDetails(rs.batchJobs);
                }

                if (rs.hasOwnProperty('data')) {
                    facilitiesList.pagerColumns = rs.headers;
                    facilitiesList.facilities = {};
                    var data = rs.data;
                    var rows = '';

                    for (var a in data)
                    {
                        facilitiesList.facilities[data[a].id] = data[a];
                        rows += '<tr id="facilityRow_' + data[a].id + '">';
                        rows += facilitiesList.getFacilityTableRow(data[a], data[a].id);
                        rows += '</tr>' + "\n";
                    }

                    return [rs.count, $(rows)];
                }
            }
        });
    },

    displayBatchJobDetails: function (data) {
        if (data.job_count == 0) {
            $('.batchJobStatusDiv').hide();
            return;
        }

        $('.batchJobStatusDiv .batchCount').text(data.job_count);
        $('.batchJobStatusDiv ul li').remove();

        for (var a in data.jobs) {
            var li = '<li><b>' + data.jobs[a].document_name + ':</b> ' + data.jobs[a].status + '</li>';

            $('.batchJobStatusDiv ul').append($(li));
        }

        $('.batchJobStatusDiv').show();
    },
    getFacilityTableRow: function (data, id) {
        var rs = '';

        for (var b in facilitiesList.pagerColumns)
        {
            rs += '<td class="' + b + '">' + data[b] + '</td>';
        }
        return rs;
    },
    handleAction: function (buttonID) {
        buttonID = buttonID.replace('facilityRow_', '');
        var id = buttonID.substring(buttonID.indexOf('_') + 1, buttonID.length);

        if (!facilitiesList.isDialog) {
            facilityForm.editFacility(facilitiesList.facilities[id]);
        } else if (facilitiesList.dialogParent == 'patient_edit') {

            var fac = facilitiesList.facilities[id];
            patient_edit.facility.cache[patient_edit.facility.cache.length] = fac['toString'];
            patient_edit.facility.data[fac['facility_master_number']] = {
                facility_num: fac['facility_master_number'],
                facility_id: fac['id'],
                toString: fac['toString']
            };
            patient_edit.facility.fillInFacility(fac['facility_master_number']);
        } else {
            var fac = facilitiesList.facilities[id];
            claim.facility.cache[claim.facility.cache.length] = fac['toString'];
            claim.facility.data[fac['facility_master_number']] = {
                facility_num: fac['facility_master_number'],
                facility_id: fac['id'],
                toString: fac['toString']
            };
            claim.facility.fillInFacility(fac['facility_master_number']);
        }
    }
};