var explCodesForm = {
	actions: {
		save: null
	},
    // TODO: phase out old messages format
	messages: {
		error: 'explCodeEditErrorIcon',
		spinner: 'explCodeEditSpinner',
		checkmark: 'explCodeEditCheckmark',
		text: 'explCodeEditMessageText',
		spinnerHtml: '',
		checkmarkHtml: '',
		errorHtml: ''
	},
    parent: 'explCodesList',
		
	initialize: function() {
        $('#createExplCodeButton')
            .button()
            .click(function () {
			explCodesForm.editExplCode({});
		});

        $('#explCodeSaveButton')
            .button()
            .click(function () {
			explCodesForm.saveExplCode();
		});

        $('#explCodeCloseDialogButton')
            .button()
            .click(function () {
			$('#explCodeEditFormDialog').dialog('close');
		});
		
		$('#explCodeEditFormDialog').dialog({
			closeOnEscape: true,
			autoOpen: false,
			height: 400,
			modal: true,
			width:  800,
			close: function() {
				explCodesForm.clearForm();
			}
		});
	},
	
	editExplCode: function(data) {
		explCodesForm.fillForm(data);
		$('#explCodeEditFormDialog').dialog('open');
	},
	saveExplCode: function() {
		if (quickClaim.ajaxLocked) {
			return;
		}

		var data = $('#explCodeEditForm').serialize();
		
 		$.ajax({
			type: 'post',
			dataType: 'json',
			url: explCodesForm.actions.save,
			data: data,
			success: function(rs) {
				if (rs.hasOwnProperty('success')) {
					$('#explCodeEditFormDialog').dialog('close');
					$('#explCodeTable').trigger('pageSet.pager');
				}
				else if (rs.hasOwnProperty('errors')) {
					quickClaim.showFormErrors(rs.errors, '#explCodesErrorDiv', '#expl_code', true);

					$('#' + explCodesForm.messages.error).show();
					$('#' + explCodesForm.messages.text).text('The form contains invalid data.').show();
				}
			},
            beforeSend: function () {
                var message = 'Saving Error / Expl Code';
				quickClaim.ajaxLocked = true;

                if (explCodesForm.parent == 'explCodesList') {
                    quickClaim.showAjaxMessage(explCodesList.messageID, 'spin', message);
                }

                $('#explCodeEditMessages').find('.icons div').hide();
				$('#' + explCodesForm.messages.spinner).show();
                $('#' + explCodesForm.messages.text).text(message).show();
			},
            complete: function () {
				quickClaim.ajaxLocked = false;

                if (explCodesForm.parent == 'explCodesList') {
                    quickClaim.hideAjaxMessage(explCodesList.messageID);
                }

				$('#' + explCodesForm.messages.spinner).hide();
			},
            error: function () {
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';
				quickClaim.ajaxLocked = false;

				$('#' + explCodesForm.messages.error).show();
                $('#' + explCodesForm.messages.text).text(message).show();

                if (explCodesForm.parent == 'explCodesList') {
                    quickClaim.showAjaxMessage(explCodesList.messageID, 'error', message);
                }
			}
		});
	},
	clearForm: function() {
        var $form = $('#explCodeEditForm');

        $('#explCodeTable').find('tr.current_edit').removeClass('current_edit');
        $form.find('#explCodesErrorDiv').text('').hide();
        $form.find('label.error').remove();
        $form.find('.error').removeClass('error');

        $('#explCodeEditMessages').find('.icons div').hide();
		$('#' + explCodesForm.messages.text).text('').hide();

        $form.clearForm();
	},
	fillForm: function(data) {
		this.clearForm();

        $('#explCodeTable').find('tr#explCodeRow_' + data.id).addClass('current_edit');

		var prefix = '#expl_code_';

		for (var key in data) {
			if ($(prefix + key)) {
				$(prefix + key).val(data[key]);
			}
		}
		quickClaim.focusTopField();
	}
};
