var facilitiesUploadForm = {
    actions: {
        upload: null
    },
    messageID: 'facilityUploadMessages',
    parent: 'facilitiesList',

    initialize: function() {
        $('#uploadFacilityFileButton')
            .button()
            .click(function() {
                $('#facilityFileUploadDialog').dialog('open');
            });

        $('#facilityUploadButton')
            .button()
            .click(function() {
                facilitiesUploadForm.submitForm();
            });

        $('#facilityUploadCloseDialogButton')
            .button()
            .click(function() {
                $('#facilityFileUploadDialog').dialog('close');
            });

        $('#facilityFileUploadDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 400,
            modal: true,
            width:  800
        });
    },

    showDialog: function () {
        $('#facilityFileUploadDialog').dialog('open');
    },

    submitForm: function() {

        var formData = new FormData($("#facilityUploadForm")[0]);
        formData.append('parent', facilitiesUploadForm.parent);

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: facilitiesUploadForm.actions.upload,
            data: formData,

            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function(rs) {
                if (rs.hasOwnProperty('success')) {
                    $('#facilityFileUploadDialog').dialog('close');

                    if (facilitiesUploadForm.parent == 'facilitiesList') {
                        quickClaim.hideAjaxMessage(facilitiesList.messageID);
                    }
                    else if (facilitiesUploadForm.parent == 'import') {
                        quickClaim.hideAjaxMessage(dataImport.messageID);
                    }
                    quickClaim.hideAjaxMessage(facilitiesUploadForm.messageID);
                }
                else if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#facilitiesUploadErrorDiv', '#facility_upload', true);

                    var errorMessage = 'The form contains invalid data';
                    quickClaim.showAjaxMessage(facilitiesUploadForm.messageID, 'error', errorMessage);
                    if (facilitiesUploadForm.parent == 'facilitiesList') {
                        quickClaim.showAjaxMessage(facilitiesList.messageID, 'error', errorMessage);
                    }
                    else if (facilitiesUploadForm.parent == 'import') {
                        quickClaim.showAjaxMessage(dataImport.messageID, 'error', errorMessage);
                    }
                }

                if (rs.hasOwnProperty('batchJobs') && facilitiesUploadForm.parent == 'facilitiesList') {
                    facilitiesList.displayBatchJobDetails(rs.batchJobs);
                }

                if (rs.hasOwnProperty('importData') && facilitiesUploadForm.parent == 'import') {
                    $('#tabs').tabs('option', 'active', 0);
                    for (var a in rs['importData']) {
                        dataImport.loadTableRow(rs['importData'][a]);
                    }
                }
            },
            beforeSend: function () {
                var message = 'Uploading .XLS File';
                quickClaim.ajaxLocked = true;

                quickClaim.showAjaxMessage(facilitiesUploadForm.messageID, 'spin', message);
                if (facilitiesUploadForm.parent == 'facilitiesList') {
                    quickClaim.showAjaxMessage(facilitiesList.messageID, 'spin', message);
                }
                else if (facilitiesUploadForm.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'spin', message);
                }
                quickClaim.showOverlayMessage(message);
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
            },
            error: function () {
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';
                quickClaim.ajaxLocked = false;

                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(facilitiesUploadForm.messageID, 'error', message);
                if (facilitiesUploadForm.parent == 'facilitiesList') {
                    quickClaim.showAjaxMessage(facilitiesList.messageID, 'error', message);
                }
                else if (facilitiesUploadForm.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'error', message);
                }
            }
        });
    }
};