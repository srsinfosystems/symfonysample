var errorReports = {
    actions: {
        claimPDF: null,
        editClaim: null,
        detailsClaim: null,
        detailsCSV: null,
        detailsPDF: null,
        downloadFile: null,
        errorReportTable: null,
        errorReportDetailsTable: null,
        patientProfile: null
    },
    messages: {
        error: 'errorReportErrorIcon',
        spinner: 'errorReportSpinner',
        checkmark: 'errorReportCheckmark',
        text: 'errorReportMessageText',
        spinnerHtml: '',
        checkmarkHtml: '',
        errorHtml: ''
    },
    tableId: '',
    pagerColumns: {},
    paginationSize: 25,
    paginationSort: [],
    listType: null,
    reconcileID: null,

    archiveIDs: null,

    details: {
        initialized: false,
        errorReportActions: {},
        pagerColumns: {},
        editableClaimIDs: {},
        patientIDs: {},
        explCodes: {},

        initialize: function () {

            errorReports.details.initialized = true;
            errorReports.details.initializePager();

            $('#errorReportDetailsSelectAll').click(function () {
                var checked = $(this).prop('checked');
                $('#errorReportDetailsTable tbody input[type=checkbox]').prop('checked', checked);
            });

            $('#errorReportDetailsEditClaims').button();
            $('#errorReportDetailsEditClaims').click(function () {
                errorReports.details.editSelectedClaims();
            })
        },
        initializePager: function () {
            $('#printButton').button();
            $('#printButton').click(function (ev) {
                $('#errorReportDetailsTable').printElement({
                    overrideElementCSS: ['css/jquery-tablesorter1.css', 'css/jquery-tablesorter/jquery.tablesorter.pager.css', 'css/jquery-tablesorter/theme.blue.css']
                });
            });

            $('#errorReportDetailsTable thead th.actions').addClass('sorter-false');
            $('#errorReportDetailsTable thead th.hcn').addClass('sorter-false');
            $('#errorReportDetailsTable thead th.select').addClass('sorter-false').addClass('actions');
            $('#errorReportDetailsTable thead th.still_rejected').addClass('sorter-false');

            $('#errorReportDetailsTable thead th:last').append(quickClaim.tablesorterTooltip);
            $('#errorReportDetailsTable').tablesorter({
                theme: 'blue',
                headerTemplate: '{content} {icon}',
                widgets: ['selectRow', 'zebra', 'pager'],
                sortList: errorReports.paginationSort
            });
            $('#errorReportDetailsTable .sortTip').tooltip();
            $('#errorReportDetailsTable').tablesorterPager({
                container: $('.errorReportDetailsPager'),
                size: errorReports.paginationSize,
                cssPageSize: '.pagesize',
                ajaxUrl: errorReports.actions.errorReportDetailsTable + '?{sortList:sort}&pageNumber={page}&size={size}',
                customAjaxUrl: function (table, url) {
                    return url + '&id=' + errorReports.reconcileID + '&listType=' + errorReports.listType;
                },
                ajaxObject: {
                    dataType: 'json',
                    complete: function () {
                        quickClaim.ajaxLocked = false;
                        $('#' + errorReports.messages.spinner).hide();
                        $('#errorReportDetailsTable').tooltip();

                        global_js.customFooterDataTableWithOptions('errorReportDetailsTable', false, ['pdf'], false);
                        $(".testtesttest").hover(function () {
                            $(this).find('span').show();
                        }, function () {
                            $(this).find('span').hide();
                        });
                    },
                    beforeSend: function () {
                        global_js.datatableDinit('errorReportDetailsTable');
                        $("#errorReportDetailsTable").dataTable().fnDestroy();
                        quickClaim.ajaxLocked = true;
                    },
                    error: function () {
//						$('#' + errorReports.messages.error).show();
//						$('#' + errorReports.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                    }
                },
                ajaxProcessing: function (rs) {
                    quickClaim.hideOverlayMessage(1);
//					$('#' + errorReports.messages.text).text('Received Error Report List.').show();
//					$('#' + errorReports.messages.checkmark).show();

                    errorReports.details.explCodes = rs.errorCodes;
                    errorReports.details.editableClaimIDs = rs.editableClaimIDs;

                    $('#errorReportDetailsEditClaims').hide();
                    for (var a in errorReports.details.editableClaimIDs) {
                        $('#errorReportDetailsEditClaims').show();
                        break;
                    }

                    if (rs.hasOwnProperty('data')) {
                        errorReports.details.errorReportActions = {};
                        errorReports.details.patientIDs = rs.patientIDs;
                        errorReports.details.pagerColumns = rs.headers;

                        var data = rs.data;
                        var rows = '';
                        var includeSelect = rs.hasOwnProperty('include_select') && rs.include_select;

                        for (var a in data)
                        {
                            rows += '<tr id="' + data[a].item_id + '">';
                            rows += errorReports.details.getReportTableRow(data[a], data[a].item_id, includeSelect);
                            rows += '</tr>' + "\n";
                        }

                        return [rs.count, $(rows)];
                    }
                }
            });
        },
        editSelectedClaims: function () {
            var c = 0;

            $('#errorReportDetailsTable tbody input[type=checkbox]:checked').each(function () {
                var id = $(this).prop('id').replace('errorDetailsReportSelect_', '');
                if (!errorReports.details.editableClaimIDs.hasOwnProperty(id)) {
                    $(this).prop('checked', false);
                } else {
                    c++;
                }
            });

            if (c) {
                $('#errorReportDetailsTableForm').submit();
            }
        },
        getReportTableRow: function (data, id, includeSelect) {
            var rs = '';
            errorReports.details.errorReportActions[id] = data['actions'];

            if (includeSelect) {
                rs += '<td class="select">';
                if (data.item_id) {
                    
                    rs += '<label class="CHKcontainer"><input type="checkbox" id="errorDetailsReportSelect_' + data.claim_id  + '" name="claim[' + data.claim_id + ']" value="' + data['claim_id'] + '" checked="checked" /><span class="CHKcheckmark"></span></label>';
                }
                rs += '</td>';
            }

            for (var b in errorReports.details.pagerColumns)
            {
                if (b == 'actions') {
                    rs += '<td class="actions">';
                    if (data.item_id) {
                        rs += '<img class="actionsButton" id="errorReportDetailActions_' + data.claim_id + '" src="' + errorReports.contextMenu.menuHtml + '" />';
                    }
                    rs += '</td>';
                } else if (b == 'error_codes') {
                    rs += '<td class="' + b + '">';
                    rs += '<ul>';

                    for (var c in data[b]) {
                        var desc = errorReports.details.getErrorCodeDescription(data[b][c]);
                        //rs += `<li><a  href="javascript:void(0)" title="Tooltip on top">
  //Tooltip on top
//</a></li>`;
                        rs += '<li class="testtesttest"><span style="display:none;background: #000000;position: absolute;padding: 5px;margin-top: -30px;border-radius: 5px;color: #fff;right: 16%;">' + desc + '</span>' + data[b][c] + '</li>';

                    }

                    rs += '</ul>';
                    rs += '</td>';
                } else {
//                    if(b != 'HX8Lines'){
                    rs += '<td class="' + b + '">' + data[b] + '</td>';
//                }
                }
            }
            return rs;
        },
        getErrorCodeDescription: function (errorCode) {
            if (errorReports.details.explCodes.hasOwnProperty(errorCode)) {
                return errorCode + ':  ' + errorReports.details.explCodes[errorCode];
            }
            return errorCode + ':  ' + 'Unknown Error Code';
        }
    },

    initialize: function () {
        $('#errorReportDetailsDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: $(window).height() - 200,
            modal: true,
            width: '80%',
            close: function () {
                $('#errorReportDetailsTable tbody tr').remove();
            }
        });
        // errorReports.initializePager();
        errorReports.initDataTable();
        sk.datepicker('#error_report_start_date');

        $('#error_report_start_date').on('changeDate', function (e) {
            $('#error_report_end_date').skDP('setStartDate', e.date);
        });
        sk.datepicker('#error_report_end_date');

        $('#error_report_end_date').on('changeDate', function (e) {
            $('#error_report_start_date').skDP('setEndDate', e.date);
        });

        $('#errorReportSearchButton').click(function () {
            var tableInst = $("#" + errorReports.tableId).DataTable();
            tableInst.clear().draw();
            errorReports.update_table(tableInst);
        });

        $('#errorReportResetButton').click(function () {
            errorReports.clearForm();
        });

        $('#archiveErrorReportButton').click(function () {
            errorReports.archive();
        });

        $('#archiveSingleErrorReportButton').button();
        $('#archiveSingleErrorReportButton').click(function () {
            errorReports.archive(errorReports.reconcileID);
        });

        $('#errorReportArchiveDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: $(window).height() - 200,
            modal: true,
            width: '80%'
        });

        $('#errorReportArchiveAreYouSure').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 200,
            modal: true,
            width: 400,
            buttons: [
                {
                    text: "Yes",
                    "class": 'btn btn-success',
                    click: function () {
                        errorReports.sendArchiveAjax();
                        $('#errorReportArchiveAreYouSure').dialog('close');
                    }
                },
                {
                    text: "No",
                    "class": 'btn btn-danger',
                    click: function () {
                        $('#errorReportArchiveAreYouSure').dialog('close');
                        $('#errorReportArchiveDialog').dialog('close');
                    }
                }
            ]
        });
    },
    initDataTable: function () {
        // thead
        var colHeader = [];
        for (var i in batch_error_report_header) {
            if (i == 'select') {
                colHeader.push({title: '<label class="CHKcontainer"><input type="checkbox" id="batchAckTableSelectAll" value="1" checked="checked" /><span class="CHKcheckmark"></span></label>', class: 'edit_checkbox'});
            } else {
                var arr = i.split('_');
                if ($.inArray('date', arr) != -1 || i == 'dob') {
                    colHeader.push({title: batch_error_report_header[i], class: i, sType: 'dateField', bSortable: true});
                } else {
                    colHeader.push({title: batch_error_report_header[i], class: i});
                }
            }
        }

        var tableId = 'errorReportTable';
        var table = global_js.dataTableWithLoader(tableId, false, [], false, colHeader);
        $(".dataTables_empty").addClass('text-center').removeClass('dataTables_empty');

        $("#errorReportTable tbody").on('click', 'tr', function () {
            if ($(this).find('input[type="checkbox"]').is(':checked')) {
                $(this).find('input[type="checkbox"]').prop('checked', false);
            } else {
                $(this).find('input[type="checkbox"]').prop('checked', true);
            }

        })

        errorReports.update_table(table);
        errorReports.tableId = tableId;
        table.on('page.dt', function () {
            setTimeout(function () {
                $("#" + tableId + " tbody .actionsButton").each(function () {
                    $(this).on('hover', function () {
                        var batchAckID = $(this).attr('id').replace('errorReportActions_', '');
                        errorReports.contextMenu.setActiveItems(batchAckID);
                    })
                })
            }, 1000)
        });
    },
    update_table: function (tableInst) {
        $.ajax({
            url: errorReports.actions.errorReportTable + '?pageNumber=1&size=1000&' + $('#errorReportForm').serialize(),
            type: 'GET',
            success: function (res) {
                global_js.dtLoader(errorReports.tableId);
                var obj = JSON.parse(res);

                if (obj.hasOwnProperty('data') && obj.data.length > 0) {
                    var allData = obj.data;
                    var dtData = [];
                    errorReports.errorReportActions = {};
                    for (var j in allData) {
                        dtData.push(errorReports.buildRow(allData[j]));
                    }
                    tableInst.rows.add(dtData);
                    tableInst.draw();
                    $(".selectRsltTbl").select2('destroy');
                    var aa = 0;
                    var apiDt = $("#" + errorReports.tableId).dataTable().api();
                    apiDt.columns().every(function () {
                        var column = this;
                        var columnText = $.trim($(column.header())[0].innerText);
                        if (aa != 0 && aa != 4) {
                            $("#" + errorReports.tableId + " thead tr:eq(1) th:eq(" + aa + ")").html('');

                            var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                    .appendTo($("#" + errorReports.tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                    .on('change', function () {
                                        var val = $(this).val();
                                        column.search(val ?  val : '', true, false).draw();
                                    });
                            var chkArr = [];
                            column.data().unique().sort().each(function (d, j) {
                                if(Array.isArray(d)){
                                    for ( var i in d){
                                        if($.inArray(d[i], chkArr) == -1 ) {
                                            chkArr.push(d[i]);
                                            select.append('<option value="' + d[i] + '">' + d[i] + '</option>');
                                        }
                                    }                                    
                                } else {
                                    if(d != '') {
                                        select.append('<option value="' + d + '">' + d + '</option>');
                                    }
                                }
                                
                            });
                        }

                           
                        aa++;
                    });
                    
                    $.fn.dataTableExt.oSort["dateField-desc"] = function (x, y) {
                        var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                        var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                        return (a.diff(b));
                    };

                    $.fn.dataTableExt.oSort["dateField-asc"] = function (x, y) {
                        var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                        var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                        return (b.diff(a));
                    }
                    $('#' + errorReports.tableId + ' .select2-arrow').hide();
                    setTimeout(function () {
                        
                        $("#" + errorReports.tableId + " thead tr:eq(1) select").select2({
                            placeholder: "Search",
                            allowClear: true,
                            dropdownAutoWidth: true,
                            width: '98%'
                        });
                        $('#' + errorReports.tableId + ' .select2-arrow').hide();
                        $("#" + errorReports.tableId + " tbody .actionsButton").each(function () {
                            $(this).on('hover', function () {
                                var batchAckID = $(this).attr('id').replace('errorReportActions_', '');
                                errorReports.contextMenu.setActiveItems(batchAckID);
                            })
                        })
                    }, 500);
                } else {
                    $(".dataTables_empty").addClass('text-center').removeClass('dataTables_empty');
                }
                global_js.initCheckbox(errorReports.tableId);
            },
            beforeSend: function () {
                $(".dataTables_empty").addClass('text-center').removeClass('dataTables_empty');
                global_js.dtLoader(errorReports.tableId, 'start');
            }
        })
    },
    buildRow: function (data) {
        errorReports.errorReportActions[data['id']] = data['actions'];
        var rowData = [];
        for (var i in batch_error_report_header) {
            if (i == 'select') {
                rowData.push('<label class="CHKcontainer"><input type="checkbox" data-file_name="' + data['file_name'] + '" data-batch_id="' + data['batch_id'] + '" data-batch_number="' + data['batch_number'] + '" id="errorReportSelect_' + data['id'] + '" value="1" checked="checked" /><span class="CHKcheckmark"></span></label>');
            } else if (i == 'actions') {
                var html = '<img class="actionsButton" id="errorReportActions_' + data['id'] + '" src="' + errorReports.contextMenu.menuHtml + '" />';
                rowData.push(html);
            } else {
                rowData.push(data[i]);
            }
        }
        return rowData;
    },

    clearForm: function () {
        $('#errorReportForm').clearForm();
        $("select").trigger('change');
        $("#errorReportSearchButton").trigger('click');
    },
    getDetails: function (id, listType)
    {
        var fileName = $('#errorReportSelect_' + id).attr('data-file_name');
        errorReports.reconcileID = id;
        errorReports.listType = listType;

        $('#errorReportTitle').text(fileName + ' -  ' + errorReports.getListTypeTitle(listType));

        if (!errorReports.details.initialized) {
            errorReports.details.initialize();
        } else {
            $('#errorReportDetailsTable').trigger('pageSet.pager');
        }

        $("#stage_track_pdf_option_header").val($("#errorReportDetailsDialog h3").text());
        $('#batchDetailsReportBatchNumber').text(' File Name ' + fileName);

        $('#errorReportDetailsDialog').dialog('open');
    },
    chkDialogIsOpen: function (id) {
        if ($('#' + id).dialog('isOpen')) {
            return true;
        }
        return false;
    },
    downloadPDF: function (id, listType) {

        if (errorReports.chkDialogIsOpen('errorReportDetailsDialog')) {
            if (quickClaim.ajaxLocked == false) {
                $("#errorReportDetailsTable_wrapper .buttons-pdf").trigger('click');
                $('#errorReportDetailsDialog').dialog("close");
                quickClaim.hideOverlayMessage(500);

            } else {
                setTimeout(function () {
                    errorReports.downloadPDF(id, listType);
                }, 500);
            }
        } else {
            quickClaim.showOverlayMessage('Downloading Batch Details PDF');
            errorReports.getDetails(id, listType);
            setTimeout(function () {
                errorReports.downloadPDF(id, listType);
            }, 500);
        }

        return false;
        errorReports.downloadErrorFile(id);

        hypeFileDownload.tokenName = "errorReportPDFDownloadToken";
        hypeFileDownload.setReturnFunction(errorReports.downloadComplete);
        var url = errorReports.actions.detailsPDF + '?id=' + id + '&errorReportPDFDownloadToken=' + hypeFileDownload.blockResubmit();

        $('#messages .icons div').hide();
        $('#' + moh.messages.spinner).show();
        $('#' + moh.messages.text).text('Downloading Error File PDF').show();
        quickClaim.showOverlayMessage('Downloading Error File PDF');

        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },

    getListTypeTitle: function (listType) {
        if (listType == 'all') {
            return 'All Records';
        }
        if (listType == 'hype') {
            return 'Hype Claims Only';
        }
        if (listType == 'non_hype') {
            return 'Non-Hype Claims Only';
        }
        if (listType == 'current') {
            return 'Currently Rejected Claims Only';
        }
    },
    collectIds: function (prefix, id) {
        var ids = new Array();
        if (!id) {

            $('#errorReportTable tbody input[type="checkbox"]:checked').each(function () {
                var reconcileID = $(this).attr('id').replace('errorReportSelect_', '');
                var actions = errorReports.errorReportActions[reconcileID];

                if (actions.hasOwnProperty(prefix) && actions[prefix]) {
                    ids[ids.length] = reconcileID;
                } else {
                    $(this).prop('checked', false);
                }
            });
        } else {
            ids[ids.length] = id;
        }

        return ids;
    },
    archive: function (id) {
        var ids = errorReports.collectIds('archive', id);

        if (!ids.length) {
            quickClaim.showOverlayMessage('No selected Error Reports can be archived.');
            setTimeout(function () {
                quickClaim.hideOverlayMessage();
            }, 2000);
            return;
        }

        $('#errorReportArchiveTable tbody tr').remove();
        for (var a = 0; a < ids.length; a++) {

            var fileName = $('#errorReportSelect_' + ids[a]).attr('data-file_name');

            var tr = '<tr id="archiveErrorReport_' + ids[a] + '">'
                    + '<td class="statuses">&nbsp;</td>'
                    + '<td class="fileName">' + fileName + '</td>'
                    + '<td class="message">&nbsp;</td>'
                    + '</tr>';

            $('#errorReportArchiveTable tbody').append($(tr));
        }
        $('#errorReportArchiveTable').trigger('update');

        errorReports.archiveIds = ids;
        $('#errorReportArchiveDialog').dialog('open');
        $('#errorReportArchiveAreYouSure').dialog('open');
    },
    sendArchiveAjax: function () {
        var sendIDs = new Array();
        var extraIDs = new Array();

        for (var a = 0; a < errorReports.archiveIds.length; a++) {
            if (a < 1) {
                sendIDs[sendIDs.length] = errorReports.archiveIds[a];

                $('#archiveErrorReport_' + errorReports.archiveIds[a] + ' td.message').text('Archiving Error Report');
                $('#archiveErrorReport_' + errorReports.archiveIds[a] + ' td.statuses').html($(errorReports.messages.spinnerHtml));
                $('#archiveErrorReport_' + errorReports.archiveIds[a]).addClass('currentlySending');
            } else {
                extraIDs[extraIDs.length] = errorReports.archiveIds[a];
            }
        }
        errorReports.archiveIds = extraIDs;

        $.ajax({
            type: 'post',
            dataType: 'json',
            data: 'ids=' + sendIDs.join(',') + '&mode=errorReport',
            url: errorReports.actions.archive,
            success: function (rs) {
                for (var a in rs.data) {
                    if (rs.data[a].actions.archive == '') {
                        $('#archiveErrorReport_' + a + ' .message').text('Archived Error Report');
                        $('#archiveErrorReport_' + a + ' .statuses').html(errorReports.messages.checkmarkHtml);
                    } else {
                        $('#archiveErrorReport_' + a + ' .statuses').html(errorReports.messages.errorHtml);
                    }
                    $('#archiveErrorReport_' + a).removeClass('currentlySending');
                }
                if (errorReports.archiveIds.length) {
                    errorReports.sendArchiveAjax();
                } else {
                    setTimeout(function () {
                        $('#errorReportArchiveDialog').dialog('close');
                        $('#errorReportDetailsDialog').dialog('close');

                        quickClaim.showOverlayMessage('Refreshing Error Reports List');
                        quickClaim.hideOverlayMessage(500);
                        $("#errorReportSearchButton").trigger('click');
                    }, 200);
                }
            },
            beforeSend: function () {
                moh.ajaxRunning = true;
                $('#messages .icons div').hide();
                $('#' + moh.messages.spinner).show();
                $('#' + moh.messages.text).text('Archiving Error Report').show();
            },
            complete: function () {
                moh.ajaxRunning = false;
                $('#' + moh.messages.spinner).hide();
            },
            error: function () {
                $('#' + moh.messages.error).show();
                $('#' + moh.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });
    },
    downloadComplete: function () {
        $('#messages .icons div').hide();
        if ($('#hiddenDownloader').contents().find('body').text()) {
            $('#' + moh.messages.error).show();
            $('#' + moh.messages.text).text('An error occurred while generating the file').show();
        } else {
            $('#' + moh.messages.checkmark).show();
            $('#' + moh.messages.text).text('Downloaded the file').show();
        }
        quickClaim.hideOverlayMessage();
    },

    downloadErrorFile: function (id) {
        hypeFileDownload.tokenName = "errorReportDownloadToken";
        hypeFileDownload.setReturnFunction(errorReports.downloadComplete);
        var url = errorReports.actions.downloadFile + '?id=' + id + '&errorReportDownloadToken=' + hypeFileDownload.blockResubmit();

        $('#messages .icons div').hide();
        $('#' + moh.messages.spinner).show();
        $('#' + moh.messages.text).text('Downloading Error File').show();
        quickClaim.showOverlayMessage('Downloading Error File');

        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    downloadCSV: function (id, listType) {
        hypeFileDownload.tokenName = "errorReportCSVDownloadToken";
        hypeFileDownload.setReturnFunction(errorReports.downloadComplete);
        var url = errorReports.actions.detailsCSV + '?id=' + id + '&errorReportCSVDownloadToken=' + hypeFileDownload.blockResubmit();

        $('#messages .icons div').hide();
        $('#' + moh.messages.spinner).show();
        $('#' + moh.messages.text).text('Downloading Error File CSV').show();
        quickClaim.showOverlayMessage('Downloading Error File CSV');

        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    contextMenu: {
        active: {
            download: true,
            details_all: true,
            details_hype: true,
            details_non_hype: true,
            details_current: true,
            pdf_all: true,
            pdf_hype: true,
            pdf_non_hype: true,
            pdf_current: true,
            csv_all: true,
            csv_hype: true,
            csv_non_hype: true,
            csv_current: true,
            archive: true
        },
        menuHtml: null,

        initialize: function () {
            var parameters = {
                selector: '#errorReportTable .actionsButton',
                trigger: 'hover',
                autoHide: true,
                className: 'errorReportMenu',
                items: {
                    download: {
                        name: 'Download Error File',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('errorReportActions_', '');
                            errorReports.downloadErrorFile(id);
                        },
                        disabled: function () {
                            return !errorReports.contextMenu.active['download'];
                        }
                    },
                    sep1: '----',
                    fold1: {
                        "name": "All Records",
                        "items": {

                            details_all: {
                                name: 'All Records',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('errorReportActions_', '');
                                    errorReports.getDetails(id, 'all');
                                },
                                disabled: function () {
                                    return !errorReports.contextMenu.active['details_all'];
                                }
                            },
                            pdf_all: {
                                name: 'All Records [PDF]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('errorReportActions_', '');
                                    errorReports.downloadPDF(id, 'all');
                                },
                                disabled: function () {
                                    return !errorReports.contextMenu.active['pdf_all'];
                                }
                            },
                            csv_all: {
                                name: 'All Records [CSV]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('errorReportActions_', '');
                                    errorReports.downloadCSV(id, 'all');
                                },
                                disabled: function () {
                                    return !errorReports.contextMenu.active['pdf_all'];
                                }
                            }
                        }
                    },

                    sep2: '----',
                    fold2: {
                        "name": "Hype-Only Records",
                        "items": {
                            details_hype: {
                                name: 'Hype-Only Records',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('errorReportActions_', '');
                                    errorReports.getDetails(id, 'hype');
                                },
                                disabled: function () {
                                    return !errorReports.contextMenu.active['details_hype'];
                                }
                            },
                            pdf_hype: {
                                name: 'Hype-Only Records [PDF]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('errorReportActions_', '');
                                    errorReports.downloadPDF(id, 'hype');
                                },
                                disabled: function () {
                                    return !errorReports.contextMenu.active['pdf_hype'];
                                }
                            },
                            csv_hype: {
                                name: 'Hype-Only Records [CSV]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('errorReportActions_', '');
                                    errorReports.downloadCSV(id, 'hype');
                                },
                                disabled: function () {
                                    return !errorReports.contextMenu.active['pdf_hype'];
                                }
                            }
                        }
                    },

                    sep3: '----',
                    fold3: {
                        "name": "Non-Hype Records",
                        "items": {
                            details_non_hype: {
                                name: 'Non-Hype Records',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('errorReportActions_', '');
                                    errorReports.getDetails(id, 'non_hype');
                                },
                                disabled: function () {
                                    return !errorReports.contextMenu.active['details_non_hype'];
                                }
                            },
                            pdf_non_hype: {
                                name: 'Non-Hype Records [PDF]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('errorReportActions_', '');
                                    errorReports.downloadPDF(id, 'non_hype');

                                },
                                disabled: function () {
                                    return !errorReports.contextMenu.active['pdf_non_hype'];
                                }
                            },
                            csv_non_hype: {
                                name: 'Non-Hype Records [CSV]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('errorReportActions_', '');
                                    errorReports.downloadCSV(id, 'non_hype');
                                },
                                disabled: function () {
                                    return !errorReports.contextMenu.active['pdf_non_hype'];
                                }
                            }
                        }
                    },

                    sep4: '----',
                    fold2: {
                        "name": "Currently Rejected Records",
                        "items": {
                            details_current: {
                                name: 'Currently Rejected Records',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('errorReportActions_', '');
                                    errorReports.getDetails(id, 'current');
                                },
                                disabled: function () {
                                    return !errorReports.contextMenu.active['details_current'];
                                }
                            },
                            pdf_current: {
                                name: 'Currently Rejected Records [PDF]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('errorReportActions_', '');
                                    errorReports.downloadPDF(id, 'current');
                                },
                                disabled: function () {
                                    return !errorReports.contextMenu.active['pdf_current'];
                                }
                            },
                            csv_current: {
                                name: 'Currently Rejected Records [CSV]',
                                callback: function (key, options, CSV) {
                                    var id = $(options.$trigger).prop('id').replace('errorReportActions_', '');
                                    errorReports.downloadCSV(id, 'current');
                                },
                                disabled: function () {
                                    return !errorReports.contextMenu.active['pdf_current'];
                                }
                            }
                        }
                    },

                    sep5: '----',
                    archive: {
                        name: 'Archive Error Report',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('errorReportActions_', '');
                            errorReports.archive(id);
                        },
                        disabled: function () {
                            return !errorReports.contextMenu.active['archive'];
                        }
                    }
                }
            };

            $.contextMenu(parameters);
            errorReports.contextMenu.setTitle('Placeholder Title');
        },
        setup: function (id) {
            $('#' + id).mouseover(function () {
                errorReports.contextMenu.setActiveItems(id);
            });
        },
        setActiveItems: function (id) {
            var id = id.replace('errorReportActions_', '');
            var actions = errorReports.errorReportActions[id];
            var menuItems = ['download', 'details_all', 'details_hype', 'details_non_hype', 'details_current', 'pdf_all', 'pdf_hype', 'pdf_non_hype', 'pdf_current', 'csv_all', 'csv_hype', 'csv_non_hype', 'csv_current', 'archive'];

            for (var a = 0; a < menuItems.length; a++) {
                if (actions.hasOwnProperty(menuItems[a]) && actions[menuItems[a]] != '') {
                    errorReports.contextMenu.enable(menuItems[a]);
                } else {
                    errorReports.contextMenu.disable(menuItems[a]);
                }
            }
        },
        disable: function (item) {
            errorReports.contextMenu.active[item] = false;
        },
        enable: function (item) {
            errorReports.contextMenu.active[item] = true;
        },
        setTitle: function (title) {
            $('.errorReportMenu').attr('data-menutitle', title);
        }
    },
    errorDetailsContextMenu: {
        active: {
            edit: true,
            details: true,
            pdf: true,
            patient_profile: true
        },
        menuHtml: null,

        initialize: function () {
            var parameters = {
                selector: '#errorReportDetailsTable .actionsButton',
                trigger: 'hover',
                autoHide: true,
                className: 'errorDetailsReportMenu',
                items: {
                    edit: {
                        name: 'Edit Claim',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('errorReportDetailActions_', '');
                            if (quickClaim.isRightClick(event)) {
                                window.open(errorReports.actions.editClaim + '?id=' + id, 'claim');
                            } else {
                                window.location = errorReports.actions.editClaim + '?id=' + id;
                            }
                        }
                    },
                    details: {
                        name: 'Claim Details',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('errorReportDetailActions_', '');
                            if (quickClaim.isRightClick(event)) {
                                window.open(errorReports.actions.detailsClaim + '?id=' + id, 'details');
                            } else {
                                window.location = errorReports.actions.detailsClaim + '?id=' + id;
                            }
                        }
                    },
                    pdf: {
                        name: 'Claim Details PDF',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('errorReportDetailActions_', '');
                            $('#hiddenDownloader').remove();
                            $('body').append('<iframe id="hiddenDownloader" src="' + errorReports.actions.claimPDF + '?id=' + id + '" style="width: 0; height: 0; border: none; display: none;"></iframe>');
                        }
                    },
                    sep1: '----',
                    patient_profile: {
                        name: 'Patient Profile',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('errorReportDetailActions_', '');
                            var url = errorReports.actions.patientProfile + '?id=' + errorReports.details.patientIDs[id];

                            if (quickClaim.isRightClick(event)) {
                                window.open(url, 'profile');
                            } else {
                                window.location = url;
                            }
                        }
                    }
                }
            };
            $.contextMenu(parameters);
            errorReports.errorDetailsContextMenu.setTitle('Placeholder Title');
        },
        setTitle: function (title) {
            $('.errorDetailsReportMenu').attr('data-menutitle', title);
        }
    }
};
