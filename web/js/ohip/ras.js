var RAs = {
    actions: {
        downloadFile: null,
        raTable: null
    },
    initialized: false,
    messages: {
        error: 'raErrorIcon',
        spinner: 'raSpinner',
        checkmark: 'raCheckmark',
        text: 'raMessageText'
    },
    tableId: '',
    pagerColumns: [],
    paginationSize: 1000,
    paginationSort: [],
    raListActions: {},
    archiveIDs: null,

    initialize: function () {
        $('#raSearchButton').button();
        $('#raResetButton').button();

        sk.datepicker('#ra_start_date');
        sk.datepicker('#ra_end_date');

        $('#ra_start_date').on('changeDate', function (e) {
            $('#ra_end_date').skDP('setStartDate', e.date);
        });

        sk.datepicker('#ra_start_date');

        $('#ra_end_date').on('changeDate', function (e) {
            $('#ra_start_date').skDP('setEndDate', e.date);
        });

        $('#raSearchButton').click(function () {
            var tableInst = $("#" + RAs.tableId).DataTable();
            tableInst.clear().draw();
            RAs.update_table(tableInst);
        });

        $('#raResetButton').click(function () {
            RAs.clearForm();
        });

        $('#archiveRAButton').click(function () {
            RAs.archive();
        });

        $('#RAArchiveDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: $(window).height() - 200,
            modal: true,
            width: '80%'
        });

        $('#RAArchiveAreYouSure').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 200,
            modal: true,
            width: 400,
            buttons: [
                {
                    text: "Yes",
                    "class": 'btn btn-success btn-squared',
                    click: function () {
                        RAs.sendArchiveAjax();
                        $('#RAArchiveAreYouSure').dialog('close');
                    }
                },
                {
                    text: "No",
                    "class": 'btn btn-danger btn-squared',
                    click: function () {
                        $('#RAArchiveAreYouSure').dialog('close');
                        $('#RAArchiveDialog').dialog('close');
                    }
                }
            ]
        });

        RAs.initRAsTable();
    },
    initRAsTable: function () {
        // thead
        var colHeader = [];
        for (var i in batch_ras_header) {
            if (i == 'select') {
                colHeader.push({title: '<label class="CHKcontainer"><input type="checkbox" value="1" checked="checked" /><span class="CHKcheckmark"></span></label>', class: 'edit_checkbox'});
            } else {
                var arr = i.split('_');
                if ($.inArray('date', arr) != -1 || i == 'dob') {
                    colHeader.push({title: batch_ras_header[i], class: i, sType: 'dateField', bSortable: true});
                } else {
                    colHeader.push({title: batch_ras_header[i], class: i});
                }
            }
        }

        console.log(colHeader);

        var tableId = 'raTable';
        var table = global_js.dataTableWithLoader(tableId, false, [], false, colHeader);
        $(".dataTables_empty").addClass('text-center').removeClass('dataTables_empty');

        $("#raTable tbody").on('click', 'tr', function () {
            if ($(this).find('input[type="checkbox"]').is(':checked')) {
                $(this).find('input[type="checkbox"]').prop('checked', false);
            } else {
                $(this).find('input[type="checkbox"]').prop('checked', true);
            }

        })

        RAs.update_table(table);
        RAs.tableId = tableId;
        table.on('page.dt', function () {
            setTimeout(function () {
                $("#" + RAs.tableId + " tbody .actionsButton").each(function () {
                    $(this).on('hover', function () {
                        var batchAckID = $(this).attr('id').replace('raSelect_', '');
                        RAs.contextMenu.setActiveItems(batchAckID);
                    })
                })
            }, 500)
        });
    },
    update_table: function (tableInst) {
        $.ajax({
            url: RAs.actions.raTable + '?pageNumber=1&size=1000&' + $('#raForm').serialize(),
            type: 'GET',
            success: function (res) {
                global_js.dtLoader(RAs.tableId);
                var obj = JSON.parse(res);

                if (obj.hasOwnProperty('data') && obj.data.length > 0) {
                    var allData = obj.data;
                    var dtData = [];
                    RAs.raListActions = {};
                    for (var j in allData) {
                        
                        dtData.push(RAs.buildRow(allData[j]));
                    }
                    //console.log(dtData);
                    tableInst.rows.add(dtData);
                    tableInst.draw();
                    $(".selectRsltTbl").select2('destroy');
                    var aa = 0;
                    var apiDt = $("#" + RAs.tableId).dataTable().api();
                    apiDt.columns().every(function () {
                        var column = this;
                         var columnText = $.trim($(column.header())[0].innerText);
                        if (aa != 0) {
                            $("#" + RAs.tableId + " thead tr:eq(1) th:eq(" + aa + ")").html('');

                            var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                    .appendTo($("#" + RAs.tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                    .on('change', function () {
                                        var val = $(this).val();
                                        column.search(val ?  val : '', true, false).draw();
                                    });
                            var chkArr = [];
                            column.data().unique().sort().each(function (d, j) {
                                if(Array.isArray(d)){
                                    for ( var i in d){
                                        if($.inArray(d[i], chkArr) == -1 ) {
                                            chkArr.push(d[i]);
                                            select.append('<option value="' + d[i] + '">' + d[i] + '</option>');
                                        }
                                    }                                    
                                } else {
                                     if(d != '') {
                                     select.append('<option value="' + d + '">' + d + '</option>');
                                     }
                                }
                                
                            });
                        }
                        aa++;
                    });
                    $(".selectRsltTbl").select2({
                        placeholder: "Search",
                        allowClear: true,
                        dropdownAutoWidth: true,
                        width: '98%'
                    });

                    $.fn.dataTableExt.oSort["dateField-desc"] = function (x, y) {
                        var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                        var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                        return (a.diff(b));
                    };

                    $.fn.dataTableExt.oSort["dateField-asc"] = function (x, y) {
                        var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                        var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                        return (b.diff(a));
                    }
                    $('#' + RAs.tableId + ' .select2-arrow').hide();
                    setTimeout(function () {
                        $("#" + RAs.tableId + " tbody .actionsButton").each(function () {
                            $(this).on('hover', function () {
                                var batchAckID = $(this).attr('id').replace('raSelect_', '');
                                RAs.contextMenu.setActiveItems(batchAckID);
                            })
                        })
                    }, 500);
                } else {
                    $(".dataTables_empty").addClass('text-center').removeClass('dataTables_empty');
                }
                global_js.initCheckbox(RAs.tableId);
            },
            beforeSend: function () {
                $(".dataTables_empty").addClass('text-center').removeClass('dataTables_empty');
                global_js.dtLoader(RAs.tableId, 'start');
            }
        })
    },
    buildRow: function (data) {
        //console.log(data);
        RAs.raListActions[data['reconcile_id']] = data['actions'];
        var rowData = [];
        for (var i in batch_ras_header) {
            if (i == 'select') {
                rowData.push('<label class="CHKcontainer"><input type="checkbox" data-file_name="' + data['file_name'] + '" data-total_accepted_claims="' + data['total_accepted_claims'] + '" data-total_rejected_claims="' + data['total_rejected_claims'] + '" id="raSelect_' + data['reconcile_id'] + '" value="1" checked="checked" /><span class="CHKcheckmark"></span></label>');
            } else if (i == 'actions') {
                var html = '<img class="actionsButton" id="raListActions_' + data['reconcile_id'] + '" src="' + RAs.contextMenu.menuHtml + '" />';
                rowData.push(html);
            } else {
                rowData.push(data[i]);
            }
        }
        return rowData;
    },
    clearForm: function () {
        $('#raForm').clearForm();
        $("select").trigger('change');
        $("#raSearchButton").trigger('click');
    },
    downloadComplete: function () {
        $('#raMessages .icons div').hide();
        if ($('#hiddenDownloader').contents().find('body').text()) {
            $('#' + RAs.messages.error).show();
            $('#' + RAs.messages.text).text('An error occurred while generating the file').show();
        } else {
            $('#' + RAs.messages.checkmark).show();
            $('#' + RAs.messages.text).text('Downloaded the file').show();
        }
        quickClaim.hideOverlayMessage();
    },
    collectIds: function (prefix, id) {
        var ids = new Array();
        if (!id) {

            $('#raTable tbody input[type="checkbox"]:checked').each(function () {
                var reconcileID = $(this).attr('id').replace('raSelect_', '');
                var actions = RAs.raListActions[reconcileID];

                if (actions.hasOwnProperty(prefix) && actions[prefix]) {
                    ids[ids.length] = reconcileID;
                }
            });
        } else {
            ids[ids.length] = id;
        }

        return ids;
    },
    archive: function (id) {
        var ids = RAs.collectIds('archive', id);

        if (!ids.length) {
            quickClaim.showOverlayMessage('No selected Remittances can be archived.');
            setTimeout(function () {
                quickClaim.hideOverlayMessage();
            }, 2000);
            return;
        }

        $('#RAArchiveTable tbody tr').remove();
        for (var a = 0; a < ids.length; a++) {
            var fileName = $('#raSelect_' + ids[a]).attr('data-file_name');

            var tr = '<tr id="archiveRA_' + ids[a] + '">'
                    + '<td class="statuses">&nbsp;</td>'
                    + '<td class="fileName">' + fileName + '</td>'
                    + '<td class="message">&nbsp;</td>'
                    + '</tr>';

            $('#RAArchiveTable tbody').append($(tr));
        }
        $('#RAArchiveTable').trigger('update');

        RAs.archiveIds = ids;
        $('#RAArchiveDialog').dialog('open');
        $('#RAArchiveAreYouSure').dialog('open');
    },
    sendArchiveAjax: function () {
        var sendIDs = new Array();
        var extraIDs = new Array();

        for (var a = 0; a < RAs.archiveIds.length; a++) {
            if (a < 1) {
                sendIDs[sendIDs.length] = RAs.archiveIds[a];

                $('#archiveRA_' + RAs.archiveIds[a] + ' td.message').text('Archiving Remittance');
                $('#archiveRA_' + RAs.archiveIds[a] + ' td.statuses').html($(RAs.messages.spinnerHtml));
                $('#archiveRA_' + RAs.archiveIds[a]).addClass('currentlySending');
            } else {
                extraIDs[extraIDs.length] = RAs.archiveIds[a];
            }
        }
        RAs.archiveIds = extraIDs;

        $.ajax({
            type: 'post',
            dataType: 'json',
            data: 'ids=' + sendIDs.join(',') + '&mode=RA',
            url: RAs.actions.archive,
            success: function (rs) {
                for (var a in rs.data) {
                    if (rs.data[a].actions.archive == '') {
                        $('#archiveRA_' + a + ' .message').text('Archived Remittance');
                        $('#archiveRA_' + a + ' .statuses').html(RAs.messages.checkmarkHtml);
                    } else {
                        $('#archiveRA_' + a + ' .statuses').html(RAs.messages.errorHtml);
                    }
                    $('#archiveRA_' + a).removeClass('currentlySending');
                }

                if (RAs.archiveIds.length) {
                    RAs.sendArchiveAjax();
                } else {
                    setTimeout(function () {

                        $('#RAArchiveDialog').dialog('close');
                        $('#raDetailsDialog').dialog('close');

                        quickClaim.showOverlayMessage('Refreshing Remittances List');
                        quickClaim.hideOverlayMessage(500)
                        $("#raSearchButton").trigger('click');
                    }, 200);
                }
            },
            beforeSend: function () {
                moh.ajaxRunning = true;
                $('#messages .icons div').hide();
                $('#' + moh.messages.spinner).show();
                $('#' + moh.messages.text).text('Archiving Remittances').show();
            },
            complete: function () {
                moh.ajaxRunning = false;
                quickClaim.hideOverlayMessage();
                $('#' + moh.messages.spinner).hide();
            },
            error: function () {
                $('#' + moh.messages.error).show();
                $('#' + moh.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });
    },

    selectAll: function (selected) {
        $('#raTable tbody td.select input').prop('checked', selected);
    },

    contextMenu: {
        active: {
            csv: true,
            download: true,
            complete_pdf: true,
            complete_csv: true,
            general: true,
            general_pdf: true,
            general_csv: true,
            accepted: true,
            accepted_pdf: true,
            accepted_csv: true,
            rejected: true,
            rejected_pdf: true,
            rejected_csv: true,
            messages: true,
            messages_pdf: true,
            messages_csv: true,
            statistics: true,
            statistics_pdf: true,
            statistics_csv: true,
            archive: true
        },
        menuHtml: null,

        initialize: function () {
            var parameters = {
                selector: '#raTable .actionsButton',
                trigger: 'hover',
                autoHide: true,
                className: 'raListMenu',
                items: {                    
                    complete_pdf: {
                        name: 'PDF [All Sections]',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                            RAFile.requestPDF(id, new Array('general', 'accepted', 'rejected', 'messages', 'statistics'));
                        },
                        disabled: function () {
                            return !RAs.contextMenu.active['complete_pdf'];
                        }
                    },
                    csv: {
                        name: 'CSV [All Sections]',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                            RAFile.requestCSV(id, new Array('general', 'accepted', 'rejected', 'messages', 'statistics'));
                        },
                        disabled: function () {
                            return !RAs.contextMenu.active['csv'];
                        }
                    },
                    sep1: '----',
                    fold1: {
                        "name": "First Page Summary",
                        "items": {
                            general: {
                                name: 'First Page Summary',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                                    RAFile.showTab(id, 'general');
                                },
                                disabled: function () {
                                    return !RAs.contextMenu.active['general'];
                                }
                            },
                            general_pdf: {
                                name: 'First Page Summary [PDF]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                                    RAFile.requestPDF(id, new Array('general'));
                                },
                                disabled: function () {
                                    return !RAs.contextMenu.active['general_pdf'];
                                }
                            },
                            general_csv: {
                                name: 'First Page Summary [CSV]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                                    RAFile.requestCSV(id, new Array('general'));
                                },
                                disabled: function () {
                                    return !RAs.contextMenu.active['general_csv'];
                                }
                            }
                        }
                    },

                    sep2: '----',
                    fold2: {
                        "name": "Claims Accepted",
                        "items": {
                            accepted: {
                                name: 'Claims Accepted',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                                    RAFile.showTab(id, 'accepted');
                                },
                                disabled: function () {
                                    return !RAs.contextMenu.active['accepted'];
                                }
                            },
                            accepted_pdf: {
                                name: 'Claims Accepted [PDF]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                                    RAFile.requestPDF(id, new Array('accepted'));
                                },
                                disabled: function () {
                                    return !RAs.contextMenu.active['accepted_pdf'];
                                }
                            },
                            accepted_csv: {
                                name: 'Claims Accepted [CSV]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                                    RAFile.requestCSV(id, new Array('accepted'));
                                },
                                disabled: function () {
                                    return !RAs.contextMenu.active['accepted_csv'];
                                }
                            }
                        }
                    },

                    sep3: '----',
                    fold3: {
                        "name": "Claims w\\ Expl. Codes",
                        "items": {
                            rejected: {
                                name: 'Claims w\\ Expl. Codes',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                                    RAFile.showTab(id, 'rejected');
                                },
                                disabled: function () {
                                    return !RAs.contextMenu.active['rejected'];
                                }
                            },
                            rejected_pdf: {
                                name: 'Claims w\\ Expl. Codes [PDF]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                                    RAFile.requestPDF(id, new Array('rejected'));
                                },
                                disabled: function () {
                                    return !RAs.contextMenu.active['rejected_pdf'];
                                }
                            },
                            rejected_csv: {
                                name: 'Claims w\\ Expl. Codes [CSV]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                                    RAFile.requestCSV(id, new Array('rejected'));
                                },
                                disabled: function () {
                                    return !RAs.contextMenu.active['rejected_csv'];
                                }
                            }
                        }
                    },

                    sep4: '----',
                    fold4: {
                        "name": "Messages",
                        "items": {
                            messages: {
                                name: 'Messages',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                                    RAFile.showTab(id, 'messages');
                                },
                                disabled: function () {
                                    return !RAs.contextMenu.active['messages'];
                                }
                            },
                            messages_pdf: {
                                name: 'Messages [PDF]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                                    RAFile.requestPDF(id, new Array('messages'));
                                },
                                disabled: function () {
                                    return !RAs.contextMenu.active['messages_pdf'];
                                }
                            },
                            messages_csv: {
                                name: 'Messages [CSV]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                                    RAFile.requestCSV(id, new Array('messages'));
                                },
                                disabled: function () {
                                    return !RAs.contextMenu.active['messages_csv'];
                                }
                            }
                        }
                    },

                    sep5: '----',
                    fold5: {
                        "name": "Statistics",
                        "items": {

                            statistics: {
                                name: 'Statistics',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                                    RAFile.showTab(id, 'statistics');
                                },
                                disabled: function () {
                                    return !RAs.contextMenu.active['statistics'];
                                }
                            },
                            statistics_pdf: {
                                name: 'Statistics [PDF]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                                    RAFile.requestPDF(id, new Array('statistics'));
                                },
                                disabled: function () {
                                    return !RAs.contextMenu.active['statistics_pdf'];
                                }
                            },
                            statistics_csv: {
                                name: 'Statistics [CSV]',
                                callback: function (key, options, event) {
                                    var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                                    RAFile.requestCSV(id, new Array('statistics'));
                                },
                                disabled: function () {
                                    return !RAs.contextMenu.active['statistics_csv'];
                                }
                            }
                        }
                    },

                    sep6: '----',
                    archive: {
                        name: 'Archive Remittance Advice File',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                            RAs.archive(id);
                        },
                        disabled: function () {
                            return !RAs.contextMenu.active['archive'];
                        }
                    },
                    download: {
                        name: 'Download Remittance Advice File',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('raListActions_', '');
                            var url = RAs.actions.downloadFile + '?id=' + id + '&raFileDownloadToken=' + hypeFileDownload.blockResubmit();

                            hypeFileDownload.tokenName = "raFileDownloadToken";
                            hypeFileDownload.setReturnFunction(RAs.downloadComplete);

                            $('#raMessages .icons div').hide();
                            $('#' + RAs.messages.spinner).show();
                            $('#' + RAs.messages.text).text('Downloading Remittance Advice File').show();
                            quickClaim.showOverlayMessage('Downloading Remittance Advice File');

                            quickClaim.hideOverlayMessage(2000);

                            $('#hiddenDownloader').remove();
                            $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
                        },
                        disabled: function () {
                            return !RAs.contextMenu.active['download'];
                        }
                    }

                }
            };

            $.contextMenu(parameters);
        },
        setup: function (id) {
            $('#' + id).mouseover(function () {
                RAs.contextMenu.setActiveItems(id);
            });
        },
        setActiveItems: function (id) {
            var id = id.replace('raListActions_', '');
            var actions = RAs.raListActions[id];
            var menuItems = ['download', 'general', 'general_pdf', 'general_csv', 'accepted_csv', 'rejected_csv', 'messages_csv', 'statistics_csv', 'accepted', 'accepted_pdf', 'rejected', 'rejected_pdf', 'messages', 'messages_pdf', 'statistics', 'statistics_pdf', 'complete_pdf', 'csv'];

            for (var a = 0; a < menuItems.length; a++) {
                if (actions.hasOwnProperty(menuItems[a]) && actions[menuItems[a]] != '') {
                    RAs.contextMenu.enable(menuItems[a]);
                } else {
                    RAs.contextMenu.disable(menuItems[a]);
                }
            }
        },
        disable: function (item) {
            RAs.contextMenu.active[item] = false;
        },
        enable: function (item) {
            RAs.contextMenu.active[item] = true;
        },
        setTitle: function (title) {
            $('.raListMenu').attr('data-menutitle', title);
        }
    }
};

var RAFile = {
    actions: {
        raClaimsList: null,
        raPDF: null,
        generalTab: null,
        messagesTab: null,
        statisticsTab: null
    },
    tableId: '',
    currentReconcileID: null,
    alreadyLoaded: false,
    acceptedPagerColumns: {},
    acceptedPagerSort: [],
    acceptedPagerSize: 25,
    acceptedTableInitialized: false,

    rejectedPagerColumns: {},
    rejectedPagerSort: [],
    rejectedPagerSize: 25,
    rejectedTableInitialized: false,

    tabsLoaded: {
        general: false,
        accepted: false,
        rejected: false,
        messages: false,
        statistics: false
    },

    messages: {
        error: 'raDataErrorIcon',
        spinner: 'raDataSpinner',
        checkmark: 'raDataCheckmark',
        text: 'raDataMessageText'
    },
    initialize: function () {
        $('#archiveSingleRAButton').click(function () {
            RAs.archive(RAFile.currentReconcileID);
        });

        $('#raDetailsDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: $(window).height() - 200,
            modal: true,
            width: '80%'
        });

        $('#ra_tabs').tabs({
            activate: function (event, ui) {
                var activeTab = $('#ra_tabs').tabs('option', 'active');
                if (activeTab == 0) {
                    RAFile.showGeneralTab();
                } else if (activeTab == 1) {
                    RAFile.showAcceptedTab();
                } else if (activeTab == 2) {
                    RAFile.showRejectedTab();
                } else if (activeTab == 3) {
                    RAFile.showMessagesTab();
                } else if (activeTab == 4) {
                    RAFile.showStatisticsTab();
                }
            }
        });
        $('#ra_tabs').tabs('tabSwitching');

        $('#raTransactionsTable').tablesorter({
            theme: 'blue',
            headerTemplate: '{content} {icon}',
            widgets: ['zebra'],
            sortList: [[0, 0]]
        });

        $('#raStatsTable').tablesorter({
            theme: 'blue',
            headerTemplate: '{content} {icon}',
            widgets: ['zebra'],
            sortList: [[0, 0]]
        });

      
    },
    initializeAcceptedPager: function () {

        // $('#printButton').button();
        //     $('#printButton').click(function (ev) {
        //         $('#raAcceptedTable').printElement({
        //             overrideElementCSS: ['css/jquery-tablesorter1.css', 'css/jquery-tablesorter/jquery.tablesorter.pager.css', 'css/jquery-tablesorter/theme.blue.css']
        //         });
        //     });


        

        //alert('RA File Details ' +$('#raDetailsDialog .ra_file_name').text());
        $('#raAcceptedTable thead th.actions').addClass('sorter-false');
        $('#raAcceptedTable thead th.hcn').addClass('sorter-false');
        $('#raAcceptedTable thead th.select').addClass('sorter-false').addClass('actions');
        $('#raAcceptedTable thead th.still_rejected').addClass('sorter-false');
        
        $('#raAcceptedTable thead th:last').append(quickClaim.tablesorterTooltip);
        $('#raAcceptedTable .sortTip').tooltip();
        
        $('#stage_track_pdf_option_header').val('RA File Details ' +$('.ra_files_details .ra_file_name').text());

        $('#raAcceptedTable').tablesorter({
            theme: 'blue',
            headerTemplate: '{content} {icon}',
            widgets: ['selectRow', 'zebra', 'pager'],
            sortList: RAFile.acceptedPagerSort
        });
        

        $('#raAcceptedTable').tablesorterPager({
            container: $('.raAcceptedPager'),
            size: RAFile.acceptedPagerSize,
            cssPageSize: '.pagesize',
            ajaxUrl: RAFile.actions.raClaimsList + '?{sortList:sort}&pageNumber={page}&size={size}',
            customAjaxUrl: function (table, url) {
                return url + '&reconcileID=' + RAFile.currentReconcileID + '&mode=accepted&alreadyLoaded=' + (RAFile.alreadyLoaded ? '1' : '0');
            },
            ajaxObject: {
                dataType: 'json',
                complete: function () {
                    quickClaim.ajaxLocked = false;
                    $('#' + RAFile.messages.spinner).hide();
                    $('#raAcceptedTable').tooltip();
                    global_js.customFooterDataTableWithOptions('raAcceptedTable', true, ['pdf'],false);
                    quickClaim.ajaxLocked = false;
                    $('#' + RAFile.messages.spinner).hide();
                },
                beforeSend: function () {
                    if (quickClaim.ajaxLocked) {
                        return false;
                    }
                    quickClaim.ajaxLocked = true;
                    $('#raDataMessages .icons div').hide();
                    $('#' + RAFile.messages.spinner).show();
                    $('#' + RAFile.messages.text).text('Requesting Accepted Claim Details').show();

                    global_js.datatableDinit('raAcceptedTable');
                    //$("#raAcceptedTable").dataTable().fnDestroy();
                    quickClaim.ajaxLocked = true;
                },
                error: function () {
                    // $('#' + RAFile.messages.error).show();
                    // $('#' + RAFile.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            },
            ajaxProcessing: function (rs) {
                quickClaim.hideOverlayMessage(1);

                $('#' + RAFile.messages.text).text('Received Accepted Claim Details.').show();
                $('#' + RAFile.messages.checkmark).show();

                if (rs.hasOwnProperty('messages_count')) {
                    $('#raDetailsDialog .count_ra_message').text(rs.messages_count);
                }

                if (rs.hasOwnProperty('data')) {
                    RAFile.acceptedPagerColumns = rs.headers;
                    // console.log(rs.headers);
                    // console.log(rs.data);
                    //alert(rs.count);
                    var data = rs.data;
                    var rows = '';

                    for (var a in data)
                    {
                        rows += '<tr id="' + data[a].id + '">';
                        rows += RAFile.getClaimTableRow(data[a], data[a].id, RAFile.acceptedPagerColumns);
                        rows += '</tr>' + "\n";
                    }
                    return [rs.count, $(rows)];
                }
            }
        });
    },
    getClaimTableRow: function (data, id, columns) {
        var rs = '';

        for (var b in columns)
        {
            rs += '<td class="' + b + '">' + data[b] + '</td>';
        }
        return rs;
    },
    initializeRejectedPager: function () {
        $('#raRejectedTable thead th.actions').addClass('sorter-false');
        $('#raRejectedTable thead th.hcn').addClass('sorter-false');
        $('#raRejectedTable thead th.select').addClass('sorter-false').addClass('actions');
        $('#raRejectedTable thead th.still_rejected').addClass('sorter-false');
        
        $('#raRejectedTable thead th:last').append(quickClaim.tablesorterTooltip);
        $('#raRejectedTable .sortTip').tooltip();
        $('#stage_track_pdf_option_header').val('RA File Details ' +$('.ra_files_details .ra_file_name').text());

        $('#raRejectedTable').tablesorter({
            theme: 'blue',
            headerTemplate: '{content} {icon}',
            widgets: ['selectRow', 'zebra', 'pager'],
            sortList: RAFile.rejectedPagerSort
        });

        $('#raRejectedTable').tablesorterPager({
            container: $('.raRejectedPager'),
            size: RAFile.rejectedPagerSize,
            cssPageSize: '.pagesize',
            ajaxUrl: RAFile.actions.raClaimsList + '?{sortList:sort}&pageNumber={page}&size={size}',
            customAjaxUrl: function (table, url) {
                return url + '&reconcileID=' + RAFile.currentReconcileID + '&mode=rejected&alreadyLoaded=' + (RAFile.alreadyLoaded ? '1' : '0');
            },
            ajaxObject: {
                dataType: 'json',
                complete: function () {
                    // quickClaim.ajaxLocked = false;
                    // $('#' + RAFile.messages.spinner).hide();
                   
                    quickClaim.ajaxLocked = false;
                    $('#' + RAFile.messages.spinner).hide();
                    $('#raRejectedTable').tooltip();
                    global_js.customFooterDataTableWithOptions('raRejectedTable', true, ['pdf'],false);
                    quickClaim.ajaxLocked = false;
                    $('#' + RAFile.messages.spinner).hide();

                },
                beforeSend: function () {
                    // if (quickClaim.ajaxLocked) {
                    //     return false;
                    // }
                    // quickClaim.ajaxLocked = true;
                    // $('#raDataMessages .icons div').hide();
                    // $('#' + RAFile.messages.spinner).show();
                    // $('#' + RAFile.messages.text).text('Requesting Rejected Claim Details').show();

                     if (quickClaim.ajaxLocked) {
                        return false;
                    }
                    quickClaim.ajaxLocked = true;
                    $('#raDataMessages .icons div').hide();
                    $('#' + RAFile.messages.spinner).show();
                    $('#' + RAFile.messages.text).text('Requesting Rejected Claim Details').show();

                    global_js.datatableDinit('raRejectedTable');
                    //$("#raAcceptedTable").dataTable().fnDestroy();
                    quickClaim.ajaxLocked = true;

                },
                error: function () {
                    // $('#' + RAFile.messages.error).show();
                    // $('#' + RAFile.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            },
            ajaxProcessing: function (rs) {
                quickClaim.hideOverlayMessage(1);

                $('#' + RAFile.messages.text).text('Received Rejected Claim Details.').show();
                $('#' + RAFile.messages.checkmark).show();

                // if (rs.hasOwnProperty('messages_count')) {
                //     $('#raDetailsDialog .count_ra_message').text(rs.messages_count);
                // }
                if (rs.hasOwnProperty('data')) {
                    RAFile.rejectedPagerColumns = rs.headers;

                    var data = rs.data;
                    var rows = '';

                    for (var a in data)
                    {
                        rows += '<tr id="' + data[a].id + '">';
                        rows += RAFile.getClaimTableRow(data[a], data[a].id, RAFile.rejectedPagerColumns);
                        rows += '</tr>' + "\n";
                    }

                    return [rs.count, $(rows)];
                }
            }
        });
    },
    clearDetails: function () {
        $('#raDetailsDialog #general_data td').text('');
        $('#raDetailsDialog .ra_file_name').text('');
        $('#raDetailsDialog .total_accepted_claims').text('');
        $('#raDetailsDialog .total_rejected_claims').text('');
        $('#raDetailsDialog .count_ra_message').text('');

        $('#raMessagesDiv ul').remove();
        $('#raTransactionsTable tbody tr').remove();
        $('#raRejectedTable tbody tr').remove();
        $('#raAcceptedTable tbody tr').remove();
        $('#raStatsTable tbody tr').remove();

        RAFile.tabsLoaded = {
            general: false,
            accepted: false,
            rejected: false,
            statistics: false
        };
    },
    downloadComplete: function () {
        $('#raMessages .icons div').hide();
        if ($('#hiddenDownloader').contents().find('body').text()) {
            $('#' + RAs.messages.error).show();
            $('#' + RAs.messages.text).text('An error occurred while generating the file').show();
        } else {
            $('#' + RAs.messages.checkmark).show();
            $('#' + RAs.messages.text).text('Downloaded the file').show();
        }
        quickClaim.hideOverlayMessage();
    },
    showTab: function (reconcileID, tabToShow) {
        if (reconcileID != RAFile.currentReconcileID) {
            RAFile.clearDetails();
            RAFile.currentReconcileID = reconcileID;
            RAFile.alreadyLoaded = false;
            RAFile.pullDataFromRATable();
        }

        if (tabToShow == 'general') {
            $('#ra_tabs').tabs('option', 'active', '0');
            RAFile.showGeneralTab();
        } else if (tabToShow == 'accepted') {
            $('#ra_tabs').tabs('option', 'active', '1');
            RAFile.showAcceptedTab();
        } else if (tabToShow == 'rejected') {
            $('#ra_tabs').tabs('option', 'active', '2');
            RAFile.showRejectedTab();
        } else if (tabToShow == 'messages') {
            $('#ra_tabs').tabs('option', 'active', '3');
            RAFile.showMessagesTab();
        } else if (tabToShow == 'statistics') {
            $('#ra_tabs').tabs('option', 'active', '4');
            RAFile.showStatisticsTab();
        }

        $('#raDetailsDialog').dialog('open');
    },
    pullDataFromRATable: function () {
        var id = RAFile.currentReconcileID;
        $('#raDetailsDialog .ra_file_name').text($('#raSelect_' + id).attr('data-file_name'));
        $('#raDetailsDialog .total_accepted_claims').text($('#raSelect_' + id).attr('data-total_accepted_claims'));
        $('#raDetailsDialog .total_rejected_claims').text($('#raSelect_' + id).attr('data-total_rejected_claims'));
        $('#raDetailsDialog .count_ra_message').html($('#raDataSpinner').html());
    },
    requestCSV: function (reconcileID, dataParts) {
        hypeFileDownload.tokenName = "raCSVDownloadToken";
        hypeFileDownload.setReturnFunction(RAFile.downloadComplete);

        var url = RAFile.actions.raCSV + '?reconcileID=' + reconcileID + '&raCSVDownloadToken=' + hypeFileDownload.blockResubmit() + '&sections=' + dataParts.join();

        $('#raMessages .icons div').hide();
        $('#' + RAs.messages.spinner).show();
        $('#' + RAs.messages.text).text('Downloading Remittance Advice CSV').show();
        quickClaim.showOverlayMessage('Downloading Remittance Advice CSV');

        quickClaim.hideOverlayMessage(2000);
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    showAcceptedTab: function () {
        if (!RAFile.acceptedTableInitialized) {
           RAFile.acceptedTableInitialized = true;
           RAFile.initializeAcceptedPager();
        } else {
            $('#raAcceptedTable').trigger('pageSet.pager');
        }
    },
    showRejectedTab: function () {
        if (!RAFile.rejectedTableInitialized) {
            RAFile.rejectedTableInitialized = true;
            RAFile.initializeRejectedPager();
        } else {
            $('#raRejectedTable').trigger('pageSet.pager');
        }
    },
    showGeneralTab: function () {
        if (quickClaim.ajaxLocked || RAFile.tabsLoaded['general']) {
            return;
        }

        var data = "id=" + RAFile.currentReconcileID + '&alreadyLoaded=' + (RAFile.alreadyLoaded ? '1' : '0');

        $.ajax({
            type: 'post',
            dataType: 'json',
            data: data,
            url: RAFile.actions.generalTab,
            success: function (rs) {
                if (rs.hasOwnProperty('general_info')) {
                    for (var key in rs.general_info) {
                        $('#raDetailsDialog #ra_general .' + key).html(rs.general_info[key]);
                    }
                }
                if (rs.hasOwnProperty('messages_count')) {
                    $('#raDetailsDialog .count_ra_message').text(rs.messages_count);
                }
                if (rs.hasOwnProperty('transactions')) {
                    RAFile.buildTransactionsTable(rs.transactions);
                }

                RAFile.tabsLoaded['general'] = true;
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                $('#raMessages .icons div').hide();
                $('#' + RAFile.messages.spinner).show();
                $('#' + RAFile.messages.text).text('Retrieving RA File Details [General Information]').show();
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                $('#' + RAFile.messages.spinner).hide();
            },
            error: function () {
                $('#' + RAFile.messages.error).show();
                $('#' + RAFile.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });
    },
    showMessagesTab: function () {
        if (quickClaim.ajaxLocked || RAFile.tabsLoaded['messages']) {
            return;
        }
        var data = "id=" + RAFile.currentReconcileID + '&alreadyLoaded=' + (RAFile.alreadyLoaded ? '1' : '0');

        $.ajax({
            type: 'post',
            dataType: 'json',
            data: data,
            url: RAFile.actions.messagesTab,
            success: function (rs) {
                if (rs.hasOwnProperty('messages_count')) {
                    $('#raDetailsDialog .count_ra_message').text(rs.messages_count);
                }

                if (rs.hasOwnProperty('messages')) {
                    ul = RAFile.buildMessagesUL(rs.messages);
                    $('#raMessagesDiv').append(ul);

                    $('#raMessagesDiv .messageUL li b').click(function () {
                        RAFile.handleMessageToggle(this);
                    });
                }

                RAFile.tabsLoaded['messages'] = true;
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                $('#raMessages .icons div').hide();
                $('#' + RAFile.messages.spinner).show();
                $('#' + RAFile.messages.text).text('Retrieving RA File Details [Messages]').show();
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                $('#' + RAFile.messages.spinner).hide();
            },
            error: function () {
                $('#' + RAFile.messages.error).show();
                $('#' + RAFile.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });
    },
    showStatisticsTab: function () {
        if (quickClaim.ajaxLocked || RAFile.tabsLoaded['statistics']) {
            return;
        }

        var data = "id=" + RAFile.currentReconcileID + '&alreadyLoaded=' + (RAFile.alreadyLoaded ? '1' : '0');

        $.ajax({
            type: 'post',
            dataType: 'json',
            data: data,
            url: RAFile.actions.statisticsTab,
            success: function (rs) {
                if (rs.hasOwnProperty('messages_count')) {
                    $('#raDetailsDialog .count_ra_message').text(rs.messages_count);
                }

                if (rs.hasOwnProperty('statistics')) {
                    for (var a in rs.statistics) {
                        var row = '<tr><th>' + a + '</th>'
                                + '<td>' + rs.statistics[a].accepted + '</td>'
                                + '<td>' + rs.statistics[a].rejected + '</td>'
                                + '</tr>';

                        $('#raStatsTable tbody').append($(row));
                    }
                    $('#raStatsTable').trigger('updateAll', true);
                }

                RAFile.tabsLoaded['statistics'] = true;
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                $('#raMessages .icons div').hide();
                $('#' + RAFile.messages.spinner).show();
                $('#' + RAFile.messages.text).text('Retrieving RA File Details [Statistics]').show();
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                $('#' + RAFile.messages.spinner).hide();
            },
            error: function () {
                $('#' + RAFile.messages.error).show();
                $('#' + RAFile.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });
    },
    buildMessagesUL: function (messages)
    {
        var c = 1;
        var rs = '<ul class="messageUL">';

        for (var a in messages) {
            rs += '<li id="message' + c + '" clear="all" style="margin-bottom:4px;">'
                    + '<b class="title" style="line-height:1.5em">'
                    + '<span class="clip-plus-circle sk-left sk-padding-small"></span>'
                    + 'Message ' + c
                    + '</b>'
                    + '<br clear="all" /><div class="messageText" style="display:block; margin-top:5px"><pre>'
                    + messages[a]
                    + '</pre></div>'
                    + '</li>';
            c++;
        }
        rs += '</ul>';

        return rs;
    },
    handleMessageToggle: function (e)
    {
        var id = $(e).closest('li').prop('id');
        $('#' + id + ' .sk-left').toggleClass('clip-plus-circle');
        $('#' + id + ' .sk-left').toggleClass('clip-minus-circle');
        $('#' + id + ' .messageText').toggle();
    },
    buildTransactionsTable: function (data) {
        for (var a in data) {
            var tr = '<tr id="' + data[a].id + '">'
                    + '<td>' + data[a].transaction_type + '</td>'
                    + '<td>' + data[a].transaction_date + '</td>'
                    + '<td style="color: ' + (data[a].transaction_amount.includes('-') ? "red" : "green") + ';">' + data[a].transaction_amount + '</td>'
                    + '<td>' + data[a].description + '</td>'
                    + '</tr>';
            $('#raTransactionsTable tbody').append($(tr));
        }

        $('#raTransactionsTable').trigger('updateAll', true);
    },
    requestPDF: function (id, dataParts) {
        hypeFileDownload.tokenName = "raPDFDownloadToken";
        hypeFileDownload.setReturnFunction(RAFile.downloadComplete);

        var url = RAFile.actions.raPDF + '?reconcileID=' + id + '&raPDFDownloadToken=' + hypeFileDownload.blockResubmit() + '&sections=' + dataParts.join();
        $('#raMessages .icons div').hide();
        $('#' + RAs.messages.spinner).show();
        $('#' + RAs.messages.text).text('Downloading Remittance Advice PDF').show();
        quickClaim.showOverlayMessage('Downloading Remittance Advice PDF');
        quickClaim.hideOverlayMessage(2000);
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
    }
};
