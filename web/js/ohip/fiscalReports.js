var fiscalReports = {
    actions: {
        fiscalReportTable: null,
        downloadFile: null
    },
    initialized: false,
    tableId: '',
    pagerColumns: {},
    paginationSize: 25,
    paginationSort: {},
    messages: {
        error: 'fiscalReportErrorIcon',
        spinner: 'fiscalReportSpinner',
        checkmark: 'fiscalReportCheckmark',
        text: 'fiscalReportMessageText'
    },
    reportActions: {},

    initialize: function () {
        $('#fiscalReportSearchButton').button();
        $('#fiscalReportSearchButton').click(function () {
            $('#fiscalReportTable').trigger('pageSet.pager');
        });

        $('#fiscalReportResetButton').button();
        $('#fiscalReportResetButton').click(function () {
            fiscalReports.clearForm();
        });

        fiscalReports.initBatchAcksTable();

        $('#fiscalReportTableSelectAll').click(function () {
            var selected = $(this).prop('checked');
            $('#fiscalReportTable tbody td.select input').prop('checked', selected);
        });

    },
    initBatchAcksTable: function () {
        // thead
        var colHeader = [];
        for (var i in batch_fiscal_header) {
            if (i == 'select') {
                colHeader.push({title: '<label class="CHKcontainer"><input type="checkbox" id="batchAckTableSelectAll" value="1" checked="checked" /><span class="CHKcheckmark"></span></label>', class: 'edit_checkbox'});
            } else {
                var arr = i.split('_');
                if ($.inArray('date', arr) != -1 || i == 'dob') {
                    colHeader.push({title: batch_fiscal_header[i], class: i, sType: 'dateField', bSortable: true});
                } else {
                    colHeader.push({title: batch_fiscal_header[i], class: i});
                }
            }
        }

        var tableId = 'fiscalReportTable';
        var table = global_js.dataTableWithLoader(tableId, true, [], false, colHeader);
        $(".dataTables_empty").addClass('text-center').removeClass('dataTables_empty');

        $("#fiscalReportTable tbody").on('click', 'tr', function () {
            if ($(this).find('input[type="checkbox"]').is(':checked')) {
                $(this).find('input[type="checkbox"]').prop('checked', false);
            } else {
                $(this).find('input[type="checkbox"]').prop('checked', true);
            }

        })

        fiscalReports.update_table(table);
        fiscalReports.tableId = tableId;

        table.on('page.dt', function () {
            $('#fiscalReportTable .actionsButton').each(function () {
                $(this).click(function () {
                    fiscalReports.downloadFile($(this).prop('id'));
                });
            });
        });
        fiscalReports.initialized = true;
    },
    update_table: function (tableInst) {
        $.ajax({
            url: fiscalReports.actions.fiscalReportTable + '?pageNumber=1&size=1000&' + $('#fiscalReportsForm').serialize(),
            type: 'GET',
            success: function (res) {
                global_js.dtLoader(fiscalReports.tableId);
                var obj = JSON.parse(res);

                if (obj.hasOwnProperty('data') && obj.data.length > 0) {
                    var allData = obj.data;
                    var dtData = [];
                    fiscalReports.reportActions = {};
                    for (var j in allData) {
                        dtData.push(fiscalReports.buildRow(allData[j]));
                    }
                    tableInst.rows.add(dtData);
                    tableInst.draw();
                    $(".selectRsltTbl").select2('destroy');
                    var aa = 0;
                    var apiDt = $("#" + fiscalReports.tableId).dataTable().api();
                    apiDt.columns().every(function () {
                        var column = this;

                        if (aa != 5) {
                            $("#" + fiscalReports.tableId + " thead tr:eq(1) th:eq(" + aa + ")").html('');

                            var select = $('<select class="selectRsltTbl"><option value=""></option></select>')
                                    .appendTo($("#" + fiscalReports.tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                        column.search(val ? '^' + val + '$' : '', true, false).draw();
                                    });

                            column.data().unique().sort().each(function (d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>')
                            });
                        }
                        aa++;
                    });
                    $("#" + fiscalReports.tableId + " thead .selectRsltTbl").select2({
                        placeholder: "Search",
                        allowClear: true,
                        dropdownAutoWidth: true,
                        width: '98%'
                    });
                    $.fn.dataTableExt.oSort["dateField-desc"] = function (x, y) {
                        var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                        var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                        return (a.diff(b));
                    };

                    $.fn.dataTableExt.oSort["dateField-asc"] = function (x, y) {
                        var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                        var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                        return (b.diff(a));
                    }
                    $('#' + fiscalReports.tableId + ' .select2-arrow').hide();
                    $('#fiscalReportTable .actionsButton').each(function () {
                        $(this).click(function () {
                            fiscalReports.downloadFile($(this).prop('id'));
                        });
                    });
                } else {
                    $(".dataTables_empty").addClass('text-center').removeClass('dataTables_empty');
                }
                global_js.initCheckbox(fiscalReports.tableId);
            },
            beforeSend: function () {
                $(".dataTables_empty").addClass('text-center').removeClass('dataTables_empty');
                global_js.dtLoader(fiscalReports.tableId, 'start');
            }
        })
    },
    buildRow: function (data) {
        fiscalReports.reportActions[data['id']] = data['actions'];
        var rowData = [];
        for (var i in batch_fiscal_header) {
            if (i == 'select') {
                rowData.push('<label class="CHKcontainer"><input type="checkbox" data-file_name="' + data['file_name'] + '" data-batch_id="' + data['batch_id'] + '" data-batch_number="' + data['batch_number'] + '" id="batchAckSelect_' + data['id'] + '" value="1" checked="checked" /><span class="CHKcheckmark"></span></label>');
            } else if (i == 'actions') {
                if (data['actions'].hasOwnProperty('download_txt')) {
                    var html = '<button type="button" class="btn btn-blue btn-squared actionsButton" id="downloadRaMessage_' + data['id'] + '"><i class="clip-download"></i> Download</button>';
                    rowData.push(html);
                }
            } else {
                rowData.push(data[i]);
            }
        }
        return rowData;
    },
    initializePager: function () {
        $('#fiscalReportTable thead th.select').addClass('sorter-false');
        $('#fiscalReportTable thead th.actions').addClass('sorter-false');
        $('#fiscalReportTable thead th:last').append(quickClaim.tablesorterTooltip);
        $('#fiscalReportTable .sortTip').tooltip();

        $('#fiscalReportTable').tablesorter({
            theme: 'blue',
            headerTemplate: '{content} {icon}',
            widgets: ['selectRow', 'zebra', 'pager'],
            sortList: fiscalReports.paginationSort
        });
        $('#fiscalReportTable').tablesorterPager({
            container: $('.fiscalReportPager'),
            size: fiscalReports.paginationSize,
            cssPageSize: '.pagesize',
            ajaxUrl: fiscalReports.actions.fiscalReportTable + '?{sortList:sort}&pageNumber={page}&size={size}',
            customAjaxUrl: function (table, url) {
                return url + '&' + $('#fiscalReportsForm').serialize();
            },
            ajaxObject: {
                dataType: 'json',
                complete: function () {
                    quickClaim.ajaxLocked = false;
                    $('#' + fiscalReports.messages.spinner).hide();

                    $('#fiscalReportTable .actionsButton').each(function () {
                        $(this).button();
                        $(this).click(function () {
                            fiscalReports.downloadFile($(this).prop('id'));
                        });
                    });

                },
                beforeSend: function () {
                    quickClaim.ajaxLocked = true;
                    $('#raMessages .icons div').hide();
                    $('#' + fiscalReports.messages.spinner).show();
                    $('#' + fiscalReports.messages.text).text('Requesting Fiscal Reports').show();
                },
                error: function () {
                    $('#' + fiscalReports.messages.error).show();
                    $('#' + fiscalReports.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            },
            ajaxProcessing: function (rs) {
                quickClaim.hideOverlayMessage(1);

                $('#' + fiscalReports.messages.text).text('Received Fiscal Report List.').show();
                $('#' + fiscalReports.messages.checkmark).show();

                if (rs.hasOwnProperty('data')) {
                    fiscalReports.reportActions = {};
                    fiscalReports.pagerColumns = rs.headers;

                    var data = rs.data;
                    var rows = '';
                    var includeSelect = rs.hasOwnProperty('include_select') && rs.include_select;

                    for (var a in data)
                    {
                        rows += '<tr id="' + data[a].id + '">';
                        rows += fiscalReports.getTableRow(data[a], data[a].id, includeSelect);
                        rows += '</tr>' + "\n";
                    }

                    return [rs.count, $(rows)];
                }
            }
        });
    },
    getTableRow: function (data, id, includeSelect) {
        var rs = '';
        fiscalReports.reportActions[id] = data['actions'];

        if (includeSelect) {
            rs += '<td class="select"><input type="checkbox" id="raSelect_' + id + '" checked="checked" /></td>';
        }

        for (var b in fiscalReports.pagerColumns)
        {
            if (b == 'actions') {
                rs += '<td class="actions">';
                if (data['actions'].hasOwnProperty('download_txt')) {
                    rs += '<button type="button" class="btn btn-blue btn-squared actionsButton" id="downloadRaMessage_' + id + '"><i class="clip-download"></i> Download</button>';
                }
                rs += '</td>';
            } else {
                rs += '<td class="' + b + '">' + data[b] + '</td>';
            }
        }
        return rs;
    },
    clearForm: function () {
        $('#fiscalReportsForm').clearForm();
    },

    downloadFile: function (buttonID) {
        var id = buttonID.replace('downloadRaMessage_', '');

        var url = fiscalReports.actions.downloadFile + '?id=' + id + '&raMessageDownloadToken=' + hypeFileDownload.blockResubmit();
        hypeFileDownload.tokenName = "raMessageDownloadToken";
        hypeFileDownload.setReturnFunction(fiscalReports.downloadComplete);

        quickClaim.showOverlayMessage('Building Fiscal Report Document');

        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    downloadComplete: function () {
        quickClaim.hideOverlayMessage();
    }

};
