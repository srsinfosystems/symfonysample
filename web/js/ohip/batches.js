var batches = {
    batchId: '456',
    actions: {
        batches_table: null,
        details: null,
        download: null,
        downloadBatchDetails: null,
        resend: null,
        reverse: null,
        summaryPDF: null
    },
    messages: {
        error: 'batchesErrorIcon',
        spinner: 'batchesSpinner',
        checkmark: 'batchesCheckmark',
        text: 'batchesMessageText',
        spinnerHtml: '',
        checkmarkHtml: '',
        errorHtml: ''
    },
    tableId: '',
    actionsColspan: 0,
    paginationSize: 25,
    paginationSort: null,
    resendIds: null,
    reverseIds: null,
    archiveIds: null,
    pagerColumns: {},
    initialized: false,
    batchActions: {},

    initialize: function () {
        batches.init_batches_table();
        sk.datepicker('#batch_overview_start_date');

        $('#batch_overview_start_date').on('changeDate', function (e) {
            $('#batch_overview_end_date').skDP('setStartDate', e.date);
        });
        sk.datepicker('#batch_overview_end_date');

        $('#batch_overview_end_date').on('changeDate', function (e) {
            $('#batch_overview_start_date').skDP('setEndDate', e.date);
        });


        $('#batches_button').click(function () {
            var datatable = $("#batches_table").DataTable();
            datatable.clear().draw();
            batches.update_batches_table(datatable);
        });

        $('#batches_reset_form_button')
                .click(function () {
                    batches.clearForm();
                });

        $('#resend_selected_button')
                .click(function () {
                    batches.resendBatches();
                });

        $('#reverse_selected_button')
                .click(function () {
                    batches.reverseBatches();
                });

        $('#summaryPDF_button')
                .click(function () {
                    global_js.datatableSelected = false;
                    $('#messages .icons div').hide();
                    $('#' + moh.messages.spinner).show();
                    $('#' + moh.messages.text).text('Building Summary PDF').show();
                    // setTimeout(function () {
                    //     $('#' + moh.messages.spinner).hide();
                    //     $('#' + moh.messages.text).text('Building Summary PDF').hide();
                    // }, 2000)
                    // $(".pdfHtml5Btn").trigger('click');


                    //New code for genrate pdf which one checkboxes is checked
                    var table = $('#batches_table').DataTable();
                    table.rows('.selected', {page: 'current'}).deselect();
                    setTimeout(function () {
                        $("#batches_table .edit_checkbox input:checkbox").each(function() {
                         $('tr').removeClass('selected');
                        if ($(this).is(":checked")) {
                            $("#" + $(this).attr("id")).parent().parent().parent().addClass('selected');
                            table.row('.selected', {page: 'current'}).select();
                            $('tr').removeClass('selected');
                        } 
                        });
                    }, 500);
                    setTimeout(function () {
                        $('#' + moh.messages.spinner).hide();
                        $('#' + moh.messages.text).text('Building Summary PDF').hide();
                         $(".pdfHtml5BtnSingle").trigger('click');
                    }, 1200)

                    setTimeout(function () {
                      table.rows( { selected: true } ).deselect();
                    }, 2000)
                    //New code end for genrate pdf which one checkboxes selected
                    //batches.getSummaryPDF();
                });

        $('#batchReverseAreYouSure').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 200,
            modal: true,
            width: 400,
            buttons: [
                {
                    text: "Yes",
                    "class": 'btn btn-success btn-squared',
                    click: function () {
                        batches.sendReverseBatchAjax();
                        $('#batchReverseAreYouSure').dialog('close');
                    }
                },
                {
                    text: "No",
                    "class": 'btn btn-danger btn-squared',
                    click: function () {
                        $('#batchReverseAreYouSure').dialog('close');
                        $('#batchReverseDialog').dialog('close');
                    }
                }
            ]
        });

        $('#archiveBatchButton')
                .click(function () {
                    batches.archiveBatches();
                });

        $('#batchArchiveDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: $(window).height() - 100,
            modal: true,
            width: $(window).width() - 100
        });

        $('#batchArchiveAreYouSure').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 300,
            modal: true,
            width: 400,
            buttons: [
                {
                    text: "Yes",
                    "class": 'btn btn-success',
                    click: function () {
                        batches.sendArchiveBatchAjax();
                        $('#batchArchiveAreYouSure').dialog('close');
                    }
                },
                {
                    text: "No",
                    "class": 'btn btn-danger',
                    click: function () {
                        $('#batchArchiveAreYouSure').dialog('close');
                        $('#batchArchiveDialog').dialog('close');
                    }
                }
            ]
        });

        batches.details.initialize();
    },
    init_batches_table: function () {
        // thead
        var colHeader = [];
        for (var i in batch_table_header) {
            if (i == 'select') {
                colHeader.push({title: '<label class="CHKcontainer"><input type="checkbox" id="batchesTableSelectAll" value="1" checked="checked" /><span class="CHKcheckmark"></span></label>', class: 'edit_checkbox'});
            } else {
                var arr = i.split('_');
                if ($.inArray('date', arr) != -1 || i == 'dob') {
                    colHeader.push({title: batch_table_header[i], class: i, sType: 'dateField', bSortable: true});
                } else {
                    colHeader.push({title: batch_table_header[i], class: i});
                }
            }
        }
        var filterFirst = false;
        if (filterFirst == false) {
            var coldef = [{"targets": 0, "orderable": false}];
        } else {
            var coldef = {};
        }

        var tableId = 'batches_table';
        var table = global_js.dataTableWithLoader(tableId, false, ['pdf'], true, colHeader);
        $(".dataTables_empty").addClass('text-center').removeClass('dataTables_empty');

        $("#batches_table tbody").on('click', 'tr', function () {
            if ($(this).find('input[type="checkbox"]').is(':checked')) {
                $(this).find('input[type="checkbox"]').prop('checked', false);
            } else {
                $(this).find('input[type="checkbox"]').prop('checked', true);
            }

        })

        batches.update_batches_table(table);
        batches.tableId = tableId;
        table.on('page.dt', function () {
            setTimeout(function () {
                $("#" + batches.tableId + " tbody .actionsButton").each(function () {
                    $(this).on('hover', function () {
                        var batchAckID = $(this).attr('id').replace('actions_', '');
                        batches.contextMenu.setTitle('Batch ' + batchAckID);
                        batches.contextMenu.setActiveItems(batchAckID);
                    })
                })
            }, 1000)
        });
        $(".pdfHtml5Btn").hide();
        $(".pdfHtml5BtnSingle").hide();
    },
    update_batches_table: function (tableInst) {
        $.ajax({
            url: batches.actions.batches_table + '?pageNumber=1&size=500&' + $('#batches_form').serialize(),
            type: 'GET',
            success: function (res) {
                global_js.dtLoader(batches.tableId);
                var obj = JSON.parse(res);
                if (obj.hasOwnProperty('data')) {
                    var allData = obj.data;
                    batches.batchActions = {};
                    var dtData = [];
                    for (var j in allData) {
                        dtData.push(batches.buildRow(allData[j]));
                    }
                    tableInst.rows.add(dtData);
                    tableInst.draw();
                    $(".selectRsltTbl").select2('destroy');
                    var aa = 0;
                    var apiDt = $("#" + batches.tableId).dataTable().api();
                    apiDt.columns().every(function () {
                        var column = this;
                          var columnText = $.trim($(column.header())[0].innerText);
                        if (aa != 0) {
                            $("#" + batches.tableId + " thead tr:eq(1) th:eq(" + aa + ")").html('');

                            var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                    .appendTo($("#" + batches.tableId + " thead tr:eq(1) th:eq(" + aa + ")")/*$("#" + pastClaims.resultsTable + " thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                                $(this).val()
                                                );
                                        val =  val.replace(/\\/gi, "");
                                        column
                                                .search(val ? '^' + val + '$' : '', true, false)
                                                .draw();
                                    });

                            column.data().unique().sort().each(function (d, j) {
                               if(d != '') {
                               select.append('<option value="' + d + '">' + d + '</option>');
                               }
                            });
                        }
                        aa++;
                    });

                    $.fn.dataTableExt.oSort["dateField-desc"] = function (x, y) {
                        var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                        var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                        return (a.diff(b));
                    };

                    $.fn.dataTableExt.oSort["dateField-asc"] = function (x, y) {
                        var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                        var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                        return (b.diff(a));
                    }

                    $(".selectRsltTbl").select2({
                        placeholder: "Search",
                        allowClear: true,
                        dropdownAutoWidth: true,
                        width: '98%'
                    });
                    $('#' + batches.tableId + ' .select2-arrow').hide();
                    setTimeout(function () {
                        $("#" + batches.tableId + " tbody .actionsButton").each(function () {
                            $(this).on('hover', function () {
                                var batchAckID = $(this).attr('id').replace('actions_', '');
                                batches.contextMenu.setTitle('Batch ' + batchAckID);
                                batches.contextMenu.setActiveItems(batchAckID);
                            })
                        })
                    }, 500);
                }
                global_js.initCheckbox(batches.tableId);
            },
            beforeSend: function () {
                global_js.dtLoader(batches.tableId, 'start');
            }
        })
    },
    buildRow: function (data) {
        batches.batchActions[data['id']] = data['actions'];
        var rowData = [];
        for (var i in batch_table_header) {
            if (i == 'select') {
                rowData.push('<label class="CHKcontainer"><input type="checkbox" data-file_name="' + data['file_name'] + '" data-batch_id="' + data['batch_id'] + '" data-batch_number="' + data['batch_number'] + '"  id="batchSelect_' + data['id'] + '" value="1" checked="checked" /><span class="CHKcheckmark"></span></label>');
            } else if (i == 'actions') {
                var html = '<img class="actionsButton" id="actions_' + data['id'] + '" src="' + batches.contextMenu.menuHtml + '" />';
                rowData.push(html);
            } else {
                rowData.push(data[i]);
            }
        }
        return rowData;
    },
    contextMenu: {
        active: {
            resend: true,
            download: true,
            summaryPDF: true,
            details: true,
            detailsPDF: true,
            archive: true
        },
        menuHtml: null,

        initialize: function () {
            var parameters = {
                selector: '#batches_table .actionsButton',
                trigger: 'hover',
                autoHide: true,
                className: 'batchesMenu',
                callback: function (key, options, event) {
                },
                items: {
                    reverse: {
                        name: 'Reverse Batch',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('actions_', '');
                            batches.reverseBatches(id);
                        },
                        disabled: function () {
                            return !batches.contextMenu.active['reverse'];
                        }
                    },
                    resend: {
                        name: 'Re-Send EDT',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('actions_', '');
                            batches.resendBatches(id);
                        },
                        disabled: function () {
                            return !batches.contextMenu.active['resend'];
                        }
                    },
                    download: {
                        name: 'Download Batch',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('actions_', '');
                            batches.downloadBatch(id);
                        },
                        disabled: function () {
                            return !batches.contextMenu.active['download'];
                        }
                    },
                    summaryPDF: {
                        name: 'Batch Summary [PDF]',
                        callback: function (key, options, event) {

                            var id = $(options.$trigger).prop('id').replace('actions_', '');

                            batches.batchId = $("#batchSelect_" + id).parent().parent().parent().find('.batch_number').text();
                            var table = $('#batches_table').DataTable();
                            table.rows( '.selected' ).nodes().to$().removeClass( 'selected' );

                            //table.rows('.selected').deselect();
                            //table.row('.selected', {page: 'current'}).deselect();
                            setTimeout(function () {
                                $('tr').removeClass('selected');
                                $("#batchSelect_" + id).parent().parent().parent().addClass('selected');
                                table.row('.selected', {page: 'current'}).select();
                            }, 500);
                            setTimeout(function () {
                                $(".pdfHtml5BtnSingle").trigger('click');
                            }, 1000)

                        },
                        disabled: function () {
                            return !batches.contextMenu.active['summaryPDF'];
                        }
                    },
                    details: {
                        name: 'Batch Details',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('actions_', '');
                            batches.details.getDetailsReport(id);
                        },
                        disabled: function () {
                            return !batches.contextMenu.active['details'];
                        }
                    },
                    detailsPDF: {
                        name: 'Batch Details [PDF]',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('actions_', '');
                            batches.downloadBatchDetaisPdfCnt = 0;
                            batches.downloadBatchDetailsPDF(id);
                        },
                        disabled: function () {
                            return !batches.contextMenu.active['detailsPDF'];
                        }
                    },
                    sep1: '---',
                    archive: {
                        name: 'Archive Batch',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('actions_', '');
                            batches.archiveBatches(id);
                        },
                        disabled: function () {
                            return !batches.contextMenu.active['archive'];
                        }
                    }
                }
            };

            $.contextMenu(parameters);
            batches.contextMenu.setTitle('Placeholder Title');
        },
        setup: function (id) {
            $('#' + id).mouseover(function () {
                var batchID = $(this).prop('id').replace('actions_', '');
                var batchNumber = $('#batches_table #' + batchID + ' .batch_number').text();

                batches.contextMenu.setTitle('Batch ' + batchNumber);
                batches.contextMenu.setActiveItems(batchID);
            });
        },
        setActiveItems: function (batchID) {
            var actions = batches.batchActions[batchID];
            var menuItems = ['reverse', 'resend', 'download', 'summaryPDF', 'details', 'detailsPDF', 'archive'];

            for (var a = 0; a < menuItems.length; a++) {
                if (actions.hasOwnProperty(menuItems[a]) && actions[menuItems[a]] != '') {
                    batches.contextMenu.enable(menuItems[a]);
                } else {
                    batches.contextMenu.disable(menuItems[a]);
                }
            }
        },
        disable: function (item) {
            batches.contextMenu.active[item] = false;
        },
        enable: function (item) {
            batches.contextMenu.active[item] = true;
        },
        setTitle: function (title) {
            $('.batchesMenu').attr('data-menutitle', title);
        }
    },
    details: {
        actions: {
            report: null
        },
        batchID: null,
        batchNumber: null,
        messages: {
            error: 'batchDetailsErrorIcon',
            spinner: 'batchDetailsSpinner',
            checkmark: 'batchDetailsCheckmark',
            text: 'batchDetailsMessageText',
            spinnerHtml: '',
            checkmarkHtml: '',
            errorHtml: ''
        },
        drcode: "",
        pagerInitialized: false,
        paginationSize: 25,
        paginationSort: [],

        initialize: function () {
            $('#batchDetailsDialog').dialog({
                closeOnEscape: true,
                autoOpen: false,
                height: $(window).height() - 200,
                modal: true,
                width: '80%',
                close: function () {
                    $('#batchDetailsReportTable tbody tr').remove();
                    $('.batchDetailsReportPager .pagedisplay').text('');
                }
            });

            $('#archiveSingleBatchButton')
                    .button()
                    .click(function () {
                        batches.archiveBatches(batches.details.batchID);
                    });
        },
        initializePager: function () {
            if (batches.details.pagerInitialized) {
                $('#batchDetailsReportTable').trigger('pageSet');
                return;
            }

            batches.details.pagerInitialized = true;
            $('#batchDetailsReportTable thead th.status_text').addClass('sorter-false');

            $('#batchDetailsReportTable thead th:last').append(quickClaim.tablesorterTooltip);
            $('#batchDetailsReportTable').tablesorter({
                theme: 'blue',
                headerTemplate: '{content} {icon}',
                //widgets: ['selectRow', 'zebra', 'pager'],
                //sortList: batches.details.paginationSort
            });
            $('#batchDetailsReportTable .sortTip').tooltip();

            $('#batchDetailsReportTable').tablesorterPager({
                container: $('.batchDetailsReportPager'),
                size: batches.details.paginationSize,
                cssPageSize: '.pagesize',
                ajaxUrl: batches.details.actions.report + '?{sortList:sort}&pageNumber={page}&size={size}',
                customAjaxUrl: function (table, url) {
                    return url + '&id=' + batches.details.batchID;
                },
                ajaxObject: {
                    dataType: 'json',
                    complete: function () {
                        moh.ajaxRunning = false;
                        $('#' + batches.details.messages.spinner).hide();
//							$('.actionsButton').each(function() {
//							batches.contextMenu.setup($(this).prop('id'));
//							});

                        global_js.customFooterDataTableWithOptions('batchDetailsReportTable', true, ['pdf'], false);
                    },
                    beforeSend: function () {
                        global_js.datatableDinit('batchDetailsReportTable');
                        moh.ajaxRunning = true;
                        $('#batchDetailsMessages .icons div').hide();
                        $('#' + batches.details.messages.spinner).show();
                        $('#' + batches.details.messages.text).text('Requesting Batch Details Report').show();
                    },
                    error: function () {
                        $('#' + batches.details.messages.error).show();
                        $('#' + batches.details.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                    }
                },
                ajaxProcessing: function (rs) {
                    quickClaim.hideOverlayMessage(1);

                    $('#' + batches.details.messages.text).text('Received Batch Details Report.').show();
                    $('#' + batches.details.messages.checkmark).show();


                    if (rs === false) {
                        $('#' + batches.messages.text).text('The Claims or Items in this batch could not be received from the History tables.').show();
                        $('#' + batches.messages.error).show();
                        $('#' + batches.messages.checkmark).hide();

                        $('#batchDetailsDialog').dialog('close');
                        quickClaim.showOverlayMessage('The Claims or Items in this batch could not be received from the History tables.');
                        quickClaim.overlayTimer = setTimeout(function () {
                            quickClaim.hideOverlayMessage(1000);
                        }, 3000);

                        return false;
                    } else if (rs.hasOwnProperty('data')) {
                        batches.details.pagerColumns = rs.headers;

                        var data = rs.data;
                        var rows = '';

                        for (var a in data)
                        {
                            rows += '<tr id="claim_id_' + data[a].id + '">';
                            rows += batches.details.getDetailsTableRow(data[a], data[a].id);
                            rows += '</tr>' + "\n";
                        }

                        return [rs.count, $(rows)];
                    }
                }
            });
        },
        getDetailsTableRow: function (data, id) {
            var rs = '';

            for (var b in batches.details.pagerColumns) {
                rs += '<td class="' + b + '">' + data[b] + '</td>';
            }
            return rs;
        },
        getDetailsReport: function (id) {
            if (!id.length) {
                quickClaim.showOverlayMessage('No batches selected to build details report for at this time.');
                setTimeout(function () {
                    quickClaim.hideOverlayMessage();
                }, 2000);
                return;
            }
            batches.details.batchID = id;
            batches.details.drcode = $('#batchSelect_' + id).parent().parent().parent().find('.doctor_name').text();
            batches.details.batchNumber = $('#batchSelect_' + id).attr('data-batch_number');
            var bdrc = batches.details.drcode != '' ? batches.details.drcode : '';
            $("#stage_track_pdf_option_header").val('Batch ' + batches.details.batchNumber + ' ' + bdrc);
            $('#batchDetailsReportBatchNumber').text(' Batch Number ' + batches.details.batchNumber);

            batches.details.initializePager();
            $('#batchDetailsDialog').dialog('open');
        }
    },

    collectIds: function (prefix, id) {
        var ids = new Array();
        if (!id) {

            $('#batches_table tbody input[type="checkbox"]:checked').each(function () {
                var batchID = $(this).attr('id').replace('batchSelect_', '');

                if (batches.batchActions.hasOwnProperty(batchID) && batches.batchActions[batchID].hasOwnProperty(prefix) && batches.batchActions[batchID][prefix]) {
                    ids[ids.length] = batchID;
                } else {
                    $(this).prop('checked', false);
                }
            });
        } else {
            ids[ids.length] = id;
        }

        return ids;
    },
    getBatchTableRow: function (data, id, includeSelect) {
        var rs = '';
        batches.batchActions[id] = data['actions'];

        if (includeSelect) {
            rs += '<td class="select">'
                    + '<label class="CHKcontainer">'
                    + '<input type="checkbox" id="batchSelect_' + id + '" checked="checked" />'
                    + ' <span class="CHKcheckmark"></span>'
                    + ' </label>'
                    + '</td>';
        }

        for (var b in batches.pagerColumns)
        {
            if (b == 'actions') {
                rs += '<td class="actions">';
                rs += '<img class="actionsButton" id="actions_' + id + '" src="' + batches.contextMenu.menuHtml + '" />';
                rs += '</td>';
            } else {
                rs += '<td class="' + b + '">' + data[b] + '</td>';
            }
        }
        return rs;
    },
    clearForm: function () {
        $('#batches_form').clearForm();
        $("select").trigger('change');
        $("#batches_button").trigger('click');
    },
    archiveBatches: function (id) {
        var ids = batches.collectIds('archive', id);

        if (!ids.length) {
            quickClaim.showOverlayMessage('No selected Batches can be archived.');
            setTimeout(function () {
                quickClaim.hideOverlayMessage();
            }, 2000);
            return;
        }

        $('#batchArchiveTable tbody tr').remove();
        for (var a = 0; a < ids.length; a++) {
            var fileName = $('#batchSelect_' + ids[a]).attr('data-file_name');
            var batchNumber = $('#batchSelect_' + ids[a]).attr('data-batch_number');

            var tr = '<tr id="archiveBatch_' + ids[a] + '">'
                    + '<td class="statuses">&nbsp;</td>'
                    + '<td class="fileName">' + fileName + '</td>'
                    + '<td class="batchNumber">' + batchNumber + '</td>'
                    + '<td class="message">&nbsp;</td>'
                    + '</tr>';

            $('#batchArchiveTable tbody').append($(tr));
        }
        $('#batchArchiveTable').trigger('update');

        batches.archiveIds = ids;
        $('#batchArchiveDialog').dialog('open');
        $('#batchArchiveAreYouSure').dialog('open');
    },
    downloadBatch: function (id) {
        hypeFileDownload.tokenName = "batchDownloadToken";
        hypeFileDownload.setReturnFunction(batches.downloadComplete);
        var url = batches.actions.download + "?id=" + id + '&batchDownloadToken=' + hypeFileDownload.blockResubmit();

        $('#messages .icons div').hide();
        $('#' + moh.messages.spinner).show();
        $('#' + moh.messages.text).text('Downloading Batch File').show();
        quickClaim.showOverlayMessage('Downloading Batch File');

        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    chkDialogIsOpen: function (id) {
        if ($('#' + id).dialog('isOpen')) {
            return true;
        }
        return false;
    },
    downloadBatchDetaisPdfCnt: 0,
    downloadBatchDetailsPDF: function (id) {

        if (batches.chkDialogIsOpen('batchDetailsDialog')) {
            if (moh.ajaxRunning == false) {
                $("#batchDetailsReportTable_wrapper .buttons-pdf").trigger('click');
                $('#batchDetailsDialog').dialog("close");
                quickClaim.hideOverlayMessage(500);
                console.log('else');
            } else {
                console.log('if else')
                setTimeout(function () {
                    batches.downloadBatchDetailsPDF(id);
                }, 1000);
            }
        } else {
            if (batches.downloadBatchDetaisPdfCnt < 1) {
                quickClaim.showOverlayMessage('Downloading Batch Details PDF');
                batches.downloadBatchDetaisPdfCnt++;
                batches.details.getDetailsReport(id);
                setTimeout(function () {
                    batches.downloadBatchDetailsPDF(id);
                }, 100);
            }

        }



        return false;
        hypeFileDownload.tokenName = "batchDetailsPDFToken";
        hypeFileDownload.setReturnFunction(batches.downloadComplete);
        var url = batches.actions.downloadBatchDetails + "?id=" + id + '&batchDetailsPDFToken=' + hypeFileDownload.blockResubmit();

        $('#messages .icons div').hide();
        $('#' + moh.messages.spinner).show();
        $('#' + moh.messages.text).text('Downloading Batch Details PDF').show();
        quickClaim.showOverlayMessage('Downloading Batch Details PDF');
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    downloadComplete: function () {
        $('#messages .icons div').hide();
        if ($('#hiddenDownloader').contents().find('body').text()) {
            $('#' + moh.messages.error).show();
            $('#' + moh.messages.text).text('An error occurred while generating the file').show();
        } else {
            $('#' + moh.messages.checkmark).show();
            $('#' + moh.messages.text).text('Downloaded the file').show();
        }
        quickClaim.hideOverlayMessage();
    },
    getSummaryPDF: function (id) {
        var ids = batches.collectIds('summaryPDF', id);

        if (!ids.length) {
            quickClaim.showOverlayMessage('No batches selected to build a summary for at this time.');
            setTimeout(function () {
                quickClaim.hideOverlayMessage();
            }, 2000);
            return;
        }

        $('#messages .icons div').hide();
        $('#' + moh.messages.spinner).show();
        $('#' + moh.messages.text).text('Building Summary PDF').show();
        //quickClaim.showOverlayMessage('Building Summary PDF');

        moh.setupIframeWithURL(batches.actions.summaryPDF, 'ids=' + ids, 'batches');
    },
    resendBatches: function (id) {
        var ids = batches.collectIds('resend', id);

        if (!ids.length) {
            quickClaim.showOverlayMessage('No selected Batches can be re-sent at this time.');
            setTimeout(function () {
                quickClaim.hideOverlayMessage();
            }, 2000);
            return;
        }

        $('#batchResendTable tbody tr').remove();
        for (var a = 0; a < ids.length; a++) {
            var fileName = $('#batchSelect_' + ids[a]).attr('data-file_name');
            var batchNumber = $('#batchSelect_' + ids[a]).attr('data-batch_number');

            var tr = '<tr id="resendEdt_' + ids[a] + '">'
                    + '<td class="statuses">&nbsp;</td>'
                    + '<td class="fileName">' + fileName + '</td>'
                    + '<td class="batchNumber">' + batchNumber + '</td>'
                    + '<td class="message">&nbsp;</td>'
                    + '</tr>';
            $('#batchResendTable tbody').append($(tr));
        }
        $('#batchResendTable').trigger('update');

        batches.resendIds = ids;
        $('#batchResendDialog').dialog('open');
        batches.sendResendBatchAjax();
    },

    reverseBatches: function (id) {
        var ids = batches.collectIds('reverse', id);

        if (!ids.length) {
            quickClaim.showOverlayMessage('No selected Batches can be reversed at this time.');
            setTimeout(function () {
                quickClaim.hideOverlayMessage();
            }, 2000);
            return;
        }

        $('#batchReverseTable tbody tr').remove();
        for (var a = 0; a < ids.length; a++) {
            var fileName = $('#batchSelect_' + ids[a]).attr('data-file_name');

            var batchNumber = $('#batchSelect_' + ids[a]).attr('data-batch_number');

            var tr = '<tr id="reverseBatch_' + ids[a] + '">'
                    + '<td class="statuses">&nbsp;</td>'
                    + '<td class="fileName">' + fileName + '</td>'
                    + '<td class="batchNumber">' + batchNumber + '</td>'
                    + '<td class="message">&nbsp;</td>'
                    + '</tr>';

            $('#batchReverseTable tbody').append($(tr));
        }
        $('#batchReverseTable').trigger('update');

        batches.reverseIds = ids;
        $('#batchReverseDialog').dialog('open');
        $('#batchReverseAreYouSure').dialog('open');
    },
    selectAll: function (selected) {
        $('#batches_table tbody td.select input').prop('checked', selected);
    },
    sendResendBatchAjax: function () {
        var sendIDs = new Array();
        var extraIDs = new Array();

        for (var a = 0; a < batches.resendIds.length; a++) {
            if (a < 5) {
                sendIDs[sendIDs.length] = batches.resendIds[a];

                $('#resendEdt_' + batches.resendIds[a] + ' td.message').text('Requesting Batch Status Update to RE-SEND EDT');
                $('#resendEdt_' + batches.resendIds[a] + ' td.statuses').html($(batches.messages.spinnerHtml));
                $('#resendEdt_' + batches.resendIds[a]).addClass('currentlySending');
            } else {
                extraIDs[extraIDs.length] = batches.resendIds[a];
            }
        }

        batches.resendIds = extraIDs;

        $.ajax({
            type: 'post',
            dataType: 'json',
            data: 'ids=' + sendIDs.join(','),
            url: batches.actions.resend,
            success: function (rs) {
                for (var a in rs.data) {
                    $('#resendEdt_' + a + ' .message').text('Updated Batch Status to ' + rs.data[a].status_text);
                    if (rs.data[a].status_text == 'RESEND EDT') {
                        $('#resendEdt_' + a + ' .statuses').html(batches.messages.checkmarkHtml);
                    } else {
                        $('#resendEdt_' + a + ' .statuses').html(batches.messages.errorHtml);
                    }
                    $('#resendEdt_' + a).removeClass('currentlySending');

                    var includeSelect = rs.hasOwnProperty('include_select') && rs.include_select;
                    var row = batches.getBatchTableRow(rs.data[a], a, includeSelect);

                    $('#batches_table #' + a).html(row);
                    $('#batches_table #' + a + ' .actions input').button();
                }

                if (batches.resendIds.length) {
                    batches.sendResendBatchAjax();
                } else {
                    $('#batchResendDialog').dialog('close');

                    quickClaim.showOverlayMessage('Refreshing Batches Report');
                    quickClaim.hideOverlayMessage(500)
                    $('#batches_button').trigger('click');
                }
            },
            beforeSend: function () {
                moh.ajaxRunning = true;
                $('#messages .icons div').hide();
                $('#' + moh.messages.spinner).show();
                $('#' + moh.messages.text).text('Resending Batches').show();
            },
            complete: function () {
                moh.ajaxRunning = false;
                $('#' + moh.messages.spinner).hide();
            },
            error: function () {
                $('#' + moh.messages.error).show();
                $('#' + moh.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });
    },
    sendReverseBatchAjax: function () {
        var sendIDs = new Array();
        var extraIDs = new Array();

        for (var a = 0; a < batches.reverseIds.length; a++) {
            if (a < 1) {
                sendIDs[sendIDs.length] = batches.reverseIds[a];

                $('#reverseBatch_' + batches.reverseIds[a] + ' td.message').text('Reversing Batch');
                $('#reverseBatch_' + batches.reverseIds[a] + ' td.statuses').html($(batches.messages.spinnerHtml));
                $('#reverseBatch_' + batches.reverseIds[a]).addClass('currentlySending');
            } else {
                extraIDs[extraIDs.length] = batches.reverseIds[a];
            }
        }

        batches.reverseIds = extraIDs;

        $.ajax({
            type: 'post',
            dataType: 'json',
            data: 'ids=' + sendIDs.join(','),
            url: batches.actions.reverse,
            success: function (rs) {
                for (var a in rs.data) {
                    $('#reverseBatch_' + a + ' .message').text('Reversed Batch');
                    if (rs.data[a].status_text == 'DELETED') {
                        $('#reverseBatch_' + a + ' .statuses').html(batches.messages.checkmarkHtml);
                    } else {
                        $('#reverseBatch_' + a + ' .statuses').html(batches.messages.errorHtml);
                    }
                    $('#reverseBatch_' + a).removeClass('currentlySending');

                    var includeSelect = rs.hasOwnProperty('include_select') && rs.include_select;
                    var row = batches.getBatchTableRow(rs.data[a], a, includeSelect);

                    $('#batches_table #' + a).html(row);
                    $('#batches_table #' + a + ' .actions input').button();
                }

                if (batches.reverseIds.length) {
                    batches.sendReverseBatchAjax();
                } else {
                    $('#batchReverseDialog').dialog('close');

                    quickClaim.showOverlayMessage('Refreshing Batches Report');
                    quickClaim.hideOverlayMessage(500)
                    $('#batches_button').trigger('click');
                }
            },
            beforeSend: function () {
                moh.ajaxRunning = true;
                $('#messages .icons div').hide();
                $('#' + moh.messages.spinner).show();
                $('#' + moh.messages.text).text('Reversing Batch').show();
            },
            complete: function () {
                moh.ajaxRunning = false;
                $('#' + moh.messages.spinner).hide();
            },
            error: function () {
                $('#' + moh.messages.error).show();
                $('#' + moh.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });
    },
    sendArchiveBatchAjax: function () {
        var sendIDs = new Array();
        var extraIDs = new Array();

        for (var a = 0; a < batches.archiveIds.length; a++) {
            if (a < 1) {
                sendIDs[sendIDs.length] = batches.archiveIds[a];

                $('#archiveBatch_' + batches.archiveIds[a] + ' td.message').text('Archiving Batch');
                $('#archiveBatch_' + batches.archiveIds[a] + ' td.statuses').html($(batches.messages.spinnerHtml));
                $('#archiveBatch_' + batches.archiveIds[a]).addClass('currentlySending');
            } else {
                extraIDs[extraIDs.length] = batches.archiveIds[a];
            }
        }

        batches.archiveIds = extraIDs;

        $.ajax({
            type: 'post',
            dataType: 'json',
            data: 'ids=' + sendIDs.join(','),
            url: batches.actions.archive,
            success: function (rs) {
                for (var a in rs.data) {
                    if (rs.data[a].actions.archive == '') {
                        $('#archiveBatch_' + a + ' .message').text('Archived Batch');
                        $('#archiveBatch_' + a + ' .statuses').html(batches.messages.checkmarkHtml);
                    } else {
                        $('#archiveBatch_' + a + ' .statuses').html(batches.messages.errorHtml);
                    }
                    $('#archiveBatch_' + a).removeClass('currentlySending');

                    var includeSelect = rs.hasOwnProperty('include_select') && rs['include_select'];
                    var row = batches.getBatchTableRow(rs.data[a], a, includeSelect);

                    $('#batches_table #' + a).html(row);
                    $('#batches_table #' + a + ' .actions input').button();
                }

                if (batches.archiveIds.length) {
                    batches.sendArchiveBatchAjax();
                } else {
                    $('#batchArchiveDialog').dialog('close');
                    $('#batchDetailsDialog').dialog('close');

                    quickClaim.showOverlayMessage('Refreshing Batches Report');
                    quickClaim.hideOverlayMessage(500)
                    $('#batches_button').trigger('click');
                }
            },
            beforeSend: function () {
                moh.ajaxRunning = true;
                $('#messages').find('.icons div').hide();
                $('#' + moh.messages.spinner).show();
                $('#' + moh.messages.text).text('Archiving Batch').show();
            },
            complete: function () {
                moh.ajaxRunning = false;
                $('#' + moh.messages.spinner).hide();
            },
            error: function () {
                $('#' + moh.messages.error).show();
                $('#' + moh.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });
    }
};
