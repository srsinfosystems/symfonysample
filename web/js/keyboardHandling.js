/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var keyHandling = {
    form:{
        name:'other',
        elements:0,
        currentElement:1,
        tabIndex:0,
        formId:''
    },
    tabs:{
        CurrentTab: 0,
        total:0
    },
    PatientSearch: function(){
        var form = document.querySelector('form');
        keyHandling.form.formId = $(form).attr('id');
        keyHandling.form.elements = $(form).find('input[type=text],[type=radio],[type=select]').length - 2;
        keyHandling.form.name = 'patient';
        Mousetrap(form).bind('tab', keyHandling._handleTab);
    },
    handleTabSwitching:function(){
        keyHandling.tabs.CurrentTab = $( "#tabs" ).tabs( "option", "active" );
        keyHandling.tabs.total = $('#tabs >ul >li[disabled!=true]').size() - 1;
        
        Mousetrap.bind(['command+up', 'ctrl+up', 'command+down', 'ctrl+down'], function(e) {
            if(e.keyCode == 38){
                //right
                if(keyHandling.tabs.CurrentTab >= keyHandling.tabs.total){
                    keyHandling.tabs.CurrentTab = 0;
                }else{
                    keyHandling.tabs.CurrentTab++;
                }
                $('#tabs').tabs('option', 'active', keyHandling.tabs.CurrentTab);
            }else if(e.keyCode == 40){
                //left
                if(keyHandling.tabs.CurrentTab >= keyHandling.tabs.total){
                    keyHandling.tabs.CurrentTab = keyHandling.tabs.total;
                }else{
                    keyHandling.tabs.CurrentTab--;
                }
                $('#tabs').tabs('option', 'active', keyHandling.tabs.CurrentTab);
            }
            return false;
        });
        
    },
    handleEscBtn:function(){
        var form = document.querySelector('form');
    },
    _handleTab: function(evt, input){
        console.log($(evt.path[0]).attr('tabindex'));
        console.log(input);
        return true;
        
        if(keyHandling.form.name == 'patient'){
            
            var lastIndex = keyHandling.form.tabIndex;//$(evt.srcElement).attr('data-tabIndex');
            var newIndex = +lastIndex + 1;
            if(newIndex == 5){
                $("#patient_search_dob").skDP('hide')
            };
            if(lastIndex >= 7){
                keyHandling.form.tabIndex = 0;
                $(form).find('[data-tabIndex="'+keyHandling.form.tabIndex+'"]').focus();
            }else{
                keyHandling.form.tabIndex = newIndex;
                var form = document.querySelector('form');
                console.log(newIndex);
                $(form).find('[data-tabIndex="'+newIndex+'"]').focus();
            }            
        } else {
            if(keyHandling.form.currentElement == keyHandling.form.elements){
                quickClaim.focusTopField();
                keyHandling.form.currentElement = 1;
            }else{
                keyHandling.form.currentElement++;
            }
        }        
    },
    _focusFirstField: function(){
        $("#"+keyHandling.form.formId+" input[type!=hidden]:first").focus();
    }
}
