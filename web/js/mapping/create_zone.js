var zones = {
	currentPolygonIDX: null,
	initialized: false,
	
	map: null,
	mapOptions: {
		zoomControl: true,
		scaleControl: true,
		panControl: true,
		mapTypeControl: false,
		streetViewControl: false,
		overviewMapControl: false,
	},
	torontoLng: -79.383,
	torontoLat: 43.653,
	
	infowWindow: null,
	polygons: new Array(),
	marker: null,

	initialize: function(address, zoom, enableDrawing) {
		var latLng = new google.maps.LatLng(zones.torontoLat, zones.torontoLng);
		
		zones.mapOptions.zoom = zoom;
		zones.mapOptions.center = latLng;
		
		zones.map = new google.maps.Map(document.getElementById('map'), zones.mapOptions);
		zones.initialized = true;
		
		if (enableDrawing) {
			zones.infoWindow = new google.maps.InfoWindow();
			zones.initializeDrawingMode();
			zones.setDrawingModeColour(zones.getColour());
		}

		if (zones.zone_submit_button) {
			$('#' + zones.zone_submit_button).button();
		}
		
		$('#location_zone_zone_colour').blur(function() {
			zones.setDrawingModeColour(zones.getColour());
		});
	},
	initializeDrawingMode: function() {
		zones.drawingManagerOptions = {
			drawingMode: null,
			drawingControl: true,
			drawingControlOptions: {
				position: google.maps.ControlPosition.TOP_CENTER,
				drawingModes: [
	               google.maps.drawing.OverlayType.POLYGON,
	               ]
			},
			polygonOptions: {
			}
		};
		
		zones.drawingManager = new google.maps.drawing.DrawingManager(zones.drawingManagerOptions);
		zones.drawingManager.setMap(zones.map);
		
		google.maps.event.addListener(zones.drawingManager, 'overlaycomplete', function(event) {
			zones.addNewPolygon(event.overlay);
		});
	},
	setDrawingModeColour: function(colour) {
		zones.drawingManagerOptions.polygonOptions.fillColor = colour;
//		zones.drawingManagerOptions.rectangleOptions.fillColor = colour;
		
		zones.drawingManager.setOptions(zones.drawingManagerOptions);
		
		zones.updatePolygonColours(colour);
	},
	centerOnAddress: function(address, zoom) {
		var geocoder = new google.maps.Geocoder();

		geocoder.geocode( { 'address': address },
			function (point) {
				if (!point) {
					alert(address + ' not found');
				}
				else if (zones.map) {
					zones.map.setCenter(point[0].geometry.location, zoom);
				}
			}
		);
	},
	createPointForAddress: function(address, center, zoom) {
		var geocoder = new google.maps.Geocoder();

		if (geocoder) {
			geocoder.geocode(
				{ address: address	},
				function(results, status) {
					 if (status == google.maps.GeocoderStatus.OK) {
				            zones.map.setCenter(results[0].geometry.location);
				            
				            if (zones.marker) {
				            	zones.marker.setMap(null);
				            }
				            
				            zones.marker = new google.maps.Marker({
				            	map: zones.map,
				            	position: results[0].geometry.location,
				            });

							if ($('#zone_address').size()) {
								$('#zone_address').val(address);
							}
					 }
				}
			);
		}
	},
	getColour: function() {
		return $('#location_zone_zone_colour').val() ? $('#location_zone_zone_colour').val() : '#000000';
	},
	updatePointsList: function() {
		var points = '';
		var area = 0;

		var polygons = zones.polygons;
		var bounds = new google.maps.LatLngBounds();

		for (var a = 0; a < polygons.length; a++) {
			if (polygons[a] != null) {
				points += (points == '' ? '' : ';');
				
				var paths = polygons[a].getPaths();
				var path;
				
				for (var i = 0; i < paths.getLength(); i++) {
					path = paths.getAt(i);
					for (var ii = 0; ii < path.getLength(); ii++) {
						points += (points == '' ? '' : ',') + '(' + path.getAt(ii).lat() + ',' + path.getAt(ii).lng() + ')';
					}
				}
				
				area += google.maps.geometry.spherical.computeArea(zones.polygons[a].getPath());
			}
		}

		$('#location_zone_points').val(points.replace(';,', ';'));
		$('#edit_zone_area').html((Math.round(area / 10000) / 100) + "km<sup>2</sup>");
	},
	addNewPolygon: function(event) {
		zones.loadPolygon(null, event.getPath(), zones.getColour(), true, true);
		event.setMap(null);
		
		zones.drawingManager.setDrawingMode(null);
	},
	loadPolygon: function(zone_name, points, colour, editable, clickable) {
		if (typeof points == 'string') {
			points = zones.parsePoints(points);
		}
		
		var index = zones.polygons.length;

		var polygon = new google.maps.Polygon({
			editable: editable,
		    paths: points,
		    strokeColor: colour,
		    strokeOpacity: 0.7,
		    strokeWeight: 2,
		    fillColor: colour,
		    fillOpacity: 0.2
		  });
		polygon.setMap(zones.map);

		if (editable) {
			google.maps.event.addListener(polygon, 'click', function(event) {
				zones.currentPolygonIDX = index;
				zones.showDeleteInfoWindow(event);
			});

			var path = polygon.getPath();
			google.maps.event.addListener(path, 'set_at', function(index, event) {
				zones.updatePointsList(polygon);
			});
			google.maps.event.addListener(path, 'insert_at', function(index, event) {
				zones.updatePointsList(polygon);
			});
		}
		else {
			google.maps.event.addListener(polygon, 'mouseover', function() {
				$('#zone_name').text(zone_name);
			});

			google.maps.event.addListener(polygon, 'mouseout', function() {
				$('#zone_name').html('&nbsp;');
			});
		}

		zones.polygons.push(polygon);
		zones.updatePointsList(polygon);

		return polygon;
	},
	updatePolygonColours: function(colour) {
		var polygons = zones.polygons;

		var polygonOptions = {
			strokeColor: colour,
			fillColor: colour
		};
		
		for (var a = 0; a < polygons.length; a++) {
			if (polygons[a] != null) {
				polygons[a].setOptions(polygonOptions);
			}
		}
		
/*
		
 */		
	},
	parsePoints: function(points) {
		var rs = new Array();

		var numbers = new Array();
		var number = null;

		while(points.length) {

			var first_char = points.charAt(0);

			if (first_char == '(' || first_char == ',' || first_char == ')' || first_char == ' ') {
				points = points.substr(1);
			}
			else {
				number = String(parseFloat(points));

				points = points.substr(number.length);
				numbers[numbers.length] = number;
			}
		}

		for (var a = 0; a < numbers.length; a = a + 2) {
			rs[rs.length] = new google.maps.LatLng(numbers[a], numbers[a + 1]);
		}
		
		return rs;
	},
	showDeleteInfoWindow: function(event) {
		var contentString = '<div onclick="zones.deleteThisPolygon()">Delete this object</div>';
		
		zones.infoWindow.setContent(contentString);
		zones.infoWindow.setPosition(event.latLng);
		zones.infoWindow.open(zones.map);
	},
	deleteThisPolygon: function() {
		var polygon = zones.polygons[zones.currentPolygonIDX];
		
		polygon.setMap(null);
		zones.infoWindow.close();

		zones.polygons[zones.currentPolygonIDX] = null;
		zones.updatePointsList();
	},
	zoomToBounds: function() {
		if (zones.polygons.length == 0) {
			return; 
		}
		
		var polygons = zones.polygons;
		var bounds = new google.maps.LatLngBounds();

		for ( var a = 0; a < polygons.length; a++ ) {
			var paths = polygons[a].getPaths();
			var path;
			
			for (var i = 0; i < paths.getLength(); i++) {
				path = paths.getAt(i);
				for (var ii = 0; ii < path.getLength(); ii++) {
					bounds.extend(path.getAt(ii));
				}
			}
		}

		zones.map.setCenter(bounds.getCenter());
		zones.map.fitBounds(bounds);
	}
	
};
