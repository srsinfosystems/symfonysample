

var reportsDefault = {
    parentDiv: null,
    param: null,
    dialogDiv: null,
    data: null,
    isEdit: false,
    editTitle: '',
    rptType: 'apptsrch',
    ajax: false,
    init: function () {
        reportsDefault.buildDefaultCols();
        $('#' + reportsDefault.dialogDiv).dialog({
            autoOpen: false,
            closeOnEscape: true,
            modal: true,
            width: 900,
            open: function () {
                $("#rptBtnSave").click(function () {
                    reportsDefault.save();
                })
            }
        });

        $("#rptBtnClose").click(function () {
            $('#' + reportsDefault.dialogDiv).dialog('close');
        })

        $("#rptBtnDelete").click(function () {
            var oldData = reportsDefault.data;
            oldData = JSON.parse(oldData);
            var editTitle = reportsDefault.editTitle;

            for (var i in oldData) {
                if (oldData[i].title == editTitle) {
                    oldData.splice(i, 1);
                }
            }
            data = JSON.stringify(oldData);
            reportsDefault.setDataToTable(data);
        })

    },
    buildDefaultCols: function () {
        if (reportsDefault.parentDiv != '') {
            reportsDefault.clear();
            reportsDefault.getDataFromTable();

        }
    },
    buildBtns: function (data) {
        var html = '<span class="alert alert-info" style="position:absolute;top:-35px;padding:5px;"><small><i class="clip-info"></i> To edit a shortcut button press Shift + left click.</small></span>';

        if (data) {
            data = JSON.parse(data);
            for (var i in data) {
                if (data[i]['title'] && data[i]['data']) {
                    html += '<button class="btn btn-teal btn-squared rptColBtn" style="margin-right:2px;" data-values="' + data[i]['data'].join() + '" type="button" value="' + data[i]['title'] + '"  >' + data[i]['title'] + '</button>';
                }
            }
        }
        html += '<button class="btn btn-success btn-squared" type="button" id="AddNewRptBtn" ><i class=" clip-plus-circle"></i> Create new reports shortcut</button>';
        $("#" + reportsDefault.parentDiv).html(html);

        $(".rptColBtn").on('click', function (e) {
            if (e.shiftKey) {
                var id = [];
                id.push({title: $(this).val()});
                id.push({data: $(this).attr('data-values')});
                reportsDefault.editTitle = $(this).val();
                reportsDefault.addEdit(id);
            } else {
                var data = $(this).attr('data-values').split(',');
                $("#mainReportSection input").each(function () {
                    if (data.indexOf($(this).val()) > -1) {
                        $(this).prop('checked', true);
                    } else {
                        $(this).prop('checked', false);
                    }
                })
                $("#mainReportSection input[type='checkbox']:first").trigger('change');
            }

        });


        $("#AddNewRptBtn").on('click', function () {
            reportsDefault.isEdit = false;
            reportsDefault.editTitle = '';
            $("#txtRptTitle").val('');
            reportsDefault.addEdit();
        })
    },
    editRpt: function (title = null) {
        console.log(title);
    },
    addEdit: function (id = null) {
        if (id == null) {
            $('#' + reportsDefault.dialogDiv).dialog('option', 'title', 'Create New Report Column Shortcut');
            $("#rptBtnDelete").hide();
            reportsDefault.isEdit = false;
            reportsDefault.buildDialogContant();
        } else {
            $('#' + reportsDefault.dialogDiv).dialog('option', 'title', 'Edit Report Column Shortcut');
            $("#rptBtnDelete").show();
            reportsDefault.isEdit = true;
            reportsDefault.buildDialogContant(id);
        }

        $('#' + reportsDefault.dialogDiv).dialog('open');
    },
    buildDialogContant: function (editData = null) {
        $("#rptCols").html('');
        if (reportsDefault.rptType == 'apptsrch') {
            //'8', '9', '10', '11', '12', '13', '14', '15', '16', 
            var apptbasedCols = ['17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39'];

            var htmlone = "<h3>Patient Based</h3><ul class='row'>";
            var htmltwo = "<h3>Appointment Based</h3><ul class='row'>";

            $('.reportColoumnsChkbox').each(function () {

                if ($.inArray($(this).attr('data-position'), apptbasedCols) > -1) {
                    htmlone += '<li class="col-sm-3">';
                    htmlone += $(this).parent('li').html();
                    htmlone += '</li>';
                } else {
                    htmltwo += '<li class="col-sm-3">';
                    htmltwo += $(this).parent('li').html();
                    htmltwo += '</li>';
                }
            })

            var html = htmlone + '</ul>' + '<br/>' + htmltwo + '</ul>';
        } else if(reportsDefault.rptType == 'apptstage'){
            
            var htmlone = "<h3>Report Columns</h3><ul class='row'>";

            $('.reportColoumnsChkbox').each(function () {

                    htmlone += '<li class="col-sm-3">';
                    htmlone += $(this).parent('li').html();
                    htmlone += '</li>';
               
            })
            htmlone = htmlone.replace('reportColoumnsChkbox', '');
            var html = htmlone + '</ul>';
        } else if(reportsDefault.rptType == 'billingcycle'){
            var one = ["34", "45", "43", "35", "44", "42", "36", "48", "47", "39", "32", "38", "33", "37"];
            var two = ["0", "30", "41", "1", "29", "40", "2", "28", "46", "3", "31", "27", "4", "10", "6", "8", "7", "5", "9", "49", "150","153","151","152","166"];
            var three = ["11", "13", "20", "12", "15", "21", "16", "14", "23", "17", "25", "23", "25", "19", "26", "18"];

            var htmlone = "<h3>Service Based</h3><ul class='row'>";
            var htmltwo = "<h3>Claim Based</h3><ul class='row'>";
            var htmlthree = "<h3>Patient Based</h3><ul class='row'>";

            $('.reportColoumnsChkbox').each(function () {

                if ($.inArray($(this).attr('data-position'), one) > -1) {
                    htmlone += '<li class="col-sm-3">';
                    htmlone += $(this).parent('li').html();
                    htmlone += '</li>';
                } else if ($.inArray($(this).attr('data-position'), two) > -1) {
                    htmltwo += '<li class="col-sm-3">';
                    htmltwo += $(this).parent('li').html();
                    htmltwo += '</li>';
                } else {
                    htmlthree += '<li class="col-sm-3">';
                    htmlthree += $(this).parent('li').html();
                    htmlthree += '</li>';
                }
            })

            var html = htmlone + '</ul>' + '<br/>' + htmltwo + '</ul>'+ '<br/>' + htmlthree + '</ul>';
        }

        $("#rptCols").html(html);

        if (editData == null) {
            $("#rptCols li").each(function () {
                $(this).find('input').attr('id', 'rpt_' + $(this).attr('id'));
                $(this).find('input').prop('checked', false);
            })
        } else {
            $("#txtRptTitle").val(editData[0].title);
            var vals = editData[1].data.split(',');
            $("#rptCols input").each(function () {
                if (vals.indexOf($(this).val()) > -1) {
                    $(this).prop('checked', true);
                } else {
                    $(this).prop('checked', false);
                }
            })
    }

    },
    getDataFromTable: function (param = reportsDefault.param) {
        if (param) {
            reportsDefault.ajax = true;
            $.ajax({
                type: 'POST',
                url: '/appointment_search/reportparams',
                data: {type: param},
                success: function (rs) {
                    reportsDefault.ajax = false;
                    reportsDefault.data = JSON.parse(rs);

                    reportsDefault.buildBtns(reportsDefault.data);
                },
                error: function (e) {
                    reportsDefault.ajax = false;
                    console.log(e);
                }
            })
    }
    },
    save: function () {
        var title = $("#txtRptTitle").val();
        if (title == '') {
            $("#txtRptTitle").addClass('cliponeErrorInput');
            $("#rptErrors").html('Title is required field').addClass('alert alert-danger sk-margin btn-squared');
            return false;
        } else {
            $("#rptErrors").html('').removeClass('alert alert-danger sk-margin btn-squared');
            $("#txtRptTitle").removeClass('cliponeErrorInput');
        }

        var oldData = reportsDefault.data;
        if (oldData == null) {
            var data = [{title: title, data: reportsDefault.getChkArr()}];
            data = JSON.stringify(data);
            reportsDefault.setDataToTable(data);
        } else {
            if (reportsDefault.isEdit) {
                oldData = JSON.parse(oldData);

                for (var i in oldData) {
                    if (oldData[i].title == reportsDefault.editTitle) {
                        oldData[i].title = title;
                        oldData[i].data = reportsDefault.getChkArr();
                    }

                }
                data = JSON.stringify(oldData);
                reportsDefault.setDataToTable(data);
            } else {
                oldData = JSON.parse(oldData);
                oldData.push({title: title, data: reportsDefault.getChkArr()});
                data = JSON.stringify(oldData);
                reportsDefault.setDataToTable(data);
            }
        }

    },
    waitForAjax: function () {
        setTimeout(function () {
            if (reportsDefault.ajax == true) {
                reportsDefault.waitForAjax();
            } else {
                return true;
            }
        }, 200)
    },
    getChkArr: function () {
        if ($("#rptCols input:checked").length <= 0) {
            $("#rptErrors").html('Please select atleast one option.').addClass('alert alert-danger sk-margin btn-squared');
            return false;
        }

        var data = [];
        $("#rptCols input:checked").each(function () {
            data.push($(this).val());
        })
        return data;
    },
    setDataToTable: function (data = null) {
        if (data) {
            $.ajax({
                type: 'POST',
                url: '/appointment_search/savereportparams',
                data: {type: reportsDefault.param, data: data},
                success: function (rs) {
                    $('#' + reportsDefault.dialogDiv).dialog('close');
                    reportsDefault.getDataFromTable();
                }
            })
    }
    },
    clear: function () {
        $("#" + reportsDefault.parentDiv).html('');
    }
}
/*
 {
 hospital: {
 service_code: true,
 
 },
 hospital day :{
 test: true
 }
 }
 */