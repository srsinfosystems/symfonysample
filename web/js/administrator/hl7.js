var app = {
	hl7: {
		actions: {
			enqueue_item: null,
			item_count: null
		},
		error_tag: null,
		item_count: 0,
		min_id: 0,
		partner: null,
		spinner_tag: null,
		type: null,

		initialize: function() {
			$('#hl7_dump_dialog').dialog({
				closeOnEscape: true,
				autoOpen: false,
                                minWidth:800,
                                minHeight:250,
				height: $(window).height() - 200,
				modal: true,
				width:  '80%'
			});

			$('#hl7_button').button();
			$('.dump_button').button();

			$('.dump_button').click(function() {
				var partner = $(this).closest('div').attr('id');
				partner = partner.replace('partner_', '');

				var dump_type = $(this).attr('id');
				dump_type = dump_type.replace('[' + partner + ']', '').replace('_dump', '');

				app.hl7.dumpSetup(dump_type, partner);
			});
		},
		dumpSetup: function(type, partner) {
			app.hl7.item_count = 0;
			app.hl7.min_id = 0;
			app.hl7.partner = partner;
			app.hl7.type = type;

			$('#process_text ul li').remove();

			var item = '<li>Perform a ' + type + ' dump for partner ' + partner + '.</li>';
			$('#process_text ul').append($(item));

			var item = '<li>Requesting number of ' + app.hl7.type + ' items...</li>';
			$('#process_text ul').append($(item));

			$('#hl7_dump_dialog').dialog('open');
			app.hl7.getItemCounts();
		},
		enqueueItems: function() {
			if (app.hl7.item_count == 0) {
				var item = '<li><b>Completed ' + app.hl7.type + ' HL7 dump. Close this dialog to start another HL7 dump.</b></li>';
				$('#process_text ul').append($(item));
				return;
			}

			var data = 'dump_type=' + app.hl7.type
				+ '&partner=' + app.hl7.partner
				+ '&min_id=' + app.hl7.min_id;

			$.ajax({
				type: 'post',
				dataType: 'json',
				url: app.hl7.actions.enqueue_item,
				data: data,
				success: function(rs) {
					app.hl7.item_count = app.hl7.item_count - parseInt(rs.count, 10);
					app.hl7.min_id = rs.max_id;

					var item = '  ' + rs.count + ' items enqueued. ' + app.hl7.item_count + ' remaining.';
					$('#process_text ul li:last').append(item);

					if (parseInt(rs.count, 10)) {
						app.hl7.enqueueItems();
					}
				},
				beforeSend: function() {
					var item = '<li>Queuing items...</li>';
					$('#process_text ul').append($(item));
					$('#process_text ul li:last').prepend($(app.hl7.spinner_tag));
				},
				complete: function() {
					$('#process_text ul li .spinner').remove();
				},
				error: function() {
					$('#process_text ul li:last').prepend(app.hl7.error_tag);
					$('#process_text ul').append('<li style="color:red">An error occurred while communicating with the server.</li>');
				}
			});
		},
		getItemCounts: function() {
			var data = 'dump_type=' + app.hl7.type + '&partner=' + app.hl7.partner;

			$.ajax({
				type: 'post',
				dataType: 'json',
				url: app.hl7.actions.item_count,
				data: data,
				success: function(rs) {
					var item = '<li><b>' + rs.count + ' total items to enqueue.</b></li>';
					$('#process_text ul').append($(item));

					app.hl7.item_count = parseInt(rs.count, 10);
					app.hl7.enqueueItems();
				},
				beforeSend: function() {
					$('#process_text ul li:last').prepend(app.hl7.spinner_tag);
				},
				complete: function() {
					$('#process_text ul li .spinner').remove();
				},
				error: function() {
					$('#process_text ul li:last').prepend(app.hl7.error_tag);
					$('#process_text ul').append('<li style="color:red">An error occurred while communicating with the server.</li>');
				}
			});
		}
	},
	mobile: {
		actions: {
			createMobileUser: null,
			sendCommand: null
		},
		doctorCodes: {},
		userActions: {},
		users: {},
		
		initialize: function() {
			$('#client_setup_button').button();
			$('#send_client_command').button();
			
			$('#echobase_user_dialog').dialog({
				closeOnEscape: true,
				autoOpen: false,
				height: $(window).height() - 100,
				modal: true,
				width:  $(window).width() - 100,
				open: function() {
					app.mobile.resetCreateUserForm();
					app.mobile.clearErrors();
				}
			});
			
			$('#open_create_dialog').button();
			$('#open_create_dialog').click(function() {
				$('#echobase_user_dialog').dialog('open');
			});
			
			$('#create_user_button').button();
			$('#create_user_button').click(function() {
				app.mobile.createUser();
			});

			$('.send_create_command').button();
			$('.send_invitation_command').button();

			$('#create_user_form input').keypress(function(e) {
				app.mobile.enableFields();
			});
			$('#create_user_form select').change(function(e) {
				app.mobile.enableFields();

				if ($(e.currentTarget).attr('id') == 'user_echobase_user_id') {
					app.mobile.updateUserDetails();
				}
				else if ($(e.currentTarget).attr('id') == 'user_echobase_doctor_id') {
					app.mobile.updateDoctorDetails();
				}

			});
			
			$('#user_echobase_role').closest('tr').before('<tr><td colspan="2">&nbsp;</td></tr>');
			$('#user_echobase_username').closest('tr').before('<tr><td colspan="2">&nbsp;</td></tr>');
			
			for (var a in app.mobile.userActions) {
				app.mobile.updateMobileUsersTableActions(app.mobile.userActions[a].id, app.mobile.userActions[a].actions);
			}
			
			app.mobileContextMenu.initialize();
		},
		clearErrors: function() {
			$('#createEchobaseUserErrors').removeClass('ui-state-error').removeClass('ui-state-highlight').text('').hide();
			$('#create_user_form label.error').remove();
			$('#create_user_form input.error').removeClass('error');
			$('#create_user_form select.error').removeClass('error');
		},
		createUser: function() {
			if (quickClaim.ajaxLocked) {
				return;
			}
			
			app.mobile.clearErrors();
			var data = $('#create_user_form').serialize();
			
			$.ajax({
				type: 'post',
				dataType: 'json',
				url: app.mobile.actions.createMobileUser,
				data: data,
				success: function(rs) {
					if (rs.hasOwnProperty('errors')) {
						quickClaim.showFormErrors(rs.errors, '#createEchobaseUserErrors', '#user_echobase', true);

						$('#createUserMessage_error_icon').show();
						$('#createUserMessage_messages_text').text('The form contains invalid data.').show();
					}
					else {
						for (var a in rs.data) {
							app.mobile.updateMobileUsersTable(rs.data[a]);
							app.mobile.userActions[rs.data[a].id] = rs.data[a];
						}
						$('#echobase_user_dialog').dialog('close');
						$('#users_table').trigger('update');
					}
				},
				beforeSend: function() {
					quickClaim.ajaxLocked = true;
					$('#create_user_button').button('disable');
					
					$('#createUserMessage .icons div').hide();
					$('#createUserMessage_spinner').show();
					$('#createUserMessage_messages_text').text('Saving Hype Mobile User').show();
				},
				complete: function() {
					quickClaim.ajaxLocked = false;
					$('#create_user_button').button('enable');
					
					$('#createUserMessage_spinner').hide();
				},
				error: function() {
					$('#createUserMessage_error_icon').show();
					$('#createUserMessage_messages_text').text('An error occurred while contacting the server. Please refresh the page and try again.').show();
				}
			});
		},
		
		editUser: function(id) {
			$('#echobase_user_dialog').dialog('open');
			
			var data = app.mobile.userActions[id];
			
			// fill in fields
			$('#user_echobase_user_id').val(data.user_id);
			$('#user_echobase_role').val(data.role);
			$('#user_echobase_doctor_id').val(data.doctor_id);
			$('#user_echobase_username').val(data.username);
			$('#user_echobase_lname').val(data.lname);
			$('#user_echobase_fname').val(data.fname);
			$('#user_echobase_email').val(data.email);
			$('#user_echobase_id').val(data.id);
			
			app.mobile.enableFields();
		},
		enableFields: function() {
			var id = $('#user_echobase_id').val();
			var email = $('#user_echobase_email').val();
			var fname = $('#user_echobase_fname').val();
			var lname = $('#user_echobase_lname').val();
			var username = $('#user_echobase_username').val();
			var doctorID = $('#user_echobase_doctor_id').val();
			var role = $('#user_echobase_role').val();
			var userID = $('#user_echobase_user_id').val();
			
			if (email && fname && lname && username && doctorID && role && userID) {
				$('#create_user_button').button('enable');
			}
			else {
				$('#create_user_button').button('disable');
			}
			
			if (userID && role && doctorID) {
				$('#user_echobase_email').removeAttr('disabled');
				$('#user_echobase_fname').removeAttr('disabled');
				$('#user_echobase_lname').removeAttr('disabled');
				$('#user_echobase_username').removeAttr('disabled');
			}
			else {
				$('#user_echobase_email').attr('disabled', true);
				$('#user_echobase_fname').attr('disabled', true);
				$('#user_echobase_lname').attr('disabled', true);
				$('#user_echobase_username').attr('disabled', true);
			}
			
			if (userID) {
				$('#user_echobase_doctor_id').removeAttr('disabled');
				$('#user_echobase_role').removeAttr('disabled');
			}
			else {
				$('#user_echobase_doctor_id').attr('disabled', true);
				$('#user_echobase_role').attr('disabled', true);
			}
		},
		resetCreateUserForm: function() {
			$('#create_user_form')[0].reset();
			$('#create_user_button').button('disable');
			
			$('#user_echobase_email').attr('disabled', true);
			$('#user_echobase_fname').attr('disabled', true);
			$('#user_echobase_lname').attr('disabled', true);
			$('#user_echobase_username').attr('disabled', true);
			
			$('#user_echobase_doctor_id').attr('disabled', true);
			$('#user_echobase_role').attr('disabled', true);
			
			$('#user_echobase_doctor_id option').removeAttr('disabled');
		},
		sendCommand: function(id, command) {
			if (quickClaim.ajaxLocked) {
				return;
			}
			
			var data = 'id=' + id + '&command=' + command;
			
			$.ajax({
				type: 'post',
				dataType: 'json',
				url: app.mobile.actions.sendCommand,
				data: data,
				success: function(rs) {
					if (rs.hasOwnProperty('errors')) {
						$('#error_icon').show();
						$('#messages_text').text(rs.errors).show();
					}
					else {
						for (var a in rs.data) {
							app.mobile.updateMobileUsersTable(rs.data[a]);
							app.mobile.userActions[rs.data[a].id] = rs.data[a];
						}
						$('#users_table').trigger('update');
					}
				},
				beforeSend: function() {
					quickClaim.ajaxLocked = true;
					$('#message .icons div').hide();
					$('#spinner').show();
					$('#messages_text').text('Sending Create User Command').show();
				},
				complete: function() {
					quickClaim.ajaxLocked = false;
					$('#spinner').hide();
				},
				error: function() {
					$('#error_icon').show();
					$('#messages_text').text('An error occurred while contacting the server. Please refresh the page and try again.').show();
				}
			});
		},
		updateDoctorDetails: function() {
			var id = $('#user_echobase_id').val();
			var doctorID = $('#user_echobase_doctor_id').val();
			var userID = $('#user_echobase_user_id').val();
			
			if (!id && app.mobile.doctorCodes.hasOwnProperty(doctorID) && app.mobile.users.hasOwnProperty(userID)) {
				var doctorCode = app.mobile.doctorCodes[doctorID];
				var username = app.mobile.users[userID].username;
				
				$('#user_echobase_username').val(username + '-' + doctorCode);
			}
			
			app.mobile.enableFields();
		},
		updateUserDetails: function() {
			var doctorID = $('#user_echobase_doctor_id').val();
			var userID = $('#user_echobase_user_id').val();
			var id = $('#user_echobase_id').val();
			
			if (app.mobile.users.hasOwnProperty(userID)) {
				if (!id) {
					$('#user_echobase_fname').val(app.mobile.users[userID].first);
					$('#user_echobase_lname').val(app.mobile.users[userID].last);
					$('#user_echobase_email').val(app.mobile.users[userID].email);

					if (doctorID && app.mobile.doctorCodes.hasOwnProperty(doctorID)) {
						var doctorCode = app.mobile.doctorCodes[doctorID];
						$('#user_echobase_username').val(app.mobile.users[userID].username + '-' + doctorCode);
					}
					else {
						$('#user_echobase_username').val(app.mobile.users[userID].username);
					}
				}
				app.mobile.disableUnselectableDoctors(app.mobile.users[userID].doctors);
			}
			
			app.mobile.enableFields();
		},
		updateMobileUsersTable: function(data) {
			var columns = ['username', 'hype_username', 'fname', 'lname', 'doctor_name', 'email', 'create_file_date', 'invite_file_date', 'status_string'];

			var tr = '<tr id="ue_' + data.id + '">';
			for (var a in columns) {
				tr += '<td class="' + columns[a] + '">' + data[columns[a]] + '</td>';
			}
			tr += '<td class="actions"></td>';
			tr += '</tr>';
			
			$('#ue_' + data.id).remove();
			$('#users_table tbody').append($(tr));
			app.mobile.updateMobileUsersTableActions(data.id);
		},
		updateMobileUsersTableActions: function(id, actions) {
			var rs = '<img src="' + quickClaim.contextMenuImage + '" class="actionsButton" />';
			$('#ue_' + id + ' td.actions').html(rs);
		},
		disableUnselectableDoctors: function(doctors) {
			$('#user_echobase_doctor_id option').removeAttr('disabled');
			$('#user_echobase_doctor_id option').each(function() {
				var v = parseInt($(this).val(), 10);
				if (v && jQuery.inArray(v, doctors) == -1) {
					$(this).attr('disabled', true);
				}
			});
		}
	},
	mobileContextMenu: {
		active: {
			edit: true,
			sendCreateUser: true,
			sendUserInvitation: true,
			sendDeactivateUser: true,
			sendDeleteUser: true
		},
		initialize: function() {
			var parameters = {
				selector: '.actionsButton',
				trigger: 'hover',
				className: 'echobaseUsersMenu',
				callback: function(key, options, event) {
					
				},
				items: {
					edit: {
						name: 'Edit User Details',
						callback: function(key, options, event) {
							app.mobile.editUser(app.mobileContextMenu.getId(options.$trigger));
						},
						disabled: function(key, options, event) {
							return !app.mobile.userActions[app.mobileContextMenu.getId(options.$trigger)]['actions']['edit'];
							
						}
					},
					sep1: '---',
					sendCreateUser: {
						name: 'Send Create User Command',
						callback: function(key, options, event) {
							app.mobile.sendCommand(app.mobileContextMenu.getId(options.$trigger), 'createUser');
						},
						disabled: function(key, options, event) {
							return !app.mobile.userActions[app.mobileContextMenu.getId(options.$trigger)]['actions']['sendCreateUser'];
						}
					},
					sendInvitation: {
						name: 'Send Invitation Email',
						callback: function(key, options, event) {
							app.mobile.sendCommand(app.mobileContextMenu.getId(options.$trigger), 'sendInvitation');
						},
						disabled: function(key, options, event) {
							return !app.mobile.userActions[app.mobileContextMenu.getId(options.$trigger)]['actions']['sendUserInvitation'];
						}
					},
					sep2: '---',
					sendDeactivate: {
						name: 'Send Deactivate Command',
						callback: function(key, options, event) {
							app.mobile.sendCommand(app.mobileContextMenu.getId(options.$trigger), 'sendDeactivate');
						},
						disabled: function(key, options, event) {
							return !app.mobile.userActions[app.mobileContextMenu.getId(options.$trigger)]['actions']['sendDeactivateUser'];
						} 
					},
					sendDelete: {
						name: 'Send Delete Command',
						callback: function(key, options, event) {
							app.mobile.sendCommand(app.mobileContextMenu.getId(options.$trigger), 'sendDelete');
						},
						disabled: function(key, options, event) {
							return !app.mobile.userActions[app.mobileContextMenu.getId(options.$trigger)]['actions']['sendDeleteUser'];
						}  
					}
				}
			};
			
			$.contextMenu(parameters);
			app.mobileContextMenu.setTitle('Placeholder Title');				
			
		},
		getId: function(trigger) {
			return $(trigger).closest('tr').prop('id').replace('ue_', '');
		},
		setTitle: function (title) {
			$('.echobaseUsersMenu').attr('data-menutitle', title);
		}
	}
};
