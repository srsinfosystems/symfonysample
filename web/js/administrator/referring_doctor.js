var ref_doc_index = {
	actions: {
		edit: null
	},

	handleRowClick: function(v) {
		id = v.replace('ref_doc_', '');
		window.location = this.actions.edit + '?id=' + id;
	},
	initialize: function() {
		this.setupTableRowClick();
		this.setupTablesort();
	},
	setupTableRowClick: function() {
		$('#tablesort tbody tr').click(function (e) {
			ref_doc_index.handleRowClick(this.id);
		});
	},
	setupTablesort: function() {
		$('#tablesort').tablesorter1({
			widgets: ['zebra', 'hover']
		});
	}
};

var ref_doc_edit = {
	address_clear_button: 'stage2_clear',
	address_row_prefix: 'rda_',
	address_form_prefix: 'referring_doctor_address_',
	address_fields: ['id', 'address', 'address2', 'city', 'province', 'postal_code', 'phone', 'fax', 'email', 'active'],

	disabled: [],
	selected: 1,
	form_id: 'stage2_address',

	initialize: function() {
		$("#tabs").tabs({
			disabled: this.disabled,
			active: this.selected,
			postShow: function() {
				quickClaim.focusTopField();
			}
		});
		$('#tabs').tabs('tabSwitching');
		$('#tabs').show();
		quickClaim.focusTopField();

		this.setupClickableRows();
		this.setupFormClears();

		$('#' + this.address_form_prefix + 'postal_code').blur(function () {
			quickClaim.formatPostal('#' + $(this).attr('id'));
		});
		$('#' + this.address_form_prefix + 'phone').blur(function () {
			quickClaim.formatPhone('#' + $(this).attr('id'));
		});
		$('#' + this.address_form_prefix + 'fax').blur(function () {
			quickClaim.formatPhone('#' + $(this).attr('id'));
		});

		if ($('input.hl7_partner_id').size()) {
			$('input.hl7_partner_id:last').closest('tr').after($('<tr class="small_row"><td colspan="2">&nbsp;</td></tr>'));
		}
	},
	handleAddressClear: function() {
		for (var a in this.address_fields) {
			switch (this.address_fields[a]) {
				case 'active':
					$('#' + this.address_form_prefix + this.address_fields[a]).prop('checked', false);
					break;
				default:
					$('#' + this.address_form_prefix + this.address_fields[a]).val('');
					break;
			};
		}
	},
	handleAddressRowClick: function(row) {
		id = $(row).attr('id');

		for (var a in this.address_fields) {
			switch (this.address_fields[a]) {
				case 'active':
					checked = $('.' + this.address_fields[a], $(row)).text() == 'Y';

					$('#' + this.address_form_prefix + this.address_fields[a]).prop('checked', checked);
					break;
				default:
					val = $('.' + this.address_fields[a], $(row)).text();
					$('#' + this.address_form_prefix + this.address_fields[a]).val(val);
					break;
			};
		}
		$('#' + this.address_form_prefix + 'id').val(id.replace(this.address_row_prefix, ''));

		$('#tablesorter1_address tr.current_edit').removeClass('current_edit');
		$(row).addClass('current_edit');
		quickClaim.focusTopField();
	},
	setupClickableRows: function() {
		if ($('#tablesorter1_address')) {
			$('#tablesorter1_address tr.clickable_row').each(function () {
				id = $(this).attr('id');

				$('tr#' + id).click(function() {
					ref_doc_edit.handleAddressRowClick($(this));
				});
			});
		}
	},
	setupFormClears: function() {
		$('#' + this.address_clear_button).click(function() {
			ref_doc_edit.handleAddressClear();
		});
	}
};