var workflow = {
    messageID: 'clientWorkflowMessages',

    initialize: function () {
        workflow.dashboard.initialize();
    },

    dashboard: {
        actions: {
            activeChange: null,
            save: null
        },
        process: [],
        steps: {},
        documentList: {},

        initialize: function () {

            $('#stepSelectionButtons').hide();
            workflow.dashboard.addStepSelectionButtons();

            $('#dashboardDefineWorkflowButton')
                .button()
                .click(function () {
                    workflow.dashboard.process = [];
                    workflow.dashboard.addStepBox('HCSwipe');
                    workflow.dashboard.addStepBox('PatientUpdate');

                    $('#dashboardDefineWorkflowButton').button('disable');
                    $('#dashboardClearWorkflowButton').button('enable');

                    $('#stepSelectionButtons').show();
                });

            $('#dashboardClearWorkflowButton')
                .button()
                .button('disable')
                .click(function () {
                    workflow.dashboard.process = [];

                    $('#dashboardWorkflow').find('div').remove();
                    $('#dashboardClearWorkflowButton').button('disable');
                    $('#dashboardSaveWorkflowButton').button('disable');
                    $('#dashboardDefineWorkflowButton').button('enable');

                    $('#stepSelectionButtons').hide();
                    $('.dashboardSelectionButtons').button('enable');
                    $('#dashboardWorkflowErrorsDiv').text('').hide();

                    $('#dashboardDeactivateWorkflowButton').hide();
                    $('#dashboardActivateWorkflowButton').hide();

                });

            $('#dashboardSaveWorkflowButton')
                .button()
                .button('disable')
                .click(function () {
                    workflow.dashboard.handleSaveRequest();
                });

            $('#dashboardDeactivateWorkflowButton')
                .button()
                .hide()
                .click(function () {
                    workflow.dashboard.handleActiveChangeRequest(false);
                });

            $('#dashboardActivateWorkflowButton')
                .button()
                .hide()
                .click(function () {
                    workflow.dashboard.handleActiveChangeRequest(true);
                });

        },

        addStepBox: function (key) {
            var stepNum = workflow.dashboard.process.length;
            workflow.dashboard.process[stepNum] = workflow.dashboard.steps[key];
            stepNum++;

            var step = workflow.dashboard.steps[key];

            var div = '<div id="dashboardStep' + stepNum + '" class="stepDiv alert alert-info btn-squared step' + key + '">'
                + '<span class="stepNumberSpan">Step ' + stepNum + '</span>'
                + '<br />' + step.description
                + '</div>';

            $('#dashboardWorkflow')
                .append($(div))
                .show();

            $('#selectButton_' + key).button('disable');

            if (key == 'GenerateDocs') {
                workflow.dashboard.addGenerateDocumentsParameters('dashboardStep' + stepNum)
            }

            if (workflow.dashboard.canBeSaved()) {
                $('#dashboardSaveWorkflowButton').button('enable');
            }
            else {
                $('#dashboardSaveWorkflowButton').button('disable');
            }
            if (step.canContinue == false) {
                $('.dashboardSelectionButtons').button('disable');
            }
        },

        addStepSelectionButtons: function () {
            var step = null;
            var icons = ['clip-health', 'clip-file-plus','clip-checkbox','clip-file-word','clip-book','fa fa-briefcase','clip-users-3'];
            var colors = ['btn-green', 'btn-success','btn-teal','btn-blue','btn-info','btn-warning','btn-red'];
            var c = 0;
            for (var a in workflow.dashboard.steps) {
                step = workflow.dashboard.steps[a];

                var button = '<button type="button" class="dashboardSelectionButtons btn btn-squared '+colors[c]+' sk5mar" id="selectButton_' + step.name + '"><i class="'+icons[c]+'"></i> ' + step.description + '</button>';

                $('#stepSelectionButtons').append($(button));

                $('#selectButton_' + step.name)
                    .button()
                    .click(function () {
                        var id = $(this).prop('id');
                        id = id.replace('selectButton_', '');
                        workflow.dashboard.addStepBox(id);
                    });
                    c++;
            }
        },

        addGenerateDocumentsParameters: function (stepDivID) {
            var rs = '<br />&nbsp;<br /><b>Select at least one of the following documents</b>'
                + '<ul class="workflowUL row" style="padding-left:40px;">';

            for (var a in workflow.dashboard.documentList) {
                var name = a;
                rs += '<li class="col-sm-4">'
                + '<input type="checkbox" name="dashboardDocuments[]" id="dashboardDocuments_' + name + '" value="' + name + '" />'
                + ' <label for="dashboardDocuments_' + name + '">' + workflow.dashboard.documentList[a] + '</label>'
                + '</li>'
            }
            rs += '</ul>';
            
            $('#' + stepDivID).append($(rs));

            $('#' + stepDivID).find('input[type=checkbox]').click(function () {
                if (workflow.dashboard.canBeSaved()) {
                    $('#dashboardSaveWorkflowButton').button('enable');
                }
                else {
                    $('#dashboardSaveWorkflowButton').button('disable');
                }
            });
        },

        canBeSaved: function () {
            if ($('#dashboardWorkflow').find('.stepGenerateDocs').size()) {
                if ($('input[name="dashboardDocuments[]"]:checked').size() == 0) {
                    return false;
                }
            }

            for (var a in workflow.dashboard.process) {
                if (workflow.dashboard.process[a].endpoint) {
                    return true;
                }
            }
        },

        handleSaveRequest: function () {
            if (!workflow.dashboard.canBeSaved()) {
                return;
            }
            if (quickClaim.ajaxLocked) {
                return;
            }

            $('#dashboardWorkflowErrorsDiv').text('').hide();

            var data = {
                steps: []
            };
            for (var a in workflow.dashboard.process) {
                var step = workflow.dashboard.process[a];

                data['steps'][data['steps'].length] = step.name;

                if (step.name == 'GenerateDocs') {
                    data['documentList'] = $('input[name="dashboardDocuments[]"]:checked').map(function () {
                        return $(this).val();
                    }).get();
                }
            }

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: workflow.dashboard.actions.save,
                data: data,

                success: function (rs) {
                    if (rs.hasOwnProperty('success')) {
                        quickClaim.showAjaxMessage(workflow.messageID, 'success', 'Saved Dashboard Workflow');

                        if (rs.active) {
                            $('#dashboardDeactivateWorkflowButton').button('enable').show();
                            $('#dashboardActivateWorkflowButton').button('disable').show();
                        }
                        else {
                            $('#dashboardDeactivateWorkflowButton').button('disable').show();
                            $('#dashboardActivateWorkflowButton').button('enable').show();
                        }
                    }
                    else if (rs.hasOwnProperty('errors')) {
                        $('#dashboardWorkflowErrorsDiv')
                            .html(rs.errors.join('<br />'))
                            .show();

                        var errorMessage = 'Invalid parameters supplied to Dashboard Workflow';
                        quickClaim.showAjaxMessage(workflow.messageID, 'error', errorMessage);
                    }
                },
                beforeSend: function () {
                    quickClaim.ajaxLocked = true;
                    var message = 'Saving Dashboard Workflow';

                    quickClaim.showAjaxMessage(workflow.messageID, 'spin', message);
                    quickClaim.showOverlayMessage(message);
                },
                complete: function () {
                    quickClaim.ajaxLocked = false;
                    quickClaim.hideOverlayMessage();
                },
                error: function () {
                    quickClaim.ajaxLocked = false;
                    var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                    quickClaim.hideOverlayMessage();
                    quickClaim.showAjaxMessage(workflow.messageID, 'error', message);
                }
            });
        },

        handleActiveChangeRequest: function (value) {
            if (!workflow.dashboard.canBeSaved()) {
                return;
            }
            if (quickClaim.ajaxLocked) {
                return;
            }

            $('#dashboardWorkflowErrorsDiv').text('').hide();

            var data = 'moduleName=DASHBOARD&active=' + value;

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: workflow.dashboard.actions.activeChange,
                data: data,

                success: function (rs) {
                    if (rs.hasOwnProperty('success')) {
                        quickClaim.showAjaxMessage(workflow.messageID, 'success', 'Saved Dashboard Workflow');

                        if (rs.active) {
                            $('#dashboardDeactivateWorkflowButton').button('enable');
                            $('#dashboardActivateWorkflowButton').button('disable');
                        }
                        else {
                            $('#dashboardDeactivateWorkflowButton').button('disable');
                            $('#dashboardActivateWorkflowButton').button('enable');
                        }
                        $('#dashboardDeactivateWorkflowButton').show();
                        $('#dashboardActivateWorkflowButton').show();
                    }
                    else if (rs.hasOwnProperty('errors')) {
                        $('#dashboardWorkflowErrorsDiv')
                            .html(rs.errors.join('<br />'))
                            .show();

                        var errorMessage = 'Invalid parameters supplied to Dashboard Workflow';
                        quickClaim.showAjaxMessage(workflow.messageID, 'error', errorMessage);
                    }
                },
                beforeSend: function () {
                    quickClaim.ajaxLocked = true;
                    var message = 'Updating Dashboard Workflow';

                    quickClaim.showAjaxMessage(workflow.messageID, 'spin', message);
                    quickClaim.showOverlayMessage(message);
                },
                complete: function () {
                    quickClaim.ajaxLocked = false;
                    quickClaim.hideOverlayMessage();
                },
                error: function () {
                    quickClaim.ajaxLocked = false;
                    var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                    quickClaim.hideOverlayMessage();
                    quickClaim.showAjaxMessage(workflow.messageID, 'error', message);
                }
            });
        },

        loadSteps: function (params, active) {
            workflow.dashboard.process = [];

            if (params.hasOwnProperty('steps')) {
                for (var a in params.steps) {
                    workflow.dashboard.addStepBox(params.steps[a]);
                }
            }

            if (params.hasOwnProperty('documents')) {
                for (var a in params['documents']) {
                    $('#dashboardDocuments_' + params['documents'][a]).prop('checked', true);
                }
            }

            $('#dashboardDefineWorkflowButton').button('disable');
            $('#dashboardClearWorkflowButton').button('enable');
            if (workflow.dashboard.canBeSaved()) {
                $('#dashboardSaveWorkflowButton').button('enable');
            }
            else {
                $('#dashboardSaveWorkflowButton').button('disable');
            }

            $('#stepSelectionButtons').show();

            if (active) {
                $('#dashboardDeactivateWorkflowButton').button('enable');
                $('#dashboardActivateWorkflowButton').button('disable');
            }
            else {
                $('#dashboardDeactivateWorkflowButton').button('disable');
                $('#dashboardActivateWorkflowButton').button('enable');
            }
            $('#dashboardDeactivateWorkflowButton').show();
            $('#dashboardActivateWorkflowButton').show();

        }
    }

};