var VPLDetails = {
    // TODO: set up actions to acknowledge all messages, all successes,

    actions: {
        claimEdit: null,
        claimLoop: null,
        getData: null,
        temporaryEdit: null
    },
    messageID: 'VPLDetailsMessage',
    parent: 'import',
    tableSorterInitialized: false,
    fileData: {},
    tableData: {},

    pagerColumns: [],
    paginationSort: [[0, 0]],
    paginationSize: 50,

    initialize: function () {
        VPLDetails.contextMenu.initialize();

        $('#VPLDetailsAcknowledgeButton')
            .button()
            .click(function () {
                VPLDetails.acknowledgeFile();

            });

        $('#VPLDaySheetDetailsDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            minWidth:800,
            minHeight:250,
            width: $(window).width() - 100,
            height: $(window).height() - 100,
            modal: true,
            open: function () {
                if (!VPLDetails.tableSorterInitialized) {
                    VPLDetails.initializeTableSorter();
                }
                else {
                    $('#VPLImportDetailsTable').trigger('pageSet', 1);
                }

                $('#VPLDaySheetDetailsDialog').find('.fileName').text(VPLDetails.fileData['document_name']);
            },
            close: function () {
                $('#VPLDaySheetDetailsDialog').find('.fileName').text('');
                VPLDetails.fileData = {};
                VPLDetails.tableData = {};

                $('#VPLImportDetailsTable').find('tbody tr').remove();
                $('#VPLDetailsEditAllClaimsButton').button('disable');
                $('#VPLDetailsEditAllTemporaryButton').button('disable');
                $('#VPLImportDetailsSelectAll').prop('checked', false);
            }
        });

        $('#VPLDetailsEditAllClaimsButton')
            .button()
            .button('disable')
            .click(function (event) {
                VPLDetails.goToEditSelectedClaims(quickClaim.isRightClick(event));
            })
            .contextmenu(function (event) {
                VPLDetails.goToEditSelectedClaims(quickClaim.isRightClick(event));
                event.preventDefault();
                event.stopPropagation();
                return false;
            });

        $('#VPLDetailsEditAllTemporaryButton')
            .button()
            .button('disable')
            .click(function (event) {
                VPLDetails.goToEditTemporaryClaims(quickClaim.isRightClick(event));
            })
            .contextmenu(function (event) {
                VPLDetails.goToEditTemporaryClaims(quickClaim.isRightClick(event));
                event.preventDefault();
                event.stopPropagation();
                return false;
            });
    },

    showDialog: function () {
        $('#VPLDaySheetDetailsDialog').dialog('open');
    },

    initializeTableSorter: function () {
        var $table = $('#VPLImportDetailsTable');

        $table.find('thead th').each(function () {
            VPLDetails.pagerColumns[VPLDetails.pagerColumns.length] = $(this).prop('class');
        });

        $table.find('thead th.select').addClass('sorter-false');
        $table.find('thead th.actions').addClass('sorter-false');
        $table.find('thead th.claim_id').addClass('sorter-false');
        $table.find('thead th.temporary_form_id').addClass('sorter-false');

        $table.find('thead th:last').append(quickClaim.tablesorterTooltip);

        $table.tablesorter({
            theme: 'blue',
            headerTemplate: '{content} {icon}',
            widgets: ['selectRow', 'zebra', 'pager'],
            sortList: VPLDetails.paginationSort
        });

        $table.find('.sortTip').tooltip();

        $('#VPLImportDetailsSelectAll').click(function () {
            var checked = $(this).prop('checked');
            $('#VPLImportDetailsTable').find('tbody tr td input[type=checkbox]').prop('checked', checked);
        });

        $table.tablesorterPager({
            container: $('.vplDetailsPager'),
            size: VPLDetails.paginationSize,
            cssPageSize: '.pagesize',
            ajaxUrl: VPLDetails.actions.getData + '?{sortList:sort}&pageNumber={page}&size={size}',
            customAjaxUrl: function (table, url) {
                return url + '&data_importer_file_id=' + VPLDetails.fileData['id'];
                //+ '&' ... TODO: form.serialize() ???
            },
            ajaxObject: {
                dataType: 'json',
                complete: function () {
                    quickClaim.ajaxLocked = false;
                    quickClaim.hideOverlayMessage();

                    $('.actionsButton').each(function () {
                        VPLDetails.contextMenu.setup($(this).prop('id'));
                    });
                },
                beforeSend: function () {
                    var message = 'Retrieving VPL Import Details';
                    quickClaim.ajaxLocked = true;

                    quickClaim.showAjaxMessage(VPLDetails.messageID, 'spin', message);
                    if (VPLDetails.parent == 'import') {
                        quickClaim.showAjaxMessage(dataImport.messageID, 'spin', message);
                    }
                    quickClaim.showOverlayMessage(message);
                },
                error: function () {
                    var message = 'An error occurred while contacting the server. Please refresh the page and try again.';
                    quickClaim.ajaxLocked = false;

                    quickClaim.hideOverlayMessage();
                    quickClaim.showAjaxMessage(VPLDetails.messageID, 'error', message);
                    if (VPLDetails.parent == 'import') {
                        quickClaim.showAjaxMessage(dataImport.messageID, 'error', message);
                    }

                }
            },
            ajaxProcessing: function (rs) {
                var message = 'Received VPL Import Details';
                quickClaim.hideOverlayMessage();

                quickClaim.showAjaxMessage(VPLDetails.messageID, 'success', message);
                if (VPLDetails.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'success', message);
                }

                if (rs.hasOwnProperty('data')) {
                    VPLDetails.tableData = {};

                    var data = rs['data'];
                    var rows = '';
                    var canEdit = false;
                    var hasTemporary = false;

                    for (var a in data) {
                        VPLDetails.tableData[data[a]['id']] = data[a];
                        rows += VPLDetails.buildTableRow(data[a]);
                        if (data[a]['actions'].hasOwnProperty('claimEdit')) {
                            canEdit = true;
                        }
                        if (data[a]['actions'].hasOwnProperty('temporaryEdit')) {
                            hasTemporary = true;
                        }
                    }

                    if (canEdit) {
                        $('#VPLDetailsEditAllClaimsButton').button('enable');
                    }
                    if (hasTemporary) {
                        $('#VPLDetailsEditAllTemporaryButton').button('enable');
                    }

                    return [rs['count'], $(rows)];
                }
            }
        });

        VPLDetails.tableSorterInitialized = true;
    },

    acknowledgeFile: function () {
        // TODO: acknowledge file
        console.log('acknowledge File');
    },

    buildTableRow: function (data) {
        var row = '<tr id="VPLDataRow_' + data['id'] + '">';

        for (var a in VPLDetails.pagerColumns) {
            var b = VPLDetails.pagerColumns[a];

            row += '<td class="' + b + '">';
            if (b == 'actions') {
                row += '<img class="actionsButton" id="vplDataRowActions_' + data['id'] + '" src="' + VPLDetails.contextMenu.menuHTML + '" />';
            }
            else if (b == 'select') {
                row += '<input type="checkbox" id="selectVPL_' + data['id'] + '" class="VPLSelect" value="' + data['id'] + '" />';
            }
            else if (b == 'log_text') {
                row += '<div><pre>' + data[b] + '</pre></div>';
            }
            else {
                row += data[b];
            }
            row += '</td>';
        }

        row += '</tr>';

        return row;
    },
    goToEditClaim: function (rowID, rightClick) {
        var claimID = VPLDetails.tableData[rowID]['claim_id'];
        var url = VPLDetails.actions.claimEdit + '?id=' + claimID;

        if (rightClick) {
            window.open(url, 'claims');
        }
        else {
            window.location = url;
        }
    },
    goToEditSelectedClaims: function (rightClick) {
        var claimIDs = [];

        $('#VPLImportDetailsTable').find('tbody tr td input[type=checkbox]:checked').each(function () {
            var rowID = $(this).prop('id').replace('selectVPL_', '');

            if (VPLDetails.tableData[rowID]['actions'].hasOwnProperty('claimEdit')) {
                claimIDs[claimIDs.length] = VPLDetails.tableData[rowID]['claim_id'];
            }
        });

        claimIDs = claimIDs.join(',');
        var url = VPLDetails.actions.claimLoop + '?claimIDs=' + claimIDs;

        if (rightClick) {
            window.open(url, 'claims');
        }
        else {
            window.location = url;
        }
    },
    goToEditTemporary: function (rowID, rightClick) {
        var temporaryID = VPLDetails.tableData[rowID]['temporary_form_id'];
        var url = VPLDetails.actions.temporaryEdit + '?id=' + temporaryID;

        if (rightClick) {
            window.open(url, 'claims');
        }
        else {
            window.location = url;
        }
    },
    goToEditTemporaryClaims: function (rightClick) {
        var temporaryIDs = [];

        $('#VPLImportDetailsTable').find('tbody tr td input[type=checkbox]:checked').each(function () {
            var rowID = $(this).prop('id').replace('selectVPL_', '');

            if (VPLDetails.tableData[rowID]['actions'].hasOwnProperty('temporaryEdit')) {
                temporaryIDs[temporaryIDs.length] = VPLDetails.tableData[rowID]['temporary_form_id'];
            }
        });

        temporaryIDs = temporaryIDs.join(',');
        var url = VPLDetails.actions.claimLoop + '?temporaryIDs=' + temporaryIDs;

        if (rightClick) {
            window.open(url, 'claims');
        }
        else {
            window.location = url;
        }
    },
    archiveRecord: function (rowID) {
        // TODO: archive one record
        console.log('archiveRecord');
    },

    contextMenu: {
        active: {
            claimEdit: false,
            temporaryEdit: false,
            acknowledge: false
        },
        menuHTML: null,

        initialize: function () {
            var parameters = {
                selector: '#VPLDaySheetDetailsDialog .actionsButton',
                trigger: 'hover',
                autoHide: true,
                className: 'VPLDataDetailsMenu',
                items: {
                    claimEdit: {
                        name: 'Edit Claim',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('vplDataRowActions_', '');
                            VPLDetails.goToEditClaim(id, quickClaim.isRightClick(event));
                        },
                        disabled: function () {
                            return !VPLDetails.contextMenu.active['claimEdit'];
                        }
                    },
                    temporaryEdit: {
                        name: 'Edit Temporary Claim',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('vplDataRowActions_', '');
                            VPLDetails.goToEditTemporary(id, quickClaim.isRightClick(event));
                        },
                        disabled: function () {
                            return !VPLDetails.contextMenu.active['temporaryEdit'];
                        }

                    },
                    acknowledge: {
                        name: 'Archive Record',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('vplDataRowActions_', '');
                            VPLDetails.archiveRecord(id);
                        },
                        disabled: function () {
                            return !VPLDetails.contextMenu.active['acknowledge'];
                        }

                    }
                }
            };

            $.contextMenu(parameters);
        },
        setup: function (id) {
            $('#' + id).mouseover(function () {
                VPLDetails.contextMenu.setActiveItems(id);
            });
        },
        setActiveItems: function (id) {
            id = id.replace('vplDataRowActions_', '');
            var actions = {};

            if (VPLDetails.tableData.hasOwnProperty(id)) {
                actions = VPLDetails.tableData[id]['actions'];
            }

            if (actions.hasOwnProperty('claimEdit')) {
                VPLDetails.contextMenu.enable('claimEdit');
            }
            else {
                VPLDetails.contextMenu.disable('claimEdit');
            }

            if (actions.hasOwnProperty('temporaryEdit')) {
                VPLDetails.contextMenu.enable('temporaryEdit');
            }
            else {
                VPLDetails.contextMenu.disable('temporaryEdit');
            }

            if (actions.hasOwnProperty('acknowledge')) {
                VPLDetails.contextMenu.enable('acknowledge');
            }
            else {
                VPLDetails.contextMenu.disable('acknowledge');
            }
        },
        disable: function (item) {
            VPLDetails.contextMenu.active[item] = false;
        },
        enable: function (item) {
            VPLDetails.contextMenu.active[item] = true;
        }
    }
};