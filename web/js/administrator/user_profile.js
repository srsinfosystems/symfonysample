var user_profile = {
	actions: {
		edit: null
	},

	handleRowClick: function(v) {
		id = v.replace('user_', '');

		window.location = user_profile.actions.edit + '?id=' + id;
	},
	initialize: function() {
		this.setupTableRowClick();
                this.setupDatatable();
	},
	setupTableRowClick: function() {
		$('#users_datatable tbody tr').click(function (e) {
			user_profile.handleRowClick(this.id);
		});
	},
	setupDatatable: function() {
		var tableId = 'users_datatable';
                
            var table = $("#" + tableId).DataTable({
            "paging": true,
            "lengthMenu": [10, 25, 50, 100, 200, 250, 500],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "order": [[0, "asc"]],
            "info": true,
            "autoWidth": false,
            "responsive": false,
            "keys": true,
            "dom": 'Bfrtip',
            "oLanguage": {
                "sSearch": "",
                "sLengthMenu": "Per page _MENU_",
                "oPaginate": {
                    "sNext": "<i class='clip-chevron-right'></i>",
                    "sPrevious": "<i class='clip-chevron-left'></i>"
                },
                "sInfo": "_START_ to _END_ rows out of _TOTAL_,",
            },
            "sDom": '<"wrapper"<"row"<"col-sm-12" <"col-sm-8 actionButtonsDiv' + tableId + '"> <"col-sm-4 printBtnDiv" Bl > <"col-sm-12" <"row well customWell" <"col-sm-4 customSearchInput' + tableId + '" f> <"col-sm-1 customSearchField' + tableId + '">  <"col-sm-7 CustomPagination' + tableId + '"ip> > > >  >rt<"clear">>',
//            "sDom": '<"wrapper"<"row"<"col-sm-12" <"col-sm-8 actionButtonsDiv"> <"col-sm-4 printBtnDiv" l > <"col-sm-12" <"row well customWell" <"col-sm-4 customSearchInput" f> <"col-sm-8 CustomPagination"ip> > > >  >rt<"clear">>',
            initComplete: function () {

                //$("#patient_service_history_results_table_length").append('<button type="button" id="printVacationTable1"  class="btn btn-danger btn-squared"><i class="fa fa-print"></i> Print</button>');
                $(".CustomPagination" + tableId).prepend('<div style="float:left"><button data-toggle="tooltip" title="Filters" type="button" class="btn btn-primary filterToggle' + tableId + '"><i class="fa fa-filter"></i></button></div>');

                $("#" + tableId).find(".entireClickable").click(function () {
                    if ($(this).find('input').prop('checked')) {
                        $(this).find('input').prop('checked', false);
                        $(this).removeClass('highlight');
                    } else {
                        $(this).find('input').prop('checked', true);
                        $(this).addClass('highlight');
                    }
                })

                var inputHtml = '<div class="input-group">' +
                        '<input type="text" placeholder="Contains..." data-focus="true" class="form-control DatatableAllRounderSearch' + tableId + '" />' +
                        '<span class="input-group-addon cursorPointer btnClearSearchTxt' + tableId + '"> ' +
                        '<i class="clip-cancel-circle-2 "></i>' +
                        '</span>' +
                        '<span class="input-group-addon cursorPointer"> ' +
                        '<i class="clip-search-2"></i>' +
                        '</span>' +
                        '</div>';

                $(".customSearchInput" + tableId).html(inputHtml);

                var searchoptions = $("#" + tableId + " thead tr:eq(0) th");
                var customfilterinputs = '<tr>';
                for (var j = 0; j < searchoptions.length; j++) {
                    customfilterinputs += '<th></th>';
                }
                customfilterinputs += '</tr>';
                $("#" + tableId + " thead").append(customfilterinputs);
//                $("#" + tableId + " tfoot").remove();

                var aa = 0;
                var filterFirst = true;
                this.api().columns().every(function () {
                    var column = this;
                    var columnText = $.trim($(column.header())[0].innerText);
                    if ($(column.header())[0].cellIndex != 0 || filterFirst == true) {
                        if(columnText != 'Actions'){
                        if (($.trim($(column.header())[0].innerText) == 'Description' && tableId == 'explCodeTable')) {
                            $('<input type="text" placeholder="Search" class="form-control btn-squared" />')
                                    .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                    .on('keyup change', function () {
                                        if (column.search() !== this.value) {
                                            column
                                                    .search(this.value)
                                                    .draw();
                                        }
                                    });
                        } else {
                            var select = $('<select class="selectRsltTbl' + tableId + '"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                    .appendTo($("#" + tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                    .on('change', function () {
                                        var val = //$.fn.dataTable.util.escapeRegex(
                                                $(this).val().trim();
                                        //);
                                        column.search(val, true, false).draw();
                                    });

                            column.data().unique().sort().each(function (d, j) {
                                if (columnText == 'Permissions' && tableId == 'user_type_table') {
                                    var arr = d.trim();
                                    arr = arr.split(',');
                                    for (var i in arr) {
                                        select.append('<option value="' + arr[i] + '">' + arr[i] + '</option>');
                                    }
                                } else {
                                    if( columnText == 'Explanation code' && tableId == 'errorReportDetailsTable'){
                                        var dd = $(d);
                                        dd.find('span').remove();
                                        d = dd.text();
                                    }
                                    if(d != '') {
                                     select.append('<option value="' + d + '">' + d + '</option>');
                                     }
                                }

                            });
                        }

                    }
                }
                    aa++;
                });


                $(".filterToggle" + tableId).click(function () {
                    $("#" + tableId + " thead tr:eq(1)").toggle();
                });
                //$(".filterToggle" + tableId).trigger("click");
                $(".selectRsltTbl" + tableId).select2({
                    placeholder: "Search",
                    allowClear: true,
                    dropdownAutoWidth: true,
//                    maximumInputLength: 20,
                    width: '98%'
                });
                $(".custActionBtns" + tableId).detach().appendTo(".actionButtonsDiv" + tableId);
                $(".actionButtonsDiv" + tableId).attr('style', 'padding-left:0px');
//                $(".actionButtonsDiv" + tableId).parent('div').css('padding-left','0px');
                //
                global_js.triggerFocusField();
            }
        });

        $(".DatatableAllRounderSearch" + tableId).keyup(function () {
            table.search($(this).val(), true).draw();
        });

        $(".btnClearSearchTxt" + tableId).click(function () {
            $(".DatatableAllRounderSearch" + tableId).val('');
            if(tableId == 'facilitiesTable'){
                $("#facilitiesResetButton").trigger('click');
            }
            table.search('', true).draw();
        });
        setTimeout(function () {
            $(".dataTables_length select").select2('destroy');
            $("#" + tableId).find('.select2-arrow').hide();
            $("#" + tableId + " thead tr:eq(1)").toggle();
        }, 200);
	}
};


var user_profile_edit = {
    actions: {
        edit: null
    },
	initialize: function() {
		// on enter keypress in password form, submit using st4
		$('#profile_confirmation_password_1').keypress(function (e) {
			if (e.which == 13) {
				$('#st4').click();
			}
		});

		$('#profile_confirmation_password_2').keypress(function (e) {
			if (e.which == 13) {
				$('#st4').click();
			}
		});

		$('#stage_2_locations input[type=checkbox]').click(function () {
			user_profile_edit.toggleAll($(this).val(), 'stage_2_locations');
		});
		$('#stage_6_doctors input[type=checkbox]').click(function () {
			user_profile_edit.toggleAll($(this).val(), 'stage_6_doctors');
		});
	},
	locationsSetup: function(regions, location_regions) {
		var reg_count = 0;
		var loc_count = 0;

		for (var a in regions) { reg_count++; }
		for (var a in loc_count) { loc_count++; }

		if (loc_count < 10 || reg_count < 3) {
			return;
		}

		$('#stage_2_locations table').width('100%');

		var ul = '<tr><td>&nbsp;</td><td>';
		var c = 0;
		for (var r in regions) {
			if (c == 0) {
				ul += '<div class="region_div_parent" style="width:100%">';
			}
			ul += '<div class="region_div">' + regions[r] + '<ul id="region_' + r + '" class="checkbox_list"></ul></div>';
			c++;
			if (c == 4) {
				ul += '</div><br clear="all" />';
				c = 0;
			}

		}

		ul += '</ul></td></tr>';
		$('#st2').parent('td').parent('tr').before($(ul));

		for (var l_id in location_regions) {
			var region_id = location_regions[l_id];
			var checked = $('#profile_locations_locations_' + l_id).prop('checked');
			var li = $('#profile_locations_locations_' + l_id).parent('li').html();

			$('#region_' + region_id).append($('<li>' + li + '</li>'));
			$('#profile_locations_locations_' + l_id).prop('checked', checked);
			$('#profile_locations_locations_' + l_id).parent('li').remove();
		}
	},
    permissionsSetup: function(ps) {

		var save_button_row = $('#st3').closest('div[class="form-group"]');
                var html = '';
		for (var cat_name in ps) {
			html += '<div class="form-group"><label class="col-sm-2 control-label" for="form-field-1"><b>' + cat_name + '</b></label><div class="col-sm-10"><ul class="checkbox_list customFormat" style="padding-bottom:10px;">';
			for (var idx in ps[cat_name]) {
				var constant = ps[cat_name][idx];
				if ($('#profile_permissions_permissions_' + constant).size()) {
html += '<li>' + $('#profile_permissions_permissions_' + constant).parent('li').html() + '</li>';
					
                    // $('#profile_permissions_permissions_' + constant).parent('li').parent('ul').parent('td').parent('tr').hide();
					// $('#profile_permissions_permissions_' + constant).parent('li').remove();
				}
			}
			html += '</ul></div></div>';
			 
		}
                save_button_row.before(html);
                $('.main-conatinaer ul').remove();
                $('.main-conatinaer label').remove();
                $(".customFormat li label").each(function (e) {
                    var label = $(this).html();
                    var lblarr = label.split('-');
                    var newlabel = '<b>' + lblarr[0] + '</b>' + '<span style="color:#858585"> - ' + lblarr[1] + '</span>';
                    $(this).html(newlabel);
                    //console.log(newlabel)
                })
	},
	toggleAll: function(value, form_id) {
		var count = $('#' + form_id + ' input:checked').size();

		if (value == 'all' && $('#' + form_id + ' input[value=all]').prop('checked')) {
			$('#' + form_id + ' input:checked').each(function () {
				if ($(this).val() != 'all') {
					$(this).prop('checked', false);   
                    $('#'+form_id+' select').multiselect('deselect', $(this).val());
                    //$('#profile_locations_locations').multiselect('refresh'); 
				}
			});

		}

		else if (count > 1) {
			$('#' + form_id + ' input[value=all]').prop('checked', false);
            $('#'+form_id+' select').multiselect('deselect', 'all');
		}

	},
	lockForms: function() {
		$('#stage_1_details input[type=text]').attr('disabled', true).addClass('readonly');
		$('#stage_1_details select').attr('disabled', true).addClass('readonly');
		$('#stage_1_details input[type=submit]').remove();

		$('#stage_2_locations input[type=checkbox]').attr('disabled', true).addClass('readonly');
		$('#stage_2_locations input[type=submit]').remove();

		$('#stage_3_permissions input[type=checkbox]').attr('disabled', true).addClass('readonly');
		$('#stage_3_permissions input[type=submit]').remove();

		$('#stage_4_password input[type=password]').attr('disabled', true).addClass('readonly');
		$('#stage_4_password input[type=submit]').remove();
	},
    RestictionLocationUpdate: function(){
        $formData = $('#locationRestictionForm').serialize();
        alert($('#accessible-ip-0').val());
        console.log($formData);
        $.ajax({
                url: user_profile_edit.actions.edit,
                type: 'POST',
                data: $formData,
                success: function (result) {
                    console.log(result);
                }
            });
    },
};