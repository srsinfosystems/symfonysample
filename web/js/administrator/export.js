var dataExport = {
    actions: {
        cancelExportRequest: null,
        createExportRequest: null,
        downloadExportRequest: null
    },
    messageID: null,
    tableHeaderColumns: [],
    dataExports: {},
    cancelRequestID: null,

    initialize: function () {
        if (dataExport.tableHeaderColumns.length == 0) {
            var headers = [];

            $('#dataExportTable').find('thead tr th').each(function () {
                headers[headers.length] = $(this).prop('class');
            });

            dataExport.tableHeaderColumns = headers;
        }

        $("#tabs") 
                .tabs({})
                .show();

        $("#backupSubmit").on('click', function () {
        //$('#backupSubmit').button().click(function () {
                    dataExport.handleBackupRequest();
        });

        $('#data_export_request_filename').blur(function () {
            dataExport.checkFilename();
        });

        // $('#dataExportTable').tablesorter({
        //     theme: 'blue',
        //     headerTemplate: '{content} {icon}',
        //     sortList: [[0, 0]],
        //     widgets: ['selectRow', 'zebra']
        // });

        $('#cancelDataExportRequestDialog').dialog({
            resizable: true,
            modal: true,
            minWidth:800,
            minHeight:250,
            width: 800,
            autoOpen: false,
            buttons: {
                'Yes': function () {
                    dataExport.handleCancelRequest();
                },
                'No': function () {
                    $(this).dialog('close');
                    return false;
                }
            }
        });
    },

    clearErrors: function () {
        $('#exportRequestErrorsDiv')
                .text('')
                .removeClass('ui-state-error')
                .removeClass('ui-state-highlight');

        $('#requestForm').find('label.error').remove();
        $('#requestForm').find('.error').removeClass('error');
    },

    handleBackupRequest: function () {
        if (quickClaim.ajaxLocked) {
            return;
        }
        dataExport.clearErrors();
        var data = $('#requestForm').serialize();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: dataExport.actions.createExportRequest,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#exportRequestErrorsDiv', '#data_export_request', false);
                    quickClaim.showAjaxMessage(dataExport.messageID, 'error', 'The form contains invalid data');
                } else {
                    $('#dataExportTable').dataTable().fnClearTable();
                    for (var a in rs.data) {
                        if (rs.data.hasOwnProperty(a)) {

                                var oTable = $('#dataExportTable').dataTable();
                                var l = dataExport.tableHeaderColumns.length;
                                 var r = rs.data[a];
                                var tr = '<tr id="dataTableRow_' + r['id'] + '">';
                                for (var a = 0; a < l; a++)
                                {
                                    var text = '<td class="' + dataExport.tableHeaderColumns[a] + '">';

                                    if (dataExport.tableHeaderColumns[a] != 'actions') {
                                        text += r[dataExport.tableHeaderColumns[a]];
                                    } else {
                                        text += '<img class="actionsButton" id="dataTableActions_' + r['id'] + '" src="' + dataExport.contextMenu.menuHTML + '" />';
                                    }

                                    text += '</td>';
                                    tr += text;
                                }
                                tr += '</td>';
                                oTable.fnAddData($(tr));
                                $('#dataExportTable').trigger('update');
                                $('#dataExportTable').find('.actionsButton').each(function () {
                                    dataExport.contextMenu.setup($(this).prop('id'));
                                });
                                dataExport.dataExports[r['id']] = r;
                                dataExport.contextMenu.setActiveItems('dataTableActions_' + r['id']);
                        }
                    }

                    quickClaim.showAjaxMessage(dataExport.messageID, 'success', null);


                    if (rs.hasOwnProperty('newID')) {
                        $('#tabs').tabs('option', 'active', 0);
                    }
                }
            },
            beforeSend: function () {
                $(".exportlistDataTable").dataTable().fnDestroy();
                quickClaim.ajaxLocked = true;
                quickClaim.showOverlayMessage('Saving Backup Request');
                quickClaim.showAjaxMessage(dataExport.messageID, 'spin', 'Saving Backup Request');
            },
            complete: function () {
                var oTable = $('#dataExportTable').dataTable();
                oTable.fnDraw();
                //oTable.fnDraw(false);
                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
                $(".exportlistDataTable").DataTable({
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true,
                    "responsive": true,
                    "oLanguage": {
                        "sSearch": "",
                        "oPaginate": {
                            "sNext": "<i class='clip-chevron-right'></i>",
                            "sPrevious": "<i class='clip-chevron-left'></i>"
                        }

                    },
                    "columnDefs": [
//                            {"width": "250", "targets": 0}
                    ],
                    "sDom": '<"row"<"col-sm-8 small"i><"col-sm-4"f<"DTsearchlabel">><"col-sm-6 small"l><"col-sm-6"p>>rt<"clear">'

                });
                $('div.dataTables_filter input').addClass('form-control');
                $('div.dataTables_filter input').attr('placeholder', 'Search');
                $(".DTsearchlabel").html('<i class="clip-search"></i>');
                $('.dataTables_filter').attr('style', 'width:100%');
                $('.dataTables_filter label').attr('style', 'width:100%');
                $('.dataTables_length select').attr('style', 'font-size: 12px;padding: 5px 3px;background: #fff;');
                $('div.dataTables_filter input').attr('style', 'float:right;width:98%;margin-right:5px;');
                $(".dataTables_wrapper .dataTables_paginate .paginate_button").attr('style', 'background:#fff !important;color:#444444 !important; border-radius: 0;');


                setTimeout(function () {
                    $('.dataTables_length select').each(function (i, item) {
                        //console.log(item);
                        $(item).select2("destroy");
                    });
                    $('.dataTables_length select').attr('style', 'background: #fff;padding: 5px;color: #858585;');



                }, 500)
            },
            error: function () {
                setTimeout(function () {
                    quickClaim.hideOverlayMessage();
                }, 2000);
                quickClaim.showAjaxMessage(dataExport.messageID, 'error', 'An error occurred while contacting the server.')
            }
        });
    },

    handleCancelRequest: function () {
        if (quickClaim.ajaxLocked) {
            return;
        }

        var data = 'cancelRequestID=' + dataExport.cancelRequestID;

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: dataExport.actions.cancelExportRequest,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('data')) {
                    // for (var a in rs.data) {
                    //     if (rs.data.hasOwnProperty(a)) {
                    //         dataExport.loadTableRow(rs.data[a]);
                    //     }
                    // }

                    $('#dataExportTable').dataTable().fnClearTable();
                    for (var a in rs.data) {
                        if (rs.data.hasOwnProperty(a)) {
                                var oTable = $('#dataExportTable').dataTable();
                                var l = dataExport.tableHeaderColumns.length;
                                var r = rs.data[a];
                                var tr = '<tr id="dataTableRow_' + r['id'] + '">';
                                for (var a = 0; a < l; a++)
                                {
                                    var text = '<td class="' + dataExport.tableHeaderColumns[a] + '">';

                                    if (dataExport.tableHeaderColumns[a] != 'actions') {
                                        text += r[dataExport.tableHeaderColumns[a]];
                                    } else {
                                        text += '<img class="actionsButton" id="dataTableActions_' + r['id'] + '" src="' + dataExport.contextMenu.menuHTML + '" />';
                                    }

                                    text += '</td>';
                                    tr += text;
                                }
                                tr += '</td>';

                                $('#dataExportTable').find('tbody tr#dataTableRow_' + r['id']).remove();
                                  oTable.fnAddData($(tr));
                                //$('#dataExportTable').find('tbody').append($(tr))
                                $('#dataExportTable').trigger('update');
                                $('#dataExportTable').find('.actionsButton').each(function () {
                                    dataExport.contextMenu.setup($(this).prop('id'));
                                });

                                dataExport.dataExports[r['id']] = r;
                                dataExport.contextMenu.setActiveItems('dataTableActions_' + r['id']);

                          
                            //dataExport.loadTableRow(rs.data[a]);
                        }
                    }

                    $('#cancelDataExportRequestDialog').dialog('close');
                    quickClaim.showAjaxMessage(dataExport.messageID, 'success', null);
                }
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                quickClaim.showOverlayMessage('Canceling Backup Request');
                quickClaim.showAjaxMessage(dataExport.messageID, 'spin', 'Canceling Backup Request');
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
            },
            error: function () {
                setTimeout(function () {
                    quickClaim.hideOverlayMessage();
                }, 2000);
                quickClaim.showAjaxMessage(dataExport.messageID, 'error', 'An error occurred while contacting the server.')
            }
        });
    },

    downloadComplete: function () {
        quickClaim.showAjaxMessage(dataExport.messageID, 'success', null);
        quickClaim.hideOverlayMessage();
    },

    handleDownloadRequest: function (id) {
        var data = 'id=' + id;
        var url = dataExport.actions.downloadExportRequest + '?' + data;

        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },

    handleEnterKeyPress: function () {
        if ($('#cancelDataExportRequestDialog').dialog('isOpen')) {
            dataExport.handleCancelRequest();
        } else {
            var tabIndex = $('#tabs').tabs('option', 'active');

            if (tabIndex == 1) {
                $('#backupSubmit').click();
            }
        }
    },

    loadTableRow: function (r) {
        var l = dataExport.tableHeaderColumns.length;

        var tr = '<tr id="dataTableRow_' + r['id'] + '">';
        for (var a = 0; a < l; a++)
        {
            var text = '<td class="' + dataExport.tableHeaderColumns[a] + '">';

            if (dataExport.tableHeaderColumns[a] != 'actions') {
                text += r[dataExport.tableHeaderColumns[a]];
            } else {
                text += '<img class="actionsButton" id="dataTableActions_' + r['id'] + '" src="' + dataExport.contextMenu.menuHTML + '" />';
            }

            text += '</td>';
            tr += text;
        }
        tr += '</td>';

        $('#dataExportTable').find('tbody tr#dataTableRow_' + r['id']).remove();
        $('#dataExportTable').find('tbody').append($(tr))
        $('#dataExportTable').trigger('update');
        $('#dataExportTable').find('.actionsButton').each(function () {
            dataExport.contextMenu.setup($(this).prop('id'));
        });

        dataExport.dataExports[r['id']] = r;
    },

    checkFilename: function () {
        var fileName = $('#data_export_request_filename').val();
        fileName = fileName.trim();
        var extension = null;
        var regex = /[^a-z^0-9^\-^_]/gi;

        if (fileName.lastIndexOf('.') != -1) {
            extension = fileName.substr(fileName.lastIndexOf('.'));
            fileName = fileName.substr(0, fileName.lastIndexOf('.'));
        }

        if (extension != null && extension.toUpperCase() != '.CSV') {
            fileName += extension;
            extension = '.csv';
        }
        fileName = fileName.replace(regex, '') + extension;

        $('#data_export_request_filename').val(fileName);
    },

    fillForm: function (id) {
        if (dataExport.dataExports.hasOwnProperty(id)) {
            var data = dataExport.dataExports[id];

            $('#tabs').tabs('option', 'active', 1);
            for (var a in data) {
                if (a == 'export_type') {
                    $('#requestForm').find('#data_export_request_' + a + '_' + data[a]).prop('checked', true);
                } else {
                    $('#requestForm').find('#data_export_request_' + a).val(data[a]);
                }
            }
        }
    },

    displayCancelDialog: function (id) {
        if (dataExport.dataExports.hasOwnProperty(id)) {
            dataExport.cancelRequestID = id;

            $('#cancelDataExportRequestDialog').dialog('open');
        }
    },

    contextMenu: {
        active: {
            edit: false,
            cancel: false,
            download: false
        },
        menuHTML: null,

        initialize: function () {
            var parameters = {
                selector: '#dataExportTable .actionsButton',
                trigger: 'hover',
                autoHide: true,
                className: 'dataExportMenu',
                items: {
                    edit: {
                        name: 'Edit',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('dataTableActions_', '');
                            dataExport.fillForm(id);
                        },
                        disabled: function () {
                            return !dataExport.contextMenu.active['edit'];
                        }
                    },
                    cancel: {
                        name: 'Cancel',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('dataTableActions_', '');
                            dataExport.displayCancelDialog(id);
                        },
                        disabled: function () {
                            return !dataExport.contextMenu.active['cancel'];
                        }
                    },
                    download: {
                        name: 'Download',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('dataTableActions_', '');
                            dataExport.handleDownloadRequest(id);
                        },
                        disabled: function () {
                            return !dataExport.contextMenu.active['download'];
                        }
                    }
                }
            };
            $.contextMenu(parameters);
        },
        setup: function (id) {
            $(document).on('mouseover','#' + id, function () {
                dataExport.contextMenu.setActiveItems(id);
            });
        },
        setActiveItems: function (id) {
            var id = id.replace('dataTableActions_', '');
            var actions = {};

            if (dataExport.dataExports.hasOwnProperty(id)) {
                actions = dataExport.dataExports[id]['actions'];
            }


            if (actions.hasOwnProperty('edit')) {
                dataExport.contextMenu.enable('edit');
            } else {
                dataExport.contextMenu.disable('edit');
            }

            if (actions.hasOwnProperty('cancel')) {
                dataExport.contextMenu.enable('cancel');
            } else {
                dataExport.contextMenu.disable('cancel');
            }

            if (actions.hasOwnProperty('download')) {
                dataExport.contextMenu.enable('download');
            } else {
                dataExport.contextMenu.disable('download');
            }
        },
        disable: function (item) {
            dataExport.contextMenu.active[item] = false;
        },
        enable: function (item) {
            dataExport.contextMenu.active[item] = true;
        }
    }

};