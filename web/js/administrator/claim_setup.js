var claim_setup = {
    ajax_running: false,

    initialize: function () {
        this.edt.initialize();
        this.modem.initialize();
        this.quick_codes.initialize();
        this.past_claim_params.initialize();
        this.direct_codes.initialize();
        this.claim_params.initialize();
        this.tax.initialize();
        this.custom_validators.initialize();
    },

    tax: {
        button: '',
        data: new Array(),
        form_prefix: 'tax',
        form_id: 'tax_form',

        initialize: function () {
            if ($('#tax_table tbody tr').size()) {
                $('#tax_table').tablesorter1({
                    widgets: ['zebra', 'hover']
                });
            }
            $('#tax_table').on('click','tr',function () {
                var id = $(this).attr('id').replace('tax_', '');
                claim_setup.tax.fillForm(id);
                $('#tax_id').val(id);
            });



            $('#' + this.button + '_clear')
                    .button()
                    .click(function () {
                        claim_setup.tax.clearForm();
                    });

            $('#' + this.button).button();
            
              setTimeout(function(){
                var datatable = $("#tax_table").DataTable();

                datatable.on('search.dt', function () {
                    claim_setup.tax.updateClickEvent();
                })
                datatable.on('length.dt', function (e, settings, len) {
                    console.log('length changed');
                    claim_setup.tax.updateClickEvent();
                });
                datatable.on('page.dt', function () {
                    claim_setup.tax.updateClickEvent();
                });
            },500);
        },
        updateClickEvent: function(){
            setTimeout(function(){
                $('#tax_table .clickable_row').on('click',function () {
                    var id = $(this).attr('id').replace('tax_', '');
                    claim_setup.tax.fillForm(id);
                });
            },500);
        },
        addData: function (id, data) {
            this.data[id] = data;
        },
        clearForm: function () {
            $('#tax_table tr.current_edit').removeClass('current_edit');
            $('#tax_form .form_error_bottom_box').text('').hide();
            $('#tax_form .error_list').remove();
            $('#tax_form .error').removeClass('error');
            
            $('#' + this.form_id).clearForm();
            $("#form_name").val('tax');
        },
        fillForm: function (id) {
            this.clearForm();
            $('#tax_table tr#tax_' + id).addClass('current_edit');
            var data = this.data[id];
            var prefix = '#' + this.form_prefix + '_';

            for (var key in data) {
                if ($(prefix + key)) {
                    $(prefix + key).val(data[key]);
                }
            }
            $("#form_name").val('tax');
            quickClaim.focusTopField();
        }
    },
    edt: {
        go_net_button: 'edt_params_button',
        data: new Array(),
        form_prefix: 'edt_params',

        initialize: function () {
            $('#' + this.go_net_button + '_clear')
                    .button()
                    .click(function () {
                        claim_setup.edt.clearForm();
                    });

            $('#' + this.go_net_button).button();

            if ($('#edt_params_table tbody tr').size()) {
                $('#edt_params_table').tablesorter1({
                    widgets: ['zebra', 'hover']
                });
            }

            $('#edt_params_table .clickable_row').click(function () {
                var doctor_code = $(this).attr('id').replace('edt_param_', '');
                claim_setup.edt.fillForm(doctor_code);
            });

            this.setDoctorName();
              setTimeout(function(){
                var datatable = $("#edt_params_table").DataTable();

                datatable.on('search.dt', function () {
                    claim_setup.edt.updateClickEvent();
                })
                datatable.on('length.dt', function (e, settings, len) {
                    console.log('length changed');
                    claim_setup.edt.updateClickEvent();
                });
                datatable.on('page.dt', function () {
                    claim_setup.edt.updateClickEvent();
                });
            },500);
        },
        updateClickEvent: function(){
            setTimeout(function(){
                $('#edt_params_table .clickable_row').on('click',function () {
                    var id = $(this).attr('id').replace('edt_param_', '');
                    claim_setup.edt.fillForm(id);
                });
            },500);
        },
        addData: function (doctor_code, data) {
            this.data[doctor_code] = data;
        },
        clearForm: function () {
            $('#edt_params_table tr.current_edit').removeClass('current_edit');
            $('#edt_form .form_error_bottom_box').text('').hide();
            $('#edt_form .error_list').remove();
            $('#edt_form .error').removeClass('error');

            $('#edt_form #doctor_name').text('');
            $('#' + this.form_id).clearForm();
            $('#' + this.form_prefix + '_pw_mode_0').prop('checked', true);
        },
        setDoctorName: function () {
            var doctor_id = $('#' + this.form_prefix + '_doctor_id').val();
            if (doctor_id) {
                $('#doctor_name').text($('#edt_params_table tr.doctor_id_' + doctor_id + ' .doctor_name').text());
            }
        },
        fillForm: function (doctor_code) {
            this.clearForm();
            $('#edt_params_table tr#edt_param_' + doctor_code).addClass('current_edit');

            var data = this.data[doctor_code];
            var prefix = '#' + this.form_prefix + '_';
            for (var key in data) {
                if ($(prefix + key)) {
                    if ($(prefix + key).attr('type') == 'checkbox') {
                        $(prefix + key).prop('checked', data[key]);
                    } else if (key == 'pw_mode') {
                        $(prefix + key + '_' + data[key]).prop('checked', true);
                    } else {
                        $(prefix + key).val(data[key]);
                    }
                }
            }
            $('#doctor_name').text($('#edt_param_' + doctor_code + ' .doctor_name').text());
            quickClaim.focusTopField();
        }
    },
    modem: {
        modem_button: 'modem_settings_button',

        initialize: function () {
            $('#' + this.modem_button).button();
        }
    },
    direct_codes: {
        button: 'dsc_button',
        data: {},
        form_prefix: 'direct_service_code',
        form_id: 'dsc_form',
        actions: {
            save_discount: ''
        },

        discounts: {
            messages: {
                checkmark: 'discount_checkmark',
                error: 'discount_error_icon',
                spinner: 'discount_spinner',
                text: 'discount_messages_text'
            },
            data: {},
            table_initialized: false,
            form_prefix: 'direct_service_code_discount',
            form_id: 'discount_form',

            initialize: function () {
                $('#new_discount_button')
                        .button()
                        .click(function () {
                            claim_setup.direct_codes.discounts.loadForm();
                        });

                $('#discount_form_save')
                        .button()
                        .click(function () {
                            claim_setup.direct_codes.discounts.saveForm();
                        });

                $('#discount_form_clear')
                        .button()
                        .click(function () {
                            $('#discount_form_modal').dialog('close');
                        });

                $('#discount_form_modal').dialog({
                    closeOnEscape: true,
                    autoOpen: false,
                    minWidth:800,
                    minHeight:250,
                    modal: true
                });

                sk.datepicker('#' + this.form_prefix + '_start_date');
                sk.datepicker('#' + this.form_prefix + '_end_date');

                var prefix = this.form_prefix;
                $('#' + prefix + '_start_date').on('changeDate', function (e) {
                    $('#' + prefix + '_end_date').skDP('setStartDate', e.date);
                });

                $('#' + this.form_prefix + '_end_date').on('changeDate', function (e) {
                    $('#' + prefix + '_appointment_date_start').skDP('setEndDate', e.date);
                });

                if ($('#dsc_table tr.current_edit').size()) {
                    var id = $('#dsc_table tr.current_edit').attr('id').replace('direct_code_', '');
                    this.loadData(id);
                }
            },

            clearForm: function () {
                $('#discount_form input[type=text]').each(function () {
                    if ($(this).attr('id') != claim_setup.direct_codes.discounts.form_prefix + '__csrf_token') {
                        $(this).val('');
                    }
                });
                $('#discount_form_errors').hide().text('');
                $("#dsc_form_name").val('direct_codes');
            },
            loadForm: function (id) {
                claim_setup.direct_codes.discounts.clearForm();

                $('#' + claim_setup.direct_codes.discounts.form_prefix + '_direct_service_code_id').val($('#' + claim_setup.direct_codes.form_prefix + '_id').val());
                $('#' + claim_setup.direct_codes.discounts.form_prefix + '_active').prop('checked', true);

                if (id) {
                    var data = this.data[id];
                    var prefix = '#' + this.form_prefix + '_';

                    for (var key in data) {
                        if ($(prefix + key).size()) {
                            if ($(prefix + key).attr('type') == 'checkbox') {
                                $(prefix + key).prop('checked', data[key]);
                            } else {
                                $(prefix + key).val(data[key]);
                            }
                        }
                    }
                    quickClaim.focusTopField();
                }

                $('#discount_form_modal').dialog('open');
            },
            saveForm: function () {
                if (claim_setup.ajax_running) {
                    return;
                }

                var data = $('#' + this.form_id).serialize();

                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: claim_setup.direct_codes.actions.save_discount,
                    data: data,
                    success: function (rs) {
                        if (rs.hasOwnProperty('errors')) {
                            quickClaim.showFormErrors(rs.errors, '#discount_form_errors', '#' + claim_setup.direct_codes.discounts.form_id, true);
                            $('#' + claim_setup.direct_codes.discounts.messages.error).show();
                            $('#' + claim_setup.direct_codes.discounts.messages.text).text('The form contains invalid data.').show();
                        } else {
                            var c = rs.data.id;

                            claim_setup.direct_codes.data[rs.data.direct_service_code_id].discounts[c] = rs.data;
                            claim_setup.direct_codes.discounts.loadData(rs.data.direct_service_code_id);

                            claim_setup.direct_codes.discounts.data[rs.data.id] = rs.data;

                            $('#discount_form_modal').dialog('close');
                        }
                    },
                    beforeSend: function () {
                        claim_setup.ajax_running = true;
                        $('#discount_messages .icons div').hide();
                        $('#' + claim_setup.direct_codes.discounts.messages.spinner).show();
                        $('#' + claim_setup.direct_codes.discounts.messages.text).text('Validating and Saving Discount').show();
                    },
                    complete: function () {
                        claim_setup.ajax_running = false;
                        $('#' + claim_setup.direct_codes.discounts.messages.spinner).hide();
                    },
                    error: function () {
                        $('#' + claim_setup.direct_codes.discounts.messages.error).show();
                        $('#' + claim_setup.direct_codes.discounts.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                    }
                });
            },
            loadData: function (id) {
                for (var a in claim_setup.direct_codes.data[id].discounts) {
                    var discount = claim_setup.direct_codes.data[id].discounts[a];

                    if ($('#dsc_discounts_table tbody tr#dsc_discount_' + discount.id).size()) {
                        $('#dsc_discounts_table tbody tr#dsc_discount_' + discount.id).remove();
                    }

                    var tr = '<tr id="dsc_discount_' + discount.id + '" class="clickable_row">'
                            + '<td>' + discount.description + '</td>'
                            + '<td>' + discount.rate + '</td>'
                            + '<td>' + discount.start_date + '</td>'
                            + '<td>' + discount.end_date + '</td>'
                            + '<td>' + (discount.active ? 'Y' : 'N') + '</td>'
                            + '</tr>';

                    $('#dsc_discounts_table tbody').append(tr);

                    $('#dsc_discount_' + discount.id).click(function () {
                        var id = $(this).attr('id').replace('dsc_discount_', '');
                        claim_setup.direct_codes.discounts.loadForm(id);
                    });
                    
                }

                if (!claim_setup.direct_codes.discounts.table_initialized && $('#dsc_discounts_table tbody tr').size()) {
                    $('#dsc_discounts_table').tablesorter1({
                        widgets: ['zebra', 'hover']
                    });
                    claim_setup.direct_codes.discounts.table_initialized = true;
                }
                $('#dsc_discounts_div').show();
                $("#dsc_form_name").val('direct_codes');
            },
            clearData: function () {
                $('#dsc_discounts_div').hide();
                $('#dsc_discounts_table tbody tr').remove();
                this.clearForm();
            }
        },

        initialize: function () {
            if ($('#dsc_table tbody tr').size()) {
                $('#dsc_table').tablesorter1({
                    widgets: ['zebra', 'hover']
                });
            }

            $('#' + this.button + '_clear')
                    .button()
                    .click(function () {
                        claim_setup.direct_codes.clearForm();
                    });

            $('#' + this.button).button();

            $('#dsc_table .clickable_row').click(function () {
                var id = $(this).attr('id').replace('direct_code_', '');
                claim_setup.direct_codes.fillForm(id);
            });

            claim_setup.direct_codes.discounts.initialize();
         setTimeout(function(){
                var datatable = $("#dsc_table").DataTable();

                datatable.on('search.dt', function () {
                    claim_setup.direct_codes.updateClickEvent();
                })
                datatable.on('length.dt', function (e, settings, len) {
                    console.log('length changed');
                    claim_setup.direct_codes.updateClickEvent();
                });
                datatable.on('page.dt', function () {
                    claim_setup.direct_codes.updateClickEvent();
                });
            },500);
        },
        updateClickEvent: function(){
            setTimeout(function(){
                $('#dsc_table').on('click','tr',function () {
                    //alert();
                    var id = $(this).attr('id').replace('direct_code_', '');
                    //alert(id)
                    claim_setup.direct_codes.fillForm(id);
                    $('#direct_service_code_id').val(id);
                });
            },500);
        },
        addData: function (id, data) {
           // alert();
            this.data[id] = data;
            for (var a in data.discounts) {
                this.discounts.data[data.discounts[a].id] = data.discounts[a];
            }
        },
        clearForm: function () {
            $('#dsc_table tr.current_edit').removeClass('current_edit');
            $('#dsc_form .form_error_bottom_box').text('').hide();
            $('#dsc_form .error_list').remove();
            $('#dsc_form .error').removeClass('error');

            $('#' + this.form_id).clearForm();
            claim_setup.direct_codes.discounts.clearData();
        },
        fillForm: function (id) {

            this.clearForm();
            $("#direct_service_code_doctor_iddsc option:selected").removeAttr("selected");
            $('#btnAllLeftdsc').trigger('click');
            //alert(id);
            //console.log(this.data);
            $('#dsc_table tr#direct_code_' + id).addClass('current_edit');
            var data = this.data[id];
            //console.log(data);
            var prefix = '#' + this.form_prefix + '_';
            $("#lstBox1dsc option").prop("selected", false);
            for (var key in data) {
                if ($(prefix + key)) {
                    if ($(prefix + key).attr('type') == 'checkbox') {
                        $(prefix + key).prop('checked', data[key]);
                    } else {

                        if(prefix + key == '#direct_service_code_direct_service_bg_color')
                        {
                            $(prefix + key).minicolors('value', data[key]);  
                        }
                        else if(prefix + key == '#direct_service_code_direct_service_font_color')
                        {
                            $(prefix + key).minicolors('value', data[key]);  
                        }
                        if(prefix + key == '#direct_service_code_doctor_ids')
                        {
                            var array = data[key].split(",");
                            for (var i in array){
                                //alert(array[i]);
                                $("#lstBox1dsc option[value="+array[i]+"]").prop("selected", true);
                                $('#btnRightdsc').trigger('click');
                                $("#direct_service_code_doctor_iddsc option[value="+array[i]+"]").prop("selected", true);
                                 //alert(array[i]);
                            }
                            //alert(prefix + key);
                        }
                        else {
                            $(prefix + key).val(data[key]);  
                        }
                    }
                }
            }
            $('#dsc_table_block').hide();
            $('#dsc_form_block, #dsc_discounts_div').show();
            $('.direct_service_code_name').text($('#direct_service_code_service_code').val());
            quickClaim.focusTopField();

            if (id) {
                this.discounts.clearData();
                this.discounts.loadData(id);
            }
        }
    },
    quick_codes: {
        button: 'qsc_button',
        data: new Array(),
        form_prefix: 'quick_service_code',
        form_id: 'qsc_form',
        actions: {
            lookup: null
        },
        cache: [],
        data: {},
        ajaxLock: false,

        initialize: function () {
            if ($('#qsc_table tbody tr').size()) {
                $('#qsc_table').tablesorter1({
                    widgets: ['zebra', 'hover']
                });
            }

            $('#' + this.button + '_clear')
                    .button()
                    .click(function () {
                        claim_setup.quick_codes.clearForm();
                    });

            $('#' + this.button).button();

            $('#qsc_table').on('click','tr',function () {
                //alert();
            //$('#qsc_table .clickable_row').click(function () {
                var id = $(this).attr('id').replace('quick_code_', '');
                claim_setup.quick_codes.fillForm(id);
                $('#quick_service_code_id').val(id);
            });

            setTimeout(function(){
                var datatable = $("#qsc_table").DataTable();

                datatable.on('search.dt', function () {
                    claim_setup.quick_codes.updateClickEvent();
                })
                datatable.on('length.dt', function (e, settings, len) {
                    console.log('length changed');
                    claim_setup.quick_codes.updateClickEvent();
                });
                datatable.on('page.dt', function () {
                    claim_setup.quick_codes.updateClickEvent();
                });
            },500);

            for (var a in claim_setup.quick_codes.data) {
                claim_setup.quick_codes.cache[claim_setup.quick_codes.cache.length] = claim_setup.quick_codes.data[a].toString;
            }

            var $description = $('#quick_service_code_facility_code');

            $description.autocomplete({
                minLength: 1,
                select: function (event, ui) {
                    var facilityNum = ui.item.label.substring(0, 4);

                    claim_setup.quick_codes.fillInFacility(facilityNum);

                    event.stopPropagation();
                },
                source: claim_setup.quick_codes.cache
            });

            $description.blur(function () {
                var facilityNum = $(this).val().substring(0, 4);
                claim_setup.quick_codes.fillInFacility(facilityNum);
            });

        },
        fillInFacility: function (facilityNum) {
            if (claim_setup.quick_codes.data.hasOwnProperty(facilityNum)) {
                var d = claim_setup.quick_codes.data[facilityNum];
                $('#quick_service_code_facility').val(d.facility_num);
                $('#quick_service_code_facility_code').val(d.toString);
            } else {
                claim_setup.quick_codes.lookup(facilityNum);
            }
        },
        lookup: function (facilityNum) {
            if (claim_setup.ajaxLock) {
                return false;
            }

            var data = 'facilityNum=' + facilityNum;

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: claim_setup.quick_codes.lookupurl,
                data: data,
                success: function (rs) {
                    if (rs.hasOwnProperty('data') && rs.data.hasOwnProperty('0')) {
                        claim_setup.quick_codes.data[rs.data[0].facility_num] = rs.data[0];
                        claim_setup.quick_codes.fillInFacility(rs.data[0].facility_num);
                    } else {
                        $('#quick_service_code_facility_code').val($('#facility_description').val());
                    }
                },
                beforeSend: function (rs) {
                    claim_setup.ajaxLock = true;
                },
                complete: function (rs) {
                    claim_setup.ajaxLock = false;
                },
                error: function (rs) {
                }
            });
        },
        updateClickEvent: function(){
            setTimeout(function(){
                $('#qsc_table .clickable_row').on('click',function () {
                    var id = $(this).attr('id').replace('quick_code_', '');
                    claim_setup.quick_codes.fillForm(id);
                });
            },500);
        },

        addData: function (id, data) {
            this.data[id] = data;
        },
        clearForm: function () {
            $('#qsc_table tr.current_edit').removeClass('current_edit');
            $('#qsc_form .form_error_bottom_box').text('').hide();
            $('#qsc_form .error_list').remove();
            $('#qsc_form .error').removeClass('error');
            //$('#' + this.form_id).clearForm();
            $("#form_name").val('quick_service_code');
        },
        fillForm: function (id) {
            //alert();
            this.clearForm();
            $("#submit_claims_doctor_id option:selected").removeAttr("selected");
            $('#btnAllLeft').trigger('click');
            $('#qsc_table tr#quick_code_' + id).addClass('current_edit');
            var data = this.data[id];
            //console.log('datadata',data);
            var prefix = '#' + this.form_prefix + '_';

            for (var key in data) {
                if ($(prefix + key)) {

                    if ($(prefix + key).attr('type') == 'checkbox') {
                        $(prefix + key).prop('checked', data[key]);
                    } else {

                        if(prefix + key == '#quick_service_code_quick_service_bg_color')
                        {
                             $(prefix + key).minicolors('value', data[key]);  
                        }
                        else if(prefix + key == '#quick_service_code_quick_service_font_color')
                        {
                             $(prefix + key).minicolors('value', data[key]);  
                        }
                        else if(prefix + key == '#quick_service_code_doctor_ids')
                        {
                            var array = data[key].split(",");
                            for (var i in array){
                                $("#lstBox1 option[value="+array[i]+"]").prop("selected", true);
                                $('#btnRight').trigger('click');
                                $("#submit_claims_doctor_id option[value="+array[i]+"]").prop("selected", true);
                                 //alert(array[i]);
                            }

                        } else {
                            $(prefix + key).val(data[key]);    
                        }
                    }
                }
            }

            claim_setup.quick_codes.fillInFacility($("#quick_service_code_facility").val());
            $("#form_name").val('quick_service_code');
            $('#qsc_table_block').hide();
            $('#qsc_form_block').show();

            $('.quick_service_code_name').text($('#quick_service_code_quick_code').val());
            $('#quick_service_code_code_readonly_show').val($('#quick_service_code_code').val());
            $('#quick_service_code_diag_code_readonly_show').val($('#quick_service_code_diag_code').val());
            quickClaim.focusTopField();
        }
    },
    past_claim_params: {
        initialize: function () {
            $('#claim_search_setup_claim_search_default_claim_type_all').closest('tr').before('<tr><td colspan="2">&nbsp;</td></tr>');
            $('#claim_search_setup_claim_search_service_date_start').closest('tr').before('<tr><td colspan="2">&nbsp;</td></tr>');
            $('#claim_search_setup_claim_search_remove_refdoc').closest('tr').before('<tr><td colspan="2">&nbsp;</td></tr>');
            $('#claim_search_setup_claim_search_remove_dob').closest('tr').after('<tr><td colspan="2">&nbsp;</td></tr>');

            $('#claim_search_setup_claim_search_default_claim_type_all').closest('tr').addClass('default_claim_type');
            quickClaim.setupToggleAll('default_claim_type');

            $('#claim_search_setup_claim_search_default_claim_status_all').closest('tr').addClass('default_claim_status');
            quickClaim.setupToggleAll('default_claim_status');
        }
    },
    claim_params: {
        initialize: function () {
            $('#claim_setup_claim_pdf_payee_id').closest('tr').addClass('claim_pdf_payee_id');
            var name = $('#claim_setup_claim_pdf_payee_payee').attr('name');

            $('input[name="' + name + '"]').click(function () {
                if ($(this).attr('value') == 'payee') {
                    $('.claim_pdf_payee_id').show();
                } else {
                    $('.claim_pdf_payee_id').hide();
                }
            });

            if (!$('#claim_setup_claim_pdf_payee_payee:checked').size()) {
                $('.claim_pdf_payee_id').hide();
            }
        }
    },
    custom_validators: {
        actions: {
            save: null
        },
        data: {},
        table_initialized: false,

        initialize: function () {
            $('#new_custom_validator').button();
            $('#save_custom_validator').button();

            $('#new_custom_validator').click(function () {
                claim_setup.custom_validators.clearForm();
                $('#claim_custom_validators_dialog').dialog('open');
            });
            $('#save_custom_validator').click(function () {
                claim_setup.custom_validators.handleFormSave();
            });

            $('#claim_custom_validator_customization_type').change(function () {
                claim_setup.custom_validators.handleCustomizationTypeSelect();
            });

            if ($('#claim_custom_validators_table tbody tr').size()) {
                $('#claim_custom_validators_table').tablesorter1({
                    widgets: ['zebra', 'hover']
                });

                $('#claim_custom_validators_table .clickable_row').click(function () {
                    var id = $(this).attr('id').replace('custom_validator_', '');
                    claim_setup.custom_validators.fillForm(id);
                });

                claim_setup.custom_validators.table_initialized = true;
            }

            $('#claim_custom_validators_dialog').dialog({
                closeOnEscape: true,
                autoOpen: false,
                modal: true,
                minWidth:800,
            minHeight:250,
                width: 800
            });

            $('#claim_custom_validator_require_ref_doc_service_code').closest('tr').addClass('require_ref_doc');
            $('#claim_custom_validator_refdoc_attending_doc').closest('tr').addClass('refdoc_attending_doc');
            claim_setup.custom_validators.handleCustomizationTypeSelect();
        },
        addData: function (id, data) {
            claim_setup.custom_validators.data[id] = data;
        },
        clearForm: function () {
            $('#claim_custom_validator_form .form_error_bottom_box').text('').hide();
            $('#claim_custom_validator_form .error_list').remove();
            $('#claim_custom_validator_form .error').removeClass('error');

            $('#claim_custom_validator_form').clearForm();
            claim_setup.direct_codes.discounts.clearData();
        },
        fillForm: function (id) {
            claim_setup.custom_validators.clearForm();
            var data = claim_setup.custom_validators.data[id];
            var prefix = '#claim_custom_validator_';

            for (var key in data) {
                if ($(prefix + key)) {
                    if ($(prefix + key).attr('type') == 'checkbox') {
                        $(prefix + key).prop('checked', data[key] == 'Y');
                    } else {
                        $(prefix + key).val(data[key]);
                    }
                }
            }

            claim_setup.custom_validators.handleCustomizationTypeSelect();
            $('#claim_custom_validators_dialog').dialog('open');
            quickClaim.focusTopField();
        },
        handleCustomizationTypeSelect: function () {
            var type = $('#claim_custom_validator_customization_type').val();

            $('.require_ref_doc').hide();
            $('.refdoc_attending_doc').hide();

            if (type == '1') { // REQUIRE REFERRING DOCTOR
                $('.require_ref_doc').show();
            } else if (type == '2') { // REFERRING DOCTOR MUST BE ATTENDING DOCTOR
                $('.refdoc_attending_doc').show();
            }
        },
        handleFormSave: function () {
            if (claim_setup.ajax_running) {
                return;
            }

            var data = $('#claim_custom_validator_form').serialize();

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: claim_setup.custom_validators.actions.save,
                data: data,
                success: function (rs) {
                    if (rs.hasOwnProperty('errors')) {
                        quickClaim.showFormErrors(rs.errors, '#claim_custom_validator_errors', '#claim_custom_validator_form', true);
                    } else {
                        var c = rs.data.id;
                        claim_setup.custom_validators.data[c] = rs.data;
                        claim_setup.custom_validators.loadData(c);
                        $('#claim_custom_validators_dialog').dialog('close');
                    }
                },
                beforeSend: function () {
                    claim_setup.ajax_running = true;
//					$('#discount_messages .icons div').hide();
//					$('#' + claim_setup.direct_codes.discounts.messages.spinner).show();
//					$('#' + claim_setup.direct_codes.discounts.messages.text).text('Validating and Saving Discount').show();
                },
                complete: function () {
                    claim_setup.ajax_running = false;
//					$('#' + claim_setup.direct_codes.discounts.messages.spinner).hide();
                },
                error: function () {
//					$('#' + claim_setup.direct_codes.discounts.messages.error).show();
//					$('#' + claim_setup.direct_codes.discounts.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            });
        },
        loadData: function (id) {
            var data = claim_setup.custom_validators.data[id];

            var tr = '<tr class="clickable_row" id="custom_validator_' + data['id'] + '">'
                    + '<td>' + data['name'] + '</td>'
                    + '<td>' + data['customization_type_name'] + '</td>'
                    + '<td>' + data['description'] + '</td>'
                    + '<td>' + data['active'] + '</td>'
                    + '</tr>';

            $('#claim_custom_validators_table tbody tr#custom_validator_' + data['id']).remove();
            $('#claim_custom_validators_table tbody').append(tr);

            $('#claim_custom_validators_table tr#custom_validator_' + data['id']).click(function () {
                var id = $(this).attr('id').replace('custom_validator_', '');
                claim_setup.custom_validators.fillForm(id);
            });

            if (!claim_setup.custom_validators.table_initialized && $('#claim_custom_validators_table tbody tr').size()) {
                $('#claim_custom_validators_table').tablesorter1({
                    widgets: ['zebra', 'hover']
                });
                claim_setup.custom_validators.table_initialized = true;
            } else {
                $('.tablesorter1').trigger("update");
                $('.tablesorter1').trigger('applyWidgets');
            }
        }
    }
}