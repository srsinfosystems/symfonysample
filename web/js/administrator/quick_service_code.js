var quick_codes = {
    actions: {
        edit: null
    },
    handleRowClick: function (v) {
        id = v.replace('quick_code_', '');
        window.location = quick_codes.actions.edit + '?id=' + id;
    },
    initialize: function () {
        this.setupTableRowClick();
        this.setupTablesort();
    },
    setupTableRowClick: function () {
        
        $('#qsc_table tbody tr').click(function (e) {
            console.log('in click function');
            quick_codes.handleRowClick(this.id);
        });
    },
    setupTablesort: function () {
        $('#tablesort').tablesorter1({
            widgets: ['zebra', 'hover']
        });
    },
    init: function () {
         var datatable = $("#qsc_table").DataTable();
         
        datatable.on('search.dt', function () {
            quick_codes.setupTableRowClick();
        })
        datatable.on('length.dt', function (e, settings, len) {
            quick_codes.setupTableRowClick();
        });
        datatable.on('page.dt', function () {
            quick_codes.setupTableRowClick();
        });
    }
};