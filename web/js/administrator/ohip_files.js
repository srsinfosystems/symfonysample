var moh = {
    actions: {
        batch_reverse: null,
        details_reconcile: null,
        download_reconcile: null,
        download_csv: null
    },
    messages: {
        error: 'error_icon',
        spinner: 'spinner',
        checkmark: 'checkmark',
        text: 'messages_text'
    },
    ajaxRunning: false,

    initialize: function () {
        $('#batch_edit_report_dialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: $(window).height() - 200,
            modal: true,
            width: '80%'
        });

        $('#error_report_dialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: $(window).height() - 200,
            modal: true,
            width: '80%'
        });

        $('#remittance_advice_dialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: $(window).height() - 200,
            modal: true,
            width: '80%'
        });

        $('#ra_tabs').tabs();
        $('#ra_tabs').tabs('tabSwitching');

        //All doctor selected when select all
        // $('#submit_claims_doctor_id').change(function(e){
        //    if($(this).val() == 'all') {
        //    var selectedItems = [];
        //     var allOptions = $("#submit_claims_doctor_id option");
        //     allOptions.each(function () {
        //         if($(this).val() != 'all') {
        //         selectedItems.push($(this).val());
        //         }
        //     });
        //     $("#submit_claims_doctor_id").val(selectedItems);
        //     }
        // });


        /*$('#select-all').button();
        $('#select-all').click(function (e) {*/
            //$("#submit_claims_doctor_id option").attr("selected","selected").trigger("change");
            /*var selectedItems = [];
            var allOptions = $("#submit_claims_doctor_id option");
            allOptions.each(function () {
                selectedItems.push($(this).val());
            });
            $("#submit_claims_doctor_id").val(selectedItems).trigger("change");*/
           
            // $("#submit_claims_doctor_id").find('option[value="all"]').prop('selected', true);

            /*$("#submit_claims_doctor_id").find('option[value="all"]').prop('selected',true).trigger('change');*/


       // });
        moh.initSubmitClaim();
        /*$('#submit_claims_doctor_id').change(function(e){
            console.log(e);
            //var value = $(this).val();

            var $select = $('select[name="submit_claims[doctor_id][]"]');
            $all = $select.find('option[value="all"]');
            $allOpts = $select.find('option').not($all);
            // console.log($allOpts);
                if (e.removed) {
                    $all.prop('selected', false);
                }
                if (e.added) {
                    if(e.added.id == "all"){
                        alert("hii");
                        $allOpts.prop('selected', false);
                    }else{
                        $all.prop('selected', false);
                    }
                }
                $(this).html('');
                //$(this).select2('destroy');
                /*if ($all.is(':selected')) {
                    $opts.prop('selected', false);
                } else {
                    $all.prop('selected', false);
                }            
        });*/

        sk.datepicker('#submit_claims_start_date');
        sk.datepicker('#submit_claims_end_date');

        $('#submit_claims_start_date').on('changeDate', function (e) {
            $('#submit_claims_end_date').skDP('setStartDate', e.date);
        });

        $('#submit_claims_end_date').on('changeDate', function (e) {
            $('#submit_claims_start_date').skDP('setEndDate', e.date);
        });

        sk.datepicker('submit_claims_end_date');

        $('#submit-claim').button();
        $('#submit-claim').click(function () {
            moh.handleSubmitClick();
        });

        $('#reconcileArchiveDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: $(window).height() - 200,
            modal: true,
            width: '80%'
        });

        $('#reconcileArchiveAreYouSure').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 200,
            modal: true,
            width: 400,
            buttons: [
                {
                    text: "Yes",
                    "class": 'btn btn-success btn-squared',
                    click: function () {
                        moh.batchAcks.sendArchiveReconcileAjax();
                        $('#reconcileArchiveAreYouSure').dialog('close');
                    }
                },
                {
                    text: "No",
                    "class": 'btn btn-danger btn-squared',
                    click: function () {
                        $('#reconcileArchiveAreYouSure').dialog('close');
                        $('#reconcileArchiveDialog').dialog('close');
                    }
                }
            ]
        });


        $('#archiveBatchAckButton').click(function () {
            moh.batchAcks.archiveBatchAck();
        });

        $('#batchReverseDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: $(window).height() - 200,
            modal: true,
            width: '80%'
        });

        $('#batchResendDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: $(window).height() - 200,
            modal: true,
            width: '80%'
        });

        $('#batchAckReverseAreYouSure').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 200,
            modal: true,
            width: 400,
            buttons: [
                {
                    text: "Yes",
                    "class": 'btn btn-success btn-squared',
                    click: function () {
                        moh.batchAcks.sendReverseBatchAjax();
                        $('#batchAckReverseAreYouSure').dialog('close');
                    }
                },
                {
                    text: "No",
                    "class": 'btn btn-danger btn-squared',
                    click: function () {
                        $('#batchAckReverseAreYouSure').dialog('close');
                        $('#batchReverseDialog').dialog('close');
                    }
                }
            ]
        });

        $('#batchAckResendAreYouSure').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 200,
            modal: true,
            width: 400,
            buttons: [
                {
                    text: "Yes",
                    "class": 'btn btn-success btn-squared',
                    click: function () {
                        moh.batchAcks.sendResendBatchAjax();
                        $('#batchAckResendAreYouSure').dialog('close');
                    }
                },
                {
                    text: "No",
                    "class": 'btn btn-danger btn-squared',
                    click: function () {
                        $('#batchAckResendAreYouSure').dialog('close');
                        $('#batchResendDialog').dialog('close');
                    }
                }
            ]
        });
    },
    initSubmitClaim: function () {

        // setTimeout(function () {
        //     $('#submit_claims_doctor_id').select2('destroy');
        //     var printselect = $('#submit_claims_doctor_id').select2Sortable({width: '100%'});
            
        //     var opts = $('#submit_claims_doctor_id').val();

        //     //var $select = $('select[name="submit_claims[doctor_id][]"]');
        //     var all = printselect.find('option[value="all"]');
        //     console.log(all);
        //     var allOpts = printselect.find('option').not(all);

        //     /*if ($all.is(':selected')) {
        //             $opts.prop('selected', false);
        //         } else {
        //             $all.prop('selected', false);
        //         }*/
        //     printselect.on("change", function (e) {
                
        //         if(e.val.length > 1){

        //             if (e.removed) {
        //                 $(this).find('option[value="all"]').prop('selected', false);                      
        //             }
        //             if (e.added) {
        //                 if(e.added.id == "all"){
        //                     $(this).find('option').not($(this).find('option[value="all"]')).prop('selected', false);
        //                 }else
        //                     $(this).find('option[value="all"]').prop('selected', false);
        //             } 
        //         }

        //         if (e.removed) {
        //             $(this).attr('selected', false);
        //         }
        //         if (e.added) {
        //             $(this).attr('selected', true);

        //         }

        //         //$('#submit_claims_doctor_id').html('');
        //         $('#submit_claims_doctor_id').select2('destroy');
                
        //         $('#submit_claims_doctor_id').select2Sortable({width: '100%'});

        //     });

        // },1000);

    },
    handleSubmitClick: function () {
        if (moh.ajaxRunning) {
            return;
        }

        $('#form_message')
                .removeClass('ui-state-error').removeClass('ui-state-highlight')
                .html('').hide();
        $('.error').removeClass('error');

        var submit = true;
        var errors = {};

        var submit_claims_doctor_id = $.map($('#submit_claims_doctor_id option'), function(e) { return e.value; });
        // as a comma separated string
        var submit_claims_doctor_id = submit_claims_doctor_id.join(',');

        if (!submit_claims_doctor_id) {
            submit = false;
            errors.doctor_code = {'label': 'Doctor Profile', 'message': 'Doctor Profile Is Required'};
        }

        if (submit) {
            if($('#submit_claims_doctor_id').find('option[value="all"]').prop('selected',true)){

                $('#submit_claims_doctor_id').find('option').not($(this).find('option[value="all"]')).prop('selected', true);
                $('#submit_claims_doctor_id').find('option[value="all"]').prop('selected',false);
            }
            batch_create.handleDoctorLocationCountRequest($('#claim-form').serialize());
        } else {
            quickClaim.showFormErrors(errors, '#form_message', '#submit_claims', true);
        }
    },
    getTableClasses: function (table_id) {
        var rs = new Array();

        $('#' + table_id + ' thead tr th').each(function () {
            rs[rs.length] = $(this).attr('class');
        });

        return rs;
    },

    batchAcks: {
        actions: {
            batch_acknowledgement_table: null
        },
        messages: {
            error: 'batchAckErrorIcon',
            spinner: 'batchAckSpinner',
            checkmark: 'batchAckCheckmark',
            text: 'batchAckMessageText',
            spinnerHtml: '',
            checkmarkHtml: '',
            errorHtml: ''
        },
        tableId: '',
        paginationSize: 25,
        paginationSort: null,
        pagerColumns: {},
        initialized: false,
        batchAckActions: null,
        archiveIds: null,
        archiveMode: null,
        reverseIds: null,
        resendIds: null,

        contextMenu: {
            active: {
                resend: true,
                reverse: true,
                download: true,
                archive: true
            },
            menuHtml: null,

            // TODO: configure this 
            // TODO: bottom buttons - reverse, resend, archive

            initialize: function () {
                var parameters = {
                    selector: '#batchAcknowledgementTable .actionsButton',
                    trigger: 'hover',
                    autoHide: true,
                    className: 'batchAckMenu',
                    callback: function (key, options, event) {
                    },
                    items: {
                        reverse: {
                            name: 'Reverse Batch',
                            callback: function (key, options, event) {
                                var id = $(options.$trigger).prop('id').replace('batchAckActions_', '');
                                moh.batchAcks.reverseBatches(id);
                            },
                            disabled: function () {
                                return !moh.batchAcks.contextMenu.active['reverse'];
                            }
                        },
                        resend: {
                            name: 'Re-Send Batch',
                            callback: function (key, options, event) {
                                var id = $(options.$trigger).prop('id').replace('batchAckActions_', '');
                                moh.batchAcks.resendBatches(id);
                            },
                            disabled: function () {
                                return !moh.batchAcks.contextMenu.active['resend'];
                            }
                        },
                        download: {
                            name: 'Download Acknowledgement',
                            callback: function (key, options, event) {
                                var id = $(options.$trigger).prop('id').replace('batchAckActions_', '');
                                moh.batchAcks.downloadBatchAck(id);
                            },
                            disabled: function () {
                                return !moh.batchAcks.contextMenu.active['download'];
                            }
                        },
                        sep1: '---',
                        archive: {
                            name: 'Archive',
                            callback: function (key, options, event) {
                                var id = $(options.$trigger).prop('id').replace('batchAckActions_', '');
                                moh.batchAcks.archiveBatchAck(id);
                            },
                            disabled: function () {
                                return !moh.batchAcks.contextMenu.active['archive'];
                            }
                        }
                    }
                };

                $.contextMenu(parameters);
                moh.batchAcks.contextMenu.setTitle('Placeholder Title');
            },
            setup: function (id) {
                $('#' + id).hover(function () {
                    var batchAckID = $(this).prop('id').replace('batchAckActions_', '');
                    var batchNumber = $('#batchAcknowledgementTable #' + batchAckID + ' .batch_number').text();
                    moh.batchAcks.contextMenu.setActiveItems(batchAckID);
                });
            },
            setActiveItems: function (batchAckID) {
                var actions = moh.batchAcks.batchAckActions[batchAckID];
                var menuItems = ['reverse', 'resend', 'download', 'archive'];

                for (var a = 0; a < menuItems.length; a++) {
                    if (actions.hasOwnProperty(menuItems[a]) && actions[menuItems[a]] != '') {
                        moh.batchAcks.contextMenu.enable(menuItems[a]);
                    } else {
                        moh.batchAcks.contextMenu.disable(menuItems[a]);
                    }
                }
            },
            disable: function (item) {
                moh.batchAcks.contextMenu.active[item] = false;
            },
            enable: function (item) {
                moh.batchAcks.contextMenu.active[item] = true;
            },
            setTitle: function (title) {
                $('.batchAckMenu').attr('data-menutitle', title);
            }
        },

        // Batches Acknowledgements function start
        initialize: function () {
            moh.batchAcks.initBatchAcksTable();

            sk.datepicker('#batch_acknowledgement_start_date');

            $('#batch_acknowledgement_start_date').on('changeDate', function (e) {
                $('#batch_acknowledgement_end_date').skDP('setStartDate', e.date);
            });

            sk.datepicker('#batch_acknowledgement_end_date');

            $('#batch_acknowledgement_end_date').on('changeDate', function (e) {
                $('#batch_acknowledgement_start_date').skDP('setEndDate', e.date);
            });

            $('#batchAckButton').click(function () {
                var tableInst = $("#" + moh.batchAcks.tableId).DataTable();
                tableInst.clear().draw();
                moh.batchAcks.update_table(tableInst);
            });

            $('#batchAckResetFormButton').click(function () {
                moh.batchAcks.clearForm();

            });

//            $('#reverseBatchAckButton').button();
            $('#reverseBatchAckButton').click(function () {
                moh.batchAcks.reverseBatches();
            });

//            $('#resendBatchAckButton').button();
            $('#resendBatchAckButton').click(function () {
                moh.batchAcks.resendBatches();
            });
        },
        initBatchAcksTable: function () {
            // thead
            var colHeader = [];
            for (var i in batch_acknowledgement_header) {
                if (i == 'select') {
                    colHeader.push({title: '<label class="CHKcontainer"><input type="checkbox" id="batchAckTableSelectAll" value="1" checked="checked" /><span class="CHKcheckmark"></span></label>', class: 'edit_checkbox'});
                } else {
                    var arr = i.split('_');
                    if ($.inArray('date', arr) != -1 || i == 'dob') {
                        colHeader.push({title: batch_acknowledgement_header[i], class: i, sType: 'dateField', bSortable: true});
                    } else {
                        colHeader.push({title: batch_acknowledgement_header[i], class: i});
                    }
                }
            }

            var tableId = 'batchAcknowledgementTable';
            var table = global_js.dataTableWithLoader(tableId, false, [], false, colHeader);
            $(".dataTables_empty").addClass('text-center').removeClass('dataTables_empty');


            $("#batchAcknowledgementTable tbody").on('click', 'tr', function () {
                if ($(this).find('input[type="checkbox"]').is(':checked')) {
                    $(this).find('input[type="checkbox"]').prop('checked', false);
                } else {
                    $(this).find('input[type="checkbox"]').prop('checked', true);
                }

            })

            moh.batchAcks.update_table(table);
            moh.batchAcks.tableId = tableId;
            table.on('page.dt', function () {
                setTimeout(function () {
                    $("#batchAcknowledgementTable tbody .actionsButton").each(function () {
                        $(this).on('hover', function () {
                            var batchAckID = $(this).attr('id').replace('batchAckActions_', '');
                            moh.batchAcks.contextMenu.setActiveItems(batchAckID);
                        })
                    })
                }, 1000)
            });
        },
        update_table: function (tableInst) {
            $.ajax({
                url: moh.batchAcks.actions.batch_acknowledgement_table + '?pageNumber=1&size=1000&' + $('#batchAcknowledgementsForm').serialize(), // batches.actions.batches_table + '?pageNumber=1&size=500&' + $('#batches_form').serialize(),
                type: 'GET',
                success: function (res) {
                    global_js.dtLoader(moh.batchAcks.tableId);
                    var obj = JSON.parse(res);

                    if (obj.hasOwnProperty('data') && obj.data.length > 0) {
                        var allData = obj.data;
                        var dtData = [];
                        moh.batchAcks.batchAckActions = {};
                        for (var j in allData) {
                            dtData.push(moh.batchAcks.buildRow(allData[j]));
                        }
                        tableInst.rows.add(dtData);
                        tableInst.draw();
                        $(".selectRsltTbl").select2('destroy');
                        var aa = 0;
                        var apiDt = $("#" + moh.batchAcks.tableId).dataTable().api();
                        apiDt.columns().every(function () {
                            var column = this;
                             var columnText = $.trim($(column.header())[0].innerText);
                            if (aa != 0) {
                                $("#" + moh.batchAcks.tableId + " thead tr:eq(1) th:eq(" + aa + ")").html('');

                                var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                        .appendTo($("#" + moh.batchAcks.tableId + " thead tr:eq(1) th:eq(" + aa + ")"))
                                        .on('change', function () {
                                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                                        });

                                column.data().unique().sort().each(function (d, j) {
                                   if(d != '') {
                                               select.append('<option value="' + d + '">' + d + '</option>');
                                               }
                                });
                            }
                            aa++;
                        });

                        $.fn.dataTableExt.oSort["dateField-desc"] = function (x, y) {
                            var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                            var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                            return (a.diff(b));
                        };

                        $.fn.dataTableExt.oSort["dateField-asc"] = function (x, y) {
                            var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                            var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                            return (b.diff(a));
                        }
                        setTimeout(function () {
                            $('#' + moh.batchAcks.tableId + ' thead tr:eq(1) select').select2({
                                placeholder: "Search",
                                allowClear: true,
                                dropdownAutoWidth: true,
                                width: '98%'
                            });
                            $('#' + moh.batchAcks.tableId + ' .select2-arrow').hide();
                            $("#batchAcknowledgementTable tbody .actionsButton").each(function () {
                                $(this).on('hover', function () {
                                    var batchAckID = $(this).attr('id').replace('batchAckActions_', '');
                                    moh.batchAcks.contextMenu.setActiveItems(batchAckID);
                                })
                            })
                        }, 500);
                    } else {
                        $(".dataTables_empty").addClass('text-center').removeClass('dataTables_empty');
                    }
                    global_js.initCheckbox(moh.batchAcks.tableId);
                },
                beforeSend: function () {
                    $(".dataTables_empty").addClass('text-center').removeClass('dataTables_empty');
                    global_js.dtLoader(moh.batchAcks.tableId, 'start');
                }
            })
        },
        buildRow: function (data) {
            moh.batchAcks.batchAckActions[data['id']] = data['actions'];
            var rowData = [];
            for (var i in batch_acknowledgement_header) {
                if (i == 'select') {
                    rowData.push('<label class="CHKcontainer"><input type="checkbox" data-file_name="' + data['file_name'] + '" data-batch_id="' + data['batch_id'] + '" data-batch_number="' + data['batch_number'] + '" id="batchAckSelect_' + data['id'] + '" value="1" checked="checked" /><span class="CHKcheckmark"></span></label>');
                } else if (i == 'actions') {
                    var html = '<img class="actionsButton" id="batchAckActions_' + data['id'] + '" src="' + moh.batchAcks.contextMenu.menuHtml + '" />';
                    rowData.push(html);
                } else {
                    rowData.push(data[i]);
                }
            }
            return rowData;
        },
        clearForm: function () {
            $('#batchAcknowledgementsForm').clearForm();
            $("select").trigger('change');
            $("#batchAckButton").trigger('click');
        },
        collectIds: function (prefix, id = false) {
            var ids = new Array();
            if (!id) {
                $('#batchAcknowledgementTable tbody input[type="checkbox"]:checked').each(function () {
                    var batchAckID = $(this).attr('id').replace('batchAckSelect_', '');
                    if (moh.batchAcks.batchAckActions.hasOwnProperty(batchAckID) && moh.batchAcks.batchAckActions[batchAckID].hasOwnProperty(prefix) && moh.batchAcks.batchAckActions[batchAckID][prefix]) {
                        ids[ids.length] = batchAckID;
                    } else {
                        $(this).prop('checked', false);
                    }
                });
            } else {
                ids[ids.length] = id;
            }

            if (prefix == 'reverse' || prefix == 'resend') {
                var batchIDs = new Array();
                for (var a = 0; a < ids.length; a++) {
                    var batchID = $('#batchAckSelect_' + ids[a]).attr('data-batch_id');
                    var batchNumber = $('#batchAckSelect_' + ids[a]).attr('data-batch_number');
                    var fileName = '-';
                    if (batchID) {
                        batchIDs[batchIDs.length] = {batchID: batchID, batchNumber: batchNumber, fileName: fileName};
                    }
                }
                return batchIDs;
            }

            return ids;
        },
        archiveBatchAck: function (id) {
            var ids = moh.batchAcks.collectIds('archive', id);
            if (!ids.length) {
                quickClaim.showOverlayMessage('No selected records can be archived.');
                setTimeout(function () {
                    quickClaim.hideOverlayMessage();
                }, 2000);
                return;
            }

            $('#reconcileArchiveTable tbody tr').remove();
            for (var a = 0; a < ids.length; a++) {
                var fileName = $('#batchAckSelect_' + ids[a]).attr('data-file_name');
                var batchNumber = $('#batchAckSelect_' + ids[a]).attr('data-batch_number');

                var tr = '<tr id="archiveReconcile_' + ids[a] + '">'
                        + '<td class="statuses">&nbsp;</td>'
                        + '<td class="fileName">' + fileName + '</td>'
                        + '<td class="batchNumber">' + batchNumber + '</td>'
                        + '<td class="message">&nbsp;</td>'
                        + '</tr>';

                $('#reconcileArchiveTable tbody').append($(tr));
            }
            $('#reconcileArchiveTable').trigger('update');
            moh.batchAcks.archiveIds = ids;
            moh.batchAcks.archiveMode = 'batchAck';

            $('#reconcileArchiveDialog').dialog('open');
            $('#reconcileArchiveAreYouSure').dialog('open');
        },
        sendArchiveReconcileAjax: function () {
            var sendIDs = new Array();
            var extraIDs = new Array();

            for (var a = 0; a < moh.batchAcks.archiveIds.length; a++) {
                if (a < 1) {
                    sendIDs[sendIDs.length] = moh.batchAcks.archiveIds[a];

                    $('#archiveReconcile_' + moh.batchAcks.archiveIds[a] + ' td.message').text('Archiving File');
                    $('#archiveReconcile_' + moh.batchAcks.archiveIds[a] + ' td.statuses').html($(moh.batchAcks.messages.spinnerHtml));
                    $('#archiveReconcile_' + moh.batchAcks.archiveIds[a]).addClass('currentlySending');
                } else {
                    extraIDs[extraIDs.length] = moh.batchAcks.archiveIds[a];
                }
            }

            moh.batchAcks.archiveIds = extraIDs;

            $.ajax({
                type: 'post',
                dataType: 'json',
                data: 'ids=' + sendIDs.join(',') + '&mode=' + moh.batchAcks.archiveMode,
                url: moh.batchAcks.actions.archive,
                success: function (rs) {
                    for (var a in rs.data) {
                        $('#archiveReconcile_' + a).removeClass('currentlySending');
                        $('#archiveReconcile_' + a + ' .message').text('Archived File');
                        $('#archiveReconcile_' + a + ' .statuses').html(moh.batchAck.messages.checkmarkHtml);
                    }

                    if (moh.batchAcks.archiveIds.length) {
                        moh.batchAcks.sendArchiveReconcileAjax();
                    } else {
                        $('#reconcileArchiveDialog').dialog('close');

                        quickClaim.showOverlayMessage('Refreshing Batch Acknowledgements Report');
                        $('#batchAckButton').trigger('click');
                    }
                },
                beforeSend: function () {
                    moh.ajaxRunning = true;

                    $('#messages .icons div').hide();
                    $('#' + moh.batchAcks.messages.spinner).show();
                    $('#' + moh.batchAcks.messages.text).text('Archiving File').show();
                },
                complete: function () {
                    moh.ajaxRunning = false;
                    quickClaim.hideOverlayMessage();
                    $('#' + moh.batchAcks.messages.spinner).hide();
                },
                error: function () {
                    $('#' + moh.batchAcks.messages.error).show();
                    $('#' + moh.batchAcks.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            });
        },
        reverseBatches: function (id) {
            var ids = moh.batchAcks.collectIds('reverse', id);
            if (!ids.length) {
                quickClaim.showOverlayMessage('No selected records have batches to Reverse.');
                setTimeout(function () {
                    quickClaim.hideOverlayMessage();
                }, 2000);
                return;
            }

            $('#batchReverseTable tbody tr').remove();
            for (var a = 0; a < ids.length; a++) {
                var tr = '<tr id="reverseBatch_' + ids[a].batchID + '">'
                        + '<td class="statuses">&nbsp;</td>'
                        + '<td class="fileName">' + ids[a].fileName + '</td>'
                        + '<td class="batchNumber">' + ids[a].batchNumber + '</td>'
                        + '<td class="message">&nbsp;</td>'
                        + '</tr>';

                ids[a] = ids[a].batchID;

                $('#batchReverseTable tbody').append($(tr));
            }
            $('#batchReverseTable').trigger('update');
            moh.batchAcks.reverseIds = ids;

            $('#batchReverseDialog').dialog('open');
            $('#batchAckReverseAreYouSure').dialog('open');
        },
        sendReverseBatchAjax: function () {
            var sendIDs = new Array();
            var extraIDs = new Array();

            for (var a = 0; a < moh.batchAcks.reverseIds.length; a++) {
                if (a < 1) {
                    sendIDs[sendIDs.length] = moh.batchAcks.reverseIds[a];

                    $('#reverseBatch_' + moh.batchAcks.reverseIds[a] + ' td.message').text('Reversing Batch');
                    $('#reverseBatch_' + moh.batchAcks.reverseIds[a] + ' td.statuses').html($(moh.batchAcks.messages.spinnerHtml));
                    $('#reverseBatch_' + moh.batchAcks.reverseIds[a]).addClass('currentlySending');
                } else {
                    extraIDs[extraIDs.length] = moh.batchAcks.reverseIds[a];
                }
            }

            moh.batchAcks.reverseIds = extraIDs;

            $.ajax({
                type: 'post',
                dataType: 'json',
                data: 'ids=' + sendIDs.join(','),
                url: batches.actions.reverse,
                success: function (rs) {
                    for (var a in rs.data) {
                        $('#reverseBatch_' + a + ' td.statuses').html($(moh.batchAcks.messages.checkmarkHtml));
                        $('#reverseBatch_' + a + ' .message').text('Reversed Batch');
                        $('#reverseBatch_' + a).removeClass('currentlySending');
                    }

                    if (moh.batchAcks.reverseIds.length) {
                        moh.batchAcks.sendReverseBatchAjax();
                    } else {
                        $('#batchReverseDialog').dialog('close');
                        quickClaim.showOverlayMessage('Refreshing Batch Acknowledgements Report');
                        $('#batchAckButton').trigger('click');
                    }
                },
                beforeSend: function () {
                    moh.ajaxRunning = true;
                    $('#messages .icons div').hide();
                    $('#' + moh.messages.spinner).show();
                    $('#' + moh.messages.text).text('Reversing Batches').show();
                },
                complete: function () {
                    moh.ajaxRunning = false;
                    quickClaim.hideOverlayMessage();
                    $('#' + moh.messages.spinner).hide();
                },
                error: function () {
                    $('#' + moh.messages.error).show();
                    $('#' + moh.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            });
        },
        resendBatches: function (id) {
            var ids = moh.batchAcks.collectIds('resend', id);
            if (!ids.length) {
                quickClaim.showOverlayMessage('No selected records have batches to Resend.');
                setTimeout(function () {
                    quickClaim.hideOverlayMessage();
                }, 2000);
                return;
            }

            $('#batchResendTable tbody tr').remove();
            for (var a = 0; a < ids.length; a++) {
                var tr = '<tr id="resendBatch_' + ids[a].batchID + '">'
                        + '<td class="statuses">&nbsp;</td>'
                        + '<td class="fileName">' + ids[a].fileName + '</td>'
                        + '<td class="batchNumber">' + ids[a].batchNumber + '</td>'
                        + '<td class="message">&nbsp;</td>'
                        + '</tr>';

                ids[a] = ids[a].batchID;

                $('#batchResendTable tbody').append($(tr));
            }
            $('#batchResendTable').trigger('update');
            moh.batchAcks.resendIds = ids;

            $('#batchResendDialog').dialog('open');
            $('#batchAckResendAreYouSure').dialog('open');
        },
        sendResendBatchAjax: function () {
            var sendIDs = new Array();
            var extraIDs = new Array();

            for (var a = 0; a < moh.batchAcks.resendIds.length; a++) {
                if (a < 5) {
                    sendIDs[sendIDs.length] = moh.batchAcks.resendIds[a];

                    $('#resendBatch_' + moh.batchAcks.resendIds[a] + ' td.message').text('Resending Batch');
                    $('#resendBatch_' + moh.batchAcks.resendIds[a] + ' td.statuses').html($(moh.batchAcks.messages.spinnerHtml));
                    $('#resendBatch_' + moh.batchAcks.resendIds[a]).addClass('currentlySending');
                } else {
                    extraIDs[extraIDs.length] = moh.batchAcks.resendIds[a];
                }
            }

            moh.batchAcks.resendIds = extraIDs;

            $.ajax({
                type: 'post',
                dataType: 'json',
                data: 'ids=' + sendIDs.join(','),
                url: batches.actions.resend,
                success: function (rs) {
                    for (var a in rs.data) {
                        $('#resendBatch_' + moh.batchAcks.resendIds[a] + ' td.statuses').html($(moh.batchAcks.messages.checkmarkHtml));
                        $('#resendBatch_' + a + ' .message').text('Resent Batch');
                        $('#resendBatch_' + a).removeClass('currentlySending');
                    }

                    if (moh.batchAcks.resendIds.length) {
                        moh.batchAcks.sendResendBatchAjax();
                    } else {
                        $('#batchResendDialog').dialog('close');

                        quickClaim.showOverlayMessage('Refreshing Batch Acknowledgements Report');
                        $('#batchAckButton').trigger('click');
                    }
                },
                beforeSend: function () {
                    moh.ajaxRunning = true;
                    $('#messages .icons div').hide();
                    quickClaim.hideOverlayMessage();
                    $('#' + moh.messages.spinner).show();
                    $('#' + moh.messages.text).text('Resending Batches').show();
                },
                complete: function () {
                    moh.ajaxRunning = false;
                    $('#' + moh.messages.spinner).hide();
                },
                error: function () {
                    $('#' + moh.messages.error).show();
                    $('#' + moh.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            });
        },
        downloadComplete: function () {
            $('#messages .icons div').hide();
            if ($('#hiddenDownloader').contents().find('body').text()) {
                $('#' + moh.messages.error).show();
                $('#' + moh.messages.text).text('An error occurred while generating the file').show();
            } else {
                $('#' + moh.messages.checkmark).show();
                $('#' + moh.messages.text).text('Downloaded the file').show();
            }
            quickClaim.hideOverlayMessage();
        },
        downloadBatchAck: function (id) {
            hypeFileDownload.tokenName = "batchAckDownloadToken";
            hypeFileDownload.setReturnFunction(moh.batchAcks.downloadComplete);
            var url = moh.batchAcks.actions.download + "?id=" + id + '&batchAckDownloadToken=' + hypeFileDownload.blockResubmit();

            $('#messages .icons div').hide();
            $('#' + moh.messages.spinner).show();
            $('#' + moh.messages.text).text('Downloading Batch Acknowledgement File').show();
            quickClaim.showOverlayMessage('Downloading Batch Acknowledgement File');

            $('#hiddenDownloader').remove();
            $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
        }
    },
    setupIframeWithURL: function (url, params, mode) {
        hypeFileDownload.tokenName = "batchPDFDownloadToken";
        hypeFileDownload.setReturnFunction(batches.downloadComplete);

        params += '&batchPDFDownloadToken=' + hypeFileDownload.blockResubmit();
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '?' + params + '"  style="width: 0; height: 0; border: none; display: block;"></iframe>');
    }
};
