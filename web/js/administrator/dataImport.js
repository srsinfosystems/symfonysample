var dataImport = {
    actions: {
        acknowledgeFile: null
    },
    messageID: 'dataImportMessages',
    tableHeaderColumns: [],
    dataImports: {},

    initialize: function () {
        if (dataImport.tableHeaderColumns.length == 0) {
            var headers = [];

            $('#dataImportTable').find('thead tr th').each(function () {
                headers[headers.length] = $(this).prop('class');
            });

            dataImport.tableHeaderColumns = headers;
        }

        $('#dataImportTable').tablesorter({
            theme: 'blue',
            headerTemplate: '{content} {icon}',
            sortList: [[0, 0]],
            widgets: ['selectRow', 'zebra']
        });

        dataImport.contextMenu.initialize();
    },

    loadTableRow: function (r) {
        var l = dataImport.tableHeaderColumns.length;

        var tr = '<tr id="dataImportTableRow_' + r['id'] + '">';
        for (var a = 0; a < l; a++) {
            var text = '<td class="' + dataImport.tableHeaderColumns[a] + '">';

            if (dataImport.tableHeaderColumns[a] != 'actions') {
                text += r[dataImport.tableHeaderColumns[a]];
            }
            else {
                if (r[dataImport.tableHeaderColumns[a]].hasOwnProperty('details')) {
                    text += '<img class="actionsButton" id="dataImportTableActions_' + r['id'] + '" src="' + dataImport.contextMenu.menuHTML + '" />';
                }
                else {
                    text += '&nbsp;';
                }
            }

            text += '</td>';
            tr += text;
        }
        tr += '</td>';

        $('#dataImportTable').find('tbody tr#dataImportTableRow_' + r['id']).remove();
        $('#dataImportTable').find('tbody').append($(tr));
        $('#dataImportTable').trigger('update');
        $('#dataImportTable').find('.actionsButton').each(function () {
            dataImport.contextMenu.setup($(this).prop('id'));
        });

        dataImport.dataImports[r['id']] = r;
    },
    goToDetailsScreen: function (id) {
        var importType = dataImport.dataImports[id]['importer_type_str'];

        console.log(dataImport.dataImports[id]);
        if (importType == 'VISUAL PRACTICE DAY SHEET') {
            VPLDetails.fileData = dataImport.dataImports[id];
            VPLDetails.showDialog();
        }
        else {
            console.log('unhandled importer type in goToDetailsScreen ' + importType);
        }
    },
    acknowlegeFile: function (id) {
        var data = 'id=' + id;
        var fileName = dataImport.dataImports[id]['document_name'];

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: dataImport.actions.acknowledgeFile,
            data: data,

            success: function (rs) {
                for (var a in rs['data']) {
                    dataImport.loadTableRow(rs['data'][a]);
                }
                quickClaim.hideAjaxMessage(dataImport.messageID);
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                var message = 'Archiving File ' + fileName;

                quickClaim.showAjaxMessage(dataImport.messageID, 'spin', message);
                quickClaim.showOverlayMessage(message);
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
            },
            error: function () {
                quickClaim.ajaxLocked = false;
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(dataImport.messageID, 'error', message);
            }
        });
    },


    contextMenu: {
        active: {
            details: false,
            acknowledge_all: false
        },
        menuHTML: null,

        initialize: function () {
            var parameters = {
                selector: '#dataImportTable .actionsButton',
                trigger: 'hover',
                autoHide: true,
                className: 'dataImportMenu',
                items: {
                    details: {
                        name: 'Details',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('dataImportTableActions_', '');
                            dataImport.goToDetailsScreen(id);
                        },
                        disabled: function () {
                            return !dataImport.contextMenu.active['details'];
                        }
                    },
                    acknowledge_all: {
                        name: 'Acknowledge File',
                        callback: function (key, options, event) {
                            var id = $(options.$trigger).prop('id').replace('dataImportTableActions_', '');
                            dataImport.acknowlegeFile(id);
                        },
                        disabled: function () {
                            return !dataImport.contextMenu.active['acknowledge_all'];
                        }
                    }
                }
            };

            $.contextMenu(parameters);

        },
        setup: function (id) {
            $('#' + id).mouseover(function () {
                dataImport.contextMenu.setActiveItems(id);
            });
        },
        setActiveItems: function (id) {
            var id = id.replace('dataImportTableActions_', '');
            var actions = {};

            if (dataImport.dataImports.hasOwnProperty(id)) {
                actions = dataImport.dataImports[id]['actions'];
            }

            if (actions.hasOwnProperty('details')) {
                dataImport.contextMenu.enable('details');
            }
            else {
                dataImport.contextMenu.disable('details');
            }

            if (actions.hasOwnProperty('acknowledge_all')) {
                dataImport.contextMenu.enable('acknowledge_all');
            }
            else {
                dataImport.contextMenu.disable('acknowledge_all');
            }
        },
        disable: function (item) {
            dataImport.contextMenu.active[item] = false;
        },
        enable: function (item) {
            dataImport.contextMenu.active[item] = true;
        }
    }
};