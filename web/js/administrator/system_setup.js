var setup = {

    initialize: function () {
        this.user_type.initialize();
        this.system_param.initialize();
    },
    system_param: {
        initialize: function () {
        }
    },
    user_type: {
        button: 'user_type_button',
        data: new Array(),
        prefix: 'user_type_client_permission',
        form_id: 'user_type_form',
        table_id: 'user_type_table',

        initialize: function () {
            $('#' + this.button).button();

            $('#' + this.button + '_clear').button()
            $('#' + this.button + '_clear').click(function () {
                setup.user_type.clearForm();
            });

            $('#' + this.table_id).tablesorter1({
                widgets: ['zebra', 'hover']
            });

            $('#' + this.table_id + ' .clickable_row').click(function () {
                var id = $(this).attr('id').replace('user_type_', '');
                setup.user_type.fillForm(id);
            });
        },
        addData: function (id, data) {
            this.data[id] = data;
        },
        clearForm: function () {
            $('#' + this.table_id + ' tr.current_edit').removeClass('current_edit');
            $('#' + this.form_id + ' .form_error_bottom_box').text('').hide();
            $('#' + this.form_id + ' .error_list').remove();
            $('#' + this.form_id + ' .error').removeClass('error');

            $('#' + this.form_id).clearForm();
        },
        fillForm: function (id) {
            this.clearForm();
            $('#' + this.table_id + ' tr#user_type_' + id).addClass('current_edit');

            var data = this.data[id];
            var prefix = '#' + this.prefix + '_';

            for (var key in data) {
                if ($(prefix + key)) {
                    if ($(prefix + key).attr('type') == 'checkbox') {
                        $(prefix + key).prop('checked', data[key]);
                    }
                    if (key == 'permission_constants') {
                        for (var a in data[key]) {
                            $(prefix + 'permissions_' + data[key][a]).prop('checked', true);
                        }
                    } else {
                        $(prefix + key).val(data[key]);
                    }
                }
            }

            quickClaim.focusTopField();
        },
        permissionsSetup: function (ps) {
            var save_button_row = $('#user_type_button').closest('tr');

            for (var cat_name in ps) {


                var html = '<div class="form-group"><label class="col-sm-2 control-label" >' + cat_name + '</label><div class="col-sm-9 permissionDiv"><ul class="checkbox_list" style="padding-left:0px">'

                for (var idx in ps[cat_name]) {
                    var constant = ps[cat_name][idx];
                    if ($('#user_type_client_permission_permissions_' + constant).size()) {

                        html += '<li>' + $('#user_type_client_permission_permissions_' + constant).closest('li').html() + '</li>';

                        $('#user_type_client_permission_permissions_' + constant).closest('tr').hide();
                        $('#user_type_client_permission_permissions_' + constant).closest('li').remove();
                    }
                }

                html += '</ul></div></div>';

                save_button_row.before($(html));
            }
            $(".permissionDiv ul li").each(function (i, item) {
                var html = $(this).find('label').text().split('-');
                var newhtml = html[0] + ' <i class="clip-question-2" title="' + html[1] + '"></i>';
                $(this).find('label').html(newhtml);
                $(this).addClass('col-sm-4');
                $(this).attr('style', 'color:#858585');
            });
        }
    }
};