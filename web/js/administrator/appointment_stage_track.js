var stage_track = {
    actions: {
        search: null,
        csv: null,
        pdf: null
    },
    messages: {
        checkmark: 'checkmark',
        error: 'error_icon',
        spinner: 'spinner',
        text: 'messages_text'
    },

    ajax_running: false,
    form_error_div: 'appointment_track_errors',
    form_id: null,
    form_prefix: null,
    location_doctors: null,
    province_regions: null,
    region_locations: null,
    results_div: 'results',
    results_table: 'results-table',
    use_provinces: true,
    use_regions: true,
    resetAll: false,
    dtCurrentPage: 1, 
    maxPages: 1,
    dtCurrentResults: 500,
    appStageDate : [],
    appStageName : [],
    columns: new Array(),

    initialize: function () {
        sk.datepicker('#' + this.form_prefix + '_appointment_date_start');
        sk.datepicker('#' + this.form_prefix + '_appointment_date_end');
        $('#' + this.form_prefix + '_appointment_date_start').on('changeDate', function (e) {
            $('#' + stage_track.form_prefix + '_appointment_date_end').skDP('setStartDate', e.date);
        });

        $('#' + this.form_prefix + '_appointment_date_end').on('changeDate', function (e) {
            $('#' + stage_track.form_prefix + '_appointment_date_start').skDP('setEndDate', e.date);
        });

        $('#find_button').button();
        $('#find_button').click(function () {
            $('#select2reports').select2SortableOrder();
            stage_track.handleFindClick();
        });

        $('#pdf_button').button();
        $('#pdf_button').click(function () {
            return stage_track.handlePdfClick();
        });

        $('#csv_button').button();
        $('#csv_button').click(function () {
            return stage_track.handleCsvClick();
        });

        $('.pdf_options_toggle').click(function () {
            stage_track.togglePDFOptions();
        });

        $('#' + stage_track.form_prefix + '_pdf_option_show_subheader').click(function () {
            stage_track.updatePdfOptionsSubheader();
        });
        stage_track.updatePdfOptionsSubheader();

        $('#' + this.form_id + ' div.province input[type=checkbox]').click(function () {
            stage_track.toggleAll('province', $(this).val());
            stage_track.toggleProvinceRegions();
        });

        $('#' + this.form_id + ' div.region input[type=checkbox]').click(function () {
            stage_track.toggleAll('region', $(this).val());
            stage_track.toggleRegionLocations();
        });

        $('#' + this.form_id + ' div.location input[type=checkbox]').click(function () {
            stage_track.toggleAll('location', $(this).val());
            stage_track.toggleLocationDoctors();
        });

        $('#' + this.form_id + ' div.doctor input[type=checkbox]').click(function () {
            stage_track.toggleAll('doctor', $(this).val());
        });

        $('#' + this.form_id + ' div.appointment_type input[type=checkbox]').click(function () {
            stage_track.toggleAll('appointment_type', $(this).val());
        });

        $('#' + this.form_id + ' div.appointment_stage input[type=checkbox]').click(function () {
            stage_track.toggleAll('appointment_stage', $(this).val());
        });

        stage_track.toggleAll('province');
        stage_track.toggleAll('region');
        stage_track.toggleAll('location');
        stage_track.toggleAll('doctor');
        stage_track.toggleAll('appointment_stage');
        stage_track.toggleAll('appointment_type');
        stage_track.toggleProvinceRegions();
//        $('.tabbable').find('input[type="checkbox"][value="all"]').prop('checked', true);
        //init custom start
        $("#locationTab ul")/*.addClass('row')*/.attr('style', 'max-height:60px;overflow:auto;margin-left:0px; margin-right:0px;padding-left:0px;padding-right:0px;');
        $("#locationTab li").attr('style', 'display:inline');
        $("#doctorprofileTab ul").addClass('row').attr('style', 'overflow-y:auto;margin-left:0px; margin-right:0px;padding:0px');
        $("#doctorprofileTab li").addClass('col-md-6');
        $("#appttypeTab ul").addClass('row').attr('style', 'overflow-y:auto;margin-left:0px; margin-right:0px;padding:0px');
        $("#appttypeTab li").addClass('col-md-4');
        $("#otherTab ul").addClass('row').attr('style', 'overflow-y:auto;margin-left:0px; margin-right:0px;padding:0px');
        $("#otherTab li").addClass('col-md-4');
        stage_track.initSearchSelect();

        stage_track.initReportSelect();
        stage_track.initReportColumn();
        $('#stage_track_pdf_option_orientation_L').attr('checked', 'checked');
    },
    initReportColumn: function () {
        var options = '';
        var firstOrder = [];
        var ct = 0;
        var oldorder = [];
        $(".reportColoumnsChkbox").each(function (chkbox) {
            firstOrder[$(this).val()] = $(this).attr('data-position');
            if ($(this).attr('data-position')) {
                ct++;
            }
            oldorder.push({val: $(this).val(), text: $(this).parent().find('label').text(), selected: $(this).is(":checked")});
        })
        var neworder = [];
        for (var r = 0; r < oldorder.length; r++) {
            if (firstOrder[oldorder[r].val]) {
                var kk = firstOrder[oldorder[r].val];
            } else {
                var kk = ct;
                ct++;
            }
            neworder[kk] = oldorder[r];
        }

        for (var newod in neworder) {
            if (neworder[newod]['selected']) {
                options += '<option selected value="' + neworder[newod]['val'] + '">' + neworder[newod]['text'].replace(/ *\([^)]*\) */g, "") + '</option>';
            } else {
                options += '<option value="' + neworder[newod]['val'] + '">' + neworder[newod]['text'].replace(/ *\([^)]*\) */g, "") + '</option>';
            }
        }

        $('#select2reports').html(options);
    },
    initReportSelect: function () {

        setTimeout(function () {
            $('#select2reports').select2('destroy');
            var printselect = $('#select2reports').select2Sortable({width: '100%'});

            var opts = $('#select2reports').val();

            printselect.on("change", function (e) {
                if (e.removed) {
                    $("#stage_track_report_columns_" + e.removed.id).attr('checked', false);
                }
                if (e.added) {
                    $("#stage_track_report_columns_" + e.added.id).attr('checked', true);
                }
                $('#select2reports').html('');
                $('#select2reports').select2('destroy');
                stage_track.initReportColumn();
                $('#select2reports').select2Sortable({width: '100%'});

            });

            $('.reportColoumnsChkbox').change(function () {
                $('#select2reports').html('');
                $('#select2reports').select2('destroy');
                stage_track.initReportColumn();
                $('#select2reports').select2Sortable({width: '100%'});
            });

            $(".btnColChange").click(function () {
                $('#select2reports').html('');
                printselect.val(null).trigger('change');
                stage_track.initReportColumn();
                printselect.trigger("change");
            });
        }, 1000);
    },
    clearErrors: function () {
        $('label.error').remove();
        $('.error').removeClass('error');
        $('#' + stage_track.form_error_div).text('').hide();
    },
    fillResultsTable: function (rs) {
        var table = '<table width="100%" class="DataTableStageTrack table sk-datatable table-hover table-striped" id="' + stage_track.results_table + '">'
                + '<thead><tr>';
        var newColumnsOrder = $('select[name="stage_track[report_columns][]"]').val();
        for (var i = 0; i < newColumnsOrder.length; i++) {

            if (newColumnsOrder[i]) {
                var text2 = newColumnsOrder[i];
                text2 = text2.replace(/\_/g, ' ');
                var text = text2.charAt(0).toUpperCase() + text2.slice(1);
                var className = newColumnsOrder[i];
                text = text.replace(/\_/g, ' ');
                table += '<th  class="sorter-false  ' + className + '">' + text.charAt(0).toUpperCase() + text.slice(1) + '</th>';
            }
        }

//        for (var column in rs.header)
//        {
//            if (column == 'actions') {
//                table += '<th colspan="' + stage_track.results_colspan + '">' + rs.header[column] + '&nbsp;&nbsp;</th>';
//            } else {
//                table += '<th>' + rs.header[column] + '</th>';
//            }
//        }

        table += '</tr></thead>'
                + '<tbody></tbody>'
                + '<tfoot></tfoot>'
                + '</table>';

        $('#' + stage_track.results_div).html(table);

        for (var index in rs.data)
        {
            var row_id = stage_track.results_row_prefix + '_' + rs.data[index]['id'];
            var row = '<tr id="' + row_id + '">';
            for (var column in newColumnsOrder) {
                var text = rs.data[index][newColumnsOrder[column]];
                row += '<td class="' + newColumnsOrder[column] + '">' + text + '</td>';
            }

            row += '</tr>';
            $(row).appendTo($('#' + stage_track.results_table + ' tbody'));
        }

        

        global_js.customFooterDataTable(stage_track.results_table, false,true,true,true, 'skStageTrackPrint');

        $(".skStageTrackPrint").click(function(){
            sk.pdf.orientation = $('input[name="stage_track[pdf_option_orientation]"]:checked').val() == "L" ? 'landscape' : 'portrait';
            sk.pdf.title = $("#stage_track_pdf_option_header").val();
            sk.pdf.fileName = $("#stage_track_pdf_option_header").val()+'_'+moment().unix();
            
            sk.pdf.init('.DataTableStageTrack');
        })
    },
    handleCsvClick: function () {
        if (stage_track.ajax_running) {
            return;
        }

        stage_track.clearErrors();
        data = $('#' + stage_track.form_id).serialize();
        data += '&mode=csv';

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: stage_track.actions.search,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#' + stage_track.form_error_div, '#' + stage_track.form_prefix, true);

                    $('#' + stage_track.messages.error).show();
                    $('#' + stage_track.messages.text).text('The form contains invalid data.').show();
                } else {
                    stage_track.handleCsvRequest(data);
                }
            },
            beforeSend: function () {
                stage_track.ajax_running = true;
                $('#messages .icons div').hide();
                $('#' + stage_track.messages.spinner).show();
                $('#' + stage_track.messages.text).text('Building CSV').show();
            },
            complete: function () {
                stage_track.ajax_running = false;
                $('#' + stage_track.messages.spinner).hide();
            },
            error: function () {
                $('#' + stage_track.messages.error).show();
                $('#' + stage_track.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });
    },
    handleCsvRequest: function (data) {
        var fields = stage_track.splitFormData(data);
        stage_track.setupIframe();
        stage_track.setupHiddenForm(fields, stage_track.actions.csv, 'csv');
    },
    initDatatable: function () {
        sk.pdf.alreadySpliced = false;
        $("#apptSearchErrors").hide();
        $("#service_based").parent('.tab-content').attr('style', 'border-color:#ddd');
        $('a[href="#service_based"]').attr('style', 'border-left-color:#ddd;border-right-color:#ddd;');
        var newColumnsOrder = $('select[name="stage_track[report_columns][]"]').val();
        stage_track.columns = newColumnsOrder;
        // if (newColumnsOrder == null) {

        //     $("#apptSearchErrors").html('Please select report column').addClass('alert alert-danger').show();
        //     $("#service_based").parent('.tab-content').attr('style', 'border-color:#BD1431');
        //     $('a[href="#service_based"]').attr('style', 'border-left-color:#BD1431;border-right-color:#BD1431');
        //     $("#tabs").tabs("option", "active", 1);
        //     return false;
        // }

        var colHeader = [];

        for (var i = 0; i < newColumnsOrder.length; i++) {
            if (newColumnsOrder[i]) {
                var text = newColumnsOrder[i];
                var width = 10;
                text = text.replace(/\_/g, ' ');

                text = text == 'Updated user id' ? 'Updated By UserID' : text;
                text = text == 'Updated username' ? 'Updated By User Name' : text;
                text = text == 'Created user id' ? 'Created By User Name' : text;
                text = text == 'Created User Name' ? 'Created By User Name' : text;

                var arr = newColumnsOrder[i].split('_');
                if ($.inArray('date', arr) != -1 || newColumnsOrder[i] == 'dob') {
                    colHeader.push({title: text.charAt(0).toUpperCase() + text.slice(1), width: '100px', sType: 'dateField', bSortable: true});
                } else {
                    colHeader.push({title: text.charAt(0).toUpperCase() + text.slice(1), width: '100px'});
                }

            }
        }

        quickClaim.ajaxLocked = true;
        if ($.fn.DataTable.isDataTable("#" + stage_track.results_table)) {
            $("#" + stage_track.results_table).dataTable().fnDestroy();
            $("#" + stage_track.results_table).html('');
        }

        if (!$.fn.DataTable.isDataTable('#' + stage_track.results_table)) {
           var CurDate = moment().format(sk.getMomentDatetimeFormat());
           var orientation = $('input[name="stage_track[pdf_option_orientation]"]:checked').val() == "L" ? 'landscape' : 'portrait';
         
           var datatable = $('#' + stage_track.results_table).DataTable({
                "paging": true,
                "lengthMenu": [10, 25, 50, 100, 200, 250, 500],                
                "lengthChange": true,
                "pageLength": sk.skdt_page_size,
                "searching": true,
                "ordering": true,
                "stateSave": true,
                "info": true,
                "dom": 'Bfrtip',
                'fnCreatedRow': function (nRow, aData, iDataIndex) {
                    // var ClaimIdsAdd = $('td .item_id', nRow).val();
                    // var ClaimIds = $('td .claim_id', nRow).val();
                    // //console.log(hidden_fields);
                    // $(nRow).addClass('claim_' + ClaimIds); 
                    // $(nRow).attr('id', 'tr_' + ClaimIdsAdd); // or whatever you choose to set as the id
                },
                "buttons": [
                {
                   text: '<i class="clip-file-pdf"></i> PDF',
                   className: 'btn btn-danger btn-squared skStageTrackPrint',
                },
                {
                    "extend": 'excel',
                    "id":'btnExport',
                    title: $("#appointment_search_pdf_option_header").val(),
                    "text": '<i class="clip-file-excel"></i> Excel',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    },
                    "className": 'btn btn-info btn-squared'
                },
                {
                    "extend": 'print',
                    title: $("#appointment_search_pdf_option_header").val(),
                    "text": '<i class="fa-print fa"></i> Print',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    },
                    "className": 'btn btn-blue btn-squared'
                }
               ],
                "oLanguage": {
                   "sSearch": "",
                "sLengthMenu": "Per page _MENU_",
                "oPaginate": {
                    "sNext": "<i class='clip-chevron-right'></i>",
                    "sPrevious": "<i class='clip-chevron-left'></i>"
                },
                "sInfo": "_START_ to _END_ rows out of _TOTAL_",

                },
                "infoCallback": function( settings, start, end, max, total, pre ) {
                    return start +" to "+ parseInt(end) + ' rows out of '+ total;
                },    
                dom: 'Bfrtip',
                "columns": colHeader,
                "columnDefs": [
                    {"targets": 0, "orderable": false}
                ], //<div class="sk-light-gray w3-light-grey" style="padding: 0px -15px;margin: 0px -15px;"><div class="sk-blue w3-blue" style="height: 5px;width:15%;"></div></div>
                "sDom": '<"wrapper"<"row"<"col-sm-12" <"col-sm-7 actionButtonsDiv"> <"col-sm-5 sk-norightpad" Bl > <"col-sm-12" <"sk-light-gray progressBarCont" <"sk-blue ProgressBarMain">><"row well customWell" <"col-sm-3 customSearchInput" f> <"col-sm-3 customSearchField">  <"col-sm-6 CustomPagination"ip> > > >  >rt<"clear">>',
                initComplete: function () {

                    $(".entireClickable").click(function () {
                    if ($(this).find('input').prop('checked')) {
                        $(this).find('input').prop('checked', false);
                        $(this).removeClass('highlight');
                    } else {
                        $(this).find('input').prop('checked', true);
                        $(this).addClass('highlight');
                    }

                    })
    

                $(".skStageTrackPrint").click(function(){
                    sk.pdf.orientation = $('input[name="stage_track[pdf_option_orientation]"]:checked').val() == "L" ? 'landscape' : 'portrait';
                    sk.pdf.title = ($("#stage_track_pdf_option_header").val() != '') ? $("#stage_track_pdf_option_header").val()  : 'Appointment Stage Search';
                    sk.pdf.fileName = $("#stage_track_pdf_option_header").val()+'_'+moment().unix();
                    
                    sk.pdf.init('.DataTableStageTrack');
                })
                 
                var inputHtml = '<div class="input-group">' +
                        '<input type="text" data-focus="true" placeholder="Contains..." class="form-control DatatableAllRounderSearch" />' +
                        '<span class="input-group-addon cursorPointer btnClearSearchTxt"> ' +
                        '<i class="clip-cancel-circle-2 "></i>' +
                        '</span>' +
                        '<span class="input-group-addon cursorPointer"> ' +
                        '<i class="clip-search-2"></i>' +
                        '</span>' +
                        '</div>';

                $(".customSearchInput").html(inputHtml);

                // var inputHtml2 = '<div class="input-group">' +
                //         '<select class="newDrProfile DatatableAllRounderSearch"></select>' +
                //         '<span class="input-group-addon cursorPointer resetDrProfile"> ' +
                //         '<i class="clip-cancel-circle-2 "></i>' +
                //         '</span>' +
                //         '</div>';
                // $(".customSearchField").html(inputHtml2);
                $(".newDrProfile").html($("#past_claims_doctor_id").html());
                $(".newDrProfile option:eq(0)").text('Change Dr. Profile');
                $(".drSelectMainPopular").select2('destroy');
                var currentDrProfile = $(".drSelectMainPopular").val();
                $(".drSelectMainPopular").select2();
                $(".newDrProfile option[value='" + currentDrProfile + "']").prop('selected', true);
                $(".newDrProfile").select2({width: '100%'});

                $('td').each(function () {
                    if ($(this).attr('style')) {
                    } else {
                        $(this).attr('style', 'padding:5px 2px;');
                    }

                })



                    var searchoptions = $("#" + stage_track.results_table + " thead tr:eq(0) th");
                    var customfilterinputs = '<tr>';
                    for (var j = 0; j < searchoptions.length; j++) {
                        customfilterinputs += '<th></th>';
                    }
                    customfilterinputs += '</tr>';
                    $("#" + stage_track.results_table + " thead").append(customfilterinputs);

                    $('#' + stage_track.results_table).on('length.dt', function (e, settings, len) {
                    });
                }
              });

            $('#' + stage_track.results_table + " tbody").on('click', 'tr', function () {
                if ($(this).find('input[type="checkbox"]').is(':checked')) {
                    $(this).find('input[type="checkbox"]').prop('checked', false);
                } else {
                    $(this).find('input[type="checkbox"]').prop('checked', true);
                }

            })
            // save page length 
            $('#' + stage_track.results_table).on('length.dt', function () {
                var info = $('#' + stage_track.results_table).DataTable().page.info();
                sk.setClientParam('dt_page_size', info.length);
            });

            $(".DatatableAllRounderSearch").keyup(function () {
                datatable.search($(this).val(), true).draw();
            })
            $(".resetDrProfile").click(function () {
//                            $(".newDrProfile").select2('destroy');
                $(".newDrProfile option:eq(0)").prop('selected', true);
//                            $(".newDrProfile").select2();
                $(".newDrProfile").trigger('change');//select2('destroy');

            })


            $(".CHKcontainer input[type='checkbox']").change(function () {
                if ($('.CHKcontainer input[type="checkbox"]:checked').length > 0) {
                    $(".actionButtonsDiv a").attr('disabled', false);
                } else {
                    $(".actionButtonsDiv a").attr('disabled', true);
                }
            });

            $('#resultsCheckAll').click(function () {
                var checked = $(this).prop('checked');
                $('#' + stage_track.results_table + ' input[type=checkbox]').prop('checked', checked);
            });

            $(".btnClearSearchTxt").click(function () {
                $(".DatatableAllRounderSearch").val('');
                datatable.search('', true).draw();
            })
            $(".otherSearchFilter").change(function () {
                var data = $(this).select2({placeholder: "Claim Type", width: '100%'}).val();

                if (data && data.length > 0) {
                    var d2 = '';
                    for (var i in data) {
                        data[i] = data[i].substring(1, data[i].length);
                        d2 += data[i] + '|';
                    }
                    d2 = d2.substring(0, d2.length - 1);
                    $("#" + stage_track.results_table).DataTable().columns('.charge_to')
                            .search(d2, true)
                            .draw();
                } else {
                    datatable
                            .columns('.charge_to')
                            .search('')
                            .draw();
                }
            })

            $(".newDrProfile").change(function () {
                var data = $(this).select2('data');

                var aa = $("#" + stage_track.results_table).DataTable().columns('.doctor').data();

                if (aa.length) {
                    if (data) {
                        data = data.text;
                        $("#" + stage_track.results_table).DataTable().columns('.doctor')
                                .search(data)
                                .draw();
                    } else {
                        datatable
                                .columns('.doctor')
                                .search('')
                                .draw();
                    }
                } else {
                    if (data) {

                        data = data.text;
                        var newoptionval = $(".newDrProfile option:contains(" + data + ")").val();
                        $(".drSelectMainPopular").select2("val", newoptionval).trigger('change');
                        $("#find_button").trigger('click');
                        //$("#" + pastClaims.resultsTable).dataTable().fnDestroy();

                    } else {
                        datatable.columns('.doctor').search('').draw();
                    }
                }
            })

            $('div.dataTables_filter input').addClass('form-control');
            $('div.dataTables_filter input').attr('placeholder', 'Search');
            $(".DTsearchlabel").html('<i class="clip-search"></i>');
            $('.dataTables_filter').attr('style', 'width:100%');
            $('.dataTables_filter label').attr('style', 'width:100%');
            $('.dataTables_length select').attr('style', 'font-size: 12px;padding: 5px 3px;background: #fff;');
            $('div.dataTables_filter input').attr('style', 'float:right;width:98%;margin-right:5px;');
            $(".dataTables_wrapper .dataTables_paginate .paginate_button").attr('style', 'background:#fff !important;color:#444444 !important; border-radius: 0;');

            $(".CustomPagination").prepend('<div style="float:left"><button data-toggle="tooltip" title="Filters" type="button" class="btn btn-default filterToggle"><i class="fa fa-filter"></i></button></div>');

            $(".filterToggle").click(function () {
                $("#" + stage_track.results_table + " thead tr:eq(1)").toggle();
            })
            stage_track.isAddedActionBtn = true;
        } else {

            var datatable = $("#" + stage_track.results_table).DataTable();
        }
        var tblWidth = (+newColumnsOrder.length * 10) - 40;

        datatable.on('search.dt', function () {

           // stage_track.clearSubTotalTable();
            //stage_track.updateSubTotalTable();
        })
        datatable.on('length.dt', function (e, settings, len) {

           //stage_track.clearSubTotalTable();
            //stage_track.updateSubTotalTable();
        });
        datatable.on('page.dt', function () {

            //stage_track.clearSubTotalTable();
            setTimeout(function () {
                //stage_track.updateSubTotalTable();
            }, 200);
        });

          stage_track.processDataTableData(datatable);
          //Past claim sort stay same page http://live.datatables.net/siqaxoqi/1/edited
          var page = 0;
          datatable.on('order', function() {
            if (datatable.page() !== page) {
              datatable.page(page).draw('page');
            }
          });
          datatable.on('page', function() {
            page = datatable.page();
           });

        // bulk action all event handle
        $(".bulkaction_all").click(function () {
            if ($(this).attr('checked')) {
                $("tbody input[type='checkbox']").attr('checked', true);
                $(".btnBulkAction").attr('disabled', false);
            } else {
                $("tbody input[type='checkbox']").attr('checked', false);
                $(".btnBulkAction").attr('disabled', true);
            }
        });

    },
    processDataTableData: function (datatable) {
        var currentPage = stage_track.dtCurrentPage;
        var maxPages = stage_track.maxPages;
        var dataSet = [];
        data = $('#' + stage_track.form_id).serialize()  + "&pageNumber=1&size=" + stage_track.dtCurrentResults;
        stage_track.clearErrors();
        if (currentPage == 1 /*&& pastClaims.resetAll == false*/) {
            stage_track.request = $.ajax({
                url: stage_track.actions.search,
                data: data,
                //url: appt_search.actions.search + '?pageNumber=1&size=' + appt_search.dtCurrentResults + '&' + $('#' + appt_search.form_id).serialize(),
                type: 'POST',
                success: function (res) {
                    var obj = JSON.parse(res);

                    if (obj.hasOwnProperty('errors')) {
                        quickClaim.showFormErrors(obj.errors, '#' + stage_track.form_error_div, '#' + stage_track.form_id, true);
                        $('#' + stage_track.messages.error).show();
                        $('#' + stage_track.messages.text).text('The form contains invalid data.').show();

                    } else {

                            $('#tabs').tabs('enable', 1);
                            $('#tabs').tabs('option', 'active', 1);

                           /* $('#' + pastClaims.messages.text).html('Processing Results.').show();
                            $('#' + pastClaims.messages.checkmark).show();*/

                            if (obj.hasOwnProperty('data')) {

                                // s = obj.headers;
                                // var out = Object.keys(s).map(function(data){
                                //     return [data,s[data]];
                                // });

                                // var a = [];
                                // for ( var i = 0; i < out.length; i++ ) {
                                //     if(out[ i ][0] != 0){
                                //         a.push( out[ i ][0] );
                                //     }
                                // }
                                //pastClaims.updateDefaultData(a.toString());

                                s = obj.header;
                                var out = Object.keys(s).map(function(data){
                                    return [data,s[data]];
                                });

                                var a = [];
                                for ( var i = 0; i < out.length; i++ ) {
                                    a.push( out[ i ][0] );
                                }

                                for (var j in obj.data) {
                                   //   console.log('dataSet',obj.data[j]);
                                    dataSet.push(stage_track.buildDataTableRow(obj.data[j], obj.header));
                                    //dataSet.push(obj.data[j]);
                                    //console.log(dataSet);
                                    // var idS = j+2;
                                    // $('#past_claims_results_table tr:eq('+idS+')').attr('id','tr_');
                                    //console.log(pastClaims.buildDataTableRow(obj.data[j]));
                                }
                                var datatable = $("#" + stage_track.results_table).DataTable();
                                console.log('dataSet',dataSet);
                                // console.log('res',res);
                                //appt_search.fillResultsTable(res);
                                datatable.rows.add(dataSet);
                                //console.log(datatable.rows);
                                datatable.draw();
                                // var i = 0;
                                // for (var j in obj.data) {
                                //         //console.log(obj.data[j]['id']);
                                //         $("#past_claims_results_table tbody tr:eq("+i+")").attr('id','tr_'+obj.data[j]['id']);
                                //         i++;
                                // }

                                //$('#past_claims_results_table tbody tr:eq(1)').attr('id','tr_');
                                stage_track.dtCurrentPage++;
                                stage_track.maxPages = Math.ceil(obj.count / stage_track.dtCurrentResults);
                                // if (stage_track.maxPages > 1) {
                                //     var message = "Found total " + obj.count + " records and fetched " + stage_track.dtCurrentResults + " records, other records are fetching in background.";
                                //     $.notify(message, {
                                //         element: 'body',
                                //         type: "info",
                                //         allow_dismiss: true,
                                //         newest_on_top: true,
                                //         placement: {
                                //             from: "top",
                                //             align: "right"
                                //         },
                                //         delay: 5000,
                                //         z_index: 99999,
                                //         mouse_over: 'pause',
                                //         animate: {
                                //             enter: 'animated bounceIn',
                                //             exit: 'animated bounceOut'
                                //         },
                                //         template:  `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                //                     <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                //                     <span data-notify="icon"></span>
                                //                     <span data-notify="title">{1}</span>
                                //                     <span data-notify="message">{2}</span>
                                //                     <div class="progress" data-notify="progressbar">
                                //                             <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                //                     </div>
                                //                     <a href="{3}" target="{4}" data-notify="url"></a>
                                //             </div>`

                                //     });
                                // }
                                // if(pastClaims.maxPages == pastClaims.dtCurrentPage){                            
                                $(".selectRsltTbl").select2('destroy');
                                var aa = 0;

                                var apiDt = $("#" + stage_track.results_table).dataTable().api();
                                apiDt.columns().every(function () {

                                    var column = this;
                                    var columnText = $.trim($(column.header())[0].innerText);

                                        $("#" + stage_track.results_table + " thead tr:eq(1) th:eq(" + aa + ")").html('');
                                        var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                                .appendTo($("#" + stage_track.results_table + " thead tr:eq(1) th:eq(" + aa + ")")/*$("#" + pastClaims.resultsTable + " thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                                                .on('change', function () {
                                                    var val = $.fn.dataTable.util.escapeRegex(
                                                            $(this).val().replace(/<\/?[^>]+(>|$)/g, "")
                                                            );
                                                    val =  val.replace(/\\/gi, "");
                                                    column
                                                            .search(val ? val : '', true, false)
                                                            .draw();
                                                });

                                        var chkarr = [];
                                        column.data().unique().sort().each(function (d, j) {
                                            d = d.replace(/<\/?[^>]+(>|$)/g, "");

                                            if(columnText == 'Appt date') {
                                                   var ss = d.split(' ')[0];
                                                   if($.inArray(ss, global_js.appStageDate) != -1) {
                                                         stage_track.appStageDate.push(ss);
                                                    } else {
                                                        if(ss != '') {
                                                        select.append('<option value="' + ss + '">' + ss + '</option>');
                                                        stage_track.appStageDate.push(ss);
                                                        }
                                                    }

                                            } else if(columnText == 'Stage name') {
                                                   var ss = d.split(' (')[0];
                                                   if($.inArray(ss, stage_track.appStageName) != -1) {
                                                         stage_track.appStageName.push(ss);
                                                   } else {
                                                        if(ss != '') {
                                                        select.append('<option value="' + ss + '">' + ss + '</option>');
                                                        stage_track.appStageName.push(ss);
                                                        }
                                                   }
                                            }else {
                                                if ($.inArray(d, chkarr) < 0) {
                                                    if(d != '') {
                                                       select.append('<option value="' + d + '">' + d + '</option>');
                                                       }
                                                    chkarr.push(d);
                                                }
                                            }

                                        });
                                    aa++;
                                });
                                $.fn.dataTableExt.oSort["dateField-desc"] = function (x, y) {
                                    var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                                    var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                                    return (a.diff(b));
                                };

                                $.fn.dataTableExt.oSort["dateField-asc"] = function (x, y) {
                                    var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                                    var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                                    return (b.diff(a));
                                }
        //                        }
                                $(".selectRsltTbl").select2({
                                    placeholder: "Search",
                                    allowClear: true,
                                    dropdownAutoWidth: true,
                                    width: '98%'
                                });
                                $('#' + stage_track.results_table + ' .select2-arrow').hide();

                                if ($("#" + stage_track.results_table + " thead tr:eq(1)").is(':visible')) {
                                    $("#" + stage_track.results_table + " thead tr:eq(1)").toggle();
                                }
                                // if (appt_search.isAddedActionBtn == true) {
                                //     appt_search.results.buildActionButtons();
                                //     appt_search.isAddedActionBtn == false;
                                // }
                                //appt_search.afterDtDataFinished();
                                //appt_search.results.buildErrorTooltips();
                                stage_track.updateProgressBar();
                                stage_track.processDataTableData(datatable);
                            }
                    }
                },
                beforeSend: function () {
                  stage_track.ajax_running = true;
                    $('#messages .icons div').hide();
                    $('#' + stage_track.messages.spinner).show();
                    $('#' + stage_track.messages.text).text('Searching for appointments').show();
                  if (stage_track.request != null) {
                        stage_track.request.abort();
                  }

                },
                complete: function () {
                   stage_track.ajax_running = false;
                    $('#' + stage_track.messages.spinner).hide();
                },error: function () {
                    $('#' + stage_track.messages.error).show();
                    $('#' + stage_track.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            })
        } else if (currentPage <= maxPages /* && pastClaims.resetAll == false*/) {
               stage_track.request = $.ajax({
                url: stage_track.actions.search,
                data: $('#' + stage_track.form_id).serialize()  + "&pageNumber="+ currentPage +"&size=" + stage_track.dtCurrentResults,
                //url: appt_search.actions.search + '?pageNumber=1&size=' + appt_search.dtCurrentResults + '&' + $('#' + appt_search.form_id).serialize(),
                type: 'POST',
                success: function (res) {
                    var obj = JSON.parse(res);
                    if (obj.hasOwnProperty('data')) {
                        stage_track.data = {};
                        stage_track.errors = {};

                        for (var j in obj.data) {
                            dataSet.push(stage_track.buildDataTableRow(obj.data[j]));
                        }
                        datatable.rows.add(dataSet);
                        datatable.draw();


                        $(".selectRsltTbl").select2('destroy');
                        var aa = 0;
                        var apiDt = $("#" + stage_track.results_table).dataTable().api();
                        apiDt.columns().every(function () {

                            var column = this;
                            var columnText = $.trim($(column.header())[0].innerText);
                            var selectedSearch = $("#" + stage_track.results_table + " thead tr:eq(1) th:eq(" + aa + ")").find('select').val();

                                $("#" + stage_track.results_table + " thead tr:eq(1) th:eq(" + aa + ")").html('');

                                var select = $('<select class="selectRsltTbl"><option value=""></option></select>')
                                        .appendTo($("#" + stage_track.results_table + " thead tr:eq(1) th:eq(" + aa + ")")/*$("#" + pastClaims.resultsTable + " thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                                        .on('change', function () {
                                            var val = $.fn.dataTable.util.escapeRegex(
                                                    $(this).val().replace(/<\/?[^>]+(>|$)/g, "")
                                                    );

                                            column
                                                    .search(val ? val : '', true, false)
                                                    .draw();
                                        });
                                var chkarr = [];
                                stage_track.appStageName = [];
                                column.data().unique().sort().each(function (d, j) {
                                    d = d.replace(/<\/?[^>]+(>|$)/g, "");

                                    if(columnText == 'Appt date') {
                                           var ss = d.split(' ')[0];
                                           if($.inArray(ss, global_js.appStageDate) != -1) {
                                                 stage_track.appStageDate.push(ss);
                                            } else {
                                                if(ss != '') {
                                                select.append('<option value="' + ss + '">' + ss + '</option>');
                                                stage_track.appStageDate.push(ss);
                                                }
                                            }

                                    } else if(columnText == 'Stage name') {
                                           var ss = d.split(' (')[0];
                                           if($.inArray(ss, stage_track.appStageName) != -1) {
                                                 stage_track.appStageName.push(ss);
                                           } else {
                                                if(ss != '') {
                                                select.append('<option value="' + ss + '">' + ss + '</option>');
                                                stage_track.appStageName.push(ss);
                                                }
                                           }
                                    } else {
                                        if ($.inArray(d, chkarr) < 0) {
                                            var selected = (selectedSearch == d) ? 'selected' : '';
                                            select.append('<option value="' + d + '" '+selected+'>' + d + '</option>');
                                            chkarr.push(d);
                                        }
                                    }

                                });
                            aa++;
                        });
//                        if(pastClaims.maxPages == pastClaims.dtCurrentPage){                   

//                        }
                        $(".selectRsltTbl").select2({
                            placeholder: "Search",
                            allowClear: true,
                            dropdownAutoWidth: true,
                            width: '98%'
                        });
                        $('#' + stage_track.results_table + ' .select2-arrow').hide();
                        stage_track.dtCurrentPage++;
                        //appt_search.afterDtDataFinished();
                        stage_track.updateProgressBar();
//                        if(pastClaims.resetAll == false){
//                            pastClaims.request.abort();
//                            quickClaim.ajaxLocked = false;
//                            datatable.clear().draw();
//                            
//                            pastClaims.processDataTableData(datatable);
//                        }else{
                        $.fn.dataTableExt.oSort["dateField-desc"] = function (x, y) {
                            var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                            var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                            return (a.diff(b));
                        };

                        $.fn.dataTableExt.oSort["dateField-asc"] = function (x, y) {
                            var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                            var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                            return (b.diff(a));
                        }
                       // appt_search.results.buildErrorTooltips();
                        stage_track.processDataTableData(datatable);
//                        }                       
                    }
                },
                beforeSend: function () {
                    if (stage_track.request != null) {
                        stage_track.request.abort();
                    }
                },
                done: function () {
                    if (stage_track.resetAll == true) {
                        stage_track.request.abort();

                    }
                }
            })
        }
                                                                                                                                            
    },
    buildDataTableRow: function (data, header) {

        var action_columns = {};
        var actions_count = 0;
        var max_colspan = 0;
        var results_count = 0;
         var row = '';
        
        if (data.hasOwnProperty('appointment_actions')) {
            for (var action in data['appointment_actions']) {
                if (!stage_track.hasOwnProperty(action)) {
                    stage_track[action] = data['appointment_actions'][action];
                    actions_count++;
                }
            }
        }
        stage_track.action_columns = action_columns;
        var html = [];
        var newColumnsOrder = $('select[name="stage_track[report_columns][]"]').val();
        for (var column in header) {
            var text = data[column];
            if (column == 'appointment_actions') {
                for (var action in this.results_actions) {
                    if (stage_track.action_columns.hasOwnProperty(this.results_actions[action])) {
                        if (data[column].hasOwnProperty(this.results_actions[action])) {
                            html.push(stage_track.action_columns[this.results_actions[action]]);
                        }
                    }
                }
            }
        }
        var dataattr = html.toString();

        var appointment_id = data.appointment_id;
        var id = data.appointment_id;
        var rowData = [];

            for (var a = 0; a < stage_track.columns.length; a++) {
            var className = stage_track.columns[a];
            className = className.replace(' ', '_');
            if (className == 'errors') {
                var e = data['errors'];
                title += '\n';
                if (stage_track.explCodes.hasOwnProperty(e)) {
                        title += stage_track.explCodes[e];
                        if (stage_track.hlx8lines[a]) {
                            var msg = stage_track.hlx8lines[a];
                        } else {
                            var msg = '';
                        }
                        if (msg != '') {
                            title += ' - Error Code Message: ' + msg;
                            }
                    } else {
                        title += 'Unknown error code';
                    }
                
                title = title.replace('?-&gt;', '').replace('&lt;-?', '').replace(/\HX8/g, '');
                var dataError = data['errors'];
                var errs = dataError.split(", ");
                var err = '';
                if(errs.length > 1){
                    $.each(errs, function( index, value ) {
                        var title = stage_track.getErrorTitle(value,a);
                        err += value != '' ? '<span id="errorTitle_' + id + '" data-errorCode="' + value + '" class="' + className + '" title="'+title+'">' + value + '</span>,' : '';
                    });
                    
                }else{
                    var title = stage_track.getErrorTitle(e,a);
                    var err = data['errors'] != '' ? '<span id="errorTitle_' + id + '" data-errorCode="' + data['errors'] + '" class="' + className + '" title="'+title+'">' + data['errors'] + '</span>' : '';
                }
                rowData.push(err);
                if (data['errors']) {
                    stage_track.errors[id] = data['errors'];
                }
            } else {
               /* console.log(pastClaims.explCodes);
                    console.log(e);
                    console.log(data);*/
                if (data[className]) {
                    var text = data[newColumnsOrder[a]];
                    var title = typeof data[className] != 'undefined' ? data[className].toString().replace('_', ' ') : '';
                      rowData.push(title.replace(/<\/?[^>]+(>|$)/g, ""));    
                } 
                   // rowData.push(row);
                else {


                    if (className == 'fee_subm') {
                        var title = '$0';
                    } 
                    // else {
                    //     var title = '';
                    // }
                    else if (className == 'fee_paid') {
                        var title = '$0';
                    } else {
                        var title = '';
                    }
                     rowData.push(title.replace(/<\/?[^>]+(>|$)/g, ""));    
                   
                }
               
                
            }
        }
        return rowData;
    },
    updateProgressBar: function () {
        var currentPage = stage_track.dtCurrentPage;
        var maxPages = stage_track.maxPages;
        currentPage--;
        var percent = (currentPage / maxPages) * 100;
        $(".ProgressBarMain").css('width', percent + '%');
        if (percent == 100) {
            stage_track.isBckgrndActive = false;
            $(".ProgressBarMain").removeClass('sk-blue');
            $(".ProgressBarMain").addClass('sk-green');
        } else {
            $(".ProgressBarMain").removeClass('sk-green');
            $(".ProgressBarMain").addClass('sk-blue');
        }
    },
    handleFindClick: function () {
        if (stage_track.isBckgrndActive == true) {
            stage_track.resetAll = true;
        }
      
        $(".ProgressBarMain").css('width', '0%');
        if (stage_track.resetAll == true) {
            stage_track.dtCurrentPage = 1;
            stage_track.dtCurrentPage = 1;
            if ($.fn.DataTable.isDataTable("#" + stage_track.results_table)) {
                //var datatable = $("#" + pastClaims.resultsTable).DataTable();
                //datatable.clear().draw();
                $("#" + stage_track.results_table).dataTable().fnDestroy();
                $("#" + stage_track.results_table).html('');
            }
            $(".ProgressBarMain").css('width', '0%');
            //astClaims.processDataTableData();
            //return;
        }
        var doctors = localStorage.getItem('doctors');
        var locations = localStorage.getItem('locations');
        localStorage.clear();
        localStorage.setItem('doctors',doctors);
        localStorage.setItem('locations',locations);
        //appt_search.form.clearErrors();
        stage_track.initDatatable();
        stage_track.isBckgrndActive = true;



        // data = $('#' + stage_track.form_id).serialize();
        // stage_track.clearErrors();

        // $.ajax({
        //     type: 'post',
        //     dataType: 'json',
        //     type: 'post',
        //     url: stage_track.actions.search,
        //     data: data,
        //     success: function (rs) {
        //         if (rs.hasOwnProperty('errors')) {
        //             quickClaim.showFormErrors(rs.errors, '#' + stage_track.form_error_div, '#' + stage_track.form_prefix, true);
        //             quickClaim.focusTopField();

        //             $('#' + stage_track.messages.error).show();
        //             $('#' + stage_track.messages.text).text('The form contains invalid data.').show();
        //         } else if (rs.hasOwnProperty('data')) {
        //             $('#tabs').tabs('option', 'disabled', []);
        //             $('#tabs').tabs('option', 'active', 1);
        //             stage_track.fillResultsTable(rs);
        //         }
        //     },
        //     beforeSend: function () {
        //         stage_track.ajax_running = true;
        //         $('#messages .icons div').hide();
        //         $('#' + stage_track.messages.spinner).show();
        //         $('#' + stage_track.messages.text).text('Searching for appointments').show();
        //     },
        //     complete: function () {
        //         stage_track.ajax_running = false;
        //         $('#' + stage_track.messages.spinner).hide();
        //     },
        //     error: function () {
        //         $('#' + stage_track.messages.error).show();
        //         $('#' + stage_track.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
        //     }
        // });
    },
    handlePdfClick: function () {
        if (stage_track.ajax_running) {
            return;
        }

        stage_track.clearErrors();
        data = $('#' + stage_track.form_id).serialize();
        data += '&mode=pdf';

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: stage_track.actions.search,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#' + stage_track.form_error_div, '#' + stage_track.form_prefix, true);

                    $('#' + stage_track.messages.error).show();
                    $('#' + stage_track.messages.text).text('The form contains invalid data.').show();
                } else {
                    stage_track.handlePdfRequest(data);
                }
            },
            beforeSend: function () {
                stage_track.ajax_running = true;
                $('#messages .icons div').hide();
                $('#' + stage_track.messages.spinner).show();
                $('#' + stage_track.messages.text).text('Building PDF').show();
            },
            complete: function () {
                stage_track.ajax_running = false;
                $('#' + stage_track.messages.spinner).hide();
            },
            error: function () {
                $('#' + stage_track.messages.error).show();
                $('#' + stage_track.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });
    },
    handlePdfRequest: function (data) {
        var fields = stage_track.splitFormData(data);
        stage_track.setupIframe();
        stage_track.setupHiddenForm(fields, stage_track.actions.pdf, 'pdf');
    },
    setupIframe: function () {
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    setupHiddenForm: function (fields, action, prefix) {
        setTimeout(function () {
            var frame_body = $('#hiddenDownloader').contents().find('body');
            var form = '<form id="hidden_form" action="' + action + '" method="POST">';

            for (var a in fields) {
                var name = fields[a].substring(0, fields[a].indexOf('='));
                var value = fields[a].substring(fields[a].indexOf('=') + 1);

                name = name.replace(stage_track.form_prefix, prefix);
                form += '<input type="text" value="' + value + '" id="' + name + '" name="' + name + '" />';
            }
            form += '</form>';
            frame_body.append($(form));
            $('#hiddenDownloader').contents().find('#hidden_form').submit();
        });
    },
    splitFormData: function (data) {
        data = data.replace(/%5B/g, '[').replace(/%5D/g, ']');
        return data.split('&');
    },
    toggleAll: function (type, value) {
        var count = $('#' + this.form_id + ' div.' + type + ' li:visible input:checked').size();
        if (value == 'all' && $('#' + this.form_id + ' div.' + type + ' input[value=all]:checked').size()) {
            $('#' + this.form_id + ' div.' + type + ' input:checked').each(function () {
                if ($(this).val() != 'all') {
                    $(this).prop('checked', false);
                }
            });
        } else if (count == 0) {
            if(type == 'location') {
                  $('#' + this.form_id + ' div.' + type + ' input[value="'+stage_track.location_id+'"]').prop('checked', true);
              } else {
                  $('#' + this.form_id + ' div.' + type + ' input[value=all]').prop('checked', true);
              }
        } else if (count > 1) {
            $('#' + this.form_id + ' div.' + type + ' input[value=all]').prop('checked', false);
        }
    },
    toggleLocationDoctors: function () {
        $('#' + this.form_id + ' div.doctor li').hide();
        $('#' + this.form_id + ' div.doctor input[value=all]').parent('li').show();

        var show_all = $('#' + this.form_id + ' div.location input[value=all]:checked').size() && true;
        if (this.use_provinces) {
            show_all = show_all && $('#' + this.form_id + ' div.province input[value=all]:checked').size();
        }
        if (this.use_regions) {
            show_all = show_all && $('#' + this.form_id + ' div.region input[value=all]:checked').size();
        }

        if (show_all) {
            $('#' + this.form_id + ' div.doctor li').show();
        } else if ($('#' + this.form_id + ' div.location input[value=all]:checked').size()) {
            $('#' + this.form_id + ' div.location li:visible input').each(function () {
                var doctor_id = $(this).attr('value');
                if (stage_track.location_doctors.hasOwnProperty(doctor_id)) {
                    for (var a in stage_track.location_doctors[doctor_id]) {
                        $('#' + stage_track.form_prefix + '_doctor_id_' + stage_track.location_doctors[doctor_id][a]).parent('li').show();
                    }
                }
            });
        } else {
            $('#' + this.form_id + ' div.location li:visible input:checked').each(function () {
                var doctor_id = $(this).attr('value');

                if (stage_track.location_doctors.hasOwnProperty(doctor_id)) {
                    for (var a in stage_track.location_doctors[doctor_id]) {
                        $('#' + stage_track.form_prefix + '_doctor_id_' + stage_track.location_doctors[doctor_id][a]).parent('li').show();
                    }
                }
            });
        }

        stage_track.toggleAll('doctor');
    },
    togglePDFOptions: function () {
        if ($('#pdf_options_toggle').hasClass('ui-icon-plusthick')) {
            $('#pdf_options_toggle').removeClass('ui-icon-plusthick');
            $('#pdf_options_toggle').addClass('ui-icon-minusthick');
            $('#pdf_options').show();
        } else {
            $('#pdf_options_toggle').removeClass('ui-icon-minusthick');
            $('#pdf_options_toggle').addClass('ui-icon-plusthick');
            $('#pdf_options').hide();
        }
    },
    toggleProvinceRegions: function () {
        if (this.use_provinces) {
            if ($('#' + this.form_id + ' div.province input[value=all]:checked').size()) {
                $('#' + this.form_id + ' div.region li').show();
            } else {
                $('#' + this.form_id + ' div.region li').hide();
                $('#' + this.form_id + ' div.region input[value=all]').parent('li').show();
                $('#' + this.form_id + ' div.province input:checked').each(function () {
                    var val = $(this).val();
                    if (val && stage_track.province_regions.hasOwnProperty(val)) {
                        for (var a in stage_track.province_regions[val]) {
                            $('#' + stage_track.form_prefix + '_region_id_' + stage_track.province_regions[val][a]).parent('li').show();
                        }
                    }
                });
            }
            stage_track.toggleAll('province');
        }
        $(".location li").attr('style', 'display:inline');
        stage_track.toggleAll('region');
        stage_track.toggleAll('location');
        stage_track.toggleAll('doctor');
        stage_track.toggleRegionLocations();
    },
    toggleRegionLocations: function () {
        if (this.use_regions) {
            $('#' + this.form_id + ' div.location li').hide();
            $('#' + this.form_id + ' div.location input[value=all]').parent('li').show();

            var show_all = true;
            if (this.use_regions) {
                show_all = show_all && $('#' + this.form_id + ' div.region input[value=all]:checked').size();
            }
            if (this.use_provinces) {
                show_all = show_all && $('#' + this.form_id + ' div.province input[value=all]:checked').size();
            }

            if (show_all) {
                $('#' + this.form_id + ' div.location li').show();
            } else if ($('#' + this.form_id + ' div.region input[value=all]:checked').size()) {
                $('#' + this.form_id + ' div.region li:visible input').each(function () {
                    var region_id = $(this).attr('value');

                    if (stage_track.region_locations.hasOwnProperty(region_id)) {
                        for (var a in stage_track.region_locations[region_id]) {
                            $('#' + stage_track.form_prefix + '_location_id_' + stage_track.region_locations[region_id][a]).parent('li').show();
                        }
                    }
                });
            } else {
                $('#' + this.form_id + ' div.region li:visible input:checked').each(function () {
                    var region_id = $(this).attr('value');
                    if (stage_track.region_locations.hasOwnProperty(region_id)) {
                        for (var a in stage_track.region_locations[region_id]) {
                            $('#' + stage_track.form_prefix + '_location_id_' + stage_track.region_locations[region_id][a]).parent('li').show();
                        }
                    }
                });
            }
        }
        $(".location li").attr('style', 'display:inline');
        stage_track.toggleAll('region');
        stage_track.toggleAll('location');
        stage_track.toggleAll('doctor');
        stage_track.toggleLocationDoctors();
    },
    updatePdfOptionsSubheader: function () {
        if ($('#' + stage_track.form_prefix + '_pdf_option_show_subheader:checked').size()) {
            $('.pdf_option_subheader').show();
        } else {
            $('.pdf_option_subheader').hide();
        }
    },
    getChildOptions: function (type) {
        var data = [];
        $("." + type + " input").each(function (e) {
            var val = $(this).val();
            var text = $(this).parent().find('label').text();
            if (val != 'all') {
                data.push({id: val, text: text});
            }
        })
        return data;
    },
    initSearchSelect: function () {
        var allProvince = stage_track.getChildOptions('province');
        var allRegion = stage_track.getChildOptions('region');
        var allLocation = stage_track.getChildOptions('location');
        var allDoctor = stage_track.getChildOptions('doctor');
        var allApptType = stage_track.getChildOptions('appointment_type');
        var allSlotType = stage_track.getChildOptions('slot_type');
//        var allStatus = stage_track.getChildOptions('appointment_status');
        var allStage = stage_track.getChildOptions('appointment_stage');
        var ALL_OPTIONS = [
            {
                id: '1all',
                text: 'Province - all',
                children: allProvince
            },
            {
                id: '2all',
                text: 'Region - all',
                children: allRegion
            },
            {
                id: '3all',
                text: 'Location - all',
                children: allLocation
            },
            {
                id: '4all',
                text: 'Doctor - all',
                children: allDoctor
            },
            {
                id: '5all',
                text: 'Appt Type - all',
                children: allApptType
            },
            {
                id: '6all',
                text: 'Appt Category - all',
                children: allSlotType
            },
//            {
//                id: '7all',
//                text: 'Appt Status - all',
//                children: allStatus
//            },
            {
                id: '8all',
                text: 'Appointment Stage - all',
                children: allStage
            }
        ];
        var select2options = $('.searchSelect').select2({
            multiple: true,
            placeholder: "Select search options...",
            data: ALL_OPTIONS,
            width: '100%',

        })
        select2options.select2('val', ["1all", "2all", "3all", "4all", "5all", "6all", "7all", "8all"]);

        select2options.on('change', function (e) {
            if (e.added) {
                var chkAll = e.added.id;
                chkAll = chkAll.substr(1);

                if (chkAll == 'all') {
                   
                    var type = e.added.text.replace(' - all', '').toLowerCase();
                    type = type.replace(' ', '_');
                    $("." + type + " input[value='all']").attr('checked', true);//.trigger('click');
                    stage_track.toggleAll(type, 'all');
                    stage_track.removeOtherData(type, e.added.id);
                } else {
                    var currentSelected = $("input[value='" + e.added.id + "']").attr('checked', true);//.trigger('click');                    
                    var type = $("input[value='" + e.added.id + "']").parents();
                    type = type[3].classList[0];
                    stage_track.toggleAll(type, currentSelected.val());
                }
            } else if (e.removed) {
                var chkAll = e.removed.id;
                chkAll = chkAll.substr(1);
                if (chkAll == 'all') {
                    if (stage_track.chkOtherIsCheckedOrNot(type)) {

                    } else {
                        var typearr = {};
                        typearr['1all'] = 'province';
                        typearr['2all'] = 'region';
                        typearr['3all'] = 'location';
                        typearr['4all'] = 'doctor';
                        typearr['5all'] = 'appointment_type';
                        typearr['6all'] = 'slot_type';
                        typearr['7all'] = 'appointment_status';
                        typearr['8all'] = 'appointment_stage';
                        stage_track.selectAllOption(typearr[e.removed.id]);
                    }
                } else {
                    var currentSelected = $("input[value='" + e.removed.id + "']").attr('checked', false);//.trigger('click');                    
                    var type = $("input[value='" + e.removed.id + "']").parents();
                    type = type[3].classList[0];

                    if (stage_track.chkOtherIsCheckedOrNot(type)) {
                        stage_track.toggleAll(type, currentSelected.val());
                    } else {
                        stage_track.selectAllOption(type);
                    }


                }
            }
            stage_track.toggleProvinceRegions();
            stage_track.toggleRegionLocations();
            stage_track.toggleLocationDoctors();
        });


        $(".tabbable input[type='checkbox']").click(function () {
            var searchSelect2data = $(".searchSelect").select2('val');
            var type = $(this).parents();
            type = type[3].classList[0];

            var value = $(this).val();
            if ($(this).attr('checked')) {
                if (value == 'all') {
                    stage_track.removeOtherData(type, searchSelect2data);
                } else {
                    searchSelect2data.push($(this).val());
                    stage_track.updateSelectData(searchSelect2data);
                }
            } else {
                if (value == 'all') {
                    //stage_track.removeOtherData(type, searchSelect2data);
                } else {
                    if (stage_track.chkOtherIsCheckedOrNot(type)) {
                        stage_track.removeSingleOption(value);
                    } else {
                        stage_track.removeSingleOption(value);
                        stage_track.selectAllOption(type);
                    }
                }
            }
        })


    $('input:checkbox[name="stage_track[location_id][]"]').prop( "checked", false );
    $('input:checkbox[name="stage_track[location_id][]"][value="'+stage_track.location_id+'"]').prop('checked', true);
    $('input:checkbox[name="stage_track[location_id][]"]').trigger('change'); 
    $('input:checkbox[name="stage_track[location_id][]"][value="'+stage_track.location_id+'"]').trigger('click'); 
    $('input:checkbox[name="stage_track[location_id][]"][value="'+stage_track.location_id+'"]').prop('checked', true);


    },
    selectAllOption: function (type) {
        var oldData = $(".searchSelect").select2('val');
        var typearr = {};
        typearr['province'] = '1all';
        typearr['region'] = '2all';
        typearr['location'] = '3all';
        typearr['doctor'] = '4all';
        typearr['appointment_type'] = '5all';
        typearr['slot_type'] = '6all';
        typearr['appointment_status'] = '7all';
        typearr['appointment_stage'] = '8all';
        oldData.push(typearr[type]);
        stage_track.updateSelectData(oldData);
    },
    removeSingleOption: function (remove) {
        var oldData = $(".searchSelect").select2('val');
        for (var i in oldData) {
            if (oldData[i] == remove) {
                oldData.splice(i, 1);
            }
        }
        stage_track.updateSelectData(oldData);
    },
    chkOtherIsCheckedOrNot: function (type) {
        var rtn = false;
        $("." + type + " input[type='checkbox']").each(function () {
            if ($(this).val() != 'all' && $(this).attr('checked')) {
                rtn = true;
            }
        })
        return rtn;
    },
    updateSelectData: function (data) {
        $(".searchSelect").select2('val', data);
    },
    removeOtherData: function (type, value) {
        var oldData = $(".searchSelect").select2('val');
        var typearr = {};//{"province":"1all","region":"2all","location":"3all","doctor":"4all","appointment_type":"5all","slot_type":"6all","appointment_status":"7all"};
        typearr['province'] = '1all';
        typearr['region'] = '2all';
        typearr['location'] = '3all';
        typearr['doctor'] = '4all';
        typearr['appointment_type'] = '5all';
        typearr['slot_type'] = '6all';
        typearr['appointment_status'] = '7all';
        typearr['appointment_stage'] = '8all';
        oldData.push(typearr[type]);
        $("." + type + " input[type='checkbox']").each(function () {
            var value = $(this).val();
            if ($(this).val() != 'all') {
                oldData = oldData.filter(function (e) {
                    return e !== value
                })
            }
        })
        stage_track.updateSelectData(oldData);
    },
    enableBulkAction: function () {
        var edit = false;
        var cancel = false;
        var reshedule = false;
        var reverse = false;
        var claim = false;
        $("tbody input[type='checkbox']:checked").each(function () {
            var terms = $(this).attr('id').split('_');
            var action = terms[0];
            var appointment_id = terms[1];
            var attendee_id = terms[2];
            var patient_id = terms[3];
        })
    },
    handleBulkAction: function (action) {
        if (action != '') {
            var ids = [];
            $(".bulkAction:checked").each(function () {
                var data = $(this).attr('data-actions');
                data = data.split(',');
                if ($.inArray(action, data) >= 0) {
                    ids.push($(this).attr('id'));
                } else {
                    $(this).attr('checked', false);
                }
            })
            if (ids.length > 0) {
                $("#appointment_book_bulk_action_ids").val(ids.toString());
                $("#appointment_book_mode_bulk").val(action);
                $('#appointment_book_form_bulk').submit();
            }

        }
    }
};
