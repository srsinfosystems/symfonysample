var batch_create = {
    spinner_html: '',
    checkbox_html: null,
    error_html: null,

    doctor_ids: new Array(),
    doctor_idx: null,
    cancel_batches: false,
    batchIds: new Array(),

    actions: {
        batch_create: null,
        batch_distinct_counts: null,
        summaryPDF: null
    },

    initialize: function () {
        $('#batch_creation_dialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            minWidth:800,
            minHeight:250,
            height: $(window).height() - 200,
            modal: true,
            width: '80%',
            close: function () {
                batch_create.resetValues();
            }
        });

        $('#batchCreationConfirmationDialog').dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            width: 300,
            buttons: [
                {
                    text: "Download Summary PDF",
                    "class": 'btn btn-teal btn-squared',
                    click: function () {
                        $('#batchCreationConfirmationDialog').dialog('close');
                        $('#batch_creation_dialog').dialog('close');
                        batch_create.downloadPDFSummary();
                    }
                },{
                    text: "Close",
                    "class": 'btn btn-danger btn-squared',
                    click: function () {
                        $('#batchCreationConfirmationDialog').dialog('close');
                        $('#batch_creation_dialog').dialog('close');
                    }
                }
            ]

        });

        $('#cancel_batch_create').button();
        $('#cancel_batch_create').click(function () {
            batch_create.handleCancel();
        });
    },

    resetValues: function () {
        $('#batch_creation_table tbody tr').remove();
        $('#batch_creation_table tfoot tr td').text('');
        $('#cancel_message').text('');

        batch_create.doctor_ids = new Array();
        batch_create.doctor_idx = null;
        batch_create.cancel_batches = false;
        batch_create.form_values = null;
    },

    downloadComplete: function () {
        quickClaim.hideOverlayMessage();
    },
    downloadPDFSummary: function () {
        var ids = batch_create.batchIds;

        if (!ids.length) {
            quickClaim.showOverlayMessage('No batches to build a summary for at this time.');
            setTimeout(function () {
                quickClaim.hideOverlayMessage();
            }, 2000);
            return;
        }

        hypeFileDownload.tokenName = 'batchPDFDownloadToken';
        hypeFileDownload.setReturnFunction(batch_create.downloadComplete);

        var url = batch_create.actions.summaryPDF + '?ids=' + ids + '&batchPDFDownloadToken=' + hypeFileDownload.blockResubmit();
        quickClaim.showOverlayMessage('Building Summary PDF');

        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    handleDoctorLocationCountRequest: function (form_values) {
        batch_create.resetValues();
        batch_create.form_values = form_values;

        $('#status_text').html(batch_create.spinner_html + '&nbsp; Requesting Claim counts by Doctor Profile & Location');
        $('#batch_creation_dialog').dialog('open');

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: batch_create.actions.batch_distinct_counts,
            data: form_values,
            success: function (rs) {
                $('#status_text').html(batch_create.spinner_html + '&nbsp; Building Doctor Profile & Location list');

                for (var a in rs.data) {
                    var classname = rs.data[a].location_id + '_' + rs.data[a].doctor_id + ' ';
                    classname += (a % 2 == 0) ? 'even' : 'odd';
                    var doctor_name = rs.doctors[rs.data[a].doctor_id];
                    var location_name = rs.locations[rs.data[a].location_id];

                    var tr = '<tr class="status_row_' + classname + '">'
                            + '<td class="status"></td>'
                            + '<td class="doctor">' + doctor_name + ' / ' + location_name + ' &nbsp; (' + rs.data[a].count + ')</td>'
                            + '<td class="batch_number"></td>'
                            + '<td class="file_name"></td>'
                            + '<td class="h"></td>'
                            + '<td class="t"></td>'
                            + '<td class="total"></td>'
                            + '</tr>';

                    $('#batch_creation_table tbody').append($(tr));
                }

                $('#status_text').html('&nbsp; Building Doctor Profile & Location list');

                batch_create.doctor_ids = rs.data;
                batch_create.doctor_idx = 0;
                batch_create.cancel_batches = false;

                batch_create.batchIds = new Array();
                batch_create.handleBatchCreate();
            },
            beforeSend: function () {

            },
            complete: function () {
            },
            error: function () {
                $('#status_text').html(batch_create.error_html + '&nbsp; Unable to retrieve Doctor Profile & Location list');
            }
        });
    },
    handleBatchCreate: function () {
        if (batch_create.cancel_batches) {
            $('#cancel_message').text("Batch Creation Cancelled");
            return;
        }
        var doctor_ids = batch_create.doctor_ids;
        var doctor_idx = batch_create.doctor_idx;

        if (doctor_ids.hasOwnProperty(doctor_idx)) {
            var doctor_id = doctor_ids[doctor_idx].doctor_id;
            var location_id = doctor_ids[doctor_idx].location_id;

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: batch_create.actions.batch_create,
                data: batch_create.form_values + '&doctor_id=' + doctor_id + '&location_id=' + location_id,
                success: function (rs) {
                    var doctor_id = batch_create.doctor_ids[batch_create.doctor_idx].doctor_id;
                    var location_id = batch_create.doctor_ids[batch_create.doctor_idx].location_id;
                    var doctor_name = $('#batch_creation_table tbody tr.status_row_' + location_id + '_' + doctor_id + ' td.doctor').text();

                    $('#status_text').html('&nbsp; Building Batch for ' + doctor_name);
                    $('#batch_creation_table tbody tr.status_row_' + location_id + '_' + doctor_id + ' td.status').html(batch_create.checkbox_html);

                    var result_count = batch_create.displayBatchResults(rs.data);

                    if (result_count < 500) {
                        batch_create.doctor_idx++;
                    }
                    
                    batch_create.handleBatchCreate();
                },
                beforeSend: function () {
                    var doctor_id = batch_create.doctor_ids[batch_create.doctor_idx].doctor_id;
                    var location_id = batch_create.doctor_ids[batch_create.doctor_idx].location_id;
                    var doctor_name = $('#batch_creation_table tbody tr.status_row_' + location_id + '_' + doctor_id + ' td.doctor').text();

                    $('#batch_creation_table tbody tr.status_row_' + location_id + '_' + doctor_id + ' td.status').html(batch_create.spinner_html);
                    $('#status_text').html(batch_create.spinner_html + '&nbsp; Building Batch for ' + doctor_name);
                },
                complete: function () {
                },
                error: function () {
                    var doctor_id = batch_create.doctor_ids[batch_create.doctor_idx].doctor_id;
                    var location_id = batch_create.doctor_ids[batch_create.doctor_idx].location_id;
                    var doctor_name = $('#batch_creation_table tbody tr.status_row_' + location_id + '_' + doctor_id + ' td.doctor').text();

                    $('#batch_creation_table tbody tr.status_row_' + location_id + '_' + doctor_id + ' td.status').html(batch_create.error_html);
                    $('#status_text').html(batch_create.error_html + '&nbsp; Error Encountered while building Batch for ' + doctor_name);
                }
            });
        } else {
            $('#batchCreationConfirmationDialog').dialog('open');
            $('#batchCreationConfirmationCount').text(batch_create.batchIds.length);
        }
    },
    displayBatchResults: function (data) {
        for (var a in data) {
            batch_create.batchIds[batch_create.batchIds.length] = data[a].id;
            var doctor_id = batch_create.doctor_ids[batch_create.doctor_idx].doctor_id;
            var location_id = batch_create.doctor_ids[batch_create.doctor_idx].location_id;

            var row = $('#batch_creation_table tbody tr.status_row_' + location_id + '_' + doctor_id);

            var batch_number = $('td.batch_number', $(row)).html();
            if (batch_number.length) {
                batch_number += '<br />';
            }
            batch_number += data[a].batch_number;
            $('td.batch_number', $(row)).html(batch_number);

            var filename = $('td.file_name', $(row)).html();
            if (filename.length) {
                filename += '<br />';
            }
            filename += data[a].filename;
            $('td.file_name', $(row)).html(filename);

            var h = $('td.h', $(row)).html();
            if (h.length) {
                h += '<br />';
            }
            h += data[a].h;
            $('td.h', $(row)).html(h);

            var t = $('td.t', $(row)).html();
            if (t.length) {
                t += '<br />';
            }
            t += data[a].t;
            $('td.t', $(row)).html(t);

            var total = $('td.total', $(row)).html();
            if (total.length) {
                total += '<br />';
            }
            total += data[a].total;
            $('td.total', $(row)).html(total);

            value = parseInt($('#batch_creation_table tfoot tr .h').text(), 10);
            if (isNaN(value)) {
                value = 0;
            }
            var h = value + data[a].h;
            $('#batch_creation_table tfoot tr .h').text(h);

            value = parseInt($('#batch_creation_table tfoot tr .t').text(), 10);
            if (isNaN(value)) {
                value = 0;
            }
            var t = value + data[a].t;
            $('#batch_creation_table tfoot tr .t').text(t);

            value = parseFloat($('#batch_creation_table tfoot tr .total').text().replace('$', '').replace(',', ''), 10);
            if (isNaN(value)) {
                value = 0;
            }
            var total_amount = value + parseFloat(data[a].total.replace('$', '').replace(',', ''), 10);
            $('#batch_creation_table tfoot tr .total').text('$' + number_format(total_amount, 2, '.', ','));
        }

        return data[a].h;

    },
    handleCancel: function () {
        batch_create.cancel_batches = true;
        $('#status_text').html('Batch Creation Cancelled By User');
    }
};