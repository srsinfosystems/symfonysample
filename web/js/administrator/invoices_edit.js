var invoice = {
    actions: {
        delete_invoice: null,
        invoice_pdf: null
    },
    initialize: function () {
        $('#delete_invoice').button();
        $('#delete_invoice').click(function () {
            $('#delete_confirm').dialog('open');
        });

        $('#pdf_invoice').button();
        $('#pdf_invoice').click(function () {
            $('#invoice_sort').dialog('open');
        });

        $("#delete_confirm").dialog({
            resizable: false,
            height: 200,
            modal: true,
            autoOpen: false,
            buttons: [
                {
                    text: "Delete Invoice",
                    "class": 'btn btn-success btn-squared',
                    click: function () {
                        window.location = invoice.actions.delete_invoice;
                    }
                }, {
                    text: "Cancel",
                    "class": 'btn btn-danger btn-squared',
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ]
        });

        $('#invoice_sort').dialog({
            resizable: true,
            width: 500,
            modal: true,
            autoOpen: false,
            buttons: {
                "By Name": function () {
                    invoice.getPdf(invoice.actions.invoice_pdf + '/sort/name');
                },
                "By Date": function () {
                    invoice.getPdf(invoice.actions.invoice_pdf + '/sort/date');
                },
                "Unpaid First": function () {
                    invoice.getPdf(invoice.actions.invoice_pdf + '/sort/unpaid_first');
                },
                "Unpaid Last": function () {
                    invoice.getPdf(invoice.actions.invoice_pdf + '/sort/unpaid_last');
                },
                "Cancel": function () {
                    $('#invoice_sort').dialog('close');
                }
            }

        });
    },
    getPdf: function (url) {
        window.location = url;
    }
}