var client_config = {

    initialize: function () {
        this.name_address.initialize();
        this.subscription.initialize();
        //this.permissions.initialize();
        this.parameters.initialize();
    },
    subscription: {
        form_prefix: '',
        form_button: '',
        form_id: '',

        initialize: function () {
            $('#' + this.form_id + ' input[type=checkbox]').click(function () {
                client_config.subscription.calculatePrices();
            });
            client_config.subscription.calculatePrices();
            
            sk.datepicker('#' + this.form_prefix + '_subscription_expiry');
            
            $('#' + this.form_button).button();
        },

        makeCheckboxRequired: function (field_name) {
            $('#client_subscription_' + field_name).click(function () {
                $(this).prop('checked', true);
                client_config.subscription.calculatePrices();
            });
            $('#client_subscription_' + field_name).prop('checked', true);
        },
        calculatePrices: function () {
            var doctor_total = 0;
            var user_total = 0;
            var location_total = 0;

            $('#' + this.form_id + ' input[type=checkbox]:checked').each(function () {
                var doctor_fee = $('.doctor_fee', $(this).parent('td').parent('tr')).text();
                var user_fee = $('.user_fee', $(this).parent('td').parent('tr')).text();
                var location_fee = $('.location_fee', $(this).parent('td').parent('tr')).text();

                if (doctor_fee) {
                    doctor_fee = doctor_fee.replace(/[^0-9\.]/g, '');
                    doctor_total += parseFloat(doctor_fee, 10);
                }
                if (user_fee) {
                    user_fee = user_fee.replace(/[^0-9\.]/g, '');
                    user_total += parseFloat(user_fee, 10);
                }

                if (location_fee) {
                    location_fee = location_fee.replace(/[^0-9\.]/g, '');
                    location_total += parseFloat(location_fee, 10);
                }
            });

            $('#doctor_fee_total').text('$' + number_format(doctor_total, 2));
            $('#user_fee_total').text('$' + number_format(user_total, 2));
            $('#location_fee_total').text('$' + number_format(location_total, 2));
        }

    },
    name_address: {
        form_prefix: '',
        form_button: '',

        initialize: function () {
            var empty_row = '<tr class="small_row"><td>&nbsp;</td><td>&nbsp;</td></tr>';

            $('#' + this.form_prefix + '_company_name').parent('td').parent('tr').before(empty_row);
            $('#' + this.form_prefix + '_contact').parent('td').parent('tr').before(empty_row);

            $('#' + this.form_prefix + '_postal_code').blur(function () {
                quickClaim.formatPostal('#' + $(this).attr('id'));
            });
            $('#' + this.form_prefix + '_phone').blur(function () {
                quickClaim.formatPhone('#' + $(this).attr('id'));
            });
            $('#' + this.form_prefix + '_fax').blur(function () {
                quickClaim.formatPhone('#' + $(this).attr('id'));
            });
            $('#' + this.form_prefix + '_contact_phone').blur(function () {
                quickClaim.formatPhone('#' + $(this).attr('id'));
            });

            $('#' + this.form_button).button();
        }
    },
    permissions: {
        permissions_button: 'permissions_button',

        initialize: function () {
        },
        permissionsSetup: function (ps) {
            //console.log(this.permissions_button);
            var save_button_row = $('#' + this.permissions_button).closest('div[class="form-group"]');
            //console.log(save_button_row);

            for (var cat_name in ps) {
                var html = '<div class="form-group"><label class="col-sm-2 control-label" for="form-field-1"><b>' + cat_name + '</b></label><div class="col-sm-10"><ul class="checkbox_list customFormat" style="padding-bottom:10px;">';

                for (var idx in ps[cat_name]) {
                    var constant = ps[cat_name][idx];

                    if ($('#permissions_permissions_' + constant).size()) {
                        //console.log($('#permissions_permissions_' + constant).closest('label').html());
                        //console.log($('#permissions_permissions_' + constant));
                        html += '<li>' +
                                $('#permissions_permissions_' + constant).parent('li').html()

                                + '</li>';

                        //$('#permissions_permissions_' + constant).parent('li').parent('ul').parent('td').parent('tr').hide();
                        //$('#permissions_permissions_' + constant).parent('li').remove();
                    }
                }
                html += '</ul></div></div>';

                save_button_row.before(html);

                //console.log(html);
            }

//            setTimeout(function () {
            $('.main-conatinaer ul').remove();
            $('.main-conatinaer label').remove();
            $(".customFormat li label").each(function (e) {
                var label = $(this).html();
                var lblarr = label.split('-');
                var newlabel = '<b>' + lblarr[0] + '</b>' + '<span style="color:#858585"> - ' + lblarr[1] + '</span>';
                $(this).html(newlabel);
                //console.log(newlabel)
            })
//            }, 2000);
        }
    },
    parameters: {
        appointment_status_text: '',
        week_checked: false,
        form_prefix: '',

        initialize: function () {
            this.appointment_status_text = $('label[for=' + this.form_prefix + '_appointment_detail_columns_stage_id]').text();
            this.week_checked = $('#' + this.form_prefix + '_appointment_book_views_7').prop('checked');

            /** Add Blank Lines **/
            this.addBlankRow($('#' + this.form_prefix + '_appointment_book_views_1').closest('tr'));
            this.addBlankRow($('#' + this.form_prefix + '_appointment_close_only_past').closest('tr'));
            this.addBlankRow($('#' + this.form_prefix + '_location_open_weekends').closest('tr'));
            this.addBlankRow($('#' + this.form_prefix + '_consultation_title_bar_colour').closest('tr'));

            /** Add Classes **/
            $('#' + this.form_prefix + '_appointment_book_views_7').parent('li').addClass('week_view');
            $('#' + this.form_prefix + '_appointment_show_end_times').closest('tr').addClass('show_end_times');

            /** Add Click Handlers **/
            $('#' + this.form_prefix + '_location_open_weekends').click(function () {
                client_config.parameters.toggleLocationOpenWeekends();
            });
            client_config.parameters.toggleLocationOpenWeekends();

            $('#' + this.form_prefix + '_appointment_book_views_7').click(function () {
                client_config.parameters.week_checked = $('#' + client_config.parameters.form_prefix + '_appointment_book_views_7').prop('checked');
            });

            $('#' + this.form_prefix + '_appointment_hide_start_times').click(function () {
                client_config.parameters.toggleAppointmentBookTimes($(this).prop('checked'));
            });
            client_config.parameters.toggleAppointmentBookTimes($('#' + this.form_prefix + '_appointment_hide_start_times').prop('checked'));
        },
        addBlankRow: function (target) {
            $(target).after($('<tr class="small_row"><td colspan="2">&nbsp;</td></tr>'));
        },
        toggleAppointmentBookTimes: function (checked) {
            if (checked) {
                $('.show_end_times').hide();
                $('#' + this.form_prefix + '_appointment_show_end_times').prop('checked', false);
            } else {
                $('.show_end_times').show();
            }
        },
        toggleLocationOpenWeekends: function () {
            if ($('#' + this.form_prefix + '_location_open_weekends').prop('checked')) {
                $('.week_view').show();
                $('#' + this.form_prefix + '_appointment_book_views_7').prop('checked', this.week_checked);
            } else {
                $('.week_view').hide();
                $('#' + this.form_prefix + '_appointment_book_views_7').prop('checked', false);
            }
        }
    }
};
