var reports = {
	client_id: '',
	actions: {
		reports: null,
        pentaho_report: null,
        pentaho_query: null
	},

	initialize: function() {
        reports.pentaho_reports.initialize();
	},

	loadReport: function(report_name, parameters) {
		//TODO: uncomment this console.log(report_name);
		//TODO: uncomment this console.log(parameters);
	},
    pentaho_reports: {



        initialize: function() {
            $('#claim_summary_button').button();
            $('#claim_summary_button').click(function() {
                reports.pentaho_reports.loadReport('CS');
            });

            $('#ra_button').button();
            $('#ra_button').click(function() {
                reports.pentaho_reports.loadReport('RA');
            });

            $('#ra_summary_button').button();
            $('#ra_summary_button').click(function() {
                reports.pentaho_reports.loadReport('RA_S');
            });

            sk.datepicker('#claims_summary_date_end');
            $('#claims_summary_date_end').blur(function(e) { 
                $('#claims_summary_date_start').skDP('setEndDate', e.date);
            });

            sk.datepicker('#claims_summary_date_start');
            $('#claims_summary_date_start').blur(function(e) { 
                $('#claims_summary_date_end').skDP('setStartDate', e.date);
            });

            sk.datepicker('#claims_summary_ra_date');
            $('#claims_summary_ra_date').on('changeDate', function () {
                 var date = new Date(dateText);
                    var month = date.getMonth();

                    $('#claims_summary_ra option').each(function(item) {
                        $(this).removeAttr('hidden');
                    });

                    switch (month) {
                        case 0:
                            $('#claims_summary_ra option').each(function(item) {
                                if (!($(this).html().includes('A'))) {
                                    //$(this).remove();

                                    $(this).attr('hidden', 'hidden');
                                }
                            });
                            break;

                        case 1:
                            $('#claims_summary_ra option').each(function(item) {
                                if (!($(this).html().includes('B'))) {
                                    $(this).attr('hidden', 'hidden');
                                }
                            });
                            break;

                        case 2:
                            $('#claims_summary_ra option').each(function(item) {
                                if (!($(this).html().includes('C'))) {
                                    $(this).attr('hidden', 'hidden');
                                }
                            });
                            break;

                        case 3:
                            $('#claims_summary_ra option').each(function(item) {
                                if (!($(this).html().includes('D'))) {
                                    $(this).attr('hidden', 'hidden');
                                }
                            });
                            break;

                        case 4:
                            $('#claims_summary_ra option').each(function(item) {
                                if (!($(this).html().includes('E'))) {
                                    $(this).attr('hidden', 'hidden');
                                }
                            });
                            break;

                        case 5:
                            $('#claims_summary_ra option').each(function(item) {
                                if (!($(this).html().includes('F'))) {
                                    $(this).attr('hidden', 'hidden');
                                }
                            });
                            break;

                        case 6:
                            $('#claims_summary_ra option').each(function(item) {
                                if (!($(this).html().includes('G'))) {
                                    $(this).attr('hidden', 'hidden');
                                }
                            });
                            break;

                        case 7:
                            $('#claims_summary_ra option').each(function(item) {
                                if (!($(this).html().includes('H'))) {
                                    $(this).attr('hidden', 'hidden');
                                }
                            });

                            break;

                        case 8:
                            $('#claims_summary_ra option').each(function(item) {
                                if (!($(this).html().includes('I'))) {
                                    $(this).attr('hidden', 'hidden');
                                }
                            });

                            break;

                        case 9:
                            $('#claims_summary_ra option').each(function(item) {
                                if (!($(this).html().includes('J'))) {
                                    $(this).attr('hidden', 'hidden');
                                }
                            });

                            break;

                        case 10:
                            $('#claims_summary_ra option').each(function(item) {
                                if (!($(this).html().includes('K'))) {
                                    $(this).attr('hidden', 'hidden');
                                }
                            });

                            break;

                        case 11:
                            $('#claims_summary_ra option').each(function(item) {
                                if (!($(this).html().includes('L'))) {
                                    $(this).attr('hidden', 'hidden');
                                }
                            });

                            break;
                    }

            })

            sk.datepicker('#ra_summary_subtotal_ra_date');

        },
        loadReport: function(type) {

            $('#checkmark').hide();
            $('#error_icon').hide();
            $('#spinner').show();

            $('#messages_text').text('Creating report').show();

            function finishReportMessage() {
                $('#checkmark').show();
                $('#error_icon').hide();
                $('#spinner').hide();

                $('#messages_text').text('Built report, please check your downloads').show();
            }

            if (type == 'RA') {
                var ra_file_id = $('#claims_summary_ra').val();

                var [year, month, day] = $('#claims_summary_ra_date').val().split('-');

                var ra_date = new Date(year, month, day);
                ra_date = ra_date.addMonths(-1);

                var req = new XMLHttpRequest();
                req.open("GET", reports.actions.pentaho_report + '?ra_file_id=' + ra_file_id + '&ra_date=' + ra_date.getTime() + '&type=RA', true);
                req.responseType = "blob";
                req.onreadystatechange = function () {
                    if (req.readyState === 4 && req.status === 200) {

                        if (typeof window.navigator.msSaveBlob === 'function') {
                            window.navigator.msSaveBlob(req.response, "PdfName-" + new Date().getTime() + ".pdf");
                        } else {
                            var blob = req.response;
                            var link = document.createElement('a');
                            link.href = window.URL.createObjectURL(blob);
                            link.download = "PdfName-" + new Date().getTime() + ".pdf";

                            // append the link to the document body

                            document.body.appendChild(link);

                            link.click();

                            finishReportMessage();
                        }
                    }
                };
                req.send();
            } else if (type == 'CS') {
                var claim_summary_s = new Date($('#claims_summary_date_start').val());
                var claim_summary_e = new Date($('#claims_summary_date_end').val());

                var req = new XMLHttpRequest();
                req.open("GET", reports.actions.pentaho_report + '?claim_summary_start=' + claim_summary_s.getTime() + '&claim_summary_end=' + claim_summary_e.getTime() + '&type=CS', true);
                req.responseType = "blob";
                req.onreadystatechange = function () {
                    if (req.readyState === 4 && req.status === 200) {

                        if (typeof window.navigator.msSaveBlob === 'function') {
                            window.navigator.msSaveBlob(req.response, "PdfName-" + new Date().getTime() + ".pdf");
                        } else {
                            var blob = req.response;
                            var link = document.createElement('a');
                            link.href = window.URL.createObjectURL(blob);
                            link.download = "PdfName-" + new Date().getTime() + ".pdf";

                            // append the link to the document body

                            document.body.appendChild(link);

                            link.click();

                            finishReportMessage();
                        }
                    }
                };
                req.send();
            } else {
                var [year, month, day] = $('#ra_summary_subtotal_ra_date').val().split('-');

                var ra_date = new Date(year, month, day);
                ra_date = ra_date.addMonths(-1);

                var req = new XMLHttpRequest();
                req.open("GET", reports.actions.pentaho_report + '?ra_date=' + ra_date.getTime() + '&type=RA_S', true);
                req.responseType = "blob";
                req.onreadystatechange = function () {
                    if (req.readyState === 4 && req.status === 200) {

                        if (typeof window.navigator.msSaveBlob === 'function') {
                            window.navigator.msSaveBlob(req.response, "PdfName-" + new Date().getTime() + ".pdf");
                        } else {
                            var blob = req.response;
                            var link = document.createElement('a');
                            link.href = window.URL.createObjectURL(blob);
                            link.download = "PdfName-" + new Date().getTime() + ".pdf";

                            // append the link to the document body

                            document.body.appendChild(link);

                            link.click();

                            finishReportMessage();
                        }
                    }
                };
                req.send();
            }
        }
    },

	transaction: {
		hash: '',
		report_id: ''
	}
}