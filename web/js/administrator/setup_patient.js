var setup_patient = {
    initialize: function () {
        this.reason_code.initialize();
        this.patient_status.initialize();
        this.patient_type.initialize();
        this.patient_params.initialize();
    },
    patient_params: {
        initialize: function () {
            var tr = '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';

            $('#client_parameters_patient_search_first').closest('tr').before($(tr));
            $('#client_parameters_patient_type_check_new_patient').closest('tr').before($(tr));
        }
    },
    patient_type: {
        data: new Array(),
        form_id: 'patient_type_stage',
        form_prefix: 'patient_type',

        initialize: function () {
            $('#patient_type_table').tablesorter1({
                widgets: ['zebra', 'hover']
            });
            
            $('#setup_patient_type_clear').click(function () {
                $('#' + setup_patient.patient_type.form_id).clearForm();
                setup_patient.patient_type.clearForm();
            });
        },
        addData: function (id, data) {
            this.data[id] = data;
        },
        clearForm: function () {
            $('#patient_type_table tr.current_edit').removeClass('current_edit');
            $('#patient_type .form_error_bottom_box').text('').hide();
            $('#patient_type .error_list').remove();
            $('#patient_type .error').removeClass('error');

            $('#' + this.form_id + ' input[type="radio"]:first').prop('checked', true);
        },
        handleRowClick: function (id) {
            this.clearForm();
            $('#patient_type_table tr#patient_type_' + id).addClass('current_edit');

            var data = this.data[id];
            var prefix = '#' + this.form_prefix + '_';

            $('#' + this.form_id).clearForm();
            for (var key in data) {
                if ($(prefix + key)) {
                    if ($(prefix + key).attr('type') == 'checkbox') {
                        $(prefix + key).prop('checked', data[key]);
                    } else {
                        $(prefix + key).val(data[key]);
                    }
                }
            }
            quickClaim.focusTopField();
        }
    },
    patient_status: {
        data: new Array(),
        form_id: 'patient_status_stage',
        form_prefix: 'patient_status',

        initialize: function () {
            $('#patient_status_table').tablesorter1({
                widgets: ['zebra', 'hover']
            });

            $('#setup_patient_status_clear').click(function () {
                $('#' + setup_patient.patient_status.form_id).clearForm();
                setup_patient.patient_status.clearForm();
            });
        },
        addData: function (id, data) {
            this.data[id] = data;
        },
        clearForm: function () {
            $('#patient_status_table tr.current_edit').removeClass('current_edit');
            $('#patient_status .form_error_bottom_box').text('').hide();
            $('#patient_status .error_list').remove();
            $('#patient_status .error').removeClass('error');
        },
        handleRowClick: function (id) {
            this.clearForm();
            $('#patient_status_table tr#patient_status_' + id).addClass('current_edit');

            var data = this.data[id];
            var prefix = '#' + this.form_prefix + '_';

            $('#' + this.form_id).clearForm();

            for (var key in data) {
                if ($(prefix + key)) {
                    if ($(prefix + key).attr('type') == 'checkbox') {
                        $(prefix + key).prop('checked', data[key]);
                    } else {
                        $(prefix + key).val(data[key]);
                    }
                }
            }
            quickClaim.focusTopField();
        }
    },
    reason_code: {
        data: new Array(),
        form_id: 'reason_codes_stage',
        form_prefix: 'patient_reason',

        initialize: function () {
            $('#reason_codes_table').tablesorter1({
                widgets: ['zebra', 'hover']
            });

            $('#setup_pateint_reason_codes_clear').click(function () {
                $('#' + setup_patient.reason_code.form_id).clearForm();
                setup_patient.reason_code.clearForm();
            });
        },
        addData: function (id, data) {
            this.data[id] = data;
        },
        clearForm: function () {
            $('#reason_codes_table tr.current_edit').removeClass('current_edit');
            $('#reason_codes .form_error_bottom_box').text('').hide();
            $('#reason_codes .error_list').remove();
            $('#reason_codes .error').removeClass('error');
        },
        handleRowClick: function (id) {
            this.clearForm();
            $('#reason_codes_table tr#reason_code_' + id).addClass('current_edit');

            var data = this.data[id];
            var prefix = '#' + this.form_prefix + '_';

            $('#' + this.form_id).clearForm();

            for (var key in data) {
                if ($(prefix + key)) {
                    if ($(prefix + key).attr('type') == 'checkbox') {
                        $(prefix + key).prop('checked', data[key]);
                    } else {
                        
                        $(prefix + key).val(data[key]);
                        if($(prefix + key).is('select')){
                             $(prefix + key).trigger('change');
                        }
                    }
                }
            }
            
            quickClaim.focusTopField();
        }
    }
};