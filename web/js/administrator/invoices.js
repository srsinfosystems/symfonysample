var invoices = {
    ajax_running: false,

    messages: {
        checkmark: 'checkmark',
        error: 'error_icon',
        spinner: 'spinner',
        text: 'messages_text'
    },

    initialize: function () {
        this.payee.initialize();
        this.third_party.initialize();
        this.search.initialize();
        this.unpaid.initialize();
        this.paid.initialize();
    },

    paid: {
        actions: {
            pdf: null
        },

        initialize: function () {
            if ($('#paid_invoices_table tbody tr').size()) {
                $('#paid_invoices_table').tablesorter1({
                    widgets: ['zebra', 'hover']
                });
            }

            $('.pdf_button').button();
            $('.pdf_button').click(function () {
                var id = $(this).attr('id').replace('download_pdf_', '');
                window.location = invoices.paid.actions.pdf + '?id=' + id;
            });
        }
    },
    unpaid: {
        pay_table_initialized: false,
        amount_owing: 0,

        actions: {
            details: null,
            get_claims_list: null,
            pay_invoices: null
        },

        initialize: function () {
            if ($('#unpaid_invoices_table tbody tr').size()) {
                $('#unpaid_invoices_table').tablesorter1({
                    widgets: ['zebra', 'hover']
                });
            }

            $('.mark_paid').button();
            $('.mark_paid').click(function () {
                var id = $(this).attr('id').replace('mark_paid_', '');
                invoices.unpaid.showPayInvoicesDialog(id);
            });

            $('.details').button();
            $('.details').click(function () {
                var id = $(this).attr('id').replace('details_', '');
                window.location = invoices.unpaid.actions.details + '/id/' + id;
            });
            
            sk.datepicker('#pay_invoices_payment_date');

            $('#pay_invoices_dialog').dialog({
                closeOnEscape: true,
                autoOpen: false,
                height: $(window).height() - 100,
                modal: true,
                width: $(window).width() - 100,
                close: function () {
                    $('#pay_invoices_invoice_id').text('');
                    $('#pay_invoices_payment_amt').val('');
                    $('#pay_invoices_payment_date').val('');
                    $('#pay_invoices_table tbody tr').remove();
                    $('#pay_invoices_id').val('');
                },
                open: function () {

                }
            });

            $('#autofill_button').button();
            $('#autofill_button').click(function () {
                $('#pay_invoices_table tr td.amt_received input').val('');
                invoices.unpaid.autofillAmounts();
            });

            $('#clear_button').button();
            $('#clear_button').click(function () {
                $('#pay_invoices_table tr td.amt_received input').val('');
            });

            $('#pay_invoices_button').button();
            $('#pay_invoices_button').click(function () {
                invoices.unpaid.payInvoices();
            });
        },

        clearErrors: function () {
            $('.form-table label.error').remove();
            $('.form-table .error').removeClass('error');
            $('#pay_invoices_message').text('').hide();
        },
        autofillAmounts: function () {
            var amount_paid = parseFloat($('#pay_invoices_payment_amt').val(), 10);
            var amount_owing = this.amount_owing;
            var amount_remaining = amount_paid;

            $('#pay_invoices_table tbody tr').each(function () {
                if (!amount_remaining) {
                    return;
                }
                var row_id = $(this).attr('id');
                var outstanding = parseFloat($('#pay_invoices_' + row_id + '_amount_owing').val(), 10);
                var amt_paid = amount_remaining;

                if (outstanding < amount_remaining) {
                    amt_paid = outstanding;
                }

                if (amt_paid) {
                    $('#pay_invoices_' + row_id + '_amount_paid').val(number_format(amt_paid, 2));
                    amount_remaining = amount_remaining - amt_paid;
                }
            });
        },
        fillPayInvoicesTable: function (data) {
            for (var a in data) {
                row = '<tr id="' + data[a]['claim_id'] + '">'
                        + '<td class="amt_received"><input type="text" id="pay_invoices_' + data[a]['claim_id'] + '_amount_paid" name="pay_invoices[' + data[a]['claim_id'] + '][amount_paid]" value="" style="width:55px;" /></td>'
                        + '<td class="amt_owing"><input type="hidden" id="pay_invoices_' + data[a]['claim_id'] + '_amount_owing" name="pay_invoices[' + data[a]['claim_id'] + '][amount_owing]" value="' + data[a]['amt_owing'].replace('$', '') + '" />' + data[a]['amt_owing'] + '</td>'
                        + '<td class="claim_id">' + data[a]['claim_id'] + '</td>'
                        + '<td class="patient_name">' + data[a]['patient_name'] + '</td>'
                        + '<td class="dob">' + data[a]['dob'] + '</td>'
                        + '<td class="doctor">' + data[a]['doctor'] + '</td>'
                        + '<td class="service_date">' + data[a]['service_date'] + '</td>'
                        + '<td class="service_code">' + data[a]['service_code'] + '</td>'
                        + '</tr>';

                $('#pay_invoices_table tbody').append($(row));
            }

            if (!this.pay_table_initialized && $('#pay_invoices_table tbody tr').size()) {
                $('#pay_invoices_table').tablesorter1({
                    widgets: ['zebra', 'hover']
                });
                this.pay_table_initialized = true;
            } else {
                $('#pay_invoices_table').trigger('applyWidgetId', ['zebra']);
            }
        },
        payInvoices: function () {
            var data = $('#pay_invoices_form').serialize();

            this.clearErrors();

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: invoices.unpaid.actions.pay_invoices,
                data: data,
                success: function (rs) {
                    if (rs.hasOwnProperty('errors')) {
                        quickClaim.showFormErrors(rs.errors, '#pay_invoices_message', '#pay_invoices', true);
                    } else if (rs.hasOwnProperty('ok')) {
                        window.location = invoices.search.actions.third_party_invoices;
                    } else {

                    }
                },
                beforeSend: function () {
                    $('#spinner').show();
                },
                complete: function () {
                    $('#spinner').hide();
                },
                error: function () {
                }
            });
        },
        showPayInvoicesDialog: function (id) {
            data = 'id=' + id;
            amount_owing = $('#invoice_' + id + ' .amt_owing').text();
            invoices.unpaid.amount_owing = 0;

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: invoices.unpaid.actions.get_claims_list,
                data: data,
                success: function (rs) {
                    if (rs.hasOwnProperty('data')) {
                        invoices.unpaid.fillPayInvoicesTable(rs.data);
                    }

                    $('#pay_invoices_invoice_id').text(id);
                    $('#pay_invoices_amount_owing').text(amount_owing);
                    $('#pay_invoices_id').val(id);
                    $('#pay_invoices_dialog').dialog('open');

                    invoices.unpaid.amount_owing = parseFloat(amount_owing.replace('$', '').replace(/,/g, ''), 10);
                },
                beforeSend: function () {
                    $('#spinner').show();
                },
                complete: function () {
                    $('#spinner').hide();
                },
                error: function () {
                }
            });

        }
    },
    search: {
        actions: {
            action_pdf: null,
            claims_search: null,
            details: null,
            edit: null,
            mark_paid: null,
            setup_claim_loop: null,
            third_party_create: null,
            third_party_invoices: null
        },

        claim_items: {},
        form_error_div: 'claims_search_errors',
        form_prefix: 'third_party_claim_search',
        form_id: 'claims_search_form',
        payees: {},
        results_colspan: 0,
        results_div: 'results-div',
        results_row_prefix: 'claim_item',
        results_table: 'results-table',

        initialize: function () {
            $('#find_button').button();
            $('#find_button').click(function () {
                invoices.search.handleFindClick();
            });

            $('#submit_invoices').button();
            $('#submit_invoices').click(function () {
                invoices.search.handleSubmitInvoicesClick();
            });

            sk.datepicker('#' + invoices.search.form_prefix + '_service_date_start');
            $('#' + invoices.search.form_prefix + '_service_date_start').on('changeDate',function (e) {
                $('#' + invoices.search.form_prefix + '_service_date_end').skDP('setStartDate', e.date);
            });
            sk.datepicker('#' + invoices.search.form_prefix + '_service_date_end');
            $('#' + invoices.search.form_prefix + '_service_date_end').on('changeDate',function (e) {
                $('#' + invoices.search.form_prefix + '_service_date_start').skDP('setEndDate', e.date);
            });

            sk.datepicker('#' + invoices.search.form_prefix + '_creation_date_start');
            $('#' + invoices.search.form_prefix + '_creation_date_start').on('changeDate',function (e) {
                $('#' + invoices.search.form_prefix + '_creation_date_end').skDP('setStartDate', e.date);
            });
            
            sk.datepicker('#' + invoices.search.form_prefix + '_creation_date_end');
            $('#' + invoices.search.form_prefix + '_creation_date_end').on('changeDate',function (e) {
                $('#' + invoices.search.form_prefix + '_creation_date_start').skDP('setEndDate', e.date);
            });

            $('#create_invoices_dialog').dialog({
                closeOnEscape: true,
                autoOpen: false,
                height: $(window).height() - 200,
                modal: true,
                width: '80%'
            });

            



        },
        clearErrors: function () {
            $('label.error').remove();
            $('.error').removeClass('error');
            $('#' + invoices.search.form_error_div).text('').hide();
        },
        handleCreateInvoicesClick: function (ids) {
            $('#create_invoices_table tbody tr').remove();
            var distinct = {};

            for (var a in ids)
            {
                var key = this.claim_items[ids[a]].third_party_id;

                if (!distinct.hasOwnProperty(key)) {
                    distinct[key] = {dr: {}, third_party: this.claim_items[ids[a]].third_party, count: 0, claim_ids: new Array()};
                }
                distinct[key].count++;
                distinct[key]['dr'][this.claim_items[ids[a]].doctor] = this.claim_items[ids[a]].doctor;
                distinct[key].claim_ids[distinct[key]['claim_ids'].length] = this.claim_items[ids[a]].claim_id;
            }

            for (var a in distinct) {
                var tr = '<tr>'
                        + '<td>'
                        + '<select id="create_invoices_payee_' + a + '" name="create_invoices[payee][' + a + ']" style="width:170px;">'
                        + '<option value="">[Select A Payee]</option>';

                for (var b in invoices.payee.data) {
                    tr += '<option value="' + invoices.payee.data[b].id + '">' + invoices.payee.data[b].name + ' [' + invoices.payee.data[b].city + ']</option>';
                }

                tr += '</select>'
                        + '<input type="hidden" name="create_invoices[payee_' + a + '_ids]" id="create_invoices_payee_' + a + '_ids" value="';

                var ids = '';
                for (var b in distinct[a]['claim_ids']) {
                    if (ids.length) {
                        ids += ',';
                    }
                    ids += distinct[a]['claim_ids'][b];
                }
                tr += ids + '" />'
                        + '</td>'
                        + '<td>';

                var td = '';
                for (var b in distinct[a]['dr']) {
                    if (td.length) {
                        td += '<br />';
                    }
                    td += b;
                }

                tr += td + '</td>'
                        + '<td>' + distinct[a].third_party + '</td>'
                        + '<td>' + distinct[a].count + '</td>'
                        + '</tr>';

                $('#create_invoices_table tbody').append(tr);

                if ($('#create_invoices_table tbody tr').size()) {
                    $('#create_invoices_table').tablesorter1({
                        widgets: ['zebra', 'hover'],
                        headers: {
                            0: {
                                sorter: false
                            }
                        }
                    });
                }
            }

            $('#create_invoices_dialog').dialog('open');
        },
        handleDetailsActionClick: function (claim_id,rightClick) {
            if(rightClick == 3){ 
                window.open(invoices.search.actions.details + '?id=' + claim_id, target="_blank");
            }else{
                window.location = invoices.search.actions.details + '?id=' + claim_id;
            }
        },
        handleEditClick: function (claim_id, rightClick) {
            if(rightClick == 3){ 
                window.open(invoices.search.actions.edit + '?id=' + claim_id, target="_blank"); 
            } else{
                window.location = invoices.search.actions.edit + '?id=' + claim_id;
            }
        },
        handleFindClick: function () {
            if (invoices.ajax_running) {
                return;
            }
            $("#results-div").html('');

            data = $('#' + invoices.search.form_id).serialize();
            invoices.search.clearErrors();

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: invoices.search.actions.claims_search,
                data: data,
                success: function (rs) {
                    if (rs.hasOwnProperty('errors')) {
                        quickClaim.showFormErrors(rs.errors, '#' + invoices.search.form_error_div, '#' + invoices.search.form_id, true);

                        $('#' + invoices.messages.error).show();
                        $('#' + invoices.messages.text).text('The form contains invalid data.').show();
                    } else if (rs.hasOwnProperty('data')) {
                        invoices.search.fillResultsTable(rs);
                    }
                    global_js.customFooterDataTableWithOptions(invoices.search.results_table);
                },
                beforeSend: function () {
                    invoices.ajax_running = true;
                    $('#messages .icons div').hide();
                    $('#' + invoices.messages.spinner).show();
                    $('#' + invoices.messages.text).text('Searching for claims').show();
                },
                complete: function () {
                    invoices.ajax_running = false;
                    $('#' + invoices.messages.spinner).hide();
                },
                error: function () {
                    $('#' + invoices.messages.error).show();
                    $('#' + invoices.messages.text).text('An error occurred while contacting the server.  Please refresh the page and try again.').show();
                }
            });
        },
        handleMarkPaidClick: function (claim_id) {
            if (invoices.ajax_running) {
                return;
            }

            if ($.isArray(claim_id)) {
                var data = 'claim_ids=' + claim_id.join(',');
                window.location = invoices.search.actions.mark_paid + '?' + data;
            } else {
                window.location = invoices.search.actions.mark_paid + '?id=' + claim_id;
            }
        },
        handlePdfActionClick: function (claim_id) {
            invoices.setupIframe();
            invoices.setupHiddenForm(new Array(), invoices.search.actions.action_pdf + '?id=' + claim_id, '');
        },
        handleSubmitInvoicesClick: function () {
            if (invoices.ajax_running) {
                return;
            }

            var payees = {};
            var size = 0;
            $('#submit_invoices_form select').each(function () {
                if ($(this).val()) {
                    size++;
                    var id = $(this).attr('id').replace('create_invoices_payee_', '');
                    var claim_ids = $('#create_invoices_payee_' + id + '_ids').val();
                    var value = $(this).val();
                    payees[id] = {
                        third_party_id: id,
                        payee_id: value,
                        claim_ids: claim_ids
                    };
                }
            });

            if (size) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: invoices.search.actions.third_party_create,
                    data: payees,
                    success: function (rs) {
                        window.location = invoices.search.actions.third_party_invoices;
                    },
                    beforeSend: function () {
                        invoices.ajax_running = true;
                        $('#messages .icons div').hide();
                        $('#' + invoices.messages.spinner).show();
                        $('#' + invoices.messages.text).text('Creating Invoices').show();
                    },
                    complete: function () {
                        invoices.ajax_running = false;
                        $('#' + invoices.messages.spinner).hide();
                    },
                    error: function () {
                        $('#' + invoices.messages.error).show();
                        $('#' + invoices.messages.text).text('An error occurred while contacting the server.  Please refresh the page and try again.').show();
                    }
                });
            }
        },
        fillResultsTable: function (rs) {

            var can_edit = true;
            var edit_count = 0;
            var mark_paid_count = 0;
            var mark_closed_count = 0;

            var table =
                    '<hr/><form id="' + invoices.search.results_table + '_form" method="POST" action="' + invoices.search.actions.setup_claim_loop + '">'
                    + '<table style="width:100%" class="sk-datatable table-hover table-striped table" id="' + invoices.search.results_table + '"><thead><tr>'
                    + '';
           table += '<th class="select">'
                           + '<label class="CHKcontainer">'
                           + '<input type="checkbox" id="results_check_all" checked="checked" />'
                           +   ' <span class="CHKcheckmark"></span>'
                           +   ' </label>'                            
                           + '</th>';
                   table += '';
                    
                    
            for (var column in rs.header) {
                
                if (column == 'actions') {
                    table += '<th>' + rs.header[column] + '&nbsp;&nbsp;</th>';
                } else {
                    table += '<th>' + rs.header[column] + '</th>';
                }
            }
            table += '</tr></thead><tbody></tbody>';

//            if (rs.totals.hasOwnProperty('subm')) {
//                table += '<tfoot><tr><td>&nbsp;</td>';
//                for (var column in rs.header) {
//                    if (column == 'actions') {
//                        table += '<td>&nbsp;</th>';
//                    } else if (column == 'fee_subm') {
//                        table += '<td>' + rs.totals.subm + '</td>';
//                    } else if (column == 'fee_paid') {
//                        table += '<td>' + rs.totals.paid + '</td>';
//                    } else {
//                        table += '<td>&nbsp;</td>';
//                    }
//                }
//                table += '</tr></tfoot>';
//            }
//            ;

            table += '</table></form>';

            $('#' + invoices.search.results_div).html(table);

            for (var index in rs.data) {
                var row_id = invoices.search.results_row_prefix + '_' + rs.data[index]['id'];
                var claim_item_id = rs.data[index]['id'];
                var claim_id = rs.data[index]['claim_id'];

                invoices.search.claim_items[claim_item_id] = {
                    claim_id: claim_id,
                    doctor: rs.data[index]['doctor'],
                    third_party: rs.data[index]['third_party_name'],
                    third_party_id: rs.data[index]['third_party_id']
                };

                var row = '<tr id="' + row_id + '">';
                       + '<td class="edit_checkbox">'
                       + '<input type="checkbox" id="' + index + '" name="claim[' + index + ']" value="' + rs.data[index]['claim_id'] + '" checked="checked" />'
                       + '</td>';

                       row += '<td class="edit_checkbox">'
                           + '<label class="CHKcontainer">'
                           + '<input type="checkbox" id="' + index + '" name="claim[' + index + ']"  checked="checked"  value="' + rs.data[index]['claim_id'] + '" checked="checked" />'
                           +   ' <span class="CHKcheckmark"></span>'
                           +   ' </label>'                            
                           + '</td>';
                    
               for (var column in rs.header) {
                   var text = rs.data[index][column];

                   if (column == 'actions') {
                       row += '<td><span class="action_edit" style="display:block;">';
                       if (rs.data[index]['actions'].hasOwnProperty('edit')) {
                           row += invoices.search.makeButton('edit', text, 'edit_' + claim_item_id);
                           edit_count++;
                       } else {
                           row += '&nbsp;';
                       }
                       row += '</span>';

                       if (rs.data[index]['actions'].hasOwnProperty('mark_paid')) {
                           mark_paid_count++;
                           row += '<span class="action_mark_paid" style="display:block;">'
                                   + invoices.search.makeButton('mark_paid', text, 'mark_paid_' + claim_item_id);
                           +'</span>';
                       } else {
                           row += '<span class="action_mark_paid">&nbsp;</span>';
                       }

                       if (rs.data[index]['actions'].hasOwnProperty('mark_closed')) {
                           mark_closed_count++;
                           row += '<span class="action_mark_closed" style="display:block;">'
                                   + invoices.search.makeButton('mark_closed', text, 'mark_closed_' + claim_item_id);
                           +'</span>';
                       } else {
                           row += '<span class="action_mark_closed">&nbsp;</span>';
                       }

                       if (rs.data[index]['actions'].hasOwnProperty('details')) {
                           row += '<span class="action_details" style="display:block;">';
                           row += invoices.search.makeButton('details', text, 'details_' + claim_item_id);
                           row += '</span>';
                       } else {
                           row += '<span class="action_details" >&nbsp;</span>';
                       }

                       if (rs.data[index]['actions'].hasOwnProperty('pdf')) {
                           row += '<span class="action_pdf">';
                           row += invoices.search.makeButton('pdf', text, 'pdf_' + claim_item_id);
                           row += '</span>';
                       } else {
                           row += '<span class="action_pdf">&nbsp;</span>';
                       }
                       row += '</td>';
                   } else {
                       row += '<td class="' + column + '">' + text + '</td>';
                   }
               }

               row += '</tr>';
               $(row).appendTo($('#' + invoices.search.results_table + ' tbody'));
                $('tr#' + row_id + ' input[type=button]').button().addClass('ui-button-inline');

                $('#edit_' + claim_item_id).mousedown(function (event) {
                    var rightClick = event.which;
                    invoices.search.handleEditClick(invoices.search.getClaimId($(this).attr('id').replace('edit_', '')),rightClick);
                });
                $('#pdf_' + claim_item_id).click(function () {
                    invoices.search.handlePdfActionClick(invoices.search.getClaimId($(this).attr('id').replace('pdf_', '')));
                });
                $('#details_' + claim_item_id).mousedown(function (event) {
                    var rightClick = event.which;

                    invoices.search.handleDetailsActionClick(invoices.search.getClaimId($(this).attr('id').replace('details_', '')),rightClick);
                });
                $('#mark_paid_' + claim_item_id).mousedown(function () {
                    invoices.search.handleMarkPaidClick(invoices.search.getClaimId($(this).attr('id').replace('mark_paid_', '')));
                });
                $('#mark_closed_' + claim_item_id).mousedown(function () {

                    invoices.search.handleMarkClosedClick(invoices.search.getClaimId($(this).attr('id').replace('mark_closed_', '')));
                });
            }

            var buttons = '<div class="custActionBtns'+invoices.search.results_table+'">';
            if (edit_count) {
                buttons += '<input type="button" id="edit_checkbox_button" name="edit_checkbox_button" value="Edit Selected Claims" />&nbsp;';
            }
            if (mark_paid_count) {
                buttons += '<input type="button" id="mark_paid_checkbox_button" name="mark_paid_checkbox_button" value="Mark Selected Claims As Paid" />&nbsp;';
            }
            if (mark_closed_count) {
                buttons += '<input type="button" id="mark_closed_checkbox_button" name="mark_closed_checkbox_button" value="Mark Selected Claims As Closed" />&nbsp;';
            }

            buttons += '<button class="btn btn-success btn-squared" type="button" id="create_invoices_button" name="create_invoices_button" ><i class="clip-file-plus"></i> Create Invoice(s)</button>';
            buttons += '</div>';
            $(buttons).appendTo($('#' + invoices.search.results_table + '_form'));

            $('#edit_checkbox_button').button();
            $('#mark_paid_checkbox_button').button();
            $('#mark_closed_checkbox_button').button();
            $('#create_invoices_button').button();

            $('#results_check_all').click(function () {
                var checked = $(this).prop('checked');
                $('#' + invoices.search.results_table + '_form input[type=checkbox]').prop('checked', checked);
            });

            $('#edit_checkbox_button').click(function () {
                $('#' + invoices.search.results_table + '_form tbody input:checked').each(function () {
                    var claim_item_id = $(this).closest('tr').attr('id').replace(invoices.search.results_row_prefix + '_', '');
                    if (!$('#edit_' + claim_item_id).size()) {
                        $(this).prop('checked', false);
                    }
                });
                $('#' + invoices.search.results_table + '_form').submit();
            });

            $('#mark_paid_checkbox_button').click(function () {
                var collected_ids = new Array();

                $('#' + invoices.search.results_table + '_form input:checked').each(function () {
                    var claim_item_id = $(this).closest('tr').attr('id').replace(invoices.search.results_row_prefix + '_', '');
                    if ($('#mark_paid_' + claim_item_id).size()) {
                        var claim_id = invoices.search.getClaimId(claim_item_id);
                        collected_ids[collected_ids.length] = claim_id;
                    }
                });

                invoices.search.handleMarkPaidClick(collected_ids);
            });

            $('#mark_closed_checkbox_button').click(function () {
                var collected_ids = new Array();

                $('#' + invoices.search.results_table + '_form input:checked').each(function () {
                    var claim_item_id = $(this).closest('tr').attr('id').replace(invoices.search.results_row_prefix + '_', '');
                    if ($('#mark_closed_' + claim_item_id).size()) {
                        var claim_id = invoices.search.getClaimId(claim_item_id);
                        collected_ids[collected_ids.length] = claim_id;
                    }
                });

                invoices.search.handleMarkClosedClick(collected_ids);
            });

            $('#create_invoices_button').click(function () {
                var collected_ids = new Array();

                $('#' + invoices.search.results_table + '_form input:checked').each(function () {
                    var id = $(this).closest('tr').attr('id');
                    if (id) {
                        var claim_item_id = id.replace(invoices.search.results_row_prefix + '_', '');
                        if (claim_item_id) {
                            collected_ids[collected_ids.length] = claim_item_id;
                        }
                    }
                });
                invoices.search.handleCreateInvoicesClick(collected_ids);
            });

            if ($('#' + invoices.search.results_table + ' tbody tr').size()) {
                $('#' + invoices.search.results_table).tablesorter1({
                    widgets: ['zebra', 'hover'],
                    headers: {
                        0: {
                            sorter: false
                        },
                        14: {
                            sorter: 'sortDate-dmy'
                        }
                    }
                });
            }

        },
        getClaimId: function (claim_item_id) {
            return invoices.search.claim_items[claim_item_id].claim_id;
        },
        makeButton: function (mode, actions, id) {
            var btnClasses='';
            var btnIcon ='';
            if(mode == 'edit'){
                btnClasses = 'btn btn-primary btnBulkAction';
                btnIcon = '<i class="clip-pencil-2"></i>';
            }
            if(mode == 'mark_paid'){
                btnClasses = 'dt-button buttons-excel buttons-html5 btn btn-info btn-squared';
                btnIcon = '<i class="fa fa-folder-open-o"></i>';
            }
            if(mode == 'mark_closed'){
                btnClasses = 'dt-button btn btn-success btn-squared';
                btnIcon = '<i class="fa fa-folder"></i>';
            }
            if(mode == 'details'){
                btnClasses = 'dt-button btn btn-warning btn-squared';
                btnIcon = '<i class="fa fa-eye"></i>';
            }
            if(mode == 'pdf'){
                btnClasses = 'dt-button btn btn-danger btn-squared';
                btnIcon = '<i class="clip-file-pdf"></i>';
            }
            if (actions.hasOwnProperty(mode)) {
                //return '<input type="button" id="' + id + '" value="' + actions[mode] + '" class="' + mode + ' '+btnClasses+'" />';
                return '<button id="' + id + '" class="' + mode + ' '+btnClasses+'">'+btnIcon+' ' + actions[mode] + '</button>'
            }
            return '&nbsp;';
        }

    },
    third_party: {
        button: 'third_party_button',
        data: new Array(),
        prefix: 'third_party',
        form_id: 'third_party_form',
        table_id: 'third_party_table',

        initialize: function () {
            $('#' + this.button).button();
            $('#' + this.button + '_clear').button();

            $('#' + this.button + '_clear').click(function () {
                invoices.third_party.clearForm();
            });

            $('#' + this.table_id).tablesorter1({
                widgets: ['zebra', 'hover']
            });

            $('#' + this.prefix + '_phone').blur(function (e) {
                quickClaim.formatPhone('#' + invoices.third_party.prefix + '_phone');
            });
            $('#' + this.prefix + '_fax').blur(function (e) {
                quickClaim.formatPhone('#' + invoices.third_party.prefix + '_fax');
            });

            $('#' + this.table_id + ' .clickable_row').click(function () {
                var id = $(this).attr('id').replace('third_party_', '');
                invoices.third_party.fillForm(id);
            });

            if ($('input.hl7_partner_id').size()) {
                $('input.hl7_partner_id:last').closest('tr').after($('<tr class="small_row"><td>&nbsp;</td></tr>'));
            }
        },
        addData: function (id, data) {
            this.data[id] = data;
        },
        clearForm: function () {
            $('#' + this.table_id + ' tr.current_edit').removeClass('current_edit');
            $('#' + this.form_id + ' .form_error_bottom_box').text('').hide();
            $('#' + this.form_id + ' .error_list').remove();
            $('#' + this.form_id + ' .error').removeClass('error');

            $('#' + this.form_id).clearForm();
        },
        fillForm: function (id) {
            this.clearForm();
            $('#' + this.table_id + ' tr#third_party_' + id).addClass('current_edit');

            var data = this.data[id];
            var prefix = '#' + this.prefix + '_';
            for (var key in data) {
                if ($(prefix + key)) {
                    if ($(prefix + key).attr('type') == 'checkbox') {
                        $(prefix + key).prop('checked', data[key]);
                    } else {
                        if(key == 'province')
                        {
                        $(prefix + key).val(data[key]).trigger('change.select2'); 
                        }
                        else {
                         $(prefix + key).val(data[key]);     
                        }
                    }
                }
            }

            quickClaim.focusTopField();
        }
    },
    payee: {
        button: 'payee_button',
        prefix: 'payee',
        form_id: 'payee_form',
        table_id: 'payees_table',
        data: {},

        initialize: function () {
            $('#' + this.button).button();
            $('#' + this.button + '_clear').button();

            $('#' + this.button + '_clear').click(function () {
                invoices.payee.clearForm();
            });

            $('#' + this.table_id).tablesorter1({
                widgets: ['zebra', 'hover']
            });

            //reformat textbox to postal code looks when lost focus
            $('#' + this.prefix + '_postal_code').blur(function (e) {
                quickClaim.formatPostal($(this));
            });

            $('#' + this.table_id + ' .clickable_row').click(function () {
                var id = $(this).attr('id').replace('payee_', '');
                invoices.payee.fillForm(id);
            });

        },

        addData: function (id, data) {
            this.data[id] = data;
        },

        clearForm: function () {
            $('#' + this.table_id + ' tr.current_edit').removeClass('current_edit');
            $('#' + this.form_id + ' .form_error_bottom_box').text('').hide();
            $('#' + this.form_id + ' .error_list').remove();
            $('#' + this.form_id + ' .error').removeClass('error');

            $('#' + this.form_id).clearForm();
        },
        fillForm: function (id) {
            this.clearForm();
            $('#' + this.table_id + ' tr#payee_' + id).addClass('current_edit');


            var data = this.data[id];
            var prefix = '#' + this.prefix + '_';
            for (var key in data) {
                if ($(prefix + key)) {
                    if ($(prefix + key).attr('type') == 'checkbox') {
                        $(prefix + key).prop('checked', data[key]);
                    } else {
                        //alert(key);
                        if(key == 'province')
                        {
                        $(prefix + key).val(data[key]).trigger('change.select2'); 
                        }
                        else {
                         $(prefix + key).val(data[key]);     
                        }
                    }
                }
            }

            quickClaim.focusTopField();
        }
    },

    setupIframe: function () {
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    setupHiddenForm: function (fields, action, prefix) {
        setTimeout(function () {
            var frame_body = $('#hiddenDownloader').contents().find('body');
            var form = '<form id="hidden_form" action="' + action + '" method="POST">';

            for (var a in fields) {
                var name = fields[a].substring(0, fields[a].indexOf('='));
                var value = fields[a].substring(fields[a].indexOf('=') + 1);

                name = name.replace(past_claims.form_prefix, prefix);
                form += '<input type="text" value="' + value + '" id="' + name + '" name="' + name + '" />';
            }
            form += '</form>';
            frame_body.append($(form));
            $('#hiddenDownloader').contents().find('#hidden_form').submit();
        });
    }
};