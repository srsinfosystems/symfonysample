var reminders = {
    ajax_lock: false,

    appointment: {
        actions: {
            save: ''
        },
        rts: {},
        form_id: '',
        prefix: '',
        submit_button: '',
        tinyMCE: {
            initialized: false,
            editor_id: ''
        },

        initialize: function () {
            $('#' + this.prefix + '_days_before').closest('tr').addClass('days_before_row');
            $('#' + this.prefix + '_email_addresses').closest('tr').addClass('email_addresses');

            $('input[name="' + this.prefix + '[recipients][]"]:first').closest('td').addClass('recipients');
            $('input[name="' + this.prefix + '[event_trigger]"]:first').closest('ul').removeClass('radio_list');

            reminders.appointment.toggleMessageRecipient($('input[name="' + this.prefix + '[event_trigger]"]:checked').val());
            reminders.appointment.toggleEmailAddress();

            if ($('#appointment_reminders_table tbody tr').size()) {
                $('#appointment_reminders_table').tablesorter1({
                    widgets: ['zebra', 'hover']
                });
            }

            $('input[name="' + this.prefix + '[event_trigger]"]').click(function () {
                reminders.appointment.toggleMessageRecipient($(this).val());
            });
            $('#' + this.prefix + '_recipients_other').click(function () {
                reminders.appointment.toggleEmailAddress();
            });

            $('#appointment_reminders_table tbody tr').click(function () {
                reminders.appointment.fillForm($(this).attr('id'));
            });

            $('#appt_message_button_clear').click(function () {
                reminders.appointment.clearForm();
            });

            $('#' + this.submit_button).button();
            $('#' + this.submit_button).click(function () {
                reminders.appointment.handleSubmitClick();
            });

            $('#new_appointment_reminder').button();
            $('#new_appointment_reminder').click(function () {
                reminders.appointment.fillForm();
            });

            $('#printReminderTable').button();
            $('#printReminderTable').click(function () {
                $('#appointment_reminders_table').printElement({
                    overrideElementCSS: ['css/jquery-tablesorter1.css', 'css/jquery-tablesorter/jquery.tablesorter.pager.css', 'css/jquery-tablesorter/theme.blue.css']
                });
            });


            $('#appointment_reminder_dialog').dialog({
                closeOnEscape: true,
                autoOpen: false,
                height: $(window).height() - 200,
                modal: true,
                width: '80%'
            });

            quickClaim.setupToggleAll('appointment_types');
        },

        clearForm: function () {
            $('#' + this.form_id + ' .form_error_bottom_box').text('').hide();
            $('#' + this.form_id + ' .error_list').remove();
            $('#' + this.form_id + ' .error').removeClass('error');
            $('#' + this.form_id).clearForm();
            $('#' + this.prefix + '_message_text').val('');

            this.toggleMessageRecipient($('input[name="' + this.prefix + '[event_trigger]"]:checked').val());
            this.toggleEmailAddress();
        },
        fillForm: function (id) {
            this.clearForm();
            if (this.rts.hasOwnProperty(id)) {
                var data = this.rts[id];
                var prefix = '#' + this.prefix + '_';

                for (var key in data) {
                    if ($(prefix + key)) {
                        if (key == 'event_trigger') {
                            $(prefix + key + '_' + data[key]).prop('checked', true);
                        } else if (key == 'recipients') {
                            for (var a in data[key]) {
                                var field = prefix + key + '_' + data[key][a];
                                field = field.replace('@', 'AT');

                                if ($(field).size()) {
                                    $(field).prop('checked', true);
                                } else {
                                    var emails = $(prefix + 'email_addresses').val() + '';
                                    if (emails.length) {
                                        emails += ', ';
                                    }
                                    emails += data[key][a];
                                    $(prefix + 'email_addresses').val(emails);
                                }
                            }
                        } else if (key == 'appt_types') {
                            for (var a in data[key]) {
                                $(prefix + key + '_' + data[key][a]).prop('checked', true);
                            }
                        } else if ($(prefix + key).attr('type') == 'checkbox') {
                            $(prefix + key).prop('checked', data[key]);
                        } else {
                            $(prefix + key).val(data[key]);
                        }
                    }
                }
            }

            this.toggleMessageRecipient($('input[name="' + this.prefix + '[event_trigger]"]:checked').val());
            this.toggleEmailAddress();

            $('#appointment_reminder_dialog').dialog('open');
            if (!this.tinyMCE.initialized) {
                $('#' + this.tinyMCE.editor_id).tinymce({
                    script_url: '/js/tiny_mce/tiny_mce.js',
                    mode: "exact",
                    elements: reminders.appointment.tinyMCE.editor_id,
                    theme: "advanced",
                    theme_advanced_toolbar_location: "top",
                    theme_advanced_toolbar_align: "left",
                    theme_advanced_statusbar_location: "bottom",
                    theme_advanced_resizing: true,
                    theme_advanced_buttons1: "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,link,unlink,image,|,forecolor,backcolor,|,formatselect,fontselect,fontsizeselect"
                });
                this.tinyMCE.initialized = true;
            }

            quickClaim.focusTopField('appointment_reminder_dialog');
        },
        toggleEmailAddress: function () {
            var checked = $('#' + this.prefix + '_recipients_other').prop('checked');

            if (checked) {
                $('.email_addresses').show();
            } else {
                $('.email_addresses').hide();
            }
        },
        toggleMessageRecipient: function (event_trigger) {

            if (event_trigger == '5') { // billed
                $('#' + this.prefix + '_recipients_patient').closest('li').hide();
                $('#' + this.prefix + '_recipients_patient').prop('checked', false);
            } else {
                $('.recipients li').show();
            }

            if (event_trigger == '6') { // pre-appt reminder
                $('.days_before_row').show();
            } else {
                $('.days_before_row').hide();
                $('#' + this.prefix + '_days_before').val('');
            }

        },
        handleSubmitClick: function () {
            if (reminders.ajax_lock) {
                return;
            }

            var data = $('#' + this.form_id).serialize();

            data += '&reminder_template[message_text]=' + escape(tinyMCE.get(this.prefix + '_message_text').getContent());

            $('label.error').remove();
            $('.error').removeClass('error');
            $('#' + this.form_id + ' .form_error_bottom_box').text('').hide();

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: reminders.appointment.actions.save,
                data: data,
                success: function (rs) {
                    if (rs.hasOwnProperty('errors')) {

                        quickClaim.showFormErrors(rs.errors, '#appt_reminder_error', '#' + reminders.appointment.prefix, true);
                        quickClaim.focusTopField('appointment_reminder_dialog');

                        $('#appt_reminder_error_icon').show();
                        $('#appt_reminder_messages_text').html('<div class="alert alert-info">The form contains invalid data.</div>').show();
                    } else if (rs.hasOwnProperty('data')) {
                        for (var a in rs.data) {
                            reminders.appointment.rts[rs.data[a]['id']] = rs.data[a];
                            console.log(rs.data[a]);
                            reminders.appointment.updateDataRow(rs.data[a]);
                        }

                        $('#appt_reminder_checkmark').show();
                        $('#appt_reminder_messages_text').text('Successfully saved Appointment Reminder.');

                        $('#appointment_reminder_dialog').dialog('close');
                    }
                },
                beforeSend: function () {
                    reminders.ajax_lock = true;
                    $('#appt_reminder_checkmark').hide();
                    $('#appt_reminder_error_icon').hide();
                    $('#appt_reminder_spinner').show();

                    $('#appt_reminder_messages_text').text('Saving Appointment Reminder').show();
                },
                complete: function () {
                    reminders.ajax_lock = false;
                    $('#appt_reminder_spinner').hide();
                },
                error: function () {
                    $('#appt_reminder_error_icon').show();
                    $('#appt_reminder_messages_text').text('An error occurred while contacting the server.');
                }
            });
        },
        updateAppointmentTypes: function (row_id) {
            var data = $('#' + row_id + ' .appointment_types').text();
            data = data.split(';');
            var text = "";

            for (var a in data) {
                if ($('#' + this.prefix + '_appt_types_' + data[a]).size()) {
                    if (text.length) {
                        text += ', ';
                    }

                    text += $('label[for="' + this.prefix + '_appt_types_' + data[a] + '"]').html();
                }
            }

            $('#' + row_id + ' .appointment_types').text(text);

        },
        updateDataRow: function (data) {
            var row = '';
            $("#appointment_reminders_table").dataTable().fnDestroy();
            $("#appointment_reminders_table thead tr:eq(1)").remove();
            if ($('#appointment_reminders_table tbody tr#' + data['id']).size()) {
                row = $('#appointment_reminders_table tbody tr#' + data['id']);
            } else {
                row = $('#appointment_reminders_table tbody tr:first').clone();
                $(row).attr('id', data['id']);
                $(row).appendTo($('#appointment_reminders_table tbody'));
            }

            $($('td', row)).each(function () {
                var cls = $(this).attr('class');

                if (cls == 'event_trigger') {
                    var text = $('label[for="' + reminders.appointment.prefix + '_event_trigger_' + data[cls] + '"]').html();
                    $(this).text(text);
                } else if (cls == 'appointment_types') {
                    $(this).text(data['appointment_types']);
                    reminders.appointment.updateAppointmentTypes(data['id']);
                } else if (cls == 'message') {
                    $(this).html(data['message_text']);
                } else if (cls == 'active') {
                    $(this).text(data[cls] ? 'Y' : 'N');
                } else {
                    $(this).text(data[cls]);
                }
            });
            sk.table.init('appointment_reminders_table');
            sk.table.loader('appointment_reminders_table', 'stop');
        }
    }
}
