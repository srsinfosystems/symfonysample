var VPLUpload = {
    actions: {
        upload: null
    },
    messageID: 'VPLUploadMessages',
    parent: '',

    initialize: function () {
        $('#VPLFileUploadButton')
            .button()
            .click(function () {
                VPLUpload.submitForm();
            });

        $('#VPLFileUploadCloseDialogButton')
            .button()
            .click(function () {
                $('#VPLFileUploadDialog').dialog('close');
            });

        $('#VPLFileUploadDialog').dialog({
            closeOnEscape: true,
            autoOpen: false,
            height: 400,
            modal: true,
            width: 800
        });
    },

    showDialog: function () {
        $('#VPLFileUploadDialog').dialog('open');
    },
    submitForm: function () {
        if (quickClaim.ajaxLocked)
            return;

        var formData = new FormData($("#VPLFileUploadForm")[0]);
        formData.append('parent', VPLUpload.parent);

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: VPLUpload.actions.upload,
            data: formData,

            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function (rs) {
                if (rs.hasOwnProperty('success')) {

                    $('#VPLFileUploadDialog').dialog('close');
                    if (VPLUpload.parent == 'import') {
                        quickClaim.hideAjaxMessage(dataImport.messageID);
                    }
                    quickClaim.hideAjaxMessage(VPLUpload.messageID);
                }
                else if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#VPLFileUploadErrorDiv', '#vpl_upload', true);

                    var errorMessage = 'The form contains invalid data';
                    quickClaim.showAjaxMessage(VPLUpload.messageID, 'error', errorMessage);
                    if (VPLUpload.parent == 'import') {
                        quickClaim.showAjaxMessage(dataImport.messageID, 'error', errorMessage);
                    }

                }

                if (rs.hasOwnProperty('importData') && VPLUpload.parent == 'import') {
                    $('#tabs').tabs('option', 'active', 0);
                    for (var a in rs['importData']) {
                        dataImport.loadTableRow(rs['importData'][a]);
                    }
                }
            },
            beforeSend: function () {
                quickClaim.ajaxLocked = true;
                var message = 'Uploading .VPL File';

                quickClaim.showAjaxMessage(VPLUpload.messageID, 'spin', message);
                if (VPLUpload.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'spin', message);
                }
                quickClaim.showOverlayMessage(message);
            },
            complete: function () {
                quickClaim.ajaxLocked = false;
                quickClaim.hideOverlayMessage();
            },
            error: function () {
                quickClaim.ajaxLocked = false;
                var message = 'An error occurred while contacting the server. Please refresh the page and try again.';

                quickClaim.hideOverlayMessage();
                quickClaim.showAjaxMessage(VPLUpload.messageID, 'error', message);
                if (VPLUpload.parent == 'import') {
                    quickClaim.showAjaxMessage(dataImport.messageID, 'error', message);
                }
            }
        });
    }
};
