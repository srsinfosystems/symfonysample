(function($) {
	$.extend($.ui.tabs.prototype, {
		tabSwitching: function(options) {
			var self = this;

			function init() {
				$(window).bind('keyup', handleKeyUp);
			}

			function handleKeyUp(event) {
				if (event.altKey) {
					var val = parseInt(String.fromCharCode(event.which), 10);

					if ($('.ui-tabs').size() > 1) {
						var currentTabID = $(self.element).prop('id');
						var isInDialogBox = $('#' + currentTabID).closest('.ui-dialog-content').size() ? true : false;
						
						if (isInDialogBox) {
							var dialogBoxID = $('#' + currentTabID).closest('.ui-dialog-content').prop('id');
							
							if (!$('#' + dialogBoxID).dialog('isOpen')) {
								return false;
							}
						}
						else if ($('.ui-dialog').is(':visible')) {
							return false;
						}
					}
					
					if (val) {
						val--;
						$(self.element).tabs('option', 'active', val);
					}
				}
			}

			init();
		}
	});
})(jQuery);