var providers_edit = {
    fname: 'doctor_name_fname',
    lname: 'doctor_name_lname',
    doc_code: 'doctor_name_qc_doctor_code',
    id: 'doctor_name_id',

    phones: ['doctor_address_phone', 'doctor_address_cell', 'doctor_address_fax', 'doctor_address_alternate_phone'],
    postal: 'doctor_address_postal_code',

    initialize: function () {
        quickClaim.setupSelectAllCheckbox('regions', 'regions_regions', 'regions');
        quickClaim.setupSelectAllCheckbox('locations', 'locations_locations', 'locations');

        $('#' + this.fname).blur(function () {
            providers_edit.formatDoctorCode();
        });

        $('#' + this.lname).blur(function () {
            providers_edit.formatDoctorCode();
        });

        for (var a in this.phones) {
            $('#' + this.phones[a]).blur(function () {
                quickClaim.formatPhone('#' + $(this).attr('id'));
            });
        }

        $('#' + this.postal).blur(function () {
            quickClaim.formatPostal('#' + $(this).attr('id'));
        });

        if ($('input.hl7_partner_id').size()) {
            $('input.hl7_partner_id:last').closest('tr').after($('<tr class="small_row"><td colspan="2">&nbsp;</td></tr>'));
        }

        $('#doctor_moh_spec_code_id').change(function () {
            if ($(this).val() == '01') {
                $('#doctor_moh_bill_as_ana').prop('checked', true);
            }
        });

        $('#doctor_moh_mcedt_username').closest('tr').addClass('mcedit');
        $('#doctor_moh_mcedt_password').closest('tr').addClass('mcedit');
        $('#doctor_moh_uses_mcedt').click(function () {
            providers_edit.checkEDT();
        });
        providers_edit.checkEDT();

    },
    checkEDT: function () {
        var uses_mcedt = $('#doctor_moh_uses_mcedt').prop('checked');

        if (uses_mcedt) {
            $('.mcedit').show();
        } else {
            $('.mcedit').hide();
        }
    },
    formatDoctorCode: function () {
        if (!$('#' + this.id).val()) {
            var fname = $('#' + this.fname).val();
            var lname = $('#' + this.lname).val();

            if (fname && lname) {
                code = fname.charAt(0) + lname.charAt(0)
                $('#' + this.doc_code).val(code.toUpperCase());
            }
        }
    },
    facility: {
        actions: {
            lookup: null
        },
        cache: [],
        data: {},
        ajaxLock: false,

        initialize: function () {
            for (var a in providers_edit.facility.data) {
                providers_edit.facility.cache[providers_edit.facility.cache.length] = providers_edit.facility.data[a].toString;
            }

            var $description = $('#facility_description');

            $description.autocomplete({
                minLength: 1,
                select: function (event, ui) {
                    var facilityNum = ui.item.label.substring(0, 4);

                    providers_edit.facility.fillInFacility(facilityNum);

                    event.stopPropagation();
                },
                source: providers_edit.facility.cache
            });

            $description.blur(function () {
                var facilityNum = $(this).val().substring(0, 4);
                providers_edit.facility.fillInFacility(facilityNum);
            });
            
        },

        fillInFacility: function (facilityNum) {
            if (providers_edit.facility.data.hasOwnProperty(facilityNum)) {
                var d = providers_edit.facility.data[facilityNum];

                $('#doctor_moh_facility_num').val(d.facility_num);
                $('#facility_description').val(d.toString);
            } else {
                providers_edit.facility.lookup(facilityNum);
            }
        },

        lookup: function (facilityNum) {
            if (providers_edit.facility.ajaxLock) {
                return false;
            }

            var data = 'facilityNum=' + facilityNum;

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: providers_edit.facility.actions.lookup,
                data: data,
                success: function (rs) {
                    if (rs.hasOwnProperty('data') && rs.data.hasOwnProperty('0')) {
                        providers_edit.facility.data[rs.data[0].facility_num] = rs.data[0];
                        providers_edit.facility.fillInFacility(rs.data[0].facility_num);
                    } else {
                        $('#doctor_moh_facility_num').val($('#facility_description').val());
                    }
                },
                beforeSend: function (rs) {
                    providers_edit.facility.ajaxLock = true;
                },
                complete: function (rs) {
                    providers_edit.facility.ajaxLock = false;
                },
                error: function (rs) {
                }
            });
        }
    },

};
