var review = {
    actions: {
        claim_edit: null,
        mark_reviewed: null,
    },

    initialize: function () {
        $('#search').button();
        sk.datepicker('#appointment_review_date')

        $('.appt_button').button();
        $('.appt_button').click(function () {
            var id = review.getAppointmentData($(this).attr('name'));
            review.handleApptButtonClick(id[1], id[0])
        });

        $('.claim_button').button();
        $('.claim_button').click(function () {
            var name = review.getItemId($(this).attr('name'));
            window.location = review.actions.claim_edit + '?id=' + name[1];
        });

        $('.mark_reviewed').button();
        $('.mark_reviewed').click(function () {
            var patient_id = $(this).attr('id').replace('mark_reviewed_', '');
            review.handleMarkReviewedClick(patient_id);
        });
    },

    handleApptButtonClick: function (patient_id, appt_id) {
        $('#appointment_book_patient_id').val(patient_id);
        $('#appointment_book_appointment_id').val(appt_id);
        $('#appointment_book_mode').val('edit');
        $('#appointment_book_form').submit();
    },
    handleMarkReviewedClick: function (patient_id) {
        var item_ids = new Array();
        var appt_ids = new Array();

        $('#appointment_list_' + patient_id + ' .claim_button').each(function () {
            var name = review.getItemId($(this).attr('name'));
            item_ids[item_ids.length] = name[0];
        });

        $('#appointment_list_' + patient_id + ' .appt_button').each(function () {
            var data = review.getAppointmentData($(this).attr('name'));
            appt_ids[appt_ids.length] = data[0];
        });

        var data = 'item_ids=' + item_ids.join(',')
                + '&appt_ids=' + appt_ids.join(',')
                + '&patient_id=' + patient_id;

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: review.actions.mark_reviewed,
            data: data,
            success: function (rs) {
                if (rs.status == 'ok') {
                    $('#appointment_list_' + patient_id).effect('blind', {}, 500);
                }
            },
            beforeSend: function () {
                $('#appointment_list_' + patient_id + ' .spinner').show();
            },
            complete: function () {
                $('#appointment_list_' + patient_id + ' .spinner').hide();
            },
            error: function () {
            }
        });
    },
    getAppointmentData: function (name) {
        return name.replace('go_to_appointment_', '').split('_');
    },
    getItemId: function (name) {
        return name.replace('go_to_claim_', '').split('_');
    }

};