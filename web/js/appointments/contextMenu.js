var contextMenu = {
    options: {
        profile: null,
        claim: null,
        direct_dob: null,
        direct_address: null,
        direct_nameLabel: null,
        direct_chart: null,
        sep2: null,
        visit_sheet: null
    },
    params: {
        label: null,
        printer_name: 'Default Printer',
        short_printer_name: 'Default Printer',
        claim: false,
        reverse: false,
        cancel: false,
        reschedule: false,
        getlabels: null,
        patient_info: null
    },

    PrintLabel: function (s,noCopy = null) {
        var patient_id = $('#patient_data_row .patient_id').html();
        var dataURL = 'patient_id=' + patient_id;

        $.ajax({
            type: 'GET',
            data: dataURL,
            url: contextMenu.params.patient_info,
            success: function (rs) {

                if (rs.hasOwnProperty('error')) {
                    alert(rs['error']);
                    return;
                }

                var patientObj = JSON.parse(rs);

                var patientid = patientObj.id;
                var patientaddr = patientObj.address;
                var patienthomephone = patientObj.hphone;
                var patientcellphone = patientObj.cellphone;
                var patientdob = patientObj.dob;
                var patientgender = patientObj.sex;
                var patientname = patientObj.lname;
                var patientfname = patientObj.fname;

                var patientlfname = patientname + ', ' + patientfname;

                var patientchart = patientObj.patient_number;
                var patientcity = patientObj.city;
                var patientpostalcode = patientObj.postal_code;
                var patientprovince = patientObj.province;
                var patienthcn = patientObj.health_num;
                var patientversion = patientObj.hcn_version_code;

                var patientophone = patientObj.ophone;
                var patientwphone = patientObj.wphone;

                if (patienthomephone) {
                    patienthomephone = patienthomephone.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
                }

                if (patientcellphone) {
                    patientcellphone = patientcellphone.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
                }

                if (patientophone) {
                    patientophone = patientophone.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
                }

                if (patientwphone) {
                    patientwphone = patientwphone.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
                }

                var patient = {
                    id: patientid,
                    name: patientname,
                    fname: patientfname,
                    lfname: patientlfname,
                    addr: patientaddr,
                    hphone: patienthomephone,
                    cphone: patientcellphone,
                    ophone: patientophone,
                    wphone: patientwphone,
                    dob: patientdob,
                    gender: patientgender,
                    chart: patientchart,
                    city: patientcity,
                    post: patientpostalcode,
                    prov: patientprovince,
                    hcn: patienthcn,
                    vers: patientversion

                };

                var labelXML;
                var dataURL = 'type=' + s;

                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: contextMenu.params.getlabels,
                    data: dataURL,
                    success: function (rs) {
                        if (rs.hasOwnProperty('error')) {
                            alert(rs['error']);
                            return;
                        }
                       

                        labelXML = rs;

                        if (labelXML) {
                            var label = dymo.label.framework.openLabelXml(labelXML);

                            if (patient) {

                                var label_objects = label.getObjectNames();

                                for (obj in label_objects) {
                                    if (contextMenu.contains(label.getObjectNames(), label_objects[obj])) {
                                        var ele = label_objects[obj];
                                        var new_text = patient[ele.toLowerCase()];
                                        if (new_text) {
                                            label.setObjectText(label_objects[obj], new_text);
                                        }
                                    }


                                }
                            }
                            // select printer to print on
                            // for simplicity sake just use the first LabelWriter printer
                            var printers = dymo.label.framework.getPrinters();
                            if (printers.length == 0)
                                throw "No DYMO printers are installed. Install DYMO printers.";

                            var printerName = "";
                            for (var i = 0; i < printers.length; ++i) {
                                var printer = printers[i];
                                if (printer.printerType == "LabelWriterPrinter") {
                                    printerName = printer.name;
                                    break;
                                }
                            }

                            if (printerName == "")
                                throw "No LabelWriter printers found. Install LabelWriter printer";

                            // finally print the label
                            var i;
                            for(i = 0; i < noCopy; i++)
                            {
                                label.print(printerName);
                            }
                        }
                    },
                    error: function (rs) {
                        alert('An error has occured: ' + rs);
                    }
                });
            }
        });
    },
    contains: function (a, obj) {
        for (var i = 0; i < a.length; i++) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    },
    initialize: function () {
        $('#doctors_appt_menu').button({
            icons: {
                primary: "ui-icon-triangle-1-s"
            },
            text: false
        });

        $('#doctors_appt_menu').click(function(){
            $('.doctors_appt_menu').css('overflow','');
        });

        if (contextMenu.params.printer_name.length > 15) {
            contextMenu.params.short_printer_name = contextMenu.params.printer_name.substring(0, 15).trim() + '...';
        } else {
            contextMenu.params.short_printer_name = contextMenu.params.printer_name;
        }

        var parameters = {
            selector: '#doctors_appt_menu',
            trigger: 'left',
            autoHide: false,
            className: 'doctors_appt_menu',
            callback: function (key, options, event) {
            },
            items: {
                cancel: {
                    name: 'Cancel Appointment',
                    icon: 'cancel',
                    disabled: function () {
                        return !contextMenu.params.cancel;
                    },
                    callback: function (key, options, event) {
                        $('#doctor_cancel_button').click();
                    }
                },
                reschedule: {
                    name: 'Reschedule Appointment',
                    icon: 'reschedule',
                    disabled: function () {
                        return !contextMenu.params.reschedule;
                    },
                    callback: function (key, options, event) {
                        $('#doctor_reschedule_button').click();
                    }
                },
                sep1: '----',
                reverse: {
                    name: 'Reverse Appointment Stage',
                    icon: 'reverse',
                    disabled: function () {
                        return !contextMenu.params.reverse;
                    },
                    callback: function (key, options, event) {
                        calendar_events.doctors_appt.reverseAppointmentStatus();
                    }
                },
                claim: {
                    name: 'Create A Claim',
                    icon: 'claim',
                    disabled: function () {
                        return !contextMenu.params.claim;
                    },
                    callback: function (key, options, event) {
                        contextMenu.goToClaim(quickClaim.isRightClick(event));
                    }
                },
                profile: {
                    name: 'Profile',
                    icon: 'profile',
                    callback: function (key, options, event) {
                        contextMenu.goToProfile(quickClaim.isRightClick(event));
                    },
                    disabled: function () {
                        return !Boolean($('#doctor_appointment_form #appointment_patient_id').val());
                    }
                },
                visit_sheet: {
                    name: 'Visit Sheet [PDF]',
                    disabled: function () {
                        return !Boolean($('#doctor_appointment_form #appointment_patient_id').val());
                    },
                    callback: function (key, options, event) {
                        contextMenu.getVisitSheet();
                    }
                },
                sep2: '----',
                direct_dob: {
                    name: 'DOB Label [' + contextMenu.params.short_printer_name + ']',
                    disabled: function () {
                        return !Boolean($('#doctor_appointment_form #appointment_patient_id').val());
                    },
                    callback: function (key, options, event) {
                        contextMenu.getLabel(true, 'dob');
                    }
                },
                direct_address: {
                    name: 'Address Label [' + contextMenu.params.short_printer_name + ']',
                    disabled: function () {
                        return !Boolean($('#doctor_appointment_form #appointment_patient_id').val());
                    },
                    callback: function (key, options, event) {
                        contextMenu.getLabel(true, 'address');
                    }
                },
                direct_nameLabel: {
                    name: 'Name Label [' + contextMenu.params.short_printer_name + ']',
                    disabled: function () {
                        return !Boolean($('#doctor_appointment_form #appointment_patient_id').val());
                    },
                    callback: function (key, options, event) {
                        contextMenu.getLabel(true, 'name');
                    }
                },
                direct_chart: {
                    'name': 'Chart Label [' + contextMenu.params.short_printer_name + ']',
                    disabled: function () {
                        return !Boolean($('#doctor_appointment_form #appointment_patient_id').val());
                    },
                    callback: function (key, options, event) {
                        contextMenu.getLabel(true, 'chart');
                    }
                },
                sep3: '----',
                fold1: {
                    "name": "Labels [PDF]",
                    "items": {

                        dob: {
                            name: 'DOB Label [PDF]',
                            disabled: function () {
                                return !Boolean($('#doctor_appointment_form #appointment_patient_id').val());
                            },
                            callback: function (key, options, event) {
                                contextMenu.getLabel(false, 'dob');
                            }
                        },
                        address: {
                            name: 'Address Label [PDF]',
                            disabled: function () {
                                return !Boolean($('#doctor_appointment_form #appointment_patient_id').val());
                            },
                            callback: function (key, options, event) {
                                contextMenu.getLabel(false, 'address');
                            }
                        },
                        nameLabel: {
                            name: 'Name Label [PDF]',
                            disabled: function () {
                                return !Boolean($('#doctor_appointment_form #appointment_patient_id').val());
                            },
                            callback: function (key, options, event) {
                                contextMenu.getLabel(false, 'name');
                            }
                        },
                        chart: {
                            name: 'Chart Label [PDF]',
                            disabled: function () {
                                return !Boolean($('#doctor_appointment_form #appointment_patient_id').val());
                            },
                            callback: function (key, options, event) {
                                contextMenu.getLabel(false, 'chart');
                            }
                        }
                    }
                },
                sep4: '----',
                fold2: {
                    "name": "Labels [DYMO]",
                    "items": {

                        dymodob: {
                            name: 'DOB Label [DYMO]',
                            disabled: function () {
                                return !Boolean($('#doctor_appointment_form #appointment_patient_id').val());
                            },
                            callback: function (key, options, event) {
                                //oldcode contextMenu.PrintLabel('dob');
                                contextMenu.printNumberofCopy('dob');
                            }
                        },
                        dymoaddress: {
                            name: 'Address Label [DYMO]',
                            disabled: function () {
                                return !Boolean($('#doctor_appointment_form #appointment_patient_id').val());
                            },
                            callback: function (key, options, event) {
                                //oldcode contextMenu.PrintLabel('address');
                                contextMenu.printNumberofCopy('address');
                            }
                        },
                        dymoname: {
                            name: 'Name Label [DYMO]',
                            disabled: function () {
                                return !Boolean($('#doctor_appointment_form #appointment_patient_id').val());
                            },
                            callback: function (key, options, event) {
                                //old codecontextMenu.PrintLabel('name');
                                contextMenu.printNumberofCopy('name');
                            }
                        },
                        dymochart: {
                            name: 'Chart Label [DYMO]',
                            disabled: function () {
                                return !Boolean($('#doctor_appointment_form #appointment_patient_id').val());
                            },
                            callback: function (key, options, event) {
                                //old code contextMenu.PrintLabel('chart');
                                contextMenu.printNumberofCopy('chart');
                            }
                        }
                    }
                }

            }
        };

        for (var a in this.options) {
            if (!this.options[a]) {
                delete parameters.items[a];
            }
        }

        if (!$.isEmptyObject(parameters.items)) {
            $.contextMenu(parameters);
            contextMenu.setTitle('Placeholder Title');
        } else {
            $('#doctors_appt_menu').remove();
        }
    },
    disable: function (item) {
        contextMenu.params[item] = false;
    },
    enable: function (item) {
        contextMenu.params[item] = true;
    },
    getLabel: function (direct_print, label_type) {
        var patient_id = $('#doctor_appointment_form #appointment_patient_id').val();

        if (direct_print) {
            var data = "direct=true&type=" + label_type + "&patient_id=" + patient_id + '&printer=' + contextMenu.params.printer_name;

            var type = '';
            if (label_type == 'dob') {
                type = 'DOB Label';
            } else if (label_type == 'address') {
                type = 'Address Label';
            } else if (label_type == 'name') {
                type = 'Name Label';
            } else if (label_type == 'chart') {
                type = 'Chart Label';
            }

            $.ajax({
                type: 'post',
                url: contextMenu.params.label,
                dataType: "json",
                data: data,
                success: function (rs) {
                    $('#patient_search_checkmark').show();
                    $('#patient_search_messages').text('Created Print Request');
                },
                beforeSend: function () {
                    $('#messages .icons div').hide();
                    $('#patient_search_spinner').show();
                    $("#spinner").show();
                    $("#messages_text").html("Searching for patient...");

                    $('#patient_search_messages').text('Direct Printing ' + type);
                },
                complete: function () {
                    $('#patient_search_spinner').hide();
                    $("#spinner").hide();
                },
                error: function () {
                    $('#patient_search_error_icon').show();
                    $('#patient_search_messages').text('An error occurred while contacting the server.  Please refresh the page and try again.');
                }
            });
        } else {
            var url = contextMenu.params.label + '?type=' + label_type + '&patient_id=' + patient_id;
            contextMenu.setupIFrame(url);
        }
    },
    appointmentVisitDownloadComplete: function () {
        quickClaim.hideOverlayMessage();
        $('#doctor_spinner').hide();
    },
    getVisitSheet: function () {
        hypeFileDownload.tokenName = "AppointmentVisitSheetToken";
        hypeFileDownload.setReturnFunction(contextMenu.appointmentVisitDownloadComplete);

        var url = calendar_events.actions.visit_sheet + '?appointmentAttendeeID='
                + $('#doctor_appointment_form #appointment_appointment_attendee_id').val() + '&AppointmentVisitSheetToken=' + hypeFileDownload.blockResubmit();

        $('#doctor_checkmark').hide();
        $('#doctor_error_icon').hide();
        $('#doctor_spinner').show();

        $('#doctor_messages_text').text('Downloading Appointment Visit Sheet').show();
        quickClaim.showOverlayMessage('Downloading Appointment Visit Sheet');

        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" src="' + url + '"  style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    goToClaim: function (newTab) {

        var doctorCode = $('.doctor_name').text();
        var Dcode = doctorCode.split(' ');
        //alert(Dcode[0]);
        var url = calendar_events.actions.claim  + '?id='+ Dcode[0] + '&patient_id='+ $('#doctor_appointment_form #appointment_patient_id').val() + '&appointment_id='+ $('#doctor_appointment_form #appointment_id').val() + '#new_tab';
        //var url = calendar_events.actions.claim + '/index/appointment_attendee_id/' + $('#doctor_appointment_form #appointment_appointment_attendee_id').val();
        //var url = calendar_events.actions.claim + '?patient_id='+ $('#doctor_appointment_form #appointment_patient_id').val();
        if (newTab) {
            window.open(url, 'claim');
        } else {
            window.location = url;
        }
    },
    goToProfile: function (newTab) {
        var url = calendar_events.actions.patient_profile + '?id=' + $('#doctor_appointment_form #appointment_patient_id').val();
        if (newTab) {
            window.open(url, 'profile');
        } else {
            window.location = url;
        }
    },
    setTitle: function (title) {
        $('.doctors_appt_menu').attr('data-menutitle', title);
    },
    setupIFrame: function (action) {
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" style="width: 0; height: 0; border: none; display: none;" src="' + action + '"></iframe>');
    },
    printNumberofCopy: function(action){
            //Print number of copy code
            printLabelString = action.replace('dymo','');
            if(patient_search.canPrintCopies == true)
            {
            $("#print_number_of_copy_button").attr("disabled", false);
            $("#print_number_of_copy").on( "keypress", function(event) {
                    if (event.which == 13 && !event.shiftKey) {
                        event.preventDefault();
                        $("#print_number_of_copy_button").trigger('click');
                    }
                   
            });
            $('#dymo_print_number_of_copy_dialog').dialog({
                autoOpen: true,
                closeOnEscape: false,
                modal: true,
                position: {my: 'top', at: 'top+100'},
                minHeight: 350,
                width: 500
            });
            $('#print_number_of_copy').focus();
            var count = 1;
            $("#print_number_of_copy_button").click(function(){
                    var noCopy = $("#print_number_of_copy").val().trim();
                    if(count == 1) {
                      if(noCopy == '') {
                            alert('Please enter valid number');
                            return false;
                      }
                      contextMenu.PrintLabel(printLabelString,noCopy); 
                      $("#print_number_of_copy_button").attr("disabled", true); 
                      $('#print_number_of_copy').val(1); 
                      $('#dymo_print_number_of_copy_dialog').dialog('close');
                    }
                    count++;
             });
            }
            else
            {   
                contextMenu.PrintLabel(printLabelString,1);
            }
    }
};