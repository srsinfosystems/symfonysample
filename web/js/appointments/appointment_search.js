var appt_search = {
    hlx8lines: {},
    actions: {
        claim: null,
        csv: null,
        pdf: null,
        profile: null,
        search: null,
        stage_update: null
    },
    messages: {
        checkmark: 'checkmark',
        error: 'error_icon',
        spinner: 'spinner',
        text: 'messages_text'
        },      
     timer: {        
        refresh_time: 20000, // 90 seconds = 90000      
        refresh_timeout: 3600000, // 60 minutes = 3600000       
        check_time: 5000, // 5 seconds = 5000       
        is_set: false
    },
    include_date_in_pdf: 0,
    action_columns: {},
    ajax_running: false,
    form_id: null,
    form_prefix: null,
    isBckgrndActive: false,
    resetAll: false,
    dtCurrentPage: 1,
    maxPages: 1,
    dtCurrentResults: 500,
    data: {},
    errors: {},
    explCodes: {},
    form_error_div: 'appointment_search_errors',
    header: {},
    columns: new Array(),
    results_div: 'results',
    results_table: 'results-table',
    request:null,
    showTotals: false,
    isAddedActionBtn: false,
    results_colspan: 1,
    results_row_prefix: 'results_table_row_',
    results_actions: ['edit', 'cancel', 'reschedule', 'groupreschedule', 'reverse', 'forward', 'claim', 'profile'],
    stages: {},
    use_provinces: true,
    use_regions: true,
    location_doctors: null,
    province_regions: null,
    region_locations: null,
    initialize: function () {
        // Bootstrap datepicker init
        sk.datepicker('#' + this.form_prefix + '_appointment_date_start');
        sk.datepicker('#' + this.form_prefix + '_appointment_date_end');

        var prefix = this.form_prefix;
        $('#' + this.form_prefix + '_appointment_date_start').on('changeDate', function (e) {
            $('#' + prefix + '_appointment_date_end').skDP('setStartDate', e.date);
        });
        $('#' + this.form_prefix + '_appointment_date_end').on('changeDate', function (e) {
            $('#' + prefix + '_appointment_date_start').skDP('setEndDate', e.date);
        });

        $('#find_button').button();
        $('#find_button').click(function () {
            $('#select2reports').select2SortableOrder();
            appt_search.handleFindClick();
        });

        $('#pdf_button').button();
        $('#pdf_button').click(function () {
            return appt_search.handlePdfClick();
        });

        $('#csv_button').button();
        $('#csv_button').click(function () {
            return appt_search.handleCsvClick();
        });

        $('#pull_charts_button').button();
        $('#pull_charts_button').click(function () {
            return appt_search.handleQuickSelection("pull_charts");
        });

        $('#hospital_day_button').button();
        $('#hospital_day_button').click(function () {
            return appt_search.handleQuickSelection("hospital_day");
        });

        $('#doctors_day_button').button();
        $('#doctors_day_button').click(function () {
            return appt_search.handleQuickSelection("doctors_day");
        });

        $('#billing_sheet_button').button();
        $('#billing_sheet_button').click(function () {
            return appt_search.handleQuickSelection("billing_sheet");
        });

        $('#todaysAppts').button();
        $('#todaysAppts').click(function () {
            var date = new Date();
            date = date.toString(quickClaim.getParseDateFormat(global_js.getNewDateFormat()));
            $('#' + appt_search.form_prefix + '_appointment_date_end').val(date);
            $('#' + appt_search.form_prefix + '_appointment_date_start').val(date);
            appt_search.handleFindClick();
        });

        $('#nextApptDate').button();
        $('#nextApptDate').click(function () {
            var date = $(this).val();

            $('#' + appt_search.form_prefix + '_appointment_date_end').val(date);
            $('#' + appt_search.form_prefix + '_appointment_date_start').val(date);
            appt_search.handleFindClick();
        });

        $('.pdf_options_toggle').click(function () {
            appt_search.togglePDFOptions();
        });

        $('#' + appt_search.form_prefix + '_pdf_option_show_subheader').click(function () {
            appt_search.updatePdfOptionsSubheader();
        });
        appt_search.updatePdfOptionsSubheader();

        $('#' + this.form_id + ' div.province input[type=checkbox]').click(function () {
            appt_search.toggleAll('province', $(this).val());
            appt_search.toggleProvinceRegions();
        });

        $('#' + this.form_id + ' div.region input[type=checkbox]').click(function () {
            appt_search.toggleAll('region', $(this).val());
            appt_search.toggleRegionLocations();
        });

        $('#' + this.form_id + ' div.location input[type=checkbox]').click(function () {
            appt_search.toggleAll('location', $(this).val());
            appt_search.toggleLocationDoctors();
        });

        $('#' + this.form_id + ' div.doctor input[type=checkbox]').click(function () {
            appt_search.toggleAll('doctor', $(this).val());
        });

        $('#' + this.form_id + ' div.appointment_status input[type=checkbox]').click(function () {
            appt_search.toggleAll('appointment_status', $(this).val());
        });

        $('#' + this.form_id + ' div.appointment_type input[type=checkbox]').click(function () {
            appt_search.toggleAll('appointment_type', $(this).val());
        });

        $('#' + this.form_id + ' div.slot_type input[type=checkbox]').click(function () {
            appt_search.toggleAll('slot_type', $(this).val());
        });

        appt_search.toggleAll('province');
        appt_search.toggleAll('region');
        appt_search.toggleAll('location');
        appt_search.toggleAll('doctor');
        appt_search.toggleAll('appointment_status');
        appt_search.toggleAll('appointment_type');
        appt_search.toggleAll('slot_type'); 

        appt_search.toggleProvinceRegions();

//init custom start
        $("#locationTab ul")/*.addClass('row')*/.attr('style', 'max-height:60px;overflow:auto;margin-left:0px; margin-right:0px;');
        $("#doctorprofileTab ul").addClass('row').attr('style', 'overflow-y:auto;margin-left:0px; margin-right:0px;');
        $("#doctorprofileTab li").addClass('col-md-6');
        $("#appttypeTab ul").addClass('row').attr('style', 'overflow-y:auto;margin-left:0px; margin-right:0px;');
        $("#appttypeTab li").addClass('col-md-4');
        $("#otherTab ul").addClass('row').attr('style', 'overflow-y:auto;margin-left:0px; margin-right:0px;');
        $("#otherTab li").addClass('col-md-4');


        appt_search.updateLabel(['appointment_search_appointment_status_4', 'appointment_search_appointment_status_5']);
//        appt_search.initSearchSelect();
        
        /* Report Columns selece2*/
        a_global_liarary.initReportSelect("select2reports","appointment_search_report_columns_","reportColoumnsChkbox");
        a_global_liarary.initReportColumn("select2reports","reportColoumnsChkbox");

        /* Doctor's profile */
        $("#doctorprofile ul li input").addClass("doctorprofile");
        a_global_liarary.initReportSelect("select2DoctorProfile","appointment_search_doctor_id_","doctorprofile");
        a_global_liarary.initReportColumn("select2DoctorProfile","doctorprofile");



        var icnt = 0;
        $(".addDataPosition").each(function () {
            $(this).attr('data-position', icnt);
            $(this).parent('li').addClass('col-sm-4');
            icnt++;
        })

        var apptbasedCols = ['17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39'];
        //var apptbasedCols = ['8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'];
        var apptbasedData = '';
        $('label[for="appointment_search_report_columns_patient_id"]').html("PID <small>(Patient's Unique ID)</small>");
        $('label[for="appointment_search_report_columns_patient_hcn"]').html("HCN <small>(Health # & Version Code)");
        $('label[for="appointment_search_report_columns_ref_doc_num"]').html("RefDoc <small>(Referring Doctor Billing #)</small>");
        $('label[for="appointment_search_report_columns_patient_number"]').html("Chart <small>(Chart #)</small>");

        $('label[for="appointment_search_report_columns_service_code"]').html("Serv Code <small>(Service Code)</small>");

        $('.reportColoumnsChkbox').each(function () {
            if ($.inArray($(this).attr('data-position'), apptbasedCols) > -1) {

                $(this).parent('li').detach().appendTo(".claim_based_tab_ul");
//                $(this).parent('li').detach()
            }
        })

        $("#mainReportSection input[type='checkbox']").change(function () {
            var searchSelect2data = [];
            $("#mainReportSection input[type='checkbox']").each(function () {
                if ($(this).prop('checked')) {
                    searchSelect2data.push($(this).val());
                }
            })


            var type = $(this).parents();
            type = type[3].classList[0];

            var value = $(this).val();


            if (value == 'all') {
                appt_search.removeOtherData(type, searchSelect2data);
            } else {
                appt_search.updateSelectData(searchSelect2data);
            }

        })
//        doctorprofileTab
        // init custom stop

        srchChkTag.init();
    },
    clearErrors: function () {
        $('label.error').remove();
        $('.error').removeClass('error');
        $('#' + appt_search.form_error_div).text('').hide();
    },
// add result table row -- start
    addResultsTableRow: function (data, header) {
       //console.log(data);
        var row_id = appt_search.results_row_prefix + data['appointment_id'] + '_' + data['appointment_attendee_id'] + '_' + data['patient_id'];
        data['appointment_attendee_id'] = data['appointment_attendee_id'].replace('no_appointment_attendee', 'noapptattendee');
        var ids = data['appointment_id'] + '_' + data['appointment_attendee_id'] + '_' + data['patient_id'];
        var html = [];
        var newColumnsOrder = $('select[name="appointment_search[report_columns][]"]').val();
        for (var column in header) {
            var text = data[column];
            if (column == 'appointment_actions') {
              
//                if (appt_search.stages.hasOwnProperty(data.stage_client_id)) {
//                    html.push('update');
//                }
                for (var action in this.results_actions) {

                    if (appt_search.action_columns.hasOwnProperty(this.results_actions[action])) {
                        if (data[column].hasOwnProperty(this.results_actions[action])) {
                            html.push(appt_search.action_columns[this.results_actions[action]]);
                        }
                    }
                }
            }
        }
        var dataattr = html.toString();
        var row = '<td class="bulkAction">'
                + '<label class="CHKcontainer">'
                + '<input data-actions="' + dataattr + '" type="checkbox" data-attndId="' + data['appointment_attendee_id'] + '" data-apptId="' + data['appointment_id'] + '" class="bulkAction" id="bulkaction_' + ids + '"  value="all" checked="checked" />'
                + '<span class="CHKcheckmark"></span>'
                + '</label></td>';

        for (var column in newColumnsOrder) {
            var text = data[newColumnsOrder[column]];
            if (newColumnsOrder[column] == 'appointment_actions') {
                row += '<td class="nowrap">' + appt_search.buildStagesCell(data.stage_client_id, 'update-stage_' + ids);//+ '</td>';
                for (var action in this.results_actions) {//cancel', 'reschedule', 'reverse', 'claim
                    if (this.results_actions[action] != 'edit' && this.results_actions[action] != 'cancel' && this.results_actions[action] != 'reschedule' && this.results_actions[action] != 'groupreschedule' && this.results_actions[action] != 'reverse' && this.results_actions[action] != 'forward' && this.results_actions[action] != 'claim') {
                        if (appt_search.action_columns.hasOwnProperty(this.results_actions[action])) {
                            if (data[newColumnsOrder[column]].hasOwnProperty(this.results_actions[action])) {
                                row += //'<td class="' + this.results_actions[action] + '">'
                                        /*+*/ '<input type="button" class="btn btn-blue sk2mar" id="' + this.results_actions[action] + '_' + ids + '" value="' + appt_search.action_columns[this.results_actions[action]] + '" />';
                                // + '</td>';
                            } else {
                                //row += '<td>&nbsp;</td>';
                            }
                        }
                    }
                }
               
            //     if(data['doctor'] || $('#appointment_search_report_columns_doctor_code').is(':checked')){
            //     var billingDoctor = data['doctor'].split(' ');
            //     var doctorCode = "";
            //     if(billingDoctor[0] ==  'MR.'){
            //         $.each(billingDoctor, function(index, value){ 
            //             if(index>0){
            //                 doctorCode += value.substr(0,1);
            //             }
            //         });
            // }else{
            //     doctorCode = billingDoctor[0];
            // }


            if(data['doctor'] || $('#appointment_search_report_columns_doctor_code').is(':checked')){
               
                if(data['doctor']) {
                      var doctorCode = "";
                      // if(data['doctor']) {
                      var billingDoctor = data['doctor'].split(' ');                
                      if(billingDoctor[0] ==  'MR.'){
                            $.each(billingDoctor, function(index, value){ 
                                if(index>0){
                                    doctorCode += value.substr(0,1);
                                }
                            });
                      } 
                      else
                      {
                            doctorCode = billingDoctor[0];
                      }
                }
                else if(!data['doctor'] || $('#appointment_search_report_columns_doctor_code').is(':checked'))
                {
                        doctorCode = data['doctor_code'];
                }
          

                //alert(doctorCode);
                //row += '<a href="'+appt_search.actions.claim+'?id='+data['appointment_attendee_id']+'" class="btn btn-success" target="_blank">Claim</a>';
                // row += '<a href="'+appt_search.actions.claim+'?id='+data['appointment_attendee_id']+'/'+billingDoctor[0]+'" class="btn btn-success" target="_blank">Claim</a>';
                row += '<a href="'+appt_search.actions.claim+'?id='+doctorCode+'&patient_id='+data['patient_id']+'&appointment_id='+data['appointment_id']+'#new_tab" class="btn btn-success" target="_blank">Claim</a>';
                // row += '<input type="button" id="billingDoctorClaim_'+ids+'" class="btn btn-success" value="Claim" />';
            }
                row += '</td>';
            } else {
                row += '<td class="' + newColumnsOrder[column] + ' inelsecodtn">' + text + '</td>';
            }
        }

        if (!$('#' + appt_search.results_table + ' tbody tr#' + row_id).size()) {
            var row = '<tr id="' + row_id + '">' + row + '</tr>';
            $(row).appendTo($('#' + appt_search.results_table + ' tbody'));

        } else {
            $('tr#' + row_id).html(row);
        }

// action button firing events
//        $('input', $('#' + row_id)).button();
        $('input', $('#' + row_id)).click(function () {
            var terms = $(this).attr('id').split('_');
            var action = terms[0];
            var appointment_id = terms[1];
            var attendee_id = terms[2];
            var patient_id = terms[3];
            if (action == 'bulkaction') {
                if ($('tbody input[type="checkbox"]:checked').length == 0) {
                    $(".btnBulkAction").attr('disabled', true);
                } else {
                    $(".btnBulkAction").attr('disabled', false);
                }
                if ($('tbody input[type="checkbox"]:checked').length != $('tbody input[type="checkbox"]').length) {
                    $(".bulkaction_all").attr('checked', false);
                } else {
                    $(".bulkaction_all").attr('checked', true);
                }

            } else if (action == 'claim') {
                var url = appt_search.actions.claim + '/index/appointment_attendee_id/' + attendee_id;
                window.location = url;
            } else if (action == 'update-stage') {
                appt_search.handleUpdateStageClick(appointment_id, attendee_id, patient_id);
                $('#find_button').trigger('click');
            } else if (action == 'profile') {
                var url = appt_search.actions.profile + '/index?id=' + patient_id;
//                window.location = url;
                window.open(url, '_blank');

            } else {
                $('#appointment_book_patient_id').val(patient_id);
                $('#appointment_book_appointment_id').val(appointment_id);
                $('#appointment_book_mode').val(action);
                $('#appointment_book_form').submit();
            }
        });

    },
    fillResultsTable: function (rs) {
        /*console.log(rs.data.length);
        console.log(rs.data);*/

        // var a = [];
        // for (var i = 0; i < rs.data.length; ++i) 
        // {
        //     for (var j = i + 1; j < rs.data.length; ++j)
        //     {
        //         if (rs.data[i]['appointment_id'] < rs.data[j]['appointment_id']) 
        //         {
        //             a =  rs.data[i];
        //             rs.data[i] = rs.data[j];
        //             rs.data[j] = a;
        //         }
        //     }
        // }
        // // console.log(rs.data);
        // rs.data.sort(function(a, b){return b-a});
        var action_columns = {};
        var actions_count = 0;
        var max_colspan = 0;
        var results_count = 0;

        var table = '<table width="100%" class="DataTableAppointmentSearch table sk-datatable" id="' + appt_search.results_table + '">'
                + '<thead><tr>';
        var colHeader = [];
        table += "<th data-column='0' style='padding-left:0px'><label class='CHKcontainer'>"
                + '<input type="checkbox" class="bulkaction_all" name="[]" value="" checked="checked" />'
                + ' <span class="CHKcheckmark"></span>'
                + ' </label></th>';
        var chkall = '<label class="CHKcontainer"><input type="checkbox" class="bulkaction_all" name="[]" value="" checked="checked" />'
                + ' <span class="CHKcheckmark"></span>'
                + ' </label>';
        var dataColumn = 1;
        var timeColumn = 0;
        colHeader.push({title: chkall});
        var newColumnsOrder = $('select[name="appointment_search[report_columns][]"]').val();
        for (var i = 0; i < newColumnsOrder.length; i++) {
            if (newColumnsOrder[i]) {
                if (newColumnsOrder[i] == 'start_time') {
                    timeColumn = dataColumn;
                }
                var text2 = newColumnsOrder[i];
                text2 = text2.replace(/\_/g, ' ');
                var text = text2.charAt(0).toUpperCase() + text2.slice(1);
                var className = newColumnsOrder[i];
                text = text.replace(/\_/g, ' ');
                var newclass = className == 'appointment_actions' ? 'noExport' : '';
                if(text == 'Status text' || text == 'Stage text'){
                    text = text.split(" ")[0];
                }
                table += '<th data-column="' + dataColumn + '" class="' + className + ' ' + newclass + '">' + text.charAt(0).toUpperCase() + text.slice(1) + '</th>';
                max_colspan++;
                dataColumn++;

                if (newColumnsOrder[i] == 'start_time') {
                    colHeader.push({title: text.charAt(0).toUpperCase() + text.slice(1), sType: "timeField", bSortable: true});
                } else if (newColumnsOrder[i] == 'start_date') {
                    colHeader.push({title: text.charAt(0).toUpperCase() + text.slice(1), sType: 'dateField', bSortable: true});
                } else if (newColumnsOrder[i] == 'patient_number') {
                    colHeader.push({title: 'Chart'});

                } else {
                    colHeader.push({title: text.charAt(0).toUpperCase() + text.slice(1)});
                }

            }
        }
        table += '</tr></thead><tbody></tbody>';


        table += '</table>';

        $('#' + appt_search.results_div).html(table);

        for (var index in rs.data) {
            if (rs.data[index].hasOwnProperty('appointment_actions')) {
                for (var action in rs.data[index]['appointment_actions']) {
                    if (!action_columns.hasOwnProperty(action)) {
                        action_columns[action] = rs.data[index]['appointment_actions'][action];
                        actions_count++;
                    }
                }
            }
        }
        $('#' + appt_search.results_table + ' tr th.appointment_actions'); //.attr('colspan', actions_count + 1);
        //alert();
        // $('.actionButtonsDiv').html('');
        // var buttons = '';
        ////////// Bulk Action Buttons HTML
        buttons = '<div class="btn-group TextWhite">';
//['edit', 'cancel', 'reschedule', 'reverse', 'claim', 'profile'],
        buttons += '<a data-action="edit" class="btn btn-primary btnBulkAction">' +
                '<i class="clip-pencil-2"></i> Edit' +
                '</a>';
        buttons += '<a data-action="cancel" class="btn btn-primary btnBulkAction" href="javascript:void(0);">' +
                '<i class="clip-cancel-circle-2"></i> Cancel' +
                '</a>';
        buttons += '<a class="btn btn-primary btnBulkAction" data-action="reschedule" href="javascript:void(0);">' +
                '<i class="clip-clock-2"></i> Reschedule' +
                '</a>';

        if($('#appointment_search_appointment_date_start').val() == $('#appointment_search_appointment_date_end').val()) {
            var grouprescheduleDisabled = '';
        } else {
            var grouprescheduleDisabled = 'disabled="disabled"';
        }

        buttons += '<a class="btn btn-primary btnBulkAction" '+grouprescheduleDisabled+' data-action="groupreschedule" href="javascript:void(0);">' +
                '<i class="clip-clock-2"></i> Group Reschedule' +
                '</a>';

        buttons += '<a data-action="reverse" class="btn btn-primary btnBulkAction" href="javascript:void(0);">' +
                '<i class="clip-transfer"></i> Reverse' +
                '</a>';

        buttons += '<a data-action="forward" data-action="forward" class="btn btn-primary btnBulkAction" href="javascript:void(0);">' +
                '<i class="clip-transfer"></i> Forward' +
                '</a>';

        buttons += '<a data-action="claim" class="btn btn-primary btnBulkAction" href="javascript:void(0);">' +
                '<i class="fa-times-circle-o fa"></i> Claim' +
                '</a>';

        buttons += '</div>';



        appt_search.action_columns = action_columns;
        appt_search.header = rs.header;

//        max_colspan += actions_count;
        for (var index in rs.data) {
            results_count++;
            appt_search.addResultsTableRow(rs.data[index], rs.header);
        }


        $('#printTable').button();
        $('#printTable').click(function (ev) {
            $(".tablesorter1").printElement({
                overrideElementCSS: ['css/jquery-tablesorter1.css', 'css/jquery-tablesorter/jquery.tablesorter.pager.css', 'css/jquery-tablesorter/theme.blue.css']
            });
        });

        $(".DataTableAppointmentSearch tbody").on('click', 'tr', function () {
            if ($(this).find('input[type="checkbox"]').is(':checked')) {
                $(this).find('input[type="checkbox"]').prop('checked', false);
            } else {
                $(this).find('input[type="checkbox"]').prop('checked', true);
            }

        })

        var table = $(".DataTableAppointmentSearch").DataTable({
            "paging": true,
            "lengthMenu": [10, 25, 50, 100, 200, 250, 500],
            "lengthChange": true,
            "pageLength": sk.skdt_page_size,
            "searching": true,
            "ordering": true,
            //"order": [[1, "asc"]],
            "info": true,
            "infoCallback": function( settings, start, end, max, total, pre ) {
                    return start +" to "+ parseInt(end) + ' rows out of '+ total;
                },
//            "autoWidth": true,
//            "responsive": false,
//             "scrollX": true,
//             "scrollY":true,
            title: 'PDF created by PDFMake with Buttons for DataTables.',
            "dom": 'Bfrtip',
            "buttons": [
                {
                    text: '<i class="icon-refresh"></i> Refresh',
                    className: 'btn btn-success btn-squared skApptRefresh',
                    
                },
                {
                    text: '<i class="clip-file-pdf"></i> PDF',
                    className: 'btn btn-danger btn-squared skApptPrint',
                    
                },
                {
                    "extend": 'excel',
                    "id":'btnExport',
                    title: $("#appointment_search_pdf_option_header").val(),
                    "text": '<i class="clip-file-excel"></i> Excel',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    },
                    "className": 'btn btn-info btn-squared'
                },
                {
                    "extend": 'print',
                    title: $("#appointment_search_pdf_option_header").val(),
                    "text": '<i class="fa-print fa"></i> Print',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    },
                    "className": 'btn btn-blue btn-squared'
                },
                {
                    "text": '<i class="clip-share"></i> Share',
                    "className": 'btn btn-primary btn-squared'
                },
            ],
            "oLanguage": {
                "sSearch": "",
                "sLengthMenu": "Per page _MENU_",
                "oPaginate": {
                    "sNext": "<i class='clip-chevron-right'></i>",
                    "sPrevious": "<i class='clip-chevron-left'></i>"
                },
                "sInfo": "_START_ to _END_ rows out of _TOTAL_",

            },
            //"columns": columnsData,
//            "columnDefs": [
//                {"targets": 0, "orderable": false, width: '20px'},
//                {targets: timeColumn, type: 'time-uni'}
//            ],
            "aoColumns": colHeader,
//            "aoColumns": customSortOptions,
            "sDom": '<"wrapper"<"row"<"col-sm-12" <"col-sm-7 actionButtonsDiv"> <"col-sm-5 sk-no-right-pad" Bl > <"col-sm-12" <"row well customWell" <"col-sm-3 customSearchInput" f>  <"col-sm-7 CustomPagination"ip> > > >  >rt<"clear">>',
            initComplete: function () {
                $(".entireClickable").click(function () {
                    if ($(this).find('input').prop('checked')) {
                        $(this).find('input').prop('checked', false);
                        $(this).removeClass('highlight');
                    } else {
                        $(this).find('input').prop('checked', true);
                        $(this).addClass('highlight');
                    }

                })
                
                $(".skApptRefresh").click(function(){
                    $('#find_button').trigger('click');
                });

                $(".skApptPrint").click(function(){
                    sk.pdf.orientation = $('input[name="appointment_search[pdf_option_orientation]"]:checked').val() == "L" ? 'landscape' : 'portrait';
                    sk.pdf.title = $("#appointment_search_pdf_option_header").val();
                    sk.pdf.fileName = $("#appointment_search_pdf_option_header").val()+'_'+moment().unix();
                    if (appt_search.include_date_in_pdf == 1) {
                        sk.pdf.headerDates = 'Date: ' + $("#appointment_search_appointment_date_start").val() + ' - ' + $("#appointment_search_appointment_date_end").val();
                    }
                    sk.pdf.init('.DataTableAppointmentSearch');
                })
                 
                var inputHtml = '<div class="input-group">' +
                        '<input type="text" data-focus="true" placeholder="Contains..." class="form-control DatatableAllRounderSearch" />' +
                        '<span class="input-group-addon cursorPointer btnClearSearchTxt"> ' +
                        '<i class="clip-cancel-circle-2 "></i>' +
                        '</span>' +
                        '<span class="input-group-addon cursorPointer"> ' +
                        '<i class="clip-search-2"></i>' +
                        '</span>' +
                        '</div>';

                $(".customSearchInput").html(inputHtml);

                var inputHtml2 = '<div class="input-group">' +
                        '<select class="newDrProfile DatatableAllRounderSearch"></select>' +
                        '<span class="input-group-addon cursorPointer resetDrProfile"> ' +
                        '<i class="clip-cancel-circle-2 "></i>' +
                        '</span>' +
                        '</div>';
                $(".customSearchField").html(inputHtml2);
                $(".newDrProfile").html($("#past_claims_doctor_id").html());
                $(".newDrProfile option:eq(0)").text('Change Dr. Profile');
                $(".drSelectMainPopular").select2('destroy');
                var currentDrProfile = $(".drSelectMainPopular").val();
                $(".drSelectMainPopular").select2();
                $(".newDrProfile option[value='" + currentDrProfile + "']").prop('selected', true);
                $(".newDrProfile").select2({width: '100%'});

                $('td').each(function () {
                    if ($(this).attr('style')) {
                    } else {
                        $(this).attr('style', 'padding:5px 2px;');
                    }

                })


                var searchoptions = $("#results-table thead tr:eq(0) th");
                var customfilterinputs = '<tr>';
                for (var j = 0; j < searchoptions.length; j++) {
                    customfilterinputs += '<th></th>';
                }
                customfilterinputs += '</tr>';
                $("#results-table thead").append(customfilterinputs);
                var aa = 0;
                var dates = [];
                this.api().columns().every(function () {
                    var column = this;
                    if ($(column.header())[0].dataset.column != 0 && $(column.header())[0].innerText != 'Actions') {
                        var select = $('<select class="selectRsltTbl"><option value=""></option></select>')
                                .appendTo($("#results-table thead tr:eq(1) th:eq(" + aa + ")")/*$("#results-table thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );

                                    column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    }
                    aa++;
                    if ($(column.header())[0].innerText == 'Start date') {
                        column.data().sort().each(function (d, j) {
                            dates.push(sk.getJsDate(d));
                        });
                    }
                });

                var orderedDates = dates.sort(function (a, b) {
                    return a > b;
                });

                if ($("#appointment_search_appointment_date_start").val() == '') {
                    $("#appointment_search_appointment_date_start").skDP('setDate', orderedDates[0]);
                }
                if ($("#appointment_search_appointment_date_end").val() == '') {
                    $("#appointment_search_appointment_date_end").skDP('setDate', orderedDates[orderedDates.length - 1]);
                }

            }

        });
        setInterval (function() {
            $('#find_button').trigger('click');
        }, 600000);
        //600000

        // save page length 
        $(".DataTableAppointmentSearch").on('length.dt', function () {
            var info = $(".DataTableAppointmentSearch").DataTable().page.info();
            sk.setClientParam('dt_page_size', info.length);
        });


        $(".DatatableAllRounderSearch").keyup(function () {
            table.search($(this).val(), true).draw();
        })
        $(".btnClearSearchTxt").click(function () {
            $(".DatatableAllRounderSearch").val('');
            table.search('', true).draw();
        })
        $(".resetDrProfile").click(function () {
            $(".newDrProfile").select2('destroy');
            $(".newDrProfile option:eq(0)").prop('selected', true);
            $(".newDrProfile").select2();
            $(".newDrProfile").trigger('change');//select2('destroy');

        })

        $.fn.dataTableExt.oSort["dateField-desc"] = function (x, y) {
            var a = moment(x, global_js.getNewDateFormat().toUpperCase());
            var b = moment(y, global_js.getNewDateFormat().toUpperCase());
            return (a.diff(b));
        };

        $.fn.dataTableExt.oSort["dateField-asc"] = function (x, y) {
            var a = moment(x, global_js.getNewDateFormat().toUpperCase());
            var b = moment(y, global_js.getNewDateFormat().toUpperCase());
            return (b.diff(a));
        }

        $.fn.dataTableExt.oSort["timeField-desc"] = function (x, y) {
            var timeF = quickClaim.timeFormat;
            timeF = timeF.replace('ii', 'mm').replace('a', 'A');
            var a = moment(x, timeF);
            var b = moment(y, timeF);
            return (a.diff(b));
        };

        $.fn.dataTableExt.oSort["timeField-asc"] = function (x, y) {
            var timeF = quickClaim.timeFormat;
            timeF = timeF.replace('ii', 'mm').replace('a', 'A');
            var a = moment(x, timeF);
            var b = moment(y, timeF);
            return (b.diff(a));
        }

        $.extend($.fn.dataTableExt.oSort, {
            "time-uni-pre": function (a) {
                var uniTime;

                if (a.toLowerCase().indexOf("am") > -1 || (a.toLowerCase().indexOf("pm") > -1 && Number(a.split(":")[0]) === 12)) {
                    uniTime = a.toLowerCase().split("pm")[0].split("am")[0];
                    while (uniTime.indexOf(":") > -1) {
                        uniTime = uniTime.replace(":", "");
                    }
                } else if (a.toLowerCase().indexOf("pm") > -1 || (a.toLowerCase().indexOf("am") > -1 && Number(a.split(":")[0]) === 12)) {
                    uniTime = Number(a.split(":")[0]) + 12;
                    var leftTime = a.toLowerCase().split("pm")[0].split("am")[0].split(":");
                    for (var i = 1; i < leftTime.length; i++) {
                        uniTime = uniTime + leftTime[i].trim().toString();
                    }
                } else {
                    uniTime = a.replace(":", "");
                    while (uniTime.indexOf(":") > -1) {
                        uniTime = uniTime.replace(":", "");
                    }
                }
                return Number(uniTime);
            },

            "time-uni-asc": function (a, b) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },

            "time-uni-desc": function (a, b) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        });

        $('div.dataTables_filter input').addClass('form-control');
        $('div.dataTables_filter input').attr('placeholder', 'Search');
        $(".DTsearchlabel").html('<i class="clip-search"></i>');
        $('.dataTables_filter').attr('style', 'width:100%');
        $('.dataTables_filter label').attr('style', 'width:100%');
        $('.dataTables_length select').attr('style', 'font-size: 12px;padding: 5px 3px;background: #fff;');
        $('div.dataTables_filter input').attr('style', 'float:right;width:98%;margin-right:5px;');
        $(".dataTables_wrapper .dataTables_paginate .paginate_button").attr('style', 'background:#fff !important;color:#444444 !important; border-radius: 0;');

        $(".selectRsltTbl").select2({
            placeholder: "Search...",
            allowClear: true,
            dropdownAutoWidth: true,
            width: '98%'
        });


        $(".select2-arrow").hide();

        $(".CustomPagination").prepend('<div style="float:left"><button data-toggle="tooltip" title="Filters" type="button" class="btn btn-default filterToggle"><i class="fa fa-filter"></i></button></div>');

        setTimeout(function () {
            $("#results-table thead tr:eq(1)").toggle();
        }, 500);
        $(".filterToggle").click(function () {
            $("#results-table thead tr:eq(1)").toggle();
        })



        //if(rs.header[index]){
        if (rs.header.hasOwnProperty('appointment_actions')) {
        // setTimeout(function () {
        //alert($('.actionButtonsDiv').length);
        //if (!$(buttons).find(".actionButtonsDiv").length) {
        $(buttons).appendTo($('.actionButtonsDiv'));
        // }
        $(".btnBulkAction").on('click', function () {
        appt_search.handleBulkAction($(this).attr('data-action'));
        })
        appt_search.enableBulkAction();
        // }, 1000);
        }
        //}

        // bulk action all event handle
        $(".bulkaction_all").click(function () {
            if ($(this).attr('checked')) {
                $("tbody input[type='checkbox']").attr('checked', true);
                $(".btnBulkAction").attr('disabled', false);
            } else {
                $("tbody input[type='checkbox']").attr('checked', false);
                $(".btnBulkAction").attr('disabled', true);
            }
        });



    },
    buildStagesCell: function (stage_client_id, id) {
        var select = '';
        var options = '';

        if (appt_search.stages.hasOwnProperty(stage_client_id)) {
            for (var a in appt_search.stages[stage_client_id].next_stage) {
                var name = appt_search.stages[stage_client_id].next_stage[a];
                options += '<option value="' + a + '">' + name + '</option>';
            }

            if (options.length) {
                var select = '<select class="actionSelect" id="select_' + id + '" name="' + id + '">'
                        + '<option value="' + stage_client_id + '" selected="selected">' + appt_search.stages[stage_client_id].name + '</option>'
                        + options
                        + '</select>'
                        + '<input type="button" id="' + id + '" class="btn btn-primary sk2mar"  onclick="appt_search.appointmentActions(this.id)" value="Update" />';
            }
        }
        return select;
    },
    handleQuickSelection: function (selection) {
        $('#appointment_search_report_columns_location, #appointment_search_report_columns_doctor, #appointment_search_report_columns_start_date, #appointment_search_report_columns_start_time, #appointment_search_report_columns_appt_length, #appointment_search_report_columns_appt_type, #appointment_search_report_columns_service_code, #appointment_search_report_columns_diag_code, #appointment_search_report_columns_patient_id, #appointment_search_report_columns_patient_number, #appointment_search_report_columns_last_first, #appointment_search_report_columns_patient_hcn, #appointment_search_report_columns_patient_dob, #appointment_search_report_columns_patient_email, #appointment_search_report_columns_patient_refdoc_num, #appointment_search_report_columns_phone, #appointment_search_report_columns_patient_location, #appointment_search_report_columns_status_text, #appointment_search_report_columns_stage_text, #appointment_search_report_columns_notes, #appointment_search_report_columns_appointment_actions').prop('checked', false);

        if (selection === "pull_charts") {
            $('#appointment_search_report_columns_start_date, #appointment_search_report_columns_start_time, #appointment_search_report_columns_patient_number, #appointment_search_report_columns_last_first, #appointment_search_report_columns_patient_hcn').prop('checked', true);
            $('#appointment_search_pdf_option_header').val('Pull Charts');
        } else if (selection === "hospital_day") {
            $('#appointment_search_report_columns_start_time, #appointment_search_report_columns_last_first, #appointment_search_report_columns_patient_hcn, #appointment_search_report_columns_patient_dob, #appointment_search_report_columns_phone, #appointment_search_report_columns_notes').prop('checked', true);
            $('#appointment_search_pdf_option_header').val('Hospital Day');
        } else if (selection === "doctors_day") {
            $('#appointment_search_report_columns_start_time, #appointment_search_report_columns_appt_type, #appointment_search_report_columns_service_code, #appointment_search_report_columns_diag_code, #appointment_search_report_columns_patient_number, #appointment_search_report_columns_last_first, #appointment_search_report_columns_phone, #appointment_search_report_columns_notes').prop('checked', true);
            $('#appointment_search_pdf_option_header').val('Doctors Day');
        } else if (selection === "billing_sheet") {
            $('#appointment_search_report_columns_location, #appointment_search_report_columns_doctor, #appointment_search_report_columns_start_date, #appointment_search_report_columns_appt_type, #appointment_search_report_columns_service_code, #appointment_search_report_columns_diag_code, #appointment_search_report_columns_last_first, #appointment_search_report_columns_patient_hcn, #appointment_search_report_columns_patient_dob, #appointment_search_report_columns_patient_refdoc_num').prop('checked', true);
            $('#appointment_search_pdf_option_header').val('Billing Sheet');
        }
    },
    handleCsvClick: function () {
        if (appt_search.ajax_running) {
            return;
        }

        appt_search.clearErrors();
        data = $('#' + appt_search.form_id).serialize();
        data += '&mode=csv';

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: appt_search.actions.search,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#' + appt_search.form_error_div, '#' + appt_search.form_id, true);
                    quickClaim.focusTopField();

                    $('#' + appt_search.messages.error).show();
                    $('#' + appt_search.messages.text).text('The form contains invalid data.').show();
                } else {
                    appt_search.handleCsvRequest(data);
                }
            },
            beforeSend: function () {
                appt_search.ajax_running = true;
                $('#messages .icons div').hide();
                $('#' + appt_search.messages.spinner).show();
                $('#' + appt_search.messages.text).text('Building CSV').show();
            },
            complete: function () {
                appt_search.ajax_running = false;
                $('#' + appt_search.messages.spinner).hide();
            },
            error: function () {
                $('#' + appt_search.messages.error).show();
                $('#' + appt_search.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });
    },
    handleCsvRequest: function (data) {
        var fields = appt_search.splitFormData(data);
        appt_search.setupIframe();
        appt_search.setupHiddenForm(fields, appt_search.actions.csv, 'csv');
    },
    initDatatable: function () {
        sk.pdf.alreadySpliced = false;
        $("#apptSearchErrors").hide();
        $("#service_based").parent('.tab-content').attr('style', 'border-color:#ddd');
        $('a[href="#service_based"]').attr('style', 'border-left-color:#ddd;border-right-color:#ddd;');
        var newColumnsOrder = $('select[name="appointment_search[report_columns][]"]').val();
        appt_search.columns = newColumnsOrder;
        if (newColumnsOrder == null) {

            $("#apptSearchErrors").html('Please select report column').addClass('alert alert-danger').show();
            $("#service_based").parent('.tab-content').attr('style', 'border-color:#BD1431');
            $('a[href="#service_based"]').attr('style', 'border-left-color:#BD1431;border-right-color:#BD1431');
            $("#tabs").tabs("option", "active", 1);
            return false;
        }

        var colHeader = [];

         colHeader.push({
            title: '<span class="edit_checkbox"><label class="CHKcontainer"><input type="checkbox" class="bulkaction_all" name="[]" value="" checked="checked"><span class="CHKcheckmark"></span></label></span>',
            width: '20px'
        });

        for (var i = 0; i < newColumnsOrder.length; i++) {
            if (newColumnsOrder[i]) {
                var text = newColumnsOrder[i];
                var width = 10;
                text = text.replace(/\_/g, ' ');

                text = text == 'Updated user id' ? 'Updated By UserID' : text;
                text = text == 'Updated username' ? 'Updated By User Name' : text;
                text = text == 'Created user id' ? 'Created By User Name' : text;
                text = text == 'Created User Name' ? 'Created By User Name' : text;

                var arr = newColumnsOrder[i].split('_');
                if ($.inArray('date', arr) != -1 || newColumnsOrder[i] == 'dob') {
                    colHeader.push({title: text.charAt(0).toUpperCase() + text.slice(1), width: '100px', sType: 'dateField', bSortable: true});
                } else {
                    colHeader.push({title: text.charAt(0).toUpperCase() + text.slice(1), width: '100px'});
                }

            }
        }

        quickClaim.ajaxLocked = true;
        if ($.fn.DataTable.isDataTable("#" + appt_search.results_table)) {
            $("#" + appt_search.results_table).dataTable().fnDestroy();
            $("#" + appt_search.results_table).html('');
        }

        if (!$.fn.DataTable.isDataTable('#' + appt_search.results_table)) {
           var CurDate = moment().format(sk.getMomentDatetimeFormat());
           var orientation = $("input[name='orientation']:checked").val();
         
           var datatable = $('#' + appt_search.results_table).DataTable({
                "paging": true,
                "lengthMenu": [10, 25, 50, 100, 200, 250, 500],                
                "lengthChange": true,
                "pageLength": sk.skdt_page_size,
                "searching": true,
                "ordering": true,
                "stateSave": true,
                "info": true,
                "dom": 'Bfrtip',
                'fnCreatedRow': function (nRow, aData, iDataIndex) {
                    var ClaimIdsAdd = $('td .item_id', nRow).val();
                    var ClaimIds = $('td .claim_id', nRow).val();
                    //console.log(hidden_fields);
                    $(nRow).addClass('claim_' + ClaimIds); 
                    $(nRow).attr('id', 'tr_' + ClaimIdsAdd); // or whatever you choose to set as the id
                },
                "buttons": [
                {
                    text: '<i class="icon-refresh"></i> Refresh',
                    className: 'btn btn-success btn-squared skApptRefresh',
                    
                },
                {
                    text: '<i class="clip-file-pdf"></i> PDF',
                    className: 'btn btn-danger btn-squared skApptPrint',
                    
                },
                {
                    "extend": 'excel',
                    "id":'btnExport',
                    title: $("#appointment_search_pdf_option_header").val(),
                    "text": '<i class="clip-file-excel"></i> Excel',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    },
                    "className": 'btn btn-info btn-squared'
                },
                {
                    "extend": 'print',
                    title: $("#appointment_search_pdf_option_header").val(),
                    "text": '<i class="fa-print fa"></i> Print',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    },
                    "className": 'btn btn-blue btn-squared'
                },
                {
                    "text": '<i class="clip-share"></i> Share',
                    "className": 'btn btn-primary btn-squared'
                },
               ],
                "oLanguage": {
                   "sSearch": "",
                "sLengthMenu": "Per page _MENU_",
                "oPaginate": {
                    "sNext": "<i class='clip-chevron-right'></i>",
                    "sPrevious": "<i class='clip-chevron-left'></i>"
                },
                "sInfo": "_START_ to _END_ rows out of _TOTAL_",

                },
                "infoCallback": function( settings, start, end, max, total, pre ) {
                    return start +" to "+ parseInt(end) + ' rows out of '+ total;
                },    
                dom: 'Bfrtip',
                "columns": colHeader,
                "columnDefs": [
                    {"targets": 0, "orderable": false}
                ], //<div class="sk-light-gray w3-light-grey" style="padding: 0px -15px;margin: 0px -15px;"><div class="sk-blue w3-blue" style="height: 5px;width:15%;"></div></div>
                "sDom": '<"wrapper"<"row"<"col-sm-12" <"col-sm-7 actionButtonsDiv"> <"col-sm-5 sk-norightpad" Bl > <"col-sm-12" <"sk-light-gray progressBarCont" <"sk-blue ProgressBarMain">><"row well customWell" <"col-sm-3 customSearchInput" f> <"col-sm-3 customSearchField">  <"col-sm-6 CustomPagination"ip> > > >  >rt<"clear">>',
                initComplete: function () {

                    $(".entireClickable").click(function () {
                    if ($(this).find('input').prop('checked')) {
                        $(this).find('input').prop('checked', false);
                        $(this).removeClass('highlight');
                    } else {
                        $(this).find('input').prop('checked', true);
                        $(this).addClass('highlight');
                    }

                    })
                    
                    $(".skApptRefresh").click(function(){
                        $('#find_button').trigger('click');
                    });



                    $(".skApptPrint").click(function(){
                    sk.pdf.orientation = $('input[name="appointment_search[pdf_option_orientation]"]:checked').val() == "L" ? 'landscape' : 'portrait';
                    sk.pdf.title = $("#appointment_search_pdf_option_header").val();
                    sk.pdf.fileName = $("#appointment_search_pdf_option_header").val()+'_'+moment().unix();
                    if (appt_search.include_date_in_pdf == 1) {
                        sk.pdf.headerDates = 'Date: ' + $("#appointment_search_appointment_date_start").val() + ' - ' + $("#appointment_search_appointment_date_end").val();
                    }
                    sk.pdf.init('.DataTableAppointmentSearch');
                })
                 
                var inputHtml = '<div class="input-group">' +
                        '<input type="text" data-focus="true" placeholder="Contains..." class="form-control DatatableAllRounderSearch" />' +
                        '<span class="input-group-addon cursorPointer btnClearSearchTxt"> ' +
                        '<i class="clip-cancel-circle-2 "></i>' +
                        '</span>' +
                        '<span class="input-group-addon cursorPointer"> ' +
                        '<i class="clip-search-2"></i>' +
                        '</span>' +
                        '</div>';

                $(".customSearchInput").html(inputHtml);

                // var inputHtml2 = '<div class="input-group">' +
                //         '<select class="newDrProfile DatatableAllRounderSearch"></select>' +
                //         '<span class="input-group-addon cursorPointer resetDrProfile"> ' +
                //         '<i class="clip-cancel-circle-2 "></i>' +
                //         '</span>' +
                //         '</div>';
                // $(".customSearchField").html(inputHtml2);
                $(".newDrProfile").html($("#past_claims_doctor_id").html());
                $(".newDrProfile option:eq(0)").text('Change Dr. Profile');
                $(".drSelectMainPopular").select2('destroy');
                var currentDrProfile = $(".drSelectMainPopular").val();
                $(".drSelectMainPopular").select2();
                $(".newDrProfile option[value='" + currentDrProfile + "']").prop('selected', true);
                $(".newDrProfile").select2({width: '100%'});

                $('td').each(function () {
                    if ($(this).attr('style')) {
                    } else {
                        $(this).attr('style', 'padding:5px 2px;');
                    }

                })



                    var searchoptions = $("#" + appt_search.results_table + " thead tr:eq(0) th");
                    var customfilterinputs = '<tr>';
                    for (var j = 0; j < searchoptions.length; j++) {
                        customfilterinputs += '<th></th>';
                    }
                    customfilterinputs += '</tr>';
                    $("#" + appt_search.results_table + " thead").append(customfilterinputs);

                    $('#' + appt_search.results_table).on('length.dt', function (e, settings, len) {
                    });
                }
              });

            $('#' + appt_search.results_table + " tbody").on('click', 'tr', function () {
                if ($(this).find('input[type="checkbox"]').is(':checked')) {
                    $(this).find('input[type="checkbox"]').prop('checked', false);
                } else {
                    $(this).find('input[type="checkbox"]').prop('checked', true);
                }

            })
            // save page length 
            $('#' + appt_search.results_table).on('length.dt', function () {
                var info = $('#' + appt_search.results_table).DataTable().page.info();
                sk.setClientParam('dt_page_size', info.length);
            });

            $(".DatatableAllRounderSearch").keyup(function () {
                datatable.search($(this).val(), true).draw();
            })
            $(".resetDrProfile").click(function () {
//                            $(".newDrProfile").select2('destroy');
                $(".newDrProfile option:eq(0)").prop('selected', true);
//                            $(".newDrProfile").select2();
                $(".newDrProfile").trigger('change');//select2('destroy');

            })


            $(".CHKcontainer input[type='checkbox']").change(function () {
                if ($('.CHKcontainer input[type="checkbox"]:checked').length > 0) {
                    $(".actionButtonsDiv a").attr('disabled', false);
                } else {
                    $(".actionButtonsDiv a").attr('disabled', true);
                }
            });

            $('#resultsCheckAll').click(function () {
                var checked = $(this).prop('checked');
                $('#' + appt_search.results_table + ' input[type=checkbox]').prop('checked', checked);
            });

            $(".btnClearSearchTxt").click(function () {
                $(".DatatableAllRounderSearch").val('');
                datatable.search('', true).draw();
            })
            $(".otherSearchFilter").change(function () {
                var data = $(this).select2({placeholder: "Claim Type", width: '100%'}).val();

                if (data && data.length > 0) {
                    var d2 = '';
                    for (var i in data) {
                        data[i] = data[i].substring(1, data[i].length);
                        d2 += data[i] + '|';
                    }
                    d2 = d2.substring(0, d2.length - 1);
                    $("#" + appt_search.results_table).DataTable().columns('.charge_to')
                            .search(d2, true)
                            .draw();
                } else {
                    datatable
                            .columns('.charge_to')
                            .search('')
                            .draw();
                }
            })

            $(".newDrProfile").change(function () {
                var data = $(this).select2('data');

                var aa = $("#" + appt_search.results_table).DataTable().columns('.doctor').data();

                if (aa.length) {
                    if (data) {
                        data = data.text;
                        $("#" + appt_search.results_table).DataTable().columns('.doctor')
                                .search(data)
                                .draw();
                    } else {
                        datatable
                                .columns('.doctor')
                                .search('')
                                .draw();
                    }
                } else {
                    if (data) {

                        data = data.text;
                        var newoptionval = $(".newDrProfile option:contains(" + data + ")").val();
                        $(".drSelectMainPopular").select2("val", newoptionval).trigger('change');
                        $("#find_button").trigger('click');
                        //$("#" + pastClaims.resultsTable).dataTable().fnDestroy();

                    } else {
                        datatable.columns('.doctor').search('').draw();
                    }
                }
            })

            $('div.dataTables_filter input').addClass('form-control');
            $('div.dataTables_filter input').attr('placeholder', 'Search');
            $(".DTsearchlabel").html('<i class="clip-search"></i>');
            $('.dataTables_filter').attr('style', 'width:100%');
            $('.dataTables_filter label').attr('style', 'width:100%');
            $('.dataTables_length select').attr('style', 'font-size: 12px;padding: 5px 3px;background: #fff;');
            $('div.dataTables_filter input').attr('style', 'float:right;width:98%;margin-right:5px;');
            $(".dataTables_wrapper .dataTables_paginate .paginate_button").attr('style', 'background:#fff !important;color:#444444 !important; border-radius: 0;');

            $(".CustomPagination").prepend('<div style="float:left"><button data-toggle="tooltip" title="Filters" type="button" class="btn btn-default filterToggle"><i class="fa fa-filter"></i></button></div>');

            $(".filterToggle").click(function () {
                $("#" + appt_search.results_table + " thead tr:eq(1)").toggle();
            })
            appt_search.isAddedActionBtn = true;
        } else {

            var datatable = $("#" + appt_search.results_table).DataTable();
        }
        var tblWidth = (+newColumnsOrder.length * 10) - 40;

        datatable.on('search.dt', function () {

           // appt_search.clearSubTotalTable();
            //appt_search.updateSubTotalTable();
        })
        datatable.on('length.dt', function (e, settings, len) {

           //appt_search.clearSubTotalTable();
            //appt_search.updateSubTotalTable();
        });
        datatable.on('page.dt', function () {

            //appt_search.clearSubTotalTable();
            setTimeout(function () {
                //appt_search.updateSubTotalTable();
            }, 200);
        });

          appt_search.processDataTableData(datatable);
          //Past claim sort stay same page http://live.datatables.net/siqaxoqi/1/edited
          var page = 0;
          datatable.on('order', function() {
            if (datatable.page() !== page) {
              datatable.page(page).draw('page');
            }
          });
          datatable.on('page', function() {
            page = datatable.page();
           });

        // bulk action all event handle
        $(".bulkaction_all").click(function () {
            if ($(this).attr('checked')) {
                $("tbody input[type='checkbox']").attr('checked', true);
                $(".btnBulkAction").attr('disabled', false);
            } else {
                $("tbody input[type='checkbox']").attr('checked', false);
                $(".btnBulkAction").attr('disabled', true);
            }
        });

    },
    processDataTableData: function (datatable) {
        var currentPage = appt_search.dtCurrentPage;
        var maxPages = appt_search.maxPages;
        var dataSet = [];
        data = $('#' + appt_search.form_id).serialize()  + "&pageNumber=1&size=" + appt_search.dtCurrentResults;
        appt_search.clearErrors();
        if (currentPage == 1 /*&& pastClaims.resetAll == false*/) {
            appt_search.request = $.ajax({
                url: appt_search.actions.search,
                data: data,
                //url: appt_search.actions.search + '?pageNumber=1&size=' + appt_search.dtCurrentResults + '&' + $('#' + appt_search.form_id).serialize(),
                type: 'POST',
                success: function (res) {
                    var obj = JSON.parse(res);

                    if (obj.hasOwnProperty('errors')) {
                        quickClaim.showFormErrors(obj.errors, '#' + appt_search.form_error_div, '#' + appt_search.form_id, true);
                        $('#' + appt_search.messages.error).show();
                        $('#' + appt_search.messages.text).text('The form contains invalid data.').show();

                    } else {

                            $('#tabs').tabs('enable', 1);
                            $('#tabs').tabs('option', 'active', 1);

                           /* $('#' + pastClaims.messages.text).html('Processing Results.').show();
                            $('#' + pastClaims.messages.checkmark).show();*/

                            if (obj.hasOwnProperty('data')) {

                                // s = obj.headers;
                                // var out = Object.keys(s).map(function(data){
                                //     return [data,s[data]];
                                // });

                                // var a = [];
                                // for ( var i = 0; i < out.length; i++ ) {
                                //     if(out[ i ][0] != 0){
                                //         a.push( out[ i ][0] );
                                //     }
                                // }
                                //pastClaims.updateDefaultData(a.toString());

                                s = obj.header;
                                var out = Object.keys(s).map(function(data){
                                    return [data,s[data]];
                                });

                                var a = [];
                                for ( var i = 0; i < out.length; i++ ) {
                                    a.push( out[ i ][0] );
                                }
                                appt_search.updateDefaultData(a.toString());


                                for (var j in obj.data) {
                                   //   console.log('dataSet',obj.data[j]);
                                    dataSet.push(appt_search.buildDataTableRow(obj.data[j], obj.header));
                                    //dataSet.push(obj.data[j]);
                                    //console.log(dataSet);
                                    // var idS = j+2;
                                    // $('#past_claims_results_table tr:eq('+idS+')').attr('id','tr_');
                                    //console.log(pastClaims.buildDataTableRow(obj.data[j]));
                                }
                                var datatable = $("#" + appt_search.results_table).DataTable();
                                console.log('dataSet',dataSet);
                                // console.log('res',res);
                                //appt_search.fillResultsTable(res);
                                datatable.rows.add(dataSet);
                                //console.log(datatable.rows);
                                datatable.draw();


                                appt_search.actionButtonCreate(obj.data, obj.header);

                                // var i = 0;
                                // for (var j in obj.data) {
                                //         //console.log(obj.data[j]['id']);
                                //         $("#past_claims_results_table tbody tr:eq("+i+")").attr('id','tr_'+obj.data[j]['id']);
                                //         i++;
                                // }

                                //$('#past_claims_results_table tbody tr:eq(1)').attr('id','tr_');
                                appt_search.dtCurrentPage++;
                                appt_search.maxPages = Math.ceil(obj.count / appt_search.dtCurrentResults);
                                if (appt_search.maxPages > 1) {
                                    var message = "Found total " + obj.count + " records and fetched " + appt_search.dtCurrentResults + " records, other records are fetching in background.";
                                    $.notify(message, {
                                        element: 'body',
                                        type: "info",
                                        allow_dismiss: true,
                                        newest_on_top: true,
                                        placement: {
                                            from: "top",
                                            align: "right"
                                        },
                                        delay: 5000,
                                        z_index: 99999,
                                        mouse_over: 'pause',
                                        animate: {
                                            enter: 'animated bounceIn',
                                            exit: 'animated bounceOut'
                                        },
                                        template:  `<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                                                    <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                                    <span data-notify="icon"></span>
                                                    <span data-notify="title">{1}</span>
                                                    <span data-notify="message">{2}</span>
                                                    <div class="progress" data-notify="progressbar">
                                                            <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                                    </div>
                                                    <a href="{3}" target="{4}" data-notify="url"></a>
                                            </div>`

                                    });
                                }
                                //                        if(pastClaims.maxPages == pastClaims.dtCurrentPage){                            
                                $(".selectRsltTbl").select2('destroy');
                                var aa = 0;

                                var apiDt = $("#" + appt_search.results_table).dataTable().api();
                                apiDt.columns().every(function () {

                                    var column = this;
                                    var columnText = $.trim($(column.header())[0].innerText);

                                    if (columnText == 'Error(s)') {
                                        $("#" + appt_search.results_table + " thead tr:eq(1) th:eq(" + aa + ")").html('');
                                        var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                                .appendTo($("#" + appt_search.results_table + " thead tr:eq(1) th:eq(" + aa + ")")/*$("#" + pastClaims.resultsTable + " thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                                                .on('change', function () {
                                                    var val = $.fn.dataTable.util.escapeRegex(
                                                            $(this).val().replace(/<\/?[^>]+(>|$)/g, "")
                                                            );
                                                    val =  val.replace(/\\/gi, "");
                                                    column
                                                            .search(val ? val : '', true, false)
                                                            .draw();
                                                });

                                        column.data().unique().sort().each(function (d, j) {
                                            d = $(d).text();
                                            d = d.replace(/<\/?[^>]+(>|$)/g, "");
                                            select.append('<option value="' + d + '">' + d + '</option>')
                                        });
                                    } else if (columnText == 'Actions') {
                                        $("#" + appt_search.results_table + " thead tr:eq(1) th:eq(" + aa + ")").html('');
                                    } else if (aa != 0) {
                                        $("#" + appt_search.results_table + " thead tr:eq(1) th:eq(" + aa + ")").html('');
                                        var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                                .appendTo($("#" + appt_search.results_table + " thead tr:eq(1) th:eq(" + aa + ")")/*$("#" + pastClaims.resultsTable + " thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                                                .on('change', function () {
                                                    var val = $.fn.dataTable.util.escapeRegex(
                                                            $(this).val().replace(/<\/?[^>]+(>|$)/g, "")
                                                            );
                                                    val =  val.replace(/\\/gi, "");
                                                    column.search(val ? val : '', true, false).draw();
                                                });

                                        var chkarr = [];
                                        column.data().unique().sort().each(function (d, j) {
                                            d = d.replace(/<\/?[^>]+(>|$)/g, "");
                                            if ($.inArray(d, chkarr) < 0) {
                                               if(d != '') {
                                               select.append('<option value="' + d + '">' + d + '</option>');
                                               }
                                                chkarr.push(d);
                                            }

                                        });
                                    }
                                    aa++;
                                });
                                $.fn.dataTableExt.oSort["dateField-desc"] = function (x, y) {
                                    var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                                    var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                                    return (a.diff(b));
                                };

                                $.fn.dataTableExt.oSort["dateField-asc"] = function (x, y) {
                                    var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                                    var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                                    return (b.diff(a));
                                }
        //                        }
                                $(".selectRsltTbl").select2({
                                    placeholder: "Search",
                                    allowClear: true,
                                    dropdownAutoWidth: true,
                                    width: '98%'
                                });
                                $('#' + appt_search.results_table + ' .select2-arrow').hide();

                                if ($("#" + appt_search.results_table + " thead tr:eq(1)").is(':visible')) {
                                    $("#" + appt_search.results_table + " thead tr:eq(1)").toggle();
                                }
                                // if (appt_search.isAddedActionBtn == true) {
                                //     appt_search.results.buildActionButtons();
                                //     appt_search.isAddedActionBtn == false;
                                // }
                                //appt_search.afterDtDataFinished();
                                //appt_search.results.buildErrorTooltips();
                                appt_search.updateProgressBar();
                                appt_search.processDataTableData(datatable);
                            }
                    }
                },
                beforeSend: function () {
                  appt_search.ajax_running = true;
                    $('#messages .icons div').hide();
                    $('#' + appt_search.messages.spinner).show();
                    $('#' + appt_search.messages.text).text('Searching for appointments').show();
                  if (appt_search.request != null) {
                        appt_search.request.abort();
                  }

                },
                complete: function () {
                   appt_search.ajax_running = false;
                    $('#' + appt_search.messages.spinner).hide();
                },error: function () {
                    $('#' + appt_search.messages.error).show();
                    $('#' + appt_search.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
                }
            })
        } else if (currentPage <= maxPages /* && pastClaims.resetAll == false*/) {
               appt_search.request = $.ajax({
                url: appt_search.actions.search,
                data: $('#' + appt_search.form_id).serialize()  + "&pageNumber="+ currentPage +"&size=" + appt_search.dtCurrentResults,
                //url: appt_search.actions.search + '?pageNumber=1&size=' + appt_search.dtCurrentResults + '&' + $('#' + appt_search.form_id).serialize(),
                type: 'POST',
                success: function (res) {
                    var obj = JSON.parse(res);
                    if (obj.hasOwnProperty('data')) {
                        appt_search.data = {};
                        appt_search.errors = {};

                        for (var j in obj.data) {
                            dataSet.push(appt_search.buildDataTableRow(obj.data[j]));
                        }
                        datatable.rows.add(dataSet);
                        datatable.draw();


                        $(".selectRsltTbl").select2('destroy');
                        var aa = 0;
                        var apiDt = $("#" + appt_search.results_table).dataTable().api();
                        apiDt.columns().every(function () {

                            var column = this;
                            var columnText = $.trim($(column.header())[0].innerText);
                            var selectedSearch = $("#" + appt_search.results_table + " thead tr:eq(1) th:eq(" + aa + ")").find('select').val();

                            if (columnText == 'Error(s)') {
                                $("#" + appt_search.results_table + " thead tr:eq(1) th:eq(" + aa + ")").html('');
                                var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="" disabled>No  '+columnText+'</option></select>')
                                        .appendTo($("#" + appt_search.results_table + " thead tr:eq(1) th:eq(" + aa + ")")/*$("#" + pastClaims.resultsTable + " thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                                        .on('change', function () {
                                            var val = $.fn.dataTable.util.escapeRegex(
                                                    $(this).val()
                                                    );
                                            val =  val.replace(/\\/gi, "");
                                            column
                                                    .search(val ? val : '', true, false)
                                                    .draw();
                                        });

                                column.data().unique().sort().each(function (d, j) {
                                    d = $(d).text();
                                     if(d != '') {
                                               select.append('<option value="' + d + '">' + d + '</option>');
                                               }
                                });
                            } else if (columnText == 'Actions') {
                                $("#" + appt_search.results_table + " thead tr:eq(1) th:eq(" + aa + ")").html('');
                            } else if (aa != 0) {
                                $("#" + appt_search.results_table + " thead tr:eq(1) th:eq(" + aa + ")").html('');

                                var select = $('<select class="selectRsltTbl"><option value=""></option><option value="">All</option><option value="^$">No '+columnText+'</option></select>')
                                        .appendTo($("#" + appt_search.results_table + " thead tr:eq(1) th:eq(" + aa + ")")/*$("#" + pastClaims.resultsTable + " thead tr:nth-child(2)")*/ /*$(column.header())[1].column*/)
                                        .on('change', function () {
                                            var val = $.fn.dataTable.util.escapeRegex(
                                                    $(this).val().replace(/<\/?[^>]+(>|$)/g, "")
                                                    );
                                            val =  val.replace(/\\/gi, "");
                                            column
                                                    .search(val ? val : '', true, false)
                                                    .draw();
                                        });
                                var chkarr = [];
                                column.data().unique().sort().each(function (d, j) {
                                    d = d.replace(/<\/?[^>]+(>|$)/g, "");
                                    if ($.inArray(d, chkarr) < 0) {
                                        var selected = (selectedSearch == d) ? 'selected' : '';
                                        if(d != '') {
                                        select.append('<option value="' + d + '" '+selected+'>' + d + '</option>');
                                        }
                                        chkarr.push(d);
                                    }

                                });
                            }
                            aa++;
                        });
//                        if(pastClaims.maxPages == pastClaims.dtCurrentPage){                   

//                        }
                        $(".selectRsltTbl").select2({
                            placeholder: "Search",
                            allowClear: true,
                            dropdownAutoWidth: true,
                            width: '98%'
                        });
                        $('#' + appt_search.results_table + ' .select2-arrow').hide();
                        appt_search.dtCurrentPage++;
                        //appt_search.afterDtDataFinished();
                        appt_search.updateProgressBar();
//                        if(pastClaims.resetAll == false){
//                            pastClaims.request.abort();
//                            quickClaim.ajaxLocked = false;
//                            datatable.clear().draw();
//                            
//                            pastClaims.processDataTableData(datatable);
//                        }else{
                        $.fn.dataTableExt.oSort["dateField-desc"] = function (x, y) {
                            var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                            var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                            return (a.diff(b));
                        };

                        $.fn.dataTableExt.oSort["dateField-asc"] = function (x, y) {
                            var a = moment(x, global_js.getNewDateFormat().toUpperCase());
                            var b = moment(y, global_js.getNewDateFormat().toUpperCase());
                            return (b.diff(a));
                        }
                       // appt_search.results.buildErrorTooltips();
                        appt_search.processDataTableData(datatable);
//                        }                       
                    }
                },
                beforeSend: function () {
                    if (appt_search.request != null) {
                        appt_search.request.abort();
                    }
                },
                done: function () {
                    if (appt_search.resetAll == true) {
                        appt_search.request.abort();

                    }
                }
            })
        }
                                                                                                                                            
    },
    actionButtonCreate:function(data,header)
    {
        buttons = '<div class="btn-group TextWhite">';
        //['edit', 'cancel', 'reschedule', 'reverse', 'claim', 'profile'],
        buttons += '<a data-action="edit" class="btn btn-primary btnBulkAction">' +
                '<i class="clip-pencil-2"></i> Edit' +
                '</a>';
        buttons += '<a data-action="cancel" class="btn btn-primary btnBulkAction" href="javascript:void(0);">' +
                '<i class="clip-cancel-circle-2"></i> Cancel' +
                '</a>';
        buttons += '<a class="btn btn-primary btnBulkAction" data-action="reschedule" href="javascript:void(0);">' +
                '<i class="clip-clock-2"></i> Reschedule' +
                '</a>';

        if($('#appointment_search_appointment_date_start').val() == $('#appointment_search_appointment_date_end').val()) {
            var grouprescheduleDisabled = '';
        } else {
            var grouprescheduleDisabled = 'disabled="disabled"';
        }

        buttons += '<a class="btn btn-primary btnBulkAction" '+grouprescheduleDisabled+' data-action="groupreschedule" href="javascript:void(0);">' +
                '<i class="clip-clock-2"></i> Group Reschedule' +
                '</a>';

        buttons += '<a data-action="reverse" class="btn btn-primary btnBulkAction" href="javascript:void(0);">' +
                '<i class="clip-transfer"></i> Reverse' +
                '</a>';

        buttons += '<a data-action="forward" data-action="forward" class="btn btn-primary btnBulkAction" href="javascript:void(0);">' +
                '<i class="clip-transfer"></i> Forward' +
                '</a>';

        buttons += '<a data-action="claim" class="btn btn-primary btnBulkAction" href="javascript:void(0);">' +
                '<i class="fa-times-circle-o fa"></i> Claim' +
                '</a>';

        buttons += '</div>';



        console.log('header',header);
        //if(rs.header[index]){
        if (header.hasOwnProperty('appointment_actions')) {
        // setTimeout(function () {
        //alert($('.actionButtonsDiv').length);
        //if (!$(buttons).find(".actionButtonsDiv").length) {
        $(buttons).appendTo($('.actionButtonsDiv'));
        // }
        $(".btnBulkAction").on('click', function () {
        appt_search.handleBulkAction($(this).attr('data-action'));
        })
        appt_search.enableBulkAction();
        // }, 1000);
        }
        //}

        // bulk action all event handle
        $(".bulkaction_all").click(function () {
            if ($(this).attr('checked')) {
                $("tbody input[type='checkbox']").attr('checked', true);
                $(".btnBulkAction").attr('disabled', false);
            } else {
                $("tbody input[type='checkbox']").attr('checked', false);
                $(".btnBulkAction").attr('disabled', true);
            }
        });



    },
    buildDataTableRow: function (data, header) {
        var row_id = appt_search.results_row_prefix + data['appointment_id'] + '_' + data['appointment_attendee_id'] + '_' + data['patient_id'];
        data['appointment_attendee_id'] = data['appointment_attendee_id'].replace('no_appointment_attendee', 'noapptattendee');
        var ids = data['appointment_id'] + '_' + data['appointment_attendee_id'] + '_' + data['patient_id'];

        var action_columns = {};
        var actions_count = 0;
        var max_colspan = 0;
        var results_count = 0;
         var row = '';
        
        if (data.hasOwnProperty('appointment_actions')) {
            for (var action in data['appointment_actions']) {
                if (!action_columns.hasOwnProperty(action)) {
                    action_columns[action] = data['appointment_actions'][action];
                    actions_count++;
                }
            }
        }
        appt_search.action_columns = action_columns;
        var html = [];
        var newColumnsOrder = $('select[name="appointment_search[report_columns][]"]').val();
        for (var column in header) {
            var text = data[column];
            if (column == 'appointment_actions') {
                for (var action in this.results_actions) {
                    if (appt_search.action_columns.hasOwnProperty(this.results_actions[action])) {
                        if (data[column].hasOwnProperty(this.results_actions[action])) {
                            html.push(appt_search.action_columns[this.results_actions[action]]);
                        }
                    }
                }
            }
        }
        var dataattr = html.toString();

        var appointment_id = data.appointment_id;
        var id = data.appointment_id;
        var rowData = [];

        rowData.push('<label class="CHKcontainer"><input data-actions="' + dataattr + '" type="checkbox" data-attndId="' + data['appointment_attendee_id'] + '" data-apptId="' + data['appointment_id'] + '" class="bulkAction" id="bulkaction_' + ids + '"  value="all" checked="checked" /><span class="CHKcheckmark"></span>');

        appt_search.data[appointment_id] = {};
        appt_search.data[appointment_id]['claim_id'] = 55;
        appt_search.data[appointment_id]['patient_id'] = 22;
        appt_search.data[appointment_id]['item_count'] = 51;
           //rowData.push('<input type="hidden"  data-item_id="' + id + '" class="claim_id" value="' + data.claim_id + '"/>');

            for (var a = 0; a < appt_search.columns.length; a++) {
            var className = appt_search.columns[a];
            className = className.replace(' ', '_');
            if (className == 'errors') {
                var e = data['errors'];
                title += '\n';
                if (appt_search.explCodes.hasOwnProperty(e)) {
                        title += appt_search.explCodes[e];
                        if (appt_search.hlx8lines[a]) {
                            var msg = appt_search.hlx8lines[a];
                        } else {
                            var msg = '';
                        }
                        if (msg != '') {
                            title += ' - Error Code Message: ' + msg;
                            }
                    } else {
                        title += 'Unknown error code';
                    }
                
                title = title.replace('?-&gt;', '').replace('&lt;-?', '').replace(/\HX8/g, '');
                var dataError = data['errors'];
                var errs = dataError.split(", ");
                var err = '';
                if(errs.length > 1){
                    $.each(errs, function( index, value ) {
                        var title = appt_search.getErrorTitle(value,a);
                        err += value != '' ? '<span id="errorTitle_' + id + '" data-errorCode="' + value + '" class="' + className + '" title="'+title+'">' + value + '</span>,' : '';
                    });
                    
                }else{
                    var title = appt_search.getErrorTitle(e,a);
                    var err = data['errors'] != '' ? '<span id="errorTitle_' + id + '" data-errorCode="' + data['errors'] + '" class="' + className + '" title="'+title+'">' + data['errors'] + '</span>' : '';
                }
                rowData.push(err);
                if (data['errors']) {
                    appt_search.errors[id] = data['errors'];
                }
            } else {
               /* console.log(pastClaims.explCodes);
                    console.log(e);
                    console.log(data);*/
                if (data[className]) {
                    var text = data[newColumnsOrder[a]];
                    if(className == 'appointment_actions') {
                       row = appt_search.buildStagesCell(data.stage_client_id, 'update-stage_' + ids);//+ '</td>';
                       for (var action in this.results_actions) {//cancel', 'reschedule', 'reverse', 'claim
                            if (this.results_actions[action] != 'edit' && this.results_actions[action] != 'cancel' && this.results_actions[action] != 'reschedule' && this.results_actions[action] != 'groupreschedule' && this.results_actions[action] != 'reverse' && this.results_actions[action] != 'forward' && this.results_actions[action] != 'claim') {
                                if (appt_search.action_columns.hasOwnProperty(this.results_actions[action])) {
                                   if (data[newColumnsOrder[a]].hasOwnProperty(this.results_actions[action])) {
                                     row += //'<td class="' + this.results_actions[action] + '">'
                                                /*+*/ '<input type="button" onclick="appt_search.appointmentActions(this.id)" class="btn btn-blue sk2mar" id="' + this.results_actions[action] + '_' + ids + '" value="' + appt_search.action_columns[this.results_actions[action]] + '" />';
                                        // + '</td>';
                                   } else {
                                     row += '';
                                   }
                                }
                            }

                      }



                      if(data['doctor'] || $('#appointment_search_report_columns_doctor_code').is(':checked')){
               
                            if(data['doctor']) {
                                  var doctorCode = "";
                                  // if(data['doctor']) {
                                  var billingDoctor = data['doctor'].split(' ');                
                                  if(billingDoctor[0] ==  'MR.'){
                                        $.each(billingDoctor, function(index, value){ 
                                            if(index>0){
                                                doctorCode += value.substr(0,1);
                                            }
                                        });
                                  } 
                                  else
                                  {
                                        doctorCode = billingDoctor[0];
                                  }
                            }
                            else if(!data['doctor'] || $('#appointment_search_report_columns_doctor_code').is(':checked'))
                            {
                                    doctorCode = data['doctor_code'];
                            }
                      

                            //alert(doctorCode);
                            //row += '<a href="'+appt_search.actions.claim+'?id='+data['appointment_attendee_id']+'" class="btn btn-success" target="_blank">Claim</a>';
                            // row += '<a href="'+appt_search.actions.claim+'?id='+data['appointment_attendee_id']+'/'+billingDoctor[0]+'" class="btn btn-success" target="_blank">Claim</a>';
                            row += '<a href="'+appt_search.actions.claim+'?id='+doctorCode+'&patient_id='+data['patient_id']+'&appointment_id='+data['appointment_id']+'#new_tab" class="btn btn-success" target="_blank">Claim</a>';
                            // row += '<input type="button" id="billingDoctorClaim_'+ids+'" class="btn btn-success" value="Claim" />';
                        } else {
                            row += '';
                        }
                      rowData.push(row);
                    }
                    else
                    {
                      var title = typeof data[className] != 'undefined' ? data[className].toString().replace('_', ' ') : '';
                      rowData.push(title.replace(/<\/?[^>]+(>|$)/g, ""));    
                    }
                } 
                   // rowData.push(row);
                else {


                    if (className == 'fee_subm') {
                        var title = '$0';
                    } 
                    // else {
                    //     var title = '';
                    // }
                    else if (className == 'fee_paid') {
                        var title = '$0';
                    } else {
                        var title = '';
                    }
                     rowData.push(title.replace(/<\/?[^>]+(>|$)/g, ""));    
                   
                }
               
                
            }
        }
        return rowData;
    },
    appointmentActions:function(rowid){
            var terms = rowid.split('_');
            var action = terms[0];
            var appointment_id = terms[1];
            var attendee_id = terms[2];
            var patient_id = terms[3];
            if (action == 'bulkaction') {
                if ($('tbody input[type="checkbox"]:checked').length == 0) {
                    $(".btnBulkAction").attr('disabled', true);
                } else {
                    $(".btnBulkAction").attr('disabled', false);
                }
                if ($('tbody input[type="checkbox"]:checked').length != $('tbody input[type="checkbox"]').length) {
                    $(".bulkaction_all").attr('checked', false);
                } else {
                    $(".bulkaction_all").attr('checked', true);
                }

            } else if (action == 'claim') {
                var url = appt_search.actions.claim + '/index/appointment_attendee_id/' + attendee_id;
                window.location = url;
            } else if (action == 'update-stage') {
                appt_search.handleUpdateStageClick(appointment_id, attendee_id, patient_id);
                $('#find_button').trigger('click');
            } else if (action == 'profile') {
                var url = appt_search.actions.profile + '/index?id=' + patient_id;
//                window.location = url;
                window.open(url, '_blank');

            } else {
                $('#appointment_book_patient_id').val(patient_id);
                $('#appointment_book_appointment_id').val(appointment_id);
                $('#appointment_book_mode').val(action);
                $('#appointment_book_form').submit();
            }
    },
    updateProgressBar: function () {
        var currentPage = appt_search.dtCurrentPage;
        var maxPages = appt_search.maxPages;
        currentPage--;
        var percent = (currentPage / maxPages) * 100;
        $(".ProgressBarMain").css('width', percent + '%');
        if (percent == 100) {
            appt_search.isBckgrndActive = false;
            $(".ProgressBarMain").removeClass('sk-blue');
            $(".ProgressBarMain").addClass('sk-green');
        } else {
            $(".ProgressBarMain").removeClass('sk-green');
            $(".ProgressBarMain").addClass('sk-blue');
        }
    },
    handleFindClick: function () {
        if (appt_search.isBckgrndActive == true) {
            appt_search.resetAll = true;
        }
        if (appt_search.resetAll == true) {
            appt_search.dtCurrentPage = 1;
            appt_search.dtCurrentPage = 1;
            if ($.fn.DataTable.isDataTable("#" + appt_search.results_table)) {
                //var datatable = $("#" + pastClaims.resultsTable).DataTable();
                //datatable.clear().draw();
                $("#" + appt_search.results_table).dataTable().fnDestroy();
                $("#" + appt_search.results_table).html('');
            }
            $(".ProgressBarMain").css('width', '0%');
            //astClaims.processDataTableData();
            //return;
        }
        var doctors = localStorage.getItem('doctors');
        var locations = localStorage.getItem('locations');
        localStorage.clear();
        localStorage.setItem('doctors',doctors);
        localStorage.setItem('locations',locations);
        //appt_search.form.clearErrors();
        appt_search.initDatatable();
        appt_search.isBckgrndActive = true;






        // sk.pdf.alreadySpliced = false;
        // $("#apptSearchErrors").hide();
        // $("#service_based").parent('.tab-content').attr('style', 'border-color:#ddd');
        // $('a[href="#service_based"]').attr('style', 'border-left-color:#ddd;border-right-color:#ddd;');
        // var newColumnsOrder = $('select[name="appointment_search[report_columns][]"]').val();

        // if (newColumnsOrder == null) {

        //     $("#apptSearchErrors").html('Please select report column').addClass('alert alert-danger').show();
        //     $("#service_based").parent('.tab-content').attr('style', 'border-color:#BD1431');
        //     $('a[href="#service_based"]').attr('style', 'border-left-color:#BD1431;border-right-color:#BD1431');
        //     $("#tabs").tabs("option", "active", 1);
        //     return false;
        // }

        // if (appt_search.ajax_running) {
        //     return;
        // }

        // data = $('#' + appt_search.form_id).serialize();
        // appt_search.clearErrors();

        // $.ajax({
        //     type: 'post',
        //     dataType: 'json',
        //     type: 'post',
        //     url: appt_search.actions.search,
        //     data: data,
        //     success: function (rs) {
        //         if (rs.hasOwnProperty('errors')) {
        //             quickClaim.showFormErrors(rs.errors, '#' + appt_search.form_error_div, '#' + appt_search.form_id, true);

        //             $('#' + appt_search.messages.error).show();
        //             $('#' + appt_search.messages.text).text('The form contains invalid data.').show();
        //         } else if (rs.hasOwnProperty('data')) {

        //             s = rs.header;
        //             var out = Object.keys(s).map(function(data){
        //                 return [data,s[data]];
        //             });

        //             var a = [];
        //             for ( var i = 0; i < out.length; i++ ) {
        //                 a.push( out[ i ][0] );
        //             }
        //             appt_search.updateDefaultData(a.toString());

        //             appt_search.fillResultsTable(rs);
        //             $('#tabs').tabs('enable', 1);
        //             $('#tabs').tabs('option', 'active', 1);
        //         }
        //     },
        //     beforeSend: function () {
        //         appt_search.ajax_running = true;
        //         $('#messages .icons div').hide();
        //         $('#' + appt_search.messages.spinner).show();
        //         $('#' + appt_search.messages.text).text('Searching for appointments').show();
        //     },
        //     complete: function () {
        //         appt_search.ajax_running = false;
        //         $('#' + appt_search.messages.spinner).hide();
        //     },
        //     error: function () {
        //         $('#' + appt_search.messages.error).show();
        //         $('#' + appt_search.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
        //     }

        // });
    },
    updateDefaultData: function (data = null) {
        if (data) {
            $.ajax({
                type: 'POST',
                url: '/appointment_search/savereportparams',
                data: {type: "appointment_search_default_columns", data: data},
                success: function (rs) {
                    
                }
            })
    }
    },
    handlePdfClick: function () {
        if (appt_search.ajax_running) {
            return;
        }

        appt_search.clearErrors();
        data = $('#' + appt_search.form_id).serialize();
        data += '&mode=pdf';

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: appt_search.actions.search,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#' + appt_search.form_error_div, '#' + appt_search.form_id, true);

                    $('#' + appt_search.messages.error).show();
                    $('#' + appt_search.messages.text).text('The form contains invalid data.').show();
                } else {
                    appt_search.handlePdfRequest(data);
                }
            },
            beforeSend: function () {
                appt_search.ajax_running = true;
                $('#messages .icons div').hide();
                $('#' + appt_search.messages.spinner).show();
                $('#' + appt_search.messages.text).text('Building PDF').show();
            },
            complete: function () {
                appt_search.ajax_running = false;
                $('#' + appt_search.messages.spinner).hide();
            },
            error: function () {
                $('#' + appt_search.messages.error).show();
                $('#' + appt_search.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });

    },
    handlePdfRequest: function (data) {
        var fields = appt_search.splitFormData(data);
        appt_search.setupIframe();
        appt_search.setupHiddenForm(fields, appt_search.actions.pdf, 'pdf');
    },
    handleUpdateStageClick: function (appointment_id, attendee_id, patient_id) {
        var stage_client_id = $('#select_update-stage_' + appointment_id + '_' + attendee_id + '_' + patient_id).val();
        var params = 'appointment[id]=' + appointment_id
                + '&appointment[stage_client_id]=' + stage_client_id
                + '&appointment[source]=appointment_search';
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: appt_search.actions.stage_update,
            data: params,
            success: function (rs) {
                if (rs.hasOwnProperty('data')) {
                     $("#find_button").trigger('click');
                    // for (var a in rs.data) {
                    //     appt_search.addResultsTableRow(rs.data[a], appt_search.header);
                    // }
                }
            },
            beforeSend: function () {
                $('#' + appt_search.messages.checkmark).hide();
                $('#' + appt_search.messages.error).hide();
                $('#' + appt_search.messages.spinner).show();

                $('#' + appt_search.messages.text).text('Updating Appointment').show();
            },
            complete: function () {
                $('#' + appt_search.messages.spinner).hide();
            },
            error: function () {
                $('#' + appt_search.messages.error).show();
                $('#' + appt_search.messages.text).text('An error occurred while contacting the server.');
            }
        });
    },
    setupIframe: function () {
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    setupHiddenForm: function (fields, action, prefix) {
        setTimeout(function () {
            var frame_body = $('#hiddenDownloader').contents().find('body');
            var form = '<form id="hidden_form" action="' + action + '" method="POST">';

            for (var a in fields) {
                var name = fields[a].substring(0, fields[a].indexOf('='));
                var value = fields[a].substring(fields[a].indexOf('=') + 1);

                name = name.replace(appt_search.form_prefix, prefix);
                form += '<input type="text" value="' + value + '" id="' + name + '" name="' + name + '" />';
            }
            form += '</form>';
            frame_body.append($(form));
            $('#hiddenDownloader').contents().find('#hidden_form').submit();
        });
    },
    splitFormData: function (data) {
        data = data.replace(/%5B/g, '[').replace(/%5D/g, ']');
        return data.split('&');
    },
    toggleAll: function (type, value = 'all') {

         //var count = $('#srchCols .' + type + ' li:visible input:checked').size();
        var count = $('#srchCols .' + type + ' li input:checked').size();
        if (value == 'all' && $('#' + this.form_id + ' div.' + type + ' input[value=all]').prop('checked')) {
            $('#' + this.form_id + ' div.' + type + ' input:checked').each(function () {
                if ($(this).val() != 'all') {
                    $(this).prop('checked', false);
                }
            });
        } else if (count == 0) {
            $('#' + this.form_id + ' div.' + type + ' input[value=all]').prop('checked', true);
        } else if (count > 1) {
            $('#' + this.form_id + ' div.' + type + ' input[value=all]').prop('checked', false);
    }
    },
    toggleProvinceRegions: function () {
        if (this.use_provinces) {
            if ($('#srchCols .province input[value=all]').prop('checked')) {
                $('#' + this.form_id + ' div.region li').show();
            } else {
                $('#' + this.form_id + ' div.region li').hide();
                $('#' + this.form_id + ' div.region input[value=all]').parent('li').show();
                $('#' + this.form_id + ' div.province input:checked').each(function () {
                    var val = $(this).val();

                    if (val && appt_search.province_regions.hasOwnProperty(val)) {
                        for (var a in appt_search.province_regions[val]) {
                            $('#' + appt_search.form_prefix + '_region_id_' + appt_search.province_regions[val][a]).parent('li').show();
                        }
                    }
                });
            }
            appt_search.toggleAll('province');
        }

        appt_search.toggleAll('region');
        appt_search.toggleAll('location');
        appt_search.toggleAll('doctor');
        appt_search.toggleRegionLocations();
    },
    toggleRegionLocations: function () {
        if (this.use_regions) {
            $('#' + this.form_id + ' div.location li').hide();
            $('#' + this.form_id + ' div.location input[value=all]').parent('li').show();

            var show_all = true;
            if (this.use_regions) {
                show_all = show_all && $('#' + this.form_id + ' div.region input[value=all]:checked').size();
            }
            if (this.use_provinces) {
                show_all = show_all && $('#' + this.form_id + ' div.province input[value=all]:checked').size();
            }

            if (show_all) {
                $('#' + this.form_id + ' div.location li').show();
            } else if ($('#' + this.form_id + ' div.region input[value=all]:checked').size()) {
                $('#' + this.form_id + ' div.region li:visible input').each(function () {
                    var region_id = $(this).attr('value');

                    if (appt_search.region_locations.hasOwnProperty(region_id)) {
                        for (var a in appt_search.region_locations[region_id]) {
                            $('#' + appt_search.form_prefix + '_location_id_' + appt_search.region_locations[region_id][a]).parent('li').show();
                        }
                    }
                });
            } else {
                $('#' + this.form_id + ' div.region li:visible input:checked').each(function () {
                    var region_id = $(this).attr('value');
                    if (appt_search.region_locations.hasOwnProperty(region_id)) {
                        for (var a in appt_search.region_locations[region_id]) {
                            $('#' + appt_search.form_prefix + '_location_id_' + appt_search.region_locations[region_id][a]).parent('li').show();
                        }
                    }
                });
            }
        }

        appt_search.toggleAll('region');
        appt_search.toggleAll('location');
        appt_search.toggleAll('doctor');
        appt_search.toggleLocationDoctors();

    },
    toggleLocationDoctors: function () {
        $('#' + this.form_id + ' div.doctor li').hide();
        $('#' + this.form_id + ' div.doctor input[value=all]').parent('li').show();

        var show_all = $('#' + this.form_id + ' div.location input[value=all]:checked').size() && true;
        if (this.use_provinces) {
            show_all = show_all && $('#' + this.form_id + ' div.province input[value=all]:checked').size();
        }
        if (this.use_regions) {
            show_all = show_all && $('#' + this.form_id + ' div.region input[value=all]:checked').size();
        }

        if (show_all) {
            $('#' + this.form_id + ' div.doctor li').show();
        } else if ($('#' + this.form_id + ' div.location input[value=all]:checked').size()) {
            $('#' + this.form_id + ' div.location li:visible input').each(function () {
                var doctor_id = $(this).attr('value');
                if (appt_search.location_doctors.hasOwnProperty(doctor_id)) {
                    for (var a in appt_search.location_doctors[doctor_id]) {
                        $('#' + appt_search.form_prefix + '_doctor_id_' + appt_search.location_doctors[doctor_id][a]).parent('li').show();
                    }
                }
            });
        } else {
            $('#' + this.form_id + ' div.location li:visible input:checked').each(function () {
                var doctor_id = $(this).attr('value');

                if (appt_search.location_doctors.hasOwnProperty(doctor_id)) {
                    for (var a in appt_search.location_doctors[doctor_id]) {
                        $('#' + appt_search.form_prefix + '_doctor_id_' + appt_search.location_doctors[doctor_id][a]).parent('li').show();
                    }
                }
            });
        }

        appt_search.toggleAll('doctor');
    },
    togglePDFOptions: function () {
        if ($('#pdf_options_toggle').hasClass('ui-icon-plusthick')) {
            $('#pdf_options_toggle').removeClass('ui-icon-plusthick');
            $('#pdf_options_toggle').addClass('ui-icon-minusthick');
            $('#pdf_options').show();
        } else {
            $('#pdf_options_toggle').removeClass('ui-icon-minusthick');
            $('#pdf_options_toggle').addClass('ui-icon-plusthick');
            $('#pdf_options').hide();
        }
    },
    updatePdfOptionsSubheader: function () {
        if ($('#' + appt_search.form_prefix + '_pdf_option_show_subheader').prop('checked')) {
            $('.pdf_option_subheader').show();
        } else {
            $('.pdf_option_subheader').hide();
        }
    },
    updateLabel: function (findfor) {
        for (var i in findfor) {
            var text = $("label[for='" + findfor[i] + "']").text();
            text = text.replace('[', '<span class="otherTextsk">[').replace(']', ']</span>');
            $("label[for='" + findfor[i] + "']").html(text);
        }

    },
    getChildOptions: function (type) {
        var data = [];
        $("." + type + " input").each(function (e) {
            var val = $(this).val();
            var text = $(this).parent().find('label').text();
            if (val != 'all') {
                data.push({id: val, text: text});
            }
        })
        return data;
    },
    initSearchSelect: function () {
        var allProvince = appt_search.getChildOptions('province');
        var allRegion = appt_search.getChildOptions('region');
        var allLocation = appt_search.getChildOptions('location');
        var allDoctor = appt_search.getChildOptions('doctor');
        var allApptType = appt_search.getChildOptions('appointment_type');
        var allSlotType = appt_search.getChildOptions('slot_type');
        var allStatus = appt_search.getChildOptions('appointment_status');
        var ALL_OPTIONS = [
            {
                id: '1all',
                text: 'Province - all',
                children: allProvince
            },
            {
                id: '2all',
                text: 'Region - all',
                children: allRegion
            },
            {
                id: '3all',
                text: 'Location - all',
                children: allLocation
            },
            {
                id: '4all',
                text: 'Doctor - all',
                children: allDoctor
            },
            {
                id: '5all',
                text: 'Appt Type - all',
                children: allApptType
            },
            {
                id: '6all',
                text: 'Appt Category - all',
                children: allSlotType
            },
            {
                id: '7all',
                text: 'Appt Status - all',
                children: allStatus
            }
        ];
        var select2options = $('.searchSelect').select2({
            multiple: true,
            placeholder: "Select search options...",
            data: ALL_OPTIONS,
            width: '100%'
        })


        select2options.select2('val', ["1all", "2all", "3all", "4all", "5all", "6all", "7all"]);

        select2options.on('change', function (e) {
            if (e.added) {
                var chkAll = e.added.id;
                chkAll = chkAll.substr(1);

                if (chkAll == 'all') {
                    var type = e.added.text.replace(' - all', '').toLowerCase();
                    $("." + type + " input[value='all']").attr('checked', true);//.trigger('click');
                    appt_search.toggleAll(type, 'all');
                    appt_search.removeOtherData(type, e.added.id);
                } else {
                    var currentSelected = $("input[value='" + e.added.id + "']").attr('checked', true);//.trigger('click');                    
                    var type = $("input[value='" + e.added.id + "']").parents();
                    type = type[3].classList[0];
                    appt_search.toggleAll(type, currentSelected.val());
                }
            } else if (e.removed) {
                var chkAll = e.removed.id;
                chkAll = chkAll.substr(1);
                if (chkAll == 'all') {
                    if (appt_search.chkOtherIsCheckedOrNot(type)) {

                    } else {
                        var typearr = {};
                        typearr['1all'] = 'province';
                        typearr['2all'] = 'region';
                        typearr['3all'] = 'location';
                        typearr['4all'] = 'doctor';
                        typearr['5all'] = 'appointment_type';
                        typearr['6all'] = 'slot_type';
                        typearr['7all'] = 'appointment_status';
                        appt_search.selectAllOption(typearr[e.removed.id]);
                    }
                } else {
                    var currentSelected = $("input[value='" + e.removed.id + "']").attr('checked', false);//.trigger('click');                    
                    var type = $("input[value='" + e.removed.id + "']").parents();
                    type = type[3].classList[0];

                    if (appt_search.chkOtherIsCheckedOrNot(type)) {
                        appt_search.toggleAll(type, currentSelected.val());
                    } else {
                        appt_search.selectAllOption(type);
                    }


                }
            }
            appt_search.toggleProvinceRegions();
            appt_search.toggleRegionLocations();
            appt_search.toggleLocationDoctors();
        });


        $("#srchCols input[type='checkbox']").change(function () {

            var searchSelect2data = [];
            $("#mainReportSection input[type='checkbox']").each(function () {
                if ($(this).prop('checked')) {
                    searchSelect2data.push($(this).val());
                }
            })


            var type = $(this).parents();
            type = type[3].classList[0];

            var value = $(this).val();


            if (value == 'all') {
                appt_search.removeOtherData(type, searchSelect2data);
            } else {
                appt_search.updateSelectData(searchSelect2data);
            }

        })


    },
    selectAllOption: function (type) {
        var oldData = $(".searchSelect").select2('val');
        var typearr = {};//{"province":"1all","region":"2all","location":"3all","doctor":"4all","appointment_type":"5all","slot_type":"6all","appointment_status":"7all"};
        typearr['province'] = '1all';
        typearr['region'] = '2all';
        typearr['location'] = '3all';
        typearr['doctor'] = '4all';
        typearr['appointment_type'] = '5all';
        typearr['slot_type'] = '6all';
        typearr['appointment_status'] = '7all';
        oldData.push(typearr[type]);
        appt_search.updateSelectData(oldData);
    },
    removeSingleOption: function (remove) {
        var oldData = $(".searchSelect").select2('val');
        for (var i in oldData) {
            if (oldData[i] == remove) {
                oldData.splice(i, 1);
            }
        }
        appt_search.updateSelectData(oldData);
    },
    chkOtherIsCheckedOrNot: function (type) {
        var rtn = false;
        $("." + type + " input[type='checkbox']").each(function () {
            if ($(this).val() != 'all' && $(this).attr('checked')) {
                rtn = true;
            }
        })
        return rtn;
    },
    updateSelectData: function (data) {
        $("#select2reports").select2('val', data);
    },
    removeOtherData: function (type, value) {
        var oldData = $(".searchSelect").select2('val');
        var typearr = {};//{"province":"1all","region":"2all","location":"3all","doctor":"4all","appointment_type":"5all","slot_type":"6all","appointment_status":"7all"};
        typearr['province'] = '1all';
        typearr['region'] = '2all';
        typearr['location'] = '3all';
        typearr['doctor'] = '4all';
        typearr['appointment_type'] = '5all';
        typearr['slot_type'] = '6all';
        typearr['appointment_status'] = '7all';
        oldData.push(typearr[type]);
        $("." + type + " input[type='checkbox']").each(function () {
            var value = $(this).val();
            if ($(this).val() != 'all') {
                oldData = oldData.filter(function (e) {
                    return e !== value
                })
            }
        })
        appt_search.updateSelectData(oldData);
    },
    enableBulkAction: function () {
        var edit = false;
        var cancel = false;
        var reshedule = false;
        var reverse = false;
        var claim = false;
        $("tbody input[type='checkbox']:checked").each(function () {
            var terms = $(this).attr('id').split('_');
            var action = terms[0];
            var appointment_id = terms[1];
            var attendee_id = terms[2];
            var patient_id = terms[3];
        })
    },
    handleBulkAction: function (action) {
        if (action != '') {
            var ids = [];
            var apptIds = [];
            var attend_ids = [];
            $(".bulkAction:checked").each(function () {
                var data = $(this).attr('data-actions');

                data = data.split(',');
                if ($.inArray(action, data) >= 0) {
                    ids.push($(this).attr('id'));
                    apptIds.push($(this).attr('data-apptid'));
                    attend_ids.push($(this).attr('data-attndId'));
                } else {
                    $(this).attr('checked', false);
                }
            })

            if (ids.length > 0) {
                    if (action == 'reverse') {
                            $('#' + appt_search.messages.spinner).show();
                            $('#' + appt_search.messages.text).text('Reversing Appointments').show();

                            $.ajax({
                                type: 'post',
                                dataType: 'json',
                                url: '/appointment_search/stageReverse',
                                data: {id: apptIds.join(',')},
                                success: function (rs) {
                                    $('#' + appt_search.messages.spinner).hide();
                                    $("#find_button").trigger('click');
                                },
                                error: function () {
                                    $('#' + appt_search.messages.spinner).hide();

                                }
                            });
                    } else if(action == 'forward') {
                            $('#' + appt_search.messages.spinner).show();
                            $('#' + appt_search.messages.text).text('Forwarding Appointments').show();
                            $.ajax({
                                type: 'post',
                                dataType: 'json',
                                url: '/appointment_search/StageForward',
                                data: {id: apptIds.join(',')},
                                success: function (rs) {
                                    $('#' + appt_search.messages.spinner).hide();
                                    $("#find_button").trigger('click');
                                },
                                error: function () {
                                    $('#' + appt_search.messages.spinner).hide();

                                }
                            });
                    } else if (action == 'claim') {
                        var url = appt_search.actions.claim + '?appointment_attendee_ids=' + attend_ids.join();
                        window.location = url;
                    } else {
                        $("#appointment_book_bulk_action_ids").val(ids.toString());
                        $("#appointment_book_mode_bulk").val(action);
                        $('#appointment_book_form_bulk').submit();
                    }


            }

        }
    }
}
