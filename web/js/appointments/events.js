var calendar_events = {
    actions: {
        claim: null,
        patient_profile: null,
        pull_events: null,
        consultation_cancel: null,
        consultation_reschedule: null,
        consultation_save: null,
        consultation_search: null,
        consultation_update_status: null,
        consultation_reset_status: null,
        doctors_appt_cancel: null,
        doctors_appt_reschedule: null,
        doctors_appt_save: null,
        doctors_appt_stage_update: null,
        doctors_appt_stage_reverse: null,
        create_patient: null,
        visit_sheet: null,
        get_appointments: null,
        pull_patient_phone: null
    },
    appointment_counts: {},
    appointment_types: {},
    appointment_types_services: {},
    calendar: 'calendar',
    drag_to_reschedule: false,
    override_rescheule: false,
    dragged_event: false,
    current_event: null,
    complex_appointment_types: false,
    initialized: false,
    last_check_time: null,
    last_requested_check: null,
    pull_phones_flag: false,
    timer: {
        refresh_time: 90000, // 90 seconds = 90000
        refresh_timeout: 3600000, // 60 minutes = 3600000
        check_time: 5000, // 5 seconds = 5000
        is_set: false
    },
    events: null,
    enable_appointment_counts: false,
    message: {
        checkmark_id: 'checkmark',
        error_id: 'error_icon',
        spinner_id: 'spinner',
        text_id: 'messages_text'
    },
    slot_types: {
        consultation: null,
        doctors_appointment: null,
        other: null
    },
    defaults: {
        draggable: function (calEvent, element) {
            return calendar_events.drag_to_reschedule;
        },
        resizable: function (calEvent, element) {
            return false;
        },
        defaultFreeBusy: {free: false},
        displayFreeBusys: true,
//		businessHours : {start: 6, end: 18, limitDisplay : false},
        daysToShow: 1,
        switchDisplay: {'1 day': '1', '3 days': '3', 'work week': '5', 'week': '7', 'month': '30'},
        headerSeparator: '  ',
        useShortDayNames: true,
        timeslotsPerHour: 4,
        timeslotHeight: 20,
        timeSeparator: '-',
        timeFormat: 'g:ia',
        alwaysShowEndTime: false,
        hideStartTime: false,
        timelineShowMinutes: true,
        allowCalEventOverlap: true,
        overlapEventsSeparate: true,
        defaultEventLength: 1,
        showAsSeparateUser: false,
        separateUsersDisplay: {'1': true, '3': true, '5': true, '7': true, '30': false},
        lastDate: $.cookie('current_date'),
        users: [],
        user_map: [],
        height: function ($calendar) {
            return $(window).height() - $('#header').outerHeight(true) - $('#title_bar').outerHeight(true) - 4;
        },
        data: function (start, end, callback) {
            calendar_events.pullData(start, end, callback);
        },
        eventClick: function (calEvent) {
            calendar_events.drawForm(calEvent, this.element);
        },
        eventNew: function (calEvent, $event, FreeBusyManager, calendar) {
            var isFree = true;
            $.each(FreeBusyManager.getFreeBusys(calEvent.start, calEvent.end), function () {
                if (this.getStart().getTime() != calEvent.end.getTime() && this.getEnd().getTime() != calEvent.start.getTime() && !this.getOption('free')) {
                    isFree = false;
                    return false;
                }
            });
 
            if (!isFree) {
                $(calendar).weekCalendar('removeEvent', calEvent.id);
                return false;
            }

            calEvent.location_id = calendar_events.defaults.user_map[calEvent.userId];
            calEvent.start_time = calEvent.start.toString('H:mm');
            calEvent.end_time = calEvent.end.toString('H:mm');
            calEvent.notes = '';
            calEvent.max_attendees = 1;

            calendar_events.drawForm(calEvent, calendar);
        },
        eventDrop: function (newEvent, oldEvent, $oldEvent) {
            if (!oldEvent.can_edit) {
                $('#calendar').weekCalendar('removeEvent', newEvent.id);
                $('#calendar').weekCalendar('updateEvent', oldEvent);
                return false;
            }

            var ok = false;
            var time_ok = false;

            var new_user_id = newEvent.userId;
            var user_info = calendar_events.defaults.user_map[new_user_id];

            var location_id = user_info.substr(0, user_info.indexOf('_'));
            var doctor_id = user_info.substr(user_info.indexOf('_') + 1);

            if (oldEvent.doctor_id == doctor_id) {
                ok = true;
            } else if (doctor_id == 'MISC' && oldEvent.doctor_id == '') {
                doctor_id = '';
                ok = true;
            } else if (doctor_id != 'MISC' && oldEvent.doctor_id != '') {
                ok = true;
            }

            // column classes must be OK
            // must be inside a feebusy block
            var fb = $('#calendar').weekCalendar('getFreeBusyManagerForEvent', newEvent);

            for (var a in fb.freeBusys) {
                var current_block = fb.freeBusys[a];
                if (current_block.isWithin(newEvent.start) && current_block.isWithin(newEvent.end) && current_block.options.free) {
                    time_ok = true;
                }
            }

            if (ok && time_ok) {
                newEvent.doctor_id = doctor_id;
                newEvent.location_id = location_id;
                calendar_events.dragged_event = true;
                calendar_events.current_event = oldEvent;
                calendar_events.doctors_appt.reschedule_event = calendar_events.current_event;
                calendar_events.doctors_appt.reschedule_appointment_id = oldEvent.id;
                $('#doctor_appt_reschedule_cancel_dialog_button').hide();
                newEvent.id = null;
                calendar_events.drawForm(newEvent, $('#calendar'));
            }

            $('#calendar').weekCalendar('removeEvent', newEvent.id);
            $('#calendar').weekCalendar('updateEvent', oldEvent);
            return false;
        },
        eventAfterRender: function (calEvent, element) {
            if (calEvent.slot_type == calendar_events.slot_types.consultation) {
                $(element).addClass('consultation-appt');
            } else {
                if (calEvent.appointment_type_id) {
                    if (calEvent.orphan) {
                        $(element).addClass('appt_type_orphan');
                    } else {
                        $(element).addClass('appt_type_' + calEvent.appointment_type_id);
                        $(element).addClass('appt_book_id_' + calEvent.patient_id);
                    }
                }
                $(element).removeClass('consultation-appt');
            }
            return element;
        },
        getUserId: function (user, index, calendar) {
            if (!user) {
                return index;
            }
            var id = jQuery.inArray(user, this.users);
            if (id != -1) {
                return id;
            }
            for (var a in calendar_events.defaults.user_map) {
                if (user == calendar_events.defaults.user_map[a]) {
                    return a;
                }
            }
            return index;
        },
        getUserNameFromUserMap: function (index) {
            return calendar_events.defaults.user_map[index];
        },
        eventHeader: function (calEvent, calendar) {
            var options = calendar.weekCalendar('option');

            if (options.hideStartTime) {
                return calEvent.title;
            } else if (options.alwaysShowEndTime) {
                return calendar.weekCalendar('formatDate', calEvent.start, options.timeFormat) + options.timeSeparator + calendar.weekCalendar('formatDate', calEvent.end, options.timeFormat) + ': ' + calEvent.title;
            }
            return calendar.weekCalendar('formatDate', calEvent.start, options.timeFormat) + ': ' + calEvent.title;
        },
        eventBody: function (calEvent, calendar) {
            return '';
        },
        changedate: function (calendar, date) {
            if (date) {
                $.cookie('last_used_date', $.cookie('current_date'));
                $.cookie('current_date', date.toString('yyyy-MM-dd'));
                $('#calendar').weekCalendar('option', 'lastDate', $.cookie('last_used_date'));
            }
        },
        getHeaderDate: function (date, calendar) {
            var short = $('#calendar').weekCalendar('option', 'useShortDayNames');
            var days = $('#calendar').weekCalendar('option', (short ? 'shortDays' : 'longDays'));
            var separator = $('#calendar').weekCalendar('option', 'headerSeparator');
            var formattedDate = $('#calendar').weekCalendar('formatDate', date);

            var notes = '';
            if (calendar_events.configuration.daily_notes.enabled) {
                var ymd = $('#calendar').weekCalendar('formatDate', date, 'Ymd');

                notes = ' <div class="date-message" id="' + ymd + '"></div> ';

                if (calendar_events.configuration.enabled) {
                    notes += '<i class="clip-settings wc-settings edit-date-message" style="font-size:18px" id="add-date-' + ymd + '"></i>';
                }
            }

            return '<div class="wc-date-header">' + days[date.getDay()] + separator + formattedDate + '</div>'
                    + notes;
        }
    },

    setupTimeline: function (patientid) {
        var url = 'patient_id=' + patientid;

        $.ajax({
            type: 'get',
            url: calendar_events.actions.get_appointments,
            dataType: 'json',
            data: url,
            success: function (rs) {
                var getStatus = function (status) {
                    var res;

                    status = parseInt(status);

                    switch (status) {
                        case 1:
                            res = 'PENDING';
                            break;
                        case 2:
                            res = 'BOOKED';
                            break;
                        case 3:
                            res = 'DELETED';
                            break;
                        case 4:
                            res = 'CANCELLED';
                            break;
                        case 5:
                            res = 'CANCELLED LOCAT';
                            break;
                        case 6:
                            res = 'RESCHEDULED';
                            break;
                        case 10:
                            res = 'ATTENDED';
                            break;

                        case 11:
                            res = 'NO SHOW';
                            break;
                        case 20:
                            res = 'BILLED';
                            break;
                    }

                    return res;
                }

                // create a dataset with items
                // we specify the type of the fields `start` and `end` here to be strings
                // containing an ISO date. The fields will be outputted as ISO dates
                // automatically getting data from the DataSet via items.get().
                var items = new vis.DataSet({
                    type: {start: 'ISODate', end: 'ISODate'}
                });

                //rs[0].Appointment.start_date
                var index = 1;
                rs.forEach(function (entry) {
                    var comparison = entry.Appointment.start_date.substring(0, 10);
                    var appt_date = new Date(comparison);
                    var current_selected_date = new Date(calendar_events.current_event.start);

                    if (appt_date.compareTo(current_selected_date) >= 0) {
                        items.add({id: index, style: 'color:white; background-color:red;', content: entry.Appointment.doctor_code + ', '
                                    + entry.Appointment.AppointmentType.name + ' - ' + getStatus(entry.Appointment.status) + '<br>' + (new Date(appt_date)).addDays(1).toDateString(), start: entry.Appointment.start_date});
                    } else {
                        items.add({id: index, content: entry.Appointment.doctor_code + ', '
                                    + entry.Appointment.AppointmentType.name + ' - ' + getStatus(entry.Appointment.status) + '<br>' + (new Date(appt_date)).addDays(1).toDateString(), start: entry.Appointment.start_date});
                    }

                    index++;
                });

                // log changes to the console
                items.on('*', function (event, properties) {
                });

                var container = document.getElementById('visualization');

                var dateObj = new Date();

                var options = {
                    start: new Date(),
//                    zoomMin: 136000,
//                    min: new Date(2016, 0, 1),
//                    max: new Date(2018, 0, 1),
                    height: '150px',

                    // allow selecting multiple items using ctrl+click, shift+click, or hold.
                    multiselect: false,

                    editable: false,
//                    repeat: 'daily',

                    showCurrentTime: true
                };
                var timeline = new vis.Timeline(container, items, options);

                timeline.setWindow(new Date(dateObj.getFullYear() - 1, dateObj.getMonth(), dateObj.getDate()), new Date(dateObj.getFullYear() + 1, dateObj.getMonth(), dateObj.getDate()));
//                timeline.moveTo(new Date());
            }
        });
    },

    initialize: function () {
        calendar_events.consultation.initialize();
        calendar_events.doctors_appt.initialize();
        calendar_events.configuration.initialize();

        $('#validateButton').click(function(){
            calendar_events.doctors_appt.hcvRequest($('#patient_data_row .hcn').text(), $('#patient_data_row .version_code').text(), $('#appointment_patient_id').val());
        });

        $('#calendar_header_cancel_button').button({
            icons: {
                primary: 'ui-icon-close'
            },
            text: false
        }).addClass('ui-button-inline');
        $('#calendar_header_cancel_button').click(function () {
            calendar_events.hideHeaderOverlay();

            calendar_events.doctors_appt.reschedule_appointment_id = null;
            calendar_events.doctors_appt.reschedule_event = null;
            calendar_events.consultation.reschedule_attendee = null;
            calendar_events.consultation.reschedule_event = null;
            calendar_events.doctors_appt.patient_from_profile = null;

            $('#doctor_appt_reschedule_form_dialog').dialog('close');
            $('#consultation_reschedule_form_dialog').dialog('close');
        });

        if (calendar_events.doctors_appt.edit_event) {
            calendar_events.drawForm(calendar_events.doctors_appt.edit_event, $('#calendar'));
        } else if (calendar_events.consultation.edit_appointment_id) {
            calendar_events.drawForm(calendar_events.consultation.edit_event, $('#calendar'));
            calendar_events.consultation.fillForm(calendar_events.consultation.edit_event, calendar_events.consultation.edit_attendee);
        }
        $('#id_appointment_search_patient').change(function(){
            //console.log('users_test',calendar_events.defaults.users);
            $('#calendar').weekCalendar('refresh');
            // setInterval(function(){
            //     $('.appt_book_id_'+$(this).val()).trigger('click');
            // },1000);
            $('.appt_book_id_'+$(this).val()).trigger('click');
            //$('.appt_book_id_'+$(this).val()).trigger('click');
        });
        var v = setTimeout("calendar_events.checkTimer()", calendar_events.timer.check_time);
    },
    pullData: function (s, e, c) {
        var dialogBox = $('#doctor_appointment_form_dialog');
        if (dialogBox.dialog("isOpen") === true) {
            return;
        }
        var params = calendar_sidebar.getSidebarParameters(s, e);
        calendar_events.last_check_time = new Date().getTime();
        if (calendar_events.timer.is_set == false) {
            var v = setTimeout("calendar_events.checkTimer()", calendar_events.timer.check_time);
        }

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: calendar_events.actions.pull_events,
            data: params,
            success: function (rs) { 
                if (rs.data.events) {
                    var appointment_types = [];
                    var patientSearchoptions = '<option value=" ">Select Patient</option>';
                    for (var event in rs.data.events) {
                        if(rs.data.events[event].patient_id !='' && rs.data.events[event].patient_id != null && rs.data.events[event].patient_id != undefined)
                        {
                            patientSearchoptions += '<option value="'+rs.data.events[event].patient_id+'">'+rs.data.events[event].patient_lname+', '+rs.data.events[event].patient_fname+'</option>';
                        }
                        if (!appointment_types[rs.data.events[event].appointment_type_id]) {
                            appointment_types[rs.data.events[event].appointment_type_id] = {};
                            appointment_types[rs.data.events[event].appointment_type_id]['count'] = 1;
                            appointment_types[rs.data.events[event].appointment_type_id]['str'] = rs.data.events[event].appointment_type_str;
                        } else {
                            appointment_types[rs.data.events[event].appointment_type_id]['count']++;
                        }
                    }
                    //calendar_events.consultation.drawForm(rs.data.events[0], $('#calendar'));
                    $('#id_appointment_search_patient').html(patientSearchoptions).trigger('change.select2');

                    var html = '';
                    $('#appointment_type_count').empty();
                    for (var type in appointment_types) {
                        html += '<b>' + appointment_types[type].str + '</b>: ' + appointment_types[type].count + '<br />';

                    }
                    $('#appointment_type_count').append(html);
                    $('#appointment_type_count').css("text-align","left");
                }


                var user_count = 0;
                if (rs.hasOwnProperty('users')) {
                    users = new Array();
                    user_map = new Array();
                    user_longnames = new Array();
                    for (var i in rs.users) {
                        users[user_count] = rs.users[i].username;
                        user_map[user_count] = i;
                        user_longnames[user_count] = rs.users[i].long_username;
                        user_count++;
                    }

                    calendar_events.defaults.users = users;
                    calendar_events.defaults.user_map = user_map;
                    calendar_events.user_longnames = user_longnames;
                    $('#calendar').weekCalendar('option', 'users', users);
                }

                calendar_events.appointment_counts = rs.data.appt_counts;
                e = rs.data.events ? rs.data.events : [];

                for (var a in e) {
                    rs.data.events[a].start = Date.parse(e[a].start);
                    rs.data.events[a].end = Date.parse(e[a].end);
                    rs.data.events[a].userId = calendar_events.defaults.getUserId(e[a].userId);
                }

                for (var a in rs.data.freebusys) {
                    rs.data.freebusys[a].start = Date.parse(rs.data.freebusys[a].start);
                    rs.data.freebusys[a].end = Date.parse(rs.data.freebusys[a].end);
                    rs.data.freebusys[a].userId = calendar_events.defaults.getUserId(rs.data.freebusys[a].userId);
                }
                c(rs.data);

                if (!user_count) {
                    $('#calendar').weekCalendar('option', 'defaultFreeBusy', {free: false});
                }

                calendar_events.consultation.fees = rs.consultation_fees;
                calendar_sidebar.consultation_dates = rs.consultation_counts;
                calendar_sidebar.doctor_dates = rs.doctor_slot_counts;
                $('#' + calendar_sidebar.minical).datepick('option', 'onDate', adminSidebarUpdateMinical);
                $('#' + calendar_events.message.text_id).hide();

                if (calendar_events.current_event != null) {
                    var id = calendar_events.current_event.id;

                    for (var a in e) {
                        if (id == e[a].id) {
                            calendar_events.drawForm(e[a], $('#calendar'));
                        }
                    }
                }
                calendar_events.events = rs.data.events;

                $('#' + calendar_events.message.text_id).text('').show();

                if (rs.hasOwnProperty('users')) {
                    calendar_events.setUserHovers(user_longnames);
                }
                if (calendar_events.configuration.daily_notes.enabled) {
                    calendar_events.setAppointmentBookMessages(rs.data.daily_messages, true);
                }
            },
            beforeSend: function () {
                $('#' + calendar_events.message.checkmark_id).hide();
                $('#' + calendar_events.message.error_id).hide();
                $('#' + calendar_events.message.spinner_id).show();

                $('#' + calendar_events.message.text_id).text('Requesting Data').show();
            },
            complete: function () {
                $('#' + calendar_events.message.spinner_id).hide();
            },
            error: function () {
                $('#' + calendar_events.message.error_id).show();
                $('#' + calendar_events.message.text_id).text('An error occurred while contacting the server.');
            }
        });
    },
    updateData: function (freebusys, events, daily_messages) {
        $('#calendar').weekCalendar('clear');

        for (var a in freebusys) {
            freebusys[a].userId = calendar_events.defaults.getUserId(freebusys[a].userId);
        }

        $('#calendar').weekCalendar('option', 'defaultFreeBusy', {free: false});
        $('#calendar').weekCalendar('updateFreeBusy', freebusys);

        for (var a in events) {
            events[a].start = Date.parse(events[a].start);
            events[a].end = Date.parse(events[a].end);
            events[a].userId = calendar_events.defaults.getUserId(events[a].userId);
            $('#calendar').weekCalendar('updateEvent', events[a]);
        }



        calendar_events.setUserAppointmentCounts(calendar_events.user_longnames);
        if (calendar_events.configuration.daily_notes.enabled) {
            calendar_events.setAppointmentBookMessages(daily_messages, false);
        }
    },
    checkTimer: function () {
        var now = new Date().getTime();
        if (now - calendar_events.last_check_time > calendar_events.timer.refresh_time) {
            $('#calendar').weekCalendar('refresh');
        }
        if (now - calendar_events.last_requested_check < calendar_events.timer.refresh_timeout) {
            var v = setTimeout("calendar_events.checkTimer()", calendar_events.timer.check_time);
            calendar_events.timer.is_set = true;
        } else {
            calendar_events.timer.is_set = false;
        }
    },
    setAppointmentBookMessages: function (messages, setupHandlers) {
        if (!calendar_events.configuration.daily_notes.enabled) {
            return;
        }

        if (setupHandlers) {
            calendar_events.configuration.daily_notes.td_height = $('.wc-time-column-header').height();

            $('.edit-date-message').click(function () {
                calendar_events.configuration.showConfigureDateDialog($(this).attr('id').replace('add-date-', ''));
            });
        }

        calendar_events.configuration.daily_notes.messages = {};
        for (var a in messages) {
            var date = messages[a].date.replace(/-/g, '');
            calendar_events.configuration.daily_notes.messages[date] = messages[a];

            calendar_events.configuration.daily_notes.displayMessage(date, messages[a].message);
        }

    },
    setUserAppointmentCounts: function (usernames) {
        if (!calendar_events.enable_appointment_counts) {
            return;
        }

        var appt_counts = calendar_events.appointment_counts;
        var date = $('#calendar').weekCalendar('getCurrentFirstDay');
        var days = $('#calendar').weekCalendar('option', 'daysToShow');

        for (var day = 1; day <= days; day++) {
            for (var a in usernames) {
                var loc_id = parseInt(calendar_events.defaults.user_map[a], 10);
                var doctor_id = calendar_events.defaults.users[a];
                var dstring = date.toString('yyyyMMdd');
                var key = doctor_id + '_' + loc_id + '_' + dstring;
                var count = 0;
                var types = '';

                if (appt_counts.hasOwnProperty(key)) {
                    count = appt_counts[key]['count'];

                    types += '<ul style="text-align:left;padding-left:5px">';
                    for (var x in appt_counts[key]['types']) {
                        types += '<li><b>' + calendar_events.appointment_types[x] + '</b>: ' + appt_counts[key]['types'][x] + '</li>';
                    }
                    types += '</ul>';
                }

                $('.wc-user-header.wc-user-' + a + '.wc-day-' + day + ' .appt_count').text('(' + count + ')');
                if (types) {
                    $('.wc-user-header.wc-user-' + a + '.wc-day-' + day + ' .appt_count').attr('title', types);
                    $('.wc-user-header.wc-user-' + a + '.wc-day-' + day + ' .appt_count').tooltip({
                        items: 'span',
                        content: function () {
                            return $(this).attr('title');
                        }
                    });
                }
            }

            date.setDate(date.getDate() + 1);
        }
    },
    setUserHovers: function (usernames) {
        var days = $('#calendar').weekCalendar('option', 'daysToShow');

        for (var day = 1; day <= days; day++) {
            for (var a in usernames) {
                var loc_id = parseInt(calendar_events.defaults.user_map[a], 10);

                $('.wc-user-header.wc-user-' + a + '.wc-day-' + day).html('<span class="doctor">' + $('.wc-user-header.wc-user-' + a + '.wc-day-' + day).text() + '</span><span class="appt_count"></span>');
                $('.wc-user-header.wc-user-' + a + '.wc-day-' + day + ' .doctor').attr('title', usernames[a]);
                $('.wc-user-header.wc-user-' + a + '.wc-day-' + day + ' .doctor').tooltip();

                $('td.wc-day-column div.wc-user-' + a + '.wc-day-' + day).parent('td').addClass('location_' + loc_id);
                $('td.wc-user-' + a).addClass('location_' + loc_id);
            }

        }
        if (calendar_events.enable_appointment_counts) {
            calendar_events.setUserAppointmentCounts(calendar_events.user_longnames);
        }
    },
    setViewList: function (views) {
        var rs = {};

        for (var a in calendar_events.defaults.switchDisplay) {
            if (jQuery.inArray(calendar_events.defaults.switchDisplay[a], views) != -1) {
                rs[a] = calendar_events.defaults.switchDisplay[a];
            }
        }

        calendar_events.defaults.switchDisplay = rs;
    },
    drawForm: function (calEvent, calendar) {

        if (!calEvent.id && calendar_events.consultation.reschedule_attendee) {
            alert('You are currently trying to reschedule a consultation.  Cannot create new events.');
            $(calendar).weekCalendar('removeEvent', calEvent.id);
            return false;
        } else if (calendar_events.current_event != null && calendar_events.doctors_appt.reschedule_appointment_id != null) {
        } else if (calendar_events.doctors_appt.patient_from_profile && calEvent.id) {
            alert('Cannot edit existing events while scheduling from a patient profile');
            return false;
        }

        if (!calEvent.id) {
            var user_name = isNaN(calEvent.userId) ? calEvent.userId : calendar_events.defaults.getUserNameFromUserMap(calEvent.userId);
            var location_id = null;
            var doctor_id = null;

            if (user_name.indexOf('_')) {
                var terms = user_name.split('_');
                location_id = terms[0];
                doctor_id = terms[1] == 'MISC' ? null : terms[1];
            }

            if (location_id && doctor_id) {
                calEvent.slot_type = calendar_events.slot_types.doctors_appointment;
            } else {
                calEvent.slot_type = calendar_events.slot_types.other;
            }
            calEvent.location_id = location_id;
            calEvent.doctor_id = doctor_id;

            if (calendar_events.defaults.nudgeEvents) {
                calendar_events.nudgeEventDown(calEvent);
            }
        }

        if (calendar_events.doctors_appt.reschedule_appointment_id && calEvent.slot_type == calendar_events.slot_types.other) {
            if (calEvent.doctor_id) {
                alert("You cannot reschedule a doctor's appointment into the MISC column.");
            } else {
                alert("Rescheduling Non-Doctor Appointments is currently unsupported.");
            }
            $(calendar).weekCalendar('removeEvent', calEvent.id);
            return false;
        }

        if (calEvent.slot_type == calendar_events.slot_types.doctors_appointment) {
            if (calendar_events.doctors_appt.reschedule_appointment_id) {
                calendar_events.doctors_appt.drawRescheduleForm(calEvent, calendar);
            } else {
                calendar_events.doctors_appt.drawForm(calEvent, calendar);
            }
        } else if (calEvent.slot_type == calendar_events.slot_types.consultation) {
            if (calendar_events.consultation.reschedule_attendee) { // reschedule mode
                calendar_events.consultation.drawRescheduleForm(calEvent, calendar);
            } else { // new attendee mode
                calendar_events.consultation.drawForm(calEvent, calendar);
            }

            if (calendar_events.current_event != null && calendar_events.consultation.cancel_appointment_id != null) {
                for (var a in calEvent.attendees) {
                    if (calEvent.attendees[a].id == calendar_events.consultation.cancel_appointment_id) {
                        $('#consultation_attendees tbody tr.hover').removeClass('hover');
                        $('#consultation_attendees tbody tr#attendee_row_' + a).addClass('hover');
                        calendar_events.consultation.fillForm(calEvent, calEvent.attendees[a]);
                        calendar_events.consultation.cancelAttendee();
                    }
                } 
            }
        } else if (calEvent.slot_type == calendar_events.slot_types.other) {

//			if (calendar_events.doctors_appt.reschedule_appointment_id) {
//				calendar_events.doctors_appt.drawRescheduleForm(calEvent, calendar);
//			}
//			else {
            calendar_events.other_appt.drawForm(calEvent, calendar);
//			}
        } else {

            alert('looks like you tried to schedule an event type that has yet to be build.  stay tuned.');
            $(calendar).weekCalendar('removeEvent', calEvent.id);
            return false;
        }

    },
    nudgeEventDown: function (event) {
        for (var a in calendar_events.events) {
            var current = calendar_events.events[a];
            if (current.userId == event.userId && event.start >= current.start && event.start < current.end) {
                event.start = current.end;
            }
        }

        return event;
    },
    configuration: {
        ajax_lock: false,
        enabled: false,
        wrench_url: '',

        initialize: function () {
            if (calendar_events.configuration.enabled) {
                $('#configuration_dialog').dialog({
                    autoOpen: false,
                    closeOnEscape: false,
                    modal: true,
//                    height: $(window).height() - 100,
                    width: 700,
                    close: function () {
                        $('#configuration_spinner').hide();
                        $('#configuration_checkmark').hide();
                        $('#configuration_error_icon').hide();
                        $('#configuration_messages_text').text('');

                        $('#configuration_title').text('');
                        $('#configuration_dialog').dialog('option', 'title', '');
                        $('#appointment_book_note_date').val('');
                        $('#appointment_book_note_id').val('');
                        $('#appointment_book_note_message').val('');
                    }
                });
            }

            if (calendar_events.configuration.daily_notes.enabled) {
                calendar_events.configuration.daily_notes.initialize();
            }
        },
        daily_notes: {
            actions: {
                save: ''
            },
            enabled: false,
            messages: {},
            td_height: null,
            initialize: function () {
                $('#save_appointment_book_note').button();
                $('#save_appointment_book_note').click(function () {
                    calendar_events.configuration.daily_notes.submitAppointmentBookNotesForm();
                });
            },
            clearForm: function () {
                $('#appointment_note_form_errors').text('').hide();
                $('#appointment_note_form label.error').remove();
                $('#appointment_note_form .error').removeClass('error');
            },
            displayMessage: function (date, message) {
                $('#' + date).text(message);
                $('#' + date).attr('title', message).tooltip();

                var td = $('#' + date).closest('td');

                var width = td.width();
                width -= $('.wc-date-header', td).width();
                width -= $('.edit-date-message', td).width();

                if (width < $('#' + date).width()) {
                    $('#' + date).width(width - 40);
                }
            },
            getMessageForDate: function (date) {
                var d = date.toString('yyyyMMdd');

                if (calendar_events.configuration.daily_notes.messages.hasOwnProperty(d)) {
                    return calendar_events.configuration.daily_notes.messages[d];
                }

                return {id: '', date: date.toString('yyyy-MM-dd'), message: ''};
            },
            loadNotesForm: function (date) {
                var msg = calendar_events.configuration.daily_notes.getMessageForDate(date);

                $('#appointment_book_note_date').val(msg.date);
                $('#appointment_book_note_id').val(msg.id);
                $('#appointment_book_note_message').val(msg.message);

                $('#appointment_book_note_message').focus();
            },
            submitAppointmentBookNotesForm: function () {
                if (calendar_events.configuration.ajax_lock) {
                    return;
                }

                calendar_events.configuration.daily_notes.clearForm();
                if (!$('#appointment_book_note_message').val().trim() && !$('#appointment_book_note_id').val()) {
                    quickClaim.showFormErrors({'message': {'label': 'Message', 'message': 'No Message Provided.'}}, '#appointment_note_form_errors', '#appointment_book_note', true);

                    $('#configuration_error_icon').show();
                    $('#configuration_messages_text').text('Form errors detected.').show();
                    return;
                }

                var params = $('#appointment_note_form').serialize();

                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: calendar_events.configuration.daily_notes.actions.save,
                    data: params,
                    success: function (rs) {
                        if (rs.hasOwnProperty('errors')) {
                            quickClaim.showFormErrors(rs.errors, '#appointment_note_form_errors', '#appointment_note_form', true);
                            $('#configuration_error_icon').show();
                            $('#configuration_messages_text').text('Form errors detected.').show();
                            quickClaim.focusTopField('appointment_note_form');
                        } else if (rs.hasOwnProperty('data')) {
                            for (var a in rs.data) {
                                var date = rs.data[a].date.replace(/-/g, '');
                                calendar_events.configuration.daily_notes.messages[date] = rs.data[a];
                                calendar_events.configuration.daily_notes.displayMessage(date, rs.data[a].message);
                            }
                            $('#configuration_dialog').dialog('close');
                        }
                    },
                    beforeSend: function () {
                        calendar_events.configuration.ajax_lock = true;
                        $('#configuration_checkmark').hide();
                        $('#configuration_error_icon').hide();
                        $('#configuration_spinner').show();

                        $('#configuration_messages_text').text('Saving Appointment Book Note').show();
                    },
                    complete: function () {
                        calendar_events.configuration.ajax_lock = false;
                        $('#configuration_spinner').hide();
                    },
                    error: function () {
                        $('#configuration_error_icon').show();
                        $('#configuration_messages_text').text('An error occurred while contacting the server.');
                    }
                });

            }
        },
        showConfigureDateDialog: function (date) {
            var year = parseInt(date.substring(0, 4), 10);
            var month = parseInt(date.substring(4, 6), 10) - 1;
            var day = parseInt(date.substring(6, 8), 10);
            var d = Date.today().set({'day': day, 'month': month, 'year': year, hour: 0, minute: 0});

            calendar_events.configuration.daily_notes.loadNotesForm(d);

            d = d.toString('dddd, MMMM, d, yyyy');

            $('#configuration_title').text(d);
            $('#configuration_dialog').dialog('option', 'title', d);
            $('#configuration_dialog').dialog('open');
        }
    },
    consultation: {
        cancellation_window: null,
        defaultPatientType: null,
        header: [],
        fees: {},
        message: {
            checkmark_id: 'consultation_checkmark',
            error_id: 'consultation_error_icon',
            spinner_id: 'consultation_spinner',
            text_id: 'consultation_messages_text'
        },
        cancel_message: {
            checkmark_id: 'consultation_cancel_checkmark',
            error_id: 'consultation_cancel_error_icon',
            spinner_id: 'consultation_cancel_spinner',
            text_id: 'consultation_cancel_messages_text'
        },
        reschedule_message: {
            checkmark_id: 'consultation_reschedule_checkmark',
            error_id: 'consultation_reschedule_error_icon',
            spinner_id: 'consultation_reschedule_spinner',
            text_id: 'consultation_reschedule_messages_text'
        },
        reschedule_attendee: null,
        cancel_appointment_id: null,
        reschedule_event: null,
        similar_patient_cache: '',
        similar_patient_header: [],
        tablesorter1_initialized: false,
        bookable_patient_statuses: {},
        update_statuses: false,
        edit_id: null,
        ajax_lock: false,
        locations_province: {},

        initialize: function () {
            $('#consultation_form').dialog({
                autoOpen: false,
                closeOnEscape: false,
                modal: true,
                height: $(window).height() - 100,
                width: 950,
                open: function () {
                    $('#' + calendar_events.consultation.message.checkmark_id).hide();
                    $('#' + calendar_events.consultation.message.error_id).hide();
                    $('#' + calendar_events.consultation.message.spinner_id).hide();
                    $('#' + calendar_events.consultation.message.text_id).text('').hide();
                },
                close: function () {
                    calendar_events.consultation.similar_patient_cache = '';
                    calendar_events.current_event = null;
                    $('#' + calendar_events.consultation.message.text_id).text('').show();
                }
            });

            $('#consultation_cancel_form_dialog').dialog({
                autoOpen: false,
                closeOnEscape: false,
                modal: true,
                height: $(window).height() - 100,
                width: 950,
                open: function () {
                    $('#' + calendar_events.consultation.cancel_message.checkmark_id).hide();
                    $('#' + calendar_events.consultation.cancel_message.error_id).hide();
                    $('#' + calendar_events.consultation.cancel_message.spinner_id).hide();
                    $('#' + calendar_events.consultation.cancel_message.text_id).text('').hide();
                }
            });

            $('#consultation_reschedule_form_dialog').dialog({
                autoOpen: false,
                closeOnEscape: false,
                modal: true,
                height: $(window).height() - 100,
                width: 950,
                open: function () {
                    $('#' + calendar_events.consultation.reschedule_message.checkmark_id).hide();
                    $('#' + calendar_events.consultation.reschedule_message.error_id).hide();
                    $('#' + calendar_events.consultation.reschedule_message.spinner_id).hide();
                    $('#' + calendar_events.consultation.reschedule_message.text_id).text('').hide();
                },
                close: function () {
                    $('#consultation_reschedule_form label.error').remove();
                    $('#consultation_reschedule_form .error').removeClass('error');
                    $('#consultation_reschedule_form_errors').text('').hide();
                }
            });

            $("#consultation_more_patients_list").dialog({
                autoOpen: false,
                closeOnEscape: false,
                modal: true,
                height: $(window).height() - 100,
                width: 950
            });

            $('#consultation_patient_form .payment_method').hide();
            $('#consultation_patient_form .reconsultation').hide();

            $('#consultation_patient_form #consultation_deposit_paid').click(function () {
                if ($(this).prop('checked')) {
                    $('#consultation_patient_form .payment_method').show();
                } else {
                    $('#consultation_patient_form .payment_method').hide();
                }
                calendar_events.consultation.calculateTotals();
            });

            $('#consultation_patient_form #consultation_reconsultation').click(function () {
                if ($(this).prop('checked')) {
                    $('#consultation_patient_form .reconsultation').show();
                } else {
                    $('#consultation_patient_form .reconsultation').hide();
                }
                calendar_events.consultation.calculateTotals();
            });

            $('#consultation_patient_form input[name="consultation[patient_type]"]').click(function () {
                calendar_events.consultation.calculateTotals();
                calendar_events.consultation.togglePatientTypeMessages();
            });

            $('#consultation_patient_form #consultation_fname').blur(function () {
                calendar_events.consultation.submitPatientSearch();
            });
            $('#consultation_patient_form #consultation_lname').blur(function () {
                calendar_events.consultation.submitPatientSearch();
            });
            $('#consultation_patient_form #consultation_phone').blur(function () {
                calendar_events.consultation.submitPatientSearch();
            });
            $('#consultation_patient_form #consultation_email').blur(function () {
                calendar_events.consultation.submitPatientSearch();
            });

            $('#consultation_submit_button').button();
            $('#consultation_submit_button').click(function () {
                calendar_events.consultation.submitAttendeeForm();
            });

            $('#consultation_cancel_button').button();
            $('#consultation_cancel_button').click(function () {
                calendar_events.consultation.cancelAttendee();
            });

            $('#consultation_reschedule_button').button();
            $('#consultation_reschedule_button').click(function () {
                calendar_events.consultation.rescheduleAttendee();
            });

            $('#consultation_reschedule_submit_button').button();
            $('#consultation_reschedule_submit_button').click(function () {
                calendar_events.consultation.submitReschedule();
            });

            $('#consultation_reschedule_cancel_dialog_button').button();
            $('#consultation_reschedule_cancel_dialog_button').click(function () {
                calendar_events.handleEscapePress();
            });

            $('#consultation_reschedule_cancel_button').button();
            $('#consultation_reschedule_cancel_button').click(function () {
                $('#consultation_reschedule_form_dialog').dialog('close');
                calendar_events.hideHeaderOverlay();
                calendar_events.consultation.reschedule_attendee = null;
                calendar_events.consultation.reschedule_event = null;
            });

            $('#consultation_phone').blur(function () {
                quickClaim.formatPhone('#consultation_phone');
            });

            $('#consultation_cancel_submit_button').button();
            $('#consultation_cancel_submit_button').click(function () {
                calendar_events.consultation.submitCancel();
            });

            $('#consultation_update_status_button').button();
            $('#consultation_update_status_button').click(function () {
                calendar_events.consultation.updateAttendeeStatus();
            });

            $('#similar_patients_button').button();
            $('#similar_patients_button').click(function () {
                $('#consultation_more_patients_list').dialog('open');
            });

            $('#consultation_cancel_refund_required_1').parent('li').parent('ul').parent('td').parent('tr').addClass('refund_required');
        },
        drawForm: function (calEvent, calendar) {
            $('#consultation_patient_form')[0].reset();
            $('#consultation_attendees tbody tr').remove();
            $('#consultation_patient_form label.error').remove();
            $('#consultation_patient_form .error').removeClass('error');
            $('#form_errors').text('').hide();
            $('#calculated_total').text();
            $('#calculated_deposit').text();
            $('#consultation_cancel_button').hide();
            $('#consultation_reschedule_button').hide();

            var attendee = Array();
            var first_empty_row = null;

            calendar_events.current_event = calEvent;

            for (var a = 1; a <= calEvent.max_attendees; a++)
            {
                if (!calEvent.attendees.hasOwnProperty(a) && first_empty_row == null) {
                    first_empty_row = a;
                }
                attendee[a] = calEvent.attendees.hasOwnProperty(a) ? calEvent.attendees[a] : calendar_events.consultation.newAttendee();

                var tr = '<tr id="attendee_row_' + a + '" class="' + (a % 2 == 0 ? 'even' : 'odd') + '">'
                        + calendar_events.consultation.generateTableRow(attendee[a], a)
                        + '</tr>';

                $('#consultation_attendees tbody').append($(tr));
                calendar_events.consultation.initializeTableRow('attendee_row_', a);

                if (first_empty_row) {
                    $('#consultation_attendees tbody tr#attendee_row_' + first_empty_row).addClass('hover');
                    calendar_events.consultation.fillForm(calEvent, attendee[first_empty_row]);
                }
            }

            for (var a in calEvent.cancellations)
            {
                var tr = '<tr id="cancellation_row_' + a + '" class="' + (a % 2 == 0 ? 'even' : 'odd') + ' cancellation">'
                        + calendar_events.consultation.generateTableRow(calEvent.cancellations[a], '#', 'cancel' + a)
                        + '</tr>';

                $('#consultation_attendees tbody').append($(tr));
                calendar_events.consultation.initializeTableRow('cancellation_row_', a);
            }

            $('#consultation_appointment_id').val(calEvent.id);

            $('#consultation_patient_form .payment_method').hide();
            $('#consultation_patient_form .reconsultation').hide();

            var location = $('#consultation_last_location_id option[value=' + calEvent.location_id + ']').text();
            var time = calEvent.start.toString('MMM dd, yyyy HH:mm') + '-' + calEvent.end.toString('HH:mm');

            $('#consultation_form').dialog('option', 'title', 'Consultation Details: ' + location + ' - ' + time);
            $('#consultation_form').dialog('open');
            calendar_events.consultation.togglePatientTypeMessages();
            quickClaim.focusTopField('consultation_patient_form');
        },
        initializeTableRow: function (row_prefix, id) {
            calEvent = calendar_events.current_event;

            $('#' + row_prefix + id + ' input[type=button]').button();

            $('#' + row_prefix + id + '_edit').click(function () {
                tr = $(this).parent('td').parent('tr');
                var k = tr.attr('id').replace('attendee_row_', '').replace('_edit', '');
                $('#consultation_attendees tbody tr.hover').removeClass('hover');
                $('#consultation_attendees #attendee_row_' + k).addClass('hover');
                calendar_events.consultation.fillForm(calEvent, calEvent.attendees[k]);
            });

            $('#' + row_prefix + id + '_cancel').click(function () {
                tr = $(this).parent('td').parent('tr');
                var k = tr.attr('id').replace('attendee_row_', '').replace('_cancel', '');
                $('#attendee_row_' + k + '_edit').click();
                $('#consultation_cancel_button').click();
            });

            $('#' + row_prefix + id + '_reschedule').click(function () {
                tr = $(this).parent('td').parent('tr');
                var k = tr.attr('id').replace('attendee_row_', '').replace('_reschedule', '');
                $('#attendee_row_' + k + '_edit').click();
                $('#consultation_reschedule_button').click();
            });

            $('#' + row_prefix + id + ' .consultation_profile').click(function () {
                var patient_id = $('.patient_id', $(this).parent('td').parent('tr')).text();
                var url = calendar_events.actions.patient_profile + '/id/' + patient_id;
                window.location = url;
            });

            $('#' + row_prefix + id + ' .consultation_reverse').click(function () {
                tr = $(this).parent('td').parent('tr');
                var k = tr.attr('id').replace('attendee_row_', '').replace('_reschedule', '');
                var row_id = $(this).parent('td').parent('tr').attr('id');

                calendar_events.consultation.resetAttendeeStatus(k, row_id);
            });
        },
        generateTableRow: function (attendee, a, row_num) {
            var tr = '';

            for (var b in calendar_events.consultation.header)
            {

                var td_class = calendar_events.consultation.header[b];
                var td = '<td class="' + td_class + '">';

                switch (td_class) {
                    case 'number':
                        td += a;
                        break;
                    case 'patient_type':
                        if (attendee['id']) {
                            td += $('label[for=consultation_patient_type_' + attendee[td_class] + ']').text();
                        } else {
                            td += '';
                        }
                        break;
                    case 'amount_paid':
                    case 'amount_owing':
                    case 'total_fee':
                        if (!attendee['id']) {
                            td += '';
                        } else {
                            td += attendee[td_class];
                        }
                        break;
                    case 'reconsultation':
                        if (!attendee['id']) {
                            td += '';
                        } else {
                            td += attendee[td_class] ? 'Y' : 'N';
                        }
                        break;
                    case 'fname':
                    case 'lname':
                    case 'phone':
                    case 'email':
                    case 'status':
                    case 'deposit_paid':
                    case 'last_first':
                        td += attendee[td_class];
                        break;
                    case 'status_text':
                        td += calendar_events.consultation.generateStatusText(attendee);
                        break;
                    case 'notes':
                        var text = attendee[td_class];
                        td = '<td class="notes" title="' + text.replace('"', '') + '">'
                                + (text.length > 9 ? text.substring(0, 10) + '...' : text).replace("\n", "<br />");

                        td = td.replace('</td>', '');

                        break;
                    case 'edit':
                        td = '<td class="' + td_class + ' actions">';
                        if (attendee.actions.hasOwnProperty('edit')) {
                            td += calendar_events.consultation.generateEventAction('edit', attendee.actions.edit, row_num ? row_num : a);
                        }
                        break;
                    case 'reschedule':
                        td = '<td class="' + td_class + ' actions">';
                        if (attendee.actions.hasOwnProperty('reschedule')) {
                            td += calendar_events.consultation.generateEventAction('reschedule', attendee.actions.reschedule, row_num ? row_num : a);
                        }
                        break;
                    case 'cancel':
                        td = '<td class="' + td_class + ' actions">';
                        if (attendee.actions.hasOwnProperty('cancel')) {
                            td += calendar_events.consultation.generateEventAction('cancel', attendee.actions.cancel, row_num ? row_num : a);
                        }
                        break;
                    case 'profile':
                        td = '<td class="' + td_class + ' actions">';
                        if (attendee.actions.hasOwnProperty('profile')) {
                            td += calendar_events.consultation.generateEventAction('profile', attendee.actions.profile, row_num ? row_num : a);
                        }
                        break;
                    case 'reverse':
                        td = '<td class="' + td_class + ' actions">';
                        if (attendee.actions.hasOwnProperty('reverse')) {
                            td += calendar_events.consultation.generateEventAction('reverse', attendee.actions.reverse, row_num ? row_num : a);
                        }
                        break;
                    default:
                        if (attendee.hasOwnProperty(td_class)) {
                            td += attendee[td_class];
                        }
                        td += ' ';
                        break;
                }
                td += '</td>';
                tr += td;
            }

            return tr;
        },
        generateStatusText: function (attendee) {
            var td = '';
            if (attendee.statuses && this.update_statuses) {
                td += '<select name="status[' + attendee['id'] + ']" id="status_' + attendee['id'] + '" class="attendee_status">';
                for (var b in attendee.statuses) {
                    td += '<option value="' + b + '">' + attendee.statuses[b] + '</option>';
                }
                td += '</select>';
            } else {
                td += attendee['status_text'];
            }
            return td;
        },
        generateEventAction: function (mode, title, row_num) {
            return '<input type="button" class="consultation_' + mode + ' ui-button-inline" id="attendee_row_' + row_num + '_' + mode + '" '
                    + 'value="' + title + '" />';
        },
        generateEventActions: function (attendee, a) {
            $('#attendee_row_' + a + ' td.actions').text('');
            for (var action in attendee.actions) {
                $('#attendee_row_' + a + ' td.' + action).html($(calendar_events.consultation.generateEventAction(action, attendee.actions[action], a)));
            }
        },
        newAttendee: function () {
            var event = {
                fname: '',
                lname: '',
                phone: '',
                email: '',
                status: '',
                deposit_paid: '',
                last_first: '',
                patient_number: '',
                notes: '',
                id: '',
                patient_id: '',
                appointment_id: '',
                patient_type: calendar_events.consultation.defaultPatientType,
                payment_method: '',
                payment_amount: '',
                reconsultation: '',
                last_location_id: '',
                years_ago: '',
                amount_owing: '',
                authorization_code: '',
                promotion: '',
                status_text: '',
                actions: {'edit': 'Edit'}
            };
            return event;
        },
        fillForm: function (event, attendee) {
            if (attendee && attendee.hasOwnProperty('id')) {
                $('#consultation_cancel_button').show();
                $('#consultation_reschedule_button').show();
            } else {
                var attendee = calendar_events.consultation.newAttendee();
            }

            $('#consultation_patient_form')[0].reset();
            $('#consultation_patient_form label.error').remove();
            $('#consultation_patient_form .error').removeClass('error');
            $('#form_errors').text('').hide();
            $('#calculated_total').text();
            $('#calculated_deposit').text();
            $('#' + calendar_events.consultation.message.text_id).text('').show();
            $('#similar_patients_list li').remove();
            $('#similar_patients_button').hide();
            $('#more_results_count').text('');

            $('#consultation_appointment_id').val(calendar_events.current_event.id);
            $('#consultation_id').val(attendee.id);
            $('#consultation_patient_id').val(attendee.patient_id);
            $('#consultation_fname').val(attendee.fname);
            $('#consultation_lname').val(attendee.lname);
            $('#consultation_phone').val(attendee.phone);
            $('#consultation_email').val(attendee.email);
            $('#consultation_payment_amount').val(attendee.payment_amount);
            $('#consultation_last_location_id').val(attendee.last_location_id);
            $('#consultation_years_ago').val(attendee.years_ago);
            $('#consultation_authorization_code').val(attendee.authorization_code);
            $('#consultation_notes').val(attendee.notes.replace('<br />', '').replace('<br>', ''));

            $('#consultation_deposit_paid').prop('checked', attendee.deposit_paid);
            $('#consultation_reconsultation').prop('checked', attendee.reconsultation);
            $('#consultation_promotion').prop('checked', attendee.promotion);

            if (attendee.payment_method) {
                $('#consultation_payment_method_' + attendee.payment_method).prop('checked', true);
            } else {
                $('#consultation_patient_form input[name="consultation[payment_method]"]').prop('checked', false);
            }

            if (attendee.patient_type) {
                $('#consultation_patient_type_' + attendee.patient_type).prop('checked', true);
            } else {
                $('#consultation_patient_form input[name="consultation[patient_type]"]').prop('checked', false);
            }

            if (attendee.deposit_paid) {
                $('#consultation_patient_form .payment_method').show();
            } else {
                $('#consultation_patient_form .payment_method').hide();
            }

            if (attendee.reconsultation) {
                $('#consultation_patient_form .reconsultation').show();
            } else {
                $('#consultation_patient_form .reconsultation').hide();
            }

            calendar_events.consultation.calculateTotals();
            calendar_events.consultation.togglePatientTypeMessages();
            quickClaim.focusTopField('consultation_patient_form');
        },
        submitAttendeeForm: function () {
            if (calendar_events.consultation.ajax_lock) {
                return;
            }

            $('#consultation_patient_form label.error').remove();
            $('#consultation_patient_form .error').removeClass('error');
            $('#form_errors').text('').hide();

            var params = $('#consultation_patient_form').serialize();
            var sidebar_params = calendar_sidebar.getSidebarParameters();
            calendar_events.last_requested_check = new Date().getTime();

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: calendar_events.actions.consultation_save,
                data: params + '&' + sidebar_params,
                success: function (rs) {
                    if (rs.hasOwnProperty('errors')) {
                        quickClaim.showFormErrors(rs.errors, '#form_errors', '#consultation', true);
                        $('#' + calendar_events.consultation.message.text_id).text('Form errors detected.').show();
                        quickClaim.focusTopField('consultation_patient_form');
                    } else if (rs.data.hasOwnProperty('events')) {
                        calendar_events.appointment_counts = rs.data.appt_counts;
                        calendar_events.updateData(rs.data.freebusys, rs.data.events, rs.data.daily_messages);

                        for (var a in rs.data.events) {
                            if (rs.data.events[a].id == $('#consultation_appointment_id').val()) {
                                calendar_events.consultation.drawForm(rs.data.events[a], $('#calendar'));
                            }
                        }

                        $('#similar_patients_list li').remove();
                        $('#similar_patients_button').hide();
                        $('#more_results_count').text('');
                        $('#calculated_deposit').text('');
                        $('#calculated_total').text('');

                        $('#' + calendar_events.consultation.message.text_id).text('Saved appointment.').show();
                    }
                },
                beforeSend: function () {
                    calendar_events.consultation.ajax_lock = true;
                    $('#' + calendar_events.consultation.message.checkmark_id).hide();
                    $('#' + calendar_events.consultation.message.error_id).hide();
                    $('#' + calendar_events.consultation.message.spinner_id).show();

                    $('#' + calendar_events.consultation.message.text_id).text('Saving Attendee').show();
                },
                complete: function () {
                    calendar_events.consultation.ajax_lock = false;
                    $('#' + calendar_events.consultation.message.spinner_id).hide();
                },
                error: function () {
                    $('#' + calendar_events.consultation.message.error_id).show();
                    $('#' + calendar_events.consultation.message.text_id).text('An error occurred while contacting the server.');
                }
            });
        },
        calculateTotals: function () {
            var event = calendar_events.current_event;
            var fees = calendar_events.consultation.fees;

            var consultation_fee = null;
            var start = event.start.toString('yyyy-MM-dd');
            var prov = calendar_events.consultation.locations_province[event.location_id];

            for (var a in fees) {
                if (fees[a].start_date <= start && (fees[a].end_date >= start || fees[a].end_date == '') && fees[a].prov_code == prov) {
                    consultation_fee = fees[a];
                }
            }
            if (consultation_fee == null) {
                return;
            }

            var deposit = 0;
            var total = 0;

            if ($('#consultation_reconsultation').prop('checked')) {
                total = consultation_fee.reconsultation_total;
                deposit = total;
            } else {
                var patient_type = $('#consultation_patient_form input[name="consultation[patient_type]"]:checked').val();
                var patient_type = consultation_fee.patient_types[patient_type];

                deposit = consultation_fee.consultation_deposit;
                switch (patient_type) {
                    case 'regular':
                        total = consultation_fee.consultation_total;
                        break;
                    case 'student':
                        total = consultation_fee.student_total;
                        break;
                    case 'child':
                        total = consultation_fee.child_total;
                        break;
                }
            }

            $('#calculated_total').text(total ? '$' + number_format(total, 2) : '');
            $('#calculated_deposit').text(deposit ? '$' + number_format(deposit, 2) : '');

            if ($('#consultation_patient_form #consultation_deposit_paid').prop('checked')) {
                $('#consultation_payment_amount').val(deposit ? number_format(deposit, 2) : '');
            }
        },
        submitPatientSearch: function () {
            var fname = $('#consultation_fname').val();
            var lname = $('#consultation_lname').val();
            var phone = $('#consultation_phone').val();
            var email = $('#consultation_email').val();

            var count = (fname != '') + (lname != '') + (phone != '') + (email != '');

            if (count > 1 && $('#consultation_form').dialog('isOpen')) {
                var params = 'params[fname]=' + fname
                        + '&params[lname]=' + lname
                        + '&params[phone]=' + phone
                        + '&params[email]=' + email
                        + '&params[appointment_id]=' + $('#consultation_appointment_id').val();

                if (params != calendar_events.consultation.similar_patient_cache) {
                    $('#similar_patients_list li').fadeOut('fast', function () {
                        $('#similar_patients_list li').remove();
                    });
                    $('#consultation_more_patients_table tbody tr').remove();

                    calendar_events.consultation.similar_patient_cache = params;
                    $('#similar_patients_button').hide();

                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        url: calendar_events.actions.consultation_search,
                        data: params,
                        success: function (rs) {
                            if (rs.hasOwnProperty('similar')) {
                                $('#' + calendar_events.consultation.message.text_id).text(rs.count + ' similar patient(s) found').show();
                                for (var v in rs.similar) {
                                    calendar_events.consultation.buildsimilarPatientListItem(rs.similar[v]);
                                }

                                $('#more_results_count').text('(' + rs.count + ')');
                                if (rs.count > rs.similar.length) {
                                    calendar_events.consultation.buildMorePatientListItems(rs.patients);

                                    $('#similar_patients_button').show();
                                }
                            }
                        },
                        beforeSend: function () {
                            $('#' + calendar_events.consultation.message.checkmark_id).hide();
                            $('#' + calendar_events.consultation.message.error_id).hide();
                            $('#' + calendar_events.consultation.message.spinner_id).show();

                            $('#' + calendar_events.consultation.message.text_id).text('Checking For Similar Patients').show();
                        },
                        complete: function () {
                            $('#' + calendar_events.consultation.message.spinner_id).hide();
                        },
                        error: function () {
                            $('#' + calendar_events.consultation.message.error_id).show();
                            $('#' + calendar_events.consultation.message.text_id).text('An error occurred while contacting the server.');
                        }
                    });
                }
            }
        },
        buildsimilarPatientListItem: function (patient) {
            var classes = '';
            if (this.bookable_patient_statuses.hasOwnProperty(patient.patient_status_id)) {
                classes += ' bookable';
            } else {
                classes += ' nonbookable';
            }

            var li = '<li id="similar_patient_' + patient.id + '" class="similar_patient ' + classes + '">';

            li += patient.fname + ' ' + patient.lname + ' (' + patient.patient_status_name + ')'
                    + '<br />Clinic: ' + patient.home_location
                    + '<br />' + patient.hphone + ' ' + patient.email;

            li += '</li>';
            $('#similar_patients_list').append($(li));

            $('#similar_patient_' + patient.id).mouseover(function () {
                $(this).addClass('hover');
            });

            $('#similar_patient_' + patient.id).mouseout(function () {
                $(this).removeClass('hover');
            });

            $('#similar_patient_' + patient.id).click(function () {
                var active = calendar_events.consultation.fillSimilarPatient(patient);
            });
        },
        buildMorePatientListItems: function (patients) {
            var classes = calendar_events.consultation.similar_patient_header;
            var patient = null;

            for (var a in patients) {
                patient = patients[a];
                var tr_class = 'bookable';
                if (!this.bookable_patient_statuses.hasOwnProperty(patient.patient_status_id)) {
                    tr_class = 'nonbookable';
                }

                var tr = '<tr id="similar_patient_table_' + a + '" + class="' + tr_class + '">';
                for (var b = 0; b < classes.length; b++) {
                    var class_name = classes[b];
                    tr += '<td>';
                    tr += patient[class_name];
                    tr += '</td>';
                }
                tr += '</tr>';

                $('#consultation_more_patients_table tbody').append($(tr));

                $('tr#similar_patient_table_' + a).click(function () {
                    var id = $(this).attr('id').replace('similar_patient_table_', '');

                    calendar_events.consultation.fillSimilarPatient(patients[id]);
                });
            }

            if (patients.length) {
                if (calendar_events.consultation.tablesorter1_initialized) {
                    $('#consultation_more_patients_table').trigger('update');
                } else {
                    $('#consultation_more_patients_table').tablesorter1({
                        widgets: ['zebra', 'hover']
                    });
                    calendar_events.consultation.tablesorter1_initialized = true;
                }
            }
        },
        fillSimilarPatient: function (p) {
            if (this.bookable_patient_statuses.hasOwnProperty(p.patient_status_id)) {
                $('#consultation_fname').val(p.fname);
                $('#consultation_lname').val(p.lname);
                $('#consultation_phone').val(p.hphone);
                $('#consultation_email').val(p.email);
                $('#consultation_patient_id').val(p.id);
                $('#consultation_patient_type_' + p.patient_type_id).prop('checked', true);
                $('#consultation_more_patients_list').dialog('close');
                return true;
            } else {
                alert('You cannot book a consultation for patients with this status.');
                return false;
            }
            calendar_events.consultation.togglePatientTypeMessages();
        },
        cancelAttendee: function () {
            var current_attendee = null;
            var refund = true;

            for (var a in calendar_events.current_event.attendees) {
                if (calendar_events.current_event.attendees[a].id == $('#consultation_id').val()) {
                    current_attendee = calendar_events.current_event.attendees[a];
                }
            }

            $('#consultation_cancel_form label.error').remove();
            $('#consultation_cancel_form .error').removeClass('error');
            $('#consultation_cancel_form_errors').text('').hide();
            $('#consultation_cancel_form').clearForm();

            if (current_attendee) {
                $('#consultation_cancel_id').val($('#consultation_id').val());
                $('#consultation_cancel_patient_id').val($('#consultation_patient_id').val());
                $('#consultation_cancel_appointment_id').val($('#consultation_appointment_id').val());
                $('#consultation_cancel_volusion_number').val('');

                if (calendar_events.current_event.start >= new Date().add(calendar_events.consultation.cancellation_window).hours()) {
                    $('#within_timeframe').text('Yes');
                } else {
                    $('#within_timeframe').text('No');
                    refund = false;
                }

                $('#deposit_amouunt').text(current_attendee.amount_paid, 2);
                if (current_attendee.amount_paid == '$0.00') {
                    refund = false;

                    $('#deposit_amouunt').text('$0.00');
                    $('#consultation_cancel_refund_required_0').prop('checked', true);
                    $('.refund_required').hide();
                }

                $('#qualifies_for_refund').text('No');
                if (refund) {
                    $('#qualifies_for_refund').text('Yes');
                }

                $('#consultation_cancel_form_dialog').dialog('open');
                quickClaim.focusTopField('consultation_cancel_form');
            } else {
                alert('You cannot cancel an attendee without selecting one first');
            }
        },
        submitCancel: function () {
            if (calendar_events.consultation.ajax_lock) {
                return;
            }
            $('#consultation_cancel_form label.error').remove();
            $('#consultation_cancel_form .error').removeClass('error');
            $('#consultation_cancel_form_errors').text('').hide();

            var params = $('#consultation_cancel_form').serialize();
            var sidebar_params = calendar_sidebar.getSidebarParameters();
            calendar_events.last_requested_check = new Date().getTime();

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: calendar_events.actions.consultation_cancel,
                data: params + '&' + sidebar_params,
                success: function (rs) {
                    if (rs.hasOwnProperty('errors')) {
                        quickClaim.showFormErrors(rs.errors, '#consultation_cancel_form_errors', '#consultation_cancel', true);
                        $('#' + calendar_events.consultation.cancel_message.text_id).text('Form errors detected.').show();
                    } else if (rs.data.hasOwnProperty('events')) {
                        calendar_events.appointment_counts = rs.data.appt_counts;
                        calendar_events.updateData(rs.data.freebusys, rs.data.events, rs.data.daily_messages);

                        for (var a in rs.data.events) {
                            if (rs.data.events[a].id == $('#consultation_appointment_id').val()) {
                                calendar_events.consultation.drawForm(rs.data.events[a], $('#calendar'));
                            }
                        }

                        $('#similar_patients_list li').remove();
                        $('#calculated_deposit').text('');
                        $('#calculated_total').text('');

                        $('#' + calendar_events.consultation.message.text_id).text('Cancelled attendee.').show();
                        $('#consultation_cancel_form_dialog').dialog('close');
                    }
                },
                beforeSend: function () {
                    calendar_events.consultation.ajax_lock = true;
                    $('#' + calendar_events.consultation.cancel_message.checkmark_id).hide();
                    $('#' + calendar_events.consultation.cancel_message.error_id).hide();
                    $('#' + calendar_events.consultation.cancel_message.spinner_id).show();
                    $('#' + calendar_events.consultation.cancel_message.text_id).text('Cancelling Attendee').show();
                },
                complete: function () {
                    calendar_events.consultation.ajax_lock = false;
                    $('#' + calendar_events.consultation.cancel_message.spinner_id).hide();
                },
                error: function () {
                    $('#' + calendar_events.consultation.cancel_message.error_id).show();
                    $('#' + calendar_events.consultation.cancel_message.text_id).text('An error occurred while contacting the server.');
                }
            });
        },
        rescheduleAttendee: function () {
            var name = '';
            var event = calendar_events.current_event;
            var attendee_id = $('#consultation_id').val();

            for (var a in event.attendees) {
                if (event.attendees[a].id == attendee_id) {
                    name = calendar_events.current_event.attendees[a].full_name;
                    calendar_events.consultation.reschedule_attendee = calendar_events.current_event.attendees[a];
                    calendar_events.consultation.reschedule_event = calendar_events.current_event;
                }
            }

            if (calendar_events.consultation.reschedule_attendee) {
                calendar_events.showHeaderOverlay('Select an appointment to reschedule <span class="name">' + name + '</span> to.', calendar_events.consultation.reschedule_attendee.patient_id);
                $('#consultation_form').dialog('close');
            } else {
                alert('You cannot reschedule an attendee without selecting one first');
            }
        },
        drawRescheduleForm: function (calEvent, calendar) {
            var current_event = calendar_events.consultation.reschedule_event;
            var current_attendee = calendar_events.consultation.reschedule_attendee;

            $('#consultation_reschedule_id').val('');
            $('#consultation_reschedule_patient_id').val(current_attendee.patient_id);
            $('#consultation_reschedule_original_appointment_id').val(current_event.id);
            $('#consultation_reschedule_new_appointment_id').val(calEvent.id);
            $('#consultation_reschedule_original_appointment_attendee_id').val(current_attendee.id);
            $('#consultation_reschedule_note').val('');

            $('#consultation_reschedule_form .person').text(current_attendee.fname + ' ' + current_attendee.lname);
            $('#consultation_reschedule_form .original_date').text(current_event.start.toString(quickClaim.dateTimeFormat));
            $('#consultation_reschedule_form .new_date').text(calEvent.start.toString(quickClaim.dateTimeFormat));

            $('#consultation_reschedule_form .original_location').text($('#consultation_last_location_id option[value=' + current_event.location_id + ']').text());
            $('#consultation_reschedule_form .new_location').text($('#consultation_last_location_id option[value=' + calEvent.location_id + ']').text());

            $('#consultation_reschedule_form_dialog').dialog('open');
            quickClaim.focusTopField('consultation_reschedule_form');
        },
        submitReschedule: function () {
            if (calendar_events.consultation.ajax_lock) {
                return;
            }
            $('#consultation_reschedule_form label.error').remove();
            $('#consultation_reschedule_form .error').removeClass('error');
            $('#consultation_reschedule_form_errors').text('').hide();

            var params = $('#consultation_reschedule_form').serialize();
            var sidebar_params = calendar_sidebar.getSidebarParameters();
            calendar_events.last_requested_check = new Date().getTime();

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: calendar_events.actions.consultation_reschedule,
                data: params + '&' + sidebar_params,
                success: function (rs) {
                   
                    if (rs.hasOwnProperty('errors')) {
                        quickClaim.showFormErrors(rs.errors, '#consultation_reschedule_form_errors', '#consultation_reschedule', true);
                        $('#' + calendar_events.consultation.reschedule_message.text_id).text('Form errors detected.').show();
                    } else if (rs.data.hasOwnProperty('events')) {
                        calendar_events.appointment_counts = rs.data.appt_counts;
                        calendar_events.updateData(rs.data.freebusys, rs.data.events, rs.data.daily_messages);

                        $('#similar_patients_list li').remove();
                        $('#calculated_deposit').text('');
                        $('#calculated_total').text('');
                        $('#' + calendar_events.message.text_id).text('Rescheduled attendee.').show();

                        calendar_events.hideHeaderOverlay();
                        calendar_events.consultation.reschedule_attendee = null;
                        calendar_events.consultation.reschedule_event = null;
                        calendar_events.current_event = null;
                        $('#consultation_reschedule_form_dialog').dialog('close');
                    }
                },
                beforeSend: function () {
                    calendar_events.consultation.ajax_lock = true;
                    $('#' + calendar_events.consultation.reschedule_message.checkmark_id).hide();
                    $('#' + calendar_events.consultation.reschedule_message.error_id).hide();
                    $('#' + calendar_events.consultation.reschedule_message.spinner_id).show();

                    $('#' + calendar_events.consultation.reschedule_message.text_id).text('Rescheduling Attendee').show();
                },
                complete: function () {
                    calendar_events.consultation.ajax_lock = false;
                    $('#' + calendar_events.consultation.reschedule_message.spinner_id).hide();
                },
                error: function () {
                    $('#' + calendar_events.consultation.reschedule_message.error_id).show();
                    $('#' + calendar_events.consultation.reschedule_message.text_id).text('An error occurred while contacting the server.');
                }
            });
        },
        resetAttendeeStatus: function (k, row_id) {
            if (calendar_events.consultation.ajax_lock) {
                return;
            }
            calEvent = calendar_events.current_event;
            attendee = calEvent.attendees[k];

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: calendar_events.actions.consultation_reset_status,
                data: 'attendee_id=' + attendee.id,
                success: function (rs) {
                    for (var id in rs.data) {
                        var row_num = row_id.replace('attendee_row_', '');

                        $('#' + row_id + ' td.status_text').html(calendar_events.consultation.generateStatusText(rs.data[id]));
                        calendar_events.consultation.generateEventActions(rs.data[id], row_num);
                        calendar_events.consultation.initializeTableRow('attendee_row_', row_num);
                    }

                    if (rs.hasOwnProperty('appointment')) {
                        rs.appointment.start = Date.parse(rs.appointment.start);
                        rs.appointment.end = Date.parse(rs.appointment.end);
                        rs.appointment.userId = parseInt(calendar_events.defaults.getUserId(rs.appointment.userId));

                        $('#calendar').weekCalendar('updateEvent', rs.appointment);
                    }

                    $('#' + calendar_events.consultation.message.checkmark_id).show();
                    $('#' + calendar_events.consultation.message.text_id).text('Reversed Attendee Statuses.');
                },
                beforeSend: function () {
                    calendar_events.consultation.ajax_lock = true;
                    $('#' + calendar_events.consultation.message.checkmark_id).hide();
                    $('#' + calendar_events.consultation.message.error_id).hide();
                    $('#' + calendar_events.consultation.message.spinner_id).show();
                    $('#' + calendar_events.consultation.message.text_id).text('Reversing Attendee Statuses').show();
                },
                complete: function () {
                    calendar_events.consultation.ajax_lock = false;
                    $('#' + calendar_events.consultation.message.spinner_id).hide();
                },
                error: function () {
                    $('#' + calendar_events.consultation.message.error_id).show();
                    $('#' + calendar_events.consultation.message.text_id).text('An error occurred while contacting the server.');
                }
            });
        },
        updateAttendeeStatus: function (id, value, row_num) {
            if (calendar_events.consultation.ajax_lock) {
                return;
            }

            var params = $('#consultation_status_form').serialize();
            var sidebar_params = calendar_sidebar.getSidebarParameters();
            calendar_events.last_requested_check = new Date().getTime();

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: calendar_events.actions.consultation_update_status,
                data: params + '&' + sidebar_params,
                success: function (rs) {
                    for (var id in rs.data) {
                        var row_num = $('#status_' + id).parent('td').parent('tr').attr('id').replace('attendee_row_', '');

                        $('#attendee_row_' + row_num + ' td.status_text').html(calendar_events.consultation.generateStatusText(rs.data[id]));
                        calendar_events.consultation.generateEventActions(rs.data[id], row_num);
                        calendar_events.consultation.initializeTableRow('attendee_row_', row_num);
                    }
                    if (rs.hasOwnProperty('appointment')) {
                        rs.appointment.start = Date.parse(rs.appointment.start);
                        rs.appointment.end = Date.parse(rs.appointment.end);
                        rs.appointment.userId = calendar_events.defaults.getUserId(rs.appointment.userId);

                        $('#calendar').weekCalendar('updateEvent', rs.appointment);
                    }
                    if (rs.events.hasOwnProperty('events')) {
                        calendar_events.appointment_counts = rs.data.appt_counts;
                        calendar_events.updateData(rs.data.freebusys, rs.data.events, rs.data.daily_messages);
                    }
                    $('#' + calendar_events.consultation.message.checkmark_id).show();
                    $('#' + calendar_events.consultation.message.text_id).text('Updated Attendee Statuses.');
                },
                beforeSend: function () {
                    calendar_events.consultation.ajax_lock = true;
                    $('#' + calendar_events.consultation.message.checkmark_id).hide();
                    $('#' + calendar_events.consultation.message.error_id).hide();
                    $('#' + calendar_events.consultation.message.spinner_id).show();
                    $('#' + calendar_events.consultation.message.text_id).text('Updating Attendee Statuses').show();
                },
                complete: function () {
                    calendar_events.consultation.ajax_lock = false;
                    $('#' + calendar_events.consultation.message.spinner_id).hide();
                },
                error: function () {
                    $('#' + calendar_events.consultation.message.error_id).show();
                    $('#' + calendar_events.consultation.message.text_id).text('An error occurred while contacting the server.');
                }
            });
        },
        togglePatientTypeMessages: function () {
            if ($('#consultation_patient_form input[name="consultation[patient_type]"]:checked').size() == 0) {
                return;
            }
            var val = $('#consultation_patient_form input[name="consultation[patient_type]"]:checked').attr('id').replace('consultation_patient_type_', '');

            $('.patient_type_msg').hide();
            if ($('#patient_type_message_' + val).size()) {
                $('#patient_type_message_' + val).show();
                $('.patient_type_message').show();
            } else {
                $('.patient_type_message').hide();
            }
        }
    },
    doctors_appt: {
        message: {
            checkmark_id: 'doctor_checkmark',
            error_id: 'doctor_error_icon',
            spinner_id: 'spinner',
            text_id: 'messages_text'
        },
        cancel_message: {
            checkmark_id: 'doctor_appt_cancel_checkmark',
            error_id: 'doctor_appt_cancel_error_icon',
            spinner_id: 'doctor_appt_cancel_spinner',
            text_id: 'doctor_appt_cancel_messages_text'
        },
        doctor_appointment_type_lengths: null,
        reschedule_message: {
            checkmark_id: 'doctor_appt_reschedule_checkmark',
            error_id: 'doctor_appt_reschedule_error_icon',
            spinner_id: 'doctor_appt_reschedule_spinner',
            text_id: 'doctor_appt_reschedule_messages_text'
        },
        reschedule_appointment_id: null,
        reschedule_event: null,
        edit_event: null,
        edit_event_id: null,
        patient_from_profile: null,
        cancel_appointment_id: null,
        include_recent_notes: false,
        close_only_past: false,
        appointment_stages: {},
        ajax_lock: false,
        overwrite_service_code: false,

        initialize: function () {
            // $('#simple_patient_hcn_version_code').blur(function () {
            //     calendar_events.doctors_appt.hcvRequest($('#simple_patient_hcn_num').val(), $('#simple_patient_hcn_version_code').val(), $('#appointment_patient_id').val());
            // });

            $('#doctor_appointment_form_dialog').dialog({
                autoOpen: false,
                closeOnEscape: false,
                modal: true,
                position: {my: 'top', at: 'top+150'},
                minHeight: $(window).height() - 200,
                minWidth: 1200,
                open: function () {
                    $('#' + patient_search.messages.checkmark).hide();
                    $('#' + patient_search.messages.error).hide();
                    $('#' + patient_search.messages.spinner).hide();
                    $('#' + patient_search.messages.text).text('').hide();

                    $('#' + calendar_events.doctors_appt.message.checkmark_id).hide();
                    $('#' + calendar_events.doctors_appt.message.error_id).hide();
                    $('#' + calendar_events.doctors_appt.message.spinner_id).hide();
                    $('#' + calendar_events.doctors_appt.message.text_id).text('').hide();

                    $('#override_doctor_appt').hide();
                    $('#repeating_event_override').hide();
                },
                close: function () {
                    calendar_events.current_event = null;
                    $('#patient_search_lname').val('');
                    $('#calendar').weekCalendar('removeUnsavedEvents');
                    $('#past-present-future').empty();
                    $('#visualization').empty();
                }
            });


            $('#patient_search').dialog({
                autoOpen: false,
                closeOnEscape: false,
                modal: true,
                height: 600,
                width: 1200
            });

            $('#doctor_appt_cancel_form_dialog').dialog({
                autoOpen: false,
                closeOnEscape: false,
                modal: true,
                height: 350,
                width: 750,
                open: function () {
                    $('#' + calendar_events.doctors_appt.cancel_message.checkmark_id).hide();
                    $('#' + calendar_events.doctors_appt.cancel_message.error_id).hide();
                    $('#' + calendar_events.doctors_appt.cancel_message.spinner_id).hide();
                    $('#' + calendar_events.doctors_appt.cancel_message.text_id).text('').hide();
                }
            });

            $('#doctor_appt_reschedule_form_dialog').dialog({
                autoOpen: false,
                closeOnEscape: false,
                modal: true,
                height: $(window).height() - 100,
                width: 950,
                open: function () {
                    $('#' + calendar_events.doctors_appt.reschedule_message.checkmark_id).hide();
                    $('#' + calendar_events.doctors_appt.reschedule_message.error_id).hide();
                    $('#' + calendar_events.doctors_appt.reschedule_message.spinner_id).hide();
                    $('#' + calendar_events.doctors_appt.reschedule_message.text_id).text('').hide();
                },
                close: function () {
                    calendar_events.doctors_appt.clearDragged();

                    $('#calendar').weekCalendar('removeUnsavedEvents');
                    $('#doctor_appt_reschedule_form_errors label.error').remove();
                    $('#doctor_appt_reschedule_form_errors .error').removeClass('error');
                    $('#doctor_appt_reschedule_form_errors').text('').hide();
                    $('#override_reschedule_doctor').hide();
                    $('#doctor_appt_reschedule_cancel_dialog_button').show();
                }
            });

            calendar_events.doctors_appt.setupAppointmentTypeButtons();

//            $('#patient_search_button').button();
            $('#patient_search_button').click(function () {
                patient_search.handleClearPress();
                $('#past-present-future').empty();
                $('#visualization').empty();
                calendar_events.patient_search.showPatientSearchForm();
                $('#patient_search_patient_number').focus();
            });

            $('#non_doctor_submit_button').button();
            $('#non_doctor_submit_button').click(function () {
                //true use for model close or not
                calendar_events.doctors_appt.submitDoctorAppointmentForm(false);
            });

            $('#non_doctor_cancel_button').button();
            $('#non_doctor_cancel_button').click(function () {
                calendar_events.other_appt.cancelAppointment();
            });

            $('#doctor_submit_button').button();
            $('#doctor_submit_button').click(function () {
                //true use for model close or not
                calendar_events.doctors_appt.submitDoctorAppointmentForm(false);
            });

            $('#doctor_submit_and_dont_close_button').button();
            $('#doctor_submit_and_dont_close_button').click(function(){
                //true use for model close or not
                calendar_events.doctors_appt.submitDoctorAppointmentForm(true); 
            })

            $('#doctor_cancel_button').button();
            $('#doctor_cancel_button').click(function () {
                calendar_events.doctors_appt.cancelAppointment();
                $('#doctor_appt_cancel_submit_button').focus();
            });

            $('#doctor_cancel_dialog_button').button();
            $('#doctor_cancel_dialog_button').click(function () {
                calendar_events.handleEscapePress();
            });

            $('#doctor_appt_cancel_submit_button').button();
            $('#doctor_appt_cancel_submit_button').click(function () {
                calendar_events.doctors_appt.submitCancelAppointment();
            });

            $('#doctor_appt_cancel_dialog_button').button();
            $('#doctor_appt_cancel_dialog_button').click(function () {
                calendar_events.handleEscapePress();
            });

            $('#doctor_reschedule_button').button();
            $('#doctor_reschedule_button').click(function () {
                calendar_events.doctors_appt.rescheduleAppointment();
            });

            $('#doctor_appt_reschedule_submit_button').button();
            $('#doctor_appt_reschedule_submit_button').click(function () {
                calendar_events.doctors_appt.submitReschedule();
            });

            $('#doctor_appt_reschedule_cancel_button').button();
            $('#doctor_appt_reschedule_cancel_button').click(function () {
                calendar_events.hideHeaderOverlay();
                calendar_events.doctors_appt.reschedule_appointment_id = null;
                calendar_events.doctors_appt.reschedule_event = null;
                $('#doctor_appt_reschedule_form_dialog').dialog('close');
            });

            $('#doctor_appt_reschedule_cancel_dialog_button').button();
            $('#doctor_appt_reschedule_cancel_dialog_button').click(function () {
                if ($('#calendar_header_overlay').css('display') == 'none') {
                    var name = calendar_events.doctors_appt.reschedule_event.patient_fname + ' ' + calendar_events.doctors_appt.reschedule_event.patient_lname;

                    calendar_events.showHeaderOverlay('Select an available time to reschedule <span class="name">' + name + '</span> to.', calendar_events.doctors_appt.reschedule_event.patient_id);
                    $('#doctor_appointment_form_dialog').dialog('close');
                }

                calendar_events.handleEscapePress();
            });

            $('#doctor_appointment_form #appointment_repeats').click(function () {
                if ($(this).prop('checked')) {
                    calendar_events.doctors_appt.showAppointmentTemplateParameters();
                } else {
                    calendar_events.doctors_appt.hideAppointmentTemplateParameters();
                }
            });

            $('#doctor_appointment_form #appointment_repeat_end_date').datepick({
                dateFormat: quickClaim.dateFormat,
                renderer: $.datepick.themeRollerRenderer
            });

            $('#doctor_appointment_form .repeating_existing_event td label').each(function () {
                $(this).html($(this).html().replace('Appointment', '<span class="appointment_type_name">Appointment</span>'));
            });

            $('#doctor_appointment_form input[name="appointment[apply_to]"]').change(function () {
                calendar_events.doctors_appt.handleApplyToChange();
            });

            $('#appointment_start_time_hour').change(function () {
                $('#appointment_override_schedule_check').prop('checked', false);
                $('#override_doctor_appt').hide();
                $('#appointment_override_repeating_check').prop('checked', false);
                $('#repeating_event_override').hide();
            });
            $('#appointment_start_time_minute').change(function () {
                $('#appointment_override_schedule_check').prop('checked', false);
                $('#override_doctor_appt').hide();
                $('#appointment_override_repeating_check').prop('checked', false);
                $('#repeating_event_override').hide();
            });
            $('#appointment_start_time_ampm').change(function () {
                $('#appointment_override_schedule_check').prop('checked', false);
                $('#override_doctor_appt').hide();
                $('#appointment_override_repeating_check').prop('checked', false);
                $('#repeating_event_override').hide();
            });
            $('#appointment_length').keypress(function () {
                $('#appointment_override_schedule_check').prop('checked', false);
                $('#override_doctor_appt').hide();
                $('#appointment_override_repeating_check').prop('checked', false);
                $('#repeating_event_override').hide();
            });
            $('input[name="appointment[repeat_weekdays][]"]').click(function () {
                $('#appointment_override_repeating_check').prop('checked', false);
                $('#repeating_event_override').hide();
            });
            $('#appointment_repeat_end_date').keypress(function () {
                $('#appointment_override_repeating_check').prop('checked', false);
                $('#repeating_event_override').hide();
            });
        },
        drawForm: function (calEvent, calendar) {
            console.log('calEvent',calEvent);
            calendar_events.doctors_appt.resetDialog();
            calendar_events.doctors_appt.setupAppointmentTypes(calEvent);
            //$.notifyClose(); 
            $('#doctor_appointment_form_dialog').dialog('option', 'title', this.buildDialogTitle(calEvent));
            contextMenu.disable('reverse');
            contextMenu.disable('claim');




            if (calEvent.id) {
                $('#doctor_appointment_form #appointment_id').val(calEvent.id);
                $('#doctor_appointment_form #appointment_doctor_id').val(calEvent.doctor_id);
                $('#doctor_appointment_form #appointment_location_id').val(calEvent.location_id);
                $('#doctor_appointment_form #appointment_appointment_attendee_id').val(calEvent.appointment_attendee_id);
                $('#doctor_appointment_form #appointment_appointment_type_id').val(calEvent.appointment_type_id);

                $('#doctor_appointment_form #appointment_length').val(calEvent.length);
                $('#doctor_appointment_form #appointment_slot_type').val(calEvent.slot_type);
                $('#doctor_appointment_form #appointment_notes').val(calEvent.notes);
                $('#doctor_appointment_form #appointment_repeat_end_date').val(calEvent.repeat_end_date);
                $('#doctor_appointment_form #appointment_appointment_template_id').val(calEvent.appointment_template_id);
                $('#doctor_appointment_form #appointment_patient_id').val(calEvent.patient_id);
                $('#doctor_appointment_form #appointment_repeats').prop('checked', calEvent.repeats);
                $('#doctor_appointment_form #appointment_service_code').val(calEvent.service_code);
                $('#doctor_appointment_form #appointment_diag_code').val(calEvent.diag_code);

                for (var a in calEvent.repeat_weekdays) {
                    $('#doctor_appointment_form #appointment_repeat_weekdays_' + a).prop('checked', calEvent.repeat_weekdays[a]);
                }
                //$("#appointment_appointment_type_" + calEvent.appointment_type).trigger('click');
                $('#patient_data_row .patient_id').text(calEvent.patient_id);
                $('#patient_data_row .last_first').text(calEvent.patient_lname + ', ' + calEvent.patient_fname);
                $('#patient_data_row .patient_number').text(calEvent.patient_number);
                $('#patient_data_row .hphone').html(calEvent.patient_phone + '<span class="ui-icon ui-icon-triangle-1-s"></span>');
                $('#patient_data_row .patient_location_name').text(calEvent.patient_location);
                $('#patient_data_row .dob').text(calEvent.patient_dob);
                $('#patient_data_row .start_time').text(calEvent.start.toString('HH:mm'));
                $('#patient_data_row .length').text(calEvent.length + ' min');
                $('#patient_data_row .status').text(calEvent.status_string);
                $('#patient_data_row .appt_type').text(calEvent.appointment_type_str);
                $('#patient_data_row .stage').text(calEvent.stage_client_name);

                $('#patient_data_row .hcn').text(calEvent.patient_hcn);
                $('#patient_data_row .vc').text(calEvent.patient_vc);
                $('#patient_data_row .version_code').text(calEvent.patient_vc);
                if (calEvent.patient_hcn_check_date && calEvent.patient_hcv_status) {
                    $('.hcv_check_text').removeClass('sk-text-green').removeClass('sk-text-danger').removeClass('hcv_pass').removeClass('hcv_fail').removeClass('hcv_warn').removeClass('hcv_mod10_pass').hide();
                    $('#patient_data_row .hcv_check_text').text(calEvent.patient_hcn_check_date + ': ' + calEvent.patient_hcv_status);
                    $('#patient_data_row .hcv_check_text').addClass(calEvent.patient_hcv_status_class).show();
                    $('#patient_data_row .hcn_td').addClass(calEvent.patient_hcv_status_class);
//                    $(".hcv_res_color").addClass(calEvent.patient_hcv_status_class);
                }

                if (calendar_events.actions.hcv) {
                    calendar_events.doctors_appt.hcvRequest($('#patient_data_row .hcn').text(), $('#patient_data_row .version_code').text(), $('#appointment_patient_id').val());
                }

                if (calEvent.appointment_template_id) {
                    $('#doctor_appointment_form .repeating_existing_event').show();
                    $('#doctor_appointment_form #appointment_apply_to_all_events').prop('checked', true);
                    calendar_events.doctors_appt.handleApplyToChange();
                }
                if (calEvent.patient_id) {
                    $('#patient_details_table .search').hide();
                    $('.appointment_template_patient').show();
                }
                if (calEvent.repeats) {
                    calendar_events.doctors_appt.showAppointmentTemplateParameters();
                }
                calendar_events.patient_search.showPatientDetails();
                calendar_events.doctors_appt.selectAppointmentType(calEvent.appointment_type);
                calendar_events.doctors_appt.updateAppointmentTypeClasses(calEvent.appointment_type);

                calendar_events.doctors_appt.setupAppointmentStages(calEvent.stage_client_id, calEvent.id);

                if (!calendar_events.doctors_appt.close_only_past || calEvent.start < new Date()) {
                    calendar_events.doctors_appt.setupApptDetailsStages(calEvent.stage_client_id);
                    $("#s2id_appointment_stage_change").css("width","64%");
                }

                if (calEvent.can_edit) {
                    $('#doctor_cancel_button').show();
                    contextMenu.enable('cancel');

                    $('#doctor_reschedule_button').show();
                    contextMenu.enable('reschedule');

                    $('#doctor_submit_button').show();
//                    $("#patient_search_button").show();
                } else {
                    $('#doctor_cancel_button').hide();
                    contextMenu.disable('cancel');

                    $('#doctor_reschedule_button').hide();
                    contextMenu.disable('reschedule');

                    $('#doctor_submit_button').hide();
//                    $("#patient_search_button").hide();
                }

                if (calEvent.can_reverse) {
                    contextMenu.enable('reverse');
                }
                if (calEvent.can_claim) {
                    contextMenu.enable('claim');
                }

                $('#patient_data .appt').show();
            } else {
                $('#doctor_appointment_form .non_patient_field').hide();
                $('#doctor_appointment_form .patient_field').hide();

                $('#doctor_cancel_button').hide();
                $('#doctor_reschedule_button').hide();

                if (calendar_events.doctors_appt.patient_from_profile) {
                    calendar_events.patient_search.loadData(calendar_events.doctors_appt.patient_from_profile);
                    calendar_events.patient_search.showPatientDetails();
                }

                var wkday = calEvent.start.toString('ddd').toLowerCase();
                wkday = (wkday == 'tue' ? 'tues' : (wkday == 'thu' ? 'thurs' : wkday));

                $('#doctor_appointment_form #appointment_repeat_weekdays_' + wkday).prop('checked', true);
                $('#doctor_appointment_form table tr.appointment_stage').hide();

                $('#patient_data .appt').hide();
                contextMenu.disable('reschedule');
                contextMenu.disable('cancel');
            }

            $('#doctor_appointment_form #appointment_location_id').val(calEvent.location_id);
            $('#doctor_appointment_form #appointment_doctor_id').val(calEvent.doctor_id);

            $('#doctor_appointment_form #appointment_start_date').val(new Date(calEvent.start).toString('yyyy-MM-dd HH:mm'));

            $('#doctor_appointment_form #appointment_start_time_hour').val(new Date(calEvent.start).toString('h'));


            $('#doctor_appointment_form #appointment_start_time_minute').val(new Date(calEvent.start).toString('m'));

            $('#doctor_appointment_form #appointment_start_time_ampm').val(new Date(calEvent.start).toString('tt').toLowerCase());

            $('#doctor_appointment_form #appointment_repeat_end_date').datepick('option', 'defaultDate', calEvent.start.toString(quickClaim.getParseDateFormat(quickClaim.dateFormat)));

            calendar_events.current_event = calEvent;

            $('#doctor_appointment_form .location_name').text($('label[for=location_select_' + calEvent.location_id + ']').text());
            $('#doctor_appointment_form .doctor_name').text($('label[for=doctor_select_' + calEvent.location_id + '_' + calEvent.doctor_id + ']').text());
            calEvent.start = new Date(calEvent.start);
            $('#doctor_appointment_form .date').text(calEvent.start.toString('MMMM d, yyyy').toUpperCase());
            $('#doctor_appointment_form .time').text(calEvent.start.toString('H:mm').toUpperCase());

            if (!calEvent.appointment_type) {
                $('label').removeClass('ui-state-focus ui-state-hover ui-state-active');
            }

            $('#patient_search_location_id').val(calEvent.location_id);
            $("#search_message").hide();
            $('#doctor_appointment_form_dialog').dialog('open');

            quickClaim.focusTopField('doctor_appointment_focus_area');

            //alert(calendar_events.doctors_appt.cancel_return_url);

            //  alert(calendar_events.doctors_appt.cancel_patient_id);
            if (calendar_events.doctors_appt.cancel_appointment_id) {
                //alert(calendar_events.doctors_appt.cancel_return_url);
                calendar_events.doctors_appt.cancelAppointment();
                calendar_events.doctors_appt.cancel_appointment_id = null;
            }

            if (!$('#doctor_appointment_form #appointment_patient_id').val()) {
                $('#patient_search_patient_number').focus();
            }

            $('#appointment_types_td input').change(function () {
                $('#appointment_override_schedule_check').prop('checked', false);
                $('#override_doctor_appt').hide();
                $('#appointment_override_repeating_check').prop('checked', false);
                $('#repeating_event_override').hide();
            });
        },
        buildDialogTitle: function (calEvent) {
            var location_name = $('#patient_search_location_id option[value=' + calEvent.location_id + ']').text();
            var time = calEvent.start.toString('MMM dd, yyyy HH:mm');
            contextMenu.setTitle(time);
            return 'Appointment Details: ' + location_name + ' - ' + time;
        },
        resetDialog: function () {
            $('#doctor_appointment_form').clearForm();
            $('#doctor_appointment_form .patient_field').hide();
            calendar_events.doctors_appt.hideAppointmentTemplateParameters();

            $('#doctor_appointment_form label.error').remove();
            $('#doctor_appointment_form .error').removeClass('error');
            $('#doctor_form_errors').text('').hide();

            $('#patient_details_table .search').show();
            $('.appointment_template_patient').show();

            $('#patient_data_row .patient').text('');
            $('#patient_data_row .appt').text('');
            $('#patient_data_row .hcv_response_text').removeClass('hcv_pass').removeClass('hcv_fail').removeClass('hcv_mod10_pass');
            $('#doctor_appointment_form .form_text').text('');
//            $('#doctor_appointment_table #appointment_types_td').html('');

            calendar_events.patient_search.showPatientSearchForm();
            $('#doctor_submit_button').show();
//            $("#patient_search_button").show();

            $('#patient_data_row .hphone').hover(function () {
                var id = $('#patient_data_row > td.patient_id.patient').html();
                $('#patient_data_row .hphone').prop('title', '');

                if (id) {
                    var url = 'pid=' + id;

                    if (calendar_events.pull_phones_flag)
                        return;

                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        url: calendar_events.actions.pull_patient_phone,
                        data: url,
                        beforeSend: function () {
                            calendar_events.pull_phones_flag = true;
                        },
                        complete: function () {
                            calendar_events.pull_phones_flag = false;
                        },
                        success: function (rs) {
                            if (rs.cphone || rs.ophone || rs.wphone) {
                                var titleContent = '', formatted;

                                if (rs.ophone) {
                                    formatted = rs.ophone.substr(0, 3) + '-' + rs.ophone.substr(3, 3) + '-' + rs.ophone.substr(6, 4) + ' +' + rs.ophone.substr(10);
                                    titleContent += 'Office: ' + formatted;
                                }

                                if (rs.wphone) {
                                    formatted = rs.wphone.substr(0, 3) + '-' + rs.wphone.substr(3, 3) + '-' + rs.wphone.substr(6, 4) + ' +' + rs.wphone.substr(10);
                                    titleContent += 'Work: ' + formatted;
                                }

                                if (rs.cphone) {
                                    formatted = rs.cphone.substr(0, 3) + '-' + rs.cphone.substr(3, 3) + '-' + rs.cphone.substr(6, 4) + ' +' + rs.cphone.substr(10);
                                    titleContent += 'Mobile: ' + formatted + ', ';
                                }

                                $('#patient_data_row .hphone').prop('title', titleContent);
                                $('#patient_data_row .hphone').tooltip({
                                    position: {
                                        my: "bottom top",
                                        at: "right+5 top-5"
                                    }
                                });
                                $('#patient_data_row .hphone').unbind('mouseenter mouseleave');
                            }
                        }
                    });
                }
            });
        },
        setAppointmentTypeClasses: function () {
            $('input[name="appointment[appointment_type]"]').each(function () {
                var appt_type = $(this).attr('id').replace('appointment_appointment_type_', '');
                appt_type = appt_type.split('_');
                $('label[for=' + $(this).attr('id') + ']').addClass('appointment_type_' + appt_type[0]);
            });
            
            $('input[name="appointment[appointment_type]"]').click(function () {
                var num = $(this).val().split("_").pop();
                var bgr = $(this).next().css('background-color');
                var clr = $(this).next().css('color');
                calendar_events.doctors_appt.updateAppointmentTypeClasses($(this).val());
                calendar_events.doctors_appt.fillAppointmentTypeServiceDetails($(this).val());
                $('.click_after').css('background', bgr);
                $('.click_after .appt_type').attr('style', 'font-size:13px !important;margin-top: 3px;');
                $('.click_after').css('border-radius', '3px');
                $('.click_after').css('text-align', 'center');
                $('.click_after .appt_type').css('color', clr);
                $('.click_after .chk_after_num').html(' [' + num + ']');
                $('.click_after .chk_after_num').attr('style', 'margin-top: 3px;color: rgb(0, 0, 0);font-weight: 400;font-size: 13px !important;font-family: open sans;');
                $('.click_after .chk_after_num').css('color', clr);
            });

        },
        setupAppointmentTypes: function (calEvent) {
            appt_types = calendar_events.doctors_appt.doctor_appointment_type_lengths[calEvent.doctor_id];
            var ul = '<ul class="radio_list" style="width:100% !important">';
            var count = 0; 
            for (var a in appt_types) {
                for (var b in appt_types[a].lengths) {
                    if($.inArray(calEvent.doctor_id, appt_types[a] .doctor_ids) !== -1)
                    {
                        //console.log('appt_types',appt_types);
                        var id = a + '_' + appt_types[a].slot_type + '_' + appt_types[a].lengths[b];
                        var label = appt_types[a].name + ' [' + appt_types[a].lengths[b] + ' min]';

                        ul += '<li class="col-sm-4" >';
                        ul += '<input type="radio" name="appointment[appointment_type]" id="appointment_appointment_type_' + id + '" value="' + id + '" />';
                        ul += '<label  style="width: 100%; margin-bottom:10px;" for="appointment_appointment_type_' + id + '">' + label + '</label>';
                        ul += '</li>';
                        count++;
                    }
                }
            }

            ul += '</ul>';
            var doctor_name = $('label[for=doctor_select_' + calEvent.location_id + '_' + calEvent.doctor_id + ']').text();
            if(count == 0)
            {
                $('.notifymsgcsrempty .message').html('No one appointment type assign for doctor : <strong>'+doctor_name+'</strong>');
                $('.notifymsgcsrempty').show();
            } else {
                $('.notifymsgcsrempty').hide();
            }

            $('#appointment_types_td').html($(ul));
            calendar_events.doctors_appt.setupAppointmentTypeButtons();
        },
        setupAppointmentStages: function (stage_client_id, event_id) {
            if (this.appointment_stages.hasOwnProperty(stage_client_id)) {
                var inputs = '<input type="radio" name="appointment[stage_client_id]" id="appointment_stage_client_id_' + stage_client_id + '" ' + 'value="' + stage_client_id + '" checked="checked" />'
                        + '<label for="appointment_stage_client_id_' + stage_client_id + '">' + this.appointment_stages[stage_client_id].name + '</label> ';
                for (var a in this.appointment_stages[stage_client_id].next_stage) {
                    var id = a;
                    var name = this.appointment_stages[stage_client_id].next_stage[a];
                    inputs += '<input type="radio" name="appointment[stage_client_id]" id="appointment_stage_client_id_' + id + '" value="' + id + '" />'
                            + '<label for="appointment_stage_client_id_' + id + '">' + name + '</label>';
                }

                // iterate over next stages
                $('#stage_client_ids').html($(inputs));
                $('#stage_client_ids').buttonset();
            }
        },
        setupApptDetailsStages: function (stage_client_id) {
            var options = '';

            if ($('#patient_data_row .stage').size() && this.appointment_stages.hasOwnProperty(stage_client_id)) {
                for (var a in this.appointment_stages[stage_client_id].next_stage) {
                    var id = a;
                    var name = this.appointment_stages[stage_client_id].next_stage[a];

                    options += '<option value="' + id + '">' + name + '</option>';
                }

                if (options.length) {
                    options = '<option value="' + stage_client_id + '" selected="selected" >' + this.appointment_stages[stage_client_id].name + '</option>'
                            + options;

                    var select = '<select id="appointment_stage_change" name="appointment[stage_change]">'
                            + options
                            + '</select>&nbsp;'
                            + '<input type="button" id="appointment_stage_change_button" class="btn btn-squared btn-info" value="Update" />';

                    $('#patient_data_row .stage').html($(select));

                    $("#appointment_stage_change").select2({
                        placeholder: "-- select --",
                        allowClear: true,
                        width: '50%'
                    });
                    $('#appointment_stage_change_button').button();

                    $('#appointment_stage_change_button').click(function () {
                        calendar_events.doctors_appt.updateAppointmentStatus();
                    });
                }
            }
        },
        updateAppointmentStatus: function () {
            if (calendar_events.doctors_appt.ajax_lock) {
                return;
            }

            var appointment_id = $('#appointment_id').val();
            var stage_client_id = $('#appointment_stage_change').val();
            var params = 'appointment[id]=' + appointment_id
                    + '&appointment[stage_client_id]=' + stage_client_id;

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: calendar_events.actions.doctors_appt_stage_update,
                data: params,
                success: function (rs) {
                    if (rs.hasOwnProperty('appointment')) {
                        rs.appointment.start = Date.parse(rs.appointment.start);
                        rs.appointment.end = Date.parse(rs.appointment.end);
                        rs.appointment.userId = parseInt(calendar_events.defaults.getUserId(rs.appointment.userId), 10);

                        calendar_events.doctors_appt.drawForm(rs.appointment, $('#calendar'));
                        $('#calendar').weekCalendar('updateEvent', rs.appointment);
                    } else {
                        $('#' + patient_search.messages.error).show();
                        $('#' + patient_search.messages.text).text('An error occurred at the server.');
                    }
                },
                beforeSend: function () {
                    calendar_events.doctors_appt.ajax_lock = true;
                    $('#' + patient_search.messages.checkmark).hide();
                    $('#' + patient_search.messages.error).hide();
                    $('#' + patient_search.messages.spinner).show();

                    $('#' + patient_search.messages.text).text('Updating Appointment').show();
                },
                complete: function () {
                    calendar_events.doctors_appt.ajax_lock = false;
                    $('#' + patient_search.messages.spinner).hide();
                },
                error: function () {
                    $('#' + patient_search.messages.error).show();
                    $('#' + patient_search.messages.text).text('An error occurred while contacting the server.');
                }
            });
        },
        reverseAppointmentStatus: function () {
            if (calendar_events.doctors_appt.ajax_lock) {
                return;
            }

            var appointment_id = $('#appointment_id').val();
            var params = 'appointment[id]=' + appointment_id;

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: calendar_events.actions.doctors_appt_stage_reverse,
                data: params,
                success: function (rs) {
                    if (rs.hasOwnProperty('appointment')) {
                        rs.appointment.start = Date.parse(rs.appointment.start);
                        rs.appointment.end = Date.parse(rs.appointment.end);
                        rs.appointment.userId = parseInt(calendar_events.defaults.getUserId(rs.appointment.userId), 10);

                        calendar_events.doctors_appt.drawForm(rs.appointment, $('#calendar'));
                        $('#calendar').weekCalendar('updateEvent', rs.appointment);

                        $('#' + patient_search.messages.checkmark).show();
                        $('#' + patient_search.messages.text).text('Reversed Appointment Status').show();
                    } else {
                        $('#' + patient_search.messages.error).show();
                        $('#' + patient_search.messages.text).text('An error occurred at the server.');
                    }
                },
                beforeSend: function () {
                    calendar_events.doctors_appt.ajax_lock = true;
                    $('#' + patient_search.messages.checkmark).hide();
                    $('#' + patient_search.messages.error).hide();
                    $('#' + patient_search.messages.spinner).show();

                    $('#' + patient_search.messages.text).text('Reversing Appointment Status').show();
                },
                complete: function () {
                    calendar_events.doctors_appt.ajax_lock = false;
                    $('#' + patient_search.messages.spinner).hide();
                },
                error: function () {
                    $('#' + patient_search.messages.error).show();
                    $('#' + patient_search.messages.text).text('An error occurred while contacting the server.');
                }
            });
        },
        setupAppointmentTypeButtons: function () {
            $('input[name="appointment[appointment_type]"]').button();
            calendar_events.doctors_appt.setAppointmentTypeClasses();
        },
        submitDoctorAppointmentForm: function (modelClose = null) {
            if ($("#appointment_start_time_hour").val() == '' || $("#appointment_start_time_minute").val() == '' || $("#appointment_start_time_ampm").val() == '') {
                $("#doctor_form_errors").html('<b>Start Time : </b> start time is required!');
                $("#doctor_form_errors").show();
                return false;
            }

            if (calendar_events.doctors_appt.ajax_lock) {
                return;
            }

            $('#doctor_appointment_form label.error').remove();
            $('#doctor_appointment_form .error').removeClass('error');
            $('#doctor_form_errors').text('').hide();

            var disabled = $('#doctor_appointment_form input:disabled');

            disabled.each(function () {
                $(this).attr('disabled', false);
            });
            var params = $('#doctor_appointment_form').serialize();
            disabled.each(function () {
                $(this).attr('disabled', 'disabled');
            });
            var sidebar_params = calendar_sidebar.getSidebarParameters();
            calendar_events.last_requested_check = new Date().getTime();

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: calendar_events.actions.doctors_appt_save,
                data: params + '&' + sidebar_params,
                success: function (rs) {
                    if (rs.hasOwnProperty('errors')) {
                        quickClaim.showFormErrors(rs.errors, '#doctor_form_errors', '#appointment', true);
                        $('#' + calendar_events.doctors_appt.message.text_id).text('Form errors detected.').show();

                        if (rs.hasOwnProperty('show_override')) {
                            $('#override_doctor_appt').show();
                        } else {
                            $('#override_doctor_appt').hide();
                        }

                        if (rs.hasOwnProperty('show_repeating_override')) {
                            $('#repeating_event_override').show();
                        } else {
                            $('#repeating_event_override').hide();
                        }
                    } else if (rs.data.hasOwnProperty('events')) {
                        calendar_events.appointment_counts = rs.data.appt_counts;
                        calendar_events.updateData(rs.data.freebusys, rs.data.events, rs.data.daily_messages);

                        $('#similar_patients_list li').remove();
                        $('#calculated_deposit').text('');
                        $('#calculated_total').text('');

                        calendar_events.hideHeaderOverlay();
                        calendar_events.doctors_appt.patient_from_profile = null;
                        calendar_events.doctors_appt.reschedule_appointment_id = null;
                        calendar_events.doctors_appt.reschedule_event = null;
                        $('#successAppoint').trigger('click');
                        
                        if($('#doctor_appointment_form #appointment_id').val() == '')
                        {
                            calendar_events.drawForm(rs.data.events.reverse()[0], '');
                        } else {
                            var indexNo = rs.data.events.map(function (img) { return img.id; }).indexOf($('#doctor_appointment_form #appointment_id').val());
                            calendar_events.drawForm(rs.data.events[indexNo], '');
                        }

                        $.notifyClose();
                        if(modelClose == false) {
                            console.log('test');
                            $('#doctor_appointment_form_dialog').dialog('close');
                        }
                        $('#' + calendar_events.message.text_id).text('Saved appointment.').show();
                    }
                },
                beforeSend: function () {
                    calendar_events.doctors_appt.ajax_lock = true;
                    $('#' + calendar_events.doctors_appt.message.checkmark_id).hide();
                    $('#' + calendar_events.doctors_appt.message.error_id).hide();
                    $('#' + calendar_events.doctors_appt.message.spinner_id).show();

                    $('#' + calendar_events.doctors_appt.message.text_id).text('Saving Appointment').show();
                },
                complete: function () {
                    calendar_events.doctors_appt.ajax_lock = false;
                    $('#' + calendar_events.doctors_appt.message.spinner_id).hide();
                },
                error: function () {
                    $('#' + calendar_events.doctors_appt.message.error_id).show();
                    $('#' + calendar_events.doctors_appt.message.text_id).text('An error occurred while contacting the server.');
                }
            });

        },
        selectAppointmentType: function (val) {
            if ($('#doctor_appointment_form label[for=appointment_appointment_type_' + val + ']').size()) {
                $('#doctor_appointment_form label[for=appointment_appointment_type_' + val + ']').click();
                $('#appointment_appointment_type_' + val).prop('selected', true);
                return;
            }

            var terms = val.split('_');
            var match_str = terms[0] + '_' + terms[1] + '_';

            $('#doctor_appointment_form input[name="appointment[appointment_type]"]').each(function () {
                var val = $(this).val();
                if (val.indexOf(match_str) != -1) {
                    $('#doctor_appointment_form label[for=appointment_appointment_type_' + val + ']').click();
                    $('#appointment_appointment_type_' + val).prop('selected', true);
                    return;
                }
            });
        },
        updateAppointmentTypeClasses: function (val) {
            var terms = val.split('_');
            var appointment_type_id = terms[0];
            var slot_type = terms[1];
            var length = terms[2];
            var doctor_id = $('#doctor_appointment_form #appointment_doctor_id').val();

            var patient_required = true;

            if (calendar_events.doctors_appt.doctor_appointment_type_lengths.hasOwnProperty('doctor_id') && calendar_events.doctors_appt.doctor_appointment_type_lengths[doctor_id].hasOwnProperty('appointment_type_id')) {
                var patient_required = calendar_events.doctors_appt.doctor_appointment_type_lengths[doctor_id][appointment_type_id].patient_required;

            }

            if (!patient_required) {
                calendar_events.patient_search.hidePatientSearch();

                if ($('#doctor_appointment_form #appointment_id').val()) {
                    calendar_events.patient_search.showPatientDetails();
                    $('#patient_data .patient').hide();
                    $('#patient_data .hcn_td').hide();
                    $('#patient_data .appt').show();
                    $('#patient_data .profile').hide();
                    $('#patient_data .search').hide();
                }
            } else {
                if ($('#doctor_appointment_form #appointment_patient_id').val()) {
                    calendar_events.patient_search.showPatientDetails();
                    $('#patient_data .patient').show();
                    $('#patient_data .hcn_td').show();
                    $('#patient_data .appt').show();
                    $('#patient_data .profile').show();
                    $('#patient_data .search').hide();
                } else {
                    calendar_events.patient_search.showPatientSearchForm();
                }
                calendar_events.doctors_appt.hideAppointmentTemplateParameters();
            }

            $('#doctor_appointment_form .appt_type').text(calendar_events.appointment_types[appointment_type_id]);
            $('#doctor_appointment_form .appointment_type_name').text(calendar_events.appointment_types[appointment_type_id].toLowerCase());
            $('#doctor_appointment_form #appointment_length').val(length);
            $('#doctor_appointment_form #appointment_slot_type').val(slot_type);
            $('#doctor_appointment_form #appointment_appointment_type_id').val(appointment_type_id);

            if (patient_required) {
                $('#doctor_appointment_form .non_patient_field').hide();
                $('#doctor_appointment_form .patient_field').show();
                $('#doctor_appointment_form .repeats').hide();
            } else {
                $('#doctor_appointment_form .patient_field').hide();
                $('#doctor_appointment_form .non_patient_field').show();
                $('#doctor_appointment_form .repeats').show();
            }

            if (!$('#appointment_id').val()) {
                $('#doctor_appointment_form table tr.appointment_stage').hide();
            }
        },
        cancelAppointment: function () {
            $('#doctor_appt_cancel_form .non_patient_field').hide();
            if ($('#appointment_id').val() && $('#appointment_repeats').prop('checked')) {
                $('#doctor_appt_cancel_form .non_patient_field').show();
            }
            $('#doctor_appt_cancel_form').clearForm();
            $('#appointment_cancel_id').val($('#appointment_id').val());
            $('#doctor_appt_cancel_form_dialog').dialog('open');
            $('#appointment_cancel_status_4').prop('checked', 'checked');
        },
        submitCancelAppointment: function () {
            if (calendar_events.doctors_appt.ajax_lock) {
                return;
            }
            $('#doctor_appt_cancel_form label.error').remove();
            $('#doctor_appt_cancel_form .error').removeClass('error');
            $('#doctor_appt_cancel_form_errors').text('').hide();
            $('#' + calendar_events.doctors_appt.cancel_message.text_id).text('').show();

            var params = $('#doctor_appt_cancel_form').serialize();
            var sidebar_params = calendar_sidebar.getSidebarParameters();
            calendar_events.last_requested_check = new Date().getTime();

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: calendar_events.actions.doctors_appt_cancel,
                data: params + '&' + sidebar_params,
                success: function (rs) {
                    if (rs.hasOwnProperty('errors')) {
                        quickClaim.showFormErrors(rs.errors, '#doctor_appt_cancel_form_errors', '#consultation_cancel', true);
                        $('#' + calendar_events.doctors_appt.cancel_message.text_id).text('Form errors detected.').show();
                    } else if (rs.data.hasOwnProperty('events')) {
                        calendar_events.appointment_counts = rs.data.appt_counts;
                        calendar_events.updateData(rs.data.freebusys, rs.data.events, rs.data.daily_messages);
                        $('#' + calendar_events.message.text_id).text('Cancelled appointment.').show();
                        $('#doctor_appointment_form_dialog').dialog('close');
                        $('#doctor_appt_cancel_form_dialog').dialog('close');
                        if(calendar_events.doctors_appt.cancel_return_url == 'patient')
                        {
                           window.location.href = 'patient/view?id='+calendar_events.doctors_appt.cancel_patient_id+'&aTab=active';
                        }
                    }
                },
                beforeSend: function () {
                    calendar_events.doctors_appt.ajax_lock = true;
                    $('#' + calendar_events.doctors_appt.cancel_message.checkmark_id).hide();
                    $('#' + calendar_events.doctors_appt.cancel_message.error_id).hide();
                    $('#' + calendar_events.doctors_appt.cancel_message.spinner_id).show();
                    $('#' + calendar_events.doctors_appt.cancel_message.text_id).text('Cancelling Appointment.').show();
                },
                complete: function () {
                    calendar_events.doctors_appt.ajax_lock = false;
                    $('#' + calendar_events.doctors_appt.cancel_message.spinner_id).hide();
                },
                error: function () {
                    $('#' + calendar_events.doctors_appt.cancel_message.error_id).show();
                    $('#' + calendar_events.doctors_appt.cancel_message.text_id).text('An error occurred while contacting the server.');
                }
            });
        },
        rescheduleAppointment: function () {
            var name = $('#patient_data_row .fname').text() + ' ' + $('#patient_data_row .lname').text();
            calendar_events.doctors_appt.reschedule_event = calendar_events.current_event;
            calendar_events.doctors_appt.reschedule_appointment_id = $('#doctor_appointment_form #appointment_id').val();

            calendar_events.showHeaderOverlay('Select an available time to reschedule <span class="name">' + name + '</span> to.', calendar_events.doctors_appt.reschedule_event.patient_id);
            $('#doctor_appointment_form_dialog').dialog('close');
        },
        drawRescheduleForm: function (calEvent, calendar) {
            var current_event = calendar_events.doctors_appt.reschedule_event;
            //console.log('current_event',current_event);
            if($('#appointment_book_mode_bulk').val() == 'groupreschedule')
            {
                var appointment_id = $('#groupreschedule_id').val();
            }
            else
            {
                var appointment_id = current_event.id;
            }

            $('#doctor_appt_reschedule_form .old_appt_type').text(current_event.appointment_type_str);
            $('#doctor_appt_reschedule_form .new_appt_type').text(current_event.appointment_type_str);
            $('#doctor_appt_reschedule_form .old_doctor_id').text($('label[for=doctor_select_' + current_event.location_id + '_' + current_event.doctor_id + ']').text());
            $('#doctor_appt_reschedule_form .new_doctor_id').text($('label[for=doctor_select_' + calEvent.location_id + '_' + calEvent.doctor_id + ']').text());
            $('#doctor_appt_reschedule_form .old_location_id').text($('label[for=location_select_' + current_event.location_id + ']').text());
            $('#doctor_appt_reschedule_form .new_location_id').text($('label[for=location_select_' + calEvent.location_id + ']').text());
            $('#doctor_appt_reschedule_form .old_date').text(current_event.start.toString('MMMM d, yyyy').toUpperCase());
            $('#doctor_appt_reschedule_form .new_date').text(calEvent.start.toString('MMMM d, yyyy').toUpperCase());
            $('#doctor_appt_reschedule_form .old_start_time').text(current_event.start.toString('h:mm tt').toUpperCase());
            $('#doctor_appt_reschedule_form .old_length').text(current_event.length);
            $('#doctor_appt_reschedule_form .new_length').text(current_event.length);
            $('#doctors_appt_reschedule_appointment_type_resched').val($('#appointment_book_mode_bulk').val());
            $('#doctor_appt_reschedule_form #doctors_appt_reschedule_start_time_hour').val(calEvent.start.toString('h'));
            $('#doctor_appt_reschedule_form #doctors_appt_reschedule_start_time_minute').val(calEvent.start.toString('m'));
            $('#doctor_appt_reschedule_form #doctors_appt_reschedule_start_time_ampm').val(calEvent.start.toString('tt').toLowerCase());
            $('#doctor_appt_reschedule_form #doctors_appt_reschedule_note').val(current_event.notes);
            $('#doctor_appt_reschedule_form #doctors_appt_reschedule_id').val('');
            $('#doctor_appt_reschedule_form #doctors_appt_reschedule_original_appointment_id').val(appointment_id);
            //$('#doctor_appt_reschedule_form #doctors_appt_reschedule_original_appointment_id').val(current_event.id);
            $('#doctor_appt_reschedule_form #doctors_appt_reschedule_doctor_id').val(calEvent.doctor_id);
            $('#doctor_appt_reschedule_form #doctors_appt_reschedule_location_id').val(calEvent.location_id);
            $('#doctor_appt_reschedule_form #doctors_appt_reschedule_length').val(current_event.length);
            $('#doctor_appt_reschedule_form #doctors_appt_reschedule_start_date').val(calEvent.start.toString('yyyy-MM-dd'));

            $('#override_reschedule_doctor').hide();

            //option override to skip

            // $('#doctor_appt_reschedule_form_dialog').dialog('open');
            // $('#doctor_appt_reschedule_submit_button').focus();
            if (calendar_events.override_rescheule) {
                $('#' + calendar_events.message.checkmark_id).hide();
                $('#' + calendar_events.message.error_id).hide();
                $('#' + calendar_events.message.spinner_id).show();
                $('#' + calendar_events.message.text_id).text('Rescheduling...').show();
                calendar_events.doctors_appt.submitReschedule();
            } else {
                $('#doctor_appt_reschedule_form_dialog').dialog('open');
                $('#doctor_appt_reschedule_submit_button').focus();
            }
        },
        submitReschedule: function () {

            if (calendar_events.doctors_appt.ajax_lock) {
                return;
            }
            $('#doctor_appt_reschedule_form label.error').remove();
            $('#doctor_appt_reschedule_form .error').removeClass('error');
            $('#doctor_appt_reschedule_form_errors').text('').hide();

            var params = $('#doctor_appt_reschedule_form').serialize();
            var sidebar_params = calendar_sidebar.getSidebarParameters();
            calendar_events.last_requested_check = new Date().getTime();

            var datanew = params + '&' + sidebar_params;

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: calendar_events.actions.doctors_appt_reschedule,
                data: params + '&' + sidebar_params,
                success: function (rs) {
                    if (rs.hasOwnProperty('errors')) {
                        quickClaim.showFormErrors(rs.errors, '#doctor_appt_reschedule_form_errors', '#doctors_appt_reschedule', true);
                        $('#' + calendar_events.doctors_appt.reschedule_message.text_id).text('Form errors detected.').show();
                        $('#calendar').weekCalendar('refresh');

                        if (rs.hasOwnProperty('show_override')) {
                            $('#override_reschedule_doctor').show();
                        }
                    } else if (rs.data.hasOwnProperty('events')) {
                        calendar_events.appointment_counts = rs.data.appt_counts;
                        calendar_events.updateData(rs.data.freebusys, rs.data.events, rs.data.daily_messages);

                        $('#' + calendar_events.message.text_id).text('Rescheduled appointment.').show();

                        $('#' + calendar_events.message.checkmark_id).show();
                        $('#' + calendar_events.message.error_id).hide();
                        $('#' + calendar_events.message.spinner_id).hide();

                        calendar_events.hideHeaderOverlay();

                        calendar_events.doctors_appt.reschedule_appointment_id = null;
                        calendar_events.current_event = null;
                        calendar_events.doctors_appt.reschedule_event = null;

                        $('#doctor_appt_reschedule_form_dialog').dialog('close');
                    }
                },
                beforeSend: function () {
                    calendar_events.doctors_appt.ajax_lock = true;
                    $('#' + calendar_events.doctors_appt.reschedule_message.checkmark_id).hide();
                    $('#' + calendar_events.doctors_appt.reschedule_message.error_id).hide();
                    $('#' + calendar_events.doctors_appt.reschedule_message.spinner_id).show();

                    $('#' + calendar_events.doctors_appt.reschedule_message.text_id).text('Rescheduling Appointment').show();
                },
                complete: function () {
                    calendar_events.doctors_appt.ajax_lock = false;
                    $('#' + calendar_events.doctors_appt.reschedule_message.spinner_id).hide();
                },
                error: function () {
                    $('#' + calendar_events.doctors_appt.reschedule_message.error_id).show();
                    $('#' + calendar_events.doctors_appt.reschedule_message.text_id).text('An error occurred while contacting the server.');
                }
            });
        },
        showAppointmentTemplateParameters: function () {
            $('#doctor_appointment_form .repeating_event').show();
        },
        hideAppointmentTemplateParameters: function () {
            $('#doctor_appointment_form .repeating_event').hide();
            $('#doctor_appointment_form .repeating_existing_event').hide();
        },
        handleApplyToChange: function () {
            if ($('#appointment_apply_to_only_current').prop('checked')) {
                $('#appointment_repeats').attr('disabled', 'disabled');
                $('#appointment_repeat_weekdays_sun').attr('disabled', 'disabled');
                $('#appointment_repeat_weekdays_mon').attr('disabled', 'disabled');
                $('#appointment_repeat_weekdays_tues').attr('disabled', 'disabled');
                $('#appointment_repeat_weekdays_wed').attr('disabled', 'disabled');
                $('#appointment_repeat_weekdays_thurs').attr('disabled', 'disabled');
                $('#appointment_repeat_weekdays_fri').attr('disabled', 'disabled');
                $('#appointment_repeat_weekdays_sat').attr('disabled', 'disabled');
                $('#appointment_repeat_end_date').attr('disabled', 'disabled');
                $('#appointment_override_repeating_check').attr('disabled', 'disabled');

            } else {
                $('#appointment_repeats').attr('disabled', false);
                $('#appointment_repeat_weekdays_sun').attr('disabled', false);
                $('#appointment_repeat_weekdays_mon').attr('disabled', false);
                $('#appointment_repeat_weekdays_tues').attr('disabled', false);
                $('#appointment_repeat_weekdays_wed').attr('disabled', false);
                $('#appointment_repeat_weekdays_thurs').attr('disabled', false);
                $('#appointment_repeat_weekdays_fri').attr('disabled', false);
                $('#appointment_repeat_weekdays_sat').attr('disabled', false);
                $('#appointment_repeat_end_date').attr('disabled', false);
                $('#appointment_override_repeating_check').attr('disabled', false);
            }
        },
        fillAppointmentTypeServiceDetails: function (appointment_type_id) {
            var details = appointment_type_id.split('_');
            appointment_type_id = details[0];

            if (!$('#appointment_id').val() && calendar_events.appointment_types_services.hasOwnProperty(appointment_type_id)) {
                $('#appointment_service_code').val(calendar_events.appointment_types_services[appointment_type_id].service_code);
                $('#appointment_diag_code').val(calendar_events.appointment_types_services[appointment_type_id].diag_code);
            } else if (calendar_events.doctors_appt.overwrite_service_code && calendar_events.appointment_types_services.hasOwnProperty(appointment_type_id)) {
               ///console.log(calendar_events.appointment_types_services);
               $('#appointment_service_code').val(calendar_events.appointment_types_services[appointment_type_id].service_code);
               $('#appointment_diag_code').val(calendar_events.appointment_types_services[appointment_type_id].diag_code);
            }
        },
        populateSimpleForm: function (person) {
            $('#simple_patient_' + 'fname').val(person.firstName);
            $('#simple_patient_' + 'lname').val(person.lastName);
            $('#simple_patient_dob').skDP('setDate', new Date(person.dateOfBirth));
            $('#simple_patient_' + 'sex').val(person.gender).trigger('change.select2');
        }, 
        hcvRequest: function (hcn, vc, id, target, isvalidatepopup = true) {

            if (hcn) {
                var data = 'hcn=' + hcn + '&vc=' + vc + '&id=' + id;
                var original_class = $('.hcv_check_text').attr('class');

                $(".hcn_td .hcn").removeClass('hcv_pass').removeClass('hcv_fail').removeClass('hcv_warn').removeClass('hcv_mod10_pass');
                $('.hcv_check_text').removeClass('hcv_pass').removeClass('hcv_fail').removeClass('hcv_warn').removeClass('hcv_mod10_pass').hide();
                $('.fee_service_row').remove();

                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: calendar_events.actions.hcv,
                    data: data,
                    success: function (rs) {

                        var date = new Date();
                        $('#' + patient_search.messages.text).text('Received message.').show();

                        if (rs.hasOwnProperty('response_message') && rs.hasOwnProperty('response_code')) {

                            if (rs.code == 500){
                                $('#' + patient_search.messages.text).text('').hide();
                                $.notifyClose();   
                                if(isvalidatepopup == 'false'){
                                 $('.hcv_check_text').text('HCV is down at the moment. Service should be restored shortly.').addClass('warning').show();
                                 return false;
                                } else {
                                     sk.skNotify('HCV is down at the moment. Service should be restored shortly.', 'warning');                                
                                    return false;
                                }
                           }

                            var msg, message;
                            var person = rs.results[0];
                            var fee_service_codes = person.feeServiceDetails;

                            if (person) {
                                calendar_events.doctors_appt.populateSimpleForm(person);
                            }

                            if (fee_service_codes) {
                                $('#feeServiceDetails').show();

                                var table = $('#patient_data tr:last');
                                var row;
                                for (var fsc in fee_service_codes) {
                                    row = '<tr class="fee_service_row">';
                                    if (fee_service_codes[fsc]['feeServiceResponseCode'] == 101) {
                                        for (var variable in fee_service_codes[fsc]) {
                                            if (variable == 'feeServiceResponseDescription') {
                                                row += '<td colspan=3 class="fee_service_row hcv_pass">' + fee_service_codes[fsc][variable] + '</td>';
                                            } else {
                                                row += '<td class="fee_service_row hcv_pass">' + fee_service_codes[fsc][variable] + '</td>';
                                            }
                                        }
                                        row += '</tr>';
                                        table.after(row);
                                    } else {
                                        for (var variable in fee_service_codes[fsc]) {
                                            if (variable == 'feeServiceResponseDescription') {
                                                if (fee_service_codes[fsc]['feeServiceDate'] != null) {
                                                    row += '<td colspan=3 class="fee_service_row hcv_fail">' + moment(fee_service_codes[fsc]['feeServiceDate']).format(global_js.getNewMomentFormat()) + ' ' + fee_service_codes[fsc][variable] + '</td>';
                                                } else {
                                                    row += '<td colspan=3 class="fee_service_row hcv_fail">' + fee_service_codes[fsc][variable] + '</td>';
                                                }
                                            } else {
                                                row += '<td class="fee_service_row hcv_fail">' + fee_service_codes[fsc][variable] + '</td>';
                                            }
                                        }
                                        row += '</tr>';
                                        table.after(row);
                                    }
                                }
                            }

                            var message = rs.response_action + ' <br><b>' + rs.response_message + '</b>';
                            var patient_dob = new Date(person['dateOfBirth'])
                            var patient_expires;

                            message = person['lastName'] + ', ' + person['firstName'] + '<br>';

                            if (person['expiryDate'] != null) {
                                patient_expires = new Date(person['expiryDate']);
                                message += rs.response_code + ' : ' + rs.response_action + ' <br>DOB: ' + patient_dob.toDateString() + ' expires: ' + patient_expires.toDateString();
                            } else {
                                patient_expires = true;
                                message += rs.response_code + ' : ' + rs.response_action + ' <br>DOB: ' + patient_dob.toDateString();
                            }
                            
                            var new_content = '';
                            var actual_content = '';
                            if (rs.hasOwnProperty('fee_service_details')) {
                                message += '<hr>';
                                for (var index = 0; index < rs['fee_service_details'].length; index++) {
                                    actual_content += '<tr id="fee_details"><td>&nbsp;</td><td class="' + rs.class_name + '">';
                                    for (var a in rs['fee_service_details'][index]) {

                                        if (!rs['fee_service_details'][index][a])
                                            continue;
                                        if (a == 'feeServiceCode') {
                                            console.log(rs['fee_service_details'][index]['feeServiceDate']);
                                            if(rs['fee_service_details'][index]['feeServiceDate'] != null)
                                            {
                                             rs['fee_service_details'][index][a] = '<b>'+rs['fee_service_details'][index][a].substr(0, 4)+'</b>';   
                                            } else {
                                             rs['fee_service_details'][index][a] = rs['fee_service_details'][index][a].substr(0, 4);
                                            }
                                        } else if (a == 'feeServiceDate') {
                                            rs['fee_service_details'][index][a] = '<b>'+new Date(rs['fee_service_details'][index][a]).toDateString()+'</b>';
                                        }

                                        new_content += rs['fee_service_details'][index][a] + ' ';
                                        message += rs['fee_service_details'][index][a] + ' ';
                                    }
                                    message += '<br>';
                                    actual_content += new_content + '</td></tr>';
                                    new_content = '';
                                    $('#hcv_validation').after(actual_content);
                                    actual_content = '';
                                }
                            }


                            //message = person.firstName + ', ' + person.lastName + ': ' + person.responseDescription + ' <br>' + person.responseAction + ' <br>' + 'ID: ' + person.responseID;


                            $('.hcv_check_text').text(global_js.todayDate() + ': ' + rs.response_message).addClass(rs.class_name).show();
                           
                            if(isvalidatepopup != 'false'){ 
                                if (rs.class_name == 'hcv_pass') {
                                    sk.skNotify(message, 'success');
                                    $('#patient_data_row .hcv_res_color').addClass('sk-text-green');
                                } else {
                                    sk.skNotify(message, 'danger');
                                    
                                    $('#patient_data_row .hcv_res_color').addClass('sk-text-danger');
                                }
                            }

                            $('.ajs-message').hover(function () {
                                msg.delay(5);
                            });
                            
                        } else if (rs.hasOwnProperty('mod_ten')) {
                            
                            $('.hcv_check_text').text(global_js.todayDate() + ': ' + rs['response_message']);
                            $('.hcv_check_text').removeClass('sk-text-green').removeClass('sk-text-danger');
                            $("#patient_data_row .hcn").removeClass('sk-text-green').removeClass('sk-text-danger');
                            if( rs.class_name == 'hcv_mod10_pass'){
                                $('.hcv_check_text').addClass('sk-text-green');
                                $("#patient_data_row .hcn").addClass('sk-text-green');
                            } else {
                                 $('.hcv_check_text').addClass('sk-text-danger');
                                $("#patient_data_row .hcn").addClass('sk-text-danger');
                            }
                        } else if (rs.hasOwnProperty('status')) {
                            
                            original_class = original_class.replace('patient ', '').replace('hcv_check_text', '');
                            var message = hcn + ' ' + vc + ' has already been checked with the Ministry of Health today';
                            $('.hcv_check_text').text(global_js.todayDate() + ': ' + message);
                            $('.hcv_check_text').addClass(original_class).show();
                            $("#patient_data_row .hcn").addClass(original_class);
                            
                        }
                    },
                    beforeSend: function () {
                        $('#' + patient_search.messages.checkmark).hide();
                        $('#' + patient_search.messages.error).hide();
                        $('#' + patient_search.messages.spinner).show();
                        $('#no_results_messages_checkmark').hide();
                        $('#no_results_messages_spinner').show();
                        $('#no_results_messages_text').text('Validating Health Card').show();
                        $('#messages_text').text('Validating Health Card').show();
                    },
                    complete: function () {
                        $('#' + patient_search.messages.spinner).hide();
                        $('#no_results_messages_spinner').hide();
                        $('#no_results_messages_checkmark').show();
                        $('#no_results_messages_text').text('Complete').show();
                    },
                    error: function () {
                        $('#' + patient_search.messages.error).show();
                        $('#' + patient_search.messages.text).text('An error occurred while contacting the server.');
                    }
                });
            }
        },
        clearDragged: function () {
            calendar_events.dragged_event = false;
            calendar_events.current_event = null;
            calendar_events.doctors_appt.reschedule_event = null;
            calendar_events.doctors_appt.reschedule_appointment_id = null;
        }
    },
    other_appt: {
        appointment_types: {},
        drawForm: function (calEvent, calendar) {
            calendar_events.other_appt.resetDialog();
            calendar_events.other_appt.setupAppointmentTypes(calEvent.doctor_id, calEvent.location_id);

            if (calEvent.id) {
                for (var a in calEvent) {
                    switch (a) {
                        case 'id':
                        case 'location_id':
                        case 'doctor_id':
                        case 'appointment_attendee_id':
                        case 'length':
                        case 'appointment_type_id':
                        case 'slot_type':
                        case 'notes':
                        case 'repeat_end_date':
                        case 'patient_id':
                            $('#doctor_appointment_form #appointment_' + a).val(calEvent[a]);
                            break;
                        case 'appointment_template_id':
                            $('#doctor_appointment_form #appointment_appointment_template_id').val(calEvent[a]);
                            if (calEvent[a]) {
                                $('#doctor_appointment_form .repeating_existing_event').show();
                                $('#doctor_appointment_form #appointment_apply_to_all_events').prop('checked', true);
                                calendar_events.doctors_appt.handleApplyToChange();
                            }
                            break;
                        case 'start':
                            $('#doctor_appointment_form #appointment_start_date').val(calEvent.start.toString('yyyy-MM-dd HH:mm'));
                            $('#doctor_appointment_form #appointment_start_time_hour').val(calEvent.start.toString('h'));
                            $('#doctor_appointment_form #appointment_start_time_minute').val(calEvent.start.toString('m'));
                            $('#doctor_appointment_form #appointment_start_time_ampm').val(calEvent.start.toString('tt').toLowerCase());
                            break;
                        case 'repeats':
                            $('#doctor_appointment_form #appointment_repeats').prop('checked', calEvent.repeats);
                            if (calEvent.repeats) {
                                calendar_events.doctors_appt.showAppointmentTemplateParameters();
                            }
                            break;
                        case 'repeat_weekdays':
                            for (var a in calEvent.repeat_weekdays) {
                                $('#doctor_appointment_form #appointment_repeat_weekdays_' + a).prop('checked', calEvent.repeat_weekdays[a]);
                            }
                            break;
                    }
                }

                calendar_events.other_appt.selectAppointmentType(calEvent.appointment_type);
                calendar_events.other_appt.updateAppointmentTypeClasses(calEvent.appointment_type);
                $('#non_doctor_cancel_button').show();
//				$('#doctor_reschedule_button').show();
            } else {
                $('#non_doctor_cancel_button').hide();
                $('#doctor_reschedule_button').hide();
                $('#doctor_appointment_form #appointment_location_id').val(calEvent.location_id);
                $('#doctor_appointment_form #appointment_doctor_id').val(calEvent.doctor_id);
                $('#doctor_appointment_form #appointment_start_date').val(calEvent.start.toString('yyyy-MM-dd HH:mm'));
                $('#doctor_appointment_form #appointment_start_time_hour').val(calEvent.start.toString('h'));
                $('#doctor_appointment_form #appointment_start_time_minute').val(calEvent.start.toString('m'));
                $('#doctor_appointment_form #appointment_start_time_ampm').val(calEvent.start.toString('tt').toLowerCase());

                var wkday = calEvent.start.toString('ddd').toLowerCase();
                wkday = (wkday == 'tue' ? 'tues' : (wkday == 'thu' ? 'thurs' : wkday));

                $('#doctor_appointment_form #appointment_repeat_weekdays_' + wkday).prop('checked', true);
            }

            $('#doctor_appointment_form #appointment_repeat_end_date').datepick('option', 'defaultDate', calEvent.start.toString(quickClaim.getParseDateFormat(quickClaim.dateFormat)));
            calendar_events.current_event = calEvent;

            // text fields
            $('#doctor_appointment_form .location_name').text($('label[for=location_select_' + calEvent.location_id + ']').text());
            $('#doctor_appointment_form .doctor_name').text($('label[for=doctor_select_' + calEvent.location_id + '_' + calEvent.doctor_id + ']').text());
            $('#doctor_appointment_form .date').text(calEvent.start.toString('MMMM d, yyyy').toUpperCase());
            $('#doctor_appointment_form .time').text(calEvent.start.toString('H:mm').toUpperCase());

            if (!calEvent.appointment_type) {
                $('label').removeClass('ui-state-focus ui-state-hover ui-state-active');
            }

            $('#doctor_appointment_form_dialog').dialog('open');
        },
        resetDialog: function () {
            $('#doctor_appointment_form .patient_field').hide();
            $('#doctor_appointment_form .non_patient_field').hide();

            calendar_events.patient_search.hidePatientSearch();
            calendar_events.patient_search.hidePatientDetails();
            calendar_events.doctors_appt.hideAppointmentTemplateParameters();
            $('#doctor_appointment_form').clearForm();

            $('#doctor_appointment_form #appointment_id').val('');
            $('#doctor_appointment_form #appointment_location_id').val('');
            $('#doctor_appointment_form #appointment_doctor_id').val('');
            $('#doctor_appointment_form #appointment_appointment_attendee_id').val('');
            $('#doctor_appointment_form #appointment_length').val('');
            $('#doctor_appointment_form #appointment_appointment_type_id').val('');
            $('#doctor_appointment_form #appointment_slot_type').val('');
            $('#doctor_appointment_form #appointment_notes').val('');
            $('#doctor_appointment_form #appointment_patient_id').val('');
            $('#doctor_appointment_form #appointment_start_date').val('');
            $('#doctor_appointment_form #appointment_start_time_hour').val('');
            $('#doctor_appointment_form #appointment_start_time_minute').val('');
            $('#doctor_appointment_form #appointment_start_time_ampm').val('');

            $('#patient_data_row .fname').text('');
            $('#patient_data_row .lname').text('');
            $('#patient_data_row .hphone').text('');
            $('#patient_data_row .hcn').text('');
            $('#patient_data_row .vc').text('');
            $('#patient_data_row .hcv_check_text').hide();
            $('#patient_data_row .patient_location_name').text('');
            $('#patient_data_row .dob').text('');
            $('#patient_data_row .patient_id').text('');

            $('#doctor_appointment_form .location_name').text('');
            $('#doctor_appointment_form .doctor_name').text('');
            $('#doctor_appointment_form .date').text('');
            $('#doctor_appointment_form .appt_type').text('');

            $('#patient_details_table .search').hide();
            $('.appointment_template_patient').hide();
            $('#appointment_types_td').html('');

            $('#doctor_appointment_form label.error').remove();
            $('#doctor_appointment_form .error').removeClass('error');
            $('#doctor_form_errors').text('').hide();
        },
        setupAppointmentTypes: function (doctor_id, location_id) {
            appt_types = calendar_events.other_appt.appointment_types;

            if (calendar_events.complex_appointment_types) {
                var province_code = $('#location_select_' + location_id).parent('li').parent('ul').parent('li').attr('class').replace('province_', '');
                appt_types = appt_types[province_code];
            }

            var ul = '<ul class="radio_list">';
            for (var a in appt_types) {
                for (var b in appt_types[a].lengths) {
                    var id = a + '_' + appt_types[a].slot_type + '_' + appt_types[a].lengths[b];
                    var label = appt_types[a].name + ' [' + appt_types[a].lengths[b] + ' min]';

                    ul += '<li>';
                    ul += '<input type="radio" name="appointment[appointment_type]" id="appointment_appointment_type_' + id + '" value="' + id + '" />';
                    ul += '<label for="appointment_appointment_type_' + id + '">' + label + '</label>';
                    ul += '</li>';
                }
            }

            ul += '</ul>';

            $('#appointment_types_td').html($(ul));
            calendar_events.other_appt.setupAppointmentTypeButtons();
        },
        setupAppointmentTypeButtons: function () {
            $('input[name="appointment[appointment_type]"]').button();
            calendar_events.other_appt.setAppointmentTypeClasses();
        },
        setAppointmentTypeClasses: function () {
            $('input[name="appointment[appointment_type]"]').each(function () {
                var appt_type = $(this).attr('id').replace('appointment_appointment_type_', '');
                appt_type = appt_type.split('_');
                $('label[for=' + $(this).attr('id') + ']').addClass('appointment_type_' + appt_type[0]);
            });

            $('input[name="appointment[appointment_type]"]').click(function () {
                calendar_events.other_appt.updateAppointmentTypeClasses($(this).val());
            });
        },
        updateAppointmentTypeClasses: function (val) {
            var terms = val.split('_');
            var appointment_type_id = terms[0];
            var slot_type = terms[1];
            var length = terms[2];
            var doctor_id = '';

            $('#doctor_appointment_form .appt_type').text(calendar_events.appointment_types[appointment_type_id]);
            $('#doctor_appointment_form .appointment_type_name').text(calendar_events.appointment_types[appointment_type_id].toLowerCase());
            $('#doctor_appointment_form #appointment_length').val(length);
            $('#doctor_appointment_form #appointment_slot_type').val(slot_type);
            $('#doctor_appointment_form #appointment_appointment_type_id').val(appointment_type_id);

            $('#doctor_appointment_form .patient_field').hide();
            $('#doctor_appointment_form .non_patient_field').show();
            $('#doctor_appointment_form .repeats').show();
        },
        selectAppointmentType: function (val) {
            if ($('#doctor_appointment_form label[for=appointment_appointment_type_' + val + ']').size()) {
                $('#doctor_appointment_form label[for=appointment_appointment_type_' + val + ']').click();
                $('#appointment_appointment_type_' + val).prop('selected', true);
                return;
            }

            var terms = val.split('_');
            var match_str = terms[0] + '_' + terms[1] + '_';

            $('#doctor_appointment_form input[name="appointment[appointment_type]"]').each(function () {
                var val = $(this).val();
                if (val.indexOf(match_str) != -1) {
                    $('#doctor_appointment_form label[for=appointment_appointment_type_' + val + ']').click();
                    $('#appointment_appointment_type_' + val).prop('selected', true);
                    return;
                }
            });
        },
        cancelAppointment: function () {
            $('#doctor_appt_cancel_form .non_patient_field').hide();
            if ($('#appointment_id').val() && $('#appointment_repeats').prop('checked')) {
                $('#doctor_appt_cancel_form .non_patient_field').show();
            }

            $('#doctor_appt_cancel_form').clearForm();
            $('#appointment_cancel_id').val($('#appointment_id').val());
            $('#doctor_appt_cancel_form_dialog').dialog('open');
        }
    },
    patient_search: {
        check_patient_status: true,
        bookable_patient_statuses: {},
        ajax_lock: false,
        autocomplete: null,

        initialize: function () {
            calendar_events.patient_search.hideClearButton();
            calendar_events.patient_search.buildCancelButton();

            $('#no_results_dialog').dialog({
                position: {my: "center", at: "center", of: window},
                closeOnEscape: false,
                autoOpen: false,
                width: 700,
                close: function () {
                    $('#new_patient_form').hide();
                },
                open: function () {
                    setTimeout(function () {
                        calendar_events.patient_search.initAutocomplete();
                        calendar_events.patient_search.geolocate();
                    }, 1000);
                    $(".cliponeErrorInput").removeClass('cliponeErrorInput');
                    $("#create_patient_errors").html('').removeClass('alert').removeClass('alert-danger');
                    $("#no_results_messages_text").html('');
                }
            });

//            $('#simple_patient_dob').datepick({
//                dateFormat: quickClaim.dateFormat,
//                renderer: $.datepick.themeRollerRenderer
//            });

            sk.datepicker('#simple_patient_dob');

            $('#patient_create_cancel').button();
            $('#patient_create_cancel').click(function () {
                $('#no_results_dialog').dialog('close');
            });

            $('#patient_create_save').button();
            $('#patient_create_save').click(function () {
                $(".cliponeErrorInput").removeClass('cliponeErrorInput')
                calendar_events.patient_search.handleCreateSave();
            });

            $('#patient_search_create_from_terms').click(function () {
                calendar_events.patient_search.handleNoSearchResultDialog();
            });
        },
        fillInAddress: function () {
            var place = calendar_events.patient_search.autocomplete.getPlace();

            var route = '';
            var street_number = '';
            var locality = '';
            var postal_code = '';
            var province = '';

            for (var x = 0; x <= place.address_components.length; x++) {
                if (place.address_components[x]) {
                    if (place.address_components[x].types[0]) {

                        switch (place.address_components[x].types[0]) {
                            case 'route':
                                route = place.address_components[x].short_name;
                                break;

                            case 'street_number':
                                street_number = place.address_components[x].short_name;
                                break;

                            case 'locality':
                                locality = place.address_components[x].short_name;
                                break;

                            case 'postal_code':
                                postal_code = place.address_components[x].short_name;
                                break;

                            case 'administrative_area_level_1':
                                province = place.address_components[x].short_name;
                                break;
                        }

                    }
                }
            }


            $('#simple_patient_address').val(place.name);

            if (postal_code) {
                $('#simple_patient_postal_code').val(postal_code);
            }
            if (locality) {
                $('#simple_patient_city').val(locality);
            }
            if (province) {
                $('#simple_patient_province').val(province).trigger('change');
            }


        },
        initAutocomplete: function () {

            calendar_events.patient_search.autocomplete = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */(document.getElementById('simple_patient_address')));

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            zones.initialize('Toronto, Ontario, Canada', 10, false);
            zones.zoomToBounds();
            calendar_events.patient_search.autocomplete.addListener('place_changed', calendar_events.patient_search.fillInAddress);
        },
        geolocate: function () {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    calendar_events.patient_search.autocomplete.setBounds(circle.getBounds());
                });
            }
        },

        buildCancelButton: function () {
            var button = '<button type="button" class="btn btn-danger btn-squared" id="patient_search_cancel"><i class="clip-cancel-circle"></i> Cancel</button>';
            $('#search_button_clear').parent().append($(button));

            $('#patient_search_cancel').button();
            $('#patient_search_cancel').click(function () {
                calendar_events.patient_search.showPatientDetails();
            });
        },
        canBookPatient: function (status_id) {
            if (calendar_events.patient_search.check_patient_status) {
                return calendar_events.patient_search.bookable_patient_statuses.hasOwnProperty(status_id);
            }
            return true;
        },
        getCurrentAddress: function () {
            if (calendar_events.doctors_appt.patient_from_profile) {
                return calendar_events.doctors_appt.patient_from_profile.address + ', '
                        + calendar_events.doctors_appt.patient_from_profile.city + ', '
                        + calendar_events.doctors_appt.patient_from_profile.province + ', '
                        + calendar_events.doctors_appt.patient_from_profile.postal_code;
            }

            return '';
        },
        handleClearPress: function () {
        },
        hideClearButton: function () {
            $('#search_button_clear').hide();
        },
        handleCreateSave: function () {
            if (calendar_events.patient_search.ajax_lock) {
                return;
            }

            var data = $('#patient_create_form').serialize();

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: calendar_events.actions.create_patient,
                data: data,
                success: function (rs) {
                    if (rs.hasOwnProperty('errors')) {
                        quickClaim.showFormErrors(rs.errors, '#create_patient_errors', '#simple_patient', true);
                        $('#no_results_messages_text').text('Form errors detected.').show();
                        quickClaim.focusTopField('new_patient_form');
                    } else {
                        calendar_events.patient_search.loadData(rs.data[0]);
                        $('#no_results_dialog').dialog('close');
                    }
                    quickClaim.hideOverlayMessage(500);
                },
                beforeSend: function () {
                    quickClaim.showOverlayMessage('Saving Patient . . .');
                    calendar_events.patient_search.ajax_lock = true;
                    $('#no_results_messages_checkmark').hide();
                    $('#no_results_messages_error_icon').hide();
                    $('#no_results_messages_spinner').show();

                    $('#no_results_messages_text').text('Saving Patient').show();
                },
                complete: function () {
                    calendar_events.patient_search.ajax_lock = false;
                    $('#no_results_messages_spinner').hide();
                },
                error: function () {
                    calendar_events.patient_search.ajax_lock = false;
                    $('#no_results_messages_error_icon').show();
                    $('#no_results_messages_text').text('An error occurred while contacting the server.');
                }
            });
        },
        hidePatientDetails: function () {
            $('#patient_details_table').hide();
            $('.appointment_template_patient').hide();
        },
        hidePatientSearch: function () {
            $('#patient_search_form').hide();
        },
        handleNoSearchResultDialog: function () {
            $("#patient_create_form").trigger('reset');
            $("#patient_create_form select").trigger('change');
            $('#no_results_dialog').dialog('open');
            this.showCreateForm();
        },
        loadData: function (data) {
            if (calendar_events.patient_search.canBookPatient(data.patient_status_id)) {
                $('#patient_data_row .fname').text(data.fname);
                $('#patient_data_row .lname').text(data.lname);
                $('#patient_data_row .patient_number').text(data.patient_number);
                $('#patient_data_row .last_first').text(data.last_first);
                $('#patient_data_row .hphone').text(data.hphone);
                $('#patient_data_row .hcn').text(data.health_num);
                $('#patient_data_row .vc').text(data.version_code);
                $('#patient_data_row .version_code').text(data.version_code);
                $('#patient_data_row .patient_location_name').text(data.location_name);
                $('#patient_data_row .dob').text(data.dob);
                $('#patient_data_row .patient_id').text(data.patient_id);
                $('#doctor_appointment_form #appointment_patient_id').val(data.patient_id);

                var clippy = new Clipboard('#doctor_submit_button', {
                    text: function (trigger) {
                        var lastName = data.lname;
                        return lastName;
                    }
                });

                // var claim_facility_num = data.facility_num;
                // var d = new Date();
                // var newDate = d.toString('yyyy-MM-dd');
                // if(data.hcv_last_checked != newDate)
                // {
                //     if (data.health_num && data.hcv_status) {
                //         $('.hcv_check_text').removeClass('hcv_pass').removeClass('hcv_fail').removeClass('hcv_warn').removeClass('hcv_mod10_pass').hide();

                //         $('#patient_data_row .hcv_check_text').text(data.hcn_check_date + ': ' + data.hcv_status).addClass(data.hcv_status_class).show();

                //     }
                // }

                var claim_facility_num = data.facility_num;
                var d = new Date();
                var newDate = d.toString('yyyy-MM-dd');
                if(data.hcv_last_checked == newDate)
                {
                   calendar_events.doctors_appt.hcvRequest($('#patient_data_row .hcn').text(), $('#patient_data_row .version_code').text(), $('#appointment_patient_id').val(),'','false');
  
                }
                else
                {
                    calendar_events.doctors_appt.hcvRequest($('#patient_data_row .hcn').text(), $('#patient_data_row .version_code').text(), $('#appointment_patient_id').val());
                }

                // if (calendar_events.actions.hcv) {
                //     console.log('-- -- -- --');
                //     calendar_events.doctors_appt.hcvRequest($('#patient_data_row .hcn').text(), $('#patient_data_row .vc').text(), $('#appointment_patient_id').val());
                // }

                calendar_events.setupTimeline(data.patient_id);

                if (calendar_events.doctors_appt.include_recent_notes) {
                    $('#doctor_appointment_form #appointment_notes').val(data.recent_note);
                }
                calendar_events.patient_search.showPatientDetails();

                $('#patient_search').dialog('close');
            } else {
                alert('You cannot book patients with the status ' + data.patient_status);
                return false;
            }

            return true;
        },
        setFocus: function () {
            return;
        },
        showCreateForm: function () {
            $('#new_patient_form').show();

            var cache = patient_search.edit_target.search_cache;

            for (var idx in cache) {
                if ($('#new_patient_form #simple_patient_' + idx).size()) {
                    $('#new_patient_form #simple_patient_' + idx).val(cache[idx].toUpperCase());
                }
            }
            $('#new_patient_form #simple_patient_location_id').val($('#appointment_location_id').val());

            $('#new_patient_form #simple_patient_hcn_num').focus();
            return false;
        },
        showPatientSearchForm: function () {
            $('#patient_search_form').show();
            $('#patient_details_table').hide();
            $('.appointment_template_patient').hide();
            var form = document.querySelector('#patient_search_form');
            Mousetrap(form).bind('enter', function (e) {
                $("#search_button_patient").trigger('click');
            });
        },
        showPatientDetails: function () {
            $('#patient_search_form').hide();
            $('#patient_details_table').show();
            $('.appointment_template_patient').show();
            var form = document.querySelector('#patient_search_form');
            //Mousetrap.unbind(form, 'enter');
        }
    },
    showHeaderOverlay: function (text, patient_id) {
        $.notify(text, {
            element: 'body',
            type: "success",
            allow_dismiss: true,
            newest_on_top: true,
            placement: {
                from: "top",
                align: "left"
            },
            delay: false,
            //delay: 3000,
            z_index: 99999,
            mouse_over: 'pause',
            animate: {
                enter: 'animated bounceIn',
                exit: 'animated bounceOut'
            },
            template: `<div data-notify="container" class="col-xs-11 col-sm-2 alert alert-{0}" role="alert">
                                                    <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
                                                    <span data-notify="icon"></span>
                                                    <span data-notify="title">{1}</span>
                                                    <span data-notify="message">{2}</span>
                                                    <div class="progress" data-notify="progressbar">
                                                            <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                                    </div>
                                                    <a href="{3}" target="{4}" data-notify="url"></a>
                                            </div>`

        });
//        $('#calendar_header_text').html(text);
//        $('#calendar_header_overlay').show();
//        $('#calendar_header_div').show();
//        $('#calendar_header_overlay').height($('#calendar_header_div').height());
    },
    hideHeaderOverlay: function () {
        $('#calendar_header_text').text('');
        $('#calendar_header_overlay').hide();
        $('#calendar_header_div').hide();
    },
    handleEscapePress: function () {
        if ($('#no_results_dialog').dialog('isOpen')) {
            $('#no_results_dialog').dialog('close');
            return;
        }
        if ($('#patient_search').dialog('isOpen')) {
            $('#patient_search').dialog('close');
            return;
        }

        if ($('#consultation_more_patients_list').dialog('isOpen')) {
            $('#consultation_more_patients_list').dialog('close');
            return;
        }

        if ($('#doctor_appt_cancel_form_dialog').dialog('isOpen')) {
            $('#doctor_appt_cancel_form_dialog').dialog('close');
            return;
        }

        if ($('#doctor_appt_reschedule_form_dialog').dialog('isOpen')) {
            $('#doctor_appt_reschedule_form_dialog').dialog('close');
            return;
        }

        if ($('#doctor_appointment_form_dialog').dialog('isOpen')) {
            $('#doctor_appointment_form_dialog').dialog('close');
            return;
        }

        if ($('#consultation_cancel_form_dialog').dialog('isOpen')) {
            $('#consultation_cancel_form_dialog').dialog('close');
            return;
        }

        if ($('#consultation_reschedule_form_dialog').dialog('isOpen')) {
            $('#consultation_reschedule_form_dialog').dialog('close');
            return;
        }

        if ($('#consultation_form').dialog('isOpen')) {
            $('#consultation_form').dialog('close');
            return;
        }
    }
};
