var calendar_sidebar = {

	minical: 'minical',
	consultation_dates: [],
	doctor_dates: [],
	last_used_params: null,

	initialize: function() {

		$('#sidebar_select_all_provinces').click(function () {
			calendar_sidebar.selectAll($(this).attr('id'));
		});
		$('#sidebar_select_all_regions').click(function () {
			calendar_sidebar.selectAll($(this).attr('id'));
		});
		$('#sidebar_select_all_locations').click(function () {
			calendar_sidebar.selectAll($(this).attr('id'));
			if($(this).is(':checked')){
				$('.location_select').attr('checked',true);
				$('.doctor_select').attr('checked',true);
				calendar_sidebar.locationLocalstorage();
			}else{
				$(this).attr('checked',false);
				$('.location_select').attr('checked',false);
				$('.doctor_select').attr('checked',false);
				localStorage.removeItem('doctors');
				localStorage.removeItem('locations');
			}
		});
		$(".location_select_").change(function(){
			if($('.location_select:checked').length == $('.location_select').length){ 
		       	$('#sidebar_select_all_locations').attr('checked',true);
			}else{ 
		       	$('#sidebar_select_all_locations').attr('checked',false);
			}
		});


        if(calendar_sidebar.patient_location_id == undefined && calendar_sidebar.currentLocation != undefined)
        {   
             $('#location_select_'+calendar_sidebar.currentLocation).prop('checked', true);
        } 
        else  if(calendar_sidebar.patient_location_id != undefined)
        {	
            $('#location_select_'+calendar_sidebar.patient_location_id).prop('checked', true);
        }
        if($('.location_select').length == 1 && $('.location_select:checked').length == 0)
        {
        	  $('.location_select').prop('checked', true);
        }

        //alert($('.location_select').length);

        //alert($('.location_select:checked').length);
        

		//$('#location_select_'+calendar_sidebar.currentLocation).prop('checked', true);
		//calendar_sidebar.setActiveLocation(65);
		$(".doctor_select").click(function(){
			var locId = $(this).val();
			var parentVal = locId.split('_');
			 
			
		    if ($('.doctor_select:checked').length == $('.doctor_select').length) {
		       	$('#sidebar_select_all_locations').attr('checked',true);
				$('.location_select').attr('checked',true);
		    }
		    
		    if($('ul#doctors_'+parentVal[0]+' input:checked').length == $('ul#doctors_'+parentVal[0]+' input').length){ 
		    	$('#location_select_'+parentVal[0]).attr('checked',true);

		    	var locationsValue = parentVal[0];
				if(localStorage.getItem('locations')){
					
	            	var arr = [];
				    arr = JSON.parse(localStorage.getItem('locations'));			    
				    arr.push(locationsValue);
				    
				    localStorage.setItem('locations', JSON.stringify(arr));

				}else{
	            	
	            	var newarr = [];
	            	newarr[0] = locationsValue;
	            	localStorage.setItem('locations', JSON.stringify(newarr));
	            	sessionStorage.setItem("sessionLocations", "added");

				}
			}else{ 
		    	$('#location_select_'+parentVal[0]).attr('checked',false);

		    	if(localStorage.getItem('locations')){
				
	            	var l = [];
	            	var newloc = [];
	            	var index = 0;
				    l = JSON.parse(localStorage.getItem('locations'));			    
				    for( var i in l){
				    	
				    	if(l[i] == parentVal[0]){
				    		continue;
				    	}

				    	newloc[index] = l[i];
				    	index++;
				    }

		            	localStorage.setItem('locations', JSON.stringify(newloc));		    

				}
			}


			if($('.location_select:checked').length == $('.location_select').length){ 
		       	$('#sidebar_select_all_locations').attr('checked',true);
			}else{ 
		       	$('#sidebar_select_all_locations').attr('checked',false);
			}

			if($(this).is(':checked')){

				var values = locId;
				var selectecLocaton = "";
				if(localStorage.getItem('doctors')){
					
	            	var a = [];
				    a = JSON.parse(localStorage.getItem('doctors'));			    
				    a.push(values);
				    
				    localStorage.setItem('doctors', JSON.stringify(a));

				}else{
	            	
	            	var arr = [];
	            	arr[0] = values;
	            	localStorage.setItem('doctors', JSON.stringify(arr));
	            	sessionStorage.setItem("sessionDoctors", "added");	            	

				}
			}else{
				if(localStorage.getItem('doctors')){
				
	            	var a = [];
	            	var newArr = [];
	            	var index = 0;
				    a = JSON.parse(localStorage.getItem('doctors'));			    
				    for( var i in a){
				    	
				    	if(a[i] == locId){
				    		continue;
				    	}

				    	newArr[index] = a[i];
				    	index++;
				    }

		            	localStorage.setItem('doctors', JSON.stringify(newArr));		    

				}
				
			}

		   
		});

		$("#regions_form input.province_select").click(function() {
			calendar_sidebar.setActiveProvince($(this).val());
		});

		$('#regions_form li .province_icon').click(function () {
			calendar_sidebar.toggleProvince($(this));
		});

		$('#regions_form input[name="region_select"]').click(function () {
			calendar_sidebar.setActiveRegion($(this).val());
		});

		$('#regions_form .region_icon').click(function () {
			calendar_sidebar.toggleRegion($(this));
		});

		$('#regions_form input[type=checkbox]').click(function () {
			calendar_sidebar.setActiveLocation($(this).val());
		});

		$('#regions_form .location_icon').click(function () {
			calendar_sidebar.toggleLocation($(this));
		});

		$('#mini_calendar .minical_icon').click(function () {
			calendar_sidebar.toggleMiniCal($(this));
		});

		$('.regions_div_icon').click(function () {
			calendar_sidebar.toggleRegionsDiv($(this));
		});

		$('.options_div').click(function () {
			calendar_sidebar.toggleCalendarOptionsDiv($(this));
		});

		
		$('#appointment_slots_button')
			.button()
			.click(function() {
				calendar_events.last_requested_check = new Date().getTime();
                                var v = $('#time_increments').val();
                                calendar_events.defaults.timeslotsPerHour = v;
                                $('#' + calendar_events.calendar).weekCalendar('option', 'timeslotsPerHour', v);
                                $('#' + calendar_events.calendar).weekCalendar('refresh');
			});

                   
                var dateformat = global_js.getNewDateFormat();
                sk.datepicker('#' + this.minical);
                
               $('#' + this.minical).on('changeDate', function(event) {
//                   alert(event.date); 
                   $('#calendar').weekCalendar('gotoDate', event.date);
               });


               $('#regions_button')
			.button()
			.click(function() {
				calendar_events.last_requested_check = new Date().getTime();
				$('#calendar').weekCalendar('refresh');
			});
               

	},
	locationLocalstorage: function(){
		var a = []; 
			if($('#sidebar_select_all_locations').is(':checked')){
				var d = [];	
				$('#regions_form input[type=checkbox].location_select').each(function () {
						if(localStorage.getItem('locations')){
							localStorage.removeItem('locations');
							var loc = $(this).val();
					    	a.push(loc);
					    }else{
							
							var loc = $(this).val();
						    a.push(loc);
							
						}

					if(localStorage.getItem('doctors')){
						localStorage.removeItem('doctors');

													
						$('ul#doctors_' + loc + ' li input.doctor_select').each(function () {

								var doc = $(this).val();
							    d.push(doc);
						});

					}else{
						$('ul#doctors_' + loc + ' li input.doctor_select').each(function () {
							// console.log($(this).val());
								var doc = $(this).val();
							    d.push(doc);
						});
					}


				});

				localStorage.setItem('doctors', JSON.stringify(d));

				localStorage.setItem('locations', JSON.stringify(a));
			}
	},
	getMiniCalDate: function() {
		var minical_date = $('#' + calendar_sidebar.minical + ' .datepick-month-year').val();
		if (minical_date) {
			minical_date = minical_date.substring(minical_date.indexOf('/') + 1) + '-' + minical_date.substring(0, minical_date.indexOf('/')) + '-01';
		}
		else {
			minical_date = '';
		}
		return minical_date;
	},
	getActiveLocations: function() {
		var rs = Array();
		var a = 0;

		$('.location_select').each(function () {
			if (calendar_sidebar.isActiveLocation($(this).val())) {
				rs[a++] = $(this).val();
			}
		});

		return rs;
	},
	getActiveDoctors: function() {
		var rs = Array();
		var a = 0;

		$('.doctor_select').each(function () {
			if (calendar_sidebar.isActiveDoctor($(this).val())) {
				rs[a++] = $(this).val();
			}
		});

		return rs;
	},
	isActiveLocation: function(e) {
		if ($('#location_select_' + e + ':checked').size()) {
			return true;
		}

		return false;
	},
	isActiveDoctor: function(e) {
		if ($('#doctor_select_' + e + ':checked').size()) {
			return true;
		}

		return false;
	},
	selectAll: function(id) {
		var checked = $('#' + id).prop('checked');
		if ($('ul#provinces li input.province_select').size()) {
			$('ul#provinces li input.province_select').each(function () {
				$(this).prop('checked', checked);
				calendar_sidebar.setActiveProvince($(this).val());
			});
		}
		else if ($('ul.regions li input.region_select').size()) {
			$('ul.regions li input.region_select').each(function () {
				$(this).prop('checked', checked);
				calendar_sidebar.setActiveRegion($(this).val());
			});
		}
		else if ($('ul.locations li input.location_select').size()) {
			$('ul.locations li input.location_select').each(function () {
				$(this).prop('checked', checked);
				calendar_sidebar.setActiveLocation($(this).val());
			});
		}
	},
	setActiveLocation: function(location) {
		if (location) {
			var checked = $('#location_select_' + location).prop('checked');
			$('ul#doctors_' + location + ' li input.doctor_select').each(function () {
				$(this).prop('checked', checked);
			});
			var a = []; 
			if($('#location_select_' + location).is(':checked')){
				
				if(localStorage.getItem('locations')){
				    a = JSON.parse(localStorage.getItem('doctors'));			    

					
	            	var arr = [];
				    arr = JSON.parse(localStorage.getItem('locations'));			    
				    arr.push(location);
				    
				    localStorage.setItem('locations', JSON.stringify(arr));

				}else{
	            	
	            	var newarr = [];
	            	newarr[0] = location;
	            	localStorage.setItem('locations', JSON.stringify(newarr));
	            	sessionStorage.setItem("sessionLocations", "added");

				}

				localStorage.removeItem('doctors');
				$('ul#doctors_' + location + ' li input.doctor_select').each(function () {
					var doc = $(this).val();					   			    
				    a.push(doc);			    
				    					
				});
				localStorage.setItem('doctors', JSON.stringify(a));

			}else{
				if(localStorage.getItem('locations')){
				
	            	var l = [];
	            	var newloc = [];
	            	var index = 0;
				    l = JSON.parse(localStorage.getItem('locations'));			    
				    for( var i in l){
				    	// localStorage.removeItem("location");
				    	// console.log(a[i]);
				    	if(l[i] == location){
				    		continue;
				    	}

				    	newloc[index] = l[i];
				    	index++;
				    }

		            	localStorage.setItem('locations', JSON.stringify(newloc));		    

				}
				var newAr = [];
	            var index = 0;
				$('ul#doctors_' + location + ' li input.doctor_select').each(function () {
						var doct = $(this).val();						

					    var d = JSON.parse(localStorage.getItem('doctors'));
						for (i=0;i<d.length;i++){
			            	if (d[i] == doct) d.splice(i,1);
						}
						localStorage["doctors"] = JSON.stringify(d);
		           
				});
			}
		}
	},
	setActiveProvince: function(prov) {
		if (prov) {
			var checked = $('#province_select_' + prov).prop('checked');
			$('ul#regions_' + prov + ' li input.region_select').each(function () {
				$(this).prop('checked', checked);

				calendar_sidebar.setActiveRegion($(this).val());
			});
		}
	},
	setActiveRegion: function(region_id) {
		if (region_id) {
			var checked = $('#region_select_' + region_id).prop('checked');
			$('ul#locations_' + region_id + ' li input.location_select').each(function () {
				$(this).prop('checked', checked);
				calendar_sidebar.setActiveLocation($(this).val());
			});
		}
	},
	/*setDefaultLocations: function(x) {
		if (!x) {
			return;
		}
		var locations = (x).split(',');

		for (var a in locations) {
			var loc_id = locations[a];

			if ($('#location_select_' + loc_id).size()) {
				$('#location_select_' + loc_id).prop('checked', true);

				if ($('#provinces').size()) {
					var prov_code =  $('#location_select_' + loc_id).parent('li').parent('ul').parent('li').attr('class').replace('province_', '');
					$('#province_select_' + prov_code).prop('checked', true);
					calendar_sidebar.toggleProvince($('.province_icon', $('#province_select_' + prov_code).parent()), 1);
				}
				if ($('.regions').size()) {
					var region_id = $('#location_select_' + loc_id).parent('li').parent('ul').attr('id').replace('locations_', '');
					$('#region_select_' + region_id).prop('checked', true);
					calendar_sidebar.toggleRegion($('.region_icon', $('#region_select_' + region_id).parent()), 1);
				}
			}
		}
	},*/
	setDefaultLocations: function(x) {	
		// console.log(x);
		if(localStorage.getItem('locations')){
			
        	var a = [];
        	var locations_id = [];
        	var index = 0;
		    a = JSON.parse(localStorage.getItem('locations'));
			for (var i in a) {
				var loc_id = a[i];
					$('#location_select_' + loc_id).prop('checked', true);

					if ($('#provinces').size()) {
						var prov_code =  $('#location_select_' + loc_id).parent('li').parent('ul').parent('li').attr('class').replace('province_', '');
						$('#province_select_' + prov_code).prop('checked', true);
						calendar_sidebar.toggleProvince($('.province_icon', $('#province_select_' + prov_code).parent()), 1);
					}
					if ($('.regions').size()) {
						var region_id = $('#location_select_' + loc_id).parent('li').parent('ul').attr('id').replace('locations_', '');
						$('#region_select_' + region_id).prop('checked', true);
						calendar_sidebar.toggleRegion($('.region_icon', $('#region_select_' + region_id).parent()), 1);
					}
			}

			if($('.location_select:checked').length == $('.location_select').length){ 
		       	$('#sidebar_select_all_locations').attr('checked',true);
			}else{ 
		       	$('#sidebar_select_all_locations').attr('checked',false);
			}

		}else{
			return;
		}
	},
	/*setDefaultDoctors: function(x) {
		if (!x && $('.location_select:checked').size()) {
			$('.location_select:checked').each(function () {
				var id = $(this).attr('id');
				$('.doctor_select', $($(this).closest('li'))).prop('checked', 'checked');
				calendar_sidebar.toggleLocation($('.location_icon', $('#' + id).parent()), 1);
			});
		}
		var doctors = (x).split(',');

		for (var a in doctors) {
			var doc_id = doctors[a];

			if ($('#doctor_select_' + doc_id).size()) {
				var loc_id = $('#doctor_select_' + doc_id).parent('li').parent('ul').parent('li').attr('class').replace('location_', '');
				$('#doctor_select_' + doc_id).prop('checked', true);
				$('#location_select_' + loc_id).prop('checked', true);
				calendar_sidebar.toggleLocation($('.location_icon', $('#location_select_' + loc_id).parent()), 1);

				if ($('#provinces').size()) {
					var prov_code =  $('#location_select_' + loc_id).parent('li').parent('ul').parent('li').attr('class').replace('province_', '');
					$('#province_select_' + prov_code).prop('checked', true);
					calendar_sidebar.toggleProvince($('.province_icon', $('#province_select_' + prov_code).parent()), 1);
				}
				if ($('.regions').size()) {
					var region_id = $('#location_select_' + loc_id).parent('li').parent('ul').attr('id').replace('locations_', '');
					$('#region_select_' + region_id).prop('checked', true);
					calendar_sidebar.toggleRegion($('.region_icon', $('#region_select_' + region_id).parent()), 1);
				}
			}
		}
	},*/
	setDefaultDoctors: function(x) {

		$('.location_select:checked').each(function () {
			var id = $(this).attr('id');
			$('.doctor_select', $($(this).closest('li'))).prop('checked', 'checked');
			calendar_sidebar.toggleLocation($('.location_icon', $('#' + id).parent()), 1);
		});

		if(localStorage.getItem('doctors')){
			
        	var a = [];
		    a = JSON.parse(localStorage.getItem('doctors'));			    
		    for( var i in a){
				var doc_id = a[i];
				// $('#doctor_select_' + a[i]).prop('checked', true);

				if ($('#doctor_select_' + doc_id).size()) {
					var loc_id = $('#doctor_select_' + doc_id).parent('li').parent('ul').parent('li').attr('class').replace('location_', '');
					$('#doctor_select_' + doc_id).prop('checked', true);
					// $('#location_select_' + loc_id).prop('checked', true);
					calendar_sidebar.toggleLocation($('.location_icon', $('#location_select_' + loc_id).parent()), 1);

					if ($('#provinces').size()) {
						var prov_code =  $('#location_select_' + loc_id).parent('li').parent('ul').parent('li').attr('class').replace('province_', '');
						$('#province_select_' + prov_code).prop('checked', true);
						calendar_sidebar.toggleProvince($('.province_icon', $('#province_select_' + prov_code).parent()), 1);
					}
					if ($('.regions').size()) {
						var region_id = $('#location_select_' + loc_id).parent('li').parent('ul').attr('id').replace('locations_', '');
						$('#region_select_' + region_id).prop('checked', true);
						calendar_sidebar.toggleRegion($('.region_icon', $('#region_select_' + region_id).parent()), 1);
					}
				}
		    }

		}
		
	},
	setupTimeslotsPerHour: function(v) {
		calendar_events.defaults.timeslotsPerHour = v;
		$('#time_increments').val(v);
	},
	toggleProvince: function(icon, open) {
		if ($(icon).hasClass('ui-icon-plusthick') || open == 1) {
			$(icon).removeClass('ui-icon-plusthick');
			$(icon).addClass('ui-icon-minusthick');
			$('ul.regions', $(icon).parent()).show();
		}
		else {
			$(icon).removeClass('ui-icon-minusthick');
			$(icon).addClass('ui-icon-plusthick');
			$('ul.regions', $(icon).parent()).hide();
		}
	},
	toggleRegion: function(icon, open) {
		if ($(icon).hasClass('ui-icon-plusthick') || open == 1) {
			$(icon).removeClass('ui-icon-plusthick');
			$(icon).addClass('ui-icon-minusthick');
			$('ul.locations', $(icon).parent()).show();
		}
		else {
			$(icon).removeClass('ui-icon-minusthick');
			$(icon).addClass('ui-icon-plusthick');
			$('ul.locations', $(icon).parent()).hide();
		}
	},
	toggleLocation: function(icon, open) {
		if ($(icon).hasClass('ui-icon-plusthick') || open == 1) {
			$(icon).removeClass('ui-icon-plusthick');
			$(icon).addClass('ui-icon-minusthick');
			$('ul.doctors', $(icon).parent()).show();
		}
		else {
			$(icon).removeClass('ui-icon-minusthick');
			$(icon).addClass('ui-icon-plusthick');
			$('ul.doctors', $(icon).parent()).hide();
		}
	},
	toggleMiniCal: function(icon) {
		if ($(icon).hasClass('ui-icon-minusthick')) {
			$(icon).removeClass('ui-icon-minusthick');
			$(icon).addClass('ui-icon-plusthick');
			$('#minical').hide();
		}
		else {
			$(icon).removeClass('ui-icon-plusthick');
			$(icon).addClass('ui-icon-minusthick');
			$('#minical').show();
		}
	},
	toggleRegionsDiv: function(icon) {
		if ($(icon).hasClass('ui-icon-minusthick')) {
			$(icon).removeClass('ui-icon-minusthick');
			$(icon).addClass('ui-icon-plusthick');
			$('#regions_form').hide();
		}
		else {
			$(icon).removeClass('ui-icon-plusthick');
			$(icon).addClass('ui-icon-minusthick');
			$('#regions_form').show();
		}
	},
	toggleCalendarOptionsDiv: function(icon) {
		if ($(icon).hasClass('ui-icon-minusthick')) {
			$(icon).removeClass('ui-icon-minusthick');
			$(icon).addClass('ui-icon-plusthick');
			$('#cal_options_div').hide();
		}
		else {
			$(icon).removeClass('ui-icon-plusthick');
			$(icon).addClass('ui-icon-minusthick');
			$('#cal_options_div').show();
		}
	},
	getSidebarParameters: function(s, e) {
		if (!s && !e) {
                        console.log(calendar_sidebar.last_used_params);
			return calendar_sidebar.last_used_params;
		}
		calendar_sidebar.last_used_params = 'start=' + s.toString('yyyy-MM-dd')
			+ '&end=' + e.toString('yyyy-MM-dd')
			+ '&active_locations=' + calendar_sidebar.getActiveLocations().join(',')
			+ '&active_doctors=' + calendar_sidebar.getActiveDoctors().join(',')
			+ '&days_to_show=' + $('#calendar').weekCalendar('option', 'daysToShow')
			+ '&timeslots_per_hour=' + $('#calendar').weekCalendar('option', 'timeslotsPerHour')
			+ '&minical_date=' + calendar_sidebar.getMiniCalDate()
			+ '&include_empty_columns=' + ($('#appointment_slots_include_empty').prop('checked') ? '1' : '0')
			+ '&include_misc_columns=' + ($('#appointment_slots_show_misc').prop('checked') ? '1' : '0');
		return calendar_sidebar.last_used_params;
	}
};

function adminSidebarUpdateMinical (date, inMonth) {
	var classes = '';
	if (inMonth) {
		if ($.inArray(date.toString('yyyy-MM-dd'), calendar_sidebar.consultation_dates) != -1) {
			classes += ' active_date';
		}
		if ($.inArray(date.toString('yyyy-MM-dd'), calendar_sidebar.doctor_dates) != -1) {
			classes += ' active_date2';
		}
	}
	if (classes == '') {
		classes = 'inactive_date';
	}
	return {dateClass: classes};
}
