var appt_search = {
    actions: {
        claim: null,
        csv: null,
        pdf: null,
        profile: null,
        search: null,
        stage_update: null
    },
    messages: {
        checkmark: 'checkmark',
        error: 'error_icon',
        spinner: 'spinner',
        text: 'messages_text'
    },

    action_columns: {},
    include_date_in_pdf:0,
    ajax_running: false,
    form_id: null,
    form_prefix: null,
    form_error_div: 'appointment_search_errors',
    header: {},
    results_div: 'results',
    results_table: 'results-table',
    results_colspan: 1,
    results_row_prefix: 'results_table_row_',
    results_actions: ['edit', 'cancel', 'reschedule', 'reverse', 'claim', 'profile'],
    stages: {},

    use_provinces: true,
    use_regions: true,

    location_doctors: null,
    province_regions: null,
    region_locations: null,

    initialize: function () {
        sk.datepicker('#' + this.form_prefix + '_appointment_date_start');
        sk.datepicker('#' + this.form_prefix + '_appointment_date_end');

        var prefix = this.form_prefix;
        $('#' + this.form_prefix + '_appointment_date_start').on('changeDate',function (e) {
            $('#' + prefix + '_appointment_date_end').skDP('setStartDate', e.date);
        });

        $('#' + this.form_prefix + '_appointment_date_end').on('changeDate',function (e) {
            $('#' + prefix + '_appointment_date_start').skDP('setEndDate', e.date);
        });

        $('#find_button').button();
        $('#find_button').click(function () {
            appt_search.handleFindClick();
        });

        $('#pdf_button').button();
        $('#pdf_button').click(function () {
            return appt_search.handlePdfClick();
        });

        $('#csv_button').button();
        $('#csv_button').click(function () {
            return appt_search.handleCsvClick();
        });

        $('#pull_charts_button').button();
        $('#pull_charts_button').click(function () {
            return appt_search.handleQuickSelection("pull_charts");
        });

        $('#hospital_day_button').button();
        $('#hospital_day_button').click(function () {
            return appt_search.handleQuickSelection("hospital_day");
        });

        $('#doctors_day_button').button();
        $('#doctors_day_button').click(function () {
            return appt_search.handleQuickSelection("doctors_day");
        });

        $('#billing_sheet_button').button();
        $('#billing_sheet_button').click(function () {
            return appt_search.handleQuickSelection("billing_sheet");
        });


        $('#todaysAppts').button();
        $('#todaysAppts').click(function () {
            var date = new Date();
            date = date.toString(quickClaim.getParseDateFormat(quickClaim.dateFormat));
            $('#' + appt_search.form_prefix + '_appointment_date_end').val(date);
            $('#' + appt_search.form_prefix + '_appointment_date_start').val(date);
            appt_search.handleFindClick();
        });

        $('#nextApptDate').button();
        $('#nextApptDate').click(function () {
            var date = $(this).val();
            $('#' + appt_search.form_prefix + '_appointment_date_end').val(date);
            $('#' + appt_search.form_prefix + '_appointment_date_start').val(date);
            appt_search.handleFindClick();
        });


        $('.pdf_options_toggle').click(function () {
            appt_search.togglePDFOptions();
        });

        $('#' + appt_search.form_prefix + '_pdf_option_show_subheader').click(function () {
            appt_search.updatePdfOptionsSubheader();
        });
        appt_search.updatePdfOptionsSubheader();

        /* custom Toggling  start */
        $('.select2toggle').on('change' ,function (e) {
            if(e.added){
                if(e.added.id == 'all'){
                    $(this).select2('val','all');
                }else{
                    var data = $(this).select2('data');
                    var removeIndex = Array();
                    for(var a in data){
                        if(data[a].id == 'all'){
                            removeIndex.push(a);
                        }
                    }
                    if(removeIndex.length > 0){
                        for(var key in removeIndex){
                            delete data[removeIndex[key]];
                        }
                    }
                    data = appt_search.removeEmptyEntry(data);
                    $(this).select2('data', data);
                }
            }else if(e.removed){
                var data = $(this).select2('data');
                if(data.length <= 0){
                    $(this).select2('val', 'all');
                }
            }
        });

/* custom Toggling  end */


        $('.select2toggle').each(function(){
            //$(this).val('all').trigger('change');
            console.log('test');
        });


        $('.drSelectDiv input').click(function () {
            console.log('clicked');
            var val = $(this).val();
            if(val == 'all'){
                //remove other
                $('.drSelectDiv input').attr('checked',false);
                $('.drSelectDiv input[value="all"]').attr('checked',true);
            }else{
                //remove all
                $('.drSelectDiv input[value="all"]').attr('checked',false);
            }
        });


        appt_search.toggleAll('doctor');
        appt_search.toggleProvinceRegions();
    },
    clearErrors: function () {
        $('label.error').remove();
        $('.error').removeClass('error');
        $('#' + appt_search.form_error_div).text('').hide();
    },
    addResultsTableRow: function (data, header) {
        var row_id = appt_search.results_row_prefix + data['appointment_id'] + '_' + data['appointment_attendee_id'] + '_' + data['patient_id'];
        var ids = data['appointment_id'] + '_' + data['appointment_attendee_id'] + '_' + data['patient_id'];
        var row = '';

        for (var column in header) {
            var text = data[column];

            if (column == 'appointment_actions') {
                row += '<td class="nowrap">' + appt_search.buildStagesCell(data.stage_client_id, 'update-stage_' + ids) + '</td>';

                for (var action in this.results_actions) {
                    if (appt_search.action_columns.hasOwnProperty(this.results_actions[action])) {
                        if (data[column].hasOwnProperty(this.results_actions[action])) {
                            row += '<td class="' + this.results_actions[action] + '">'
                                    + '<input type="button" class="ui-button-inline" id="' + this.results_actions[action] + '_' + ids + '" value="' + appt_search.action_columns[this.results_actions[action]] + '" />'
                                    + '</td>';
                        } else {
                            row += '<td>&nbsp;</td>';
                        }
                    }
                }
            } else {
                row += '<td class="' + column + '">' + text + '</td>';
            }
        }

        if (!$('#' + appt_search.results_table + ' tbody tr#' + row_id).size()) {
            var row = '<tr id="' + row_id + '">' + row + '</tr>';
            $(row).appendTo($('#' + appt_search.results_table + ' tbody'));
        } else {
            $('tr#' + row_id).html(row);
        }

        $('input', $('#' + row_id)).button();
        $('input', $('#' + row_id)).click(function () {
            var terms = $(this).attr('id').split('_');
            var action = terms[0];
            var appointment_id = terms[1];
            var attendee_id = terms[2];
            var patient_id = terms[3];

            if (action == 'claim') {
                var url = appt_search.actions.claim + '/index/appointment_attendee_id/' + attendee_id;
                window.location = url;
            } else if (action == 'update-stage') {
                appt_search.handleUpdateStageClick(appointment_id, attendee_id, patient_id);
            } else if (action == 'profile') {
                var url = appt_search.actions.profile + '/index/id/' + patient_id;
                window.location = url;
            } else {
                $('#appointment_book_patient_id').val(patient_id);
                $('#appointment_book_appointment_id').val(appointment_id);
                $('#appointment_book_mode').val(action);
                $('#appointment_book_form').submit();
            }
        });
    },
    fillResultsTable: function (rs) {
        var action_columns = {};
        var actions_count = 0;
        var max_colspan = 0;
        var results_count = 0;

        var table = '<div class="panel panel-default"><div class="panel-heading"><i class="clip-file-2"></i>Result<div class="panel-tools"><a class="btn btn-xs btn-link panel-collapse collapses" href="#"></a></div></div><div class="panel-body"  style="overflow-x:scroll"><button type="button" id="printTable" class="btn btn-blue btn-squared sk5mar"><i class="fa-print fa"></i> Print</button><hr/><table style="width:100%"  class="DataTableAppointmentSearch table-hover table" id="' + appt_search.results_table + '">'
                + '<thead><tr>';

        for (var column in rs.header) {
            table += '<th class="' + column + '">' + rs.header[column] + '</th>';
            max_colspan++;
        }

        table += '</tr></thead>'
                + '<tbody></tbody>'
                + '</table></div></div>';

        $('#' + appt_search.results_div).html(table);

        for (var index in rs.data) {
            if (rs.data[index].hasOwnProperty('appointment_actions')) {
                for (var action in rs.data[index]['appointment_actions']) {
                    if (!action_columns.hasOwnProperty(action)) {
                        action_columns[action] = rs.data[index]['appointment_actions'][action];
                        actions_count++;
                    }
                }
            }
        }
        $('#' + appt_search.results_table + ' tr th.appointment_actions').attr('colspan', actions_count + 1);

        appt_search.action_columns = action_columns;
        appt_search.header = rs.header;

        max_colspan += actions_count;

        for (var index in rs.data) {
            results_count++;
            appt_search.addResultsTableRow(rs.data[index], rs.header);
        }

        var tfoot = '<tfoot><tr><td colspan="' + max_colspan + '">' + results_count + ' appointments</td></tr></tfoot><input type="button" value="Print table" id="printBtn"/>';
        $(tfoot).appendTo($('#' + appt_search.results_table));



        if ($('#' + appt_search.results_table + ' tbody tr').size()) {
            $('#' + appt_search.results_table).tablesorter1({
                widgets: ['zebra', 'hover']
            });
        }

        $('#printTable').button();
        $('#printTable').click(function (ev) {
            $(".DataTableAppointmentSearch").printElement({
                overrideElementCSS: ['css/jquery-tablesorter1.css', 'css/jquery-tablesorter/jquery.tablesorter.pager.css', 'css/jquery-tablesorter/theme.blue.css']
            });
        });

        $(".DataTableAppointmentSearch").DataTable({
            "paging": true,
                            "lengthMenu": [10, 25, 50, 100, 200, 250, 500],
                            "lengthChange": true,
                            "searching": true,
                            "ordering": true,
                            "order": [[1, "asc"]],
                            "info": true,
                            "autoWidth": false,
                            "responsive": false,
                            // "scrollX": true,
                            // "scrollY":300,
                            "dom": 'Bfrtip',
                            "buttons": [
                                {
                                    "extend": 'pdf',
                                    "text": '<i class="clip-file-pdf"></i> PDF',
                                    "className": 'btn btn-danger btn-squared',
                                    extend: 'pdfHtml5',
                                    orientation: 'landscape',
                                    pageSize: 'LEGAL',
                                    customize: function (doc) {
                                        doc.defaultStyle.fontSize = sk.print_font_size;
                                    }
                                },
                                {
                                    "extend": 'excel',
                                    "text": '<i class="clip-file-excel"></i> Excel',
                                    "className": 'btn btn-info btn-squared'
                                },
                                {
                                    "extend": 'print',
                                    "text": '<i class="fa-print fa"></i> Print',
                                    "className": 'btn btn-blue btn-squared'
                                },
                            ],
                            "oLanguage": {
                                "sSearch": "",
                                "sLengthMenu": "Per page _MENU_",
                                "oPaginate": {
                                    "sNext": "<i class='clip-chevron-right'></i>",
                                    "sPrevious": "<i class='clip-chevron-left'></i>"
                                },
                                "sInfo": "_START_ to _END_ rows out of _TOTAL_",

                            },
                            //"columns": columnsData,
                            "columnDefs": [
                                {"targets": 0, "orderable": false}
                            ],
//                            "language": {
//                                "oSearch": "",
//                                "lengthMenu": "Per page _MENU_",
////                                "zeroRecords": "No report found.",
////                                "info": "Showing page _PAGE_ of _PAGES_",
////                                "infoEmpty": "No records available",
////                                "infoFiltered": "(filtered from _MAX_ total records)"
//                            },
        
                            "sDom": '<"wrapper"<"row"<"col-sm-12" <"col-sm-6 actionButtonsDiv"> <"col-sm-6" Bl > <"col-sm-12" <"row well customWell" <"col-sm-3 customSearchInput" f> <"col-sm-3 customSearchField">  <"col-sm-6 CustomPagination"ip> > > >  >rt<"clear">>',
// #D2EDF7                           "sDom": '<"wrapper"<"row"<"col-sm-12" <"row" <"col-sm-7" <"col-sm-6 small"l><"col-sm-6"f<"DTsearchlabel">><"col-sm-8"p><"col-sm-4 small"i>> <"col-sm-5 pdfTableOptions small"B>>>>rt<"clear">>',


        });
        $('div.dataTables_filter input').addClass('form-control');
        $('div.dataTables_filter input').attr('placeholder', 'Search');
        $(".DTsearchlabel").html('<i class="clip-search"></i>');
        $('.dataTables_filter').attr('style', 'width:100%');
        $('.dataTables_filter label').attr('style', 'width:100%');
        $('.dataTables_length select').attr('style', 'font-size: 12px;padding: 5px 3px;background: #fff;');
        $('div.dataTables_filter input').attr('style', 'float:right;width:98%;margin-right:5px;');
        $(".dataTables_wrapper .dataTables_paginate .paginate_button").attr('style', 'background:#fff !important;color:#444444 !important; border-radius: 0;');

    },
    buildStagesCell: function (stage_client_id, id) {
        var select = '';
        var options = '';

        if (appt_search.stages.hasOwnProperty(stage_client_id)) {
            for (var a in appt_search.stages[stage_client_id].next_stage) {
                var name = appt_search.stages[stage_client_id].next_stage[a];
                options += '<option value="' + a + '">' + name + '</option>';
            }

            if (options.length) {
                var select = '<select id="select_' + id + '" name="' + id + '">'
                        + '<option value="' + stage_client_id + '" selected="selected">' + appt_search.stages[stage_client_id].name + '</option>'
                        + options
                        + '</select>'
                        + '<input type="button" id="' + id + '" class="ui-button-inline" value="Update" />';
            }
        }
        return select;
    },
    handleQuickSelection: function (selection) {
        /*
         appointment_search_report_columns_location
         appointment_search_report_columns_doctor
         appointment_search_report_columns_start_date
         appointment_search_report_columns_start_time
         appointment_search_report_columns_appt_length
         appointment_search_report_columns_appt_type
         appointment_search_report_columns_service_code
         appointment_search_report_columns_diag_code
         appointment_search_report_columns_patient_id
         appointment_search_report_columns_patient_number
         appointment_search_report_columns_last_first
         appointment_search_report_columns_patient_hcn
         appointment_search_report_columns_patient_dob
         appointment_search_report_columns_patient_email
         appointment_search_report_columns_patient_refdoc_num
         appointment_search_report_columns_phone
         appointment_search_report_columns_patient_location
         appointment_search_report_columns_status_text
         appointment_search_report_columns_stage_text
         appointment_search_report_columns_notes
         appointment_search_report_columns_appointment_actions
         */

        $('#appointment_search_report_columns_location, #appointment_search_report_columns_doctor, #appointment_search_report_columns_start_date, #appointment_search_report_columns_start_time, #appointment_search_report_columns_appt_length, #appointment_search_report_columns_appt_type, #appointment_search_report_columns_service_code, #appointment_search_report_columns_diag_code, #appointment_search_report_columns_patient_id, #appointment_search_report_columns_patient_number, #appointment_search_report_columns_last_first, #appointment_search_report_columns_patient_hcn, #appointment_search_report_columns_patient_dob, #appointment_search_report_columns_patient_email, #appointment_search_report_columns_patient_refdoc_num, #appointment_search_report_columns_phone, #appointment_search_report_columns_patient_location, #appointment_search_report_columns_status_text, #appointment_search_report_columns_stage_text, #appointment_search_report_columns_notes, #appointment_search_report_columns_appointment_actions').prop('checked', false);

        if (selection === "pull_charts") {
            $('#appointment_search_report_columns_start_date, #appointment_search_report_columns_start_time, #appointment_search_report_columns_patient_number, #appointment_search_report_columns_last_first, #appointment_search_report_columns_patient_hcn').prop('checked', true);
            $('#appointment_search_pdf_option_header').val('Pull Charts');
        } else if (selection === "hospital_day") {
            $('#appointment_search_report_columns_start_time, #appointment_search_report_columns_last_first, #appointment_search_report_columns_patient_hcn, #appointment_search_report_columns_patient_dob, #appointment_search_report_columns_phone, #appointment_search_report_columns_notes').prop('checked', true);
            $('#appointment_search_pdf_option_header').val('Hospital Day');
        } else if (selection === "doctors_day") {
            $('#appointment_search_report_columns_start_time, #appointment_search_report_columns_appt_type, #appointment_search_report_columns_service_code, #appointment_search_report_columns_diag_code, #appointment_search_report_columns_patient_number, #appointment_search_report_columns_last_first, #appointment_search_report_columns_phone, #appointment_search_report_columns_notes').prop('checked', true);
            $('#appointment_search_pdf_option_header').val('Doctors Day');
        } else if (selection === "billing_sheet") {
            $('#appointment_search_report_columns_location, #appointment_search_report_columns_doctor, #appointment_search_report_columns_start_date, #appointment_search_report_columns_appt_type, #appointment_search_report_columns_service_code, #appointment_search_report_columns_diag_code, #appointment_search_report_columns_last_first, #appointment_search_report_columns_patient_hcn, #appointment_search_report_columns_patient_dob, #appointment_search_report_columns_patient_refdoc_num').prop('checked', true);
            $('#appointment_search_pdf_option_header').val('Billing Sheet');
        }
    },
    handleCsvClick: function () {
        if (appt_search.ajax_running) {
            return;
        }

        appt_search.clearErrors();
        data = $('#' + appt_search.form_id).serialize();
        data += '&mode=csv';

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: appt_search.actions.search,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#' + appt_search.form_error_div, '#' + appt_search.form_id, true);
                    quickClaim.focusTopField();

                    $('#' + appt_search.messages.error).show();
                    $('#' + appt_search.messages.text).html('<div class="alert alert-info">The form contains invalid data.</div>').show();
                } else {
                    appt_search.handleCsvRequest(data);
                }
            },
            beforeSend: function () {
                appt_search.ajax_running = true;
                $('#messages .icons div').hide();
                $('#' + appt_search.messages.spinner).show();
                $('#' + appt_search.messages.text).text('Building CSV').show();
            },
            complete: function () {
                appt_search.ajax_running = false;
                $('#' + appt_search.messages.spinner).hide();
            },
            error: function () {
                $('#' + appt_search.messages.error).show();
                $('#' + appt_search.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });
    },
    handleCsvRequest: function (data) {
        var fields = appt_search.splitFormData(data);
        appt_search.setupIframe();
        appt_search.setupHiddenForm(fields, appt_search.actions.csv, 'csv');
    },
    handleFindClick: function () {
        if (appt_search.ajax_running) {
            return;
        }


        /** This is new Modification On dated 23rd Nov 2017 **/
//		var s_date = $('#' + appt_search.form_id).find('input[id="appointment_search_appointment_date_start"]').val();
//		if(s_date !=undefined || s_date !=null){
//			var from = s_date.split("/");
//			if(from !=""){
//				var f = new Date('"'+from[1]+'/'+from[0]+'/'+from[2]+'"');
//                                var month = +f.getMonth() + 1 ;
//				var date_string = f.getFullYear() + "-" + month + "-" + f.getDate();
//				$('#' + appt_search.form_id).find('input[id="appointment_search_appointment_date_start"]').val(date_string);
//			}
//		}
//		var e_date = $('#' + appt_search.form_id).find('input[id="appointment_search_appointment_date_end"]').val();
//		if(e_date !=undefined || e_date !=null){
//			var to = e_date.split("/");
//			if(to !=""){
//				var t = new Date('"'+to[1]+'/'+to[0]+'/'+to[2]+'"');
//                                var month = +t.getMonth() + 1 ;
//				var date_string = t.getFullYear() + "-" + month + "-" + t.getDate();
//				$('#' + appt_search.form_id).find('input[id="appointment_search_appointment_date_end"]').val(date_string);
//			}
//		}
        /** New Modification Ends **/

        data = $('#' + appt_search.form_id).serialize();
        appt_search.clearErrors();


//appointment_search[appointment_date_start]=
//        &appointment_search[appointment_date_end]=
//        &appointment_search[patient_number]=
//        &appointment_search[patient_id]=
//        &appointment_search[patient_name]=
//        &appointment_search[province_code]=all
//        &appointment_search[region_id]=all
//        &appointment_search[location_id]=all
//        &appointment_search[doctor_id][]=all
//        &appointment_search[report_columns][]=start_time
//        &appointment_search[report_columns][]=appt_type
//        &appointment_search[report_columns][]=last_first
//        &appointment_search[report_columns][]=phone
//        &appointment_search[pdf_option_orientation]=L
//        &appointment_search[sort_order]=name
//        &appointment_search[pdf_option_header]=Appointments+Report
//        &appointment_search[pdf_option_subheader]=
//        &appointment_search[_csrf_token]=d5406cee136473f7a87b206f2259294a
//

console.log(data);
//return false;
        $.ajax({
            type: 'post',
            dataType: 'json',
            type: 'post',
            url: appt_search.actions.search,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#' + appt_search.form_error_div, '#' + appt_search.form_id, true);

                    $('#' + appt_search.messages.error).show();
                    $('#' + appt_search.messages.text).html('<div class="alert alert-info">The form contains invalid data.</div>').show();
                } else if (rs.hasOwnProperty('data')) {
                    console.log(rs);
                    appt_search.fillResultsTable(rs);
                    $('#tabs').tabs('enable', 1);
                    $('#tabs').tabs('option', 'active', 1);
                }
            },
            beforeSend: function () {
                appt_search.ajax_running = true;
                $('#messages .icons div').hide();
                $('#' + appt_search.messages.spinner).show();
                $('#' + appt_search.messages.text).text('Searching for appointments').show();
            },
            complete: function () {
                appt_search.ajax_running = false;
                $('#' + appt_search.messages.spinner).hide();
            },
            error: function () {
                $('#' + appt_search.messages.error).show();
                $('#' + appt_search.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }

        });
    },
    handlePdfClick: function () {
        if (appt_search.ajax_running) {
            return;
        }

        appt_search.clearErrors();
        data = $('#' + appt_search.form_id).serialize();
        data += '&mode=pdf';

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: appt_search.actions.search,
            data: data,
            success: function (rs) {
                if (rs.hasOwnProperty('errors')) {
                    quickClaim.showFormErrors(rs.errors, '#' + appt_search.form_error_div, '#' + appt_search.form_id, true);

                    $('#' + appt_search.messages.error).show();
                    $('#' + appt_search.messages.text).html('<div class="alert alert-info">The form contains invalid data.</div>').show();
                } else {
                    appt_search.handlePdfRequest(data);
                }
            },
            beforeSend: function () {
                appt_search.ajax_running = true;
                $('#messages .icons div').hide();
                $('#' + appt_search.messages.spinner).show();
                $('#' + appt_search.messages.text).text('Building PDF').show();
            },
            complete: function () {
                appt_search.ajax_running = false;
                $('#' + appt_search.messages.spinner).hide();
            },
            error: function () {
                $('#' + appt_search.messages.error).show();
                $('#' + appt_search.messages.text).text('An error occurred while contacting the server. Please refresh the page and try again.').show();
            }
        });

    },
    handlePdfRequest: function (data) {
        var fields = appt_search.splitFormData(data);
        appt_search.setupIframe();
        appt_search.setupHiddenForm(fields, appt_search.actions.pdf, 'pdf');
    },
    handleUpdateStageClick: function (appointment_id, attendee_id, patient_id) {
        var stage_client_id = $('#select_update-stage_' + appointment_id + '_' + attendee_id + '_' + patient_id).val();
        var params = 'appointment[id]=' + appointment_id
                + '&appointment[stage_client_id]=' + stage_client_id
                + '&appointment[source]=appointment_search';

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: appt_search.actions.stage_update,
            data: params,
            success: function (rs) {
                if (rs.hasOwnProperty('data')) {
                    for (var a in rs.data) {
                        appt_search.addResultsTableRow(rs.data[a], appt_search.header);
                    }
                }
            },
            beforeSend: function () {
                $('#' + appt_search.messages.checkmark).hide();
                $('#' + appt_search.messages.error).hide();
                $('#' + appt_search.messages.spinner).show();

                $('#' + appt_search.messages.text).text('Updating Appointment').show();
            },
            complete: function () {
                $('#' + appt_search.messages.spinner).hide();
            },
            error: function () {
                $('#' + appt_search.messages.error).show();
                $('#' + appt_search.messages.text).text('An error occurred while contacting the server.');
            }
        });
    },
    setupIframe: function () {
        $('#hiddenDownloader').remove();
        $('body').append('<iframe id="hiddenDownloader" style="width: 0; height: 0; border: none; display: none;"></iframe>');
    },
    setupHiddenForm: function (fields, action, prefix) {
        setTimeout(function () {
            var frame_body = $('#hiddenDownloader').contents().find('body');
            var form = '<form id="hidden_form" action="' + action + '" method="POST">';

            for (var a in fields) {
                var name = fields[a].substring(0, fields[a].indexOf('='));
                var value = fields[a].substring(fields[a].indexOf('=') + 1);

                name = name.replace(appt_search.form_prefix, prefix);
                form += '<input type="text" value="' + value + '" id="' + name + '" name="' + name + '" />';
            }
            form += '</form>';
            frame_body.append($(form));
            $('#hiddenDownloader').contents().find('#hidden_form').submit();
        });
    },
    splitFormData: function (data) {
        data = data.replace(/%5B/g, '[').replace(/%5D/g, ']');
        return data.split('&');
    },
    toggleAll: function (type, value) {
        var count = $('#' + this.form_id + ' td.' + type + ' li:visible input:checked').size();

        if (value == 'all' && $('#' + this.form_id + ' td.' + type + ' input[value=all]').prop('checked')) {
            $('#' + this.form_id + ' td.' + type + ' input:checked').each(function () {
                if ($(this).val() != 'all') {
                    $(this).prop('checked', false);
                }
            });
        } else if (count == 0) {
            $('#' + this.form_id + ' td.' + type + ' input[value=all]').prop('checked', true);
        } else if (count > 1) {
            $('#' + this.form_id + ' td.' + type + ' input[value=all]').prop('checked', false);
        }
    },
    toggleProvinceRegions: function () {
        if (this.use_provinces) {
            if ($('#' + this.form_id + ' td.province input[value=all]').prop('checked')) {
                $('#' + this.form_id + ' td.region li').show();
            } else {
                $('#' + this.form_id + ' td.region li').hide();
                $('#' + this.form_id + ' td.region input[value=all]').parent('li').show();
                $('#' + this.form_id + ' td.province input:checked').each(function () {
                    var val = $(this).val();

                    if (val && appt_search.province_regions.hasOwnProperty(val)) {
                        for (var a in appt_search.province_regions[val]) {
                            $('#' + appt_search.form_prefix + '_region_id_' + appt_search.province_regions[val][a]).parent('li').show();
                        }
                    }
                });
            }
            appt_search.toggleAll('province');
        }

        appt_search.toggleAll('region');
        appt_search.toggleAll('location');
        appt_search.toggleAll('doctor');
        appt_search.toggleRegionLocations();
    },
    toggleRegionLocations: function () {
        if (this.use_regions) {
            $('#' + this.form_id + ' td.location li').hide();
            $('#' + this.form_id + ' td.location input[value=all]').parent('li').show();

            var show_all = true;
            if (this.use_regions) {
                show_all = show_all && $('#' + this.form_id + ' td.region input[value=all]:checked').size();
            }
            if (this.use_provinces) {
                show_all = show_all && $('#' + this.form_id + ' td.province input[value=all]:checked').size();
            }

            if (show_all) {
                $('#' + this.form_id + ' td.location li').show();
            } else if ($('#' + this.form_id + ' td.region input[value=all]:checked').size()) {
                $('#' + this.form_id + ' td.region li:visible input').each(function () {
                    var region_id = $(this).attr('value');

                    if (appt_search.region_locations.hasOwnProperty(region_id)) {
                        for (var a in appt_search.region_locations[region_id]) {
                            $('#' + appt_search.form_prefix + '_location_id_' + appt_search.region_locations[region_id][a]).parent('li').show();
                        }
                    }
                });
            } else {
                $('#' + this.form_id + ' td.region li:visible input:checked').each(function () {
                    var region_id = $(this).attr('value');
                    if (appt_search.region_locations.hasOwnProperty(region_id)) {
                        for (var a in appt_search.region_locations[region_id]) {
                            $('#' + appt_search.form_prefix + '_location_id_' + appt_search.region_locations[region_id][a]).parent('li').show();
                        }
                    }
                });
            }
        }

        appt_search.toggleAll('region');
        appt_search.toggleAll('location');
        appt_search.toggleAll('doctor');
        appt_search.toggleLocationDoctors();

    },
    toggleLocationDoctors: function () {
        $('#' + this.form_id + ' td.doctor li').hide();
        $('#' + this.form_id + ' td.doctor input[value=all]').parent('li').show();

        var show_all = $('#' + this.form_id + ' td.location input[value=all]:checked').size() && true;
        if (this.use_provinces) {
            show_all = show_all && $('#' + this.form_id + ' td.province input[value=all]:checked').size();
        }
        if (this.use_regions) {
            show_all = show_all && $('#' + this.form_id + ' td.region input[value=all]:checked').size();
        }

        if (show_all) {
            $('#' + this.form_id + ' td.doctor li').show();
        } else if ($('#' + this.form_id + ' td.location input[value=all]:checked').size()) {
            $('#' + this.form_id + ' td.location li:visible input').each(function () {
                var doctor_id = $(this).attr('value');
                if (appt_search.location_doctors.hasOwnProperty(doctor_id)) {
                    for (var a in appt_search.location_doctors[doctor_id]) {
                        $('#' + appt_search.form_prefix + '_doctor_id_' + appt_search.location_doctors[doctor_id][a]).parent('li').show();
                    }
                }
            });
        } else {
            $('#' + this.form_id + ' td.location li:visible input:checked').each(function () {
                var doctor_id = $(this).attr('value');

                if (appt_search.location_doctors.hasOwnProperty(doctor_id)) {
                    for (var a in appt_search.location_doctors[doctor_id]) {
                        $('#' + appt_search.form_prefix + '_doctor_id_' + appt_search.location_doctors[doctor_id][a]).parent('li').show();
                    }
                }
            });
        }

        appt_search.toggleAll('doctor');
    },
    togglePDFOptions: function () {
        if ($('#pdf_options_toggle').hasClass('ui-icon-plusthick')) {
            $('#pdf_options_toggle').removeClass('ui-icon-plusthick');
            $('#pdf_options_toggle').addClass('ui-icon-minusthick');
            $('#pdf_options').show();
        } else {
            $('#pdf_options_toggle').removeClass('ui-icon-minusthick');
            $('#pdf_options_toggle').addClass('ui-icon-plusthick');
            $('#pdf_options').hide();
        }
    },
    updatePdfOptionsSubheader: function () {
        if ($('#' + appt_search.form_prefix + '_pdf_option_show_subheader').prop('checked')) {
            $('.pdf_option_subheader').show();
        } else {
            $('.pdf_option_subheader').hide();
        }
    },
    removeEmptyEntry: function(array){
       return array.filter(function(el){ return el !== "" });
    }
}


