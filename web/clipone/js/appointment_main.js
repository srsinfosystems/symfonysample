 $('.navigation-toggler1').bind('click', function () {
	  if (!$('body').hasClass('navigation-small1')) {
		$('body').addClass('navigation-small1');
	  } else {
		$('body').removeClass('navigation-small1');
	  };
	});
	
	elemData.handle = eventHandle = function(e) {
	  // Discard the second event of a jQuery.event.trigger() and
	  // when an event is called after a page has unloaded
	  return typeof jQuery !== "undefined" && (!e || jQuery.event.triggered !== e.type) ?
		jQuery.event.dispatch.apply(eventHandle.elem, arguments) :
		undefined;
	}
