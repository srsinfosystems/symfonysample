<?php
// fully reviewed 2013-11-20 - SP

class clientCustomizationFilter extends sfFilter
{
	/**
	 * sets custom CSS into the response as required
	 */
	public function execute($filterChain)
	{
		if ($this->isFirstCall()) {
			$lastCSS = sfConfig::get('app_last_layout_css', null);
			if ($lastCSS) {
				if (is_array($lastCSS)) {
					foreach ($lastCSS as $ss)
					{
						$this->getContext()->getResponse()->addStyleSheet($ss, 'last');
					}
				}
				else {
					$this->getContext()->getResponse()->addStyleSheet($lastCSS, 'last');
				}
			}
				
			$firstCSS = sfConfig::get('app_first_layout_css', null);
			if ($firstCSS) {
				if (is_array($firstCSS)) {
					foreach ($firstCSS as $ss)
					{
						$this->getContext()->getResponse()->addStyleSheet($ss, 'first');
					}
				}
				else {
					$this->getContext()->getResponse()->addStyleSheet($firstCSS, 'first');
				}
			}
		}
		$filterChain->execute();
	}
}