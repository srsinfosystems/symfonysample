<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class rememberFilter extends sfFilter {

    public function execute($filterChain) {
        // Execute this filter only once
//        if ($this->isFirstCall()) {
            // Filters don't have direct access to the request and user objects.
            // You will need to use the context object to get them
            $request = $this->getContext()->getRequest();
            $user = $this->getContext()->getUser();
//echo $request->getCookie('hype_medical_sk').'test';exit;
            if ($request->getCookie('hype_medical_sk')){
//            if ($request->getCookie(sfConfig::get('app_sf_guard_plugin_remember_cookie_name', 'sfRemember'))) {
                // sign in
               
                $this->getUser()->signin($user);
                $this->setAttribute('user_id', $user->getId(), 'sfGuardSecurityUser');
                $this->setAuthenticated(true);
            }
//        }

        // Execute next filter
        $filterChain->execute();
    }

}
