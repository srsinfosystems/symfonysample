<?php

// fully reviewed 2013-11-21 - SP

class hypeUser extends sfGuardSecurityUser {

    public function initialize(sfEventDispatcher $dispatcher, sfStorage $storage, $options = array()) {
// disable timeout anyway, doesn't matter, what user checked
        $options['timeout'] = false;
        Parent::initialize($dispatcher, $storage, $options);
    }

    public function hasCredential($credential, $useAnd = true) {
        return Parent::hasCredential($credential, $useAnd);
    }

    public function signIn($user, $remember = false, $con = null) {

        $this->clearCredentials();
        foreach ($this->getAttributeHolder()->getNamespaces() as $ns) {
            $this->getAttributeHolder()->removeNamespace($ns);
        }
        
         $remember = $remember == 1 ? true : false;

         Parent::signIn($user, $remember, $con);    

        
        $data = Doctrine_Query::create()
                ->from('UserProfile')
                ->addWhere('id = ?', $user->UserProfile->get('id'))
                ->execute(array(), Doctrine::HYDRATE_ARRAY);

        // echo '<pre>'; print_r($_SERVER);exit;

        if($data[0]['restricted_ips'] != ''){
            /*print_r($_SERVER);
            echo "Remote addr: " . $_SERVER['REMOTE_ADDR']."<br/>";
            echo "X Forward: " . $_SERVER['HTTP_X_FORWARDED_FOR']."<br/>";
            echo "Clien IP: " . $_SERVER['HTTP_CLIENT_IP']."<br/>";
            echo "SERVER_PORT" . $_SERVER['SERVER_PORT'];exit;*/

            $ipAddress = "";
            if(isset($_COOKIE['visitor_ip'])) {
                $ipAddress = $_COOKIE['visitor_ip'];
            }
            $banned = 0;
            if(!empty($ipAddress)){
                $dbIp = explode(',', $data[0]['restricted_ips']);

                foreach ($dbIp as  $value) {
                    if(!empty($value)) { 
                        $banned = 1;
                        break;
                    }
                }
                if($banned == 1) {
                    foreach ($dbIp as  $value) {
                        if(trim($ipAddress) == trim($value)) {
                            $banned = 0;
                            break;
                            }
                        }
                }
                if($banned == 1){
                    parent::signOut();
                    $this->getAttributeHolder()->clear();
                    parent::shutdown();
                }
            }
            }

        
        $this->setAttribute('user_name', $user->getUsername(), 'UserParameters');
        $this->setAttribute('user_id', $user->UserProfile->get('id'), 'UserParameters');
        $this->setAttribute('client_id', $user->UserProfile->getClientID(), 'UserParameters');
        $this->setAttribute('first_last', $user->UserProfile->getUserName(), 'UserParameters');

        //remember me session_commit()
        $this->setAttribute('remember', $remember, 'UserParameters');
        if($remember == 1){
           // $user = Doctrine_Query::create()
           //      ->from('sfGuardUser u')
           //      ->addWhere('u.username = (?)', $user)
           //      ->fetchOne();      
           // print_r($user->last_login); 
           // exit;  
           ini_set('session.gc_maxlifetime',14400);
           $maxlifetime = ini_set('session_cookie_lifetime',14400);
        }

        $this->setupLocations($user);
        $this->setupDoctors($user);

        $this->addCredential($user->UserProfile->UserType->name);
        foreach ($user->UserProfile->UserPermissionNew as $upn) {
            if ($upn->active) {
                $this->addCredential($upn->permission_name);
            }
        }
    }

    private function setupLocations($user) {
        $clientLocations = Doctrine_Query::create()
                ->from('Location l')
                ->addWhere('l.client_id = ?', $this->getAttribute('client_id', null, 'UserParameters'))
                ->addWhere('l.active = ?', array(true))
                ->execute();

        if (count($clientLocations) == 1) {
            $this->setDefaultLocation($clientLocations[0]['id']);
        } else {
            $this->setDefaultLocation(null);
        }

        $userLocations = array();
        if (($user->UserProfile->client_id != $this->getClientID()) || !$user->UserProfile->all_locations) {
            $userLocations = Doctrine_Query::create()
                    ->from('UserLocation ul INDEXBY ul.location_id')
                    ->addWhere('ul.user_id = (?)', $user->UserProfile->id)
                    ->execute(array(), Doctrine::HYDRATE_ARRAY);
        }

        $locations = array();
        foreach ($clientLocations as $location) {
            if ($location->active) {
                if (count($userLocations)) {
                    if (array_key_exists($location['id'], $userLocations)) {
                        $locations[$location['id']] = $location->getShortCode();
                    }
                } else {
                    $locations[$location['id']] = $location->getShortCode();
                }
            }
        }
        $this->setAttribute('location_ids', $locations, 'UserParameter');
    }

    public function setDefaultLocation($locationID) {
        if (!$locationID) {
            $this->setAttribute('location_name', 'All ' . $this->getLocationTitle(true), 'ClientParameters');
            return;
        }

        $loc = Doctrine_Query::create()
                ->from('Location l')
                ->leftJoin('l.DoctorLocation dl')
                ->leftJoin('dl.Doctor d')
                ->addWhere('l.id = ?', array($locationID))
                ->addWhere('l.active = ?', array(true))
                ->fetchOne();

        if ($loc instanceof Location) {
            $this->setAttribute('current_location', $loc->get('id'), 'UserParameter');
            $this->setAttribute('location_name', $loc->getShortCode(), 'ClientParameters');
            $this->setAttribute('default_locations', array($loc->get('id')), 'AppointmentBook');

            $doctor_codes = array();
            foreach ($loc->DoctorLocation as $ld) {
                $doctor_codes[] = $ld->Doctor->qc_doctor_code;
            }
            $this->setAttribute('default_doctors', $doctor_codes, 'AppointmentBook');
        } else {
            $this->setAttribute('location_name', 'All ' . $this->getLocationTitle(true), 'ClientParameters');
        }
    }

    public function getLocationTitle($plural = false) {
        $value = $this->getClientParameter('location_title');

        return $value . ($plural ? 's' : '');
    }

    public function getReportDefaults($parameterName = 'appt') {
        $value = ClientParamTable::getParamOrDefaultValueForClient($this->getClientID(), $parameterName);
        return $value;
    }

    public function setReportDefaults($parameterName = 'appt', $value) {
        $value = ClientParamTable::setParamForClient($this->getClientID(), $parameterName, $value);
        return $value;
    }
    
    public function setClientParameter($parameterName, $value){
        $value = ClientParamTable::setParamForClient($this->getClientID(), $parameterName, $value);
        return $value;
    }

    public function getClientParameter($parameterName, $namespace = 'ClientParameters', $saveInSession = true) {
        $value = $this->getAttribute($parameterName, null, $namespace);
        
//        if (is_null($value)) {
            $value = ClientParamTable::getParamOrDefaultValueForClient($this->getClientID(), $parameterName);
            if ($saveInSession) {
                $this->setAttribute($parameterName, $value, $namespace);
            }
//        }

        return $value;
    }

    public function getClientID() {
        return $this->getAttribute('client_id', null, 'UserParameters');
    }

    public function getUserID() {
        return $this->getAttribute('user_id', null, 'UserParameters');
    }

    public function setupDoctors($user) {
        $doctorToString = $this->getDoctorTitleFormat();
        $this->setAttribute('all_doctors', $user->UserProfile->all_doctors, 'ClientParameters');

        if (($user->UserProfile->client_id != $this->getClientID()) || $user->UserProfile->all_doctors) {
            $doctors = Doctrine_Query::create()
                    ->from('Doctor d')
                    ->addWhere('d.client_id = (?)', array($this->getClientID()))
                    ->addWhere('d.active = (?)', array(true))
                    ->execute();
        } else {
            $doctors = Doctrine_Query::create()
                    ->from('Doctor d')
                    ->leftJoin('d.UserDoctor ud')
                    ->addWhere('ud.user_id = (?) OR ud.user_id IS NULL', array($user->UserProfile->id))
                    ->addWhere('d.active = (?)', array(true))
                    ->addWhere('ud.doctor_id is not null')
                    ->execute();
        }

        $docs = array();
        foreach ($doctors as $doctor) {
            $docs[$doctor['id']] = $doctor->getCustomToString($doctorToString);
        }

        asort($docs);
        $this->setAttribute('doctor_ids', $docs, 'ClientParameters');
    }

    public function getDoctorTitleFormat() {
        return $this->getClientParameter('doctor_title_format');
    }

    public function getDoctorId() {
        return $this->getClientParameter('doctor_id');
    }
    

    public function signOut() {
        parent::signOut();
        $this->getAttributeHolder()->clear();
        parent::shutdown();
    }

    public function switchClientID($clientID) {

        $username = $this->getAttribute('user_name', null, 'UserParameters');
        $user_id = $this->getUserID();
        $sfg_id = $this->getAttribute('user_id', null, 'sfGuardSecurityUser');

        foreach ($this->getAttributeHolder()->getNamespaces() as $ns) {
            $this->getAttributeHolder()->removeNamespace($ns);
        }

        $this->setAttribute('user_name', $username, 'UserParameters');
        $this->setAttribute('user_id', $user_id, 'UserParameters');
        $this->setAttribute('user_id', $sfg_id, 'sfGuardSecurityUser');
        $this->setAttribute('client_id', $clientID, 'UserParameters');

        $user = Doctrine_Query::create()
                ->from('sfGuardUser u')
                ->addWhere('u.id = (?)', $sfg_id)
                ->fetchOne();

        $this->setupLocations($user);
        $this->setupDoctors($user);

        $this->addCredential($user->UserProfile->UserType->name);
        foreach ($user->UserProfile->UserPermissionNew as $upn) {
            if ($upn->active) {
                $this->addCredential($upn->permission_name);
            }
        }
    }

    /*     * * CLIENT PARAMETER RETRIEVAL FUNCTIONS * */

    public function getAllowSimultaneousDoctorSchedules() {
        return $this->getClientParameter('allow_simultaneous_doctor_schedules');
    }

    public function getAppointmentBookCloseOnlyPast() {
        return $this->getClientParameter('appointment_close_only_past');
    }

    public function getAppointmentBookConsultationCancellationWindow() {
        return $this->getClientParameter('consultation_cancellation_window');
    }

    public function getAppointmentBookConsultationColour() {
        return $this->getClientParameter('consultation_colour');
    }

    public function getAppointmentBookConsultationLength() {
        return $this->getClientParameter('consultation_length');
    }

    public function getAppointmentBookConsultationTitle() {
        return $this->getClientParameter('consultation_appointment_title');
    }

    public function getAppointmentBookCreatePastRepeatingEvents() {
        return $this->getClientParameter('appointment_create_past_repeating');
    }

    public function getAppointmentBookCreatePatients() {
        return $this->getClientParameter('appointment_enable_patient_creation');
    }

    public function getAppointmentBookDetailsColumns() {
        $value = $this->getClientParameter('appointment_detail_columns');
        if (!is_array($value)) {
            return explode(',', $value);
        }
        return $value;
    }

    public function getAppointmentBookDragToReschedule() {
        return $this->getClientParameter('appointment_drag_to_reschedule');
    }

    public function getAppointmentBookDropdownInterval() {
        return $this->getClientParameter('appointment_dropdown_interval');
    }

    public function getAppointmentBookEnableCounts() {
        return $this->getClientParameter('appointment_book_enable_counts');
    }

    public function getAppointmentBookOverrideRescheduling() {
        return $this->getClientParameter('appointment_skip_confirm_on_reschedule');
    }

    public function getAppointmentBookEnableDailyNotes() {
        return $this->getClientParameter('appointment_book_enable_daily_notes');
    }

    public function getAppointmentBookHideStartTimes() {
        return $this->getClientParameter('appointment_hide_start_times');
    }

    public function getAppointmentBookIgnoreOverlapCheck() {
        return $this->getClientParameter('appointment_ignore_overlap');
    }

    public function getAppointmentBookIncludeRecentNote() {
        return $this->getClientParameter('appointment_include_recent_note');
    }

    public function getAppointmentBookOverwriteServiceCode() {
        return $this->getClientParameter('appointment_overwrite_service_code');
    }

    public function getAppointmentBookNudgeEvents() {
        return $this->getClientParameter('appointment_nudge_events');
    }

    public function getAppointmentBookShowEndTimes() {
        return $this->getClientParameter('appointment_show_end_times');
    }

    public function getAppointmentBookShowMisc() {
        return $this->getClientParameter('appointment_book_show_misc');
    }

    public function getAppointmentBookShowRedNames() {
        return $this->getClientParameter('appointment_title_red_name');
    }

    public function getAppointmentBookShowTimelineMinutes() {
        return $this->getClientParameter('appointment_book_timeline_minutes');
    }

    public function getAppointmentBookTimeslotsPerHour() {
        return $this->getClientParameter('appointment_book_timeslots_per_hour');
    }

    public function getAppointmentBookTitleAlignment() {
        return $this->getClientParameter('appointment_title_alignment');
    }

    public function getAppointmentBookUseComplexAppointmentTypes() {
        return $this->getClientParameter('use_complex_appointment_types');
    }

    public function getAppointmentBookViewsToShow() {
        return $this->getClientParameter('appointment_book_views');
    }

    public function getAppointmentSearchDefaultEndDate() {
        return $this->getClientParameter('appointment_search_date_end');
    }

    public function getAppointmentSearchDefaultSort() {
        return $this->getClientParameter('appointment_search_default_sort');
    }

    public function getAppointmentSearchDefaultStartDate() {
        return $this->getClientParameter('appointment_search_date_start');
    }

    public function getAppointmentSearchDefaultStatuses() {
        return $this->getClientParameter('appointment_search_default_statuses');
    }

    public function getAppointmentSearchPDFIncludeDateRange() {
        return $this->getClientParameter('appointment_search_pdf_include_date_range');
    }

    public function getAppointmentSearchIncludeInactiveDoctors() {
        return $this->getClientParameter('appointment_search_include_inactive_doctors');
    }

    public function getAppointmentSearchRemoveProvinces() {
        return $this->getClientParameter('appointment_search_remove_provinces');
    }

    public function getAppointmentSearchRemoveRegions() {
        return $this->getClientParameter('appointment_search_remove_regions');
    }

    public function getAppointmentSearchReportColumns() {
        return $this->getClientParameter('appointment_search_default_columns');
    }

    public function getPastClaimsDefaultReportColumns() {
        return $this->getClientParameter('past_claims_default_report_cols');
    }

    public function getAppointmentSearchReportHeader() {
        return $this->getClientParameter('appointment_search_report_header');
    }

    public function getAppointmentTitleFormat() {
        return $this->getClientParameter('appointment_title_format');
    }

    public function getAutofillClaimDetails() {
        return $this->getClientParameter('autofill_claims');
    }

    public function getAutofillClaimItemServiceDate() {
        return $this->getClientParameter('autofill_service_date');
    }

    public function getBaseAnaFee() {
        return $this->getSystemParameter('base_ana_fee');
    }

    private function getSystemParameter($parameterName, $namespace = 'ClientParameters') {
        $value = $this->getAttribute($parameterName, $namespace);
        if (is_null($value)) {
            $value = SystemParamTable::getValue($parameterName);
            $this->setAttribute($parameterName, $value, $namespace);
        }

        return $value;
    }

    public function getBaseAssistantFee() {
        return $this->getSystemParameter('base_assistant_fee');
    }

    public function getCheckServiceCodeGroupNumber() {
        return $this->getClientParameter('service_code_group_number_check');
    }

    public function getCheckServiceCodeTechnicalRefDoc() {
        return $this->getClientParameter('service_code_tech_require_ref_doc');
    }

    public function getClaimAlwaysShowTemporaryButton() {
        return $this->getClientParameter('claim_always_show_temporary_button');
    }

    public function getPremiumBypassOption() {
        return $this->getClientParameter('premium_code_bypass');
    }

    public function getClaimItemIntervalCheck() {
        return $this->getClientParameter('claim_item_interval_check');
    }

    public function getPatientHistorySelection() {
        return $this->getClientParameter('patient_history_selection');
    }

    public function getClaimDownArrow() {
        return $this->getClientParameter('down_arrow_option');
    }

    public function getLoadLastFiveSeconds() {
        return $this->getClientParameter('load_last_five_services');
    }

    public function getCompareAutofill() {
        return $this->getClientParameter('compare_autofill');
    }

    public function getSplitClaimAvailable() {
        return $this->getClientParameter('split_claim');
    }

    public function getClaimsDefaultSuffix() {
        return $this->getClientParameter('claims_default_suffix');
    }

    public function getClaimsSkipSearchTab() {
        return $this->getClientParameter('claims_skip_search_tab');
    }

    public function getClientName() {
        $client = Doctrine_Query::create()
                ->from('Client c')
                ->addWhere('c.id = (?)', $this->getClientID())
                ->fetchOne(array(), Doctrine::HYDRATE_ARRAY);

        return $client['name'];
    }

    public function getClientHL7InboundPartners() {
        $value = $this->getClientParameter('client_hl7_inbound_partners');

        if (is_null($value)) {
            $value = array();

            $client = Doctrine_Query::create()
                    ->from('Client c')
                    ->addWhere('c.id = (?)', $this->getClientID())
                    ->fetchOne();

            if ($client instanceof Client) {
                $partners = $client->hl7_inbound_partners;
                if ($partners) {
                    $value = strpos($partners, ';') !== false ? explode(';', $partners) : array($partners);
                }

                $this->setAttribute('client_hl7_inbound_partners', $value, 'ClientParameters');
            }
        }
        return $value;
    }

    public function getClientHL7OutboundPartners() {
        $value = $this->getClientParameter('client_hl7_outbound_partners');

        if (is_null($value)) {
            $value = array();

            $client = Doctrine_Query::create()
                    ->from('Client c')
                    ->addWhere('c.id = (?)', $this->getClientID())
                    ->fetchOne();

            if ($client instanceof Client) {
                $partners = $client->hl7_outbound_partners;
                if ($partners) {
                    $value = strpos($partners, ';') !== false ? explode(';', $partners) : array($partners);
                }

                $this->setAttribute('client_hl7_outbound_partners', $value, 'ClientParameters');
            }
        }
        return $value;
    }

    public function getClientHL7Partners() {
        $value = $this->getClientParameter('client_hl7_partners');

        if (is_null($value)) {
            $value = array();

            $client = Doctrine_Query::create()
                    ->from('Client c')
                    ->addWhere('c.id = (?)', $this->getClientID())
                    ->fetchOne();

            if ($client instanceof Client) {
                $partners = $client->hl7_outbound_partners;
                if ($client->hl7_inbound_partners) {
                    if ($partners) {
                        $partners .= ';';
                    }
                    $partners .= $client->hl7_inbound_partners;
                }
                if ($partners) {
                    $value = strpos($partners, ';') !== false ? explode(';', $partners) : array($partners);
                    $value = array_unique($value);
                }

                $this->setAttribute('client_hl7_partners', $value, 'ClientParameters');
            }
        }
        return $value;
    }

    public function getConsultationEarliestTime() {
        return $this->getClientParameter('consultation_earliest_time');
    }

    public function getConsultationLatestTime() {
        return $this->getClientParameter('consultation_latest_time');
    }

    public function getConsultationNoShowNote() {
        return $this->getClientParameter('consultation_no_show_note');
    }

    public function getConsultationNoShowPatientStatus() {
        return $this->getClientParameter('consultation_no_show_patient_status');
    }

    public function getConsultationNoShowReasonCode() {
        return $this->getClientParameter('consultation_no_show_reason_code');
    }

    public function getConsultationPatientNote() {
        return $this->getClientParameter('consultation_patient_note');
    }

    public function getConsultationPatientReasonCode() {
        return $this->getClientParameter('consultation_patient_reason_code');
    }

    public function getConsultationPatientStatus() {
        return $this->getClientParameter('consultation_patient_status');
    }

    public function getConsultationRelocateOnReschedule() {
        return $this->getClientParameter('consultation_relocate_on_reschedule');
    }

    public function getConsultationTitleBarColour() {
        return $this->getClientParameter('consultation_title_bar_colour');
    }

    public function getCurrentLocID() {
        if($this->getAttribute('current_location', null, 'UserParameter') == null){
            $current_location_id = $_COOKIE['default_locations_cookie'][$this->getClientID()];
        } else {
            $current_location_id = $this->getAttribute('current_location', null, 'UserParameter');
        }
        return $current_location_id;
    }

    public function getDashboardShowFollowUpWidget() {
        return (bool) $this->getClientParameter('dashboard_appointment_follow_up_widget');
    }

    public function getDashboardGenerateBlankAppointments() {
        return (bool) $this->getClientParameter('generate_blank_appointments');
    }

    public function getDashboardShowMyDoctorCareReportWidget() {
        return (bool) $this->getClientParameter('show_report_widget');
    }

    public function getDashboardShowOrphansWidget() {
        return (bool) $this->getClientParameter('dashboard_appointment_orphans_widget');
    }

    public function getDashboardShowPatientFollowUpsWidget() {
        return (bool) $this->getClientParameter('dashboard_follow_up_widget');
    }

    public function getDashboardShowTodaysAppointmentsWidget() {
        return (bool) $this->getClientParameter('dashboard_appointment_book_widget');
    }

    public function getDateTimeFormat() {
        return $this->getDateFormat() . ' ' . $this->getTimeFormat();
    }

    public function getDateFormat() {
        if ($this->getClientParameter('date_format') == 'd/m/yy') {
            return 'd/m/y';
        } else {
            return $this->getClientParameter('date_format');
        }
    }

    public function getTimeFormat() {
        return $this->getClientParameter('time_format', 'ClientParameters');
    }

    public function getDefaultPatientTypeId() {
        return $this->getClientParameter('default_patient_type_id');
    }

    public function getDefaultProvince() {
        return $this->getClientParameter('patient_default_province');
    }

    public function getDefaultPrinter() {
        return $this->getAttribute('default_printer', null, 'UserParameter');
    }

    public function getDoctors() {
        return $this->getAttribute('doctor_ids', array(), 'ClientParameters');
    }

    public function getEchobaseCreatePracticeSendDate() {
        return $this->getClientParameter('echobase_create_practice_send_date', 'ClientParameters', false);
    }

    public function getEchobasePracticeAdmin() {
        return $this->getClientParameter('echobase_practice_name', 'ClientParameters', false);
    }

    public function getEchobasePracticeAdminPw() {
        return $this->getClientParameter('echobase_practice_adminpw', 'ClientParameters', false);
    }

    public function getEchobasePracticeName() {
        $rv = $this->getClientParameter('echobase_practice_name', 'ClientParameters', false);
        if (is_null($rv)) {
            $client = Doctrine_Query::create()
                    ->from('Client c')
                    ->addWhere('c.id = (?)', array($this->getClientID()))
                    ->fetchOne();

            if ($client instanceof Client) {
                return $client->name;
            }
        }
        return $rv;
    }

    public function getEnableLocationMapping() {
        return (bool) $this->getClientHasModule(hype::MODULE_MAPPING);
    }

    public function getClientHasModule($moduleName) {
        $str = 'client_module_' . $moduleName;
        $rv = $this->getAttribute($str, null, 'ClientModules');

        if ($rv) {
            
        } else {
            $rv = false;

            $clientModule = Doctrine_Query::create()
                    ->from('ClientModule cm')
                    ->addWhere('cm.client_id = (?)', $this->getClientID())
                    ->addWhere('cm.module_name = (?)', $moduleName)
                    ->fetchOne(array(), Doctrine::HYDRATE_ARRAY);

            if (count($clientModule)) {
                $rv = (bool) $clientModule['active'];
            }
            $this->setAttribute($str, $rv, 'ClientModules');
        }

        return $rv;
    }

    public function getEnablePrintService() {
        return (bool) $this->getClientParameter('enable_print_service');
    }

    public function getFillDownDiagCode() {
        return $this->getClientParameter('fill_down_diag_code');
    }

    public function getFollowUpDefaultDate() {
        $value = $this->getClientParameter('patient_form_default_follow_up_date');
        if ($value == 'today') {
            return date($this->getDateFormat());
        }
        if ($value == 'tomorrow') {
            return date($this->getDateFormat(), strtotime('tomorrow'));
        }
        return '';
    }

    public function getGoogleMapsApiKey() {
        return $this->getClientParameter('google_maps_api_key');
    }

    public function getNewPDFSFlag() {
        return $this->getClientParameter('new_pdf_flag');
    }

    public function getJSDateTimeFormat() {
        return $this->getJSDateFormat() . ' ' . $this->getJSTimeFormat();
    }

    public function getJSDateFormat() {
        $value = ClientParamTable::getParamOrDefaultValueForClient($this->getClientID(), 'date_format');

        if ($saveInSession) {
            $this->setAttribute($parameterName, $value, $namespace);
        }
//        $value = $this->getClientParameter('date_format');
        return $value;
    }

    public function getJSTimeFormat() {
        $value = $this->getClientParameter('time_format');
        return str_replace(array('h', 'H', 'i'), array('hh', 'HH', 'ii'), $value);
    }

    public function getLocations() {
        return $this->getAttribute('location_ids', array(), 'UserParameter');
    }

    public function getLocationsOpenWeekends() {
        return $this->getClientParameter('location_open_weekends');
    }

    public function getMultiSelectServiceDate() {
        return $this->getClientParameter('service_date_multiselect');
    }

    public function getNewDirectPaymentCheckMakePayment() {
        return $this->getClientParameter('direct_autocheck_make_a_payment', 'ClientParameters');
    }

    public function getNewDirectPaymentForcePayment() {
        return $this->getClientParameter('new_direct_always_make_payment', 'ClientParameters');
    }

    public function getPastClaimsCreationDateEndDefault() {
        return $this->getClientParameter('claim_search_creation_date_end', 'ClientParameters');
    }

    public function getPastClaimsCreationDateStartDefault() {
        return $this->getClientParameter('claim_search_creation_date_start', 'ClientParameters');
    }

    public function getPastClaimsDefaultClaimStatus() {
        return $this->getClientParameter('claim_search_default_claim_status', 'ClientParameters');
    }

    public function getPastClaimsDefaultClaimType() {
        return $this->getClientParameter('claim_search_default_claim_type', 'ClientParameters');
    }

    public function getPastClaimsRemoveDob() {
        return $this->getClientParameter('claim_search_remove_dob', 'ClientParameters');
    }

    public function getPastClaimsRemoveRefDoc() {
        return $this->getClientParameter('claim_search_remove_refdoc', 'ClientParameters');
    }

    public function getPastClaimsRemoveSex() {
        return $this->getClientParameter('claim_search_remove_sex', 'ClientParameters');
    }

    public function getPastClaimsReportHeader() {
        return $this->getClientParameter('past_claims_report_header', 'ClientParameters');
    }

    public function getPastClaimsServiceDateEndDefault() {
        return $this->getClientParameter('claim_search_service_date_end', 'ClientParameters');
    }

    public function getPastClaimsServiceDateStartDefault() {
        return $this->getClientParameter('claim_search_service_date_start', 'ClientParameters');
    }

    public function getPatientDOBLimit() {
        return $this->getClientParameter('patient_dob_limit');
    }

    public function getPatientAppointmentChecklist() {
        return $this->getClientParameter('patient_appointment_check', 'ClientParameters');
    }

    public function getPDFPreference() {
        return $this->getClientParameter('pdf_preference', 'ClientParameters');
    }

    public function getClaimAndPatientRequiresNames() {
        return $this->getClientParameter('claim_patient_require_name_fields', 'ClientParameters');
    }

    public function getDoctorsListIncludeInactive() {
        return $this->getClientParameter('include_inactive_doctors', 'ClientParameters');
    }

    public function getPatientRequireLocationID() {
        return $this->getClientParameter('require_loc_id', 'ClientParameters');
    }

    public function getPatientSearchAlwaysShowResultsDialog() {
        return $this->getClientParameter('patient_search_always_show_results_dialog', 'ClientParameters');
    }

    public function getPatientSuggestedFields($json = false) {
        $all_options = PatientTable::getSuggestedFieldsChoices();
        $values = $this->getClientParameter('patient_suggested_fields', 'ClientParameters');

        if (!is_array($values)) {
            $values = explode(',', $values);
        }

        foreach ($all_options as $key => $option) {
            if (!in_array($key, $values)) {
                unset($all_options[$key]);
            }
        }

        if ($json) {
            return json_encode($all_options);
        }
        return $all_options;
    }

    public function getPatientSearchMatchType() {
        return $this->getClientParameter('patient_search_match_type', 'ClientParameters');
    }

    public function getPatientSearchSkipToHcn() {
        return $this->getClientParameter('patient_search_skip_to_hcn', 'ClientParameters');
    }

    public function getPatientTypeChangeChecklist() {
        return $this->getClientParameter('patient_type_check', 'ClientParameters');
    }

    public function getPatientUseAlphaPatientNumbers() {
        return $this->getClientParameter('patient_alpha_patient_numbers', 'ClientParameters');
    }

    public function getPdfFooterHypeMessage() {
        return $this->getClientParameter('pdf_footer_hype_systems', 'ClientParameters');
    }

    public function getRequirePatientSearch() {
        return $this->getClientParameter('patient_search_first', 'ClientParameters');
    }

    public function getShowDirectCodesForThirdParty() {
        return $this->getClientParameter('claims_show_direct_codes_for_third_party', 'ClientParameters');
    }

    public function getSubtotalCurrentClaim() {
        return $this->getClientParameter('subtotal_current_claim', 'ClientParameters');
    }

    public function getThirdPartyRequiresPolicyNum() {
        return $this->getClientParameter('require_policy_num', 'ClientParameters');
    }

    public function getUpdateAppointmentsOnClaimSave() {
        return $this->getClientParameter('claim_update_appointment_status', 'ClientParameters');
    }

    public function getUpdateExistingClaims() {
        return $this->getClientParameter('update_existing_claims', 'ClientParameters');
    }

    public function getUseHealthCardSwipeReplace() {
        return $this->getClientParameter('swipe_replace', 'ClientParameters');
    }

    public function getUseMohProvince() {
        return $this->getClientParameter('use_moh_province', 'ClientParameters');
    }

    public function getUseChartNumbers() {
        return $this->getClientParameter('chart_numbers', 'ClientParameters');
    }

    public function isAllDoctors() {
        return (bool) $this->getAttribute('all_doctors', false, 'ClientParameters');
    }

    public function setFormDefaults($formName, $values = array(), $omissions = array()) {

        $omissions[] = '_csrf_token';
        $omissions[] = 'client_id';
        $omissions[] = 'clientID';

        foreach ($omissions as $key) {
            unset($values[$key]);
        }

        $values = serialize($values);

        $up = Doctrine_Query::create()
                ->from('UserParam up')
                ->addWhere('up.user_id = (?)', $this->getUserID())
                ->addWhere('up.param = (?)', $formName)
                ->fetchOne();



        if (!$up instanceof UserParam) {
            $up = new UserParam();
            $up->setUserId($this->getUserID());
            $up->setParam($formName);
        }

        
        $up->setValue($values);
        $up->save();
    }

    public function getFormDefaultsForSpecificKeys($formName, $rs = array(), $keys = array()) {
        $rs = $this->getFormDefaults($formName, $rs);

        foreach ($rs as $key => $value) {
            if (!in_array($key, $keys)) {
                unset($rs[$key]);
            }
        }

        return $rs;
    }

    public function getFormDefaults($formName, $rs = array()) {
        $up = Doctrine_Query::create()
                ->from('UserParam up')
                ->addWhere('up.user_id = (?)', $this->getUserID())
                ->addWhere('up.param = (?)', $formName)
                ->limit(1)
                ->fetchOne();

        if ($up instanceof UserParam) {
            $x = unserialize($up->getValue());
            if (is_array($x)) {
                foreach ($x as $key => $val) {
                    $rs[$key] = $val;
                }
            }
        }
        return $rs;
    }

}
