<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <?php include_title() ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Style-Type" content="text/css" />

        <?php include_stylesheets() ?>
        <?php include_javascripts() ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>

        <!-- The HTML5 Shim is required for older browsers, mainly older versions IE -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

    </head>
    <body>
        <div id="spinner" style="display:none;z-index: 999999;width: 100%;height: 100vh;background: #000;opacity: 0.5;position: fixed;">
            <i class="fa clip-spinner-5 fa-spin" style="font-size: 60px;color: #fff;position: absolute;margin: 0 auto;top: 30%;left: 35%;"></i>
            <span id="messages_text" class="right" style="font-size: 40px;opacity:1;color: #fff;position: absolute;margin: 0 auto;top: 30%;left: 40%;"></span>
        </div>
        <?php if ($sf_user->isAuthenticated()) { ?>
            <?php include_partial('global/header'); ?>
            <div class="main-container">
                <?php include_component('default', 'menu'); ?>
                <div class="main-content">
                    <?php if ($sf_user->hasFlash('notice')): ?>
                        <div class="flash_notice <?php echo $sf_user->getFlash('notice_class'); ?>"><?php echo $sf_user->getFlash('notice') ?></div>
                    <?php endif; ?>
                    <?php echo $sf_content; ?>
                </div>
            </div>

            <footer id="footer">
                <?php include_partial('global/' . sfConfig::get('app_footer_file')); ?>
            </footer>
<input id="fontSizeChanger" type="hidden" />
        <?php }else { ?>
            <?php echo $sf_content; ?>
        <?php } ?>
        <script type="text/javascript">
            quickClaim.dateFormat = '<?php echo sfOutputEscaper::escape('esc_specialchars', $sf_user->getJSDateFormat()); ?>';
            quickClaim.timeFormat = '<?php echo sfOutputEscaper::escape('esc_specialchars', $sf_user->getJSTimeFormat()); ?>';
            quickClaim.dateTimeFormat = '<?php echo sfOutputEscaper::escape('esc_specialchars', $sf_user->getJSDateTimeFormat()); ?>';
            quickClaim.useAlphaPatientNumbers = <?php echo $sf_user->getPatientUseAlphaPatientNumbers() ? 'true' : 'false'; ?>;
            quickClaim.skipToHCN = <?php echo $sf_user->getPatientSearchSkipToHcn() ? 'true' : 'false'; ?>;
            quickClaim.tablesorterTooltip = '<?php echo image_tag('icons/info.gif', array('class' => 'sortTip', 'align' => 'right', 'title' => 'Click columns with an up/down arrow to sort. &nbsp; Hold Shift to add additional columns to the sort order.')); ?>';
            quickClaim.contextMenuImage = '<?php echo image_path('icons/redDown.png'); ?>';


            $(window).load(function () {
                if ($('ul.sf-menu').size()) {
                    var min_width = parseInt($('ul.sf-menu').css('padding-left').replace("px", "")) + parseInt($('ul.sf-menu').css('padding-right').replace("px", ""));
                    $('ul.sf-menu li.first_submenu').each(function () {
                        min_width += $(this).width();
                    });
                    $('ul.sf-menu').css('width', min_width);

                    $('ul.sf-menu').superfish({
                        delay: 500,
                        speed: 'fast',
                        autoArrows: false,
                        dropShadows: false
                    });
                    jQuery(".sf-menu > li > a").append("<i></i>")
                }
            });
            $(window).scroll(function () {
                quickClaim.handleScroll();
            });


        </script>
        <script>
            setTimeout(function () {
                Main.init();
//                $('.hasDatepick').datepicker({changeMonth: true, changeYear: true, autoclose: true});
//                $("input[type='checkbox'], input[type='radio']").iCheck({
//                    checkboxClass: 'icheckbox_minimal',
//                    radioClass: 'iradio_minimal'
//                });

$('.Select2').removeClass('form-control');
                            $(".Select2").select2({
                                placeholder: "-- select --",
                                allowClear: true,
                                width: '100%'
                            });
            }, 2000)
            if ($(window).width() > 1500) {
                $('body').addClass('layout-boxed');
            }
        </script>
                    <?php if ($sf_user->isAuthenticated()) { ?>
                        <script>
                            $(document).ready(function () {
                                // check client color, font settings
                                $("#fontSizeChanger").easyView();
                                var dataColor = sessionStorage.getItem('color');
                                var dataFontSize = sessionStorage.getItem('fontSize');
                                if (dataColor) {
                                    localStorage.setItem('color', dataColor);
                                    $("p,span,small,h1,h2,h3,h4,h5,h6,li,div,input[type='text'],textarea,select")
                                                    .not('.input-group-addon')
                                                    .not('.ui-button-text')
                                                    .not('.wc-time')
                                                    .not('.main-navigation-menu span')
                                                    .css('color', dataColor);
                                            
                                    if ($("#text_color")) {
                                        $("#text_color").minicolors('value', dataColor);
                                    }
                                }
                                if(dataFontSize){
                                    localStorage.setItem('fontSize', dataFontSize);

                                    if (dataFontSize > 0) {
                                        for (var i = 0; i <= dataFontSize; i++) {
                                            $('#fontSizeChanger').easyView('increase');
                                        }
                                    } else {
                                        for (var i = 0; i >= dataFontSize; i--) {
                                            $('#fontSizeChanger').easyView('decrease');
                                        }
                                    }
                                }
                                /*$.ajax({
                                    url: "/default/gettextsettings",
                                    type: "POST",
                                    success: function (res) {
                                        var obj = JSON.parse(res);
                                        if (obj.text_color != null && obj.text_color != '') {
                                            $("p,span,small,h1,h2,h3,h4,h5,h6,li,div,input[type='text'],textarea,select")
                                                    .not('.input-group-addon')
                                                    .not('.ui-button-text')
                                                    .not('.wc-time')
                                                    .not('.main-navigation-menu span')
                                                    .css('color', obj.text_color);
                                            
                                            if ($("#text_color")) {
                                                $("#text_color").minicolors('value', obj.text_color);
                                            }
                                        }

                                        if (obj.font_sizes) {
                                            if (obj.font_sizes > 0) {
                                                for (var i = 0; i <= obj.font_sizes; i++) {
                                                    $('#fontSizeChanger').easyView('increase');
                                                }
                                            } else {
                                                for (var i = 0; i >= obj.font_sizes; i--) {
                                                    $('#fontSizeChanger').easyView('decrease');
                                                }
                                            }
                                        }
                                    }
                                })*/
                            })
                        </script>
                    <?php } ?>

                    <script>
                        global_js.print_font_size = sk.print_font_size = <?php echo $sf_user->getClientParameter('font_options')  ?>;
                        sk.appt_book_sidemenu_position = '<?php echo $sf_user->getClientParameter('appt_book_sidemenu_position') ?>';
                    </script>
                    <script type="text/javascript">
                        shortcuts.add('alt+shift+h',function() {
                            window.location = '<?php echo url_for('default/main'); ?>';
                        });

                        shortcuts.add('alt+shift+p',function() {
                            window.location = '<?php echo url_for('patient/view'); ?>';
                        });

                        shortcuts.add('alt+shift+a',function() {
                            window.location = '<?php echo url_for('appointment_book/index'); ?>';
                        });

                        shortcuts.add('alt+shift+c',function() {
                            window.location = '<?php echo url_for('claim/index'); ?>';
                        });

                        shortcuts.add('alt+shift+d',function() {
                            window.location = '<?php echo url_for('provider/index'); ?>';
                        });

                        shortcuts.add('alt+shift+s',function() {
                            window.location = '<?php echo url_for('region/index'); ?>';
                        });
                    </script>
    </body>
</html>
