<?php if ($url && $id): ?>
    <form action="<?php echo $url; ?>" id="<?php echo $id; ?>" method="post">
    <?php endif; ?>
    <div class='row'>
        <?php
        foreach ($form as $key => $field):
            if ($key != 'id' AND $key != '_csrf_token') {
                ?>
        <?php if($key == 'inbound' || $key == 'outbound'){ ?>
                <div class="col-md-12 col-sm-12">
                    <div class="form-group">
                        <label class="control-label">
                            <b><?php echo $field->renderLabel() ?></b><?php if($error): ?><small style="color:red;margin-left:10px"><?php echo $error; ?></small><?php endif; ?>
                        </label>
                        <?php                        
                            echo $field->render(['class' => $class])
                        ?>
                       <small class="text-muted">
                            <?php echo $field->renderHelp(); ?>
                        </small>
                    </div>                
                </div>
        <?php }else{ ?>
            <div class="col-md-4 col-sm-4">
                <div class="form-group" style="min-height:64px">
                        <label class="control-label">
                            <?php echo $field->renderLabel() ?>  <?php if($error): ?><small style="color:red;margin-left:10px"><?php echo $error; ?></small> <?php endif; ?>                     
                        </label>
                        <?php
                        $class = $field->hasError() ? 'skhas-error' : 'form-control';
                        echo $field->render(['class' => $class])
                        ?>
                       <small class="text-muted"><?php echo $field->renderHelp(); ?></small>
                    </div>                
                </div>
                
        <?php } ?> 
                    
            <?php } endforeach; ?>
        <div class="col-md-12 col-sm-12">
            <?php echo $form->renderBottomErrorBox(); ?>
            <?php echo $form->renderHiddenFields(); ?>
            <button onclick="$('#<?php echo $button; ?>').trigger('click')" class="btn btn-primary btn-squared" type="button"><i class="clip-folder-plus"></i> Save</button>
            <input style="display:none" type="submit" value="Save" id="<?php echo $button; ?>" name="<?php echo $button; ?>" class="left " />
        </div>

    </div>

    <?php if ($url && $id): ?>
    </form>
<?php endif; ?>