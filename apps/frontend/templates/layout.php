<!DOCTYPE html>
<html class="no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <?php include_title() ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Style-Type" content="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <?php include_stylesheets() ?>
            <?php include_javascripts() ?>

                

                </head>
                <body>
                
                    <div id="spinner">
                        <i class="fa clip-spinner-5 fa-spin"></i> <span id="messages_text"></span>
                    </div>
                    <script>
                        $("#spinner").show();
                        $("#messages_text").html("Loading . . .");
                    </script>
                    <div id="batchesSpinner" style="display:none;z-index: 999999;width: 100%;height: 800px;background: #000;opacity: 0.3;position: fixed;"><i class="fa clip-spinner-5 fa-spin" style="font-size: 60px;
                                                                                                                                                              color: #fff;position: absolute;margin: 0 auto;top: 30%;left: 40%;"></i><span id="batchesMessageText" style="font-size: 24px;
                                                                                                                                                              color: #fff;position: absolute;margin: 0 auto;top: 32%;left: 45%;"></span></div>
                    <div id="referringDoctorSpinner" style="display:none;z-index: 999999;width: 100%;height: 800px;background: #000;opacity: 0.3;position: fixed;"><i class="fa clip-spinner-5 fa-spin" style="font-size: 60px;
                                                                                                                                                                      color: #fff;position: absolute;margin: 0 auto;top: 30%;left: 40%;"></i><span id="referringDoctorMessageText" style="font-size: 24px;
                                                                                                                                                                      color: #fff;position: absolute;margin: 0 auto;top: 32%;left: 45%;"></span></div>

                    <?php if ($sf_user->isAuthenticated()) { ?>
                        <?php include_partial('global/header'); ?>
                        <div class="main-container">
                            <?php include_component('default', 'menu'); ?>

                            <div class="main-content">
                                <?php echo $sf_content; ?>
                            </div>
                        </div>
                        <!-- <div id="ScrollToTop" style="position:fixed;top:95%;right:5%;z-index:9999">
                            <span class=" go-top fab-action-button">
                                <i class="fab-action-button__icon"></i>
                            </span>
                        </div> -->
                        <!--<div class="main-content">-->
                        <footer id="footer">                            
                            <?php include_partial('global/' . sfConfig::get('app_footer_file')); ?>
                        </footer>
                        <!--</div>-->
                    <?php } else { ?>
                        <?php echo $sf_content; ?>
                    <?php } ?>
                    <script type="text/javascript">
                        $("#spinner").hide();
                        var cssClass = 'hcv_valid';
                        quickClaim.dateFormat = '<?php echo sfOutputEscaper::escape('esc_specialchars', $sf_user->getJSDateFormat()); ?>';

                        quickClaim.timeFormat = '<?php echo sfOutputEscaper::escape('esc_specialchars', $sf_user->getJSTimeFormat()); ?>';
                        quickClaim.dateTimeFormat = '<?php echo sfOutputEscaper::escape('esc_specialchars', $sf_user->getJSDateTimeFormat()); ?>';
                        quickClaim.useAlphaPatientNumbers = <?php echo $sf_user->getPatientUseAlphaPatientNumbers() ? 'true' : 'false'; ?>;
                        quickClaim.skipToHCN = <?php echo $sf_user->getPatientSearchSkipToHcn() ? 'true' : 'false'; ?>;
                        quickClaim.tablesorterTooltip = '<?php echo image_tag('icons/info.gif', array('class' => 'sortTip', 'align' => 'right', 'title' => 'Click columns with an up/down arrow to sort. &nbsp; Hold Shift to add additional columns to the sort order.')); ?>';
                        quickClaim.contextMenuImage = '<?php echo image_path('icons/redDown.png'); ?>';
                        quickClaim.spinnerHTML = '<?php echo image_tag('icons/spinner.gif'); ?>';

                        $(window).load(function () {

                            if ($('ul.sf-menu').size()) {
                                var min_width = parseInt($('ul.sf-menu').css('padding-left').replace("px", "")) + parseInt($('ul.sf-menu').css('padding-right').replace("px", ""));
                                $('ul.sf-menu li.first_submenu').each(function () {
                                    min_width += $(this).width();
                                });
                                $('ul.sf-menu').css('width', min_width);

                                $('ul.sf-menu').superfish({
                                    delay: 500,
                                    speed: 'fast',
                                    autoArrows: false,
                                    dropShadows: false
                                });
                                jQuery(".sf-menu > li > a").append("<i></i>");
                            }

                            $('#tabs').bind('tabsshow', function () {
                                resizeContent();
                            });
                            resizeContent();
                        });

                        $(window).resize(function () {
                            resizeContent();
                        });
                        $(window).scroll(function () {
                            quickClaim.handleScroll();
                        });

                        $('#topofpage').click(function () {
                            $('html, body').animate({scrollTop: 0}, 'slow');
                        });

                        function resizeContent() {
                            var diff = $(window).height() - $('header').outerHeight(true) - $('#footer').outerHeight(true)
                                    - ($('#content_box').outerHeight(true) - $('#content_box').height());

                            $('#content_box').css('min-height', diff + 'px');
                        }
                        ;
                    </script>


                    <script>
                        $(document).ready(function () {
                            Main.init();
                            //                $('.hasDatepick').datepicker({
                            //                    selectOtherMonths: true,
                            //                    showOtherMonths: true,
                            //                    yearRange: 'c-100:c+10',
                            //                    changeMonth: true,
                            //                    changeYear: true,
                            //                    autoclose: true,
                            //                    dateFormat: quickClaim.dateFormat.replace('yyyy','yy')//'dd/mm/yy'
                            //                });
                            $('.Select2').removeClass('form-control');
                            $(".Select2").select2({
                                placeholder: "-- select --",
                                allowClear: true,
                                selectOnClose: true,
                                closeOnSelect: true,
                                width: '100%'
                            });
                            $(".Select2DOB").select2({
                                placeholder: "-- select --",
                                allowClear: true,
                                selectOnClose: true,
                                closeOnSelect: true,
                                width: '87%'
                            });
                            $(".Select2claimType").select2({
                                placeholder: "-- select --",
                                allowClear: true,
                                closeOnSelect: true,

                            });
                            $('select[data-class="Select2"]').removeClass('form-control');
                            $('select[data-class="Select2"]').select2({
                                placeholder: "-- select --",
                                allowClear: true,
                                closeOnSelect: true,
                                selectOnClose: true,
                                width: '100%'
                            });

                            $(".Select2Cust").select2({
                                placeholder: "-- select --",
                                allowClear: true,
                                closeOnSelect: true,
                                selectOnClose: true,
                                width: $(".Select2Cust").attr('data-select2width')
                            });

                            // document.getElementById('ScrollToTop').style.display = 'none';
                            // $(window).on('scroll', function () {
                            //     if (window.scrollY < 100) {
                            //         document.getElementById('ScrollToTop').style.display = 'none';
                            //     } else {
                            //         document.getElementById('ScrollToTop').style.display = 'block';
                            //     }
                            // });

                            if ($(window).width() > 1500) {
                                $('body').addClass('layout-boxed');
                            }

                            sk.datepicker('.sk-datepicker');
                            $(".customWell button").addClass('btn-primary');
                            setTimeout(function () {
                                $('input[data-focus="true"]').focus();

                            }, 1000);
                        });
                    </script>

                    <?php if ($sf_user->isAuthenticated()) { ?>
                    
                        <script>
                            
                            $(document).ready(function () {
                                // check client color, font settings
                                $("#fontSizeChanger").easyView();
                                var dataColor = sessionStorage.getItem('color');
                                var dataFontSize = sessionStorage.getItem('fontSize');
                                
                                if (dataColor) {
                                    //localStorage.setItem('color', dataColor);

                                    $("p,span,small,h1,h2,h3,h4,h5,h6,a,ul li,div,input[type='text'],textarea,select,.sk-datatable th, .sk-datatable td, .sk-datatable td div, .sk-datatable div, .title, .table, td,th")
                                                    .not('.styletype')
//                                                    .not('.message')
                                                    .not('.alert')
                                                    .not('.alert-heading')
                                                    .not('.side_text1')
//                                                    .not('.title')
                                                    .not('.fs12')
                                                    .not('.maintainColor')
                                                    .not('#generate_cal th')
                                                    .not('.ui-state-highlight')
                                                    .not('.ui-button-text')
                                                    .not('.dt-button span i')
                                                    .not('.dt-button span')
                                                    .not('.dt-button')
                                                    .not('li.btn-primary')
                                                    .not('span.ui-button-text')
                                                    .not('.main-navigation-menu li')
                                                    .not('.main-navigation-menu a')
                                                    .not('.main-navigation-menu span')
                                                    .css('color', dataColor);
                                            $(".sk-datatable th *").css('color', dataColor);
                                            if ($("#text_color")) {
                                                $("#text_color").minicolors('value', dataColor);
                                            }
                                }
                                if(dataFontSize){
                                    //localStorage.setItem('fontSize', dataFontSize);

                                    global_js.local_fontSize = dataFontSize;
                                            if (dataFontSize > 0) {
                                                for (var i = 0; i <= dataFontSize; i++) {
                                                    $('#fontSizeChanger').easyView('increase');
                                                }
                                            } else {
                                                for (var i = 0; i >= dataFontSize; i--) {
                                                    $('#fontSizeChanger').easyView('decrease');
                                                }
                                            }
                                }

                                /*$.ajax({
                                    url: "/default/gettextsettings",
                                    type: "POST",
                                    success: function (res) {
                                        var obj = JSON.parse(res);
                                        if (obj.text_color != null && obj.text_color != '') {
                                            $("p,span,small,h1,h2,h3,h4,h5,h6,ul li,div,input[type='text'],textarea,select,.sk-datatable th, .sk-datatable td, .sk-datatable td div, .sk-datatable div, .title, .table, td,th")
                                                    .not('.styletype')
//                                                    .not('.message')
                                                    .not('.alert')
                                                    .not('.alert-heading')
                                                    .not('.side_text1')
//                                                    .not('.title')
                                                    .not('.ui-state-highlight')
                                                    .not('.ui-button-text')
                                                    .not('.dt-button span i')
                                                    .not('.dt-button span')
                                                    .not('.dt-button')
                                                    .not('li.btn-primary')
                                                    .not('span.ui-button-text')
                                                    .not('.main-navigation-menu span')
                                                    .css('color', obj.text_color);
                                            $(".sk-datatable th *").css('color', obj.text_color);
                                            if ($("#text_color")) {
                                                $("#text_color").minicolors('value', obj.text_color);
                                            }
                                        }

                                        if (obj.font_sizes) {
                                            global_js.local_fontSize = obj.font_sizes;
                                            if (obj.font_sizes > 0) {
                                                for (var i = 0; i <= obj.font_sizes; i++) {
                                                    $('#fontSizeChanger').easyView('increase');
                                                }
                                            } else {
                                                for (var i = 0; i >= obj.font_sizes; i--) {
                                                    $('#fontSizeChanger').easyView('decrease');
                                                }
                                            }
                                        }
                                    }
                                })*/
                                $('*').on('search.dt, length.dt, page.dt, init.dt, draw.dt', function () {
                                    var newFont = (+global_js.local_fontSize * +2) + +12;
                                    $(".sk-datatable td,.sk-datatable th").css('font-size', newFont + 'px');
                                    $(".actionButtonsDiv").css('font-size', newFont + 'px');
                                });
                                

                            
                            })

                            var sessionStorage_transfer = function(event) {
                                // console.log(event);
                              if(!event) { event = window.event; } // ie suq
                              if(!event.newValue) return;          // do nothing if no value to work with
                              if (event.key == 'getSessionStorage') {
                                // another tab asked for the sessionStorage -> send it
                                localStorage.setItem('sessionStorage', JSON.stringify(sessionStorage));
                                // the other tab should now have it, so we're done with it.
                                localStorage.removeItem('sessionStorage'); // <- could do short timeout as well.
                              } else if (event.key == 'sessionStorage' && !sessionStorage.length) {
                                // another tab sent data <- get it
                                var data = JSON.parse(event.newValue);
                                for (var key in data) {
                                  sessionStorage.setItem(key, data[key]);
                                }
                              }
                            };

                            // listen for changes to localStorage
                            if(window.addEventListener) {
                              window.addEventListener("storage", sessionStorage_transfer, false);
                            } else {
                              window.attachEvent("onstorage", sessionStorage_transfer);
                            };


                            // Ask other tabs for session storage (this is ONLY to trigger event)
                            if (!sessionStorage.length) {
                              localStorage.setItem('getSessionStorage', 'foobar');
                              localStorage.removeItem('getSessionStorage', 'foobar');
                            };
                        
                        </script>
                    <?php } ?>

                    <script>
                        
                        global_js.print_font_size = sk.print_font_size = sk.pdf.fontSize = <?php echo $sf_user->getClientParameter('font_options')  ?>;
                        sk.appt_book_sidemenu_position = '<?php echo $sf_user->getClientParameter('appt_book_sidemenu_position') ?>';
                        sk.skdt_page_size = <?php echo $sf_user->getClientParameter('dt_page_size') ?>;
                    </script>
                    <script type="text/javascript">
                         
                         shortcuts.add('alt+shift+h',function() {
                            // alert("anju");
                            window.location = '<?php echo url_for('default/main'); ?>';
                        });

                         shortcuts.add('alt+shift+p',function() {
                            window.location = '<?php echo url_for('patient/view'); ?>';
                        });

                         shortcuts.add('alt+shift+a',function() {
                            window.location = '<?php echo url_for('appointment_book/index'); ?>';
                        });

                        shortcuts.add('alt+shift+c',function() {
                            window.location = '<?php echo url_for('claim/index'); ?>';
                        });

                        shortcuts.add('alt+shift+d',function() {
                            window.location = '<?php echo url_for('provider/index'); ?>';
                        });

                        shortcuts.add('alt+shift+s',function() {
                            window.location = '<?php echo url_for('region/index'); ?>';
                        });

                    </script>
                </body>
                </html>
