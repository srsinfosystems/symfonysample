<div id="<?php echo $messageID; ?>" class="right">
    <span class="spinner" style="display:none"><?php echo image_tag('icons/spinner.gif'); ?></span>
    <span class="checkmark" style="display:none"><?php echo image_tag('icons/checkmark.png', array('height' => '16')); ?></span>
    <span class="error_icon" style="display:none"><?php echo image_tag('icons/error.png', array('height' => '16')); ?></span>
    <span id="<?php echo $messageID; ?>Text" class="text right"></span>
</div>
