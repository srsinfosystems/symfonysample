<?php if ($url && $id): ?>
<form action="<?php echo $url; ?>" id="<?php echo $id; ?>" method="post">
<?php endif; ?>
<table class="form-table">
	<?php echo $form; ?>
	<tr>
		<td>&nbsp;</td>
		<td><input type="submit" value="Save" id="<?php echo $button; ?>" name="<?php echo $button; ?>" class="left" />

<?php echo $form->renderBottomErrorBox(); ?>

		</td>
</table>
<?php if ($url && $id): ?>
</form>
<?php endif; ?>

<script type="text/javascript">
$(window).ready(function() {
	$('#<?php echo $button; ?>').button();
});
</script>
