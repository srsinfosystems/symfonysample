<div class="<?php echo $prefix; ?>Pager">
    <a href="javascript:void(0)" class="first">
        <span class="fa-stack">
            <i class="fa fa-circle teal fa-stack-2x"></i>
            <i class="fa fa-step-backward fa-stack-1x fa-inverse"></i>
        </span>
    </a>

    <a href="javascript:void(0)" class="prev">
        <span class="fa-stack">
            <i class="fa fa-circle teal fa-stack-2x"></i>
            <i class="fa fa-backward fa-stack-1x fa-inverse"></i>
        </span>
    </a>
    <span class="pagedisplay"></span>
        <a href="javascript:void(0)" class="next">
        <span class="fa-stack">
            <i class="fa fa-circle teal fa-stack-2x"></i>
            <i class="fa fa-forward fa-stack-1x fa-inverse"></i>
        </span>
    </a>
    <a href="javascript:void(0)" class="last">
        <span class="fa-stack">
            <i class="fa fa-circle teal fa-stack-2x"></i>
            <i class="fa fa-step-forward fa-stack-1x fa-inverse"></i>
        </span>
    </a>
    
    <select class="pagesize sk-select" style='margin-left:10px;width:100px!important;'>
        <?php foreach ($sizes as $size): ?>
            <option value="<?php echo $size; ?>"><?php echo $size; ?></option>
        <?php endforeach; ?>
    </select>

    <?php if (count($buttons)): ?>
    <?php 
        $btnIcons['PDF']['color'] = 'btn-danger';
        $btnIcons['PDF']['icon'] = 'clip-file-pdf';
        $btnIcons['CSV']['color'] = 'btn-info';
        $btnIcons['CSV']['icon'] = 'clip-file-excel';
        $btnIcons['PRINT']['color'] = 'btn-blue';
        $btnIcons['PRINT']['icon'] = 'fa-print fa';
    ?>
        <?php foreach ($buttons as $id => $value): ?>
            <?php 
                $class = isset($btnIcons[$value])?$btnIcons[$value]['color']:'btn-primary';
                $icon = isset($btnIcons[$value])?$btnIcons[$value]['icon']:'';
            ?>
    <button type="button" id="<?php echo $id; ?>" name="<?php echo $id; ?>" class="btn <?= $class ?> btn-squared" ><i class="<?= $icon ?>"></i> <?php echo $value; ?></button>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<script>
$(document).ready(function(){
    setTimeout(function(){
        $('.sk-select').attr('style','width:100px !important');
    })
})
</script>