<?php if ($url && $id): ?>
    <form action="<?php echo $url; ?>" id="<?php echo $id; ?>" method="post">
    <?php endif; ?>
    <div class='row'>
        <?php
        foreach ($form as $key => $field):
            if ($key != 'id' AND $key != '_csrf_token') {
                ?>
                <div class="col-md-6 col-sm-6">
                    <div class="form-group" style="min-height:64px">
                        <label class="control-label">
                            <?php echo $field->renderLabel() ?>  <small style="color:red;margin-left:10px"><?php echo $error; ?></small>                      
                        </label>

                        <?php
                        $error = $field->renderError();
                        $class = $field->hasError() ? 'skhas-error' : 'form-control';
                        echo $field->render(['class' => $class])
                        ?>
                       <small class="text-muted">
                                <?php
                                echo $error;
                                echo $field->renderHelp();
                                ?>
                            </small>
                    </div>                
                </div>
            <?php } endforeach; ?>
        <div class="col-md-12 col-sm-12">
            <?php echo $form->renderBottomErrorBox(); ?>
            <?php echo $form->renderHiddenFields(); ?>
            <button onclick="$('#<?php echo $button; ?>').trigger('click')" class="btn btn-primary btn-squared" type="button"><i class="clip-folder-plus"></i> Save</button>
            <input style="display:none" type="submit" value="Save" id="<?php echo $button; ?>" name="<?php echo $button; ?>" class="left " />
        </div>

    </div>

    <?php if ($url && $id): ?>
    </form>
<?php endif; ?>

<style>
    #permissions_stage .checkbox_list li{
        list-style-type: none;
        display: block !important;
    }
    .checkbox_list li{
        list-style-type: none;
        display: inline-block;
        min-width: 150px;
    }
    .radio_list li{
        list-style-type: none;
        display: inline-block;
        min-width: 150px;
    }
    ul{padding-left:0px}
</style>
<script type="text/javascript">
    $(window).ready(function () {
        $('#<?php echo $button; ?>').button();
        $('input[type=text]').addClass('form-control');
        $('textarea').addClass('form-control');
        $('input[type=text]').attr('style', 'width:100%');
//        $('input[type=submit]').addClass('btn btn-success sk5mar col-sm-3 col-xs-5');
//        $('input[type=button]').addClass('btn btn-warning sk5mar col-sm-3 col-xs-5');
    });
</script>
