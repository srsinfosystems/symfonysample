<?php if ($url && $id): ?>
    <form action="<?php echo $url; ?>" id="<?php echo $id; ?>" method="post">
    <?php endif; ?>
    <?php
    foreach ($form as $key => $field):
        if ($key != 'id' AND $key != '_csrf_token') {
            $test = $field->render();
            if (strpos($test, 'type="hidden"') === false) {
                if (strpos($test, 'type="checkbox"') !== false) {
                    ?>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group" style="margin-top:32px">                    
                            <label class="control-label">
                                <?php
                                $error = $field->renderError();
                                $class = $field->hasError() ? 'skhas-error' : '';
                                echo $field->render(['class' => $class]) . ' ';
                                echo $field->renderLabel()
                                ?>                          
                            </label>                        
                            <small class="text-muted">
                                <?php
                                echo $error;
                                echo $field->renderHelp();
                                ?>
                            </small>
                        </div>                
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group" style="min-height:85px">                    
                            <label class="control-label">
                                <?php echo $field->renderLabel() ?>                          
                            </label>
                            <?php
                            $error = $field->renderError();
                            $class = $field->hasError() ? 'skhas-error' : 'form-control';
                            echo strpos($test, 'type="radio"') !== false ? $field->render() : $field->render(['class' => $class]);
                            ?>
                            <small class="text-muted">
                                <?php
                                echo $error;
                                echo $field->renderHelp();
                                ?>
                            </small>
                        </div>                
                    </div>
                    <?php
                }
            }
        } endforeach;
    ?>  
    <div class="col-md-12 col-sm-12">
        <?php echo $form->renderBottomErrorBox(); ?>
    </div>
    <div class="col-md-12 col-sm-12">
        <hr style="margin:5px"/>
        <?php echo $form->renderHiddenFields(); ?>
        <button onclick="$('#<?php echo $button; ?>').trigger('click')" class="btn btn-primary btn-squared" type="button"><i class="clip-folder-plus"></i> Save</button>
        <button onclick="$('#<?php echo $button; ?>_clear').trigger('click')" class="btn btn-danger btn-squared" type="button"><i class="clip-file-minus"></i> Clear</button>
        <input style="display:none" type="submit" value="Save" id="<?php echo $button; ?>" name="<?php echo $button; ?>" class="left" />
        <input style="display:none" type="button" value="Clear" id="<?php echo $button; ?>_clear" />
    </div>




    <?php if ($url && $id): ?>
    </form>
<?php endif; ?>
<script type="text/javascript">
    $(window).ready(function () {
        $('#<?php echo $button; ?>').button();
        $('#<?php echo $button; ?>_clear').button();
    });
</script>

<style>.radio_list{padding-left:40px}</style>
