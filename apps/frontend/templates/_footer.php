<!-- start: FOOTER -->
 <style>
    .fab-action-button {
        position: absolute;
        bottom: 0;
        display: block;
        width: 56px;
        height: 56px;
        background-color: #0080F8 !important;
        border-radius: 50%;
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
    }

    .fab-action-button__icon {
        display: inline-block;
        width: 56px;
        height: 56px;
        background: url(data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCIgdmlld0JveD0iMCAwIDI4NC45MjkgMjg0LjkyOSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMjg0LjkyOSAyODQuOTI5OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxnPgoJPHBhdGggZD0iTTI4Mi4wODIsMTk1LjI4NUwxNDkuMDI4LDYyLjI0Yy0xLjkwMS0xLjkwMy00LjA4OC0yLjg1Ni02LjU2Mi0yLjg1NnMtNC42NjUsMC45NTMtNi41NjcsMi44NTZMMi44NTYsMTk1LjI4NSAgIEMwLjk1LDE5Ny4xOTEsMCwxOTkuMzc4LDAsMjAxLjg1M2MwLDIuNDc0LDAuOTUzLDQuNjY0LDIuODU2LDYuNTY2bDE0LjI3MiwxNC4yNzFjMS45MDMsMS45MDMsNC4wOTMsMi44NTQsNi41NjcsMi44NTQgICBjMi40NzQsMCw0LjY2NC0wLjk1MSw2LjU2Ny0yLjg1NGwxMTIuMjA0LTExMi4yMDJsMTEyLjIwOCwxMTIuMjA5YzEuOTAyLDEuOTAzLDQuMDkzLDIuODQ4LDYuNTYzLDIuODQ4ICAgYzIuNDc4LDAsNC42NjgtMC45NTEsNi41Ny0yLjg0OGwxNC4yNzQtMTQuMjc3YzEuOTAyLTEuOTAyLDIuODQ3LTQuMDkzLDIuODQ3LTYuNTY2ICAgQzI4NC45MjksMTk5LjM3OCwyODMuOTg0LDE5Ny4xODgsMjgyLjA4MiwxOTUuMjg1eiIgZmlsbD0iI0ZGRkZGRiIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=) center no-repeat;
    }


</style>

<div class="footer clearfix">
    <div class="footer-inner">
           <a target="_blank" href="http://dev.hypemedical.com"><strong>Hype Systems Inc &copy; 2006 - <?= date('Y') ?></strong></a>
           <div class="info2">OHIP Billing & Practice Management Solution</div>
    <ul>
        <li style='border-left: 1px #aaa solid'><a target="_blank" href="index.html">Home</a></li>
        <li><a target="_blank" href="http://dev.hypemedical.com/terms-of-use/">Terms of Use</a></li>
        <li><a target="_blank" href="http://dev.hypemedical.com/resources/">General Knowledge</a></li>
        <li><a target="_blank" href="http://dev.hypemedical.com/faq/">Training FAQs</a></li>
    </ul>
    </div>

        <div id="ScrollToTop" style="position:fixed;top:95%;right:5%;z-index:9999">
        <!--<span class="go-top"><i class="clip-chevron-up"></i></span>-->
        <span class=" go-top fab-action-button">
        <i class="fab-action-button__icon"></i>
        </span>
        </div>
    <div class="footer-items">
<!--        <div style="float:right;margin:5px">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>-->
        <a target="_blank" class="btn" href="https://hypemedical.com/contact-us/"><i class="fa fa-envelope-o"></i> Message Support</a>  |<a  class="btn" href="tel:18005922138"> <i class="clip-phone"></i> Toll Free Support: 1-800-592-2138  </a>|
        <a target="_blank" class="btn" href="https://hypemedical.com/hype-support-2/"><i class="fa fa-desktop"></i>  Remote Access Plugin</a>
    </div>
</div>

<input id="fontSizeChanger" type="hidden" />
<input id="selectorDT" type="hidden" />
<textarea id="copyTextareaBreadCrum" class="sk0opacity" ></textarea>                        
<button type="button" class="sk0opacity" data-clipboard-action="copy" data-clipboard-target="#copyTextareaBreadCrum" id="copyTriggerBtnBreadcrum">copy trigger</button>

<script>
  document.getElementById('ScrollToTop').style.display = 'none';
                    $(window).on('scroll', function () {
                        if (window.scrollY < 50) {
                            document.getElementById('ScrollToTop').style.display = 'none';
                        } else {
                            document.getElementById('ScrollToTop').style.display = 'block';
                        }
                    });
</script>

<!-- end: FOOTER -->

<!--<div class="left text-bot">
    <a target="_blank" href="http://dev.hypemedical.com"><strong>Hype Systems &copy; 2016</strong></a>
    <div class="info2">OHIP Billing & Practice Management Solution</div>

    <ul class="menu-bot">
        <li><a target="_blank" href="index.html">Home</a></li>
        <li><a target="_blank" href="http://dev.hypemedical.com/terms-of-use/">Terms of Use</a></li>
        <li><a target="_blank" href="http://dev.hypemedical.com/resources/">General Knowledge</a></li>
        <li><a target="_blank" href="http://dev.hypemedical.com/faq/">Training FAQs</a></li>
    </ul>

</div>-->

<!--<div class="right alignright">
    <div class="info1" style="cursor:pointer" id="topofpage">Back to top of page</div>
    <div class="info2">
<a target="_blank" href="http://www.dev.hypemedical.com/contact-us/">Email Support</a>  | Toll Free Support: 1-800-592-2138</div>

    <a target="_blank" href="http://dev.hypemedical.com/support/">Remote Access Plugin</a>
</div>-->

<!--<br clear="all" />-->
