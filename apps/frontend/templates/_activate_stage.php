<?php if ($url && $id): ?>
    <form action="<?php echo $url; ?>" id="<?php echo $id; ?>" method="post">
<?php endif; ?>
    <?php if ($active): ?>
        <p style="color:#858585;">This <?php echo $obj_name; ?> is currently active.</p>
        <button  type="button" onclick="$('#deactivate_button').trigger('click')" class="btn btn-danger btn-squared"  ><i class="clip-user-block"></i> Deactivate!</button>
        <input id="deactivate_button" type="submit" name="deactivate" value="Deactivate!" style="display: none"/>
    <?php else: ?>
        <p style="color:#858585;">You have set up everything that is required to activate this <?php echo $obj_name; ?>.</p>
        <button  type="button" onclick="$('#activate_button').trigger('click')" class="btn btn-success btn-squared"  ><i class="clip-checkmark-circle-2"></i> Activate!</button>

        <input id="activate_button" type="submit" name="activate" value="Activate!" style="display: none"/>
    <?php endif; ?>
<?php if ($url && $id): ?>
    </form>
<?php endif; ?>

<script type="text/javascript">
    $(window).ready(function () {
        $('#activate_button').button();
        $('#deactivate_button').button();
    });
</script>
