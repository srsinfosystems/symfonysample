<?php

class defaultComponents extends hypeComponents {

    public function executeMenu(hypeWebRequest $request) {
        $menu = $submenu = array();
        /**
         * @param hypeUser
         */
        $user = $this->getUser();
        $module = $request->getParameter('module');
        $action = $request->getParameter('action');

        if ($user->isAuthenticated()) {
            $menu['home'] = array(
                'selected' => $this->isInHome($module, $action),
                'url' => null,
                'text' => 'Home'
            );

            if ($this->canSeePatient($user)) {
                $menu['patient'] = array(
                    'selected' => $this->isInPatient($module, $action),
                    'url' => null,
                    'text' => 'Patients'
                );
            }

            if ($this->canSeeAppointmentBook($user)) {
                $menu['appointment_book'] = array(
                    'selected' => $this->isInAppointmentBook($module, $action),
                    'url' => null,
                    'text' => 'Appt Book'
                );
            }

            if ($this->canSeeClaims($user)) {
                $menu['claim'] = array(
                    'selected' => $this->isInClaims($module, $action),
                    'url' => null,
                    'text' => 'Claims'
                );
            }

            if ($this->canSeeDoctors($user)) {
                $menu['doctor'] = array(
                    'selected' => $this->isInDoctors($module, $action),
                    'url' => null,
                    'text' => 'Doctor Profiles'
                );
            }

            if ($this->canSeeSysAdmin($user)) {
                $menu['sys_admin'] = array(
                    'selected' => $this->isInSysAdmin($module, $action),
                    'url' => null,
                    'text' => 'SysAdmin'
                );
            }

            //if ($user->hasCredential(hype::PERMISSION_NAME_MESSAGES)) {                
//                $menu['messages'] = array(
//                    'selected' => $this->isInMessages($module, $action),
//                    'url' => 'messages/index',
//                    'text' => 'Messages'
//                );
            //}

            $menu['sign_out'] = array(
                'selected' => false,
                'url' => '@sf_guard_signout',
                'text' => 'Log out'
            );
        }

        if (array_key_exists('home', $menu)) {
            $submenu['home'][] = array(
                'selected' => ($module == 'default' && $action == 'main'),
                'url' => '@main',
                'text' => 'Home'
            );

            $submenu['home'][] = array(
                'selected' => ($module == 'user_profile' && $action == 'changePassword'),
                'url' => 'user_profile/changePassword',
                'text' => 'Change Password'
            );
        }

        if (array_key_exists('patient', $menu)) {
            $submenu['patient'][] = array(
                'selected' => ($module == 'patient'),
                'url' => 'patient/index',
                'text' => 'Patient Profile'
            );

            if ($user->hasCredential(hype::PERMISSION_NAME_PATIENT_MERGE)) {
                $submenu['patient'][] = array(
                    'selected' => ($module == 'patientMerge'),
                    'url' => 'patientMerge/index',
                    'text' => 'Merge Patient Profiles',
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_IMPORT_DAY_SHEET)) {
                $submenu['patient'][] = array(
                    'selected' => ($module == 'importDaySheet'),
                    'url' => 'importDaySheet/index',
                    'text' => 'Import Day Sheet'
                );
            }
        }

        if (array_key_exists('appointment_book', $menu)) {
            if ($user->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK)) {
                $submenu['appointment_book'][] = array(
                    'selected' => ($module == 'appointment_book' && $action == 'index'),
                    'url' => 'appointment_book/index',
                    'text' => 'Appt Book'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK)) {
                $submenu['appointment_book'][] = array(
                    'selected' => ($module == 'appointment_search'),
                    'url' => 'appointment_search/index',
                    'text' => 'Appt Search'
                );
            }

//            if ($user->hasCredential(hype::PERMISSION_NAME_REPEATING_APPOINTMENT, hype::PERMISSION_NAME_APPOINTMENT_BOOK)) {
//                $submenu['appointment_book'][] = array(
//                    'selected' => ($module == 'repeating_event'),
//                    'url' => 'repeating_event/index',
//                    'text' => 'Repeating Appointments'
//                );
//            }

            if ($user->hasCredential(hype::PERMISSION_NAME_CONSULTATION_ADMIN, hype::PERMISSION_NAME_APPOINTMENT_BOOK)) {
                $submenu['appointment_book'][] = array(
                    'selected' => $module == 'consult_admin',
                    'url' => 'consult_admin/index',
                    'text' => 'Consult Admin'
                );

                $submenu['appointment_book'][] = array(
                    'selected' => $module == 'consultation',
                    'url' => 'consultation/index',
                    'text' => 'Consult Templates'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_STAGE_TRACK, hype::PERMISSION_NAME_APPOINTMENT_BOOK)) {
                $submenu['appointment_book'][] = array(
                    'selected' => ($module == 'appointment_stage_track'),
                    'url' => 'appointment_stage_track/index',
                    'text' => 'Stage Tracking'
                );
            }

            if ($user->getClientHasModule(hype::MODULE_APPT_REVIEW)) {
                $submenu['appointment_book'][] = array(
                    'selected' => ($module == 'appointment_review'),
                    'url' => 'appointment_review/index',
                    'text' => 'Appointment Review',
                );
            }
        }

        if (array_key_exists('claim', $menu)) {
            if ($user->hasCredential(hype::PERMISSION_NAME_CLAIM_CREATE)) {
                $submenu['claim'][] = array(
                    'selected' => ($module == 'claim' && $action == 'index'),
                    'url' => 'claim/index',
                    'text' => 'Create Claim'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_PAST_CLAIM_VIEW)) {
                $submenu['claim'][] = array(
                    'selected' => ($module == 'past_claim' && $action == 'index'),
                    'url' => 'past_claim/index',
                    'text' => 'Billing Cycle'
                );
            }

            if ($user->hasCredential(array(array(hype::PERMISSION_NAME_CLAIM_SUBMIT, hype::PERMISSION_NAME_BATCH_REVERSE, hype::PERMISSION_NAME_CLAIM_RECONCILE)))) {
                $submenu['claim'][] = array(
                    'selected' => ($module == 'ohip' && $action == 'index'),
                    'url' => 'ohip/index',
                    'text' => 'MCEDT Files'
                );
            }

            //Analytic menu

            if ($user->hasCredential(hype::PERMISSION_NAME_PAST_CLAIM_VIEW)) {
                $submenu['claim'][] = array(
                    'selected' => ($module == 'analytics' && $action == 'index'),
                    'url' => 'analytics/index',
                    'text' => 'Analytics'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_PAST_CLAIM_VIEW)) {
                $submenu['claim'][] = array(
                    'selected' => ($module == 'totalsReporting' && $action == 'index'),
                    'url' => 'totalsReporting/index',
                    'text' => 'Monthly Totals'
                );
            }



            if ($user->hasCredential(array(array(hype::PERMISSION_NAME_CLAIM_SUBMIT, hype::PERMISSION_NAME_CLAIM_CREATE, hype::PERMISSION_NAME_THIRD_PARTY_EDIT)))) {
                $submenu['claim'][] = array(
                    'selected' => ($module == 'invoices'),
                    'url' => 'invoices/index',
                    'text' => 'Third Party Invoices',
                );
            }

//            if ($user->hasCredential(hype::PERMISSION_NAME_EXECUTIVE_REPORTS)) {
//                $submenu['claim'][] = array(
//                    'selected' => ($module == 'reporting'),
//                    'url' => 'reporting/index',
//                    'text' => 'Reporting'
//                );
//
//                $submenu['claim'][] = array(
//                    'selected' => ($module == 'mcedt'),
//                    'url' => 'mcedt/index',
//                    'text' => 'MCEDT'
//                );
//            }
        }

        if (array_key_exists('doctor', $menu)) {
            if ($user->hasCredential(hype::PERMISSION_NAME_PROVIDER_EDIT)) {
                $submenu['doctor'][] = array(
                    'selected' => ($module == 'provider'),
                    'url' => 'provider/index',
                    'text' => 'Setup'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_MANAGE_LOCATIONS)) {
                $submenu['doctor'][] = array(
                    'selected' => ($module == 'location'),
                    'url' => 'location/index',
                    'text' => $this->getUser()->getLocationTitle(true)
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_DOCTOR_TEMPLATE)) {
                $submenu['doctor'][] = array(
                    'selected' => ($module == 'general_schedule'),
                    'url' => 'general_schedule/index',
                    'text' => 'Work Schedules'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_DOCTOR_SCHEDULE)) {
                $submenu['doctor'][] = array(
                    'selected' => ($module == 'build_schedule'),
                    'url' => 'build_schedule/index',
                    'text' => $user->getLocationTitle() . ' Schedule'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_VACATION_ADMIN)) {
                $submenu['doctor'][] = array(
                    'selected' => ($module == 'vacation'),
                    'url' => 'vacation/index',
                    'text' => 'Vacations',
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_REFERRING_DOCTOR_EDIT)) {
                $submenu['doctor'][] = array(
                    'selected' => ($module == 'referring_doctor'),
                    'url' => 'referring_doctor/index',
                    'text' => 'Referring Doctors'
                );
            }
        }

        if (array_key_exists('sys_admin', $menu)) {
            if ($user->hasCredential(hype::PERMISSION_NAME_MANAGE_LOCATIONS)) {
                $submenu['sys_admin'][] = array(
                    'selected' => ($module == 'region'),
                    'url' => 'region/index',
                    'text' => 'Regions'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_SETUP_CLIENT)) {
                $submenu['sys_admin'][] = array(
                    'selected' => ($module == 'client_configuration'),
                    'url' => 'client_configuration/index',
                    'text' => 'Clients'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_HYPE_ADMINISTRATOR) && $user->getClientHasModule(hype::MODULE_DASHBOARD_WORKFLOW)) {
                $submenu['sys_admin'][] = array(
                    'selected' => $module == 'client_workflows',
                    'url' => 'client_workflows/index',
                    'text' => 'Client Workflows',
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_SETUP_CLIENT) && ($user->getClientHasModule(hype::MODULE_MOBILE_APP) || $user->getClientHasModule(hype::MODULE_HL7))) {
                $submenu['sys_admin'][] = array(
                    'selected' => $module == 'mobileAppSetup',
                    'url' => 'mobileAppSetup/index',
                    'text' => 'HL7/Mobile Tools',
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_USER_PROFILE_EDIT)) {
                $submenu['sys_admin'][] = array(
                    'selected' => ($module == 'user_profile' && $action != 'changePassword'),
                    'url' => 'user_profile/index',
                    'text' => 'Users'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_EXPORT_DATA)) {
                $submenu['sys_admin'][] = array(
                    'selected' => ($module == 'export'),
                    'url' => 'export/index',
                    'text' => 'Export Data',
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_HYPE_ADMINISTRATOR) || $user->hasCredential(hype::PERMISSION_NAME_REFERRING_DOCTOR_EDIT)) {
                $submenu['sys_admin'][] = array(
                    'selected' => ($module == 'import'),
                    'url' => 'import/index',
                    'text' => 'Import Data',
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_TYPE) || $user->hasCredential(hype::PERMISSION_NAME_CONSULTATION_ADMIN) || $user->hasCredential(hype::PERMISSION_NAME_HOLIDAY_ADMIN)) {
                $submenu['sys_admin'][] = array(
                    'selected' => $module == 'setup_consultation',
                    'url' => 'setup_consultation/index',
                    'text' => 'Appt Book'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_CLIENT_PARAM_EDIT) || $user->hasCredential(hype::PERMISSION_NAME_PATIENT_STATUS)) {
                $submenu['sys_admin'][] = array(
                    'selected' => ($module == 'setup_patient'),
                    'url' => 'setup_patient/index',
                    'text' => 'Patient'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_GONET_EDIT) || $user->hasCredential(hype::PERMISSION_NAME_MODEM_EDIT) || $user->hasCredential(hype::PERMISSION_NAME_QUICK_SERVICE_CODE_EDIT) || $user->hasCredential(hype::PERMISSION_NAME_CLIENT_PARAM_EDIT)) {
                $submenu['sys_admin'][] = array(
                    'selected' => ($module == 'setup_claim'),
                    'url' => 'setup_claim/index',
                    'text' => 'Claim'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_REMINDER_SETUP) && $user->getClientHasModule(hype::MODULE_EMAIL_APPOINTMENT_REMINDERS)) {
                $submenu['sys_admin'][] = array(
                    'selected' => ($module == 'reminders'),
                    'url' => 'reminders/index',
                    'text' => 'Reminders & Reports'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_HYPE_ADMINISTRATOR)) {
                $submenu['sys_admin'][] = array(
                    'selected' => ($module == 'setup_ohip'),
                    'url' => 'setup_ohip/index',
                    'text' => 'OHIP Setup'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_THIRD_PARTY_EDIT) || $user->hasCredential(hype::PERMISSION_NAME_USER_TYPE_PERMISSION_EDIT) || $user->hasCredential(hype::PERMISSION_NAME_CLIENT_PARAM_EDIT)) {
                $submenu['sys_admin'][] = array(
                    'selected' => ($module == 'setup_system'),
                    'url' => 'setup_system/index',
                    'text' => 'System'
                );
            }

            if ($user->hasCredential(hype::PERMISSION_NAME_HYPE_PARAMS)) {
                $submenu['sys_admin'][] = array(
                    'selected' => ($module == 'system_params'),
                    'url' => 'system_params/index',
                    'text' => 'System-Wide Parameters'
                );
            }
        }



        foreach ($menu as $key => $val) {
            if (is_null($menu[$key]['url']) && array_key_exists($key, $submenu) && array_key_exists(0, $submenu[$key])) {
                $menu[$key]['url'] = $submenu[$key][0]['url'];
            } else {
                if ($val['url'] == null) {
                    unset($menu[$key]);
                }
            }
        }

        $this->setVar('menu', $menu);
        $this->setVar('submenu', $submenu);
    }

    private function isInMessages($module, $action) {
        if ($module == 'messages' && $action == 'index') {
            return true;
        }

        if ($module == 'user_profile' && $action == 'changePassword') {
            return true;
        }
        return false;
    }

    private function isInHome($module, $action) {
        if ($module == 'default' && $action == 'main') {
            return true;
        }

        if ($module == 'user_profile' && $action == 'changePassword') {
            return true;
        }
        return false;
    }

    private function canSeePatient(hypeUser $user) {
        return $user->hasCredential(hype::PERMISSION_NAME_VIEW_PATIENT_PROFILE);
    }

    private function isInPatient($module) {
        if ($module == 'patient') {
            return true;
        }
        if ($module == 'importDaySheet') {
            return true;
        }
        if ($module == 'patientMerge') {
            return true;
        }



        return false;
    }

    private function canSeeAppointmentBook(hypeUser $user) {
        return
                $user->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK) ||
                $user->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_REVIEW);
    }

    private function isInAppointmentBook($module) {
        if ($module == 'appointment_book') {
            return true;
        }
        if ($module == 'appointment_search') {
            return true;
        }
        if ($module == 'consultation') {
            return true;
        }
        if ($module == 'consult_admin') {
            return true;
        }
        if ($module == 'appointment_stage_track') {
            return true;
        }
        if ($module == 'repeating_event') {
            return true;
        }
        if ($module == 'appointment_review') {
            return true;
        }

        return false;
    }

    private function canSeeClaims(hypeUser $user) {
        return
                $user->hasCredential(hype::PERMISSION_NAME_CLAIM_CREATE) ||
                $user->hasCredential(hype::PERMISSION_NAME_PAST_CLAIM_VIEW) ||
                $user->hasCredential(hype::PERMISSION_NAME_CLAIM_SUBMIT) ||
                $user->hasCredential(hype::PERMISSION_NAME_BATCH_REVERSE) ||
                $user->hasCredential(hype::PERMISSION_NAME_CLAIM_RECONCILE);
    }

    private function isInClaims($module) {
        if ($module == 'claim') {
            return true;
        }
        if ($module == 'past_claim') {
            return true;
        }
        if ($module == 'totalsReporting') {
            return true;
        }
        if ($module == 'ohip') {
            return true;
        }
        if ($module == 'invoices') {
            return true;
        }
        if ($module == 'reporting') {
            return true;
        }
        if ($module == 'mcedt') {
            return true;
        }

        return false;
    }

    private function canSeeDoctors(hypeUser $user) {
        return
                $user->hasCredential(hype::PERMISSION_NAME_PROVIDER_EDIT) ||
                $user->hasCredential(hype::PERMISSION_NAME_DOCTOR_SCHEDULE) ||
                $user->hasCredential(hype::PERMISSION_NAME_DOCTOR_TEMPLATE) ||
                $user->hasCredential(hype::PERMISSION_NAME_VACATION_ADMIN) ||
                $user->hasCredential(hype::PERMISSION_NAME_REFERRING_DOCTOR_EDIT);
    }

    private function isInDoctors($module) {
        if ($module == 'provider') {
            return true;
        }
        if ($module == 'location') {
            return true;
        }
        if ($module == 'general_schedule') {
            return true;
        }
        if ($module == 'build_schedule') {
            return true;
        }
        if ($module == 'vacation') {
            return true;
        }
        if ($module == 'referring_doctor') {
            return true;
        }

        return false;
    }

    private function canSeeSysAdmin(hypeUser $user) {
        return
                $user->hasCredential(hype::PERMISSION_NAME_PATIENT_STATUS) ||
                $user->hasCredential(hype::PERMISSION_NAME_HOLIDAY_ADMIN) ||
                $user->hasCredential(hype::PERMISSION_NAME_CLIENT_PARAM_EDIT) ||
                $user->hasCredential(hype::PERMISSION_NAME_QUICK_SERVICE_CODE_EDIT) ||
                $user->hasCredential(hype::PERMISSION_NAME_MODEM_EDIT) ||
                $user->hasCredential(hype::PERMISSION_NAME_GONET_EDIT) ||
                $user->hasCredential(hype::PERMISSION_NAME_SETUP_CLIENT) ||
                $user->hasCredential(hype::PERMISSION_NAME_USER_PROFILE_EDIT) ||
                $user->hasCredential(hype::PERMISSION_NAME_THIRD_PARTY_EDIT) ||
                $user->hasCredential(hype::PERMISSION_NAME_USER_TYPE_PERMISSION_EDIT) ||
                $user->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_TYPE) ||
                $user->hasCredential(hype::PERMISSION_NAME_CONSULTATION_ADMIN) ||
                $user->hasCredential(hype::PERMISSION_NAME_MANAGE_LOCATIONS) ||
                $user->hasCredential(hype::PERMISSION_NAME_HYPE_PARAMS) ||
                $user->hasCredential(hype::PERMISSION_NAME_EXPORT_DATA) ||
                $user->hasCredential(hype::PERMISSION_NAME_REFERRING_DOCTOR_EDIT) ||
                $user->hasCredential(hype::PERMISSION_NAME_HYPE_ADMINISTRATOR);
    }

    private function isInSysAdmin($module, $action) {
        if ($module == 'setup_patient') {
            return true;
        }
        if ($module == 'holiday') {
            return true;
        }

        if ($module == 'region') {
            return true;
        }
        if ($module == 'setup_claim') {
            return true;
        }
        if ($module == 'client_configuration') {
            return true;
        }
        if ($module == 'mobileAppSetup') {
            return true;
        }
        if ($module == 'user_profile' && $action != 'changePassword') {
            return true;
        }
        if ($module == 'setup_system') {
            return true;
        }
        if ($module == 'setup_consultation') {
            return true;
        }
        if ($module == 'import') {
            return true;
        }
        if ($module == 'export') {
            return true;
        }
        if ($module == 'client_workflows') {
            return true;
        }
        if ($module == 'reminders') {
            return true;
        }
        if ($module == 'setup_ohip') {
            return true;
        }
        if ($module == 'system_params') {
            return true;
        }

        return false;
    }

}
