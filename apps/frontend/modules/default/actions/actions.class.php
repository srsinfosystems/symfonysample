<?php

class defaultActions extends hypeActions {

    public function executeSavecontentsetting(hypeWebRequest $request){
//        $oldColor = $this->getUser()->getClientHasModule('color');
        $color = $request->getParameter('text_color');
        $font_size = $request->getParameter('font_size');
        $this->getUser()->setReportDefaults('text_color', $color);
        $this->getUser()->setReportDefaults('font_sizes', $font_size);
        return $this->renderText(json_encode($font_size));
    }
    
    public function executeGettextsettings(hypeWebRequest $request){
        $data['text_color'] = $this->getUser()->getReportDefaults('text_color');
        $data['font_sizes'] = $this->getUser()->getReportDefaults('font_sizes');
        return $this->renderText(json_encode($data));
    }
    
    public function executeChangeClient(hypeWebRequest $request) {
        $this->checkCredentials('HypeAdministrator');
        $client = Doctrine_Core::getTable('Client')
                ->createQuery('c')
                ->addWhere('c.id = (?)', array($request->getParameter('client_id')))
                ->fetchOne();

        if ($client instanceof Client) {
            $this->getUser()->switchClientID($client->get('id'));
        }
        unset($_SESSION['claimlock']);
        $this->redirect('@homepage');
    }

    public function executeChangeLocation(hypeWebRequest $request) {
      
        if ($request->isMethod('post')) {
            
            $id = $request->getParameter('location_id');

            $clientId = $_SESSION['symfony/user/sfUser/attributes']['UserParameters']['client_id'];
            setcookie('default_locations_cookie['.$clientId.']', $id, time() + 432000, "/"); // 86400 = 1 day 


            $query = Doctrine_Query::create()
                    ->from('DoctorLocation dl')
                    ->addWhere('dl.location_id = (?)', $id);

            if (!$this->getUser()->isAllDoctors()) {
                $query->whereIn('dl.doctor_id', array_keys($this->getUser()->getDoctors()));
            }
            $location_doctors = $query->execute();

            $doctors_by_location = array();
            foreach ($location_doctors as $ld) {
                $doctors_by_location[] = $ld->location_id . '_' . $ld->doctor_id;
            }

            $this->getUser()->setAttribute('current_location', null, 'UserParameter');
            $this->getUser()->setAttribute('default_doctors_by_location', $doctors_by_location, 'AppointmentBook');
            $this->getUser()->setAttribute('default_locations', array($id), 'AppointmentBook');
            $this->getUser()->setDefaultLocation($id);
        }
        $this->redirect('default/main');
    }

    public function executeSignup(hypeWebRequest $request) {
        
    }

    public function executeIndex(hypeWebRequest $request) {


        $user = $this->getUser();
        if ($user->isAuthenticated()) {
            $this->redirect('@main');
            return null;
        }
 
        $this->form = new SigninForm();


        $last_message = '';

        if ($last_message) {
            if ($last_message instanceof Notes) {
                $this->last_message = $last_message->getNote();
            }
        }

        if ($request->isMethod('post')) {
            if (in_array('signup', $request->getRequestParameters())) {

                $rs = array();
                //If they have a username and password
                if ($request->getParameter('username') && $request->getParameter('password')) {

                    $this->clients = Doctrine_Query::create()
                            ->from('sfGuardUser c')
                            ->where('c.username = (?)', $request->getParameter('username'))
                            ->count();

                    if ($this->clients > 0) {
                        $rs['err'] = 'Username already taken';
                        $this->redirect('@signup?' . 'e=Username already taken');
                        //return $this->renderText('Username already taken!');
                    }

                    $demoClient = Doctrine_Query::create()
                            ->from('Client c')
                            ->where('c.id = (?)', 196)
                            ->fetchOne();

                    $demoClientPermissions = Doctrine_Query::create()
                            ->from('ClientPermissionNew cp')
                            ->where('cp.client_id = (?)', 196)
                            ->execute();

                    $demoClientParams = Doctrine_Query::create()
                            ->from('ClientParam cp')
                            ->where('client_id = (?)', 196)
                            ->execute();

                    $demoClientModules = Doctrine_Query::create()
                            ->from('ClientModule cm')
                            ->where('cm.client_id = (?)', 196)
                            ->execute();

                    $demoClientLocation = Doctrine_Query::create()
                            ->from('Location l')
                            ->where('l.client_id = (?)', 196)
                            ->fetchOne();

                    $demoClientRegion = Doctrine_Query::create()
                            ->from('Region r')
                            ->where('r.client_id = (?)', 196)
                            ->fetchOne();

                    $demoClientPatientType = Doctrine_Query::create()
                            ->from('PatientType pt')
                            ->where('pt.client_id = (?)', 196)
                            ->fetchOne();

                    $demoClientPatientStatus = Doctrine_Query::create()
                            ->from('PatientStatus ps')
                            ->where('ps.client_id = (?)', 196)
                            ->execute();

                    $demoClientStages = Doctrine_Query::create()
                            ->from('StageClient sc')
                            ->where('sc.client_id = ?', 196)
                            ->execute();

//                    $demoClientUserTypes = Doctrine_Query::create()
//                                            ->from('UserType ut')
//                                            ->addWhere('ut.client_id = (?)', 196)
//                                            ->execute();
//
                    //Make new client, already got user profile
                    if ($demoClient instanceof Client) {
                        $this->client = $demoClient->copy(true);

                        $this->client->active = true;
                        $this->client->setCompanyName($request->getParameter('company'));
                        $this->client->setName($request->getParameter('fname') . ' ' . $request->getParameter('lname'));
                        $this->client->save();

                        //Permissions
                        foreach ($demoClientPermissions as $permission) {
                            if ($permission instanceof ClientPermissionNew) {
                                $demoPermission = $permission->copy();
                                $demoPermission->setClientId($this->client->get('id'));

                                $this->client->ClientPermissionNew->add($demoPermission);
                            }
                        }

                        //Params
                        foreach ($demoClientParams as $param) {
                            if ($param instanceof ClientParam) {
                                $demoClientParam = $param->copy();
                                $demoClientParam->setClientId($this->client->get('id'));

                                $this->client->ClientParam->add($demoClientParam);
                            }
                        }

                        //Modules
                        foreach ($demoClientModules as $module) {
                            if ($module instanceof ClientModule) {
                                $demoClientModule = $module->copy();
                                $demoClientModule->setClientId($this->client->get('id'));

                                $this->client->ClientModule->add($demoClientModule);
                            }
                        }

                        //Stage
                        foreach ($demoClientStages as $stage) {
                            if ($stage instanceof StageClient) {
                                $newStage = $stage->copy();
                                $newStage->setClientId($this->client->get('id'));

                                $this->client->StageClient->add($newStage);
                            }
                        }

                        //Region
                        if ($demoClientRegion instanceof Region) {
                            $newClientRegion = $demoClientRegion->copy();
                            $newClientRegion->setClientId($this->client->get('id'));
                            $this->client->Region->add($newClientRegion);

                            $this->client->save();

                            //Location
                            if ($demoClientLocation instanceof Location) {
                                $newClientLocation = $demoClientLocation->copy();
                                $newClientLocation->setClientId($this->client->get('id'));
                                $newClientLocation->setRegionId($newClientRegion->get('id'));

                                $this->client->Location->add($newClientLocation);
                            }
                        }

//                        //User Type
//                        foreach ($demoClientUserTypes as $userType) {
//                            if ($userType instanceof UserType) {
//                                $newClientUserType = $userType->copy();
//                                $newClientUserType->setClientId($this->client->get('id'));
//                                $this->client->UserType->add($newClientUserType);
//                            }
//                        }
                        //Patient Type
                        $newClientPatientType = $demoClientPatientType->copy();
                        $newClientPatientType->setClientId($this->client->get('id'));
                        $this->client->PatientType->add($newClientPatientType);

                        //Patient Status
                        foreach ($demoClientPatientStatus as $status) {
                            $newClientPatientStatus = $status->copy();
                            $newClientPatientStatus->setClientId($this->client->get('id'));
                            $this->client->PatientStatus->add($newClientPatientStatus);
                        }

                        if ($this->client instanceof Client) {
                            $this->client->setContactEmail($request->getParameter('email'));
                            $this->client->setPhone($request->getParameter('phone'));
                            $this->client->setCompanyName($request->getParameter('company'));
                            $this->client->setAddress1($request->getParameter('address') . ' ' . $request->getParameter('postal') . ' ' . $request->getParameter('city'));
                            $this->client->setContact($request->getParameter('fname') . ' ' . $request->getParameter('lname'));
                        }
                        $this->client->save();
                    }

                    $demoUser = Doctrine_Query::create()
                            ->from('UserProfile up')
                            ->leftJoin('up.SfGuardUser sfg')
                            ->where('up.id = (?)', 279)
                            ->fetchOne();

                    $demoUserPermissions = Doctrine_Query::create()
                            ->from('UserPermissionNew upn')
                            ->where('upn.user_id = (?)', 279)
                            ->execute();

                    if ($demoUser instanceof UserProfile) {
                        $newUser = $demoUser->copy(true);
                        if ($newUser instanceof UserProfile) {
                            $newUser->setClientId($this->client->get('id'));
                            $newUser->save();

                            $newUser->SfGuardUser = new sfGuardUser();
                            $newUser->SfGuardUser->setUsername($request->getParameter('username'));
                            $newUser->SfGuardUser->setPassword($request->getParameter('password'));
                            $newUser->SfGuardUser->setIsActive(true);
                            $newUser->setSfgUserId($newUser->SfGuardUser->get('id'));

                            foreach ($demoUserPermissions as $userPermission) {
                                if ($userPermission instanceof UserPermissionNew) {

                                    $demoUserPermission = $userPermission->copy();
                                    $demoUserPermission->setUserId($newUser->get('id'));

                                    $newUser->UserPermissionNew->add($demoUserPermission);
                                }
                            }

                            $newUser->save();
                            $params = array();
                            $params['username'] = $request->getParameter('username');
                            $params['password'] = $request->getParameter('password');

                            $this->form->bind($params);
                            if ($this->form->isValid()) {
                                $values = $this->form->getValues();

                                if ($values['user']->UserProfile->active && $values['user']->UserProfile->Client->canLogIn()) {
                                    $email = new hypeEmail();
                                    $email->writeNotificationEmail('charlene@hypesystems.com', 196, 'New client!');

                                    $this->getUser()->signin($values['user'], array_key_exists('remember', $values) ? $values['remember'] : false);
                                    $this->redirect('@main');
                                    return null;
                                }
                            }
                        }
                    }
                }
            } 
            else {

                // print_r($request->getParameter('signin'));
                // exit;
                $this->form->bind($request->getParameter('signin'));
                if ($this->form->isValid()) {
                    $values = $this->form->getValues();
                    //print_r($values);
                    // echo 'test';
                    // exit;
                    if ($values['user']->UserProfile->active && $values['user']->UserProfile->Client->canLogIn()) {
                        $this->getUser()->signin($values['user'], array_key_exists('remember', $values) ? $values['remember'] : false);
                        $this->redirect('@main');
                        return null;
                    }
                }
            }
        } else if ($request->isXmlHttpRequest()) {
            $this->getResponse()->setHeaderOnly(true);
            $this->getResponse()->setStatusCode(401);
            return sfView::NONE;
        } else {
            $user->setReferer($this->getContext()->getActionStack()->getSize() > 1 ? $request->getUri() : $request->getReferer());
            $this->getResponse()->setStatusCode(401);
        }
    }

    public function executePullTemporaryFormDataFiltered(hypeWebRequest $request) {
        $user_id = $request->getParameter('user');
        $rs = array();

        if ($user_id == 0 && $this->getUser()->hasCredential('System Administrator')) {
            $this->temporary_form_count_filtered = $this->getTemporaryFormClaimQuery()->count();
            if ($this->temporary_form_count_filtered) {
                $this->temporary_form_items_filtered = $this->getTemporaryFormClaimQuery()->limit(10)->execute();

                foreach ($this->temporary_form_items_filtered as $temp) {
                    $rs[] = $temp->getValuesForClaimForm();
                }
            }
        } else if ($user_id != 'undefined') {
            $this->temporary_form_count_filtered = $this->getTemporaryFormClaimQueryFiltered($user_id)->count();
            if ($this->temporary_form_count_filtered) {
                $this->temporary_form_items_filtered = $this->getTemporaryFormClaimQueryFiltered($user_id)->limit(10)->execute();

                foreach ($this->temporary_form_items_filtered as $temp) {
                    $rs[] = $temp->getValuesForClaimForm();
                }
            }
        } else {
            $this->temporary_form_count_filtered = $this->getTemporaryFormClaimQueryFiltered($this->getUser()->getUserID())->count();
            if ($this->temporary_form_count_filtered) {
                $this->temporary_form_items_filtered = $this->getTemporaryFormClaimQueryFiltered($this->getUser()->getUserID())->limit(10)->execute();

                foreach ($this->temporary_form_items_filtered as $temp) {
                    $rs[] = $temp->getValuesForClaimForm();
                }
            }
        }
        return $this->renderText(json_encode($rs));
    }

    public function executeMain(hypeWebRequest $request) { 

        $this->currentLocationID = $this->getUser()->getCurrentLocID();
        $locationIDs = $this->currentLocationID ? array($this->currentLocationID) : array_keys($this->getUser()->getLocations());

        $this->locationsData = Doctrine_Query::create()
                ->from('Location l')
                ->addWhere('l.client_id = ?', $this->getUser()->getClientID())
                ->addWhere('l.active = ?', array(true))
                ->execute();

        $this->showClientSwitcher = false;
        $this->showLocationSwitcher = (count($this->getUser()->getLocations()) > 1) ? true : false;
        $this->showTemporaryFormItems = false;
        $this->showHl7InboundReport = false;
        $this->showErrorReportWidget = false;
        $this->showAuditTrailReportWidget = false;
        $this->quickReports = array();
        $this->clients = null;
        
        $users_query = Doctrine_Query::create()
                ->from('UserProfile up')
                ->leftJoin('up.SfGuardUser sfg')
                ->leftJoin('up.UserLocation ul')
                ->leftJoin('up.UserType ut')
                ->addWhere('up.client_id = (?)', $this->getUser()->getClientID()); 

        $this->userCreatedDoctor = Doctrine_Query::create()
                ->from('Doctor d')
                ->addWhere('d.client_id = (?)', $this->getUser()->getClientID())
                ->count();

        if (!$this->show_archived) {
            $users_query->addWhere('up.archived = (?)', false);
        }

        $this->users = $users_query->execute();

        // client switcher widget
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_HYPE_ADMINISTRATOR) && $this->getUser()->hasCredential(hype::PERMISSION_NAME_SETUP_CLIENT)) {
            $this->clients = ClientTable::getDropdown('id', '__toString', false, array('active' => true));
            $this->showClientSwitcher = true;
        }

        // Error Report Widget
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_HYPE_ADMINISTRATOR)) {
            $this->errorReportWidgetCount = Doctrine::getTable('ErrorReportItem')->getUnparsableCount();

            if ($this->errorReportWidgetCount) {
                $this->showErrorReportWidget = true;
            }
        } else if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_FIX_ERROR_REPORT_PARSE_ERRORS)) {
            $this->errorReportWidgetCount = Doctrine::getTable('ErrorReportItem')->getUnparsableCount($this->getUser()->getClientID());

            if ($this->errorReportWidgetCount) {
                $this->showErrorReportWidget = true;
            }
        }

        $this->can_print_no_of_copy_dialog_box = ClientParamTable::getParamOrDefaultValueForClient($this->getUser()->getClientID(), 'dialog_box_printed_number_of_copy');



        // Audit Trail Health Checks
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_HYPE_ADMINISTRATOR)) {
            $this->showAuditTrailReportWidget = true;
            $this->auditTrailHealthCounts = array();
            $this->auditTrailHealthCounts['unprocessedClaims'] = Doctrine_Query::create()
                    ->from('ClaimVersion cv')
                    ->addWhere('cv.processed = (?)', ClaimVersionTable::PROCESSED_INCOMPLETE)
                    ->count();

            $this->auditTrailHealthCounts['unprocessedClaimItems'] = Doctrine_Query::create()
                    ->from('ClaimItemVersion civ')
                    ->addWhere('civ.processed = (?)', ClaimItemVersionTable::PROCESSED_INCOMPLETE)
                    ->count();

            $this->auditTrailHealthCounts['errorClaims'] = Doctrine_Query::create()
                    ->from('ClaimVersion cv')
                    ->addWhere('cv.processed = (?)', ClaimVersionTable::PROCESSED_ERRORS)
                    ->count();

            $this->auditTrailHealthCounts['errorClaimItems'] = Doctrine_Query::create()
                    ->from('ClaimItemVersion civ')
                    ->addWhere('civ.processed = (?)', ClaimItemVersionTable::PROCESSED_ERRORS)
                    ->count();
        }

        // orphaned appointments
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK) && $this->getUser()->getDashboardShowOrphansWidget()) {
            $this->quickReports['orphan_refine_form'] = new OrphanedAppointmentRefineForm(array(), array(
                'client_id' => $this->getUser()->getClientID(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'current_location_id' => $this->getUser()->getCurrentLocID(),
                'doctor_title' => $this->getUser()->getDoctorTitleFormat(),
                'doctors' => $this->getUser()->getDoctors(),
                'all_doctors' => $this->getUser()->isAllDoctors(),
                'date_format' => $this->getUser()->getDateFormat(),
                'screen_date_format' => $this->getUser()->getJsDateFormat(),
            ));
            $this->quickReports['orphan_count'] = $this->quickReports['orphan_refine_form']->getCount();
        }

        // follow-up appointments
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK) && $this->getUser()->getDashboardShowFollowUpWidget()) {
            $this->quickReports['follow_up_appointment_refine_form'] = new FollowUpAppointmentRefineForm(array(), array(
                'client_id' => $this->getUser()->getClientID(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'current_location_id' => $this->getUser()->getCurrentLocID(),
                'doctor_title' => $this->getUser()->getDoctorTitleFormat(),
                'doctors' => $this->getUser()->getDoctors(),
                'all_doctors' => $this->getUser()->isAllDoctors(),
                'date_format' => $this->getUser()->getDateFormat(),
                'screen_date_format' => $this->getUser()->getJsDateFormat(),
            ));
            $this->quickReports['follow_up_appointment_count'] = $this->quickReports['follow_up_appointment_refine_form']->getCount();
        }

        // attended appointments
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK) && $this->getUser()->getDashboardShowFollowUpWidget()) {
            $this->quickReports['attended_appointment_refine_form'] = new AttendedAppointmentRefineForm(array(), array(
                'client_id' => $this->getUser()->getClientID(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'current_location_id' => $this->getUser()->getCurrentLocID(),
                'doctor_title' => $this->getUser()->getDoctorTitleFormat(),
                'doctors' => $this->getUser()->getDoctors(),
                'all_doctors' => $this->getUser()->isAllDoctors(),
                'date_format' => $this->getUser()->getDateFormat(),
                'screen_date_format' => $this->getUser()->getJsDateFormat(),
            ));
            $this->quickReports['attended_appointment_count'] = $this->quickReports['attended_appointment_refine_form']->getCount();
        }

        // unresolved claims
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK)) {
            $this->quickReports['unresolved_claims_form'] = new UnresolvedClaimsForm(array(), array(
                'client_id' => $this->getUser()->getClientID(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'current_location_id' => $this->getUser()->getCurrentLocID(),
                'date_format' => $this->getUser()->getDateFormat(),
                'screen_date_format' => $this->getUser()->getJsDateFormat(),

            ));
            $this->quickReports['unresolved_claims_count'] = $this->quickReports['unresolved_claims_form']->getCount();
        }
     
        // number of unacknowledged and unarchived, batch submissions 7 days to 7 weeks ago
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK)) {
            $this->quickReports['unacknowledged_claims_count'] = new UnacknowledgedClaimsForm(array(), array(
                'client_id' => $this->getUser()->getClientID(),
                'date_format' => $this->getUser()->getDateFormat(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'current_location_id' => $this->getUser()->getCurrentLocID(),
                'screen_date_format' => $this->getUser()->getJsDateFormat(),
            ));
            $this->quickReports['unacknowledged_claims_count'] = $this->quickReports['unacknowledged_claims_count']->getCount();
        }

        // adjudication
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK)) {
            $this->quickReports['adjudication_claims_count'] = new AdjudicationClaimsForm(array(), array(
                'client_id' => $this->getUser()->getClientID(),
                'date_format' => $this->getUser()->getDateFormat(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'current_location_id' => $this->getUser()->getCurrentLocID(),
                'screen_date_format' => $this->getUser()->getJsDateFormat(),
            ));
            $this->quickReports['adjudication_claims_count'] = $this->quickReports['adjudication_claims_count']->getCount();
        }


        // today's appointments
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK) && $this->getUser()->getDashboardShowTodaysAppointmentsWidget()) {
            $this->quickReports['todays_appointment_refine_form'] = new TodaysAppointmentsRefineForm(array(), array(
                'client_id' => $this->getUser()->getClientID(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'current_location_id' => $this->getUser()->getCurrentLocID(),
                'doctor_title' => $this->getUser()->getDoctorTitleFormat(),
                'doctors' => $this->getUser()->getDoctors(),
                'all_doctors' => $this->getUser()->isAllDoctors(),
            ));
            $this->quickReports['todays_appointment_count'] = $this->quickReports['todays_appointment_refine_form']->getCount();
        }

        // Patient Follow Ups
        if ($this->getUser()->getDashboardShowPatientFollowUpsWidget()) {
            $followUpCount = $this->getPatientFollowUpQuery($locationIDs,'location_search')->count();
            if ($followUpCount) {
                $this->quickReports['follow_up_form'] = new FollowUpForm();
                $this->quickReports['follow_up_count'] = $followUpCount;
            }
        }

     
        // MDC-style Appointment Report
        // TODO: remove this My Doctor Care Appointments Widget as nobody is using it.
        $this->showMyDoctorCareAppointmentWidget = $this->getUser()->getDashboardShowMyDoctorCareReportWidget();
        if ($this->showMyDoctorCareAppointmentWidget) {
            $this->appointment_report_form = new AppointmentReportForm(array(
                'client_id' => $this->getUser()->getClientID(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'doctor_title' => $this->getUser()->getDoctorTitleFormat(),
                'doctors' => $this->getUser()->getDoctors(),
                'all_doctors' => $this->getUser()->isAllDoctors(),
                'date_format' => $this->getUser()->getDateFormat(),
                'time_format' => $this->getUser()->getTimeFormat(),
                'screen_date_format' => $this->getUser()->getJsDateFormat(),
                'datetime_format' => $this->getUser()->getDateTimeFormat(),
            ));
        }

        // Generate Blank Appointments for Mobile Application
        $this->showGenerateBlankAppointments = $this->getUser()->getDashboardGenerateBlankAppointments();
        if ($this->showGenerateBlankAppointments) {
            $this->showGenerateBlankAppointments = true;

            $defaults = array();
            $defaults['date'] = date($this->getUser()->getDateFormat());
            $defaults['location_id'] = $this->getUser()->getCurrentLocID();
            $defaults = $this->getUser()->getFormDefaults('generateBlankAppointmentsForm', $defaults);

            $this->generateBlankAppointmentsForm = new GenerateBlankAppointmentsForm($defaults, array(
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'doctors' => $this->getUser()->getDoctors(),
                'date_format' => $this->getUser()->getDateFormat(),
                'screen_date_format' => $this->getUser()->getJsDateFormat(),
            ));

            $this->unusedCount = Doctrine_Query::create()
                    ->from('AppointmentAttendee att')
                    ->leftJoin('att.Appointment a')
                    ->addWhere('a.start_date < (?)', date('Y-m-d 00:00:00'))
                    ->addWhere('a.client_id = (?)', $this->getUser()->getClientID())
                    ->addWhere('att.client_id = (?)', array($this->getUser()->getClientID()))
                    ->addWhere('a.dummy_data = (?)', true)
                    ->count();
        }

        // Default Printer for Direct Printing labels
        $this->enablePrintService = $this->getUser()->getEnablePrintService();
        if ($this->enablePrintService) {
        	$this->setupLocationPrinterDropdown();
        }
        // Bill Appointments Widget
        // TODO: this could be integrated into the Quick Reports - #### appointments ready to be billed
//        print_r($this->getUser()->getClientHasModule('appointment_review') || $this->getUser()->getClientHasModule('appointment_book'));exit;
        $this->showBillAppointmentsWidget = $this->getUser()->getClientHasModule('appointment_review') && $this->getUser()->getClientHasModule('appointment_book');
        if ($this->showBillAppointmentsWidget) {


            $appointment_statuses = AppointmentSearchForm::getAppointmentStatuses();

            $this->bill_appointments_form = new BillAppointmentsForm(array(
                'client_id' => $this->getUser()->getClientID(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(true),
                'doctor_title' => $this->getUser()->getDoctorTitleFormat(),
                'doctors' => $this->getUser()->getDoctors(),
                'all_doctors' => $this->getUser()->isAllDoctors(),
                'date_format' => $this->getUser()->getDateFormat(),
                'screen_date_format' => $this->getUser()->getJsDateFormat(),
                'datetime_format' => $this->getUser()->getDateTimeFormat(),
                'appt_status' => $appointment_statuses
            ));
            $this->bill_appointments_form->setDefault('start_date', date($this->getUser()->getDateFormat()));
            $this->bill_appointments_form->setDefault('end_date', date($this->getUser()->getDateFormat()));
            $this->bill_appointments_form->setDefault('location_id', $this->currentLocationID);
            $this->bill_appointments_form->setDefault('status', 10);
            
            
        }

        // HL7 Inbound Report
        // TODO: this report could benefit from a client parameter
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_HL7_REPORTS)) {
            $this->hl7Counts = $this->getHL7InboundCounts();
            $this->showHl7InboundReport = count($this->hl7Counts);
        }

        // temporary claims
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_CLAIM_CREATE)) {
            $this->isAdmin = $this->getUser()->hasCredential('System Administrator');

            if ($this->isAdmin) {
                $this->temporary_form_count = $this->getTemporaryFormClaimQuery()->count();
                
                if ($this->temporary_form_count) {
                    $this->temporary_form_items = $this->getTemporaryFormClaimQuery()->limit(10)->execute();
                    $this->showTemporaryFormItems = true;
                }
            } else {
                $this->temporary_form_count = $this->getTemporaryFormClaimQueryFiltered($this->getUser()->getUserID())->count();
                if ($this->temporary_form_count) {
                    $this->temporary_form_items = $this->getTemporaryFormClaimQueryFiltered($this->getUser()->getUserID())->limit(10)->execute();
                    $this->showTemporaryFormItems = true;
                }
            }
        }

        // reminders queue
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_HYPE_ADMINISTRATOR)) {
            $templates = Doctrine_Query::create()
                    ->from('ReminderTemplate e')
                    ->addWhere('e.active = (?)', array(true))
                    ->execute();

            $this->templateCount = 0;
            $this->templateUnprocessedCount = 0;
            $this->templateEmailsSentCount = 0;
            $templateTypes = array();

            foreach ($templates as $template) {
                $this->templateCount++;
                $templateTypes[$template->getEventTrigger()] = $template->getEventTrigger();
            }

            if ($this->templateCount) {
                $this->templateUnprocessedCount = Doctrine_Query::create()
                        ->from('ReminderEvent e')
                        ->addWhere('e.status = (?)', array(ReminderEventTable::STATUS_UNPROCESSED))
                        ->whereIn('e.event_type', array_keys($templateTypes))
                        ->count();

                $this->templateEmailsSentCount = Doctrine_Query::create()
                        ->from('ReminderEvent r')
                        ->addWhere('r.updated_at >= (?)', date(hype::DB_ISO_DATE, strtotime('today - 2 days')) . ' 00:00:00')
                        ->addWhere('r.email_text is not null')
                        ->count();
            }
        } else {
            $templates = Doctrine_Query::create()
                    ->from('ReminderTemplate e')
                    ->addWhere('e.active = (?)', array(true))
                    ->addWhere('e.client_id = (?)', $this->getUser()->getClientID())
                    ->execute();

            $this->templateCount = 0;
            $this->templateUnprocessedCount = 0;
            $this->templateEmailsSentCount = 0;
            $templateTypes = array();

            foreach ($templates as $template) {
                $this->templateCount++;
                $templateTypes[$template->getEventTrigger()] = $template->getEventTrigger();
            }

            if ($this->templateCount) {
                $this->templateUnprocessedCount = Doctrine_Query::create()
                        ->from('ReminderEvent e')
                        ->addWhere('e.client_id = (?)', $this->getUser()->getClientID())
                        ->addWhere('e.status = (?)', array(ReminderEventTable::STATUS_UNPROCESSED))
                        ->whereIn('e.event_type', array_keys($templateTypes))
                        ->count();

                $this->templateEmailsSentCount = Doctrine_Query::create()
                        ->from('ReminderEvent r')
                        ->addWhere('r.updated_at >= (?)', date(hype::DB_ISO_DATE, strtotime('today - 2 days')) . ' 00:00:00')
                        ->addWhere('r.client_id = (?)', array($this->getUser()->getClientID()))
                        ->addWhere('r.email_text is not null')
                        ->count();
            }
        }

        $this->hasExportsReady = false;
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_EXPORT_DATA)) {
            $this->hasExportsReady = Doctrine_Query::create()
                    ->from('DataExportRequest d')
                    ->addWhere('d.client_id = (?)', array($this->getUser()->getClientID()))
                    ->addWhere('d.status = (?)', array(DataExportRequestTable::FILE_STATUS_COMPLETED))
                    ->count();
        }

        $this->showQuickReports = (bool) count($this->quickReports);

        $this->hasClientWorkflow = false;
        if ($this->getUser()->getClientHasModule(hype::MODULE_DASHBOARD_WORKFLOW)) {
            $workflowData = ClientWorkflowTable::getWorkflowData($this->getUser()->getClientId(), ClientWorkflowTable::MODULE_NAME_DASHBOARD);

            if (is_array($workflowData)) {
                $this->hasClientWorkflow = true;
                $this->setVar('workflowSteps', array_keys($workflowData));
                $this->setVar('workflowData', json_encode($workflowData));

                $this->getResponse()->addJavascript('workflows/hypeMedicalWorkflow.js?v=2015.03');
                $this->getResponse()->addJavascript('card_swipe/CardReader.js?v=2015.03');
            }
        }



        //TODO change to permission
        $this->showPerformanceChart = true;


//        if ($this->showQuickReports) {
//            $this->unsubmittedCount = Doctrine_Query::create()
//                                        ->from('Items i')
//                                        ->where('i.client_id = (?)', $this->getUser()->getClientID())
//                                        ->addWhere('i.status = (?)', ClaimItemTable::STATUS_PAID_CLOSED)
//                                        ->addWhere('i.created_at >= (?)', date(hype::DB_ISO_DATE, strtotime('today - 7 days')) . ' 00:00:00')
//                                        ->count();
//
//            $this->rejectedCount = Doctrine_Query::create()
//                ->from('Items i')
//                ->where('i.client_id = (?)', $this->getUser()->getClientID())
//                ->addWhere('i.status = (?)', ClaimItemTable::STATUS_REJECTED_OPEN)
//                ->addWhere('i.created_at >= (?)', date(hype::DB_ISO_DATE, strtotime('today - 7 days')) . ' 00:00:00')
//                ->count();
//
//            $this->rejectedCloseCount = Doctrine_Query::create()
//                ->from('Items i')
//                ->where('i.client_id = (?)', $this->getUser()->getClientID())
//                ->addWhere('i.status = (?)', ClaimItemTable::STATUS_REJECTED_CLOSED)
//                ->addWhere('i.created_at >= (?)', date(hype::DB_ISO_DATE, strtotime('today - 7 days')) . ' 00:00:00')
//                ->count();
//
//        }

        $this->acknowledgedBatches = Doctrine_Query::create()
                ->from('Batch b')
                ->where('b.status = (?)', BatchTable::STATUS_ACKNOWLEDGED)
                ->addWhere('b.client_id = (?)', $this->getUser()->getClientID())
                ->addWhere('b.updated_at >= (?)', date(hype::DB_ISO_DATE, strtotime('today - 7 days')) . ' 00:00:00')
                ->count();

        $this->submittedBatches = Doctrine_Query::create()
                ->from('Batch b')
                ->where('b.status = (?)', BatchTable::STATUS_BATCH_SUBMITTED)
                ->addWhere('b.client_id = (?)', $this->getUser()->getClientID())
                ->addWhere('b.updated_at >= (?)', date(hype::DB_ISO_DATE, strtotime('today - 7 days')) . ' 00:00:00')
                ->count();

        $this->unsubmittedBatches = Doctrine_Query::create()
                ->from('Batch b')
                ->where('b.status = (?)', BatchTable::STATUS_READY_TO_SUBMIT)
                ->addWhere('b.client_id = (?)', $this->getUser()->getClientID())
                ->addWhere('b.updated_at >= (?)', date(hype::DB_ISO_DATE, strtotime('today - 7 days')) . ' 00:00:00')
                ->count();

        $this->wipBatches = Doctrine_Query::create()
                ->from('Batch b')
                ->where('b.status = (?)', BatchTable::STATUS_WIP)
                ->addWhere('b.client_id = (?)', $this->getUser()->getClientID())
                ->addWhere('b.updated_at >= (?)', date(hype::DB_ISO_DATE, strtotime('today - 7 days')) . ' 00:00:00')
                ->count();

        $this->rejectedBatches = Doctrine_Query::create()
                ->from('Batch b')
                ->where('b.status = (?)', BatchTable::STATUS_REJECTED)
                ->addWhere('b.client_id = (?)', $this->getUser()->getClientID())
                ->addWhere('b.updated_at >= (?)', date(hype::DB_ISO_DATE, strtotime('today - 7 days')) . ' 00:00:00')
                ->count();


        $doctorData = Doctrine_Query::create()
                    ->from('Doctor d')
                    ->select('d.id, d.fname')
                    ->addWhere('d.fname LIKE (?)', '%WALK%')
                    ->addwhere('d.client_id = ?', $this->getUser()->getClientID())
                    ->execute(array(), Doctrine::HYDRATE_ARRAY);

         $appointment_types = Doctrine_Query::create()
            ->from('AppointmentType at')
            ->select('at.id, at.name')
            ->addWhere('at.name LIKE (?)', '%WALK%')
            ->addWhere('at.client_id = ?', $this->getUser()->getClientID())
            ->execute(array(), Doctrine::HYDRATE_ARRAY);
            // print_r($appointment_types);exit;
            // $walkinDoctorId = $this->doctorData[0]['id'];

            if(!empty($doctorData)){
                $this->walkindoctorData = $doctorData;
            }else{
                $this->walkindoctorData = null;

            }
            if(!empty($appointment_types)){
                $this->appointmentTypeName = $appointment_types;
            }else{
                $this->appointmentTypeName = null;

            }
    }

    public function executeLocationWiseCount(hypeWebRequest $request)
    {
        $this->currentLocationID = $request->getParameter('location_id');

        //$this->currentLocationID = $this->getUser()->getCurrentLocID();
        $locationIDs = $this->currentLocationID ? array($this->currentLocationID) : '';
        $locationIDs1 = $this->currentLocationID ? array($this->currentLocationID) : array_keys($this->getUser()->getLocations());


        $this->showClientSwitcher = false;
        $this->showLocationSwitcher = (count($this->getUser()->getLocations()) > 1) ? true : false;
        $this->showTemporaryFormItems = false;
        $this->showHl7InboundReport = false;
        $this->showErrorReportWidget = false;
        $this->showAuditTrailReportWidget = false;
        $this->quickReports = array();
        $this->clients = null;

        // Patient Follow Ups // skip for some time
        if ($this->getUser()->getDashboardShowPatientFollowUpsWidget()) {
            $followUpCount = $this->getPatientFollowUpQuery($locationIDs1,'location_search')->count();
            if ($followUpCount) {
                $this->quickReports['follow_up_form'] = new FollowUpForm();
                $this->quickReports['follow_up_count'] = $followUpCount;
            }
        }


        // orphaned appointments
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK) && $this->getUser()->getDashboardShowOrphansWidget()) {
            $this->quickReports['orphan_refine_form'] = new OrphanedAppointmentRefineForm(array(), array(
                'client_id' => $this->getUser()->getClientID(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'current_location_id' => $locationIDs,
                'doctor_title' => $this->getUser()->getDoctorTitleFormat(),
                'doctors' => $this->getUser()->getDoctors(),
                'all_doctors' => $this->getUser()->isAllDoctors(),
                'date_format' => $this->getUser()->getDateFormat(),
                'screen_date_format' => $this->getUser()->getJsDateFormat(),
            ));
           $this->quickReports['orphan_count'] = $this->quickReports['orphan_refine_form']->getCount();
        }
        
        // follow-up appointments
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK) && $this->getUser()->getDashboardShowFollowUpWidget()) {
            $this->quickReports['follow_up_appointment_refine_form'] = new FollowUpAppointmentRefineForm(array(), array(
                'client_id' => $this->getUser()->getClientID(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'current_location_id' => $locationIDs,
                'doctor_title' => $this->getUser()->getDoctorTitleFormat(),
                'doctors' => $this->getUser()->getDoctors(),
                'all_doctors' => $this->getUser()->isAllDoctors(),
                'date_format' => $this->getUser()->getDateFormat(),
                'screen_date_format' => $this->getUser()->getJsDateFormat(),
            ));
            $this->quickReports['follow_up_appointment_count'] = $this->quickReports['follow_up_appointment_refine_form']->getCount();
        }
       

        // attended appointments
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK) && $this->getUser()->getDashboardShowFollowUpWidget()) {
            $this->quickReports['attended_appointment_refine_form'] = new AttendedAppointmentRefineForm(array(), array(
                'client_id' => $this->getUser()->getClientID(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'current_location_id' => $locationIDs,
                'doctor_title' => $this->getUser()->getDoctorTitleFormat(),
                'doctors' => $this->getUser()->getDoctors(),
                'all_doctors' => $this->getUser()->isAllDoctors(),
                'date_format' => $this->getUser()->getDateFormat(),
                'screen_date_format' => $this->getUser()->getJsDateFormat(),
            ));
            $this->quickReports['attended_appointment_count'] = $this->quickReports['attended_appointment_refine_form']->getCount();
        }


        // today's appointments
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK) && $this->getUser()->getDashboardShowTodaysAppointmentsWidget()) {
            $this->quickReports['todays_appointment_refine_form'] = new TodaysAppointmentsRefineForm(array(), array(
                'client_id' => $this->getUser()->getClientID(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'current_location_id' => $locationIDs,
                'doctor_title' => $this->getUser()->getDoctorTitleFormat(),
                'doctors' => $this->getUser()->getDoctors(),
                'all_doctors' => $this->getUser()->isAllDoctors(),
            ));
            $this->quickReports['todays_appointment_count'] = $this->quickReports['todays_appointment_refine_form']->getCount();
        }


        // unresolved claims
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK)) {
            $this->quickReports['unresolved_claims_form'] = new UnresolvedClaimsForm(array(), array(
                'client_id' => $this->getUser()->getClientID(),
                'date_format' => $this->getUser()->getDateFormat(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'current_location_id' => $locationIDs,
                'screen_date_format' => $this->getUser()->getJsDateFormat(),
            ));
            $this->quickReports['unresolved_claims_count'] = $this->quickReports['unresolved_claims_form']->getCount();
        }

     
        // number of unacknowledged and unarchived, batch submissions 7 days to 7 weeks ago
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK)) {
            $this->quickReports['unacknowledged_claims_count'] = new UnacknowledgedClaimsForm(array(), array(
                'client_id' => $this->getUser()->getClientID(),
                'date_format' => $this->getUser()->getDateFormat(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'current_location_id' => $locationIDs,
                'screen_date_format' => $this->getUser()->getJsDateFormat(),
            ));
            $this->quickReports['unacknowledged_claims_count'] = $this->quickReports['unacknowledged_claims_count']->getCount();
        }

        // adjudication
        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK)) {
            $this->quickReports['adjudication_claims_count'] = new AdjudicationClaimsForm(array(), array(
                'client_id' => $this->getUser()->getClientID(),
                'date_format' => $this->getUser()->getDateFormat(),
                'locations' => $this->getUser()->getLocations(),
                'location_title' => $this->getUser()->getLocationTitle(),
                'current_location_id' => $locationIDs,
                'screen_date_format' => $this->getUser()->getJsDateFormat(),
            ));
            $this->quickReports['adjudication_claims_count'] = $this->quickReports['adjudication_claims_count']->getCount();
        }
        

        $getCountList = array(
                              'follow_up_count' => number_format($this->quickReports['follow_up_count'], 0, '.', ','),
                              'orphan_count' => number_format($this->quickReports['orphan_count'], 0, '.', ','),
                              'follow_up_appointment_count' =>number_format($this->quickReports['follow_up_appointment_count'], 0, '.', ','),
                              'attended_appointment_count' =>number_format($this->quickReports['attended_appointment_count'], 0, '.', ','),
                              'todays_appointment_count' =>number_format($this->quickReports['todays_appointment_count'], 0, '.', ','),
                              'unresolved_claims_count' =>number_format($this->quickReports['unresolved_claims_count'], 0, '.', ','),
                              'unacknowledged_claims_count' =>number_format($this->quickReports['unacknowledged_claims_count'], 0, '.', ','),
                              'adjudication_claims_count' =>number_format($this->quickReports['adjudication_claims_count'], 0, '.', ','),
                             );

        echo json_encode($getCountList);
        exit;
    }

    public function executeResetSystemCounters(hypeWebRequest $request) {
        
    }

    public function executeSetPrinter(hypeWebRequest $request) {
        $printer = $request->getParameter('printer');
        $this->getUser()->setAttribute('default_printer', $printer, 'UserParameter');
        return $this->renderText(json_encode(array('success')));
    }

    public function executeSignout(hypeWebRequest $request) {

        unset($_SESSION['claimlock']);
        $user = $this->getUser();
        if ($user->isAuthenticated()) {
            $this->getUser()->signOut();
        }
        return $this->redirect('@homepage');
    }

    public function executeMyDbrSignon(hypeWebRequest $request) {
        $user = $this->getUser();

        $username = $this->getUser()->getAttribute('user_name', null, 'UserParameters');
        $name = null;
        $groups = $this->getUser()->getClientID();
        $token = $_REQUEST['token'];
        $url = $_REQUEST['url'];
        $hash_seed = sfConfig::get('app_mydbr_sso_hash_seed', null);

        $hash = sha1($username . $name . $groups . $token . $hash_seed);
        $url = $url . '?user=' . urlencode($username) . '&name=' . urlencode($name) . '&hash=' . $hash . '&groups=' . urlencode($groups);

        header('Location:' . $url);
        die;
    }

    public function executeError404() {
        
    }

}
