<div id="todays_appointments_dialog" style="display:none" title="Today's Doctor Appointments">
    <div class="panel panel-default">
        <div class="panel-heading expand">
           
                <i class="clip-search-3"></i>
                Refine Your Search <span id="todays_appts_spinner" style="display: none;"><i class="fa fa-spin fa-refresh"></i></span>
            
        </div>
        <div class="panel-body buttons-widget" style="overflow: hidden; display: none;">
            <form id="todays_appointment_refine_search">
                <?php echo $form->renderHiddenFields(); ?>
                <div id="todays_appointment_refine" class="form-table row">
                    <div class="col-sm-4 ">
                        <div class="form-group">
                            <label class="control-label">
                                <?php echo $form['location_id']->renderLabel(); ?>
                            </label>
                            <?php echo $form['location_id']->render(['class' => 'form-control Select2']); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">
                                <?php echo $form['doctor_id']->renderLabel(); ?>
                            </label>
                            <?php echo $form['doctor_id']->render(['class' => 'form-control Select2']); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group" style="margin-top:30px;">

                            <button type="button" class="btn btn-blue btn-squared" id="todays_appointment_refine_button" ><i class="clip-search-3"> </i> Search</button>  
                            <button type="button" class="btn btn-danger btn-squared" id="todays_appointment_clear_button"><i class="clip-file-minus"> </i> Clear</button>  

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>



 <a  id="hyperLink" href="javascript:void(0)">
    <h4>Displaying <span id="todays_appts_result_count"></span> of <?php echo number_format($count, 0, '.', ','); ?> appointments</h4>
</a>
    <table id="todays_appts_report_results" class="sk-datatable table table-hover table-striped"></table>

    <p style="color:#858585;font-size:12px">These appointments all occur today (<?php echo date($sf_user->getDateFormat()); ?>). Clicking on any of the appointments will take you to the appointment's details in the appointment book.
        <br />Maximum number of results: 500</p>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        todaysAppointments.actions.search = '<?php echo url_for('appointment_search/todaysAppointments'); ?>';
        todaysAppointments.actions.visit_sheet = '<?php echo url_for('appointment_review/visitPDF'); ?>';
        todaysAppointments.initialize();
        
        $("#hyperLink").click(function(){
            var loc_id = $("#todays_appointments_location_id").val();
            var doc_id = $("#todays_appointments_doctor_id").val();
            var url = '/appointment_search?ref=desktop&t=today&doc='+doc_id+'&loc='+loc_id;
            window.open(url, '_blank');
        })
        
    });
</script>