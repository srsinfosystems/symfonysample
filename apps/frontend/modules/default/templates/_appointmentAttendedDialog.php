<div id="attendedAppointmentDialog" style="display:none" title="Past Appointments Not Billed">
<div class="panel panel-default">
        <div class="panel-heading expand">
            <i class="clip-search-3"></i>
            Refine Your Search <span id="follow_up_appts_spinner" style="display: none;"><i class="fa fa-spin fa-refresh"></i></span>
        </div>
        <div class="panel-body buttons-widget" style="overflow: hidden; display: none;">
    <form id="attendedAppointmentsForm">
        <?php echo $form->renderHiddenFields(); ?>
        <div id="attended_refine" class="form-table row">
            <div class="col-sm-3 col-xs-6">
                <div class="form-group">
                    <label class="control-label">
                       <?php echo $form['start_date']->renderLabel(); ?>
                    </label>
                    <?php echo $form['start_date']->render(['class'=>'form-control btn-squared']); ?>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group">
                    <label class="control-label">
                       <?php echo $form['location_id']->renderLabel(); ?>
                    </label>
                    <?php echo $form['location_id']->render(['class'=>'form-control Select2']); ?>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group">
                    <label class="control-label">
                       <?php echo $form['doctor_id']->renderLabel(); ?>
                    </label>
                    <?php echo $form['doctor_id']->render(['class'=>'form-control Select2']); ?>
                </div>
            </div>
         <div class="col-sm-3 col-xs-6">
                <div class="form-group" style="margin-top:30px;">
  
                     <button type="button" class="btn btn-blue btn-squared" id="appointmentAttendedRefineSubmit" ><i class="clip-search-3"> </i> Search</button>  
                <button type="button" class="btn btn-danger btn-squared" id="appointmentAttendedRefineClear"><i class="clip-file-minus"> </i> Clear</button> 
          
                </div>
            </div>
        </div>
    </form>
        </div>
</div>
    <h4>Displaying <span id="attended_appt_result_count"></span> of <span id="attended_appt_total_dialog"><?php echo number_format($count, 0, '.', ','); ?></span> possible appointments</h4>
    <p style="color:#858585;font-size:12px">These appointments need to be billed. To quickly bill what you see below, use the Bill Appointments utility on the home page.
        <br />Maximum number of results: 500</p>
    <form id="attended_update_form">
        <table id="appointmentAttendedTable" class="sk-datatable table table-hover table-striped">
        </table>
    </form>


</div>