<body class="login example2">
    <div class="main-login col-sm-6 col-sm-offset-3  col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
        <div class="logo">
            <img class="img-responsive" src="<?= image_path('/clipone/hype-logo.png') ?>" />
        </div>
        
        <!-- start: LOGIN BOX -->
        <div class="box-login">
            <h3>Log in to your account</h3>
            <p>
                Please enter your name and password to Log in.
            </p>
            <form class="form-login" id="login-form" method="post" action="<?= url_for('@sf_guard_signin'); ?>">
                <?php if($form->renderBottomErrorBox()): ?>
                <div class="errorHandler alert alert-danger">
                    <ul>
                        <?php $errors = $form->getErrorSchema()->getErrors() ?>
                        <?php if (count($errors) > 0) : ?>
                            <?php foreach ($errors as $name => $error) : ?>
                                <li><?php echo $name ?> : <?php echo $error ?></li>
                            <?php endforeach ?>
                        <?php endif ?>
                    </ul>
                </div>
                <?php endif ?>
                <fieldset>

                    <div class="form-group">
                        <span class="input-icon">
                            <?php echo $form['username']->render(['class' => 'form-control', 'placeholder' => 'Username', 'required' => 'true']); ?>
                            <!--<input type="text" class="form-control" name="username" placeholder="Username">-->
                            <i class="fa fa-user"></i> </span>
                    </div>
                    <div class="form-group form-actions">
                        <span class="input-icon">
                            <?php echo $form['password']->render(['class' => 'form-control password', 'placeholder' => 'Password', 'required' => 'true']); ?>
                            <!--<input type="password" class="form-control password" name="password" placeholder="Password">-->
                            <i class="fa fa-lock"></i>
<!--                            <a class="forgot" href="javascript:void(0)">
                                I forgot my password
                            </a> -->
                        </span>
                    </div>
                    <div class="form-actions">
                        <label for="remember" class="checkbox-inline">
                            <?php echo $form['remember']?>
                            <!--<input type="checkbox" class="grey remember" id="remember" name="remember">-->
                            Keep me Logged in
                        </label>
                        <button type="submit" class="btn btn-bricky pull-right" name="log_in" id="log_in" >
                            Login <i class="fa fa-arrow-circle-right"></i>
                        </button>
                    </div>
<!--                    <div class="new-account">
                        Don't have an account yet?
                        <a href="javascript:void(0)" class="register">
                            Create an account
                        </a>
                    </div>-->
                </fieldset>
            </form>
        </div>
        <!-- end: LOGIN BOX -->
        <!-- start: FORGOT BOX -->
        <div class="box-forgot">
            <h3>Forget Password?</h3>
            <p>
                Enter your e-mail address below to reset your password.
            </p>
            <form class="form-forgot">
                <div class="errorHandler alert alert-danger no-display">
                    <i class="fa fa-remove-sign"></i> You have some form errors. Please check below.
                </div>
                <fieldset>
                    <div class="form-group">
                        <span class="input-icon">
                            <input type="email" class="form-control" name="email" placeholder="Email">
                            <i class="fa fa-envelope"></i> </span>
                    </div>
                    <div class="form-actions">
                        <a class="btn btn-light-grey go-back">
                            <i class="fa fa-circle-arrow-left"></i> Back
                        </a>
                        <button type="submit" class="btn btn-bricky pull-right">
                            Submit <i class="fa fa-arrow-circle-right"></i>
                        </button>
                    </div>
                </fieldset>
            </form>
        </div>
        <!-- end: FORGOT BOX -->
        <!-- start: REGISTER BOX -->
        <div class="box-register">
            <h3>Sign Up</h3>
            <p>
                Register for a free trial
            </p>
            <form class="form-register" action="signup" href="#signup" method="POST">
                 <?php if($form->renderBottomErrorBox() || isset($_GET['e'])): ?>
                <div class="errorHandler alert alert-danger">
                    <ul>
                        <?php if(isset($_GET['e'])){echo '<li>'.$_GET['e'].'</li>'; }  ?>
                        <?php $errors = $form->getErrorSchema()->getErrors() ?>
                        <?php if (count($errors) > 0) : ?>
                            <?php foreach ($errors as $name => $error) : ?>
                                <li><?php echo $name ?> : <?php echo $error ?></li>
                            <?php endforeach ?>
                        <?php endif ?>
                    </ul>
                </div>
                <?php endif ?>
                <fieldset>
                    <div class="form-group">
                        <input type="text" required class="form-control" name="fname" id="inputFname" placeholder="First name"  >
                    </div>
                    <div class="form-group">
                        <input type="text" required class="form-control" name="lname" id="inputLname" placeholder="Last name"  >
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="company" id="inputCompany" placeholder="Company name" >
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="address" id="inputAddress" placeholder="Address" >
                    </div>
                    <div class="form-group">
                                        <input type="text" class="form-control" name="city" id="inputCity" placeholder="City" >

                    </div>
                     <div class="form-group">
                                        <input type="text" class="form-control" name="postal" id="inputPostal" placeholder="Postal" >

                    </div>
                    <div class="form-group">
                <input type="phone" class="form-control" name="phone" id="inputPhone" placeholder="Phone" required>
                    </div>
        

                    <p>
                        Enter your account details below:
                    </p>
                    <div class="form-group">
                        <span class="input-icon">
                <input type="email" required class="form-control" name="email" id="inputEmail3" placeholder="Email" required>
                            <i class="fa fa-envelope"></i> </span>
                    </div>
                    <div class="form-group">
                        <span class="input-icon">
                <input type="text" required class="form-control" name="username" id="inputUsername" placeholder="Username" required>
                            <i class="fa fa-user"></i> </span>
                    </div>
                    <div class="form-group">
                        <span class="input-icon">
                <input type="password" required class="form-control" name="password" id="inputPassword" placeholder="Password" required>
                            <i class="fa fa-lock"></i> </span>
                    </div>
                    <div class="form-group">
                        <div>
                            <label for="agree" class="checkbox-inline">
                                <input type="checkbox" required class="grey agree" id="agree" name="agree">
                                I agree to the Terms of Service and Privacy Policy
                            </label>
                        </div>
                    </div>
                    <div class="form-actions">
                        <a class="btn btn-light-grey go-back">
                            <i class="fa fa-circle-arrow-left"></i> Back
                        </a>
                        <button type="submit" id="signupBtn" value='signup' class="btn btn-bricky pull-right">
                            Submit <i class="fa fa-arrow-circle-right"></i>
                        </button>
                    </div>
                </fieldset>
            </form>
        </div>
        <!-- end: REGISTER BOX -->
        <!-- start: COPYRIGHT -->
        <div class="copyright">
            <?= date('Y') ?>  &copy; Hype Systems.
        </div>
        <div class="logo customlogo2">
            <img src="<?= image_path('logo/HypeSystemsInc.gif') ?>" />
        </div>
        <!-- end: COPYRIGHT -->
    </div>
</body>
<!--<div id="login_dialog" class="ui-dialog ui-widget ui-widget-content ui-corner-all">
    <div class="ui-widget-header ui-dialog-titlebar ui-corner-all">Log In</div>

    <form action="<?php echo url_for('@sf_guard_signin'); ?>" method="POST" id="login-form">
        <table class="form-table" id="login_form_table">
<?php echo $form; ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="submit" value="Log In" name="log_in" id="log_in" />
<?php echo $form->renderHiddenFields(); ?>
                </td>
            </tr>
        </table>
    </form>
<?php echo $form->renderBottomErrorBox(); ?>
</div>-->

<!--<br/>
<div id="twitter_dialog">

    <a class="twitter-timeline" data-height="350" href="https://twitter.com/HYPEsystems">Tweets by HYPEsystems</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

</div>

<br/>-->

<!--<div id="register" style="width: 700px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all">
    <div class="ui-widget-header ui-dialog-titlebar ui-corner-all">Register for a free trial</div>
    <br/>
    <form action="signup" href="#signup" method="POST" class="form-horizontal" style="max-width:700px;">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-8">
                <input type="email" class="form-control" name="email" id="inputEmail3" placeholder="Email" required>
            </div>
        </div>

        <div class="form-group">
            <label for="inputFname" class="col-sm-2 control-label">First Name</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="fname" id="inputFname" placeholder="John"  >
            </div>
        </div>

        <div class="form-group">
            <label for="inputLname" class="col-sm-2 control-label">Last Name</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="lname" id="inputLname" placeholder="Smith"  >
            </div>
        </div>

        <div class="form-group">
            <label for="inputCompany" class="col-sm-2 control-label">Company Name</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="company" id="inputCompany" placeholder="Company" >
            </div>
        </div>

        <div class="form-group">
            <label for="inputAddress" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="address" id="inputAddress" placeholder="Address" >
            </div>
        </div>

        <div class="form-group">
            <label for="inputCity" class="col-sm-2 control-label">City</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="city" id="inputCity" placeholder="City" >
            </div>
        </div>

        <div class="form-group">
            <label for="inputPostal" class="col-sm-2 control-label">Postal Code</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="postal" id="inputPostal" placeholder="Postal" >
            </div>
        </div>

        <div class="form-group">
            <label for="inputPhone" class="col-sm-2 control-label">Phone</label>
            <div class="col-sm-4">
                <input type="phone" class="form-control" name="phone" id="inputPhone" placeholder="Phone" required>
            </div>
        </div>


        <div class="form-group">
            <label for="inputUsername" class="col-sm-2 control-label">Username</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="username" id="inputUsername" placeholder="Username" required>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-5">
                <input type="password" class="form-control" name="password" id="inputPassword" placeholder="Password" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" id="signupBtn" class="btn btn-default">Sign up</button>
            </div>
        </div>
    </form>


</div>-->



<script>
    jQuery(document).ready(function () {
        Main.init();
        Login.init();
    });
</script>

<script>
    $('.forgot').on('click', function () {
        $('.box-login').hide();
        $('.box-forgot').show();
    });
    $('.register').on('click', function () {
        $('.box-login').hide();
        $('.box-register').show();
    });
    $('.go-back').click(function () {
        $('.box-login').show();
        $('.box-forgot').hide();
        $('.box-register').hide();
    });
</script>
<?php if(sfContext::getInstance()->getRouting()->getCurrentInternalUri() == 'default/signup'){ ?>
    <script>
        $('.register').trigger('click');
    </script>
<?php }?>
<script>
    var runSetDefaultValidation = function () {
        $.validator.setDefaults({
            errorElement: "span", // contain the error msg in a small tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
                    error.appendTo($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: ':hidden',
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error');
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').addClass('has-error');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            }
        });
    };
    var runLoginValidator = function () {
        var form = $('.form-login');
        var errorHandler = $('.errorHandler', form);
        form.validate({
            rules: {
                username: {
                    minlength: 2,
                    required: true
                },
                password: {
                    minlength: 6,
                    required: true
                }
            },
            submitHandler: function (form) {
                errorHandler.hide();
                form.submit();
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                errorHandler.show();
            }
        });
    };
    var runForgotValidator = function () {
        var form2 = $('.form-forgot');
        var errorHandler2 = $('.errorHandler', form2);
        form2.validate({
            rules: {
                email: {
                    required: true
                }
            },
            submitHandler: function (form) {
                errorHandler2.hide();
                form2.submit();
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                errorHandler2.show();
            }
        });
    };
    var runRegisterValidator = function () {
        var form3 = $('.form-register');
        var errorHandler3 = $('.errorHandler', form3);
        form3.validate({
            rules: {
                full_name: {
                    minlength: 2,
                    required: true
                },
                address: {
                    minlength: 2,
                    required: true
                },
                city: {
                    minlength: 2,
                    required: true
                },
                gender: {
                    required: true
                },
                email: {
                    required: true
                },
                password: {
                    minlength: 6,
                    required: true
                },
                password_again: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                agree: {
                    minlength: 1,
                    required: true
                }
            },
            submitHandler: function (form) {
                errorHandler3.hide();
                form3.submit();
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                errorHandler3.show();
            }
        });
    };
    return {
        //main function to initiate template pages
        init: function () {
            runLoginButtons();
            runSetDefaultValidation();
            runLoginValidator();
            runForgotValidator();
            runRegisterValidator();
        }
    }
    ;

</script>


<!--
<script type="text/javascript">
    $(document).ready(function () {
        $('#signupBtn').button();


        $('#log_in').button();
        $(document).keypress(function (e) {
            if (e.which == 13) {
                $('#login-form').submit();
                return false;
            }
        });
        quickClaim.focusTopField();
        $('#msgcontainer').html("<?php echo $last_message; ?>");

        if (window.location.pathname === '/signup') {
            $('#login_dialog').hide();
            $('#twitter_dialog').hide();
            $('#header').hide();
            $('#footer').hide();
        }
    });
</script>

<style>
    #login_dialog {
        position: relative;
        margin: 0px auto;
    }

    #register {
        position: relative;
        margin: 0px auto;
    }
    #login_form_table {
        margin-top: 4px;
    }

    #twitter_dialog {
        position: relative;
        margin: 0px auto;
        width: 600px;

    }
</style>-->
