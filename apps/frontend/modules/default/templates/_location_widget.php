

<div class="panel-heading">
    <h4 class="panel-title">
    <a class="accordion-toggle" data-toggle="collapse" data-parent=".myaccordion" href="#collapse2">
        <div class="heading dashboard_widget"  id="exportsWidget">
            <i class="clip-location circle-bricky circle-icon"></i>
            Select A Default <?php echo $sf_user->getLocationTitle(); ?>
        </div>
    </a></h4>
</div>
<div id="collapse2" class="panel-collapse collapse">
    <div class="panel-body">
        <div id="reports_widget" class="dashboard_widget quick_reports">
        <div class="core-box">           
            <div id="location_widget" class="content">
                <form method="post" action="<?php echo url_for('default/changeLocation'); ?>">
                    <div class="form-group ">
                        <label class="control-label">
                            Location
                        </label>
                        <div class="row">
                        <div class="col-sm-9 col-md-9">
                        <select class="form-control Select2" id="location_id" name="location_id">
                            <option value="" <?php echo $current_loc == null ? 'selected="selected"' : null; ?>>[<?php echo 'All ' . $sf_user->getLocationTitle() . 's'; ?>]</option>

                            <!-- <?php foreach ($sf_user->getLocations() as $loc_id => $loc_name): ?>
                                <option value="<?php echo $loc_id; ?>" <?php echo $loc_id == $current_loc ? 'selected="selected"' : null; ?>><?php echo $loc_name; ?></option>
                            <?php endforeach; ?>
                             -->
                            <?php foreach ($locationsData as $loc_id => $loc): ?>
                                <option value="<?php echo $loc->id; ?>" <?php echo $loc->id == $current_loc ? 'selected="selected"' : null; ?>><?php echo $loc->code.' - ' .$loc->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                        <div class="form-group  col-sm-3 col-md-3">
                        <button onclick="$('#change_location_save').trigger('click')" class="btn btn-blue btn-squared" type="button"><i class="clip-location"></i> Set <?php echo $sf_user->getLocationTitle(); ?></button>
                        <input style="display:none" type="submit" value="Set <?php echo $sf_user->getLocationTitle(); ?>" name="change_location_save" id="change_location_save" class="btn btn-success btn-squared" />
                    </div>
                    </div>
                    </div>
                    
                </form>
                <small style="color:#858585">
                    This <?php echo strtolower($sf_user->getLocationTitle()); ?> will be the default <?php echo strtolower($sf_user->getLocationTitle()); ?> selection for the duration of your session.
                </small>

            </div>
        </div>
        </div>
    </div>
</div>





<script type="text/javascript">
    $(document).ready(function () {
        $('#change_location_save').button();
    });
</script>
