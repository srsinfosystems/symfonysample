<div id="login_dialog" class="ui-dialog ui-widget ui-widget-content ui-corner-all">
    <div class="ui-widget-header ui-dialog-titlebar ui-corner-all">Log In</div>


</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#log_in').button();
        $(document).keypress(function (e) {
            if (e.which == 13) {
                $('#login-form').submit();
                return false;
            }
        });
        quickClaim.focusTopField();
        $('#msgcontainer').html("<?php echo $last_message; ?>");
    });
</script>

<style>
    #login_dialog {
        position: relative;
        margin: 0px auto
    }
    #login_form_table {
        margin-top: 4px;
    }

    #twitter_dialog {
        position: relative;
        margin: 0px auto;
        width: 600px;

    }
</style>
