
<style>
    canvas {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    .heading{
        font-size: 18px;
        font-family: inherit;
        font-weight: 500;
        line-height: 1.1;
        color: inherit;
    }
</style>


<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="breadcrumb">
                <div class="page-header" style="margin:-10px 0px 20px">
                    <h1>
                        <i class="clip-home" style="font-size: 32px !important"></i> 
                        Home
                        <small>overview &amp; stats</small>
                        <ul class="hidden-xs breadcrumbul">                            
                            <li class="active">
                                <i class="clip-home"></i>                   
                                Home            
                            </li>
                        </ul>
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <?php
            if ($showQuickReports):
                include_partial('quick_reports_widget', array(
                    'parameters' => $quickReports
                ));
            endif;
            ?>
        </div>
    </div>
    <div class="row">
        <!-- start: ACCORDION PANEL -->            
        <div class="col-sm-6 col-md-6">
            <div class="panel-group accordion-custom accordion-teal myaccordion" id="accordion">
                <?php if ($hasClientWorkflow): ?>
                    <div class="panel panel-default"> 
                        <?php
                        include_partial('client_workflows/dashboardWorkflow', array(
                            'data' => $workflowData,
                            'steps' => $workflowSteps,
                            'doctorData' => $walkindoctorData,
                            'appointmentTypeName' => $appointmentTypeName,
                        ));
                        ?> 
                    </div>
                <?php endif; ?>

                <?php if ($showLocationSwitcher): ?>
                    <div class="panel panel-default">            
                        <?php
                        include_partial('location_widget', array(
                            'current_loc' => $currentLocationID,
                            'locationsData' => $locationsData
                        ));
                        ?>
                    </div>
                <?php endif; ?>

                <div class="panel panel-default">
                    <?php
                    include_partial('styleSettings', array());
                    ?>
                </div>

                <?php if ($showBillAppointmentsWidget && $sf_user->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK)): ?>
                    <div class="panel panel-default">            
                        <?php
                        include_partial('bill_appointments_widget', array(
                            'form' => $bill_appointments_form,
                            'current_loc' => $currentLocationID,
                             'locationsData' => $locationsData
                        ));
                        ?>
                    </div>
                <?php endif; ?>

                <?php if ($showMyDoctorCareAppointmentWidget && $sf_user->hasCredential(hype::PERMISSION_NAME_APPOINTMENT_BOOK)): ?>
                    <div class="panel panel-default">
                        <?php
                        include_partial('reports_widget', array(
                            'form' => $appointment_report_form,
                        ));
                        ?>              
                    </div>
                <?php endif; ?>

                <?php if ($enablePrintService && count($printersList) + 1): ?>
                    <div class="panel panel-default">
                        <?php
                        include_partial('labelPrinterWidget', array(
                            'printers' => $printersList,
                            'default' => $defaultPrinter,
                        ));
                        ?>
                    </div>
                <?php endif; ?>



                <?php if ($showTemporaryFormItems): ?>
                    <div class="panel panel-default">           
                        <?php
                        include_partial('temporary_form_items', array(
                            'items' => $temporary_form_items,
                            'users' => $users,
                            'temporary_form_count' => $temporary_form_count,
                            'admin' => $isAdmin
                        ));
                        ?>                
                    </div>
                <?php endif; ?>

            </div>
        </div>
        <div class="col-sm-6 col-md-6">
            <?php if ($showAuditTrailReportWidget): ?>
            <div id="canvas-holder" style="width:100%;height:20%;">
                <div class="heading dashboard_widget">
                    <i class="clip-health circle-icon circle-bricky"></i>
                    Audit Trail Builder Health 
                </div>
                <canvas id="chart-area" style="height: 190px; width: 430px; display: block;"></canvas>
            </div>
            <?php endif; ?>
            <?php
            if ($sf_user->getClientHasModule('email_appointment_reminders')):
                include_partial('reminders_widget', array(
                    'templateCount' => $templateCount,
                    'templateUnprocessedCount' => $templateUnprocessedCount,
                    'templateEmailsSentCount' => $templateEmailsSentCount
                ));
            endif;
            ?>
            <?php if ($showErrorReportWidget): ?>
                <ul class="list-group">
                    <li href="#" class="list-group-item ">
                        <span class="heading dashboard_widget">
                            <i class="clip-atom circle-bricky circle-icon"></i>
                            OHIP Service
                        </span>
                        <span class="pull-right"><?php
                            include_partial('errorReport/error_report_item_widget', array(
                                'count' => $errorReportWidgetCount
                            ));
                            ?></span>
                    </li> 
                </ul>
            <?php endif; ?>

            <?php if ($hasExportsReady): ?>
                <div class="panel panel-default">
                    <?php
                    include_partial('exportsWidget', array(
                        'exportsCount' => $hasExportsReady
                    )); 
                    ?>
                </div>
            <?php endif; ?>


        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">  <br> <hr>         
            <?php
            if ($showHl7InboundReport):
                include_partial('hl7_inbound', array(
                    'hl7Counts' => $hl7Counts
                ));
            endif;
            ?>
        </div>
    </div>
    <div class="col-sm-4 col-ms-4">
        <?php
        if ($showClientSwitcher):
            include_partial('client_switcher_widget', array(
                'clients' => $clients,
                'current_client' => $sf_user->getAttribute('client_id', null, 'UserParameters'),
            ));
        endif;
        ?>
    </div>



    <script>
       
        
        $(document).ready(function (event) {
            setup.variables.doctorCount = "<?php echo $userCreatedDoctor; ?>";
            setup.variables.claimCount = "<?php echo url_for('claim/temporaryFormLoop'); ?>";
            setup.events.init();
            $('.quick_report_item').mouseover(function () {
                $(this).addClass('mouseover');
            });
            $('.quick_report_item').mouseout(function () {
                $(this).removeClass('mouseover');
            });
            $('.tablesorter1').tablesorter1({
                widgets: ['zebra']
            });
            $('#edit_all_temporary_forms').button();
            $('#edit_all_temporary_forms').click(function () {
                window.location = "<?php echo url_for('claim/temporaryFormLoop'); ?>";
            });
            Index.init();
        });
    </script>
    <script src="js/Chart.bundle.min.js"></script>
    <script src="js/utils.js"></script>
    <script >
                
        workflowActions.canPrintCopies = <?php echo $can_print_no_of_copy_dialog_box ? 'true' : 'false'; ?>;

        var unprocessedClaims = "<?php echo number_format($auditTrailHealthCounts['unprocessedClaims'], 0); ?>".replace(/\,/g, '');

        var auditTrailHealthCounts = "<?php echo number_format($auditTrailHealthCounts['errorClaims'], 0, '.', ','); ?>".replace(/\,/g, '');

        var unprocessedClaimItems = "<?php echo number_format($auditTrailHealthCounts['unprocessedClaimItems'], 0, '.', ','); ?>".replace(/\,/g, '');

        var errorClaims = "<?php echo number_format($auditTrailHealthCounts['errorClaimItems'], 0, '.', ','); ?>".replace(/\,/g, '');

        var config = {
            type: 'doughnut',
            data: {
                datasets: [{
                        data: [
                            parseInt(unprocessedClaims, 10),
                            parseInt(auditTrailHealthCounts, 10),
                            parseInt(unprocessedClaimItems, 10),
                            parseInt(errorClaims, 10)

                        ],
                        backgroundColor: [
                            window.chartColors.red,
                            window.chartColors.orange,
                            window.chartColors.yellow,
                            window.chartColors.green,
                            window.chartColors.blue,
                        ],
                        label: 'Dataset 1'
                    }],
                labels: [
                    'Unprocessed Claims: ' + parseInt(unprocessedClaims, 10),
                    'Claims With Errors: ' + parseInt(auditTrailHealthCounts, 10),
                    'Unprocessed Claim Items: ' + parseInt(unprocessedClaimItems, 10),
                    'Claim Items With Errors: ' + parseInt(errorClaims, 10)
                ]

            },
            options: {
                responsive: true,

                legend: {
                    position: 'right',
                    labels: {
                        fontSize: 14,
                        fontStyle: 600
                    }
                },
                /*title: {
                 display: true,
                 text: 'Audit Trail Builder Health '
                 },*/
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        };

        window.onload = function () {
            var ctx = document.getElementById('chart-area').getContext('2d');
            window.myDoughnut = new Chart(ctx, config);
        };
    </script>