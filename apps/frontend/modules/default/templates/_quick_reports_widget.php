<div id="reports_widget" class="dashboard_widget quick_reports ">
    <div class="core-box" style="width:100%;border-top: 1px solid #eee;padding-top: 20px;">
        <div class="content">
            <div class="row space12">


                <ul class="mini-stats col-sm-12 errorReportUl skQuickReport">
                    <!-- <li class="col-sm-2">
                        <i class="clip-copy-3 circle-icon circle-bricky"></i>
                        <h4>
                            <span id="quick_reports_spinner" style="display:none"><i class="fa fa-spin fa-refresh"></i></span> 
                            Quick Reports
                        </h4>
                    </li> -->
                    

                    
                    <?php if ($parameters->offsetExists('follow_up_count')): ?>
                        <li class="col-sm-2 quick_report_item cursor" id="patientFollowUpCount">                
                            <div class="sparkline_bar_neutral">
                                <i style="background-color:#ffc443; font-style: normal;" class="circle-icon teal">!</i>
                            </div> 
                            <div class="values">
                                <strong id="dashboard_follow_up_count"><?php echo number_format($parameters['follow_up_count'], 0, '.', ','); ?></strong>
                                <p class="skf14">Follow-Up<!-- patients needing follow-up --></p>
                             </div>
                        </li>
                    <?php endif; ?>
                    <?php if ($parameters->offsetExists('orphan_count')): ?>
                        <li class="col-sm-2 cursor" id="orphanAppointmentsCount">                
                            <div class="sparkline_bar_neutral">
                                <i style="background-color:#82007b;" class="clip-calendar circle-icon teal"></i>
                            </div>
                            <div class="values">
                                <strong  id="dashboard_orphan_count"><?php echo number_format($parameters['orphan_count'], 0, '.', ','); ?></strong>
                                <p class="skf14">Reschedule ! <!-- patient appointments needing rescheduling --></p>
                            </div>
                        </li>
                    <?php endif; ?>
                    <?php if ($parameters->offsetExists('follow_up_appointment_count')): ?>
                        <li class="col-sm-2 cursor" id="appointmentFollowUpCount">                
                            <div class="sparkline_bar_neutral">
                                <i style="background-color:#82007b;" class="clip-calendar circle-icon teal"></i>
                            </div>
                            <div class="values">
                                <strong id="dashboard_follow_up_appointment_count"><?php echo number_format($parameters['follow_up_appointment_count'], 0, '.', ','); ?></strong>
                                <p class="skf14">No-Show ?!<!-- past appointments needing follow-up --></p>
                            </div>
                        </li>
                        <li class="col-sm-2 cursor" id="appointmentAttendedCount">
                            <div class="sparkline_bar_neutral">
                                <i style="background-color:#00b058;" class="clip-calendar circle-icon teal"></i>
                            </div>
                            <div class="values">
                                <strong id="dashboard_attended_appointment_count"><?php echo number_format($parameters['attended_appointment_count'], 0, '.', ','); ?></strong>
                                <p class="skf14">Not Billed ?!<!-- past appointments not billed --></p>
                            </div>
                        </li>
                    <?php endif; ?>
                    <?php if ($parameters->offsetExists('todays_appointment_count')): ?>                
                        <li id="appointment_count_report" class="col-sm-2 cursor">
                            <div class="sparkline_bar_neutral">
                                <i style="background-color:#a5499f;" class="clip-calendar circle-icon teal"></i>
                            </div>
                            <div class="values">
                                <strong id="dashboard_todays_appointment_count"><?php echo number_format($parameters['todays_appointment_count'], 0, '.', ','); ?></strong>
                                <p class="skf14">Today...<!-- doctor appointments scheduled today --></p>
                            </div>
                        </li>
                    <?php endif; ?>
                    <?php if ($parameters->offsetExists('unresolved_claims_count')): ?>                
                        <li class="col-sm-2 cursor" id="unresolved_claims_report">
                            <div class="sparkline_bar_neutral">
                                <i style="background-color:#f50031; font-style: normal;" class="circle-icon teal">$</i>
                                <i  class="queiconset">?</i>
                            </div>
                            <div class="values">
                                <strong id="dashboard_unresolved_claims_count"><?php echo number_format($parameters['unresolved_claims_count'], 0, '.', ','); ?></strong>
                                <p class="skf14"><spam style="text-decoration: underline;">Un</spam>-Resolved<br> 6w To 6m <!-- unresolved claims from 6 months to 6 weeks ago --></p>
                            </div>
                        </li>
                    <?php endif; ?>

                    <?php if ($parameters->offsetExists('unacknowledged_claims_count')): ?>                
                        <li class="col-sm-2 cursor" id="unacknowledged_claims_report">
                            <div class="sparkline_bar_neutral">

                               <i style="background-color:#f50031; font-style: normal;" class="circle-icon teal">$</i>
                               <i class="fa-envelope-o fa" style="position: absolute;margin-left: -47px;color: #FFF;margin-top: 7px;font-size: 40px;font-style: normal;"></i>
                            </div>
                            <div class="values">
                                <strong id="dashboard_unacknowledged_claims_count"><?php echo number_format($parameters['unacknowledged_claims_count'], 0, '.', ','); ?></strong>
                                <p class="skf14"><spam style="text-decoration: underline;">Un</spam>-Acknowledged<br> 7d To 7w<!-- number of unacknowledged and unarchived, batch submissions 7 days to 7 weeks ago --></p>
                            </div>
                        </li>
                    <?php endif; ?>

                    <?php if ($parameters->offsetExists('adjudication_claims_count')): ?>                
                        <li class="col-sm-2 cursor" id="adjudication_claims_report">
                            <div class="sparkline_bar_neutral">
                                <i style="background-color:#f50031; font-style: normal;" class="circle-icon teal">$</i>
                                <i class="queiconset">?</i>
                            </div>
                            <div class="values">
                                <strong id="dashboard_adjudication_claims_count"><?php echo number_format($parameters['adjudication_claims_count'], 0, '.', ','); ?></strong>
                                <p class="skf14">Adjudication...<!-- number of claims parked in adjudication --></p>
                            </div>
                        </li>
                    <?php endif; ?>
                </ul>

                <div class="col-sm-12 hyperdashboardfont"><center>H Y P E M e d i c a l D a s h b o a r d</center></div>
            </div>

            <?php
            if ($parameters->offsetExists('follow_up_count')):
                include_partial('patientFollowUpDialogs', array(
                    'count' => $parameters['follow_up_count'],
                    'form' => $parameters['follow_up_form'],
                ));
            endif;
            ?>

            <?php
            if ($parameters->offsetExists('orphan_count')):
                include_partial('orphanAppointmentsDialog', array(
                    'count' => $parameters['orphan_count'],
                    'form' => $parameters['orphan_refine_form'],
                ));
            endif;
            ?>

            <?php
            if ($parameters->offsetExists('follow_up_appointment_count')):
                include_partial('appointmentFollowUpsDialog', array(
                    'count' => $parameters['follow_up_appointment_count'],
                    'form' => $parameters['follow_up_appointment_refine_form']
                ));

                include_partial('appointmentAttendedDialog', array(
                    'count' => $parameters['attended_appointment_count'],
                    'form' => $parameters['attended_appointment_refine_form']
                ));

                //move back down after dev
                include_partial('unresolvedClaimsDialog', array(
                    'count' => $parameters['unresolved_claims_count'],
                    'form' => $parameters['unresolved_claims_form']
                ));
               
                //move back down after dev
                include_partial('unacknowledgedClaimsDialog', array(
                    'count' => $parameters['unacknowledged_claims_count'],
                    'form' => $parameters['unacknowledged_claims_form']
                ));


                include_partial('adjudicationClaimsDialog', array(
                    'count' => $parameters['adjudication_claims_count'],
                    'form' => $parameters['adjudication_claims_form']
                ));
            endif;
            ?>

            <?php
            if ($parameters->offsetExists('todays_appointment_count')):
                include_partial('todaysAppointmentsDialog', array(
                    'count' => $parameters['todays_appointment_count'],
                    'form' => $parameters['todays_appointment_refine_form']
                ));
            endif;
            ?>

            <?php
            if ($parameters->offsetExists('unresolved_claims_count')):
            //move back after dev
            endif;
            ?>

            <div id="appointmentBookFormDiv" style="display:none;">
                <form id="appointment_book_form" action="<?php echo url_for('appointment_book/index'); ?>" method="post">
                    <input id="appointment_book_patient_id" name="appointment_book[patient_id]" value="" type="hidden" />
                    <input id="appointment_book_appointment_id" name="appointment_book[appointment_id]" value="" type="hidden" />
                    <input id="appointment_book_mode" name="appointment_book[mode]" value="" type="hidden" />
                </form>
            </div>

            <div id="todaysAppointmentsForm" style="display:none;">
                <form id="todays_appts_form" action="<?php echo url_for('appointment_book/index'); ?>" method="post">
                    <input id="todays_appointment_book_patient_id" name="appointment_book[patient_id]" value="" type="hidden" />
                    <input id="todays_appointment_book_appointment_id" name="appointment_book[appointment_id]" value="" type="hidden" />
                    <input id="todays_appointment_book_mode" name="appointment_book[mode]" value="" type="hidden" />
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".skQuickReport li").removeClass('col-sm-2');
        var width = 100 / $(".skQuickReport li").length;
        $(".skQuickReport li").attr('style', 'width:'+width+'%;float:left');
    })
</script>

<style type="text/css">
.hyperdashboardfont
{
    font-family: "Times New Roman", Times, serif;
    font-size:28pt;
    word-spacing: 18pt;
    color: #ffdde2;
    font-style: italic;
    text-decoration: underline;
}
.queiconset
{
    font-size: 12px;
    position: absolute;
    margin-left: -21px;
    margin-top: 6px;
    color: #FFF; 
    font-style: normal;
}

</style>