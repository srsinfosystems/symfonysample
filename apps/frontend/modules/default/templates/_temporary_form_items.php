
<div class="panel-heading">
    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent=".myaccordion" href="#collapse10" class="weightHeadings accordion-toggle" >
            <div class="heading dashboard_widget"  id="exportsWidget">
                <i class="clip-data circle-bricky circle-icon"></i>
                Temporary Data (<?php echo count($items); ?> of <?php echo $temporary_form_count; ?>)
            </div>
        </a>
    </h4>
</div>

<div id="collapse10" class="panel-collapse collapse">
    <div class="panel-body">
        <div class="col-sm-12 col-md-12">
            <div id="reports_widget" class="dashboard_widget quick_reports">
                <div class="core-box" style="width:100%">            
                    <div class="content" id="temporary_form_items" >
                        <div class="row">
                        <div class="col-sm-6 col-md-6">
                        <?php if ($admin): ?>
                        <form class="form-inline">
                            <div class="form-group">
                                <label for="email">Filter user:</label>
                                <select class="form-control Select2" style="width:200px;flote:left" name="filter_user" id="filter_user">
                                    <option value="0">All Users</option>
                                    <?php foreach ($users as $user): ?>
                                        <option value="<?php echo $user->id; ?>"><?php echo $user->first_name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>        
                        </form>
                        <br/>
                        <?php endif; ?>
                        </div>
                        <div class="col-sm-6 col-md-6">
                        <div style="margin-top: 25px;"> 
                            <button type="button" id="edit_all_temporary_forms" class="btn btn-info btn-squared"/><i class="clip-pencil-2"></i> Edit All Claims</button>
                            <a class="btn btn-blue btn-squared" href="/temporary_form/search"><i class="clip-search-3"></i> Search</a>
                        </div>
                        </div>
                        </div>
                        <hr/>
                        <ul class="temporary_list" style="max-height:120px;overflow: auto">
                            <?php foreach ($items as $item): ?>
                            <li id="temporary_item_<?php echo $item->id; ?>" class="alert alert-info" style="list-style-type:none !important">
                                    <a href="<?php echo url_for(strtolower($item->form_type) . '/temporary_form?id=' . $item->id); ?>">
                                        <?php echo nl2br($item->getDashboardData("\n")); ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<script>
    $(document).ready(function () {
        temporaryForm.actions.pullData = "<?php echo url_for('default/pullTemporaryFormDataFiltered'); ?>";
        temporaryForm.actions.deleteAll = "<?php echo url_for('temporary_form/deleteAll'); ?>";
        temporaryForm.methods.init();
    });
</script>
