<div class="panel-heading">
  <h4 class="panel-title">
    <a data-toggle="collapse" data-parent=".myaccordion" href="#collapse4" class="weightHeadings accordion-toggle">
        <div class="heading dashboard_widget">
        <i style="background-color:purple" class="clip-calendar circle-icon teal"></i> 
        Bill Appointments <span id="bill_appointments_spinner" style="display:none"><i class="fa fa-spin fa-refresh"></i></span>
        </div>
    </a>
  </h4>
</div>
<div id="collapse4" class="panel-collapse collapse">
    <div class="panel-body ">
        <div class="col-sm-12 col-md-12">
         
        <p style="color:#858585;margin-left: 60px;margin-top: -10px;">
            <small>All appointments that match your search criteria will be returned for you to select from.</small>
        </p>
        <hr>
        <form method="post" id="create_claims_form" role="form">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">
                            <?php echo $form['doctor_id']->renderLabel(); ?>
                        </label>
                        <?php echo $form['doctor_id']->render(['class' => 'form-control Select2']); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">
                            <?php echo $form['location_id']->renderLabel(); ?>
                        </label>
                        <select class="form-control Select2" id="bill_appointments_location_id" name="bill_appointments[location_id]">
                            <option value="" <?php echo $current_loc == null ? 'selected="selected"' : null; ?>>[<?php echo 'All ' . $sf_user->getLocationTitle() . 's'; ?>]</option>

                            <!-- <?php foreach ($sf_user->getLocations() as $loc_id => $loc_name): ?>
                                <option value="<?php echo $loc_id; ?>" <?php echo $loc_id == $current_loc ? 'selected="selected"' : null; ?>><?php echo $loc_name; ?></option>
                            <?php endforeach; ?>
                             -->
                            <?php foreach ($locationsData as $loc_id => $loc): ?>
                                <option value="<?php echo $loc->id; ?>" <?php echo $loc->id == $current_loc ? 'selected="selected"' : null; ?>><?php echo $loc->code.' - ' .$loc->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php //echo $form['location_id']->render(['class' => 'form-control Select2']); ?>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">
                            <?php echo $form['status']->renderLabel(); ?>
                        </label>
                        <?php echo $form['status']->render(['class' => 'form-control Select2']); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label">
                                    <?php echo $form['start_date']->renderLabel(); ?>
                                </label>
                                <?php echo $form['start_date']->render(['class' => 'form-control']); ?>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">
                                    <?php echo $form['end_date']->renderLabel(); ?>
                                </label>
                                <?php echo $form['end_date']->render(['class' => 'form-control']); ?>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label"></label>
                        <button id="billAppointmentsButton" class="btn btn-blue btn-block" type="button">
                            <i class="clip-search-3"></i> Search
                        </button>            
                    </div>

                </div>
            </div>
            <?php echo $form->renderHiddenFields(); ?>
        </form>
    </div>
    </div>
</div>
<div id="billAppointmentsDialog" style="display:none;" title="Billable Appointments"></div>


<script type="text/javascript">
    $(document).ready(function () {
        $("#bill_appointments_status option[value='all'], #bill_appointments_status option[value='20']").remove();
        billAppointments.actions.search = '<?php echo url_for('appointment_search/billableAppointmentsSearch'); ?>';
        billAppointments.actions.setupBillings = '<?php echo url_for('claim/setupAppointmentBillings'); ?>';
        billAppointments.initialize();
    });
</script>
