
<div class="row space12 hl7widget">
    <ul class="mini-stats col-sm-12 errorReportUl">
        <li class="col-sm-2">
            <i class="fa fa-envelope-o circle-icon circle-bricky "></i>
            <h4>
                <span id="hl7_spinner" style="display:none"><i class="fa fa-spin fa-refresh"></i></span> 
                HL7 Messaging<br/>Last 30 Days
            </h4>
        </li>

        <?php foreach ($hl7Counts as $h): ?>
            <li class="quick_report_item cursor" id="hl7_inbound_<?php echo str_replace(' ', '_', strtolower($h['status_name'])); ?>">
                <div class="sparkline_bar_neutral">
                    <span>4,6,10,8,12,21,11</span><?php echo number_format($h['status_count'], 0, '.', ','); ?>
                </div>
                <div class="values">
                    <strong><?php echo number_format($h['status_count'], 0, '.', ','); ?> </strong>
                    <p class="skf14"><?php echo $h['status_name']; ?></p>

                    <small><i><?php echo $h['status_description']; ?> </i></small>
                </div>
            </li>            
        <?php endforeach; ?>
    </ul>
    <small class="text-muted sk-padding-large"><i>These Messages have yet to be acknowledged. Once acknowledged, they will be removed from this widget.</i></small>
    <hr/>
</div>


<div id="hl7_processed_dialog" title="HL7 Messaging - Processed Successfully" style="display: none">

    <span id="hl7_processed_messages_text" class="right" style="padding-right: 5px;"></span>

    <h3><span id="hl7_processed_spinner" style="display:none"><i class="fa fa-spin fa-refresh"></i></span>  Processed Successfully Summary</h3>

    <small>
        <?php echo $hl7Counts[HL7InboundTable::STATUS_PROCESSED]['status_description']; ?>
    </small>

    <div id="hl7_processed_data"></div>
</div>


<div id="hl7_duplicate_dialog" title="HL7 Messaging - Duplicate Messages" style="display: none">
    <div class="icons right" style="padding-left: 5px; padding-right: 5px;">
        <div id="hl7_duplicate_spinner" style="display: none">
            <?php echo image_tag('icons/spinner.gif'); ?>
        </div>
        <div id="hl7_duplicate_checkmark" style="display: none">
            <?php echo image_tag('icons/checkmark.png', array('height' => '16')); ?>
        </div>
        <div id="hl7_duplicate_error_icon" style="display: none">
            <?php echo image_tag('icons/error.png', array('height' => '16')); ?>
        </div>
    </div>
    <span id="hl7_duplicate_messages_text" class="right" style="padding-right: 5px;"></span>

    <h3>Duplicate Messages Summary</h3>

    <p>
        <?php echo $hl7Counts[HL7InboundTable::STATUS_DUPLICATE]['status_description']; ?>
    </p>

    <div id="hl7_duplicate_data"></div>
</div>

<div id="hl7_form_error_dialog" title="HL7 Messaging - Form Errors" style="display: none">
    <div class="icons right" style="padding-left: 5px; padding-right: 5px;">
        <div id="hl7_form_error_spinner" style="display: none">
            <?php echo image_tag('icons/spinner.gif'); ?>
        </div>
        <div id="hl7_form_error_checkmark" style="display: none">
            <?php echo image_tag('icons/checkmark.png', array('height' => '16')); ?>
        </div>
        <div id="hl7_form_error_error_icon" style="display: none">
            <?php echo image_tag('icons/error.png', array('height' => '16')); ?>
        </div>
    </div>
    <span id="hl7_form_error_messages_text" class="right" style="padding-right: 5px;"></span>

    <h3>Messages with Errors Summary</h3>

    <p>
        <?php echo $hl7Counts[HL7InboundTable::STATUS_FORM_ERROR]['status_description']; ?>
    </p>

    <div id="hl7_form_error_data"></div>
</div>

<div id="hl7_details_dialog" title="HL7 Messaging - Details" style="display: none">
    <div class="icons right" style="padding-left: 5px; padding-right: 5px;">
        <div id="hl7_details_spinner" style="display: none">
            <?php echo image_tag('icons/spinner.gif'); ?>
        </div>
        <div id="hl7_details_checkmark" style="display: none">
            <?php echo image_tag('icons/checkmark.png', array('height' => '16')); ?>
        </div>
        <div id="hl7_details_error_icon" style="display: none">
            <?php echo image_tag('icons/error.png', array('height' => '16')); ?>
        </div>
    </div>
    <span id="hl7_details_messages_text" class="right" style="padding-right: 5px;"></span>

    <h3>HL7 Messaging Details</h3>

    <div id="hl7_details_data"></div>

    <button type="button" name="acknowledge_all_details" id="acknowledge_all_details"><i class="clip-checkmark-circle"></i> Mark All Messages Acknowledged</button>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        detailContextMenu.actions.claimDetails = '<?php echo url_for('claim/details'); ?>';
        errorDetailContextMenu.actions.editClaim = '<?php echo url_for('claim/temporary_form'); ?>';

        hl7_inbound.actions.acknowledgeByIds = '<?php echo url_for('hl7Inbound/acknowledgeByIds'); ?>';
        hl7_inbound.actions.acknowledgeGroup = '<?php echo url_for('hl7Inbound/acknowledgeGroup'); ?>';
        hl7_inbound.actions.duplicate = '<?php echo url_for('hl7Inbound/duplicateData'); ?>';
        hl7_inbound.actions.form_error_data = '<?php echo url_for('hl7Inbound/formErrorData'); ?>';
        hl7_inbound.actions.processed = '<?php echo url_for('hl7Inbound/processedData'); ?>';
        hl7_inbound.actions.details = '<?php echo url_for('hl7Inbound/details'); ?>';
        hl7_inbound.initialize();

    });
</script>
<script>
    $(document).ready(function () {
        $(".hl7widget li").removeClass('col-sm-2');
        var width = 100 / $(".hl7widget li").length;
        $(".hl7widget li").attr('style', 'width:' + width + '%;float:left');
    })
</script>
<style>
    .hl7widget{
        color: #444444;
    }
    .hl7widget h3{
        font-size:18px;
    }
    .hl7widget li{
        padding: 5px;
    }
</style>