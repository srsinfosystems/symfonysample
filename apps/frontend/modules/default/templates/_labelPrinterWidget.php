
<div class="panel-heading">
    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent=".myaccordion" href="#collapse8" class="weightHeadings accordion-toggle" >
            <div class="heading dashboard_widget">
                <i class="fa fa-print circle-bricky circle-icon"></i>
                Select A Label Printer
            </div>
        </a>
    </h4>
</div>

<div id="collapse8" class="panel-collapse collapse">
    <div class="panel-body">
     <div class="col-sm-12 col-md-12">
    <div id="reports_widget" class="dashboard_widget quick_reports">
        <div class="core-box">
            <div id="labelPrinterWidget" class="content">
                <div id="printer_messages" class="right">
                    <span id="printer_spinner" style="display:none"><?php echo image_tag('icons/spinner.gif'); ?></span>
                    <span id="printer_checkmark" style="display:none"><?php echo image_tag('icons/checkmark.png', array('height' => '16')); ?></span>
                    <span id="printer_error_icon" style="display:none"><?php echo image_tag('icons/error.png', array('height' => '16')); ?></span>
                    <span id="printer_messages_text" class="right" style="padding-right: 5px;"></span>
                </div>

                <div class="form-group">
                    <label class="control-label">
                        Printer
                    </label>
                    <div class="row">
                        <div class="col-sm-9 col-md-9">
                            <select id="direct_print_select" class="form-control">
                        <option value="">[Select A Printer]</option>
                        <?php foreach ($printers as $id => $value): ?>
                            <option value="<?php echo $id; ?>" <?php echo ($id == $default) ? 'selected="selected"' : "" ?>><?php echo $value; ?></option>
                        <?php endforeach; ?>
                    </select>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <button type="button" value="Set Printer" class="btn-blue btn btn-squared" id="select_printer"><i class="fa-print fa"></i> Set Printer</button>
                        </div>
                    </div>
                         
                </div>
                
            </div>
        </div>
    </div>
</div>
</div>
</div>



<script type="text/javascript">
    $(document).ready(function () {
        $('#select_printer').button();
        $('#select_printer').click(function () {
            $.ajax({
                type: 'post',
                url: '<?php echo url_for('default/setPrinter'); ?>',
                dataType: "json",
                data: 'printer=' + $('#direct_print_select').val(),

                success: function (rs) {
                    $('#printer_checkmark').show();
                    $('#printer_messages_text').text('Set Printer');
                },
                beforeSend: function () {
                    $('#printer_messages .icons div').hide();
                    $('#printer_spinner').show();

                    $('#printer_messages_text').text('Setting Printer');
                },
                complete: function () {
                    $('#printer_spinner').hide();
                },
                error: function () {
                    $('#printer_error_icon').show();
                    $('#printer_messages_text').text('An error occurred while contacting the server.  Please refresh the page and try again.');
                }
            });
        });
    });
</script>
