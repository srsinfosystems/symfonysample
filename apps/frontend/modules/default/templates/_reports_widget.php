
<div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent=".myaccordion" href="#collapse5" class="weightHeadings accordion-toggle" >
            <div class="heading dashboard_widget">
                <i class="clip-book circle-icon circle-bricky"></i> 
                Appointment Book Reports <span id="apt_report_spinner" style="display:none"><i class="fa fa-spin fa-refresh"></i></span>
            </div>
            </a>
      </h4>
</div>

<div id="collapse5" class="panel-collapse collapse">
    <div class="panel-body">
        <form id="mdcAppointmentBookForm">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">
                            <?php echo $form['report_type']->renderLabel(); ?>
                        </label>
                        <?php echo $form['report_type']->render(['class' => 'form-control Select2']); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">
                            <?php echo $form['email_doctor']->render(); ?> <?php echo $form['email_doctor']->renderLabel(); ?>
                        </label>
                        <small class="text-muted"><?php echo $form['email_doctor']->renderHelp() ?></small>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">
                            <?php echo $form['doctor_id']->renderLabel(); ?>
                        </label>
                        <?php echo $form['doctor_id']->render(['class' => 'form-control Select2']); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">
                            <?php echo $form['location_id']->renderLabel(); ?>
                        </label>
                        <?php echo $form['location_id']->render(['class' => 'form-control Select2']); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label">
                                    <?php echo $form['start_date']->renderLabel(); ?>
                                </label>
                                <?php echo $form['start_date']->render(['class' => 'form-control sk-datepicker']); ?>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">
                                    <?php echo $form['end_date']->renderLabel(); ?>
                                </label>
                                <?php echo $form['end_date']->render(['class' => 'form-control sk-datepicker']); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">
                            <label>Generate Appointment book Report</label>
                        </label>
                        <button class="btn btn-blue btn-block btn-squared" id="appointment_book_submit" value="Generate" type="button"><i class="clip-checkbox-checked-2"></i> Generate</button>
                    </div>
                </div>

            </div>

            <i><b class="text-muted">Report Types:</b></i>
            <ul class="text-muted small">
                <li><i>Doctor Profile Summary:</i> Summary for a day of each doctor profile's appointments.  A summary is returned to the user in a text file. Optionally, you can email each doctor with a list of their individual appointments.</li>
                <li><i>Appointments:</i> Spreadsheet of appointments for the provided parameter set.  Optionally, you can email each doctor with a copy of the attachment tailored just for them.</li>
            </ul>
            <?php echo $form->renderHiddenFields(); ?>

        </form>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('#appointment_book_submit')
                .click(function () {
                    $("#apt_report_spinner").show();
                    setTimeout(function(){
                        $("#apt_report_spinner").hide();
                    },1000);
                    var data = $('#mdcAppointmentBookForm').serialize();
                    downloadURL('<?php echo url_for('appointment_search/downloadAppointmentReport'); ?>?' + data);
                });
    });

    function downloadURL(url) {
        var iframe = $('#hiddenDownloader');
        if (iframe.size() == 0) {
            iframe = $('<iframe id="hiddenDownloader" style="display:none;" />');
            $('body').append(iframe);
        }
        iframe.attr('src', url);
    }
</script>
