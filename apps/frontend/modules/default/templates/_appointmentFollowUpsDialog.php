<div id="followUpAppointmentDialog" style="display:none" title="Past Appointments Needing Follow-Up">
<div class="panel panel-default">
        <div class="panel-heading expand">
            <i class="clip-search-3"></i>
            Refine Your Search <span id="follow_up_appts_spinner" style="display: none;"><i class="fa fa-spin fa-refresh"></i></span>
        </div>
        <div class="panel-body buttons-widget" style="overflow: hidden; display: none;">
    <form id="followUpAppointmentsForm">
        <?php echo $form->renderHiddenFields(); ?>
        <div id="follow_up_refine" class="form-table row">
            <div class="col-sm-3 col-xs-6">
                <div class="form-group">
                    <label class="control-label">
                        <?php echo $form['start_date']->renderLabel(); ?>
                    </label>
                    <?php echo $form['start_date']->render(['class' => 'form-control btn-squared']); ?>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group">
                    <label class="control-label">
                        <?php echo $form['location_id']->renderLabel(); ?>
                    </label>
                    <?php echo $form['location_id']->render(['class' => 'form-control Select2']); ?>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group">
                    <label class="control-label">
                        <?php echo $form['doctor_id']->renderLabel(); ?>
                    </label>
                    <?php echo $form['doctor_id']->render(['class' => 'form-control Select2']); ?>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group" style="margin-top:30px;">

                    <button type="button" class="btn btn-blue btn-squared" id="appointmentFollowUpRefineSubmit" ><i class="clip-search-3"> </i> Search</button>  
                <button type="button" class="btn btn-danger btn-squared" id="appointmentFollowUpRefineClear"><i class="clip-file-minus"> </i> Clear</button>  


                </div>
            </div>
        </div>
    </form>
        </div>
</div>
    <h4>Displaying <span id="follow_up_appt_result_count"></span> <span id="follow_up_appt_total_dialog"><?php echo number_format($count, 0, '.', ','); ?></span> possible appointments</h4>
    <p style="color:#858585;font-size:12px">These appointments need to be followed up on. The appointment should be marked as either <?php echo AppointmentTable::getStatusString(AppointmentTable::STATUS_ATTENDED); ?> or <?php echo AppointmentTable::getStatusString(AppointmentTable::STATUS_NO_SHOW); ?>
        <br />Maximum number of results: 500</p>

    <form id="follow_up_update_form">
        <table id="appointmentFollowUpsTable" class="sk-datatable table-striped  table table-hover table-striped"></table>
        <div class="custActionBtnsappointmentFollowUpsTable">
            <button class="btn btn-info btn-squared" id="appointmentFollowUpSubmit" type="button"><i class="clip-pencil-2"></i> Update Statuses</button>
            <button class="btn btn-teal btn-squared" id="appointmentSelectAllRadio" type="button"><i class="clip-list"></i> Select All</button>
        </div>        
    </form>


</div>

<script type="text/javascript">
    $(document).ready(function () {
        appointmentFollowUp.actions.search = '<?php echo url_for('appointment_search/appointmentFollowUpReport'); ?>';
        appointmentFollowUp.actions.attended_search = '<?php echo url_for('appointment_search/appointmentAttendedReport'); ?>';
        appointmentFollowUp.profile = '<?php echo url_for('patient/view'); ?>';
        appointmentFollowUp.actions.dashboardlocationwisecount = '<?php echo url_for('default/LocationWiseCount'); ?>';
        appointmentFollowUp.initialize();
    });
</script>