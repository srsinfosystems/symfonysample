
<div class="navbar-content">
    <!--start: SIDEBAR--> 
    <div class="main-navigation navbar-collapse collapse" style="position:fixed; -webkit-transition: all 0.4s ease;-o-transition: all 0.4s ease;transition: all 0.4s ease;">
        <!--start: MAIN MENU TOGGLER BUTTON--> 
        <div class="navigation-toggler">
            <i class="clip-arrow-left"></i>
            <i class="clip-arrow-right"></i>
        </div>
        <ul class="main-navigation-menu">
            <?php
            $menu_icons = array(
                'Home' => 'clip-home',
                'Patients' => 'clip-users-3',
                'Appt Book' => 'clip-book',
                'Claims' => 'fa fa-briefcase',
                'Doctor Profiles' => 'fa fa-user-md',
                'SysAdmin' => 'clip-cog',
                'Messages' => 'clip-bubbles-2',
                'Log out' => 'fa fa-sign-out'
            );
            $i = 0;
            foreach ($menu as $id => $item): //print_r($id);  ?>
                <li class="skmainmenu <?php echo $item['selected'] ? 'active' : '' ?>">
                    <?php if ($submenu->offsetExists($id)) { ?>

                        <a id="a_<?= $id ?>" href="<?php echo url_for($item['url']); ?>"><i class="<?= $menu_icons[$item['text']] ?>"></i>
                            <span class="title"><?php echo $item['text']; ?> </span>
                            <!--<i onclick="return ShowHideMenu('a_<?= $id ?>')" class="icon-arrow"></i>-->
                            <span class="selected"></span>
                        </a>
                        <ul class="<?php echo ($menu[$id]['selected']) ? ' active' : ''; ?> sub-menu submenuautoscroll" id="submenu_<?php echo $id; ?>">
                            <?php foreach ($submenu[$id] as $item): ?>
                                <li class="<?php echo ($item['selected'] == 1) ? 'active' : ''; ?>">
                                    <a href="<?php echo url_for($item['url']); ?>">
                                        <span class="title"> <?php echo $item['text']; ?> </span>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php }else { ?>
                        <a href="<?php echo url_for($item['url']); ?>"><i class="<?= $menu_icons[$item['text']] ?>"></i>
                            <span class="title"> <?php echo $item['text']; ?> </span>
                            <span class="selected"></span>
                        </a>
                    <?php } ?>
                </li>
                <?php
                $i++;
            endforeach;
            ?>
        </ul>
    </div>
</div>


<style type="text/css">
@media (max-width:1344px) {
    .submenuautoscroll
    {
        height:250px !important; 
        overflow-y: auto !important;
    }
}
.submenuautoscroll
{
    overflow-y: auto;
}
</style>
<script>

//    $(document).ready(function () {
//        $('.main-navigation-menu > li a').hover(function () {
////            if($(this).parent().children('ul').hasClass('sub-menu')){
////                wait(200);
////            }
//            
//            if (!$(this).parent().hasClass('open')) {
//                
//                $(this).parent().addClass('open');
//                $(this).parent().parent().children('li.open').not($(this).parent())/*.not($('.main-navigation-menu > li.active'))*/.removeClass('open').children('ul').hide();
//                $(this).parent().children('ul').slideDown(200, function () {
//                    
//                    runContainerHeight();
//                    
//                });
//            }
//
//        })
//        
//        $(".main-navigation-menu").hover(function(){},function(){ $('ul.sub-menu').hide();})
//        $('ul.sub-menu').hide();
//    })

    function wait(ms) {
        var start = new Date().getTime();
        var end = start;
        while (end < start + ms) {
            end = new Date().getTime();
        }
    }
//function to adapt the Main Content height to the Main Navigation height
    var runContainerHeight = function () {
        mainContainer = $('.main-content > .container');
        mainNavigation = $('.main-navigation');
        if ($pageArea < 760) {
            $pageArea = 760;
        }
        if (mainContainer.outerHeight() < mainNavigation.outerHeight() && mainNavigation.outerHeight() > $pageArea) {
            mainContainer.css('min-height', mainNavigation.outerHeight());
        } else {
            mainContainer.css('min-height', $pageArea);
        }
        ;
        if ($windowWidth < 768) {
            mainNavigation.css('min-height', $windowHeight - $('body > .navbar').outerHeight());
        }
    };
</script>