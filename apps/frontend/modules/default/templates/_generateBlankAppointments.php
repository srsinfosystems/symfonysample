<div id="reports_widget" class="dashboard_widget quick_reports panel panel-default">
    <div class="panel-body ">
        <h2 style="color:#444444">
            <i class="clip-calendar circle-bricky circle-icon"></i>
            Generate Blank Appointments <span id="generateAppointments_spinner" style="display:none"><i class="fa fa-spin fa-refresh"></i></span>
        </h2>
        <p style="color:#858585;margin-left: 60px;margin-top: -10px;">
            <small>Create new blank appointments.</small>
        </p>
        <hr>
        <div class="heading dashboard_widget"  id="exportsWidget">
            <div id="generateAppointments" class="content generateAppointments">
                <form id="generateBlankAppointmentsForm">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">
                                    <?php echo $form['doctor_id']->renderLabel(); ?>
                                </label>
                                <?php echo $form['doctor_id']->render(['class' => 'form-control Select2']); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">
                                    <?php echo $form['location_id']->renderLabel(); ?>
                                </label>
                                <?php echo $form['location_id']->render(['class' => 'form-control Select2']); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">
                                    <?php echo $form['number_of_appointments']->renderLabel(); ?>
                                </label>
                                <?php echo $form['number_of_appointments']->render(['class' => 'form-control Select2']); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">
                                    <?php echo $form['date']->renderLabel(); ?>
                                </label>
                                <?php echo $form['date']->render(['class' => 'form-control']); ?>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"></label>
                                <button id="generateAppointmentsButton" class="btn btn-blue btn-block" type="button">
                                    <i class="clip-paperplane"></i> Generate
                                </button>            
                            </div>
                            <?php if ($unusedCount): ?>
                                <input type="button" name="deleteGeneratedAppointments" id="deleteGeneratedAppointments" value="Delete Unused Past Appointments & Patients" />
                                <?php echo $unusedCount; ?> unused past appointments found
                            <?php endif; ?>
                            <div id="generateAppointmentsErrorMessages" style="display:none"></div>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        generate.actions.deletePastGeneratedAppointments = '<?php echo url_for('appointment_search/deletePastGeneratedAppointments'); ?>';
        generate.actions.validate = '<?php echo url_for('appointment_search/generateFormValidate'); ?>';
        generate.initialize();
    });
</script>
