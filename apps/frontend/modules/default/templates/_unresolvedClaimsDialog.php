<div id="unresolvedClaimsDialog" style="display:none" title="Unresolved claims from past 6 months">

    <p style="color:#858585;font-size:12px">The claims below cover a date range of 6 months to 6 weeks ago (6m-6w). These claims have not been marked as PAID or CLOSED (thus Unresolved), and are approaching their stale date.</p>
    <a target="_blank" href="/past_claim?ref=desktop&t=unresolved">
        <h4>Displaying <span id="unresolved_claims_total_dialog"><?php echo number_format($count, 0, '.', ','); ?></span> possible claims <span id="follow_up_appts_spinner" style="display:none"><i class="fa fa-spin fa-refresh"></i></span></h4>
    </a>
    

    <form id="unresolved_claims_update_form">
        <table id="unresolvedClaimsTable" class="sk-datatable table-striped table table-hover table-striped">
        </table>
    </form>

</div>

<script type="text/javascript">
    $(document).ready(function() {
        unresolvedClaims.actions.search = '<?php echo url_for('past_claim/unresolvedClaimsReport'); ?>';
        unresolvedClaims.profile = '<?php echo url_for('patient/view'); ?>';
        unresolvedClaims.initialize();
    });
</script>