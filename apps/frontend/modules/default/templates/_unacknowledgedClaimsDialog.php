<div id="unacknowledgedClaimsDialog" style="display:none" title="The batch submissions below cover a date range of 7 days to 7 weeks ago">

    <p style="color:#858585;font-size:12px">The batch submissions below cover a date range of 7 days to 7 weeks ago (7d-7w). The claims within these batch submissions, though unacknowledged with a batch edit report, may be PAID. Click on a batch submission record to check the status of the claims therein. If the claims are Paid then Archive the batch submission record. If the claims are still in the SUBMITTED status then Resend EDIT with the red action arrow.</p>
    <!-- <a target="_blank" href="/past_claim?ref=desktop&t=unsubmitted">
        <h4>Displaying <span id="unacknowledged_claims_total_dialog"><?php echo number_format($count, 0, '.', ','); ?></span> possible claims <span id="follow_up_appts_spinner" style="display:none"><i class="fa fa-spin fa-refresh"></i></span></h4>
    </a> -->
    

    <form id="unacknowledged_claims_update_form">
        <table id="unacknowledgedClaimsTable" class="sk-datatable table-striped table table-hover table-striped">
        </table>
    </form> 
</div>

<script type="text/javascript">
    $(document).ready(function() {
        unacknowledgedClaims.actions.search = '<?php echo url_for('past_claim/unacknowledClaimsReport'); ?>';
        unacknowledgedClaims.profile = '<?php echo url_for('patient/view'); ?>';
        unacknowledgedClaims.initialize();
    });

    $('#unacknowledgedClaimsTable').on( 'click', 'tbody tr', function () {
       var myTD = parseInt($(this).find("td:first").text());
       if($.isNumeric(myTD))
       {
            window.open('ohip?batch_id='+myTD+'', '_blank');
       }
    });
</script>

<style type="text/css">
#unacknowledgedClaimsTable tbody tr td
{
    color: #428bca !important;
}  
#unacknowledgedClaimsTable tbody tr td .dataTables_empty
{
    color: #000 !important;
} 

#unacknowledgedClaimsTable tbody tr .dataTables_empty:hover
{
    cursor:context-menu !important;
}  

#unacknowledgedClaimsTable tbody tr:hover
{
    cursor:pointer !important;
}  

</style>