
<div class="row">
    <div class="col-sm-12">
        <hr/>
        <div class="heading dashboard_widget">
            <i class="fa-envelope-o fa circle-icon circle-bricky"></i>
            <span id="quick_reports_spinner" style="display:none"><i class="fa fa-spin fa-refresh"></i></span> 
            Email Reminders    
        </div> 
        <br/>
    </div>


    <div class="col-sm-4 text-center">
        <div class="alert alert-info" style="min-height:130px">
            <h3 style="font-weight: bold;font-size:32px;margin-top:5px;" > 
                <?php echo number_format($templateCount, 0, '.', ','); ?>
            </h3>
            <span style="font-size:16px;font-weight: bold">Active Reminder Templates</span>
        </div>            
    </div>
    <div class="col-sm-4 text-center">
        <div class="alert alert-danger" style="min-height:130px">
            <h3 style="font-weight: bold;font-size:32px;margin-top:5px;" > 
                <?php echo number_format($templateUnprocessedCount, 0, '.', ','); ?>
            </h3>
            <span style="font-size:16px;font-weight: bold">Queued Reminders</span>
        </div>            
    </div>
    <div class="col-sm-4 text-center">
        <div class="alert alert-success" style="min-height:130px">
            <h3 style="font-weight: bold;font-size:32px;margin-top:5px;" > 
                <?php echo number_format($templateEmailsSentCount, 0, '.', ','); ?>
            </h3>
            <span style="font-size:16px;font-weight: bold">Emails sent since yesterday</span>
        </div>            
    </div>
    
</div>
<br/>