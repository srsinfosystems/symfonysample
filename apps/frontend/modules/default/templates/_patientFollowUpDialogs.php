<div id="patientFollowUpDialog" style="display:none" title="Patients Requiring Follow-Up">
        <h4>
        <span id="follow_up_spinner" style="display:none"><i class="fa fa-spin fa-refresh"></i></span> Displaying <span id="patient_follow_up_count"></span> of
        <span id="patient_follow_up_total_count"><?php echo number_format($count, 0, '.', ','); ?></span> Patients Requiring Follow-Ups
    </h4>

    <table id="patientFollowUpResults" class="sk-datatable table table-hover table-striped">
    </table>

    <div id="patientFollowUpFormDialog" style="display:none" title="Mark Item As Completed">
        <div class="content_header">
            <div id="follow_up_form_messages" class="right">
                <div class="icons right" style="padding-left:5px;">
                    <div id="follow_up_form_spinner" style="display:none"><?php echo image_tag('icons/spinner.gif'); ?></div>
                    <div id="follow_up_form_checkmark" style="display:none"><?php echo image_tag('icons/checkmark.png', array('height' => '16')); ?></div>
                    <div id="follow_up_form_error_icon" style="display:none"><?php echo image_tag('icons/error.png', array('height' => '16')); ?></div>
                </div>
                <span id="follow_up_form_messages_text" class="right"></span>
            </div>
        </div>

        <br clear="all" />
        <form id="follow_up_form">
            <table class="form-table">
                <div class="form-group">
                    <label class="control-label">
                        <?= $form['completion_notes']->renderLabel() ?>
                    </label>
                    <?= $form['completion_notes']->render() ?>
                </div>
                <?php echo $form->renderHiddenFields(); ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <button class="btn btn-primary btn-squared" type="button" id="patientFollowUpFormSubmit"><i class="clip-checkbox"></i> Mark Reminder As Completed</button>
                        <button class="btn btn-danger btn-squared" type="button" id="patientFollowUpFormClose"><i class="clip-cancel-circle"></i> Close Without Saving</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        patientFollowUps.actions.markComplete = '<?php echo url_for('follow_up/update'); ?>';
        patientFollowUps.actions.search = '<?php echo url_for('follow_up/report'); ?>';
        patientFollowUps.actions.profile = '<?php echo url_for('patient/index'); ?>';
        patientFollowUps.initialize();
    });
</script>