<div id="adjudicationClaimsDialog" style="display:none" title="Number of claims parked in adjudication">

    <p style="color:#858585;font-size:12px">Number of claims parked in adjudication.</p>
     <a target="_blank" href="/past_claim?ref=desktop&t=adjudication">
        <h4>Displaying <span id="adjudication_claims_total_dialog"><?php echo number_format($count, 0, '.', ','); ?></span> possible claims <span id="follow_up_appts_spinner" style="display:none"><i class="fa fa-spin fa-refresh"></i></span></h4>
    </a>
    

    <form id="adjudication_claims_update_form">
        <table id="adjudicationClaimsTable" class="sk-datatable table-striped table table-hover table-striped">
        </table>
    </form>

</div>

<script type="text/javascript">
    $(document).ready(function() {
        adjudicationClaims.actions.search = '<?php echo url_for('past_claim/adjudicationClaimsReport'); ?>';
        adjudicationClaims.profile = '<?php echo url_for('patient/view'); ?>';
        adjudicationClaims.initialize();
    });
</script>