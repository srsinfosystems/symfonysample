
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a class="accordion-toggle weightHeadings" data-toggle="collapse" data-parent=".myaccordion" href="#collapse3" >
                        <div class="heading dashboard_widget"  id="exportsWidget">
                            <i class="clip-pencil-3 circle-bricky circle-icon"></i>
                            Content Style Settings
                        </div>
                    </a>
                  </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse">
                  <div class="panel-body">
                    <div id="reports_widget" class="dashboard_widget quick_reports">
                    <div class="core-box">
                        <!-- <div class="heading dashboard_widget"  id="exportsWidget">
                            <i class="clip-pencil-3 circle-bricky circle-icon"></i>
                            <h2>Content Style Settings</h2>
                        </div> -->
                        <div class="content">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="more-input-group">Select text color.</label>
                                        <div class="input-group">
                                            <span title="Reset color" style="cursor:pointer" id="resetColor" class="input-group-addon">Reset Color</span>
                                            <input type="text" id="text_color" class="form-control" value="#444444"/>                                        
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="more-input-group">Select text size.</label>
                                        <div id="font-setting-buttons">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default decrease-me"><i class="glyphicon glyphicon-minus"></i> Decrease</button>
                                                <button type="button" class="btn btn-default reset-me"><i class="glyphicon glyphicon-font"></i> Normal</button>
                                                <button type="button" class="btn btn-default increase-me"><i class="glyphicon glyphicon-plus"></i> Increase</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <span class="input-group-btn">
                                        <button id="contentUpdateBtn" class="btn btn-blue" type="button">Save</button>
                                    </span>
                                </div>
                                <script>
                                    $(document).ready(function () {
                                        $('#text_color').minicolors({
                                            control: $(this).attr('data-control') || 'hue',
                                            defaultValue: $(this).attr('data-defaultValue') || '',
                                            format: $(this).attr('data-format') || 'hex',
                                            keywords: $(this).attr('data-keywords') || '',
                                            inline: $(this).attr('data-inline') === 'true',
                                            letterCase: $(this).attr('data-letterCase') || 'lowercase',
                                            opacity: $(this).attr('data-opacity'),
                                            position: $(this).attr('data-position') || 'bottom',
                                            swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
                                            change: function (value, opacity) {
                                                if (!value)
                                                    return;
                                                if (opacity)
                                                    value += ', ' + opacity;
                                            },
                                            theme: 'bootstrap'
                                        });

                                        $('#font-setting-buttons').easyView({
                                            container: '*',
                                            increaseSelector: '.increase-me',
                                            decreaseSelector: '.decrease-me',
                                            normalSelector: '.reset-me',
                                            contrastSelector: '.change-me'
                                        });

                                        $("#resetColor").click(function(){
                                            var color = null;
                                            var font_size = global_js.font_size;
                                            // quickClaim.showOverlayMessage('Saving Content Settings...');
                                            /*$.ajax({
                                                url: '/default/savecontentsetting',
                                                type: "POST",
                                                data: {text_color: color, font_size: font_size},
                                                success: function(res){
                                                    quickClaim.hideOverlayMessage();
                                                    $("p,span,small,h1,h2,h3,h4,h5,h6,li,a,div,input,textarea,select").css('color', color);
                                                }
                                            })*/
                                            //console.log(sessionStorage);
                                            //alert(sessionStorage.fontSize);


                                            var dataColor = sessionStorage.getItem('color');
                                            if(dataColor == null)
                                            {
                                            $("#text_color").minicolors('value', '#444444');
                                            } else {
                                            //$("#text_color").minicolors('value', dataColor);    
                                            $("#text_color").minicolors('value', '#444444');  
                                            }
                                            

                                            //alert(dataFontSize);

                                            // if(dataFontSize){

                                            //     global_js.local_fontSize = dataFontSize;
                                            //             if (dataFontSize > 0) {
                                            //                 for (var i = 0; i <= dataFontSize; i++) {
                                            //                     $('#fontSizeChanger').easyView('increase');
                                            //                 }
                                            //             } else {
                                            //                 for (var i = 0; i >= dataFontSize; i--) {
                                            //                     $('#fontSizeChanger').easyView('decrease');
                                            //                 }
                                            //             }
                                            // }

                                            if(typeof(Storage) !== "undefined") {
                                                $("p,span,small,h1,h2,h3,h4,h5,h6,li,a,div,input,textarea,select").not('.main-navigation-menu li').not('.main-navigation-menu a').not('.main-navigation-menu span').css('color', window.sessionStorage.getItem("color"));
                                            }
                                        });
                                        
                                        $("#contentUpdateBtn").click(function(){
                                            var color = $("#text_color").val();
                                            var font_size = global_js.font_size;
                                            //quickClaim.showOverlayMessage('Saving Content Settings...');
                                            /*$.ajax({
                                                url: '/default/savecontentsetting',
                                                type: "POST",
                                                data: {text_color: color, font_size: font_size},
                                                success: function(res){
                                                    quickClaim.hideOverlayMessage();
                                                    $("p,span,small,h1,h2,h3,h4,h5,h6,li,a,div,input,textarea,select").css('color', color);
                                                }
                                            })*/
                                            window.sessionStorage.setItem("color", color);
                                            window.sessionStorage.setItem("fontSize", font_size);

                                            /*localStorage.setItem('color', color);
                                            localStorage.setItem('fontSize', font_size);*/

                                            if(typeof(Storage) !== "undefined") {
                                                    
                                                $("p,span,small,h1,h2,h3,h4,h5,h6,li,a,div,input,textarea,select").not('.main-navigation-menu li').not('.main-navigation-menu a').not('.main-navigation-menu span').css('color', window.sessionStorage.getItem("color"));
                                            }
                                            
                                        })
                                    });
                                </script>

                            </div>

                    </div>
                </div>
            </div>

      </div>
    </div>
   