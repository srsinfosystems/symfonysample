<body class="login example2">
   <script src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
   <div class="user-login-5">
        <div class="row bs-reset">
            <div class="col-md-6 login-container bs-reset" style="height:100vh;">
                <img class="login-logo login-6" src="<?= image_path('/clipone/hype-logo.png') ?>" width="50%"/>
                <div class="login-content">
                    <h1>Log in to your account</h1>
                    <p>Please enter your name and password to Log in.</p>
                    <?php if ($form->renderBottomErrorBox()): ?>
                        <div class="errorHandler alert alert-danger">
                            <ul>
                                <?php $errors = $form->getErrorSchema()->getErrors() ?>
                                <?php if (count($errors) > 0) : ?>
                                    <?php foreach ($errors as $name => $error) : ?>
                                        <?php if (strpos($error, 'IP') !== false) { ?>
                                        <li>0 : <?php echo $error ?></li>
                                         <?php } else { ?>
                                        <li><?php echo $name ?> : <?php echo $error ?></li>
                                        <?php } ?>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </ul>
                        </div>
                    <?php endif ?>
                    <div class="box-login">
                        <form class="form-login" id="login-form" method="post" action="<?= url_for('@sf_guard_signin'); ?>">
                            <div class="row">
                                <div class="col-xs-6">
                                    <?php echo $form['username']->render(['class' => 'form-control form-control-solid placeholder-no-fix form-group', 'placeholder' => 'Username', 'required' => 'true']); ?>
                                </div>
                                <div class="col-xs-6">
                                    <?php echo $form['password']->render(['class' => 'form-control form-control-solid placeholder-no-fix form-group password', 'placeholder' => 'Password',  'required' => 'true']); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                                        <?php echo $form['remember'] ?> Keep me Logged in
                                        <span></span>
                                    </label>
                                </div>
                                <div class=" col-sm-8 text-right">
                                    <button style="background-color: #BD1431 !important; border-color: #BD1431" type="submit" class="btn btn-bricky pull-right btn-block" name="log_in" id="log_in" >
                                        Login <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                    <div class="box-login " style="margin-top:50px;">
                        <a class="twitter-timeline" data-height="350" href="https://twitter.com/HYPEsystems">Tweets by HYPEsystems</a>
                    </div>

                    <div class="box-register">
                        <h3>Sign Up</h3>
                        <p>
                            Register for a free trial
                        </p>
                        <form class="form-register" action="signup" href="#signup" method="POST">
                            <?php if ($form->renderBottomErrorBox() || isset($_GET['e'])): ?>
                                <div class="errorHandler alert alert-danger">
                                    <ul>
                                        <?php
                                        if (isset($_GET['e'])) {
                                            echo '<li>' . $_GET['e'] . '</li>';
                                        }
                                        ?>
                                        <?php $errors = $form->getErrorSchema()->getErrors() ?>
                                        <?php if (count($errors) > 0) : ?>
                                            <?php foreach ($errors as $name => $error) : ?>
                                                <li><?php echo $name ?> : <?php echo $error ?></li>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            <?php endif ?>
                            <fieldset>
                                <div class="form-group">
                                    <input type="text" required class="form-control" name="fname" id="inputFname" placeholder="First name"  >
                                </div>
                                <div class="form-group">
                                    <input type="text" required class="form-control" name="lname" id="inputLname" placeholder="Last name"  >
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="company" id="inputCompany" placeholder="Company name" >
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="address" id="inputAddress" placeholder="Address" >
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="city" id="inputCity" placeholder="City" >

                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="postal" id="inputPostal" placeholder="Postal" >

                                </div>
                                <div class="form-group">
                                    <input type="phone" class="form-control" name="phone" id="inputPhone" placeholder="Phone" required>
                                </div>


                                <p>
                                    Enter your account details below:
                                </p>
                                <div class="form-group">
                                    <span class="input-icon">
                                        <input type="email" required class="form-control" name="email" id="inputEmail3" placeholder="Email" required>
                                        <i class="fa fa-envelope"></i> </span>
                                </div>
                                <div class="form-group">
                                    <span class="input-icon">
                                        <input type="text" required class="form-control" name="username" id="inputUsername" placeholder="Username" required>
                                        <i class="fa fa-user"></i> </span>
                                </div>
                                <div class="form-group">
                                    <span class="input-icon">
                                        <input type="password" required class="form-control" name="password" id="inputPassword" placeholder="Password" required>
                                        <i class="fa fa-lock"></i> </span>
                                </div>
                                <div class="form-group">
                                    <div>
                                        <label for="agree" class="checkbox-inline">
                                            <input type="checkbox" required class="grey agree" id="agree" name="agree">
                                            I agree to the Terms of Service and Privacy Policy
                                        </label>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <a class="btn btn-light-grey go-back">
                                        <i class="fa fa-circle-arrow-left"></i> Back
                                    </a>
                                    <button type="submit" id="signupBtn" value='signup' class="btn btn-bricky pull-right">
                                        Submit <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <br/><br/>
                </div>
                <div class="footer clearfix" style="position:absolute;bottom:0px;width:100%">
                    <div class="footer-inner">
                        <a target="_blank" href="http://dev.hypemedical.com"><strong>Hype Systems Inc &copy; 2006 - <?= date('Y') ?></strong></a>
                        <div class="info2">OHIP Billing & Practice Management Solution</div>
                        <ul>
                            <li style="padding-left:0px"><a target="_blank" href="http://dev.hypemedical.com/terms-of-use/">Terms of Use</a></li>
                            <li><a target="_blank" href="http://dev.hypemedical.com/resources/">General Knowledge</a></li>
                            <li><a target="_blank" href="http://dev.hypemedical.com/faq/">Training FAQs</a></li>
                        </ul>
                    </div>
                    <div class="footer-items">
                        <a target="_blank" class="btn" href="https://hypemedical.com/contact-us/"><i class="fa fa-envelope-o"></i> Message Support</a>  |<a  class="btn" href="tel:18005922138"> <i class="clip-phone"></i> Toll Free Support: 1-800-592-2138  </a>|
                        <a target="_blank" class="btn" href="https://hypemedical.com/hype-support-2/"><i class="fa fa-desktop"></i>  Remote Access Plugin</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 bs-reset hidden-xs">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">

                        <div class="item active">
                            <div style="display:block;width:100%;height:100vh;overflow:hidden;">
                                <div style="background-image:url('/clipone/bg1.jpg'); background-size: auto 100%; width:100%; height: 100%;">
                                    <div class="loginPageSlideHeader">
                                        “Friendly expert OHIP billing support staff.”
                                        <div style="font-weight: bold;">
                                            REQUEST A FREE TRIAL
                                        </div>
                                    </div>
                                    <div class="loginPageSlideFooter">
                                        Online Clinic Management & OHIP Billing
                                        <br />
                                        “Creating elegant medical office solutions is our passion.”
                                        <div class="loginPageSlideRegisterBtnContainer">
                                            <a href="https://hypemedical.com/contact-us/" class="btn btn-circle btn-primary survayGreen btn-lg btn-block" target="_blank">
                                                REQUEST A FREE TRIAL
                                            </a>
                                        </div>
                                    </div>
                                </div>   
                            </div>
                        </div>


                        <div class="item">
                            <div style="display:block;width:100%;height:100vh;overflow:hidden;">
                                <div style="background-image:url('/clipone/bg3.jpg'); width:100%; height: 100%;">
                                    <div class="loginPage2ndSlideHeader">
                                        <b>Support 24/7</b>
                                        <div class="loginPage2ndSlideDescription">
                                            Your Complete Ontario Medical Office Solution That’s Secured Online & Offline.
                                        </div>
                                    </div>
                                    <div class="loginPage2ndSlideVideo">
                                        <iframe width="800" height="400" src="https://www.youtube.com/embed/JjDVujfb1_s" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                    <div class="loginPage2ndSlideFooter">
                                        <div class="loginPageSlideRegisterBtnContainer">
                                            <a href="https://hypemedical.com/contact-us/" class="btn btn-circle btn-primary survayGreen btn-lg btn-block" target="_blank">
                                                REQUEST A FREE TRIAL
                                            </a>
                                        </div>
                                    </div>
                                </div>   
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="ScrollToTop" style="position:fixed;top:95%;right:5%">
        <span class=" go-top fab-action-button">
            <i class="fab-action-button__icon"></i>
        </span>
    </div>


    <style>
        /* Cubic Bezier Transition */
        .user-login-5 {
            min-height: 100vh; }
        .user-login-5 .bs-reset {
            margin: 0;
            padding: 0; }
        .user-login-5 .text-right {
            text-align: right; }
        .user-login-5 .login-bg {
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
            min-height: 100vh; }
        .user-login-5 .login-logo {
            position: absolute;
            top: 2.5em;
            left: 2.5em; }
        .user-login-5 .login-logo.login-6 {
            top: 25px;
            left: 80px; }
        .user-login-5 .login-container {
            position: relative;
            min-height: 100vh; }
        .user-login-5 .login-container > .login-content,
        .user-login-5 .login-container .login-social,
        .user-login-5 .login-container .login-copyright {
            padding: 0 80px; }
        .user-login-5 .login-container > .login-content {
            margin-top: 15%; }
        .user-login-5 .login-container > .login-content > h1 {
            font-size: 30px;
            font-weight: 300;
            color: #4e5a64; }
        .user-login-5 .login-container > .login-content p {
            color: #a0a9b4;
            font-size: 15px;
            line-height: 22px; }
        .user-login-5 .login-container > .login-content > .login-form {
            margin-top: 80px;
            color: #a4aab2;
            font-size: 13px; }
        .user-login-5 .login-container > .login-content > .login-form .form-control {
            width: 100%;
            padding: 10px 0;
            border: none;
            border-bottom: 1px solid;
            border-color: #a0a9b4;
            color: #868e97;
            font-size: 14px;
            margin-bottom: 30px; }
        .user-login-5 .login-container > .login-content > .login-form .form-control:focus {
            outline: 0; }
        .user-login-5 .login-container > .login-content > .login-form .forgot-password,
        .user-login-5 .login-container > .login-content > .login-form .login-button {
            display: inline-block; }
        .user-login-5 .login-container > .login-content > .login-form .rem-password {
            margin-top: 10px; }
        .user-login-5 .login-container > .login-content > .login-form .rem-password > p {
            margin: 0; }
        .user-login-5 .login-container > .login-content > .login-form .rem-password > .rem-checkbox {
            border-color: #a4aab2; }
        .user-login-5 .login-container > .login-content > .login-form .forgot-password {
            margin-right: 1em; }
        .user-login-5 .login-container > .login-content > .login-form .forgot-password > a {
            color: #a4aab2; }
        .user-login-5 .login-container > .login-content > .login-form .forgot-password > a:hover {
            color: #337ab7;
            text-decoration: none; }
        .user-login-5 .login-container > .login-content > .login-form .forgot-password > a:focus {
            color: #a4aab2;
            text-decoration: none; }
        .user-login-5 .login-container > .login-footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            padding-bottom: 10px; }
        .user-login-5 .login-container > .login-footer .login-social {
            padding-right: 0; }
        .user-login-5 .login-container > .login-footer .login-social li {
            display: inline-block;
            list-style: none;
            margin-right: 1em; }
        .user-login-5 .login-container > .login-footer .login-social a {
            color: #a9b5be;
            font-size: 18px; }
        .user-login-5 .login-container > .login-footer .login-social a:hover {
            color: #337ab7;
            text-decoration: none; }
        .user-login-5 .login-container > .login-footer .login-social a:focus {
            color: #a9b5be; }
        .user-login-5 .login-container > .login-footer .login-copyright {
            padding-left: 0;
            margin-top: 6px; }
        .user-login-5 .login-container > .login-footer .login-copyright > p {
            margin: 0;
            font-size: 13px;
            color: #a9b5be; }
        /*        .user-login-5 .alert {
                    margin-top: -60px; }*/
        .user-login-5 .form-group.has-error {
            border-bottom: 2px solid #ed6b75 !important; }
        .user-login-5 .form-group.valid {
            border-bottom: 1px solid #a0a9b4 !important; }

        @media (max-width: 1365px) {
            .user-login-5 .login-logo.login-6 {
                top: 40px;
                left: 40px; }
            .user-login-5 .login-container > .login-content,
            .user-login-5 .login-container .login-social,
            .user-login-5 .login-container .login-copyright {
                padding: 0 40px; }
            .user-login-5 .login-container .login-social {
                padding-right: 0; }
            .user-login-5 .login-container .login-copyright {
                padding-left: 0; } }

        @media (max-width: 1023px) {
            .user-login-5 {
                min-height: 50vh; }
            .user-login-5 .login-bg {
                min-height: 50vh; }
            .user-login-5 .login-logo.login-6 {
                position: relative;
                margin: 0 0 40px 0; }
            .user-login-5 .login-container {
                min-height: 50vh; }
            .user-login-5 .login-container > .login-content {
                margin-top: 60px; }
            .user-login-5 .login-container > .login-content .login-form {
                margin-top: 40px; }
            .user-login-5 .login-container .rem-password {
                margin-bottom: 1em; }
            .user-login-5 .login-container > .login-footer {
                position: relative;
                margin-top: 40px;
                padding-bottom: 0; }
            .user-login-5 .login-container > .login-footer .login-social li {
                margin-right: 0.5em; }
            .user-login-5 .alert {
                margin-top: -20px; } }

        @media (max-width: 640px) {
            .user-login-5 .login-container > .login-content .text-right {
                text-align: left; } }


        .loginPageSlideHeader
        {
            padding-top: 6vh;
            color: white;
            font-size: 40px;
            text-align: center;
            width: 85%;
            margin: auto;  
        }


        .loginPageSlideFooter
        {
            padding-top: 25vh;
            color: white;
            font-size: 22px;
            text-align: center;
            width: 61%;
            margin: auto;
        }

        .loginPageSlideRegisterBtnContainer
        {
            padding-top: 10px;
        }


        .loginPage2ndSlideHeader
        {
            padding-top: 10vh;
            color: #94cb5b;
            font-size: 40px;
            text-align: center;
            width: 60%;
            margin: auto;
        }


        .loginPage2ndSlideDescription
        {
            padding-top: 10px;
            color: #fff;
            font-weight: bold;
            font-size: 26px;
            text-align: center;
            margin: auto;
        }

        .loginPage2ndSlideVideo
        {
            text-align: center;
            padding-top: 20px;
        }

        .loginPage2ndSlideFooter
        {
            padding-top: 20px;
            color: white;
            font-size: 22px;
            text-align: center;
            width: 60%;
            margin: auto;
        }
    </style>



</body>

<script>
    jQuery(document).ready(function () {
        Main.init();
//        Login.init();
    });
</script>

<script>
    $('.forgot').on('click', function () {
        $('.box-login').hide();
        $('.box-forgot').show();
    });
    $('.register').on('click', function () {
        $('.box-login').hide();
        $('.box-register').show();
    });
    $('.go-back').click(function () {
        $('.box-login').show();
        $('.box-forgot').hide();
        $('.box-register').hide();
    });
</script>
<?php if (sfContext::getInstance()->getRouting()->getCurrentInternalUri() == 'default/signup') { ?>
    <script>
        $('.register').trigger('click');
    </script>
<?php } ?>
 <script type="text/javascript">
     $.getJSON('https://ipapi.co/json/', function(data) {
         console.log(data.ip);
         document.cookie="visitor_ip="+data.ip;

        });
 </script>
 <script type="text/javascript">
     $('#log_in').click(function(){
        if(localStorage.getItem('locations')){
            localStorage.removeItem("locations");
        }
        if(localStorage.getItem('doctors')){
            localStorage.removeItem("doctors"); 
        }
     });
 </script>