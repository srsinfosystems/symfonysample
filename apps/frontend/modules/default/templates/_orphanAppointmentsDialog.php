<div id="orphanAppointmentsDialog" style="display:none" title="Appointments Needing Rescheduling">
    <div class="panel panel-default">
        <div class="panel-heading expand">
            <i class="clip-search-3"></i>
            Refine Your Search <span id="orphan_spinner" style="display:none"><i class="fa fa-spin fa-refresh"></i></span>
        </div>
        <div class="panel-body buttons-widget" style="overflow: hidden; display: none;">
            <form id="orphanAppointmentsForm">
                <div id="orphans_refine" class="form-table">
                    <div class="col-sm-2 col-xs-6">
                        <div class="form-group">
                            <label class="control-label">
                                <?php echo $form['start_date']->renderLabel(); ?>
                            </label>
                            <?php echo $form['start_date']->render(['class' => 'form-control btn-squared']); ?>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-6">
                        <div class="form-group">
                            <label class="control-label">
                                <?php echo $form['end_date']->renderLabel(); ?>
                            </label>
                            <?php echo $form['end_date']->render(['class' => 'form-control btn-squared']); ?>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-6">
                        <div class="form-group">
                            <label class="control-label">
                                <?php echo $form['location_id']->renderLabel(); ?>
                            </label>
                            <?php echo $form['location_id']->render(['class' => 'form-control Select2']); ?>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-6">
                        <div class="form-group">
                            <label class="control-label">
                                <?php echo $form['doctor_id']->renderLabel(); ?>
                            </label>
                            <?php echo $form['doctor_id']->render(['class' => 'form-control Select2']); ?>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-6">
                        <div class="form-group" style="margin-top:30px;">
                            <button type="button" class="btn btn-blue btn-squared" id="orphanAppointmentsSubmit" ><i class="clip-search-3"> </i> Search</button>  
                            <button type="button" class="btn btn-danger btn-squared" id="orphanAppointmentsClear"><i class="clip-file-minus"> </i> Clear</button>  
                        </div>
                    </div>

                </div>
                <?php echo $form->renderHiddenFields(); ?>
            </form>
        </div>
    </div>
    <h4>Displaying <span id="orphanResultsCount"></span> of <?php echo number_format($count, 0, '.', ','); ?> possible appointments</h4>

    <table id="orphanAppointments" class="table table-hover sk-datatable table-striped">
    </table>

    <br clear="all" />
    These appointments were booked before a doctor became unavailable and have been marked as needing to be either rescheduled or cancelled.
    The appointments will appear in red in the appointment book and can be cancelled or rescheduled from there.
    <br />Maximum number of results: 500
</div>

<script>
    $(document).ready(function () {
        orphanAppointments.actions.search = '<?php echo url_for('appointment_search/orphanReport'); ?>';
        orphanAppointments.actions.profile = '<?php echo url_for('patient/index'); ?>';
        orphanAppointments.initialize();
    });
</script>