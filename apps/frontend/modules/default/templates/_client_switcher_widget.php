<div class="panel panel-default">
    <div class="panel-heading">
        <i class=" clip-user-5"></i>
        Select A Session Client <small class="text-muted">(Active: <?= $current_client ?>)</small>
        <div class="panel-tools">
            <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
            </a>
            <a class="btn btn-xs btn-link panel-close" href="#">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
    <div class="panel-body" id="clientSwitcherWidget">
        <table class="table table-hover" id="clientDT">
            <thead>
                <tr>
                    <th style="padding:5px"></th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($clients) > 1): ?>
                    <?php foreach ($clients as $id => $client): ?>
                        <tr style="cursor:pointer" class="clientSwitcherTr">
                            <?php if ($id == $current_client): ?>
                                <th>
                                    <?php echo link_to($id . ' - ' . $client, 'default/changeClient?client_id=' . $id, ['class' => 'todo-actions']); ?>
                                </th>
                            <?php else: ?>
                                <td>
                                    <?php echo link_to($id . ' - ' . $client, 'default/changeClient?client_id=' . $id, ['class' => 'todo-actions']); ?>
                                </td>
                            <?php endif; ?>

                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>  
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".todo-actions").click(function () {
            window.location.href = $(this).attr('href');
        })
        $('#changeClientSave').button();

        global_js.DTCustomSortFunction()

        var table = $("#clientDT").DataTable({
            "paging": false,
            "ordering": false,
            "scrollY": "400px",
            "scrollCollapse": true,
            "info": false,
            "sDom": '<"wrapper"  <"col-sm-12 customSearchInput" f> rt<"clear">>',
            initComplete: function () {
                var inputHtml = '<div class="input-group">' +
                        '<input type="text" placeholder="Contains..." data-focus="true" class=" dashboardClientSession form-control DatatableAllRounderSearch" />' +
                        '<span class="input-group-addon cursorPointer btnClearSearchTxt" style="cursor:pointer"> ' +
                        '<i class="clip-cancel-circle-2 "></i>' +
                        '</span>' +
                        '<span class="input-group-addon cursorPointer"> ' +
                        '<i class="clip-search-2"></i>' +
                        '</span>' +
                        '</div>';

                $(".customSearchInput").html(inputHtml);
            }
        });

        $('.DatatableAllRounderSearch').on('keyup', function () {
            table.search(this.value).draw();
        });

        $(".btnClearSearchTxt").click(function () {
            $('.DatatableAllRounderSearch').val('').trigger('keyup');
        })

        $(".clientSwitcherTr").click(function () {
            $(this).find('a').trigger('click');
        })
    });
</script>