<?php

/*
 * This file is part of the symfony package.
 * (c) Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 *
 * @package    symfony
 * @subpackage plugin
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfGuardValidatorUser.class.php 23319 2009-10-25 12:22:23Z Kris.Wallsmith $
 */
class sfGuardValidatorUser extends sfValidatorBase
{
  public function configure($options = array(), $messages = array())
  {
    $this->addOption('username_field', 'username');
    $this->addOption('password_field', 'password');
    $this->addOption('throw_global_error', false);

    $this->setMessage('invalid', 'The username and/or password is invalid.');
  }

  protected function doClean($values)
  {
    $username = isset($values[$this->getOption('username_field')]) ? $values[$this->getOption('username_field')] : '';
    $password = isset($values[$this->getOption('password_field')]) ? $values[$this->getOption('password_field')] : '';

    if ($username && $user = $this->getTable()->retrieveByUsername($username))
    {
      
       $data = Doctrine_Query::create()
            ->from('UserProfile')
            ->addWhere('id = ?', $user->UserProfile->id)
            ->fetchOne(); 
        $ipAddress = "";
        if(isset($_COOKIE['visitor_ip'])) {
            $ipAddress = $_COOKIE['visitor_ip'];
        }
        $banned = 0;
        if(!empty($ipAddress)){

                if(!empty($data->restricted_ips)) { 
                    $banned = 1;
                }
                if($banned == 1) {
                  if(trim($ipAddress) == trim($data->restricted_ips)) {
                    $banned = 0;
                  }
                }
        }
        if($banned == 1)
        {
            $this->setMessage('invalid', 'Your current IP address/location is not approved for log-in to HYPEMedical.');
        }
        // password is ok?
        else if ($user->getIsActive() && $user->checkPassword($password))
        {
          return array_merge($values, array('user' => $user));
        }
    }

    if ($this->getOption('throw_global_error'))
    {
      throw new sfValidatorError($this, 'invalid');
    }

    throw new sfValidatorErrorSchema($this, array($this->getOption('username_field') => new sfValidatorError($this, 'invalid')));
  }

  protected function getTable()
  {
    return Doctrine::getTable('sfGuardUser');
  }
}
