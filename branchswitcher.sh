
printf "Hello,  This script will used to switch branch. \n\n\n"

printf  "Please select branch and press [ENTER]: \n\n"
printf  "[1] : master - master branch main production branch \n"
printf  "[2] : next-release - branch with next release code. \n\n"
printf  "Enter 1 or 2 to switch branch\n\n"


read branch

if [ $branch -eq 1 ]; then
  	printf "Switching to master branch... \n"
	git checkout master
	
else
  	printf "Switching to next-release branch... \n"
	git checkout next-release
fi

