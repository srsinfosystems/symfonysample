<?php

class setClaimFacilityIDTask extends sfBaseTask
{
    private $claimsCount = 0;
    private $claimsLimit = 10000;

    public function execute($arguments = array(), $options = array())
    {
        try {
            sfContext::createInstance($this->configuration);
            $this->executeTask($arguments, $options);
        }
        catch (Exception $e) {
            $this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
            throw $e;
        }
    }

    private function executeTask($arguments, $options)
    {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase('doctrine')->getConnection();

        $minID = 0;
        if (array_key_exists('minID', $options)) {
            $minID = intval($options['minID']);
        }

        do {
            $facility = $this->getFacilityByMinID($minID);

            if ($facility instanceof Facility) {
                $this->updateClaims($facility);

                $minID = $facility->get('id');
            }
        } while ($facility instanceof Facility);
    }

    private function getFacilityByMinID($id)
    {
        return Doctrine_Query::create()
            ->from('Facility f')
            ->addWhere('f.id > (?)', array($id))
            ->limit(1)
            ->addOrderBy('f.id asc')
            ->fetchOne();
    }

    private function updateClaims(Facility $facility)
    {
        echo 'Updating claims with Facility Number ' . $facility->getFacilityMasterNumber() . '  [' . $facility->get('id') . ']' . "\n";
        do {
            $claims = Doctrine_Query::create()
                ->from('Claim c')
                ->addWhere('c.facility_num = (?)', array($facility->getFacilityMasterNumber()))
                ->addWhere('c.facility_id IS NULL')
                ->limit(200)
                ->execute();

            foreach ($claims as $key => $claim)
            {
                $claims[$key]->setFacilityId($facility->get('id'));
            }
            $claims->save();
            $this->claimsCount += $claims->count();

            echo '  updated ' . $claims->count() . ' claims.' . "\n";

            if ($this->claimsCount > $this->claimsLimit) {
                exit();
            }
        } while (count($claims));
    }

    protected function configure()
    {
        $this->namespace = 'updateDataModel';
        $this->name = '2015-02-setClaimFacilityID';
        $this->briefDescription = "Fills in the facility ID in the claims table";

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('minID', null, sfCommandOption::PARAMETER_REQUIRED, 'Minimum ID to start on', 0),
        ));
    }
}