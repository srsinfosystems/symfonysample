<?php

class setAppointmentAttendeeClientIDTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'updateDataModel';
		$this->name = '2015-02-AttendeeClientID';
		$this->briefDescription = "Fills in the client ID in the appointment attendee table";

		$this->addOptions(array(
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
				new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
		));
	}
	public function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$this->setAppointmentAttendeeClientID();
	}

	private function setAppointmentAttendeeClientID()
	{
		$count = 0;
		$limit = 25000;

		do {
			$attendees = Doctrine_Query::create()
				->from('AppointmentAttendee att')
				->leftJoin('att.Appointment a')
				->addWhere('att.client_id is null')
				->addWhere('a.client_id is not null')
				->limit(5)
				->execute();

			foreach ($attendees as $attendee)
			{
				$attendee->setClientID($attendee->Appointment->getClientID());
				$attendee->save();
				$count++;
			}

			echo $count . ' Set Client ID on ' . $attendees->count() . ' attendees.' . "\n";
		} while ($count < $limit && $attendees->count());

		if ($attendees->count()) {
			exit();
		}
	}
}