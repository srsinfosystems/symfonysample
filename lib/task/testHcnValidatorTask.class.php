<?php

class testHcnValidatorTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'test';
		$this->name = 'hcn_validator';
		$this->briefDescription = 'Says hello';

		$this->addOptions(array(
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
				new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
				new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{

		$valids = array(
				'00865386' => 'PE',
				'99269953' => 'PE',
				'9812519027' => 'BC',
				'8996824762' => 'ON',
				'365586931' => 'YT',
				'188590441' => 'YT',
				'820216801' => 'SK',
				'552403032' => 'SK',
				'120672686' => 'MB',
				'116583760' => 'MB',
				'335766644' => 'YT',
				'483000041' => 'YT',
				'6534778375' => 'NB',
				'918749995' => 'NB',
				'230002235508' => 'NF',
				'619853490016' => 'NF',
				'0011477403' => 'NS',
				'2598074223' => 'NS',
				'171524087' => 'NU',
				'BELF88090715' => 'QU',
				'AOUJ62120917' => 'QU',
				'N7024649' => 'NW',
		);

		foreach ($valids as $hcn => $prov)
		{
			echo $hcn
			. ' [' . $prov . ']  -- '
			. hypeHealthCardValidation::getProvinceFromHCN($hcn)
			. "\n";
		}
	}
	private function getValidNbHcn() {
		for ($a = 1; $a <= 9; $a++)
		{
			for ($b = 0; $b <= 9; $b++)
			{
				for ($c = 0; $c <= 9; $c++)
				{
					for ($d = 0; $d <= 9; $d++)
					{
						for ($e = 0; $e <= 9; $e++)
						{
							for ($f = 0; $f <= 0; $f++)
							{
								for ($g = 0; $g <= 9; $g++)
								{
									for ($h = 0; $h <= 9; $h++)
									{
										for ($i = 0; $i <= 9; $i++)
										{
											$hcn = $a . $b . $c . $d . $e . $f . $g . $h . $i . '';
											if (hypeHealthCardValidation::isValidHcnNB($hcn)) {
												return $hcn;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	private function getValidSkHcn() {
		for ($a = 1; $a <= 9; $a++)
		{
			for ($b = 0; $b <= 9; $b++)
			{
				for ($c = 0; $c <= 9; $c++)
				{
					for ($d = 0; $d <= 9; $d++)
					{
						for ($e = 0; $e <= 9; $e++)
						{
							for ($f = 0; $f <= 0; $f++)
							{
								for ($g = 0; $g <= 9; $g++)
								{
									for ($h = 0; $h <= 9; $h++)
									{
										for ($i = 0; $i <= 9; $i++)
										{
											$hcn = $a . $b . $c . $d . $e . $f . $g . $h . $i . '';
											if (hypeHealthCardValidation::isValidHcnSK($hcn)) {
												return $hcn;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}