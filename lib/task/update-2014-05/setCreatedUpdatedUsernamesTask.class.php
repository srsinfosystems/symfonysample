<?php

class setCreatedUpdatedUsernamesTask extends sfBaseTask
{
	private $tables = array("Claim", "ClaimItem", "ReportClaim");
	private $currentTable;

	protected function configure()
	{
		$this->namespace = 'updateDataModel';
		$this->name = '2014-05-CreatedUpdatedUsernames';
		$this->briefDescription = "Fills in the Created Username & Updated Username values in Claim, ClaimItem & ReportClaim tables";

		$this->addOptions(array(
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
				new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
		));
	}
	public function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$this->setClaimItemClientID();
		$this->setClaimsCreatedUsername();
		$this->setClaimsUpdatedUsername();
		$this->setClaimItemCreatedUsername();
		$this->setClaimItemUpdatedUsername();
	}

	private function setClaimItemClientID()
	{
		$count = 0;
		$limit = 1000;

		do {
			$claimItems = Doctrine_Query::create()
				->from('ClaimItem ci')
				->leftJoin('ci.Claim c')
				->addWhere('c.client_id is not null')
				->addWhere('ci.client_id is null')
				->limit(10)
				->execute();

			foreach ($claimItems as $claimItem)
			{
				$claimItem->client_id = $claimItem->Claim->client_id;
				$claimItem->save();

				$count++;
			}

			echo $count . ' Set Client ID on ' . $claimItems->count() . ' claim items.' . "\n";
		} while ($count < $limit && $claimItems->count());

		if ($claimItems->count()) {
			exit();
		}
	}
	private function setClaimsCreatedUsername()
	{
		$userID = null;

		$count = 0;
		$limit = 500;

		do {
			if (!$userID) {
				$userID = $this->lookForID('Claim', 'created');
				if ($userID) {
					$userName = Doctrine::getTable("UserProfile")->buildUserNameStr($userID);
					$userIDStr = Doctrine::getTable('UserProfile')->buildUserIDStr($userID);
				}
				else {
					return;
				}
			}

			$claims = Doctrine_Query::create()
				->from('Claim c')
				->addWhere('c.created_by = (?)', $userID)
				->addWhere('c.created_username is null')
				->limit(5)
				->execute();


			foreach ($claims as $claim)
			{
				if ($claim->getCreatedBy() == $claim->getUpdatedBy()) {
					$c = Doctrine_Query::create()
						->update('Claim c')
						->set('c.created_username', '?', $userName)
						->set('c.updated_username', '?', $userName)
						->set('c.created_user_id', '?', $userIDStr)
						->set('c.updated_user_id', '?', $userIDStr)
						->addWhere('c.id = (?)', $claim->getID())
						->execute();

				}
				else {
					$c = Doctrine_Query::create()
						->update('Claim c')
						->set('c.created_username', '?', $userName)
						->set('c.created_user_id', '?', $userIDStr)
						->addWhere('c.id = (?)', $claim->getID())
						->execute();
				}
				$count++;

			}
			if ($claims->count()) {
				echo $count . ' Updated ' . $claims->count() . ' Claims. '  . $userName  . '[' . $userIDStr . ']' .  "\n";
			}
		} while ($count < $limit && $claims->count());

		if ($claims->count()) {
			exit();
		}
	}
	private function setClaimsUpdatedUsername()
	{
		$userID = null;

		$count = 0;
		$limit = 500;

		do {
			if (!$userID) {
				$userID = $this->lookForID('Claim', 'updated');
				if ($userID) {
					$userName = Doctrine::getTable("UserProfile")->buildUserNameStr($userID);
					$userIDStr = Doctrine::getTable('UserProfile')->buildUserIDStr($userID);
				}
				else {
					return;
				}
			}

			$claims = Doctrine_Query::create()
				->from('Claim c')
				->addWhere('c.updated_by = (?)', $userID)
				->addWhere('c.updated_username is null')
				->limit(5)
				->execute();


			foreach ($claims as $claim)
			{
				$c = Doctrine_Query::create()
					->update('Claim c')
					->set('c.updated_username', '?', $userName)
					->set('c.created_user_id', '?', $userIDStr)
					->addWhere('c.id = (?)', $claim->getID())
					->execute();
				$count++;

			}
			if ($claims->count()) {
				echo $count . ' Updated ' . $claims->count() . ' Claims. '  . $userName  . '[' . $userIDStr . ']' .  "\n";
			}
		} while ($count < $limit && $claims->count());

		if ($claims->count()) {
			exit();
		}
	}

	private function setClaimItemCreatedUsername()
	{
		$userID = null;

		$count = 0;
		$limit = 500;

		do {
			if (!$userID) {
				$userID = $this->lookForID('ClaimItem', 'created');
				if ($userID) {
					$userName = Doctrine::getTable("UserProfile")->buildUserNameStr($userID);
					$userIDStr = Doctrine::getTable('UserProfile')->buildUserIDStr($userID);
				}
				else {
					return;
				}
			}

			$claims = Doctrine_Query::create()
				->from('ClaimItem ci')
				->addWhere('ci.created_by = (?)', $userID)
				->addWhere('ci.created_username is null')
				->limit(5)
				->execute();

			foreach ($claims as $claim)
			{
				if ($claim->getCreatedBy() == $claim->getUpdatedBy()) {
					$c = Doctrine_Query::create()
						->update('ClaimItem ci')
						->set('ci.created_username', '?', $userName)
						->set('ci.updated_username', '?', $userName)
						->set('ci.created_user_id', '?', $userIDStr)
						->set('ci.updated_user_id', '?', $userIDStr)
						->addWhere('ci.id = (?)', $claim->getID())
						->execute();

				}
				else {
					$c = Doctrine_Query::create()
						->update('ClaimItem ci')
						->set('ci.created_username', '?', $userName)
						->set('ci.created_user_id', '?', $userIDStr)
						->addWhere('ci.id = (?)', $claim->getID())
						->execute();
				}
				$count++;

			}
			if ($claims->count()) {
				echo $count . ' Updated ' . $claims->count() . ' Claim Items. '  . $userName . '[' . $userIDStr . ']' . "\n";
			}
		} while ($count < $limit && $claims->count());

		if ($claims->count()) {
			exit();
		}
	}
	private function setClaimItemUpdatedUsername()
	{
		$userID = null;

		$count = 0;
		$limit = 500;

		do {
			if (!$userID) {
				$userID = $this->lookForID('ClaimItem', 'updated');
				if ($userID) {
					$userName = Doctrine::getTable("UserProfile")->buildUserNameStr($userID);
					$userIDStr = Doctrine::getTable('UserProfile')->buildUserIDStr($userID);
				}
				else {
					return;
				}
			}

			$claimItems = Doctrine_Query::create()
				->from('ClaimItem ci')
				->addWhere('ci.updated_by = (?)', $userID)
				->addWhere('ci.updated_username is null')
				->limit(5)
				->execute();

			foreach ($claimItems as $item)
			{
				$c = Doctrine_Query::create()
					->update('ClaimItem ci')
					->set('ci.updated_username', '?', $userName)
					->set('ci.updated_user_id', '?', $userIDStr)
					->addWhere('ci.id = (?)', $item->getID())
					->execute();
				$count++;
			}
			if ($claimItems->count()) {
				echo $count . ' Updated ' . $claimItems->count() . ' Claim Items. '  . $userName  . '[' . $userIDStr . ']' . "\n";
			}
		} while ($count < $limit && $claimItems->count());

		if ($claimItems->count()) {
			exit();
		}
	}

	private function lookForID($model, $column)
	{
		$object = Doctrine_Query::create()
			->from($model . ' c')
			->addWhere('c.' . $column . '_by is not null')
			->addWhere('c.' . $column . '_username is null')
			->limit(1)
			->fetchOne();

		if ($object instanceof $model) {
			return $object[$column . '_by'];
		}
		return null;
	}
}