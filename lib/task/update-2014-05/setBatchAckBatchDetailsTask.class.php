<?php

class setBatchAckBatchDetailsTask extends sfBaseTask
{
 	private $minID = 0;
 	private $database = null;

	protected function configure()
	{
		$this->namespace = 'updateDataModel';
		$this->name = '2014-05-setBatchAckBatchDetails';
		$this->briefDescription = "Places Batch Details in the new Batch Edit Report tables";

		$this->addOptions(array(
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
				new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
				new sfCommandOption('min_id', null, sfCommandOption::PARAMETER_REQUIRED, 'Minimum ID to start with'),
		));
	}
	public function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

 		$count = 0;
 		$limit = 10000;

		if (array_key_exists('min_id', $arguments) && intval($arguments['min_id'])) {
			$this->minID = $arguments['min_id'];
		}

 		do {
 			$batchEditReport = Doctrine_Query::create()
 				->from('BatchEditReport ber')
 				->addWhere('ber.id > (?)', $this->minID)
 				->addWhere('ber.batch_id IS NULL')
 				->limit(1)
 				->fetchOne();

 			if ($batchEditReport instanceof BatchEditReport) {
 				$this->minID = $batchEditReport->getId();

 				echo 'Batch Edit Report found - ID = ' . $this->minID . "\n";

 				$batch = Doctrine_Query::create()
 					->from('Batch b')
 					->addWhere('b.batch_number = (?)', $batchEditReport->getBatchNumber())
 					->addWhere('b.provider_num = (?)', $batchEditReport->getProviderNum())
 					->addWhere('b.group_num = (?)', $batchEditReport->getGroupNum())
 					->addWhere('b.status != (?)', BatchTable::STATUS_DELETED)
 					->limit(1)
 					->fetchOne();

 				if ($batch instanceof Batch) {
 					echo 'Batch found - ID = ' . $batch->id . "\n";

 					$batchEditReport->setBatchId($batch->getId());
 					$batchEditReport->setBatchClientId($batch->getClientId());
 					$batchEditReport->save();
 				}
 			}

 			$count++;
 		} while ($batchEditReport instanceof BatchEditReport && $count < $limit);
	}
}