<?php

class setAppointmentAttendeePatientNumberTask extends sfBaseTask
{
	private $minID = 0;
	private $database = null;

	protected function configure()
	{
		$this->namespace = 'updateDataModel';
		$this->name = '2014-05-AppointmentAttendeePatientNumber';
		$this->briefDescription = "Places current Patient's Chart Number in the new Appointment Attendee column";

		$this->addOptions(array(
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
				new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
		));
	}
	public function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$count = 0;
		$limit = 10000;

		if (array_key_exists('min_id', $arguments) && intval($arguments['min_id'])) {
			$this->minID = $arguments['min_id'];
		}

		do {
			$patientID = $this->getNextPatientID();
			$patientNumber = $this->getPatientNumber($patientID);

			if ($patientID && $patientNumber) {
				echo 'Patient ID ' . $patientID . ".\n";

				do {
					$attendees = Doctrine_Query::create()
						->from('AppointmentAttendee att')
						->addWhere('att.patient_id = (?)', $patientID)
						->addWhere('att.patient_number is null')
						->limit(10)
						->execute();

					foreach ($attendees as $attendee)
					{
						echo '  Appointment ID ' . $attendee->appointment_id . "\n";
						$attendee->patient_number = $patientNumber;
						$attendee->save();
					}
				} while (count($attendees));
			}
			echo "\n";
			$count++;
		} while ($patientID && $count < $limit);
	}

	private function getNextPatientID()
	{
		$appointmentAttendee = Doctrine_Query::create()
			->from('AppointmentAttendee att')
			->addWhere('att.patient_number is null')
			->addWhere('att.id > (?)', $this->minID)
			->orderBy('att.id asc')
			->limit(1)
			->fetchOne();

		if ($appointmentAttendee instanceof AppointmentAttendee) {
			$this->minID = $appointmentAttendee->id;
			return $appointmentAttendee->patient_id;
		}
	}
	private function getPatientNumber($id)
	{
		if ($id === null) {
			return null;
		}

		$patient =  Doctrine_Query::create()
			->from('Patient p')
			->addWhere('p.id = (?)', $id)
			->fetchOne();

		if ($patient instanceof Patient) {
			return $patient->patient_number;
		}

		return null;
	}
}