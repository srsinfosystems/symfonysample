<?php

class testHcnSearchTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'test';
		$this->name = 'hcn_search';
		$this->briefDescription = 'Says hello';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$terms = array(
			'client_id'    => '27',
			'health_num'   => '8716343879',
			'lname'        => 'PATTERSON',
			'fname'        => 'SHANNON J',
			'expiry_date'  => '0000',
			'sex'          => '2',
			'dob'          => '19820913',
			'version_code' => 'B',
		);

		$search = HealthCardSearch::create()
			->setClientId($terms['client_id'])
			->setFname($terms['fname'])
			->setLname($terms['lname'])
			->setHealthCardNumber($terms['health_num'])
			->setSex($terms['sex'] == 1 ? 'M' : 'F')
			->setVersionCode($terms['version_code'])
			->setExpiryDate(HealthCardSearch::formatExpiryDate($terms['expiry_date']))
			->setDob(HealthCardSearch::formatDob($terms['dob']));

		$search->debug(true);
		$patients = $search->getPatients();
	}
}