<?php

class UpdateClaimItemPaymentsTask extends sfBaseTask
{
	/**
	 * HOW TO RUN THIS COMMAND:
	 *
	 * symfony hype:updateClaimItemPayments
	 */
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'updateClaimItemPayments';
		$this->briefDescription = "Checks for Claim Items where a payments was received in an RA but doesn't match what is in the Claim Item";

		$this->addOptions(array(
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$totalUpdates = 0;
		do {
			$claimItems = Doctrine_Query::create()
				->from('ClaimItem ci')
				->leftJoin('ci.Claim c')
				->leftJoin('ci.RaItem i')
				->addWhere('c.deleted = (?)', false)
				->addWhere('ci.status != (?)', ClaimItemTable::STATUS_DELETED)
				->addWhere('i.amount_paid > ci.fee_paid')
				->addWhere('ci.fee_paid = (?)', 0)
				->limit(15)
				->orderBy('ci.service_date desc')
				->execute();

			foreach ($claimItems as $claimItem)
			{
				$originalFee = $claimItem->getFeePaid();
				$totalPaid = 0;
				$updatedCount = 0;

				$raItems = Doctrine_Query::create()
					->from('RaItem i')
					->addWhere('i.item_id = (?)', $claimItem['id'])
					->addWhere('i.provider_num = (?)', $claimItem->RaItem[0]['provider_num'])
					->addWhere('i.group_num = (?)', $claimItem->RaItem[0]['group_num'])
					->addWhere('i.hcn = (?)', $claimItem->RaItem[0]['hcn'])
					->addWhere('i.serv_date = (?)', $claimItem->RaItem[0]['serv_date'])
					->addWhere('i.serv_code = (?)', $claimItem->RaItem[0]['serv_code'])
					->execute(array(), Doctrine::HYDRATE_ARRAY);

				foreach ($raItems as $raItem)
				{
					$totalPaid += (float) $raItem['amount_paid'];
				}

				if ($totalPaid > 0) {
					$claimItem->fee_paid = $totalPaid;
					if ($claimItem->status != ClaimItemTable::STATUS_PAID_BY_USER && $claimItem->status != ClaimItemTable::STATUS_PAID_BY_USER) {
						$claimItem->errors = '';
						$claimItem->status = ClaimItemTable::STATUS_PAID_CLOSED;
					}
					$claimItem->save();
					$updatedCount++;

					echo 'Service Date: ' . substr($claimItem->getServiceDate(), 0, 10) . '  Updated Claim Item ID ' . $claimItem->getId() . ' Fee Paid to ' . number_format($totalPaid, 2) . "\n";
				}
				else {
					echo 'Service Date: ' . substr($claimItem->getServiceDate(), 0, 10) . '  Ignored Claim Item ID ' . $claimItem->getId() . "\n";
				}
			}

			$totalUpdates += $claimItems->count();

		} while ($claimItems->count() && $totalUpdates < 6000 && $updatedCount);
	}
}