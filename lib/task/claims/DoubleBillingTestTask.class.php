<?php

class DoubleBillingTestTask extends sfBaseTask
{
	/**
	 * HOW TO RUN THIS COMMAND:
	 * symfony hype:doubleBillingTest --clientID=2 --userID=-1 --doctorID=1
	 * symfony hype:doubleBillingTest --clientID=2 --userID=-2 --doctorID=1
	 * symfony hype:doubleBillingTest --clientID=2 --userID=-3 --doctorID=1
	 * symfony hype:doubleBillingTest --clientID=2 --userID=-4 --doctorID=1
	 */

	private $clientID;
	private $doctorID;
	private $limit = 100;
	private $userID;

	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'doubleBillingTest';
		$this->briefDescription = 'try to duplicate the concurrent save issue that is happening with items';

		$this->addOptions(array(
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
			new sfCommandOption('clientID', null, sfCommandOption::PARAMETER_REQUIRED, 'Client ID'),
			new sfCommandOption('userID', null, sfCommandOption::PARAMETER_REQUIRED, 'User ID'),
			new sfCommandOption('doctorID', null, sfCommandOption::PARAMETER_REQUIRED, 'Doctor ID'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getDoctrineConnection();

		$this->clientID = $options['clientID'];
		$this->doctorID = $options['doctorID'];
		$this->userID = $options['userID'];

		for ($a = 1; $a <= $this->limit; $a++)
		{
 			$this->saveClaim($a, $connection);
		}
	}
	private function saveClaim($c, $con)
	{
// 		$lockingManager = new Doctrine_Locking_Manager_Pessimistic($con);

		$claim = new Claim();
		$claim->setClientId($this->clientID);
		$claim->setCreatedBy($this->userID);
		$claim->setDoctorID($this->doctorID);
// 		$claim->save();

		$item = new ClaimItem();
		$item->setClientId($this->clientID);
		$item->setCreatedBy($this->userID);
		$item->setItemId(1);
//  	$item->setClaimId((int) $claim->getClaimId());
// 		$item->setClaim($claim);

		$claim->ClaimItem->add($item);


		try {
// 			$lockingManager->releaseAgedLocks(300);
// 			$lockingManager->getLock($claim, 'DoubleBilling' . $this->userID);

//  			$isolation = $con->transaction;
//  			$isolation->setIsolation('READ COMMITTED');
 			$con->beginTransaction();


//  			sleep(1);

//  			echo microtime() . ' save start' . "\n";
			$claim->save();
//  			echo microtime() . ' save end' . "\n";

//  			$con->commit();
//  			$lockingManager->releaseLock($claim, 'DoubleBilling' . $this->userID);
		}
		catch (Exception $e) {
			throw $e;
		}
// 		exit();
	}
}