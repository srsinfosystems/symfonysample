<?php

class UnsubmitUnpaidClaimsTask extends sfBaseTask
{
	/**
	 * HOW TO RUN THIS COMMAND:
	 * - clientID is the clientID that you want to use
	 * - endDate is the claim create date range end point [FORMAT: yyyy-mm-dd]
	 * - startDate is the claim create date range start point [FORMAT: yyyy-mm-dd]
	 *
	 * symfony hype:unsubmitUnpaidClaims --clientID=2 --startDate="2013-01-01" --endDate="2013-06-30"
	 */
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'unsubmitUnpaidClaims';
		$this->briefDescription = 'Moves all Unpaid Claims to the unsubmitted status.';

		$this->addOptions(array(
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
			new sfCommandOption('clientID', null, sfCommandOption::PARAMETER_REQUIRED, 'client ID', null),
			new sfCommandOption('startDate', null, sfCommandOption::PARAMETER_REQUIRED, 'the claim create date range start point', null),
			new sfCommandOption('endDate', null, sfCommandOption::PARAMETER_REQUIRED, 'the claim create date range end point', null),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		if (!array_key_exists('clientID', $options)) {
			echo '--clientID is required' . "\n";
			exit();
		}

		if (!array_key_exists('startDate', $options)) {
			echo '--startDate is required' . "\n";
			exit();
		}
		try {
			$options['startDate'] = date(hype::DB_DATE_FORMAT, hype::parseDateToInt($options['startDate'], hype::DB_DATE_FORMAT));
		}
		catch (Exception $e) {
			echo 'startDate is an invalid date. (Format: yyyy-mm-dd)' . "\n";
		}

		if (!array_key_exists('endDate', $options)) {
			echo '--endDate is required';
			exit();
		}
		try {
			$options['endDate'] = date(hype::DB_DATE_FORMAT, hype::parseDateToInt($options['endDate'], hype::DB_DATE_FORMAT));
		}
		catch (Exception $e) {
			echo 'endDate is an invalid date. (Format: yyyy-mm-dd)' . "\n";
		}

		$clientId = $options['clientID'];
		$startDate = $options['startDate'];
		$endDate = $options['endDate'];

		$terms = array(
			'client_id' => $options['clientID'],
			'start_date' => $options['startDate'],
			'end_date' => $options['endDate'],
			'pay_prog' => array('HCP - P', 'RMB'),
			'limit' => 100,
		);

 		$totalClaims = 0;

  		do {
			$itemCount = ClaimItemTable::unsubmitUnpaidSubmittedClaims($terms);
			if ($itemCount) {
				echo 'Moved ' . $itemCount . ' claims to Unsubmitted status.' . "\n";
				$totalClaims += $itemCount;
			}
  		} while ($itemCount && $totalClaims < 3000);

  		echo "\n" . 'Moved a total of ' . $totalClaims . ' Claim Items to the Unsubmitted status' . "\n";
		echo ClaimItemTable::getUnpaidSubmittedClaimItemsCount($terms) . ' Unpaid Claim Items remain submitted' . "\n";
	}
}