<?php

class MoveRaInfoToItemsTask extends sfBaseTask
{
	/**
	 * HOW TO RUN THIS COMMAND:
	 * symfony hype:moveRaInfoToItems
	 */
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'moveRaInfoToItems';
		$this->briefDescription = 'Moves RA data (MOH Claim ID, Reconcile ID, Reconcile File Name) to Claim Items table.';

		$this->addOptions(array(
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$minID = 0;
		$count = 0;
		$limit = 2000;
		do {
			$itemIDs = Doctrine_Query::create()
				->select('DISTINCT ci.id')
				->from('ClaimItem ci')
				->leftJoin('ci.RaItem ri')
				->addWhere('ci.id > (?)', $minID)
				->addWhere('ci.reconcile_id IS NULL')
				->addWhere('ri.id IS NOT NULL')
				->addWhere('ri.ra_claim_type = (?)', 1)
				->limit(100)
				->orderBy('ci.id asc')
				->execute(array(), Doctrine::HYDRATE_SCALAR);

			foreach ($itemIDs as $rs)
			{
				$count++;
				$minID = $rs['ci_id'];
				echo 'Checking for Reconcile Data for Item ID ' . $minID . ' .... ';
				$reconcileID = $this->updateClaimItem($rs['ci_id']);

				if ($reconcileID) {
					echo ' Set Reconcile ID to ' . $reconcileID . "\n";
				}
				else {
					echo "\n";
				}
			}
		} while (count($itemIDs) && $count <= $limit);
	}

	private function updateClaimItem($claimItemID)
	{
		$raItems = Doctrine_Query::create()
			->from('RaItem r')
			->leftJoin('r.RaFile rf')
			->leftJoin('rf.Reconcile rec')
			->addWhere('r.item_id = (?)', $claimItemID)
			->addWhere('r.amount_paid > (?)', 0)
			->addWhere('r.ra_claim_type = (?)', 1)
			->orderBy('r.created_at asc')
			->execute(array(), Doctrine::HYDRATE_ARRAY);

		$claimItem = Doctrine_Query::create()
			->from('ClaimItem ci')
			->leftJoin('ci.Claim c')
			->addWhere('ci.id = (?)', $claimItemID)
			->fetchOne();

		if (count($raItems) && $claimItem instanceof ClaimItem) {
			$claimItem->setMohClaim($raItems[0]['claim_num']);
			$claimItem->setRa($raItems[0]['RaFile']['Reconcile']['file_name']);
			$claimItem->setReconcileId($raItems[0]['RaFile']['reconcile_id']);
			$claimItem->save();

			return ($claimItem->getReconcileId());
		}

		$raItems = Doctrine_Query::create()
			->from('RaItem r')
			->leftJoin('r.RaFile rf')
			->leftJoin('rf.Reconcile rec')
			->addWhere('r.item_id = (?)', $claimItemID)
			->addWhere('r.ra_claim_type = (?)', 1)
			->addWhere('r.amount_paid >= (?)', 0)
			->orderBy('r.created_at asc')
			->execute(array(), Doctrine::HYDRATE_ARRAY);

		if (count($raItems) && $claimItem instanceof ClaimItem) {
			$claimItem->setMohClaim($raItems[0]['claim_num']);
			$claimItem->setRa($raItems[0]['RaFile']['Reconcile']['file_name']);
			$claimItem->setReconcileId($raItems[0]['RaFile']['reconcile_id']);
			$claimItem->save();

			return ($claimItem->getReconcileId());
		}
	}
}