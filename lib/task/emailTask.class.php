<?php

class emailTask extends sfBaseTask
{
	protected function configure()
  {

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));

    $this->namespace        = 'test';
    $this->name             = 'email';
    $this->briefDescription = 'Email Test';
    $this->detailedDescription = <<<EOF
The [email|INFO] task does things.
Call it with:

  [php symfony email|INFO]
EOF;
  }
	protected function execute($arguments = array(), $options = array())
  {
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
  	// initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

    // add your code here

	$email = new hypeEmail();
	$email->writeEmail(682,true);

	//hypeEmail::writeEmail();

  }
}
