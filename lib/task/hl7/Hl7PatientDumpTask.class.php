<?php

class Hl7PatientDumpTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace        = 'hl7';
		$this->name             = 'patientDump';
		$this->briefDescription = 'Patient Dump';
		$this->detailedDescription = 'detailed description';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
			new sfCommandOption('client_id', null, sfCommandOption::PARAMETER_REQUIRED, 'The client id'),
			new sfCommandOption('greater_id', null, sfCommandOption::PARAMETER_REQUIRED, 'The minimum id'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$partners = array();
		$hl7_queues = new Doctrine_Collection('Hl7Queue');
		$count = 0;
		$max_id = null;

		if (!array_key_exists('client_id', $options) || !array_key_exists('greater_id', $options)) {
			echo 'Required parameters: client_id & greater_id' . "\n";
			exit();
		}

    	$client = Doctrine_Query::create()
    		->from('Client c')
    		->addWhere('c.id = (?)', $options['client_id'])
    		->fetchOne();

    	if ($client instanceof Client) {
			$partners = $client->hl7_outbound_partners;
			$partners = strpos($partners, ';') !== false ? explode(';', $partners) : $partners = array($partners);
    	}
    	else {
    		echo 'Unknown client ID' . "\n";
    		exit();
    	}

    	if (!count($partners)) {
    		echo 'Client has no HL7 Partners' . "\n";
    		exit();
    	}

		$patients = Doctrine_Query::create()
			->from('Patient p')
			->addWhere('p.client_id = ?', $options['client_id'])
			->addWhere('p.id > (?)', $options['greater_id'])
			->addWhere('p.deleted = (?)', false)
			->orderby('p.id')
			->limit(1000)
			->execute(array(), Doctrine::HYDRATE_ARRAY);

		foreach ($patients as $patient)
		{
			foreach ($partners as $partner)
			{
				$hl7_queue = new HL7Queue();
				$hl7_queue->client_id = $patient['client_id'];
				$hl7_queue->version_number = $patient['version'];
				$hl7_queue->message_type = 'ADT';
				$hl7_queue->message_subtype = HL7OutboundProcessorBase::getADTSubtype(false);
				$hl7_queue->model_id = $patient['id'];
				$hl7_queue->model_type = 'PATIENT';
				$hl7_queue->status = HL7QueueTable::STATUS_CREATED_VIA_DUMP;
				$hl7_queue->partner_name = $partner;

				$hl7_queues->add($hl7_queue);
				$count++;
				$max_id = $patient['id'];
			}
		}
		$hl7_queues->save();

		echo 'count:' . $count . '     maxid:' . $max_id . "\n";
	}
}