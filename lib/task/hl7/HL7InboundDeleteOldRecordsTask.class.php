<?php

// TODO: add to crontab once a day; will only really run once or twice a month but the low count means it won't hog resources

class HL7InboundDeleteOldRecordsTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace        = 'hl7';
		$this->name             = 'deleteOldInboundRecords';
		$this->briefDescription = 'Delete Old Inbound Records [updated_at older than the first day of the previous month]';
		$this->detailedDescription = 'Deletes records from the HL7Inbound table that are older than the first day of the previous month';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
		));
	}

	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$maxRecords = 1000;
		$count = 1;

		$date = new DateTime();
		$date->setDate(date('Y'), date('m') - 1, 1)
			->setTime(0, 0, 0);

 		do {
			$hl7Records = Doctrine_Query::create()
				->from('HL7Inbound h')
				->addWhere('h.updated_at < (?)', $date->format(hype::DB_ISO_DATE))
				->limit(50)
				->execute();

			foreach ($hl7Records as $hl7)
			{
				echo $count . ': Deleting ID ' . $hl7->getId() . "\n";
				$hl7->delete();

				$count++;
			}

 		} while (count($hl7Records) && $count <= $maxRecords);
	}
}