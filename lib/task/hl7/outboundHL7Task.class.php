<?php

class outboundHl7Task extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hl7';
		$this->name = 'outbound';
		$this->briefDescription = 'Processes one inbound HL7 message.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();
		$limit = 10;
		$inbound_options = sfConfig::get('app_hl7_partners', array());

		$hl7_queues = HL7QueueTable::getOldestUnprocessed($limit);
		if (!$hl7_queues) {
			return;
		}

		foreach ($hl7_queues as $hl7_queue)
		{
			$hl7_queue->setStatus(HL7QueueTable::STATUS_PROCESSING);
			$hl7_queue->save();

			switch ($hl7_queue->message_type) {
				case 'ADT':
					$function = 'processADT';

					$object = Doctrine_Query::create()
						->from('Patient p')
						->addWhere('p.id = (?)', $hl7_queue->model_id)
						->addWhere('p.client_id = (?)', $hl7_queue->client_id)
						->addWhere('p.deleted = (?)', false)
						->fetchOne();

					if (!$object instanceof Patient) {
						$hl7_queue->status = HL7QueueTable::STATUS_ERRORS;
						$hl7_queue->errors = 'Cannot retrieve patient record ' . $hl7_queue->model_id;
						$hl7_queue->save();
						return;
					}

					break;

				case 'MFN':
					$function = 'processMFN';

					$object = Doctrine_Query::create()
						->from('ReferringDoctor rd')
						->addWhere('rd.id = (?)', $hl7_queue->model_id)
						->addWhere('rd.client_id = (?)', $hl7_queue->client_id)
						->fetchOne();

					if (!$object instanceof ReferringDoctor) {
						$hl7_queue->status = HL7QueueTable::STATUS_ERRORS;
						$hl7_queue->errors = 'Cannot retrieve referring doctor record ' . $hl7_queue->model_id;
						$hl7_queue->save();
						return;
					}

					break;

				case 'SIU':
					$function = 'processSIU';

					$object = Doctrine_Query::create()
						->from('AppointmentAttendee att')
						->leftJoin('att.Appointment a')
						->leftJoin('a.AppointmentType at')
						->addWhere('att.id = (?)', $hl7_queue->model_id)
						->addWhere('a.client_id = (?)', $hl7_queue->client_id)
						->addWhere('att.client_id = (?)', array($hl7_queue->client_id))
						->fetchOne();

					if (!$object instanceof AppointmentAttendee) {
						$hl7_queue->status = HL7QueueTable::STATUS_ERRORS;
						$hl7_queue->errors = 'Cannot retrieve appointment attendee record ' . $hl7_queue->model_id;
						$hl7_queue->save();
						return;
					}
					break;

				default:
					$hl7_queue->status = HL7QueueTable::STATUS_ERRORS;
					$hl7_queue->errors = 'Unsupported Message Type ' . $hl7_queue->message_type;
					$hl7_queue->save();
					return;
			}

			$class = 'HL7OutboundProcessor';

			if (array_key_exists($hl7_queue->partner_name, $inbound_options)) {
				$class = 'HL7OutboundProcessor' . $inbound_options[$hl7_queue->partner_name];
			}
			if (!class_exists($class)) 	{
				$hl7_queue->status = HL7QueueTable::STATUS_ERRORS;
				$hl7_queue->errors = 'Unknown HL7 Outbound Processor ' . $class;
				$hl7_queue->save();
				return;
			}

			$outboundProcessor = new $class();
			$outboundProcessor->setHl7Queue($hl7_queue);
			$outboundProcessor->setMessageSubtype($hl7_queue->message_subtype);
			$outboundProcessor->$function($object, null);

		}

	}
}
