<?php

class Hl7AppointmentDumpTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace        = 'hl7';
		$this->name             = 'appointmentDump';
		$this->briefDescription = 'Appointment Dump';
		$this->detailedDescription = 'detailed description';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
			new sfCommandOption('client_id', null, sfCommandOption::PARAMETER_REQUIRED, 'The client id'),
			new sfCommandOption('greater_id', null, sfCommandOption::PARAMETER_REQUIRED, 'The minimum id'),
			new sfCommandOption('exclude_cancels', null, sfCommandOption::PARAMETER_REQUIRED, 'Whether to exclude cancellations'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$partners = array();
		$hl7_queues = new Doctrine_Collection('Hl7Queue');
		$count = 0;
		$max_id = null;
		$no_cancellations = null;

		if (!array_key_exists('client_id', $options) || !array_key_exists('greater_id', $options) || !array_key_exists('exclude_cancels', $options)) {
			echo 'Required parameters: client_id, greater_id & exclude_cancels' . "\n";
			exit();
		}

		if ($options['exclude_cancels'] == 'false' || $options['exclude_cancels'] == '0') {
			$no_cancellations = false;
		}
		else if ($options['exclude_cancels'] == 'true' || $options['exclude_cancels'] == '1') {
			$no_cancellations = true;
		}

		if (is_null($no_cancellations)) {
			echo 'exclude_cancels must be true or false' . "\n";
			exit();
		}

    	$client = Doctrine_Query::create()
    		->from('Client c')
    		->addWhere('c.id = (?)', $options['client_id'])
    		->fetchOne();

    	if ($client instanceof Client) {
			$partners = $client->hl7_outbound_partners;
			$partners = strpos($partners, ';') !== false ? explode(';', $partners) : $partners = array($partners);
    	}
    	else {
    		echo 'Unknown client ID' . "\n";
    		exit();
    	}

    	if (!count($partners)) {
    		echo 'Client has no HL7 Partners' . "\n";
    		exit();
    	}

		$query = Doctrine_Query::create()
			->from('AppointmentAttendee att')
			->leftJoin('att.Appointment a')
			->addWhere('a.client_id = ?', $options['client_id'])
			->addWhere('att.client_id = (?)', array($options['client_id']))
			->addWhere('att.id > (?)', $options['greater_id'])
			->addWhere('a.status != (?)', AppointmentTable::STATUS_DELETED);

		if ($no_cancellations) {
			$query->addWhere('a.status != (?)', AppointmentTable::STATUS_CANCELLED_BY_LOCATION)
				->addWhere('a.status != (?)', AppointmentTable::STATUS_CANCELLED_BY_PATIENT);
		}

		$attendees = $query->orderby('att.id')
			->limit(1000)
			->execute(array(), Doctrine::HYDRATE_ARRAY);

		foreach ($attendees as $attendee)
		{
			foreach ($partners as $partner)
			{
				$hl7_queue = new HL7Queue();
				$hl7_queue->client_id = $attendee['Appointment']['client_id'];
				$hl7_queue->version_number = $attendee['version'];
				$hl7_queue->message_type = 'SIU';
				$hl7_queue->message_subtype = HL7OutboundProcessorBase::getSIUSubtype($attendee['Appointment']['status'], false);
				$hl7_queue->model_id = $attendee['id'];
				$hl7_queue->model_type = 'APPT';
				$hl7_queue->status = HL7QueueTable::STATUS_CREATED_VIA_DUMP;
				$hl7_queue->partner_name = $partner;

				$hl7_queues->add($hl7_queue);
				$count++;
				$max_id = $attendee['id'];
			}
		}

		$hl7_queues->save();

		echo 'count:' . $count . '     maxid:' . $max_id . "\n";

	}
}