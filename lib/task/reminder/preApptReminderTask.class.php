<?php

// ONCE A DAY
class preApptReminderTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace        = 'reminder';
		$this->name             = 'preAppt';
		$this->briefDescription = 'create pre-appointment reminder events';
		$this->detailedDescription = 'detailed description';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$ars = new Doctrine_Collection('ReminderEvent');

		$reminder_templates = Doctrine_Query::create()
			->from('ReminderTemplate rt')
			->addWhere('rt.event_trigger = (?)', ReminderTemplateTable::TRIGGER_APPT_REMINDER)
			->addWhere('rt.active = (?)', true)
			->execute();

		foreach ($reminder_templates as $reminder_template)
		{
			$appt_types = $reminder_template->getAppointmentTypesAsArray();
			$date = $reminder_template->getAppointmentDateToRemind();

			$att_q = Doctrine_Query::create()
				->from('AppointmentAttendee att')
				->leftJoin('att.Appointment a')
				->addWhere('a.client_id = (?)', $reminder_template->client_id)
				->addWhere('att.client_id = (?)', array($reminder_template->getClientId()))
				->addWhere('a.status = (?)', AppointmentTable::STATUS_POSTED)
				->addWhere('a.slot_type = (?)', AppointmentTable::SLOT_TYPE_NORMAL)
				->addWhere('a.start_date >= (?)', $date . ' 00:00:00')
				->addWhere('a.end_date <= (?)', $date . ' 23:59:59');

			if (!is_null($appt_types)) {
				$att_q->whereIn('a.appointment_type_id', $appt_types);
			}
			$attendees = $att_q->execute(array(), Doctrine::HYDRATE_ARRAY);

			foreach ($attendees as $attendee)
			{
				$ar = new AttendeeReminderEvent();
				$ar->client_id = $attendee['Appointment']['client_id'];
				$ar->event_type = AttendeeReminderEventTable::EVENT_TYPE_PRE_APPT_REMINDER;
				$ar->status = AttendeeReminderEventTable::STATUS_UNPROCESSED;
				$ar->record_id = $attendee['id'];
				$ar->reminder_template_id = $reminder_template['id'];

				$ars->add($ar);
			}
		}

		$ars->save();
	}
}
