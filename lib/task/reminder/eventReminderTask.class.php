<?php

class eventReminderTask extends sfBaseTask
{
	private $DEBUG = false;

	protected function configure()
	{
		$this->namespace        = 'reminder';
		$this->name             = 'sendEmail';
		$this->briefDescription = 'send Event Reminder emails';
		$this->detailedDescription = 'detailed description';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
		));
	} 
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$event_reminders = Doctrine_Query::create()
			->from('ReminderEvent re')
			->addWhere('re.status = (?)', array(ReminderEventTable::STATUS_UNPROCESSED))
			->limit(100)
			->execute();

		foreach ($event_reminders as $event_reminder)
		{
			if ($event_reminder instanceof AttendeeReminderEvent) {
				if ($event_reminder->reminder_template_id) {
					$reminder_templates = Doctrine_Query::create()
						->from('ReminderTemplate rt')
						->addWhere('rt.client_id = (?)', array($event_reminder->client_id))
						->addWhere('rt.id = (?)', array($event_reminder->reminder_template_id))
						->addWhere('rt.active = (?)', array(true))
						->execute();
				}
				else {
					$reminder_templates = Doctrine_Query::create()
						->from('ReminderTemplate rt')
						->addWhere('rt.client_id = (?)', array($event_reminder->client_id))
						->addWhere('rt.event_trigger = (?)', array(ReminderTemplateTable::getEventTriggerFromEventType($event_reminder->event_type)))
						->addWhere('rt.active = (?)', array(true))
						->execute();
				}

				foreach ($reminder_templates as $reminder_template)
				{
					if ($reminder_template->AppointmentTypesMatch($event_reminder->AppointmentAttendee->Appointment->appointment_type_id)) {
						$email_addresses = $reminder_template->getAttendeeEmailAddresses($event_reminder->AppointmentAttendee);

						foreach ($email_addresses as $key => $email)
						{
							if ($email) {
								$error = $this->sendAttendeeEmail(
										$event_reminder->AppointmentAttendee,
										$reminder_template,
										$email
								);

								if (!is_null($error)) {
                                    $event_reminder->error_msg = $event_reminder->error_msg . $error;
								} else {
									$event_reminder->email_text = $reminder_template->getAttendeeMessageText($event_reminder->AppointmentAttendee);
								}
							}
							else if ($key == 'patient' && $reminder_template->follow_up_patients) {
								$followup = $this->createFollowUp($event_reminder->AppointmentAttendee, $reminder_template);
								$followup->save();
							}

						}
					}
				}

				$event_reminder->status = ReminderEventTable::STATUS_PROCESSED;
				$event_reminder->save();
			}
		}
	}

	/**
	 * @param AppointmentAttendee
	 * @param ReminderTemplate
	 * @param array
	 * @return null|string
	 */
	protected function sendAttendeeEmail($attendee, ReminderTemplate $template, $emails)
	{
		if ($this->DEBUG) {
			return $this->sendDebugEmail($attendee, $template);
		}
		$email_text = $template->getAttendeeMessageText($attendee);
		$security = $template->Client->email_secure_server;
		$transport = Swift_SmtpTransport::newInstance($template->Client->email_server, $template->Client->email_port ? $template->Client->email_port : '25')
			->setUsername($template->Client->email_username)
			->setPassword($template->Client->email_password);

		if ($security == 2) {
			$transport->setEncryption('tls');
		}
		else if ($security == 1) {
			$transport->setEncryption('ssl');
		}

		try {
			$mailer = Swift_Mailer::newInstance($transport);
			$message = Swift_Message::newInstance()
				->setSubject($template->subject)
				->setBody($email_text)
				->addPart($email_text,'text/html');

				$message->setFrom(array($template->Client->email_username => $template->Client->email_from));
				$message->setTo($emails);

			$mailer->send($message);
		}
		catch (Exception $e) {
			return $e->getMessage();
		}
		return null;
	}

	/**
	 * @param AppointmentAttendee
	 * @param ReminderTemplate
	 * @return null|string
	 */
	protected function sendDebugEmail(AppointmentAttendee $attendee, ReminderTemplate $template)
	{
		echo 'sendDebugEmail' . "\n";
		$email_text = $template->getAttendeeMessageText($attendee);

		$username = 'support@hypesystems.com';
		$password = '0w3n4l0n';
		$smtp = 'mail.hypesystems.com';
		$port = 25;
		$from = array('support@hypesystems.com' => 'Hype Support - Event Reminder Debug');
		$to = array('may@hypesystems.com');
		$subject = 'Hype Medical - Event Reminder Debugging';

		$transport = Swift_SmtpTransport::newInstance($smtp, $port)
			->setUsername($username)
			->setPassword($password);
		$mailer = Swift_Mailer::newInstance($transport);

		$message = Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom($from)
			->setTo($to)
			->setBody($email_text)
			->addPart($email_text,'text/html');

		try {
			$mailer->send($message);
		}
		catch (Exception $e) {
			return $e->getMessage();
		}
		return null;
	}

	/**
	 * @param AppointmentAttendee
	 * @param ReminderTemplate
	 * @return FollowUp
	 */
	protected function createFollowUp(AppointmentAttendee $attendee, ReminderTemplate $template)
	{
		$email_text = strip_tags(html_entity_decode($template->getAttendeeMessageText($attendee)));

		$follow_up = new FollowUp();
        $follow_up->client_id = $attendee->Patient->client_id;
        $follow_up->location_id = $attendee->Appointment->location_id;
        $follow_up->patient_id = $attendee->patient_id;
        $follow_up->note = $email_text;
        $follow_up->due_date = date('Y-m-d 00:00:00');
        $follow_up->completed = false;
        $follow_up->appointment_attendee_id = $attendee->get('id');

        return $follow_up;
	}
}
