<?php

class setPatientFacilityIDTask extends sfBaseTask
{
    private $patientsCount = 0;
    private $patientsLimit = 10000;

    public function execute($arguments = array(), $options = array())
    {
        try {
            sfContext::createInstance($this->configuration);
            $this->executeTask($arguments, $options);
        }
        catch (Exception $e) {
            $this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
            throw $e;
        }
    }

    private function executeTask($arguments, $options)
    {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase('doctrine')->getConnection();

        $minID = 0;
        if (array_key_exists('minID', $options)) {
            $minID = intval($options['minID']);
        }

        do {
            $facility = $this->getFacilityByMinID($minID);

            if ($facility instanceof Facility) {
                $this->updatePatients($facility);

                $minID = $facility->get('id');
            }
        } while ($facility instanceof Facility);
    }

    private function getFacilityByMinID($id)
    {
        return Doctrine_Query::create()
            ->from('Facility f')
            ->addWhere('f.id > (?)', array($id))
            ->limit(1)
            ->addOrderBy('f.id asc')
            ->fetchOne();
    }

    private function updatePatients(Facility $facility)
    {
        echo 'Updating patients with Facility Number ' . $facility->getFacilityMasterNumber() . '  [' . $facility->get('id') . ']' . "\n";
        do {
            $patients = Doctrine_Query::create()
                ->from('Patient p')
                ->addWhere('p.facility_num = (?)', array($facility->getFacilityMasterNumber()))
                ->addWhere('p.facility_id IS NULL')
                ->limit(200)
                ->execute();

            foreach ($patients as $key => $patient) {
                $patients[$key]->setFacilityId($facility->get('id'));
            }
            $patients->save();
            $this->patientsCount += $patients->count();

            echo '  updated ' . $patients->count() . ' patients.' . "\n";

            if ($this->patientsCount > $this->patientsLimit) {
                exit();
            }
        } while (count($patients));
    }

    protected function configure()
    {
        $this->namespace = 'updateDataModel';
        $this->name = '2015-03-setPatientFacilityID';
        $this->briefDescription = "Fills in the facility ID in the patients table";

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('minID', null, sfCommandOption::PARAMETER_REQUIRED, 'Minimum ID to start on', 0),
        ));
    }
}