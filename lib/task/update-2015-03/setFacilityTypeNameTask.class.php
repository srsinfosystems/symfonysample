<?php

class setFacilityTypeNameTask extends sfBaseTask
{
    private $facilityTypes = array();

    public function execute($arguments = array(), $options = array())
    {
        try {
            sfContext::createInstance($this->configuration);
            $this->executeTask($arguments, $options);
        }
        catch (Exception $e) {
            $this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
            throw $e;
        }
    }

    private function executeTask($arguments, $options)
    {
//        $databaseManager = new sfDatabaseManager($this->configuration);
//        $connection = $databaseManager->getDatabase('doctrine')->getConnection();
//
        $minID = 0;
        if (array_key_exists('minID', $options)) {
            $minID = intval($options['minID']);
        }

        do {
            $facility = $this->getFacilityByMinID($minID);

            if ($facility instanceof Facility && !$facility->getFacilityTypeName()) {
                $this->updateFacility($facility);
                $minID = $facility->get('id');
            }
        } while ($facility instanceof Facility);
    }

    private function getFacilityByMinID($id)
    {
        return Doctrine_Query::create()
            ->from('Facility f')
            ->addWhere('f.id > (?)', array($id))
            ->addWhere('f.facility_type_name is null')
            ->limit(1)
            ->addOrderBy('f.id asc')
            ->fetchOne();
    }

    private function updateFacility(Facility $facility)
    {
        $facilityTypeName = null;
        if (!array_key_exists($facility->getFacilityType(), $this->facilityTypes)) {
            $facilityType = $this->getFacilityType($facility->getFacilityType());

            $this->facilityTypes[$facility->getFacilityType()] = $facilityType->getName();
            $facilityTypeName = $facilityType->getName();
        }
        else {
            $facilityTypeName = $this->facilityTypes[$facility->getFacilityType()];
        }


        if ($facilityTypeName) {
            $facility->setFacilityTypeName($facilityTypeName);
            $facility->save();

            echo 'Updated Facility ' . $facility->get('id') . ' with facility type ' . $facilityTypeName . "\n";
        }
        else {
            echo 'Cannot update facility ' . $facility->get('id') . ' because there is not matching facility type' . "\n";
        }
    }

    private function getFacilityType($val)
    {
        echo 'Querying for Facility Type ' . $val . "\n";

        return Doctrine_Query::create()
            ->from('FacilityType f')
            ->addWhere('f.code = (?)', $val)
            ->limit(1)
            ->fetchOne();

    }

    protected function configure()
    {
        $this->namespace = 'updateDataModel';
        $this->name = '2015-03-setFacilityTypeName';
        $this->briefDescription = "Fills in the facility type name in the Facility table";

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('minID', null, sfCommandOption::PARAMETER_REQUIRED, 'Minimum ID to start on', 0),
        ));
    }
}