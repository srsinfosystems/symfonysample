<?php

class CVIAInitialImporterFixTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'CVIAImportFix';
		$this->briefDescription = 'Fixes CVIA Importer Fix.';

		$this->addOptions(array(
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
				new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		do {
			$dataImports = Doctrine_Query::create()
				->from('DataImporter d')
				->addWhere('d.importer_name = (?)', 'CVIAInitialImport')
				->addWhere('d.status = (?)', 0)
				->limit(100)
				->execute();

			if (count($dataImports)) {
				foreach ($dataImports as $import)
				{
					try {
						$connection->beginTransaction();
						$data = $this->parseData($import->getDataText());
						$patient = null;

						if ($data['hcn']) {
							$patient = Doctrine_Query::create()
								->from('Patient p')
								->addWhere('p.hcn_num = (?)', hype::encryptHCN($data['hcn'] . ' '))
								->addWhere('p.client_id = (?)', $import->getClientId())
								->fetchOne();

							if (!$patient instanceof Patient) {
								$patient = Doctrine_Query::create()
									->from('Patient p')
									->addWhere('p.hcn_num = (?)', hype::encryptHCN($data['hcn']))
									->addWhere('p.client_id = (?)', $import->getClientId())
									->fetchOne();
							}

							if (!$patient instanceof Patient) {
								$patient = Doctrine_Query::create()
									->from('Patient p')
									->addWhere('p.fname  = (?)', $data['fname'])
									->addWhere('p.lname  = (?)', $data['lname'])
									->addWhere('p.patient_number = (?)', $data['patient_number'])
									->addWhere('p.client_id = (?)', $import->getClientId())
									->fetchOne();
							}
							if ($patient instanceof Patient) {
								echo 'Matched Patient ID ' . $patient->id . "\n";

								$patient->fromArray($data);
								$patient->setDeleted(false);
								$patient->setHcnNum(hype::encryptHcn($data['hcn']));

								$patient->save();
								$import->setStatus(10)->save();
							}
							else {
								echo 'No Patient Found ' . $data['fname'] . ' ' . $data['lname']  . ' ' . $data['hcn'] . "\n";
								$import->setStatus(1)->save();
							}
						}
						else {
							echo 'No Health Card Number ' . $data['fname'] . ' ' . $data['lname']  . ' ' . $data['hcn'] . "\n";
							$import->setStatus(2)->save();
						}

						$connection->commit();
					}
					catch (Exception $e) {
						$connection->rollback();
						throw $e;
					}
				}
			}

		} while (count($dataImports));
	}
	private function parseData($data)
	{
		$rs = array(
				'patient_number' => trim(substr($data, 0, 9)),
				'lname' => trim(substr($data, 9, 24)),
				'fname' => trim(substr($data, 33, 19)),
				'sex' => trim(substr($data, 52, 1)),
				'dob' => date(hype::DB_DATE_FORMAT, hype::parseDateToInt(trim(substr($data, 53, 8)), 'Ymd')),
				'address' => trim(substr($data, 82, 34)),
				'city' => trim(substr($data, 116, 23)),
				'prov' => trim(substr($data, 139, 4)),
				'postal' => trim(substr($data, 143, 11)),
				'hphone' => trim(substr($data, 154, 10)),
				'wphone' => trim(substr($data, 164, 10)),
				'hcn' => trim(substr($data, 180, 10))
		);

		if ($rs['hcn'] == '0000000000') {
			$rs['hcn'] = '';
		}

		return $rs;
	}
}