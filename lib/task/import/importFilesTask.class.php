<?php
class importFilesTask extends sfBaseTask
{
	private $client_id = 2;
	private $location_id = 1;

	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'importFiles';
		$this->briefDescription = "Imports CSV files to Hype Medical .";

		$this->addOptions(array(
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend')));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$fileToImport = 'C:\\dev\\DonnaCurtis\\WR130326.BAK';
		$fileType = strtoupper(substr($fileToImport,strrpos($fileToImport,'.')));
		echo $fileType . "\n";
		$dat_file = null;
		if ($fileType=='.TXT') {
			$dat_file = new importTXTFiles($fileToImport, $this->client_id, $this->location_id);
		} else {
			if ($fileType == '.XLSX' || $fileType =='.XLS') {
				$dat_file = new importExcelFiles($fileToImport, $this->client_id, $this->location_id);
				$this->checked = false;
			}else {
				if ($fileType == '.BAK'){
					$dat_file = new importBAKFiles($fileToImport, $this->client_id, $this->location_id);
					$dat_file->setIsATask(true);
				}
			}
		}
		//exit();
		if($dat_file){
			$dat_file->setDebug(true);
			$dat_file->run();
		}else{
			echo 'problemsssss....';
		}
	}
}