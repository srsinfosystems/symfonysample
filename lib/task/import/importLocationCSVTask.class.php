<?php

class importLocationCSVTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'importLocationCSV';
		$this->briefDescription = 'Imports Location CSV formatted file.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('fileName', null, sfCommandOption::PARAMETER_REQUIRED, 'Location File name & path'),
			new sfCommandOption('lineNumber', null, sfCommandOption::PARAMETER_OPTIONAL, 'Line number to begin processing at', 0),
			new sfCommandOption('clientID', null, sfCommandOption::PARAMETER_REQUIRED, 'Client ID'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$starting_line = ($options['lineNumber']) ? $options['lineNumber'] : 0;
		$line_number = 0;
		$headers = array();

		if (!file_exists($options['fileName'])) {
			echo 'this file is whack';
			exit();
		}
		if (!$options['clientID']) {
			echo 'no client ID provided';
			exit();
		}
		$this->client_id = $options['clientID'];
		$this->regions = array();

		$handle = @fopen($options['fileName'], "rw");
		if ($handle) {
			try {
				while (($buffer = fgets($handle, 4096)) !== false) {
					$buffer = explode(';', trim($buffer));
					if ($line_number == 0) {
						$headers = array_flip($buffer);
					}
					else if ($line_number >= $starting_line) {
						$id = $this->processLine($buffer, $headers);
						echo 'Processed Line ' . $line_number . ': ' . $id . "\n";

					}
					$line_number++;
				}
				if (!feof($handle)) {
					echo "Error: unexpected fgets() fail\n";
				}
			}
			catch (Exception $e) {
				print_r ($e->getMessage());
				echo "unable to complete processing file.\n";
			}
			fclose($handle);
		}
	}
	protected function processLine($columns, $headers)
	{
		if ($columns[$headers['code']] || $columns[$headers['name']]) {
			$location = Doctrine_Query::create()
				->from('Location l')
				->addWhere('l.code = (?)', $columns[$headers['code']])
				->addWhere('l.name = (?)', $columns[$headers['name']])
				->addWhere('l.client_id = (?)', $this->client_id)
				->fetchOne();

			if (!$location instanceof Location) {
				$location = new Location();
				$location->region_id = $this->getRegionId(trim(strtoupper($columns[$headers['region_name']])));
				$location->client_id = $this->client_id;
				$location->name = trim(strtoupper($columns[$headers['name']]));
				$location->code = trim(strtoupper($columns[$headers['code']]));
				$location->address1 = trim(strtoupper($columns[$headers['address1']]));
				$location->city = trim(strtoupper($columns[$headers['city']]));
				$location->prov_code = trim(strtoupper($columns[$headers['prov_code']]));
				$location->postal_code = trim(strtoupper($columns[$headers['postal_code']]));
				$location->phone1 = trim(strtoupper($columns[$headers['phone1']]));
				$location->active = (bool) $location->code;

				$location->setHl7IDForPartner($location->code, 'MERGERIS');

				$location->save();

				return $location->getShortCode();
			}
		}
	}
	protected function getRegionId($region)
	{
		if (array_key_exists($region, $this->regions)) {
			return $this->regions[$region]->id;
		}

		$region_obj = Doctrine_Query::create()
			->from('Region r')
			->addWhere('r.name = (?)', $region)
			->addWhere('r.client_id = (?)', $this->client_id)
			->fetchOne();

		if ($region_obj instanceof Region) {
			$this->regions[$region] = $region_obj;
			return $region_obj->id;
		}

		return null;
	}
}