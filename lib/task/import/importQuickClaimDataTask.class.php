<?php
ini_set("memory_limit","-1");
gc_enable();

class importQuickClaimDataTask extends sfBaseTask
{
	private $database;
	private $location_id;
	private $clientId;

	public function connectSourceDb()
	{
		// TODO:
        $this->clientId = 166;
        $this->location_id = 135;

		try {
			// TODO:
            $this->database = new  PDO("odbc:Driver={Microsoft Access Driver (*.mdb)};Dbq=C:\QuickClaim\medical.mdb;Uid=");
			$this->database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		} catch (PDOException $e) {
 			throw new Exception('PDO Connection Error: ' . $e->getMessage());
		}
	}

	public function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
 			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}

	private function executeTask($arguments, $options)
	{
        echo "I put in a purposeful exit() here. Be sure you know what you're doing.\n";
        //exit();

		if ($options['clientID'] == '') {
			echo 'ClientID not supplied';
			exit();
		}

		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$this->connectSourceDb();

		if ($this->clientId != $options['clientID'])
		{
			echo "\n" . 'Wrong clientID parameter' . "\n";
			exit;
		}
		echo ' Conected to QuickClaim DataBase' . "\n";
		//exit;

		$this->addHypeId();

		//this function set to null HYPE_ID in all tables in source data
        //$this->updateSource();

 		$this->importDoctors($this->clientId);

 		$this->importRefDocs($this->clientId);

		$this->importPatients($this->clientId);

 		$this->importThirdParty($this->clientId);

 		$this->importBatchs($this->clientId);

        $this->importAppmt($this->clientId);

        $this->importClaims($this->clientId);

        $this->importServicesCode($this->clientId);

 		$this->importQuickServicesCode($this->clientId);

	}

	public function addHypeId()
	{
		$tables = array("Doctors","DoctorsOther","Patients","Claims","Appointments", "ServiceCodesDirect", "QuickServCodes","ThirdParty", "Batches");
		//$tables = array("ServiceCodesDirect", "QuickServCodes");
		$i = 0;
		foreach ($tables as $table)
		{
			try {
				$sql = 'alter table ' . $table . ' add column HYPE_ID int';
				$sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$sth->execute();
				echo ' HYPE_ID field inserted in table ' . $table . "\n";
				$i++;
			}
			catch (Exception $e) {
				echo $e->getMessage() . "\n";
			}
		}
	}

	public function importAppmt($client_id)
	{
		echo ' Importing Appointments .....' . "\n";
		$execute = true;
		$i=1;
		$today = date(hype::DB_DATE_FORMAT, time());

		$appointmentTypeId = $this->getAppointmentTypeID();

		$attendedStageClientID = $this->getAttendedStageClientID();
		$bookedStageClientID = $this->getBookedStageClientID();

		while ($execute)
		{
			$sql = 'Select top 1 * from Appointments where HYPE_ID is null order by AppDate desc';
			$sth = $this->database->prepare($sql);
			$sth->execute();
			$source = $sth->fetch();
			if ($source)
			{
				$appointment = new Appointment();
				$appmtattendee = new AppointmentAttendee();
				$appmtattendee->setClientId($client_id);
				$appointment->client_id = $client_id;
				$appointment->slot_type = AppointmentTable::SLOT_TYPE_NORMAL;
				$appointment->service_code = $source['ServiceCode'];
				$appointment->diag_code = $source['DiagCode'];
				$appointment->location_id = $this->location_id;

				$appointment->notes = str_replace('?', '', iconv("UTF-8", "UTF-8//IGNORE", $source['Notes']));
				$appointment->appointment_type_id = $appointmentTypeId;

				$appstarttime = $this->convertTime($source['AppTime']);
				$appendtime = $this->convertTime($source['AppTime'] + $source['AppLength']);

				if (substr($source['AppDate'], 0, 2) == '01') {
					$source['AppDate'][0] = '1';
					$source['AppDate'][1] = '9';
				}
				if (substr($source['AppDate'], 0, 2) == '02') {
					$source['AppDate'][0] = '2';
					$source['AppDate'][1] = '0';
				}

				$appointment->start_date = substr($source['AppDate'],0,10) . ' ' . $appstarttime;
				$appointment->end_date = substr($source['AppDate'],0,10) . ' ' .$appendtime;

				if ($appendtime == '24:00:00') {
					$appointment->end_date = strtotime(substr($source['AppDate'], 0, 10) . ' + 1 day');
					$appointment->end_date = date(hype::DB_ISO_DATE, $appointment->end_date);
				}
				else if ($appendtime > '24:00:00') {
					$appointment->end_date = date(hype::DB_ISO_DATE, strtotime($appointment->end_date));
				}

				if ($source['AppDate'] > $today) {
					$appointment->status = AppointmentTable::STATUS_POSTED;
					$appointment->stage_client_id = $bookedStageClientID;
				}
				else {
					$appointment->status = AppointmentTable::STATUS_ATTENDED;
					$appointment->stage_client_id = $attendedStageClientID;
				}


				$doctor = $this->findDoctor($source['DoctorID']);
				if ($doctor instanceof Doctor) {
					$appointment->doctor_id = $doctor->getId();
					$appointment->doctor_code = $doctor->qc_doctor_code;
				}

				$patient = $this->findPatient($source['PatientID']);
				if ($patient instanceof Patient) {
					//echo 'mmmmm ';
					$appmtattendee->patient_fname = $patient->fname;
					$appmtattendee->patient_lname = $patient->lname;
					$appmtattendee->patient_phone = $patient->hphone;
					$appmtattendee->patient_id = $patient->getId();
					$appmtattendee->patient_email = $patient->email;

					if ($source['AppDate'] > $today) {
						$appmtattendee->status = AppointmentAttendeeTable::STATUS_BOOKED;
					}
					else {
						$appmtattendee->status = AppointmentAttendeeTable::STATUS_ATTENDED;
					}

					$appointment->AppointmentAttendee->add($appmtattendee);
				}
				try{
					$appointment->save();
				} catch (Exception $e)
				{
					print_r($source);
					print_r($appointment->toArray());
					echo $e;
					exit;
				}

				$sql = 'update Appointments set HYPE_ID = ? where AppID = ?';
				$sth = $this->database->prepare($sql);
				$sth->execute(array($appointment->getId(),$source['AppID']));
				echo $i . '  Appointment Inserted ' . "\n";
				$i++;

				if ($i > 1000) {
					exit();
				}
			}
			else {
				$execute = false;
				echo $i . 'Appointments inserted ' . "\n" . ' Done importing Appointments . ...... !!!' . "\n";
			}
		}
	}

    private function getAppointmentTypeID()
    {
        $appointmentType = Doctrine_Query::create()
            ->from('AppointmentType at')
            ->addWhere('at.client_id = (?)', $this->clientId)
            ->addWhere('at.name = (?)', 'IMPORTED')
            ->fetchOne();

        if (!$appointmentType instanceof AppointmentType) {
            $appointmentType = new AppointmentType();
            $appointmentType->client_id = $this->clientId;
            $appointmentType->slot_type = AppointmentTable::SLOT_TYPE_NORMAL;
            $appointmentType->name = 'IMPORTED';
            $appointmentType->appt_lengths = 15;
            $appointmentType->colour = '#ff0000';
            $appointmentType->title_colour = '#bb0000';
            $appointmentType->save();
        }

        return $appointmentType->id;
    }

    private function getAttendedStageClientID()
    {
        $stageClient = Doctrine_Query::create()
            ->from('StageClient sc')
            ->addWhere('sc.client_id = (?)', $this->clientId)
            ->addWhere('sc.stage_name = (?)', 'ATTENDED')
            ->fetchOne();

        if (!$stageClient instanceof StageClient) {
            echo 'No ATTENDED Stage Client record. Go make one.' . "\n";
            exit();
        }

        return $stageClient->id;
    }

    private function getBookedStageClientID()
    {
        $stageClient = Doctrine_Query::create()
            ->from('StageClient sc')
            ->addWhere('sc.client_id = (?)', $this->clientId)
            ->addWhere('sc.stage_name = (?)', 'BOOKED')
            ->fetchOne();

        if (!$stageClient instanceof StageClient) {
            echo 'No BOOKED Stage Client record. Go make one.' . "\n";
            exit();
        }

        return $stageClient->id;
    }

    public function convertTime($time)
    {
        $hour = (int)($time / 60);
        if ($hour < 10) {
            $strhour = '0' . $hour;
        }
        else {
            $strhour = $hour;
        }
        $min = $time % 60;
        if ($min < 10) {
            $strmin = '0' . $min;
        }
        else {
            $strmin = $min;
        }

        return $strhour . ':' . $strmin . ':00';
    }

    public function findDoctor($doctorW)
    {
        $rdsql = 'select * from Doctors where "DoctorID" = ?';
        $rdsth = $this->database->prepare($rdsql);
        $rdsth->execute(array($doctorW));
        $doc = $rdsth->fetch();
        $docnum = $doc['HYPE_ID'];

        if ($docnum) {
            $doctor = Doctrine_Query::create()
                ->from('Doctor d')
                ->addWhere('d.id = (?)', $docnum)
                ->fetchOne();

            return $doctor;
        }
        else {
            return null;

        }
    }

    public function findPatient($patOdo)
    {
        $psql = 'select * from Patients where "PatientID" = ?';
        $rdsth = $this->database->prepare($psql);
        $rdsth->execute(array($patOdo));
        $patient = $rdsth->fetch();
        $patientId = $patient['HYPE_ID'];

        if ($patientId) {
            $patInfo = Doctrine_Query::create()
                ->from('Patient p')
                ->addWhere('p.id = (?)', $patientId)
                ->addWhere('p.deleted = (?)', false)
                ->fetchOne();

            return $patInfo;
        }
        else {
            return null;
        }
    }

	public function importBatchs($client_id)
	{
		echo 'importing Doctors' . "\n";
		$execute = true;

		while ($execute)
		{
			$sql = 'select top 1 * from Batches where HYPE_ID is null';
			$sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$sth->execute();
			$source = $sth->fetch();

			if ($source) {
				$batch = new Batch();
				$batch->batch_number = $source['BatchID'];
				$bn = $batch->batch_number;
				$batch->batch_date = substr($bn, 0, 4) . '-' . substr($bn, 4, 2) . '-' . substr($bn, 6, 2);
				$batch->client_id = $client_id;
				$batch->location_id = $this->location_id;
				$batch->moh_office = $source['MOHOffice'];
				$batch->group_num = $source['GroupNum'];
				$batch->provider_num = $source['ProviderNum'];
				$batch->h = $source['H'];
				$batch->r = $source['R'];
				$batch->t = $source['T'];
				$batch->records = $source['Records'];
				$batch->total = $source['Total'];
				$batch->submit_date = $source['SubmitDate'];
				$batch->file_name = $source['FileName'];
				$batch->status = BatchTable::STATUS_BATCH_SUBMITTED;

				$spc = Doctrine_Query::create()
				->from('SpecCode sc')
				->addWhere('sc.moh_spec_code = (?)', $source['SpecCode'])
				->fetchOne();
				if ($spc instanceof SpecCode) {
					$batch->spec_code_id = $spc->getId();
				}

				$doc = $this->findBatchDoctor($batch->provider_num, $batch->group_num);
				 if  ($doc instanceof Doctors){
					$batch->doctor_id = $doc->getId();
				}
				$batch->save();
				$done = 'update Batches set HYPE_ID = ? where BatchID = ? ';
				$sth = $this->database->prepare($done);
				$sth->execute(array($batch->getId(),$source['BatchID']));
				echo 'Inserted Batch ==> ' . $batch->getId() . "\n";
			}
			else {
				$execute = false;
			}
		}
		echo 'Done importing Batchs ';
	}

    public function findBatchDoctor($provider, $group)
	{
        $rdsql = 'select * from Doctors where "ProviderNum" = ? and GroupNum = ?';
        $rdsth = $this->database->prepare($rdsql);
        $rdsth->execute(array($provider, $group));
        $doc = $rdsth->fetch();
        $docnum = $doc['HYPE_ID'];

        if ($docnum) {
            $doctor = Doctrine_Query::create()
                ->from('Doctor d')
                ->addWhere('d.id = (?)', $docnum)
			->fetchOne();

            return $doctor;
        }
        else {
            return null;

        }
	}

	public function importClaims($client_id)
	{
		echo 'Importing Claims' . "\n";
		$execute = true;
		$i = 1;
		while ($execute)
		{
			$sql = 'select top 1 * from Claims where HYPE_ID is null and "ClaimID" is not null';
			$sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$sth->execute();
			$source = $sth->fetch();

			if ($source) {
				$claim = new Claim();
				$claim->client_id = $client_id;

				$claim->batch_number = $source['BatchID'];
				$batch = $this->findBatch($claim->batch_number);
				if ($batch instanceof Batch){
					$claim->batch_id = $batch->id;
				}
				$claim->acct_num = $source['AcctNum'];

				$doctor = $this->findDoctor($source['DoctorID']);
				if ($doctor instanceof Doctor) {
					$claim->doctor_id = $doctor->getId();
					$claim->doctor_code = $doctor->qc_doctor_code;
				}
				else {
					echo ' No Doctor' . "\n";
				}

                $claim->pay_prog = strtoupper($source['PayProg']);

				$thirdparty = $this->findThirdParty($source['ThirdPartyID']);
				if ($thirdparty instanceof ThirdParty) {
					$claim->third_party_id = $thirdparty->getId();
				}
				$patient = $this->findPatient($source['PatientID']);
				if ($patient instanceof Patient) {
					$claim->patient_id = $patient->getId();
					$claim->patient_number = $patient->getPatientNumber();
				}
				if($source['HealthNum']) {
					// $claim->health_num = hype::encryptHCN($source['HealthNum'])
                    $claim->health_num = $source['HealthNum'];
				}
				$claim->version_code = $source['VersionCode'];

				if ($source['DOB'][0] == '0' && $source['DOB'][1] == '1') {
					$source['DOB'][0] = '1';
					$source['DOB'][1] = '9';
				}
				if ($source['DOB'][0] == '1' && $source['DOB'][1] == '0') {
					$source['DOB'][0] = '1';
					$source['DOB'][1] = '9';
				}

				$claim->dob = $source['DOB'];
				$claim->fname = $source['FName'];
				$claim->lname = $source['LName'];
				$claim->sex = $source['Sex'];
				$claim->province = $source['Province'];
				$claim->facility_num = $source['FacilityNum'];
				if ($source['AdmitDate'] && $source['AdmitDate'] != '0000-00-00 00:00:00') {
					$claim->admit_date = $source['AdmitDate'];
				}
				$claim->ref_lab = $source['RefLab'];
				$claim->manual_review = $source['ManualReview'];
				$claim->reg_num = $source['RegNum'];
				$claim->date_created = $source['DateCreated'];
				$claim->sli = $source['SLI'];
				$claim->split_flag = array_key_exists('SplitFlag', $source) ? $source['SplitFlag'] : null;
				$claim->ac_flag = array_key_exists('ACFlag', $source) ? $source['ACFlag'] : null;
				$claim->invoice_num = array_key_exists('InvoiceNum', $source) ? $source['InvoiceNum'] : null;
				$claim->moh_claim = array_key_exists('MOHClaim', $source) ? $source['MOHClaim'] : null;
				$claim->all_batches = isset($source['AllBatches']) ? $source['AllBatches'] : '';
				$claim->ra = isset($source['RA']) ? $source['RA'] : '';
				$claim->location_id = $this->location_id;

				if ($source['RefDoc']) {
					$ref_doc = $this->findRefDoc($source['RefDoc'], $client_id);
					if ($ref_doc instanceof ReferringDoctor) {
						$claim->ref_doc_id = $ref_doc->id;
					}

					$claim->ref_doc = $source['RefDoc'];
				}

				//Adding items to the Claim
				$Id = $source['ClaimID'];
				$itemSql = 'select * from Items where ClaimID = ?';
				$itemsth = $this->database->prepare($itemSql);
				$itemsth->execute(array($Id));
				$itemNum = 1;

				while ($rs = $itemsth->fetch())
				{
					$item = new ClaimItem();
					$item->client_id = $client_id;
					$item->item_id = $itemNum;
					$itemNum ++;
					$item->service_code = $rs['ServiceCode'];
					$item->diag_code = substr($rs['DiagCode'], 0, 4);
					$item->num_serv = $rs['NumServ'];
					$item->service_date = $rs['ServiceDate'];
					$item->fee_subm = $rs['FeeSubm'];
					$item->fee_paid = $rs['FeePaid'];
					$item->status = $rs['Status'];
					$item->errors = $rs['Errors'];
					// 					$item->order_id = $rs['OrderId'];
					// 					$item->ordernum = $rs['OrderNum'];
					// 					$item->orderstatus = $rs['OrderStatus'];
					// 					$item->placer_id = $rs['PlacerID'];
					$item->description1 = array_key_exists('Description2', $rs) ? $rs['Description1'] : null;
					$item->description2 = array_key_exists('Description2', $rs) ? $rs['Description2'] : null;

					$claim->ClaimItem->add($item);

				}

// 				print_r($source);
// 				print_r($claim->toarray());
// 				echo ' Claim ready to insert .... ' . "\n";
// 				exit();

				try {
					$conn = Doctrine_Manager::connection();
					$conn->beginTransaction();

					$claim->save();

					$conn->commit();

					$sql = 'update Claims set HYPE_ID = ? where ClaimID = ?';
					$sth = $this->database->prepare($sql);
					$sth->execute(array($claim->getID(), $source['ClaimID']));
					echo $i . '-->  Claim Inserted .... ' . $claim->getId() .  "\n";
				}
				catch (Exception $e) {
// 					print_r ($source);
// 					print_r ($claim->toArray());
					echo $e->getMessage();exit();
					throw $e;
					exit();
				}
				$i ++;
				//exit();
			}
			else {
				$execute = false;
			}

			if ($i > 300) {
				exit();
			}
		}
	}

    public function findBatch($batchNum)
    {
        $batch = Doctrine_Query::create()
            ->from('Batch b')
            ->addWhere('b.batch_number = (?)', $batchNum)
            ->fetchOne();

        return $batch;
    }

    public function findThirdParty($val)
    {
        $rdsql = 'select * from ThirdParty where "ThirdPartyID" = ?';
        $rdsth = $this->database->prepare($rdsql);
        $rdsth->execute(array($val));
        $party = $rdsth->fetch();
        $partynum = $party['HYPE_ID'];

        if ($partynum) {
            $thirdparty = Doctrine_Query::create()
                ->from('ThirdParty d')
                ->addWhere('d.id = (?)', $partynum)
                ->fetchOne();

            return $thirdparty;
        }
        else {
            return null;

        }
    }

    public function findRefDoc($provider_num, $client_id)
    {
        if ($provider_num) {
            return Doctrine_Query::create()
                ->from('ReferringDoctor r')
                ->addWhere('r.provider_num = (?)', $provider_num)
                ->addWhere('r.client_id = (?)', $client_id)
                ->fetchOne();
        }

        return null;
    }

	public function importDoctors($client_id)
	{
		echo 'importing Doctors' . "\n";
		$execute = true;

		while ($execute)
		{
			$sql = 'select top 1 * from Doctors where HYPE_ID is null';
			$sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$sth->execute();
			$source = $sth->fetch();

			if ($source) {
                $doctor = new Doctor();
                $doctor->fname = $source['FName'];
                $doctor->lname = str_replace('�', 'e', $source['LName']);
                $doctor->spec_code_id = $source['SpecCode'];
                $doctor->province = 'ON';
                $doctor->country = 'CA';
                $doctor->group_num = $source['GroupNum'];
                $doctor->provider_num = $source['ProviderNum'];
                $doctor->address = $source['Address'];
                $doctor->city = $source['City'];
                $doctor->postal_code = $source['PostalCode'];
                $doctor->phone = $source['Phone'];
                $doctor->client_id = $client_id;
                $doctor->active = 1;
                $doctor->email = $source['Email'];
                $doctor->fax = $source['Fax'];
                $doctor->sli = $source['SLI'];

                $office_id = $source['MOHOffice'];   // I am not sure about this one

                if ($office_id) {
                    $mohOffice = Doctrine_Query::create()
                        ->from('MOHOffice mo')
                        ->addWhere('mo.moh_office_code = (?)', $office_id)
                        ->fetchOne();

                    if ($mohOffice instanceof MOHOffice) {
                        $doctor->setMohOfficeId($mohOffice->id);
                    }
                }

                $doctor_code = $doctor->fname[0] . $doctor->lname[0];
                $doctor['qc_doctor_code'] = DoctorTable::checkDoctorCode($doctor_code, $client_id);

				$regions =  Doctrine_Query::create()
				->from('Region r')
				->addWhere('r.client_id = (?)', $client_id )
				->execute();

				foreach ( $regions as $dr )
				{
                    $dregion = new DoctorRegion();
                    $dregion->region_id = $dr->getId();
                    $doctor->DoctorRegion->add($dregion);
				}

				$locations = Doctrine_Query::create()
				->from('Location l')
				->addWhere('l.client_id = (?)', $client_id)
				->execute();

				foreach ($locations as $loc)
				{
                    $dlocation = new DoctorLocation();
                    $dlocation->location_id = $loc->getId();
                    $doctor->DoctorLocation->add($dlocation);
				}

				$doctor->save();
				$done = 'update Doctors set HYPE_ID = ? where DoctorID = ? ';
				$sth = $this->database->prepare($done);
				$sth->execute(array($doctor->getId(),$source['DoctorID']));

                echo 'Inserted Doctor ' . $doctor->getDropdownString() . "\n";
			}
			else {
				$execute = false;
				echo ' All doctors inserted.....' . "\n";
			}
		}
	}

    public function importPatients($client_id)
	{
		$patient_status_id = null;
		$patient_type_id = null;

        // TODO: test this
		$cp = ClientParamTable::getParamForClient($client_id, 'patient_status_default');
		if ($cp instanceof ClientParam) {
			$patient_status_id = $cp->value;
		}

        // TODO: test this
		$cp = ClientParamTable::getParamForClient($client_id, 'default_patient_type_id');
		if ($cp instanceof ClientParam) {
			$patient_type_id = $cp->value;
		}

		if (!$patient_status_id || !$patient_type_id) {
			echo 'No patient Status or Patient Type default; cant continue' . "\n";
			exit();
		}

		$execute = true;
		$i  = 1;
		while ($execute)
		{
			gc_collect_cycles(); // # of elements cleaned up
			$sql = 'select top 1 * from Patients where HYPE_ID is null';
			$sth = $this->database->prepare($sql,array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$sth->execute();
            $source = $sth->fetch();

			if ($source)
			{
                $patient = new Patient();
                $patient->client_id = $client_id;

                $patient->patient_number = (int)$source['PatientID'];


                if ($source['HealthNum']) {
                    // $patient->hcn_num = hype::encryptHCN($source['HealthNum']);
                    $patient->hcn_num = $source['HealthNum'];
                }
                $patient->hcn_version_code = $source['VersionCode'];
                $patient->hcn_exp_year = $source['ExpYear'];
                $patient->fname = trim(str_replace('?', '_', $source['FName']));
                $patient->lname = trim($source['LName']);

                if ($source['DOB'][0] == '0' && $source['DOB'][1] == '1') {
                    $source['DOB'][0] = '1';
                    $source['DOB'][1] = '9';
                }
                if ($source['DOB'][0] == '1' && $source['DOB'][1] == '0') {
                    $source['DOB'][0] = '1';
                    $source['DOB'][1] = '9';
                }

                $patient->dob = $source['DOB'];
                $patient->sex = $source['Sex'];
                $patient->address = $source['Address'];
                $patient->city = $source['City'];
                $patient->province = $source['Province'];
                $patient->postal_code = str_replace(' ', '', $source['PostalCode']);
                $patient->country = $source['Country'];
                $patient->hphone = $source['HPhone'];
                $patient->wphone = $source['WPhone'];

                if ($source['RefDoc']) {
                    $ref_doc = $this->findRefDoc($source['RefDoc'], $client_id);
                    if ($ref_doc instanceof ReferringDoctor) {
                        $patient->ref_doc_id = $ref_doc->id;
                    }

                    $patient->ref_doc_num = $source['RefDoc'];
                }

                if ($source['FamDoc']) {
                    $fam_doc = $this->findRefDoc($source['FamDoc'], $client_id);
                    if ($fam_doc instanceof ReferringDoctor) {
                        $patient->fam_doc_id = $fam_doc->id;
                    }
                    $patient->fam_doc_num = $source['FamDoc'];
                }

                $patient->facility_num = $source['FacilityNum'];
                if ($source['AdmitDate'] && $source['AdmitDate'] != '0000-00-00 00:00:00') {
                    $patient->admit_date = $source['AdmitDate'];
                }
                $patient->location_id = $this->location_id;
                $patient->patient_status_id = $patient_status_id;
                $patient->patient_type_id = $patient_type_id;

                if ($source['Notes']) {
                    $notes = new Notes();
                    $notes->note = str_replace('?', '_', $source['Notes']);
                    $notes->client_id = $client_id;
                    $patient->Notes->add($notes);
                }

                //$patient->hcn_check_date = array_key_exists('HNCheckDate', $source) ? $source['HNCheckDate'] : null;
                $patient->response = array_key_exists('Response', $source) ? $source['Response'] : null;
                $patient->hl7_pat_id = array_key_exists('HL7PatID', $source) ? $source['HL7PatID'] : null;

				try {
					$patient->save();
				}
				catch (Exception $e) {
					print_r($source);
					print_r($patient->toArray());
					echo $e->getMessage();
					exit();
				}

				$sql3 = 'update Patients set HYPE_ID = ? where PatientID = ?';
				$sth = $this->database->prepare($sql3);
				$sth->execute( array($patient->getId(), $source['PatientID']));

				echo $i . ' Patient  ' . $patient->getNameString() . '  Inserted ...' . "\n";
				$i ++;

				if ($i > 1000) {
					exit();
				}
				//exit();
			}
			else {
				$execute = false;
				echo ' Done importing Patients .....!!!!' . "\n";
			}
		}
	}

    public function importQuickServicesCode($client_id)
	{
		$execute = true;
		$i = 0;
		while ($execute)
		{
			$sql = 'Select top 1 * from QuickServCodes where HYPE_ID is null';
			$sht = $this->database->prepare($sql);
			$sht->execute();
			$source = $sht->fetch();
			//print_r($source);
			//exit();
			if ($source)
			{
				$qsc = new QuickServiceCode();
				$qsc->code = $source['Code'];
				$qsc->client_id = $client_id;
				$qsc->diag_code = $source['DiagCode'];
				$qsc->fee_percent = $source['FeePercent'];
				//$qsc->is_default = $source['IsDefault'];
				$qsc->description = $source['Description'];
				$qsc->num_serv = $source['NumServ'];
				//$qsc->colour = $source['Colour'];
				//$qsc->length = 	$source['length'];
				$qsc->active = 1;

				$qsc->save();

				$sql= 'Update QuickServCodes set HYPE_ID = ? where QuickCode = ?';
				$sht = $this->database->prepare($sql);
				$sht->execute(array($qsc->getId(),$source['QuickCode']));

				echo $i . '  Quick Service Code inserted ' . "\n";
				$i++;
			}
			else {
				$execute= false;
			}
			$i++;
		}
		echo 'Done with Quick Codes' . "\n";
	}

    public function importRefDocs($client_id)
	{
		$execute = true;
		$i=1;
		while ($execute)
		{
			$sql = 'select top 1 * from DoctorsOther where HYPE_ID is null';
			$sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$sth->execute();
            $source = $sth->fetch();
			 //print_r($source);

			if ($source) {
                // make refdoc object
                $refdoc = new ReferringDoctor();

                $refdoc->setClientId($client_id);

                $refdoc->salutation = $source['Salutation'];
                $refdoc->fname = $source['FName'];
                $refdoc->lname = $source['LName'];
                $refdoc->provider_num = $source['ProviderNum'];
                $refdoc->spec_code = $source['SpecCode'];
                $refdoc->active = 1;

                // set spec code information
                if ($refdoc->spec_code) {
                    $specCode = Doctrine_Query::create()
                        ->from('SpecCode sc')
                        ->addWhere('sc.moh_spec_code = (?)', $refdoc->spec_code)
                        ->fetchOne();

                    if ($specCode instanceof SpecCode) {
                        $refdoc->setSpecCodeId($specCode->id);
                        //$refdoc->setSpecCodeDescription($specCode->description);
                    }
                }

                $refdoc->address = $source['Address'];
                $refdoc->address2 = $source['Address2'];
                $refdoc->city = $source['City'];
                $refdoc->province = $source['Province'];
                $refdoc->postal_code = iconv("UTF-8", "UTF-8//IGNORE", $source['PostalCode']);
                $refdoc->phone = $source['Phone'];

                // 2014-06-02 - add fax number into importer set
                if ($source['Fax']) {
                    $refdoc->fax = $source['Fax'];
                }

                //print_r($refdoc->toarray());
				try{
				$refdoc->save();
				} catch (Exception $e)
				{
					print_r($source);
					exit;
				}
				echo $i . ' => Inserted Referring Doctor ' . $refdoc->getDropdownString() . "\n";

				$done = 'update DoctorsOther set HYPE_ID = ? where "ProviderNum" = ? ';
				$sth = $this->database->prepare($done);
				$sth->execute(array($refdoc->getId(),$source['ProviderNum']));
				$i ++;

			}
			else {
				$execute = false;
				echo ' All RefDoctors already Inserted....' . "\n";
			}
		}
	}

    public function importServicesCode($client_id)
	{
		$execute = true;
		$i = 0;
		while ($execute)
		{
			$sql = 'Select top 1 * from ServiceCodesDirect where HYPE_ID is null';
			$sht = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$sht->execute();
			$source = $sht->fetch();
			//print_r($source);
			if ($source)
			{
				$dsc = new DirectServiceCode();
				$dsc->active = 1;
				$dsc->client_id = $client_id;
				$dsc->description = $source['Description'];
				$dsc->fee = $source['Fee'];
				$dsc->service_code = $source['ServiceCode'];

				$dsc->save();

				$sql = 'update ServiceCodesDirect set HYPE_ID = ? where ServiceCode = ?';
				$sht = $this->database->prepare($sql);
				$sht->execute(array($dsc->getId(), $source['ServiceCode']));

				echo $i . '   Direct Service Code inserted' ."\n";
				$i++;
			}
			else {
				$execute= false;
			}

		}
		echo 'Done with Sevices Codes' . "\n";

	}

    public function importThirdParty($client_id)
	{
		$execute = true;
		$i=1;
		while ($execute)
		{
			$sql = 'Select top 1 * from ThirdParty where HYPE_ID is null';
			$sht = $this->database->prepare($sql);
			$sht->execute();
			$source = $sht->fetch();

			if ($source) {
				$thirdpary = new ThirdParty();
				$thirdpary->client_id = $client_id;
				$thirdpary->name = $source['Name'];
				$thirdpary->contact = $source['Contact'];
				$thirdpary->address = $source['Address'];
				$thirdpary->city = $source['City'];
				$thirdpary->province = $source['Province'];
				$thirdpary->postal_code = $source['PostalCode'];
				$thirdpary->country = $source['Country'];
				$thirdpary->phone = $source['Phone'];
				$thirdpary->email = $source['Email'];
				$thirdpary->fax = $source['Fax'];
				$thirdpary->save();

				$sql = 'update ThirdParty set HYPE_ID = ? where ThirdPartyID = ?';
				$sth = $this->database->prepare($sql);
				$sth->execute(array($thirdpary->getId(), $source['ThirdPartyID']));
				echo $i . '....' . "\n";

			}
			else {
				$execute = false;
			}
		}
		echo ' Done with Third Party' . "\n";
	}

    public function updateSource()
	{
		$tables = array("Doctors","DoctorsOther","Patients","Claims","Appointments","ThirdParty","Batches");
		$i = 0;

		foreach ($tables as $table)
		{
			$sql = 'update ' . $tables[$i] . ' set HYPE_ID = null where HYPE_ID is not null';
			$sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$sth->execute();
			echo 'HYPE_ID set to null ' . $tables[$i] . "\n";
			$i++;
		}

	}

    protected function configure()
	{
        $this->namespace = 'hype';
        $this->name = 'importQuickClaimData';
        $this->briefDescription = "Imports QuickClaim formatted file.";

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('clientID', null, sfCommandOption::PARAMETER_REQUIRED, 'Client ID'),
            new sfCommandOption('locationID', null, sfCommandOption::PARAMETER_REQUIRED, 'Location ID'),
            new sfCommandOption('filePath', null, sfCommandOption::PARAMETER_REQUIRED, 'The Path of the medical.mdb file [including medical.mdb]')

        ));
	}
}
