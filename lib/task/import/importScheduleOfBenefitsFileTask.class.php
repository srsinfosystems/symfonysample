<?php

class importScheduleOfBenefitsFileTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'importSOB';
		$this->briefDescription = 'Imports Schedule of Benefits formatted file.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('fileName', null, sfCommandOption::PARAMETER_REQUIRED, 'SOB File name & path'),
			new sfCommandOption('lineNumber', null, sfCommandOption::PARAMETER_OPTIONAL, 'Line number to begin processing at', 0),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$starting_line = ($options['lineNumber']) ? $options['lineNumber'] : 0;
		$line_number = 0;

		if (!file_exists($options['fileName'])) {
			echo 'this file is whack';
			exit();
		}

		$ana_base = floatval(Doctrine::getTable('SystemParam')->getValue('service_code_ana'));
		$assistant_base = floatval(Doctrine::getTable('SystemParam')->getValue('service_code_assistant'));

		if (!$ana_base || !$assistant_base) {
			echo 'Base Assistant and/or Base Anaesthesiologist Fee is not set. Go set it.';
			exit();
		}

		$handle = @fopen($options['fileName'], "r");
		if ($handle) {
			try {
				while (($buffer = fgets($handle, 4096)) !== false) {
					if ($line_number >= $starting_line) {
						$code = $this->processLine($buffer, $ana_base, $assistant_base);

						echo 'Processed Line ' . $line_number . ': ' . $code . "\n";
					}
					$line_number++;
				}
				if (!feof($handle)) {
					echo "Error: unexpected fgets() fail\n";
				}
			}
			catch (Exception $e) {
				print_r ($e->getMessage());
				echo "unable to complete processing file.\n";
			}
			fclose($handle);
		}
	}
	protected function processLine($line, $base_ana, $base_assistant)
	{
		$data['field_name'] = substr($line, 0, 4);
		$data['effective_date'] = substr($line, 4, 8);
		$data['termination_date'] = substr($line, 12, 8);
		$data['provider_fee'] = intval(substr($line, 20, 11)) / 10000;
		$data['assistant_fee'] = intval(substr($line, 31, 11)) / 10000;
		$data['specialist_fee'] = intval(substr($line, 42, 11)) / 10000;
		$data['ana_fee'] = intval(substr($line, 53, 11)) / 10000;
		$data['non_ana_fee'] = intval(substr($line, 65, 11)) / 10000;

		$service_code = Doctrine_Query::create()
			->from('ServiceCode sc')
			->addWhere('sc.service_code = (?)', $data['field_name'])
			->fetchOne();

		if (!$service_code instanceof ServiceCode) {
			$service_code = new ServiceCode();
			$service_code['service_code'] = $data['field_name'];
			$service_code['description'] = 'New Code';
			$service_code['sc_type'] = ServiceCodeTable::SC_TYPE_NORMAL;
		}

		if ($service_code->sc_type != ServiceCodeTable::SC_TYPE_NORMAL) {
			return $service_code['service_code'];
		}

		$service_code->units_assistant = 1;
		$service_code->units_ana = 1;
		$service_code->units_non_ana = 1;

		if ($data['effective_date']) {
			$service_code->date_start = self::formatAsDate($data['effective_date']);
		}
		if ($data['termination_date'] != '99999999') {
			$service_code->date_term = self::formatAsDate($data['termination_date']);
		}
		if ($data['provider_fee']) {
			$service_code->fee_general = $data['provider_fee'];
		}
		if ($data['assistant_fee']) {
			$service_code->fee_assistant = $data['assistant_fee'];
			if ($base_assistant && ($data['assistant_fee'] / $base_assistant) == intval($data['assistant_fee'] / $base_assistant)) {
				$service_code->units_assistant = $data['assistant_fee'] / $base_assistant;
				$service_code->fee_assistant = $base_assistant;
			}
		}
		if ($data['specialist_fee']) {
			$service_code->fee_specialist = $data['specialist_fee'];
		}
		if ($data['ana_fee']) {
			$service_code->fee_ana = $data['ana_fee'];

			if ($base_ana && ($data['ana_fee'] / $base_ana) == intval($data['ana_fee'] / $base_ana)) {
				$service_code->units_ana = $data['ana_fee'] / $base_ana;
				$service_code->fee_ana = $base_ana;
			}
		}
		if ($data['non_ana_fee']) {
			$service_code->fee_non_ana = $data['non_ana_fee'];

			if ($base_ana && ($data['non_ana_fee'] / $base_ana) == intval($data['non_ana_fee'] / $base_ana)) {
				$service_code->units_non_ana = $data['non_ana_fee'] / $base_ana;
				$service_code->fee_non_ana = $base_ana;
			}
		}
		$service_code->save();

		return $data['field_name'];
	}
	public static function formatAsDate($date)
	{
		$y = intval(substr($date, 0, 4));
		$m = intval(substr($date, 4, 2));
		$d = intval(substr($date, 6));
		return  $y . '-' . ($m ? $m : '01') . '-' . ($d ? $d : '01');
	}
}