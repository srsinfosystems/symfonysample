<?php

class testCsvTask extends sfBaseTask
{
	private $client_id = 2;
	private $location_id = 1;

	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'testCsv';
		$this->briefDescription = "Imports CSV files to Hype Medical .";

		$this->addOptions(array(
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend')));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$this->test($this->client_id);
	}
	public function findDuplicated($hcn, $client_id)
	{
		$patients = Doctrine_Query::create()
		->from('Patient p')
		->addWhere('p.hcn_num = (?)', $hcn)
		->addWhere('p.client_id = (?)', $client_id)
		->addWhere('p.deleted = (?)', false)
		->execute();
		return $patients;
	}
	public function test($client_id)
	{
		$txt_file = file_get_contents('C:\public_html\cvs_imports\WR120108.TXT');
		$rows = explode("\n", $txt_file);

		$patient_type_id = null;
		$patient_status_id = null;

		$cp = ClientParamTable::getParamForClient($client_id, 'patient_status_default');
		if ($cp instanceof ClientParam) {
			$patient_status_id = $cp->value;
		}

		$cp = ClientParamTable::getParamForClient($client_id, 'default_patient_type_id');
		if ($cp instanceof ClientParam) {
			$patient_type_id = $cp->value;
		}

		foreach ($rows as $row)
		{
			$duplicates = array();

			if (strlen($row)) {
				$patient = new Patient();
				//---------------------------/
				$patient->client_id=$client_id;
				$patient->location_id = $this->location_id;
				//---------------------------/

				$patient->patient_number = strtoupper(trim(substr($row, 0,9)));
				$patient->lname = strtoupper(trim(substr($row, 9,24)));
				$patient->fname = strtoupper(trim(substr($row, 33,16)));
				$patient->sex = strtoupper(trim(substr($row, 52,1)));
				$dob = strtoupper(trim(substr($row, 53,8)));
				$patient->dob = substr($dob,0,-4) . '-' . substr($dob,4,-2) . '-' . substr($dob,-2);
				$patient->address = strtoupper(trim(substr($row, 82,34)));
				$patient->city = strtoupper(trim(substr($row, 116,20)));
				$patient->province = strtoupper(trim(substr($row, 139,4)));
				$patient->postal_code = strtoupper(trim(substr($row, 143,10)));
				$patient->hphone = strtoupper(trim(substr($row, 154,10)));
				$patient->wphone = strtoupper(trim(substr($row, 164,10)));
				$patient->hcn_version_code = strtoupper(trim(substr($row, 192,5)));
				$date = strtoupper(trim(substr($row, 202,10)));
				if ($date) {
					$patient->hcn_exp_year = substr($date,0,-4) . '-' . substr($date,4,-2) . '-' . substr($date,-2);
				}
				$patient->patient_type_id = $patient_type_id;
				$patient->patient_status_id = $patient_status_id;

				$hcnum = strtoupper(trim(substr($row, 180,11)));
				echo 'HC num ' . $hcnum . "\n";
				$note = new Notes();
				$note->client_id = $client_id;
				$note->note = 'Imported from file WR120108.TXT' ;
				if (strlen($hcnum) > 1) {
					$patient->hcn_num = hype::encryptHCN($hcnum);
					$duplicates = $this->findDuplicated($patient->hcn_num, $patient->getClientId());
					echo 'amount of duplicates entries for this patient --> ' . count($duplicates) . "\n";
				}
				else {
					$note->note = $note->note . "\n" . '  There was no Health Card Number to import.';
				}
				$patient->Notes->add($note);
				echo 'Duplicates = ' . count($duplicates) . "\n";

// 				print_r($patient->toarray());
// 				exit();

				if(!count($duplicates))
				{
					try {
						$patient->save();
					}
					catch (Exception $e) {
						echo $e->getMessage() . "\n";
						print_r($patient->toarray());
						exit();
					}
				}
				else {
					echo 'already in data base. ' . "\n";
				}
			}
		}
	}
}