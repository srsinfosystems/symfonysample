<?php

class CVIASecondUpdateTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'CVIASecondUpdate';
		$this->briefDescription = 'Fixes Second CVIA Import.';

		$this->addOptions(array(
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
				new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$dataImports = Doctrine_Query::create()
			->from('DataImporter d')
			->addWhere('d.importer_name = (?)', 'CVIASecondUpdate')
			->addWhere('d.status = (?)', 0)
			->limit(50)
			->execute();

		if (count($dataImports)) {
			foreach ($dataImports as $import)
			{
				try {
					$connection->beginTransaction();
					$data = $this->parseData($import->getDataText());

					if ($data['hcn']) {
						$patient = null;

						// match on hype ID
						if ($data['hype_id'] && $data['hcn']) {
							$patient = Doctrine_Query::create()
							->from('Patient p')
							->addWhere('p.id = (?)', $data['hype_id'])
							->addWhere('p.hcn_num = (?)', hype::encryptHCN($data['hcn']))
							->addWhere('p.client_id = (?)', $import->getClientId())
							->fetchOne();
						}
						else if ($data['hype_id']) {
							$patient = Doctrine_Query::create()
							->from('Patient p')
							->addWhere('p.id = (?)', $data['hype_id'])
							->addWhere('p.hcn_num = (?) or p.hcn_num is null', '')
							->addWhere('p.client_id = (?)', $import->getClientId())
							->fetchOne();
						}

						// match on original merge ID
						if (!$patient instanceof Patient && $data['original_merge_id'] && $data['hcn']) {
							$patient = Doctrine_Query::create()
							->from('Patient p')
							->addWhere('p.patient_number = (?)', $data['original_merge_id'])
							->addWhere('p.hcn_num = (?)', hype::encryptHCN($data['hcn']))
							->addWhere('p.client_id = (?)', $import->getClientId())
							->fetchOne();
						}
						else if (!$patient instanceof Patient && $data['original_merge_id'] && !$data['hcn']) {
							$patient = Doctrine_Query::create()
							->from('Patient p')
							->addWhere('p.patient_number = (?)', $data['original_merge_id'])
							->addWhere('p.hcn_num = (?) or p.hcn_num is null', '')
							->addWhere('p.client_id = (?)', $import->getClientId())
							->fetchOne();
						}

						// match on health card number
						if (!$patient instanceof Patient && $data['hcn']) {
							$patient = Doctrine_Query::create()
							->from('Patient p')
							->addWhere('p.hcn_num = (?)', hype::encryptHCN($data['hcn']))
							->addWhere('p.client_id = (?)', $import->getClientId())
							->fetchOne();
						}

						// match on last name, first name, dob
						if (!$patient instanceof Patient) {
							$patient = Doctrine_Query::create()
							->from('Patient p')
							->addWhere('p.client_id = (?)', $import->getClientId())
							->addWhere('p.fname = (?)', $data['fname'])
							->addWhere('p.lname = (?)', $data['lname'])
							->addWhere('p.dob = (?)', $data['dob'])
							->fetchOne();
						}

						// 					if (!$patient instanceof Patient && !$data['hcn']) {
						// 						echo 'No patient matched yet.' . "\n";
						// 						print_r ($data);exit();
						// 					}

						// make new patient
						if (!$patient instanceof Patient) {
							$patient = new Patient();
							$patient->setClientId($import->getClientId());
						}

						$patient->fromArray($data);
						$patient->setPatientNumber($data['new_merge_id']);
						$patient->setLocationId($import->getLocationId());
						$patient->setDeleted(false);
						if (array_key_exists('hcn', $data)) {
							$patient->setHcnNum(hype::encryptHcn($data['hcn']));
						}

						if (!$patient->patient_status_id) {
							$cp = ClientParamTable::getParamForClient($import->getClientId(), 'patient_status_default');
							if ($cp instanceof ClientParam) {
								$patient->patient_status_id = $cp->value;
							}
						}

						if (!$patient->patient_type_id) {
							$cp = ClientParamTable::getParamForClient($import->getClientId(), 'default_patient_type_id');
							if ($cp instanceof ClientParam) {
								$patient->patient_type_id = $cp->value;
							}
						}

						$patient->save();

						$hl7_queue = new Hl7Queue();
						$hl7_queue->client_id = $patient->client_id;
						$hl7_queue->version_number = $patient->version;
						$hl7_queue->message_type = 'ADT';
						$hl7_queue->message_subtype = 'A08';
						$hl7_queue->model_id = $patient->id;
						$hl7_queue->model_type = 'PATIENT';
						$hl7_queue->status = HL7QueueTable::STATUS_UNPROCESSED;
						$hl7_queue->partner_name = 'MERGERIS';

						$hl7_queue->save();

						echo 'Saved Patient ' . $patient->getId() . ' (' . $patient->getFullName() . ')' . "\n";

					}
					else {
						echo 'Handled patient without HC # (' . $data['fname'] . ' ' . $data['lname'] . ")\n";
					}

					$import->setStatus(10)->save();

					$connection->commit();
				}
				catch (Exception $e) {
					print_r ($data);
					print_r ($patient->toArray());

					$connection->rollback();
					throw $e;
				}
			}
		}

	}
	private function parseData($data)
	{
		$data = unserialize($data);

		$rs = array(
			'hype_id' => intval($data[0]),
			'original_merge_id' => strtoupper(trim($data[1])),
			'new_merge_id' => strtoupper(trim($data[2])),
			'fname' => strtoupper(trim($data[3])),
			'lname' => strtoupper(trim($data[4])),
			'dob' => strtoupper(trim($data[5])),
			'hcn' => strtoupper(trim($data[6])),
			'hcn_version_code' => strtoupper(trim($data[7])),
			'address' => strtoupper(trim($data[8])),
			'city' => strtoupper(trim($data[10])),
			'hphone' => strtoupper(trim($data[12])),
		);

		foreach ($rs as $key => $value)
		{
			if ($value == 'NULL') {
				unset($rs[$key]);
			}
		}

		if (array_key_exists('hcn', $rs) && $rs['hcn'] == '0000000000') {
			$rs['hcn'] = '';
		}
		else if (!array_key_exists('hcn', $rs)) {
			$rs['hcn'] = '';
		}

		if (!array_key_exists('hype_id', $rs)) {
			$rs['hype_id'] = null;
		}

		return $rs;
	}
}