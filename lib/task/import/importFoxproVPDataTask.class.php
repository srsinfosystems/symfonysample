<?php
ini_set("memory_limit","-1");
gc_enable();

class importFoxproHiroDataTask extends sfBaseTask
{

	/**
	 * @var PDO
	 */
	private $database;

	private $fkProviderTables = array('provider', 'appt2', 'apptright', 'bctop', 'bctoparc', 'dailyatt', 'fhgroster', 'filetrans', 'obecbatch', 'officehrs', 'osubtop', 'personprv', 'provadrh', 'provadrw', 'provlics', 'provobill', 'provotype', 'provprac', 'provres', 'qbque', 'schdappt');
	private $fkPersonTables = array('person', 'alerts', 'appt2', 'apptptree', 'bctop', 'bctoparc', 'bdpaidi', 'bfincid', 'cusbddl', 'cusbdhis', 'cusbdreq', 'dailyatt', 'fhgroster', 'hl7data', 'inspolicy', 'libgroups', 'medicate', 'obecbatch', 'overpaid', 'perblock', 'perclass', 'perpics', 'persmisc', 'personadrh', 'personadrm', 'personadrw', 'personhis', 'personpcn', 'personprv', 'personptree', 'personref', 'provinsno', 'qbque', 'recalls', 'schdappt', 'tasklines', 'vaccine', 'vaultactions', 'vaultitems', 'vltsubsmain');
	private $fkThirdPartyTables = array('libinscos', 'inspolicy');
	private $fkBatchTables = array('osubtop', 'oprevsubs', 'osubline');
	private $fkClaimTables = array('bctop', 'appt2', 'bcline', 'bclinearc', 'bcpaidline', 'bohipbi', 'bohipbiarc');
	private $fkAppointmentTables = array('appt2');

	//TODO: reset these values
	private $locationId = 105;
	private $clientId = 132;
	private $count = 0;
	private $recordLimit = 10000;

	public function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
//			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}

	private function executeTask($arguments, $options)
	{
//		echo "I put in a purposeful exit() here. Be sure you know what you're doing.\n";
//		exit();

		if ($options['clientID'] == '') {
			echo 'ClientID not supplied';
			exit();
		}

		if ($this->clientId != $options['clientID'])
		{
			echo "\n" . 'Wrong clientID parameter' . "\n";
			exit();
		}

//		$databaseManager = new sfDatabaseManager($this->configuration);
//		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$this->connectSourceDb();
		echo 'Connected to FoxPro DataBase' . "\n\n";

		$this->addHypeColumns();
//		 $this->resetHypeColumnsToNull();

		$this->importDoctors();
		if ($this->count > $this->recordLimit) {
			exit();
		}

		$this->importReferringDoctors();
		if ($this->count > $this->recordLimit) {
			exit();
		}

		$this->importPatients();
		if ($this->count > $this->recordLimit) {
			exit();
		}

		$this->importThirdParties();
		if ($this->count > $this->recordLimit) {
			exit();
		}

		$this->importBatches();
		if ($this->count > $this->recordLimit) {
			exit();
		}

		$this->importAppointments();
		if ($this->count > $this->recordLimit) {
			exit();
		}

		$this->importOHIPClaims();
		if ($this->count > $this->recordLimit) {
			exit();
		}

		// TODO: import Direct and third party claims
		exit();

		// TODO: after importing all claims, re-calculate the h,t,r,records and total values
		// TODO: do we need to deal with claims that do not have a doctor attached? (fkprovider = 0)
		// TODO: appointments with null hype patient IDs
		// TODO: claims with null hype patient IDs
		// TODO: claims with null hype doctor IDs
	}

	private function connectSourceDb()
	{
        // TODO: here is the foxPro converted database
		$serverName = '192.168.1.103';
		$dbname = 'foxpro';
		$username = 'admin';
		$password = 'owenalon';

		try {
			$this->database = new PDO("mysql:host=" . $serverName . ';dbname=' . $dbname, $username, $password );
			$this->database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
			throw new Exception('PDO Connection Error: ' . $e->getMessage());
		}
	}

	private function addHypeColumns()
	{
		foreach ($this->fkProviderTables as $key => $table) {
			$this->addHypeColumn($table, 'HYPE_DOCTOR_ID', 'int');
			$this->addHypeColumn($table, 'HYPE_DOCTOR_TYPE', 'nvarchar(20)');
		}

		foreach ($this->fkPersonTables as $key => $table) {
			$this->addHypeColumn($table, 'HYPE_PATIENT_ID', 'int');
		}

		foreach ($this->fkThirdPartyTables as $key => $table) {
			$this->addHypeColumn($table, 'HYPE_THIRD_PARTY_ID', 'int');
		}

		foreach ($this->fkBatchTables as $key => $table) {
			$this->addHypeColumn($table, 'HYPE_BATCH_ID', 'int');
		}

		foreach ($this->fkClaimTables as $key => $table) {
			$this->addHypeColumn($table, 'HYPE_CLAIM_ID', 'int');
		}

		foreach ($this->fkAppointmentTables as $key => $table)
		{
			$this->addHypeColumn($table, 'HYPE_APPOINTMENT_ID', 'int');
		}
	}

    private function addHypeColumn($table, $columnName, $dataType)
	{
        try {
            $alterSQL = 'alter table ' . $table . ' add column ' . $columnName . ' ' . $dataType;
            $sth = $this->database->prepare($alterSQL, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $sth->execute();
            echo $columnName . ' field inserted in table ' . $table . "\n";
		}
        catch (Exception $e) {
            if (strpos($e->getMessage(), 'Duplicate column name') !== false) {
            }
            else {
                echo $e->getMessage() . "\n";
                exit();
            }
		}
	}

    private function importDoctors()
	{
        echo 'Step 1: Import Doctor Profiles' . "\n";
        $execute = true;

        while ($execute == true && $this->count <= $this->recordLimit) {
            $fpDoctorID = $this->getFPDoctorToImport();

            if (strlen($fpDoctorID)) {
                $this->importDoctorByFKProvider($fpDoctorID);
                $this->count++;
            }
            else {
                $execute = false;
                echo '  No more Doctor Profiles to import.' . "\n";
            }
		}
	}

	private function getFPDoctorToImport()
	{
		$sql = 'SELECT fkprovider from provobill where HYPE_DOCTOR_ID is null and phystype = ? limit 1';
		$sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$sth->execute(array('1'));

		$rs = $sth->fetch(PDO::FETCH_ASSOC);

		if ($rs) {
			$providerPKGUID = $rs['fkprovider'];

			$sql = 'SELECT * from provider where pkguid = ? and HYPE_DOCTOR_ID is null';
			$sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$sth->execute(array($providerPKGUID));

			$rs = $sth->fetch(PDO::FETCH_ASSOC);

			if ($rs) {
				$rs = $rs['pkguid'];
			}
		}

		return $rs;
	}

	private function importDoctorByFKProvider($fpDoctorID)
	{
		$fpProviderRec = $this->getFPProviderByGUID($fpDoctorID);
		$fpProvoBillRec = $this->getFPProvoBillByFkProvider($fpDoctorID);
		$fpProvAddressRec = $this->getFPProvAdrWByFkProvider($fpDoctorID);
		$fpLibHospRec = false;

		$doctor = new Doctor();
		$doctor->setClientID($this->clientId);

		if ($fpProvoBillRec) {
			$doctor->setGroupNum($this->processString($fpProvoBillRec['ohipgrp']));
			while (strlen($doctor->getGroupNum()) < 4)
			{
				$doctor->setGroupNum('0' . $doctor->getGroupNum());
			}

			$doctor->setProviderNum($this->processString($fpProvoBillRec['physno']));
			while (strlen($doctor->getProviderNum()) < 6)
			{
				$doctor->setProviderNum('0' . $doctor->getProviderNum());
			}

			$doctor->setSpecCodeId($this->processString($fpProvoBillRec['specialty']));
			while (strlen($doctor->getSpecCodeId()) < 2)
			{
				$doctor->setSpecCodeId('0' . $doctor->getSpecCodeId());
			}

			$mohOfficeID = $this->getHypeMohOfficeIDByMohOfficeCode($this->processString($fpProvoBillRec['distcode']));
			$doctor->setMohOfficeId($mohOfficeID);

			$fpLibHospRec = $this->getFPLibHospByHospName($fpProvoBillRec['defhosp']);
		}

		if ($fpLibHospRec) {
			$doctor->setFacilityNum($this->processString($fpLibHospRec['hospno']));
		}

		if ($fpProviderRec) {
			$doctor->setFname($this->processString($fpProviderRec['firstname']));
			$doctor->setLname($this->processString($fpProviderRec['lastname']));
			$doctor->setLname(str_replace('DR. ', '', $doctor->getLname()));
			$doctor->setTitle($this->processString($fpProviderRec['title']));
		}

		if ($fpProvAddressRec) {
			$doctor->setAddress($this->processString($fpProvAddressRec['address']));
			$doctor->setCity($this->processString($fpProvAddressRec['city']));
			$doctor->setProvince($this->processString($fpProvAddressRec['provstate']));
			$doctor->setPostalCode($this->processString($fpProvAddressRec['mailcode']));
			$doctor->setCountry($this->processString($fpProvAddressRec['countrycd']));
			$doctor->setPhone($this->processString($fpProvAddressRec['phone']));
			$doctor->setCell($this->processString($fpProvAddressRec['fax']));
			$doctor->setFax($this->processString($fpProvAddressRec['fax']));
		}

		$doctor->setActive(true);
		$doctor->setSli($this->processString($fpProviderRec['sli']));

		// qc doctor code
		$qcCode = $doctor->fname[0] . $doctor->lname[0];
		$doctor->setQcDoctorCode(DoctorTable::checkDoctorCode($qcCode, $this->clientId));

		$regions = $this->getHypeRegions();
		foreach ($regions as $region)
		{
			$doctorRegion = new DoctorRegion();
			$doctorRegion->setRegionId($region->getId());
			$doctor->DoctorRegion->add($doctorRegion);
		}

		$locations = $this->getHypeLocations();
		foreach ($locations as $location)
		{
			$doctorLocation = new DoctorLocation();
			$doctorLocation->setLocationId($location->getId());
			$doctor->DoctorLocation->add($doctorLocation);
		}

		try {
			$conn = Doctrine_Manager::connection();
			$conn->beginTransaction();

			$doctor->save();
			$success = $this->setFPProviderHypeID($fpDoctorID, $doctor->get('id'), 'DOCTOR');
			$fkCount = $this->setFPProviderFKsHypeIDs($fpDoctorID, $doctor->get('id'), 'DOCTOR');

			if ($success) {
				echo '  Created Doctor profile for ' . $doctor->getDropdownString() . "\n";
				echo '  Updated FP Provider Record ' . $fpDoctorID . ' with Hype ID ' . $doctor->get('id') . "\n";
				echo '  Updated ' . $fkCount . ' fkprovider records in ' . count($this->fkProviderTables) . ' tables.' . "\n";

				$conn->commit();
			}
		}
		catch (Exception $e) {
			echo $e->getMessage() . "\n";
			echo $e->getTraceAsString() . "\n";

			echo "\n\n\n" . 'Please ensure source database record with pkguid ' . $fpDoctorID . ' has a null value in HYPE_ID' . "\n";
			exit();
		}
	}

    private function getFPProviderByGUID($guid)
	{
        $sql = 'SELECT * from provider where pkguid = ?';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($guid));

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    private function getFPProvoBillByFkProvider($fkProvider)
    {
        $sql = 'SELECT * from provobill where fkprovider = ?';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($fkProvider));

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    private function getFPProvAdrWByFkProvider($fkProvider)
    {
        $sql = 'SELECT * from provadrw where fkprovider = ?';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($fkProvider));

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    private function processString($str, $includeNewLines = false)
    {
        if (!$includeNewLines) {
            $str = str_replace(array("\n", "\r", "\r\n"), ' ', $str);
        }

        return strtoupper(trim(iconv("UTF-8", "UTF-8//IGNORE", $str)));
    }

    private function getHypeMohOfficeIDByMohOfficeCode($officeName)
    {
        $rs = null;

        $mohOffice = Doctrine_Query::create()
            ->from('MOHOffice m')
            ->where('m.moh_office_code = (?)', array($officeName))
            ->fetchOne();

        if ($mohOffice instanceof MOHOffice) {
            $rs = $mohOffice->get('id');
        }

        return $rs;
    }

    private function getFPLibHospByHospName($hospitalName)
    {
        $sql = 'SELECT * from libhosp where hospname = ?';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($hospitalName));

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    private function getHypeRegions()
    {
        return Doctrine_Query::create()
            ->from('Region r')
            ->addWhere('r.client_id = (?)', array($this->clientId))
            ->execute();
    }

    private function getHypeLocations()
    {
        return Doctrine_Query::create()
            ->from('Location l')
            ->addWhere('l.client_id = (?)', array($this->clientId))
            ->execute();
    }

    private function setFPProviderHypeID($providerPKGUID, $hypeDoctorID, $hypeDoctorType)
    {
        $sql = 'UPDATE provider set HYPE_DOCTOR_ID = ?, HYPE_DOCTOR_TYPE = ? where pkguid = ?';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($hypeDoctorID, $hypeDoctorType, $providerPKGUID));

        return $sth->rowCount();
    }

    private function setFPProviderFKsHypeIDs($FKProvider, $hypeDoctorID, $hypeDoctorType)
    {
        $count = 0;

        foreach ($this->fkProviderTables as $table) {
            if ($table != 'provider') {
                $sql = 'UPDATE ' . $table . ' set HYPE_DOCTOR_ID = ?, HYPE_DOCTOR_TYPE = ? where fkprovider = ?';
                $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $sth->execute(array($hypeDoctorID, $hypeDoctorType, $FKProvider));
                $count += $sth->rowCount();
            }
        }

        return $count;
    }

    private function importReferringDoctors()
    {
        echo "\n" . 'Step 2: Import Referring Doctors' . "\n";
		$execute = true;

		while ($execute == true && $this->count <= $this->recordLimit)
		{
            $fpReferringDoctorID = $this->getFPReferringDoctorToImport();

            if (strlen($fpReferringDoctorID)) {
                $this->importReferringDoctorByFKProvider($fpReferringDoctorID);
				$this->count++;
			}
			else {
				$execute = false;
                echo '    No more Referring Doctors to import.' . "\n";
			}
		}
	}

    private function getFPReferringDoctorToImport()
    {
        $sql = 'SELECT fkprovider from provobill where HYPE_DOCTOR_ID is null and phystype = ? limit 1';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array('2'));

        $rs = $sth->fetch(PDO::FETCH_ASSOC);

        if ($rs) {
            $providerPKGUID = $rs['fkprovider'];

            $sql = 'SELECT * from provider where pkguid = ? and HYPE_DOCTOR_ID is null';
            $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $sth->execute(array($providerPKGUID));

            $rs = $sth->fetch(PDO::FETCH_ASSOC);

            if ($rs) {
                $rs = $rs['pkguid'];
            }
        }

        return $rs;
    }

	private function importReferringDoctorByFKProvider($fpReferringDoctorID)
	{
		$fpProviderRec = $this->getFPProviderByGUID($fpReferringDoctorID);
		$fpProvoBillRec = $this->getFPProvoBillByFkProvider($fpReferringDoctorID);
		$fpProvAddressRec = $this->getFPProvAdrWByFkProvider($fpReferringDoctorID);

		$referringDoctor = new ReferringDoctor();
		$referringDoctor->setClientId($this->clientId);

		if ($fpProviderRec) {
			$referringDoctor->setSalutation($this->processString($fpProviderRec['title']));
			$referringDoctor->setFname($this->processString($fpProviderRec['firstname']));
			$referringDoctor->setLname($this->processString($fpProviderRec['lastname']));
		}

		if ($fpProvoBillRec) {
			$referringDoctor->setProviderNum($this->processString($fpProvoBillRec['physno']));
			$referringDoctor->set('spec_code', $this->processString($fpProvoBillRec['specialty']));

			$specCode = $this->getHypeSpecCode($referringDoctor->get('spec_code'));
			if ($specCode instanceof SpecCode) {
				$referringDoctor->setSpecCodeId($specCode->get('id'));
			}
		}

		if ($fpProvAddressRec) {
			$referringDoctor->setAddress($this->processString($fpProvAddressRec['address']));
			$referringDoctor->setCity($this->processString($fpProvAddressRec['city']));
			$referringDoctor->setProvince($this->processString($fpProvAddressRec['provstate']));
			$referringDoctor->setPostalCode($this->processString($fpProvAddressRec['mailcode']));
			$referringDoctor->setFax($this->processString($fpProvAddressRec['fax']));
			$referringDoctor->setPhone($this->processString($fpProvAddressRec['phone']));
		}

		$referringDoctor->setActive(true);

		try {
			$conn = Doctrine_Manager::connection();
			$conn->beginTransaction();

			$referringDoctor->save();
			$success = $this->setFPProviderHypeID($fpReferringDoctorID, $referringDoctor->get('id'), 'REFDOC');
			$fkCount = $this->setFPProviderFKsHypeIDs($fpReferringDoctorID, $referringDoctor->get('id'), 'REFDOC');

			if ($success) {
				echo '  Created Referring Doctor profile for ' . $referringDoctor->getAutocomplete() . "\n";
				echo '  Updated FP Provider Record ' . $fpReferringDoctorID . ' with Hype ID ' . $referringDoctor->get('id') . "\n";
				echo '  Updated ' . $fkCount . ' fkprovider records in ' . count($this->fkProviderTables) . ' tables.' . "\n\n";

				$conn->commit();
			}
		}
		catch (Exception $e) {
			echo $e->getMessage() . "\n";
			echo $e->getTraceAsString() . "\n";

			echo "\n\n\n" . 'Please ensure source database record with pkguid ' . $fpReferringDoctorID . ' has a null value in HYPE_ID' . "\n";
			exit();
		}
	}

    private function getHypeSpecCode($specCode)
	{
        return Doctrine_Query::create()
            ->from('SpecCode s')
            ->where('s.moh_spec_code = (?)', $specCode)
            ->fetchOne();
    }

    private function importPatients()
    {
        echo "\n" . 'Step 3: Import Patients' . "\n";
		$execute = true;

		while ($execute == true && $this->count <= $this->recordLimit)
		{
            $fpPatientID = $this->getFPPatientToImport();

            if (strlen($fpPatientID)) {
                $this->importPatientByFKPerson($fpPatientID);
				$this->count++;
			}
			else {
				$execute = false;
                echo '    No more Patients to import.' . "\n";
			}
		}
	}

    private function getFPPatientToImport()
    {
        $sql = 'SELECT pkguid from person where HYPE_PATIENT_ID is null limit 1';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute();

        $rs = $sth->fetch(PDO::FETCH_ASSOC);

        if ($rs) {
            $rs = $rs['pkguid'];
        }

        return $rs;
    }

	private function importPatientByFKPerson($fpPatientID)
	{
		$patientStatusID = $this->getHypeDefaultPatientStatusID();
		$patientTypeID = $this->getHypeDefaultPatientTypeID();

		$fpPersonRec = $this->getFPPersonByGUID($fpPatientID);
		$fpProvinsnoRec = $this->getFPProvinsnoByFKPerson($fpPatientID);
		$fpPersonAdrHRec = $this->getFPPersonAdrHByFKPerson($fpPatientID);
		$fpPersonAdrWRec = $this->getFPPersonAdrWByFKPerson($fpPatientID);
		$fpPersonPrvRefDocRec = $this->getFPPersonPrvByFKPersonAndPhysType($fpPatientID, 2);
		$fpPersonPrvFamDocRec = $this->getFPPersonPrvByFKPersonAndPhysType($fpPatientID, 4);
		$fpPersonNoteRecs = $this->getFPPerClassByFKPerson($fpPatientID);
		$fpPersonMiscRecs = $this->getFPPersMiscByFKPerson($fpPatientID);

		$patient = new Patient();
		$patient->setClientID($this->clientId);
		$patient->setPatientStatusId($patientStatusID);
		$patient->setPatientTypeId($patientTypeID);
		$patient->setLocationId($this->locationId);

		if ($fpPersonRec) {
			$patient->setHcnNum($this->processString($fpPersonRec['hccardno']));
			$patient->setFname($this->processString($fpPersonRec['firstname'] . ' ' . $fpPersonRec['secname']));
			$patient->setLname($this->processString($fpPersonRec['lastname']));
			$patient->setDob($this->processString($fpPersonRec['dob']));
			$patient->setSex($this->processString($fpPersonRec['gender']));

		}

		if ($fpProvinsnoRec) {
			$patient->setHcnVersionCode($this->processString($fpProvinsnoRec['hcversion']));
			$patient->setHcnExpYear($this->processString($fpProvinsnoRec['hcexpiry']));
			$patient->setMohProvince($this->processString($fpProvinsnoRec['hcprovcode']));
		}

		if ($fpPersonAdrHRec) {
			$patient->setAddress($this->processString($fpPersonAdrHRec['address']));
			$patient->setCity($this->processString($fpPersonAdrHRec['city']));
			$patient->setProvince($this->processString($fpPersonAdrHRec['provstate']));
			$patient->setPostalCode($this->processString($fpPersonAdrHRec['mailcode']));
			$patient->setCountry($this->processString($fpPersonAdrHRec['countrycd']));
			$patient->setHphone($this->processString($fpPersonAdrHRec['phone']));
			$patient->setCellphone($this->processString($fpPersonAdrHRec['mobile']));
		}

		if ($fpPersonAdrWRec) {
			$patient->setWphone($this->processString($fpPersonAdrWRec['phone']));
		}

		if ($fpPersonPrvRefDocRec) {
			if ($fpPersonPrvRefDocRec['HYPE_DOCTOR_TYPE'] == 'REFDOC') {
				$patient->setRefDocId($fpPersonPrvRefDocRec['HYPE_DOCTOR_ID']);
				$patient->setRefDocNum($patient->ReferringDoctor->getProviderNum());
			}
		}

		if ($fpPersonPrvFamDocRec) {
			if ($fpPersonPrvFamDocRec['HYPE_DOCTOR_TYPE'] == 'REFDOC') {
				$patient->setFamDocId($fpPersonPrvFamDocRec['HYPE_DOCTOR_ID']);
				$patient->setFamDocNum($patient->FamilyDoctor->getProviderNum());
			}
		}

		// HCN Expiry Date should be 1997 or greater
		if ($patient->getHcnExpYear()) {
			$year = intval($patient->getHcnExpYear());

			if ($year < 1997) {
				$patient->setHcnExpYear(null);
			}
		}

		// Date of Birth should be in the 20th century or greater and should be in this calendar year or less
		if ($patient->getDob()) {
			$year = intval($patient->getDob());

			if ($year < 1900) {
				$patient->setDob(null);
			}
			else if ($year > date('Y')) {
				$patient->setDob(null);
			}
		}

		if ($fpPersonRec['chartno']) {
			$note = new Notes();
			$note->setNote($this->processString($fpPersonRec['chartno'], true));
			$note->setClientId($this->clientId);

			$patient->Notes->add($note);
		}

		if ($fpPersonNoteRecs) {
			foreach ($fpPersonNoteRecs as $noteRec)
			{
				if ($noteRec['classname'] != 'H') {
					$note = new Notes();
					$note->setNote($this->processString($noteRec['classname'] . '  ' . $noteRec['comments'], true));
					$note->setClientId($this->clientId);

					$patient->Notes->add($note);
				}
			}
		}

		if ($fpPersonMiscRecs) {
			foreach ($fpPersonMiscRecs as $noteRec)
			{
				if ($noteRec['comments']) {
					$note = new Notes();
					$note->setNote($this->processString(str_replace('(IMPORTED)-', '', $noteRec['comments']), true));
					$note->setClientId($this->clientId);

					$patient->Notes->add($note);
				}
			}
		}

		try {
			$conn = Doctrine_Manager::connection();
			$conn->beginTransaction();

			$patient->save();
			$success = $this->setFPPersonHypeID($fpPatientID, $patient->get('id'));
			$fkCount = $this->setFPPersonFKsHypeIDs($fpPatientID, $patient->get('id'));

			if ($success) {
				echo '  Created Patient profile for ' . $patient->getFullName() . "\n";
				echo '  Updated FP Patient Record ' . $fpPatientID . ' with Hype ID ' . $patient->get('id') . "\n";
				echo '  Updated ' . $fkCount . ' fkperson records in ' . count($this->fkPersonTables) . ' tables.' . "\n\n";

				$conn->commit();
			}
		}
		catch (Exception $e) {
			echo $e->getMessage() . "\n";
			echo $e->getTraceAsString() . "\n";

			echo "\n\n\n" . 'Please ensure source database record with pkguid ' . $fpPatientID . ' has a null value in HYPE_ID' . "\n";
			exit();
		}
	}

    private function getHypeDefaultPatientStatusID()
	{
        $cp = ClientParamTable::getParamForClient($this->clientId, 'patient_status_default');
        if ($cp instanceof ClientParam) {
            return $cp->value;
        }

        return null;
    }

    private function getHypeDefaultPatientTypeID()
    {
        $cp = ClientParamTable::getParamForClient($this->clientId, 'default_patient_type_id');
        if ($cp instanceof ClientParam) {
            return $cp->value;
        }

        return null;
    }

    private function getFPPersonByGUID($guid)
    {
        $sql = 'SELECT * from person where pkguid = ?';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($guid));

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    private function getFPProvinsnoByFKPerson($fkPerson)
    {
        $sql = 'SELECT * from provinsno where fkperson = ? order by updated desc';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($fkPerson));

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    private function getFPPersonAdrHByFKPerson($fkPerson)
    {
        $sql = 'SELECT * from personadrh where fkperson = ? order by updated desc';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($fkPerson));

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    private function getFPPersonAdrWByFKPerson($fkPerson)
    {
        $sql = 'SELECT * from personadrw where fkperson = ? order by updated desc';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($fkPerson));

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    private function getFPPersonPrvByFKPersonAndPhysType($fkPerson, $physType)
    {
        $sql = 'SELECT * from personprv where fkperson = ? and phystype = ? order by updated desc';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($fkPerson, $physType));

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    private function getFPPerClassByFKPerson($fkPerson)
    {
        $sql = 'SELECT * from perclass where fkperson = ? order by updated asc';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($fkPerson));

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    private function getFPPersMiscByFKPerson($fkPerson)
    {
        $sql = 'SELECT * from persmisc where fkperson = ? order by updated asc';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($fkPerson));

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    private function setFPPersonHypeID($personPKGUID, $hypePatientID)
    {
        $sql = 'UPDATE person set HYPE_PATIENT_ID = ? where pkguid = ?';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($hypePatientID, $personPKGUID));

        return $sth->rowCount();
    }

    private function setFPPersonFKsHypeIDs($FKPerson, $hypePatientID)
    {
        $count = 0;

        foreach ($this->fkPersonTables as $table) {
            if ($table != 'person') {
                $sql = 'UPDATE ' . $table . ' set HYPE_PATIENT_ID = ? where fkperson = ?';
                $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $sth->execute(array($hypePatientID, $FKPerson));
                $count += $sth->rowCount();
			}
		}

        return $count;
	}

	private function importThirdParties()
	{
		echo "\n" . 'Step 4: Import Third Party Providers' . "\n";
		$execute = true;

		while ($execute == true && $this->count <= $this->recordLimit)
		{
			$fpInscoRec = $this->getFPLibInsCosRecToImport();


			if ($fpInscoRec) {
				$thirdParty = new ThirdParty();
				$thirdParty->setClientId($this->clientId);
				$thirdParty->setName($fpInscoRec['insureco']);
				$thirdParty->setContact($fpInscoRec['attn']);
				$thirdParty->setAddress($fpInscoRec['address']);
				$thirdParty->setCity($fpInscoRec['city']);
				$thirdParty->setProvince($fpInscoRec['provstate']);
				$thirdParty->setPostalCode($fpInscoRec['mailcode']);
				$thirdParty->setCountry($fpInscoRec['countrycd']);
				$thirdParty->setPhone($fpInscoRec['phone']);
				$thirdParty->setFax($fpInscoRec['fax']);

				try {
					$conn = Doctrine_Manager::connection();
					$conn->beginTransaction();

					$thirdParty->save();
					$success = $this->setFPThirdPartyHypeID($fpInscoRec['pkguid'], $thirdParty->get('id'));
					$fkCount = $this->setFPThirdPartyFKsHypeIDs($fpInscoRec['pkguid'], $thirdParty->get('id'));

					if ($success) {
						echo '  Created Third Party profile for ' . $thirdParty->getName() . "\n";
						echo '  Updated FP Third Party Record ' . $fpInscoRec['pkguid'] . ' with Hype ID ' . $thirdParty->get('id') . "\n";
						echo '  Updated ' . $fkCount . ' fklibinscos records in ' . count($this->fkThirdPartyTables) . ' tables.' . "\n\n";

						$conn->commit();
					}
					$this->count++;
				}
				catch (Exception $e) {
					echo $e->getMessage() . "\n";
					echo $e->getTraceAsString() . "\n";

					echo "\n\n\n" . 'Please ensure source database record with pkguid ' . $fpInscoRec['pkguid'] . ' has a null value in HYPE_THIRD_PARTY_ID' . "\n";
					exit();
				}
			}
			else {
				$execute = false;
				echo '    No more Third Party Providers to import.' . "\n";
			}
		}
	}

    private function getFPLibInsCosRecToImport()
    {
        $sql = 'SELECT * from libinscos where HYPE_THIRD_PARTY_ID is null limit 1';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute();

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    private function setFPThirdPartyHypeID($thirdPartyPKGUID, $hypeID)
    {
        $sql = 'UPDATE libinscos set HYPE_THIRD_PARTY_ID = ? where pkguid = ?';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($hypeID, $thirdPartyPKGUID));

        return $sth->rowCount();
    }

    private function setFPThirdPartyFKsHypeIDs($thirdPartyPKGUID, $hypeID)
    {
        $count = 0;

        foreach ($this->fkThirdPartyTables as $table) {
            if ($table != 'libinscos') {
                $sql = 'UPDATE ' . $table . ' set HYPE_THIRD_PARTY_ID = ? where fklibinscos = ?';
                $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $sth->execute(array($hypeID, $thirdPartyPKGUID));
                $count += $sth->rowCount();
            }
        }

        return $count;
    }

    private function importBatches()
    {
        echo "\n" . 'Step 5: Import Batches' . "\n";
        $execute = true;

        while ($execute == true && $this->count <= $this->recordLimit) {
            $fpOSubTopRec = $this->getFPOSubTopRecToImport();

            if ($fpOSubTopRec) {
                if ($fpOSubTopRec['HYPE_DOCTOR_TYPE'] != 'DOCTOR') {
                    $this->importDoctorByFKProvider($fpOSubTopRec['fkprovider']);
                }
                else {
                    $this->importBatchByOSubTop($fpOSubTopRec);
                }
                $this->count++;
            }
            else {
                $execute = false;
                echo '    No more Batches to import.' . "\n";
            }
        }
    }

    private function getFPOSubTopRecToImport()
    {
        $sql = 'SELECT * from osubtop where HYPE_BATCH_ID is null and HYPE_DOCTOR_ID is not null limit 1';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute();

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

	private function importBatchByOSubTop($fpOSubTopRec)
	{
		$batchNumber = hype::getFormattedDate('Ymd', $fpOSubTopRec['subdate'], hype::DB_ISO_DATE) . $fpOSubTopRec['subno'];

		$batch = new Batch();
		$batch->setBatchNumber($batchNumber);
		$batch->setClientID($this->clientId);
		$batch->setLocationId($this->locationId);
		$batch->setDoctorId($fpOSubTopRec['HYPE_DOCTOR_ID']);
		$batch->setBatchDate(hype::getFormattedDate(hype::DB_ISO_DATE, $fpOSubTopRec['subdate'], hype::DB_ISO_DATE));
		$batch->setMohOffice($batch->Doctor->MOHOffice->getMohOfficeCode());
		$batch->setGroupNum($batch->Doctor->getGroupNum());
		$batch->setProviderNum($batch->Doctor->getProviderNum());
        $batch->setSpecCodeId($batch->Doctor->getSpecCodeID());
		$batch->setSubmitDate(hype::getFormattedDate(hype::DB_ISO_DATE, $fpOSubTopRec['subdate'], hype::DB_ISO_DATE));
		$batch->setFileName($fpOSubTopRec['filename']);
		$batch->setHasFile(false);
		$batch->setStatus(BatchTable::STATUS_ACKNOWLEDGED);
		$batch->setAcknowledgementStatus(Batchtable::ACK_STATUS_ARCHIVED);

		try {
			$conn = Doctrine_Manager::connection();
			$conn->beginTransaction();

			$batch->save();
			$success = $this->setFPBatchHypeID($fpOSubTopRec['pkguid'], $batch->get('id'));
			$fkCount = $this->setFPBatchFKsHypeIDs($fpOSubTopRec['pkguid'], $batch->get('id'));

			if ($success) {
				echo '  Created Batch record ' . $batch->getBatchNumber() . "\n";
				echo '  Updated FP OSubTop Record ' . $fpOSubTopRec['pkguid'] . ' with Hype ID ' . $batch->get('id') . "\n";
				echo '  Updated ' . $fkCount . ' fkosubtop records in ' . count($this->fkBatchTables) . ' tables.' . "\n\n";

				$conn->commit();
			}
		}
		catch (Exception $e) {
			echo $e->getMessage() . "\n";
			echo $e->getTraceAsString() . "\n";

			echo "\n\n\n" . 'Please ensure source database record with pkguid ' . $fpOSubTopRec['pkguid'] . ' has a null value in HYPE_BATCH_ID' . "\n";
			exit();
		}
	}

    private function setFPBatchHypeID($batchPKGUID, $hypeID)
	{
        $sql = 'UPDATE osubtop set HYPE_BATCH_ID = ? where pkguid = ?';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($hypeID, $batchPKGUID));

        return $sth->rowCount();
    }

    private function setFPBatchFKsHypeIDs($batchPKGUID, $hypeID)
    {
        $count = 0;

        foreach ($this->fkBatchTables as $table) {
            if ($table != 'osubtop') {
                $sql = 'UPDATE ' . $table . ' set HYPE_BATCH_ID = ? where fkosubtop = ?';
                $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $sth->execute(array($hypeID, $batchPKGUID));
                $count += $sth->rowCount();
            }
        }

        return $count;
    }

    private function importAppointments()
    {
        echo "\n" . 'Step 8: Import Appointments' . "\n";
		$execute = true;

		while ($execute == true && $this->count <= $this->recordLimit)
		{
            $fpAppt2Rec = $this->getFPAppt2RecToImport();

            if ($fpAppt2Rec) {
                $this->importAppointmentByAppt2($fpAppt2Rec);
                $this->count++;
            }
            else {
                $execute = false;
                echo '    No more Appointments to import.' . "\n";
            }
        }
    }

    private function getFPAppt2RecToImport()
    {
        $sql = 'SELECT * from appt2 where HYPE_APPOINTMENT_ID is null and HYPE_PATIENT_ID is not null order by realdate desc limit 1';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute();

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    private function importAppointmentByAppt2($fpAppt2Rec)
    {
        if (!$fpAppt2Rec['HYPE_DOCTOR_ID']) {
            $this->importDoctorFromAppt2($fpAppt2Rec);
        }
        else if ($fpAppt2Rec['HYPE_DOCTOR_TYPE'] != 'DOCTOR') {
            print_r($fpAppt2Rec);
            echo 'DOCTOR TYPE IS NOT DOCTOR - make one somehow' . "\n";
            exit();
        }
        else if (!$fpAppt2Rec['HYPE_PATIENT_ID']) {
            print_r($fpAppt2Rec);
            echo 'NO PATIENT RECORD - something something' . "\n";
            exit();
        }
        else {
            $appointmentTypeId = $this->getHypeAppointmentTypeID();
            $attendedStageClientID = $this->getHypeAttendedStageClientID();
            $bookedStageClientID = $this->getHypeBookedStageClientID();
            $today = date(hype::DB_DATETIME_FORMAT);

            $notes = '';
            if ($fpAppt2Rec['visitreason']) {
                $notes = $fpAppt2Rec['visitreason'];
            }
            if ($fpAppt2Rec['notes']) {
                if (strlen($notes)) {
                    $notes .= "\n";
				}
                $notes .= $fpAppt2Rec['notes'];
            }

            $startDate = $this->getDate($fpAppt2Rec['realdate'], $fpAppt2Rec['timestart']);
            $endDate = $this->getDate($fpAppt2Rec['realdate'], $fpAppt2Rec['timeend']);

            $appointment = new Appointment();
            $attendee = new AppointmentAttendee();

            $appointment->setClientId($this->clientId);
            $appointment->setSlotType(AppointmentTable::SLOT_TYPE_NORMAL);
            $appointment->setLocationId($this->locationId);
            $appointment->setAppointmentTypeId($appointmentTypeId);
            $appointment->setDoctorID($fpAppt2Rec['HYPE_DOCTOR_ID']);
            $appointment->setDoctorCode($appointment->Doctor->getQcDoctorCode());

            $appointment->setStartDate(hype::getFormattedDateTime(hype::DB_ISO_DATE, $startDate, hype::DB_ISO_DATE));
            $appointment->setEndDate(hype::getFormattedDateTime(hype::DB_ISO_DATE, $endDate, hype::DB_ISO_DATE));
            $appointment->setNotes($this->processString($notes, true));

            if ($appointment->getStartDate() > $today) {
                $appointment->setStatus(AppointmentTable::STATUS_POSTED);
                $appointment->setStageClientId($bookedStageClientID);
                $attendee->setStatus(AppointmentAttendeeTable::STATUS_BOOKED);
            }
            else {
                $appointment->setStatus(AppointmentTable::STATUS_ATTENDED);
                $appointment->setStageClientId($attendedStageClientID);
                $attendee->setStatus(AppointmentAttendeeTable::STATUS_ATTENDED);
            }

            $attendee->setPatientId($fpAppt2Rec['HYPE_PATIENT_ID']);
            $attendee->setPatientFname($attendee->Patient->getFname());
            $attendee->setPatientLname($attendee->Patient->getLname());
            $attendee->setPatientPhone($attendee->Patient->getHphone());
            $attendee->setPatientEmail($attendee->Patient->getEmail());
            $attendee->setPatientNumber($attendee->Patient->getPatientNumber());
            $attendee->setClientId($this->clientId);

            $appointment->AppointmentAttendee->add($attendee);

            try {
                $conn = Doctrine_Manager::connection();
                $conn->beginTransaction();

                $appointment->save();
                $success = $this->setFPAppointmentHypeID($fpAppt2Rec['keyid'], $appointment->get('id'));

                if ($success) {
                    echo '  Created Appointment  ' . $appointment->get('id') . "\n";
                    echo '  Updated FP Appt2 Record ' . $fpAppt2Rec['keyid'] . ' with Hype ID ' . $appointment->get('id') . "\n\n";

                    $conn->commit();
                }
            }
            catch (Exception $e) {
                echo $e->getMessage() . "\n";
                echo $e->getTraceAsString() . "\n";

                echo "\n\n\n" . 'Please ensure source database record with pkguid ' . $fpAppt2Rec['keyid'] . ' has a null value in HYPE_CLAIM_ID' . "\n";
                exit();
            }
        }
    }

    private function importDoctorFromAppt2($appt2)
    {
        $fpDoctorID = $appt2['fkprovider'];

        print_r($appt2);

        $doctor = new Doctor();
        $doctor->setClientID($this->clientId);

        $name = $appt2['forwho'];
        $doctor->setLname(substr($appt2['forwho'], 0, strpos($appt2['forwho'], ',')));
        $doctor->setFname(substr($appt2['forwho'], strpos($appt2['forwho'], ',') + 1));

        $doctor->setGroupNum('0000');
        $doctor->setProviderNum('000000');
        $doctor->setSpecCodeId('00');


        $doctor->setActive(true);

        // qc doctor code
        $qcCode = $doctor->fname[0] . $doctor->lname[0];
        $doctor->setQcDoctorCode(DoctorTable::checkDoctorCode($qcCode, $this->clientId));

        $regions = $this->getHypeRegions();
        foreach ($regions as $region) {
            $doctorRegion = new DoctorRegion();
            $doctorRegion->setRegionId($region->getId());
            $doctor->DoctorRegion->add($doctorRegion);
        }

        $locations = $this->getHypeLocations();
        foreach ($locations as $location) {
            $doctorLocation = new DoctorLocation();
            $doctorLocation->setLocationId($location->getId());
            $doctor->DoctorLocation->add($doctorLocation);
        }

        try {
            $conn = Doctrine_Manager::connection();
            $conn->beginTransaction();

            $doctor->save();
            $fkCount = $this->setFPProviderFKsHypeIDs($fpDoctorID, $doctor->get('id'), 'DOCTOR');

            echo '  Created Doctor profile for ' . $doctor->getDropdownString() . "\n";
            echo '  Updated FP Provider Record ' . $fpDoctorID . ' with Hype ID ' . $doctor->get('id') . "\n";
            echo '  Updated ' . $fkCount . ' fkprovider records in ' . count($this->fkProviderTables) . ' tables.' . "\n";
            $conn->commit();
        }
        catch (Exception $e) {
            echo $e->getMessage() . "\n";
            echo $e->getTraceAsString() . "\n";

            echo "\n\n\n" . 'Please ensure source database record with pkguid ' . $fpDoctorID . ' has a null value in HYPE_ID' . "\n";
            exit();
        }

        exit();
    }

    private function getHypeAppointmentTypeID()
    {
        $appointmentType = Doctrine_Query::create()
            ->from('AppointmentType at')
            ->addWhere('at.client_id = (?)', array($this->clientId))
            ->addWhere('at.name = (?)', array('IMPORTED'))
            ->fetchOne();

        if (!$appointmentType instanceof AppointmentType) {
            $appointmentType = new AppointmentType();
            $appointmentType->client_id = $this->clientId;
            $appointmentType->slot_type = AppointmentTable::SLOT_TYPE_NORMAL;
            $appointmentType->name = 'IMPORTED';
            $appointmentType->appt_lengths = 15;
            $appointmentType->colour = '#ff0000';
            $appointmentType->title_colour = '#bb0000';
            $appointmentType->save();
        }

        return $appointmentType->get('id');
    }

    private function getHypeAttendedStageClientID()
    {
        $stageClient = Doctrine_Query::create()
            ->from('StageClient sc')
            ->addWhere('sc.client_id = (?)', array($this->clientId))
            ->addWhere('sc.stage_name = (?)', array('ATTENDED'))
            ->fetchOne();

        if (!$stageClient instanceof StageClient) {
            echo 'No ATTENDED Stage Client record. Go make one.' . "\n";
            exit();
        }

        return $stageClient->get('id');
    }

    private function getHypeBookedStageClientID()
    {
        $stageClient = Doctrine_Query::create()
            ->from('StageClient sc')
            ->addWhere('sc.client_id = (?)', array($this->clientId))
            ->addWhere('sc.stage_name = (?)', array('BOOKED'))
            ->fetchOne();

        if (!$stageClient instanceof StageClient) {
            echo 'No BOOKED Stage Client record. Go make one.' . "\n";
            exit();
        }

        return $stageClient->get('id');
    }

    private function getDate($date, $time)
    {
        $rv = $date;

        $hrs = floor($time / 60);
        $minutes = $time % 60;

        $rv .= ' ' . ($hrs < 10 ? '0' : '') . $hrs . ':' . ($minutes < 10 ? '0' : '') . $minutes . ':00';

        return $rv;
    }

    private function setFPAppointmentHypeID($appointmentPKGUID, $hypeID)
    {
        $sql = 'UPDATE appt2 set HYPE_APPOINTMENT_ID = ? where keyid = ?';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($hypeID, $appointmentPKGUID));

        return $sth->rowCount();
    }

    private function importOHIPClaims()
    {
        echo "\n" . 'Step 6: Import OHIP Claims' . "\n";
        $execute = true;

        while ($execute == true && $this->count <= $this->recordLimit) {
            $fpBCTopRec = $this->getFPBCTopRecToImport();

            if ($fpBCTopRec) {
                if ($fpBCTopRec['HYPE_DOCTOR_TYPE'] != 'DOCTOR') {
                    print_r($fpBCTopRec);
                    echo 'REFERRING DOCTOR FOUND IN CLAIM - CREATE A DOCTOR RECORD';
                    exit();
                }
				else {
                    $this->importClaimByBCTop($fpBCTopRec);
                    $this->count++;
				}
			}
			else {
				$execute = false;
                echo '    No more Claims to import.' . "\n";
			}
		}
	}

    private function getFPBCTopRecToImport()
    {
        $sql = 'SELECT * from bctop where HYPE_CLAIM_ID is null and fkprovider != 0 and HYPE_PATIENT_ID is not null and HYPE_DOCTOR_ID is not null limit 1';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute();

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

	private function importClaimByBCTop($fpBCTopRec)
	{
		$fpBOhipBIRec = $this->getFPBOhipBIRecByFkBCTop($fpBCTopRec['pkguid']);

		$claim = new Claim();
		$claim->setClientID($this->clientId);
		$claim->setLocationId($this->locationId);

		$claim->patient_id = $fpBCTopRec['HYPE_PATIENT_ID'];
		if ($claim->Patient instanceof Patient) {
			$claim->setPatientNumber($claim->Patient->getPatientNumber());
			$claim->setHealthNum($claim->Patient->getHcn());
			$claim->setVersionCode($claim->Patient->getVersionCode());
			$claim->setDob($claim->Patient->getDob());
			$claim->setFname($claim->Patient->getFname());
			$claim->setLname($claim->Patient->getLname());
			$claim->setSex($claim->Patient->getSex());
			$claim->setProvince($claim->Patient->getMohProvince());
		}

		$acctNum = $fpBCTopRec['billno'];
		while (strlen($acctNum) < 8)
		{
			$acctNum = "0" . $acctNum;
		}

		$claim->setAcctNum($acctNum);
		if ($fpBCTopRec['billdate']) {
			$claim->setDateCreated(hype::getFormattedDate(hype::DB_DATE_FORMAT, $fpBCTopRec['billdate'], hype::DB_DATE_FORMAT));
		}

		if ($fpBCTopRec['HYPE_DOCTOR_TYPE'] == 'DOCTOR') {
			$claim->setDoctorId($fpBCTopRec['HYPE_DOCTOR_ID']);
			$claim->setDoctorCode($claim->Doctor->getQcDoctorCode());
		}
		else {
			echo 'BCTOP.fkprovider maps to a Referring Doctor - create a normal doctor profile for them' . "\n";
			exit();
		}

		if ($fpBOhipBIRec) {
			if ($fpBOhipBIRec['payprog'] == 'HCP' && $fpBOhipBIRec['payee'] == 'P') {
				$claim->setPayProg(ClaimTable::CLAIM_TYPE_HCPP);
			}
			else if ($fpBOhipBIRec['payprog'] == 'RMB') {
				$claim->setPayProg(ClaimTable::CLAIM_TYPE_RMB);
			}
			else if ($fpBOhipBIRec['payprog'] == 'WCB') {
				$claim->setPayProg(ClaimTable::CLAIM_TYPE_WCB);
			}
			else {
				echo 'UNSUPPORTED CLAIM PAYEE IN bohipbi - ' . $fpBOhipBIRec['payprog'] . ', ' . $fpBOhipBIRec['payee'] . "\n";
				exit();
			}

			if ($fpBOhipBIRec['refprovider']) {
				$fpProviderRec = $this->getFPProviderByGUID($fpBOhipBIRec['refprovider']);

				if ($fpProviderRec) {
					if ($fpProviderRec['HYPE_DOCTOR_TYPE'] == 'REFDOC') {
						$claim->setRefDocId($fpProviderRec['HYPE_DOCTOR_ID']);
						$claim->setRefDoc($claim->ReferringDoctor->getProviderNum());
					}
					else {
						$fpProvoBillRec = $this->getFPProvoBillByFkProvider($fpProviderRec['pkguid']);

						if ($fpProvoBillRec) {
							$refDoc = $this->getHypeReferringDoctorByProviderNumber($fpProvoBillRec['physno']);

							if ($refDoc instanceof ReferringDoctor) {
								$claim->setReferringDoctor($refDoc);
								$claim->setRefDoc($refDoc->getProviderNum());
							}
							else {
								$claim->setRefDoc($fpProvoBillRec['physno']);
							}
						}
					}
				}
			}

			if ($fpBOhipBIRec['hospitalno']) {
				$claim->setFacilityNum($fpBOhipBIRec['hospitalno']);
			}

			if ($fpBOhipBIRec['doa']) {
				$claim->setAdmitDate(hype::getFormattedDate(hype::DB_DATE_FORMAT, $fpBOhipBIRec['doa'], hype::DB_DATE_FORMAT));
			}

			if ($fpBOhipBIRec['sli']) {
				echo 'FILL IN SLI' . "\n";
				exit();
			}
		}

		// CLAIM ITEMS
		$fpBCLineRecs = $this->getFPBCLinesByFKBCTop($fpBCTopRec['pkguid']);
		foreach ($fpBCLineRecs as $fpBCLineRec)
		{
			$fpBOhipLiRec = $this->getFPBOhipLiRecByFKBCLine($fpBCLineRec['pkguid']);
			$fpBCPaidLineRecs = $this->getFPBCPaidLineRecsByFKBCLine($fpBCLineRec['pkguid']);

			$claimItem = new ClaimItem();
			$claimItem->setClientId($this->clientId);
			$claimItem->setItemId(count($claim->ClaimItem) + 1);
			$claimItem->setServiceCode($this->processString($fpBCLineRec['itemcode']));
			$claimItem->setNumServ($this->processString($fpBCLineRec['qty']));
			$claimItem->setBaseFee($this->processString($fpBCLineRec['ppu']));

            $linePaid = $this->processString($fpBCLineRec['linepaid']);

			if ($fpBOhipLiRec) {
				$fpOSubLineRec = $this->getFPOSubLineRecByFKBOhipLi($fpBOhipLiRec['pkguid']);

				$claimItem->setFeeSubm($claimItem->getBaseFee() * $claimItem->getNumServ());
				$claimItem->setServiceDate(hype::getFormattedDate(hype::DB_DATE_FORMAT, $fpBOhipLiRec['dos'], hype::DB_DATE_FORMAT));
				$claimItem->setDiagCode($this->processString($fpBOhipLiRec['diagcode']));
				$claim->setManualReview($claim->getManualReview() || $fpBOhipLiRec['manrev']);

				if ($fpOSubLineRec) {
					if ($fpOSubLineRec['HYPE_BATCH_ID']) {
						$claim->setBatchId($fpOSubLineRec['HYPE_BATCH_ID']);
						$claim->setBatchNumber($claim->Batch->getBatchNumber());
					}
				}
			}

			if (!$claim->batch_id) {
				$claim->setBatchNumber(ClaimTable::EMPTY_BATCH);
			}

			if (count($fpBCPaidLineRecs)) {
				foreach ($fpBCPaidLineRecs as $fpBCPaidLineRec)
				{
					if ($linePaid == 1) {
                        $claimItem->setStatus(ClaimItemTable::STATUS_PAID_CLOSED);

                        if ($fpBCPaidLineRec['amount'] > 0) {
                            $claimItem->setFeePaid($claimItem->getFeePaid() + $fpBCPaidLineRec['amount']);
                        } else if ($fpBCPaidLineRec['amount'] == 0) {
                            $claimItem->setFeePaid($claimItem->getFeeSubm());
                        }
                    } else if ($linePaid == 0) {
                        $claimItem->setStatus(ClaimItemTable::STATUS_REJECTED_CLOSED);
                    }
				}
			}

			$claim->ClaimItem->add($claimItem);
		}

		try {
			$conn = Doctrine_Manager::connection();
			$conn->beginTransaction();

			$claim->save();
			$success = $this->setFPClaimHypeID($fpBCTopRec['pkguid'], $claim->get('id'));
			$fkCount = $this->setFPClaimFKsHypeIDs($fpBCTopRec['pkguid'], $claim->get('id'));

			if ($success) {
				echo '  Created Claim  ' . $claim->get('id') . "\n";
				echo '  Updated FP BCTop Record ' . $fpBCTopRec['pkguid'] . ' with Hype ID ' . $claim->get('id') . "\n";
				echo '  Updated ' . $fkCount . ' fkbctop records in ' . count($this->fkClaimTables) . ' tables.' . "\n\n";

				$conn->commit();
			}
		}
		catch (Exception $e) {
			echo $e->getMessage() . "\n";
			echo $e->getTraceAsString() . "\n";

			echo "\n\n\n" . 'Please ensure source database record with pkguid ' . $fpBCTopRec['pkguid'] . ' has a null value in HYPE_CLAIM_ID' . "\n";
			exit();
		}
	}

    private function getFPBOhipBIRecByFkBCTop($fkBcTop)
	{
        $sql = 'SELECT * from bohipbi where fkbctop = ? order by updated asc';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($fkBcTop));

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    private function getHypeReferringDoctorByProviderNumber($providerNum)
    {
        return Doctrine_Query::create()
            ->from('ReferringDoctor rd')
            ->addWhere('rd.client_id = (?)', array($this->clientId))
            ->addWhere('rd.provider_num = (?)', array($providerNum))
            ->fetchOne();
	}

    private function getFPBCLinesByFKBCTop($fkBCTop)
	{
        $sql = 'SELECT * from bcline where fkbctop = ? order by updated asc';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($fkBCTop));

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    private function getFPBOhipLiRecByFKBCLine($fkBCLine)
    {
        $sql = 'SELECT * from bohipli where fkbcline = ?';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($fkBCLine));

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    private function getFPBCPaidLineRecsByFKBCLine($fkBCLine)
    {
        $sql = 'SELECT * from bcpaidline where fkbcline = ? order by updated asc';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($fkBCLine));

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    private function getFPOSubLineRecByFKBOhipLi($fkBOhipLi)
    {
        $sql = 'SELECT * from osubline where fkbohipli = ? order by updated desc';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($fkBOhipLi));

        return $sth->fetch(PDO::FETCH_ASSOC);

    }

    private function setFPClaimHypeID($claimPKGUID, $hypeID)
    {
        $sql = 'UPDATE bctop set HYPE_CLAIM_ID = ? where pkguid = ?';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array($hypeID, $claimPKGUID));

        return $sth->rowCount();
	}

    private function setFPClaimFKsHypeIDs($claimPKGUID, $hypeID)
	{
        $count = 0;

        foreach ($this->fkClaimTables as $table)
		{
            if ($table != 'bctop') {
                $sql = 'UPDATE ' . $table . ' set HYPE_CLAIM_ID = ? where fkbctop = ?';
                $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $sth->execute(array($hypeID, $claimPKGUID));
                $count += $sth->rowCount();
			}
		}

        return $count;
	}

    protected function configure()
	{
        $this->namespace = 'hype';
        $this->name = 'importFoxproVPData';
        $this->briefDescription = "Imports VP formatted database.";

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('clientID', null, sfCommandOption::PARAMETER_REQUIRED, 'Client ID'),
            new sfCommandOption('locationID', null, sfCommandOption::PARAMETER_REQUIRED, 'Location ID'),
        ));
	}

    private function resetHypeColumnsToNull()
	{
        foreach ($this->fkProviderTables as $table) {
            $this->resetHypeColumnToNull($table, 'HYPE_DOCTOR_ID');
            $this->resetHypeColumnToNull($table, 'HYPE_DOCTOR_TYPE');
        }
        echo "\n";

        foreach ($this->fkPersonTables as $table) {
            $this->resetHypeColumnToNull($table, 'HYPE_PATIENT_ID');
		}
        echo "\n";

        foreach ($this->fkThirdPartyTables as $table) {
            $this->resetHypeColumnToNull($table, 'HYPE_THIRD_PARTY_ID');
		}
        echo "\n";

        foreach ($this->fkBatchTables as $table) {
            $this->resetHypeColumnToNull($table, 'HYPE_BATCH_ID');
		}
        echo "\n";

        foreach ($this->fkClaimTables as $table) {
            $this->resetHypeColumnToNull($table, 'HYPE_CLAIM_ID');
		}

        foreach ($this->fkAppointmentTables as $table) {
            $this->resetHypeColumnToNull($table, 'HYPE_APPOINTMENT_ID');
        }
	}

    private function resetHypeColumnToNull($table, $columnName)
    {
        $sql = 'update ' . $table . ' set ' . $columnName . ' = null where ' . $columnName . ' is not null';
        $sth = $this->database->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute();
        echo $columnName . ' set to null in table ' . $table . "\n";
	}
}
