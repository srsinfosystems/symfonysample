<?php
class importDatsTask extends sfBaseTask
{
	private $client_id = 2;
	private $location_id = 1;

	private $isATask = false;

	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'importDats';
		$this->briefDescription = "Imports CSV files to Hype Medical .";

		$this->addOptions(array(
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend')));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$filename = array('22apr_bch.dat', '20apr_ehc.dat', '22jul_BCH.dat', '22jul_EHC.dat');
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$fileToImport = 'C:\\dev\\DonnaCurtis\\WR130326.BAK';
		$dat_file = new importBAKFiles($fileToImport,  $this->client_id, $this->location_id);
		$dat_file->setIsATask(true);

		$fileToImport = 'C:\\dev\\DonnaCurtis\\WR130326.BAK';
		$dat_file = new importBAKFiles($fileToImport,  $this->client_id, $this->location_id);

		$dat_file->setDebug(true);
		$dat_file->run();

// 		$this->import();
	}
/*

	public function testPrint($p)
	{
		echo 'Fist Name -> ' . $p['fname'] . "\n";
		echo 'Last Name -> ' . $p['lname'] . "\n";
		echo 'Sex       -> ' . $p['sex'] . "\n";
		echo 'hcn       -> ' . $p['hcn_num'] . "\n";
		echo 'DOB       -> ' . $p['dob'] . "\n";
		echo 'Address   -> ' . $p['address'] . "\n";
		echo 'City      -> ' . $p['city'] . "\n";
		echo 'Postal C  -> ' . $p['postal_code'] . "\n";
		echo 'Province  -> ' . $p['province'] . "\n";
		echo 'Home Phone-> ' . $p['hphone'] . "\n";
		echo 'HCN vc    -> ' . $p['hcn_version_code']. "\n";
		echo 'P type id -> ' . $p['patient_type_id'] . "\n";
		echo 'P. Sts id -> ' . $p['patient_status_id'] . "\n";
		echo 'Notes     -> ' . $p['Notes'][0]['note'] . "\n";

		echo "\n" . '----------------------------------------------------------------' . "\n";

	}
	public function findDuplicated($hcn, $client_Id)
	{
		$patients = Doctrine_Query::create()
		->from('Patient p')
		->addWhere('p.hcn_num = (?)', $hcn)
		->addWhere('p.client_id = (?)', $client_Id)
		->addWhere('p.deleted = (?)', false)
		->execute();
		return $patients;
	}
	public function import()
	{
		$n=0;
		$total=0;
		for ($j=0; $j<=3; $j++ )
		{
			echo 'n = ' . $n . ' File to insert --> ' . $this->filename[$n] . "\n";

			$txt_file = file_get_contents('C:\\public_html\\cvs_imports\\dats\\' . $this->filename[$n]);

			$rows = explode("\n", $txt_file);

			$patient_type_id = null;
			$patient_status_id = null;

			$cp = ClientParamTable::getParamForClient($client_id, 'patient_status_default');
			if ($cp instanceof ClientParam) {
				$patient_status_id = $cp->value;
			}

			$cp = ClientParamTable::getParamForClient($client_id, 'default_patient_type_id');
			if ($cp instanceof ClientParam) {
				$patient_type_id = $cp->value;
			}
			echo 'Processing file --> ' . $this->filename[$n] . "\n";
			$i = 0;
			foreach ($rows as $row)
			{
				$duplicates = array();

				if (strlen($row)>170) {
					//echo $i;
					$patient = new Patient();
					//---------------------------/
					$patient->client_id=$this->client_id;
					$patient->location_id = $this->location_id;
					//---------------------------/

					$patient->patient_number = strtoupper(trim(substr($row, 1,9)));
					$name = strtoupper(trim(substr($row, 20,30)));
					$names = explode(',',$name);

					$patient->lname = $names[0];
					$patient->fname = $names[1];
					$patient->sex = strtoupper(trim(substr($row, 50,1)));
					$dob = strtoupper(trim(substr($row, 51,10)));
					$patient->dob = substr($dob,-4) . '-' . substr($dob,3,2) . '-' . substr($dob,0,2);
					$patient->address = strtoupper(trim(substr($row, 64,20)));
					$patient->city = strtoupper(trim(substr($row, 94,15)));

					$patient->postal_code = strtoupper(trim(substr($row, 109,7)));
					$patient->hphone = strtoupper(trim(substr($row, 116,12)));

					$patient->hcn_version_code = strtoupper(trim(substr($row, 141,2)));
					$date = strtoupper(trim(substr($row, 202,10)));
					if ($date) {
						$patient->hcn_exp_year = substr($date,0,-4) . '-' . substr($date,4,-2) . '-' . substr($date,-2);
					}
					$patient->patient_type_id = $patient_type_id;
					$patient->patient_status_id = $patient_status_id;

					$hcnum = strtoupper(trim(substr($row, 130,10)));
					echo "\n" . 'HC num    -> ' . $hcnum . "\n";
					$note = new Notes();
					$note->client_id = $client_id;
					$note->note = 'n=> ' . $n . ':  Imported from file ' . $this->filename[$n];
					if (strlen($hcnum) > 1) {
						$patient->hcn_num = hype::encryptHCN($hcnum);
						$duplicates = $this->findDuplicated($patient->hcn_num, $patient->getClientId());
						$patient->province = 'ON';
					}
					else {
						$note->note = $note->note . "\n" . '  There was no Health Card Number to import.';
					}
					$note->note = $note->note . "\n" . 'Contact info: ' . substr($row, 143,27);
					$patient->Notes->add($note);

					if(!count($duplicates))
					{
						$this->testPrint($patient);
						try {
							$patient->save();
						}
						catch (Exception $e) {
							echo $e->getMessage() . "\n";
							print_r($patient->toarray());
							echo ' Records inserted --> ' . $i . "\n";
							exit();
						}
					}
					else {
						echo 'already in data base ...... ' . "\n";
					}
				}
				$i++;
				$total++;
				echo 'Total of procesed records == ' . $total . "\n";
				echo 'Current file == ' . $this->filename[$n] . "\n";
				echo 'Total of procesed records in this file == ' . $i . "\n";
			}
			$n++;
			echo ' Records inserted --> ' . $i . "\n";
		}
	}
*/
}