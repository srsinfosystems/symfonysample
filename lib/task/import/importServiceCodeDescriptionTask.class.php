<?php

class importServiceCodeDescriptionTask extends sfBaseTask
{
	private $client_id = 2;
	private $location_id = 1;

	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'importSCDescription';
		$this->briefDescription = "Imports CSV files to Hype Medical .";

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('fileName', null, sfCommandOption::PARAMETER_REQUIRED, 'SOB File name & path'),
			new sfCommandOption('lineNumber', null, sfCommandOption::PARAMETER_OPTIONAL, 'Line number to begin processing at', 0),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$starting_line = ($options['lineNumber']) ? $options['lineNumber'] : 0;
		$line_number = 0;

		if (!file_exists($options['fileName'])) {
			echo 'file ' . $options['fileName'] . ' does not exist. use --fileName option';
			exit();
		}

		$handle = @fopen($options['fileName'], "r");
		if ($handle) {
			try {
				while (($buffer = fgets($handle, 4096)) !== false) {
					if ($line_number >= $starting_line) {
						$code = $this->processLine($buffer);

						if ($code) {
							echo 'Processed Line ' . $line_number . ': ' . $code . "\n";
						}
					}
					$line_number++;
				}
				if (!feof($handle)) {
					echo "Error: unexpected fgets() fail\n";
				}
			}
			catch (Exception $e) {
				print_r ($e->getMessage());
				echo $e->getTraceAsString();
				echo "unable to complete processing file.\n";
			}
			fclose($handle);
		}
	}
	protected function processLine($line)
	{
		list($code, $description) = explode(';', $line);
		$code = trim($code);
		$description = str_replace(array("�", '�'), '', trim($description));
		$description = str_replace(array('�', '$0.00', '  '), array('ae', "0.00", ' '), $description);
		$description = str_replace(array('�', '�-', '�'), '-', $description);

		if ($description && $description != 'New Code') {
			$service_code = Doctrine_Query::create()
				->from('ServiceCode sc')
				->addWhere('sc.service_code = (?)', $code)
				->addWhere('sc.description != (?)', $description)
				->fetchOne();

			if ($service_code instanceof ServiceCode) {
				$service_code->description = $description;
				$service_code->save();

				return $code;
			}
		}

		return;
	}
}