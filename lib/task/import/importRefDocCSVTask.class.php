<?php

class importRefDocCSVTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'importRefDocCSV';
		$this->briefDescription = 'Imports Referring Doctor CSV formatted file.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('fileName', null, sfCommandOption::PARAMETER_REQUIRED, 'Referring Doctor File name & path'),
			new sfCommandOption('lineNumber', null, sfCommandOption::PARAMETER_OPTIONAL, 'Line number to begin processing at', 0),
			new sfCommandOption('clientID', null, sfCommandOption::PARAMETER_REQUIRED, 'Client ID'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$starting_line = ($options['lineNumber']) ? $options['lineNumber'] : 0;
		$line_number = 0;
		$headers = array();

        var_dump($options);

		if (!file_exists($options['fileName'])) {
			echo 'this file is whack';
			exit();
		}
		if (!$options['clientID']) {
			echo 'no client ID provided';
			exit();
		}
		$this->client_id = $options['clientID'];

		$handle = @fopen($options['fileName'], "rw");
		if ($handle) {
			try {
				while (($buffer = fgets($handle, 4096)) !== false) {
					$buffer = explode(',', trim($buffer));
					if ($line_number == 0) {
						$headers = array_flip($buffer);
					}
					else if ($line_number >= $starting_line) {
						$id = $this->processLine($buffer, $headers);
						echo 'Processed Line ' . $line_number . ': ' . $id . "\n";

					}
					$line_number++;
				}
				if (!feof($handle)) {
					echo "Error: unexpected fgets() fail\n";
				}
			}
			catch (Exception $e) {
				print_r ($e->getMessage());
				echo "unable to complete processing file.\n";
			}
			fclose($handle);
		}
	}
	protected function processLine($columns, $headers)
	{
		if ($columns[$headers['fname']] || $columns[$headers['lname']] || $columns[$headers['provider_num']]) {

			while (strlen($columns[$headers['provider_num']]) < 6)
			{
				$columns[$headers['provider_num']] = '0' . $columns[$headers['provider_num']];
			}

			$referring_doctor = Doctrine_Query::create()
				->from('ReferringDoctor rd')
				->addWhere('rd.client_id = (?)', $this->client_id)
				->addWhere('rd.provider_num = (?)', $columns[$headers['provider_num']])
				->addWhere('rd.fname = (?)', $columns[$headers['fname']])
				->addWhere('rd.lname = (?)', $columns[$headers['lname']])
				->fetchOne();

			if (!$referring_doctor instanceof ReferringDoctor) {
				$referring_doctor = new ReferringDoctor();
				$referring_doctor->client_id = $this->client_id;
// 				$referring_doctor->salutation = 'DR.';
				$referring_doctor->fname = $columns[$headers['fname']];
				$referring_doctor->lname = $columns[$headers['lname']];
				$referring_doctor->provider_num = $columns[$headers['provider_num']];

				$referring_doctor->setHl7IDForPartner($referring_doctor->provider_num, 'MERGERIS');
				$referring_doctor->active = true;

				$referring_doctor->save();
			}

			return $referring_doctor->fname . ' ' . $referring_doctor->lname;
		}
	}
}