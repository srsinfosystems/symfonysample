<?php

class importThirdPartyProviderCSVTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'importThirdPartyProviderCSV';
		$this->briefDescription = 'Imports Third Party Provider CSV formatted file.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('fileName', null, sfCommandOption::PARAMETER_REQUIRED, 'Third Party Provider File name & path'),
			new sfCommandOption('lineNumber', null, sfCommandOption::PARAMETER_OPTIONAL, 'Line number to begin processing at', 0),
			new sfCommandOption('clientID', null, sfCommandOption::PARAMETER_REQUIRED, 'Client ID'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$starting_line = ($options['lineNumber']) ? $options['lineNumber'] : 0;
		$line_number = 0;
		$headers = array();

		if (!file_exists($options['fileName'])) {
			echo 'this file is whack';
			exit();
		}
		if (!$options['clientID']) {
			echo 'no client ID provided';
			exit();
		}
		$this->client_id = $options['clientID'];

		$handle = @fopen($options['fileName'], "rw");
		if ($handle) {
			try {
				while (($buffer = fgets($handle, 4096)) !== false) {
					$buffer = explode(',', trim($buffer));

					if ($line_number == 0) {
						$headers = array_flip($buffer);
					}
					else if ($line_number >= $starting_line) {
						$id = $this->processLine($buffer, $headers);
						echo 'Processed Line ' . $line_number . ': ' . $id . "\n";

					}
					$line_number++;
				}
				if (!feof($handle)) {
					echo "Error: unexpected fgets() fail\n";
				}
			}
			catch (Exception $e) {
				print_r ($e->getMessage());
				echo "unable to complete processing file.\n";
			}
			fclose($handle);
		}
	}
	protected function processLine($columns, $headers)
	{
		if ($columns[$headers['CompanyName']]) {
			$third_party = Doctrine_Query::create()
				->from('ThirdParty tp')
				->addWhere('tp.client_id = (?)', $this->client_id)
				->addWhere('tp.name = (?)', $columns[$headers['CompanyName']])
				->fetchOne();

			if (!$third_party instanceof ThirdParty) {
				$third_party = new ThirdParty();
				$third_party->client_id = $this->client_id;
				$third_party->setName($columns[$headers['CompanyName']]);
				$third_party->city = $columns[$headers['CompanyCity']];
				$third_party->province = $columns[$headers['CompanyState']];
				$third_party->address = $columns[$headers['CompanyStreetAddress']];
				$third_party->postal_code = $columns[$headers['CompanyPostalCode']];
				$third_party->phone = $columns[$headers['CompanyPhone']];

				$third_party->setHl7IDForPartner($third_party->name, 'MERGERIS');
				$third_party->save();
			}

			return $third_party->name;
		}
	}
}