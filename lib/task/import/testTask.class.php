<?php
class testTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'test';
		$this->briefDescription = "Test formatted file.";
	}
	public function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$txt_file = file_get_contents('C:\\public_html\\cvs_imports\\dats\\backup\\22apr bch.dat');
		$rows = explode("\n", $txt_file);
		$i = 0;
		foreach ($rows as $row)
		{
			$i++;
			if (strlen($row)>170) {
				echo ' Line => ' . $i . ' is OK' . "\n";
			}
			else {
				echo ' Line => ' . $i . ' is not OK' . "\n";
			}
		}
		echo ' Total Lines => ' . $i;
	}
}