<?php

class translateReferringDoctorModels2Task extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'updateDataModel';
		$this->name = '2014-09-translateRefDocs2';
		$this->briefDescription = 'Translates Old Referring Doctor models to new format';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
		));
	}

	public function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		// start with claims
		// get lowest ref doc num that doesn't have a ref_doc_id
		// look for that ref doc num in the referring_doctor table (ref doc num and client id)
		// if a match is found update any claims with the proper ref doc id
		// store the lowest value in a database field - i don't remember what table has values like that

		// do the same with fam doc num
		// do the same with patient's ref doc num

		$maxCount = 25;
		$count = 0;

		do {
			$continue = $this->checkClaim();
			$count++;
		} while ($continue && $count <= $maxCount);
		if ($count > $maxCount) {
			exit();
		}

		echo 'patients ref' . "\n";
		do {
			$continue = $this->checkPatientRefDoc();
			$count++;
		} while ($continue && $count <= $maxCount);
		if ($count > $maxCount) {
			exit();
		}

		echo 'patients fam' . "\n";
		do {
			$continue = $this->checkPatientFamDoc();
			$count++;
		} while ($continue && $count <= $maxCount);
	}

	private function checkClaim()
	{
		$lowRefDocNum = $this->getLowestRefDocNum('claim_ref_doc');

		$claim = Doctrine_Query::create()
			->from('Claim c')
			->addWhere('c.ref_doc is not null')
			->addWhere('c.ref_doc != (?)', '')
			->addWhere('c.ref_doc_id is null')
			->addWhere('c.ref_doc > (?)', $lowRefDocNum)
			->orderBy('c.ref_doc')
			->limit(1)
			->fetchOne();

		if ($claim instanceof Claim) {
			echo 'Found un-referenced provider number ' . $claim->ref_doc . "\n";

			// find any referring doctor records that match this ref doc num and
			$referringDoctors = Doctrine_Query::create()
				->from('ReferringDoctor rd')
				->addWhere('rd.provider_num = (?)', $claim->ref_doc)
				->execute();

			if ($referringDoctors->count()) {
				foreach ($referringDoctors as $refDoc)
				{
					echo 'Found matching Referring Doctor record with client ID ' . $refDoc->client_id . ' and ID ' . $refDoc->getID() . "\n";
					$this->updateClaims($refDoc);
					$this->updatePatientsRefDoc($refDoc);
					$this->updatePatientsFamDoc($refDoc);

				}
				echo "\n";
			}

			$lowRefDocNum = $claim->ref_doc;
			$this->setLowestRefDocNum('claim_ref_doc', $lowRefDocNum);

			return true;
		}

		return false;
	}
	private function checkPatientRefDoc()
	{
 		$lowRefDocNum = $this->getLowestRefDocNum('patient_ref_doc');
 		
		$patient = Doctrine_Query::create()
			->from('Patient p')
			->addWhere('p.ref_doc_num is not null')
			->addWhere('p.ref_doc_num != (?)', '')
			->addWhere('p.ref_doc_id is null')
			->addWhere('p.ref_doc_num > (?)', $lowRefDocNum)
			->orderBy('p.ref_doc_num')
			->limit(1)
			->fetchOne();

		if ($patient instanceof Patient) {
			echo $patient->ref_doc_num . "\n";

			// find any referring doctor records that match this ref doc num and
			$referringDoctors = Doctrine_Query::create()
			->from('ReferringDoctor rd')
			->addWhere('rd.provider_num = (?)', $patient->ref_doc_num)
			->execute();

			if ($referringDoctors->count()) {
				foreach ($referringDoctors as $refDoc)
				{
					$this->updateClaims($refDoc);
					$this->updatePatientsRefDoc($refDoc);
					$this->updatePatientsFamDoc($refDoc);

				}
			}

			$lowRefDocNum = $patient->ref_doc_num;
			$this->setLowestRefDocNum('patient_ref_doc', $lowRefDocNum);

			return true;
		}

		return false;
	}
	private function checkPatientFamDoc()
	{
		$lowRefDocNum = $this->getLowestRefDocNum('patient_fam_doc');

		$patient = Doctrine_Query::create()
		->from('Patient p')
		->addWhere('p.fam_doc_num is not null')
		->addWhere('p.fam_doc_num != (?)', '')
		->addWhere('p.fam_doc_id is null')
		->addWhere('p.fam_doc_num > (?)', $lowRefDocNum)
		->orderBy('p.fam_doc_num')
		->limit(1)
		->fetchOne();

		if ($patient instanceof Patient) {
			echo $patient->fam_doc . "\n";

			// find any referring doctor records that match this ref doc num and
			$referringDoctors = Doctrine_Query::create()
			->from('ReferringDoctor rd')
			->addWhere('rd.provider_num = (?)', $patient->fam_doc_num)
			->execute();

			if ($referringDoctors->count()) {
				foreach ($referringDoctors as $refDoc)
				{
					$this->updateClaims($refDoc);
					$this->updatePatientsRefDoc($refDoc);
					$this->updatePatientsFamDoc($refDoc);

				}
			}

			$lowRefDocNum = $patient->fam_doc_num;
			$this->setLowestRefDocNum('patient_fam_doc', $lowRefDocNum);

			return true;
		}

		return false;
	}
	// tableKey will be either claim_ref_doc, patient_ref_doc, or patient_fam_doc
	private function getLowestRefDocNum($tableKey)
	{
		$this->systemParam = Doctrine_Query::create()
			->from('SystemParam sp')
			->addWhere('sp.name = (?)', $tableKey)
			->fetchOne();

		if (!$this->systemParam instanceof SystemParam) {
			return '';
		}
		return $this->systemParam->getValue();
	}
	// tableKey will be either claim_ref_doc, patient_ref_doc, or patient_fam_doc
	private function setLowestRefDocNum($tableKey, $val)
	{
		if (!$this->systemParam instanceof SystemParam) {
			$this->systemParam = new SystemParam();
			$this->systemParam->name = $tableKey;
		}
		$this->systemParam->value = $val;
		$this->systemParam->save();

		return $this->systemParam->value;
	}

	private function updateClaims($referringDoctor)
	{
		do {
			$updated = Doctrine_Query::create()
				->update('Claim c')
				->set('ref_doc_id', '?', $referringDoctor->id)
				->addWhere('c.client_id = (?)', $referringDoctor->client_id)
				->addWhere('c.ref_doc = (?)', $referringDoctor->provider_num)
				->addWhere('c.ref_doc_id is null')
				->limit(100)
				->execute();

			if ($updated) {
				echo 'Updated ' . $updated . ' claims to have new Referring Doctor ID ' . $referringDoctor->id . " for Client ID " . $referringDoctor->client_id . "\n";
			}
		} while ($updated);
	}
	private function updatePatientsRefDoc($referringDoctor)
	{
		do {
			$updated = Doctrine_Query::create()
				->update('Patient p')
				->set('ref_doc_id', '?', $referringDoctor->id)
				->addWhere('p.client_id = (?)', $referringDoctor->client_id)
				->addWhere('p.ref_doc_num = (?)', $referringDoctor->provider_num)
				->addWhere('p.ref_doc_id is null')
				->limit(100)
				->execute();

			if ($updated) {
				echo 'Updated ' . $updated . ' patients to have new Referring Doctor ID ' . $referringDoctor->id . ' for Client ID ' . $referringDoctor->client_id . "\n";
			}
		} while ($updated);
	}
	private function updatePatientsFamDoc($referringDoctor)
	{
		do {
			$updated = Doctrine_Query::create()
			->update('Patient p')
			->set('fam_doc_id', '?', $referringDoctor->id)
			->addWhere('p.client_id = (?)', $referringDoctor->client_id)
			->addWhere('p.ref_doc_num = (?)', $referringDoctor->provider_num)
			->addWhere('p.fam_doc_id is null')
			->limit(100)
			->execute();

			if ($updated) {
				echo 'Updated ' . $updated . ' patients to have new Family Doctor ID ' . $referringDoctor->id . ' for Client ID ' . $referringDoctor->client_id . "\n";
			}
		} while ($updated);

	}
}