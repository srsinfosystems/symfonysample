<?php

class didykSetReferringDoctorFromPatientsTask extends sfBaseTask
{
	// Hard coded client ID 31 because this is to fix an import.
	private $clientID = 31;
	private $clientName = 'DIDYK';
	private $systemParamKey = 'didyk_lowest_patient_id';
	private $systemParamKey2 = 'didyk_lowest_patient_id3';

	protected function configure()
	{
		$this->namespace = 'updateDataModel';
		$this->name = '2014-09-didykSetClaimDataFromPatient';
		$this->briefDescription = 'Adds in new data from the patient record to the claim';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
		));
	}
	public function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$maxCount = 500;
		$count = 0;

		$client = Doctrine_Query::create()
			->from('Client c')
			->addWhere('c.id = (?)', $this->clientID)
			->addWhere('c.name = (?)', $this->clientName)
			->fetchOne();

		if (!$client instanceof Client) {
			echo 'Wrong database or client ID - Client ID ' . $this->clientID . ' does not have the name  "' . $this->clientName . '"' . "\n";
			exit();
		}

		do {
			$continue = $this->findPatient();
			$count++;
		} while ($continue && $count <= $maxCount);

		do {
			$continue = $this->findPatient2();
			$count++;
		} while ($continue && $count <= $maxCount);

	}

	private function findPatient()
	{
		$lowestPatientID = $this->getLowestPatientID($this->systemParamKey);

		$patient = Doctrine_Query::create()
			->from('Patient p')
			->addWhere('p.client_id = (?)', $this->clientID)
			->addWhere('p.id > (?)', $lowestPatientID)
			->orderBy('p.id asc')
			->limit(1)
			->fetchOne();

		if ($patient instanceof Patient) {
			echo 'Updating Claims for Patient ID ' . $patient->id . ' [' . $patient->lname . ']' . "\n";
			$this->updateClaims($patient);

			$this->setLowestPatientID($patient->id, $this->systemParamKey);
			return true;
		}

		return false;
	}
	private function findPatient2()
	{
		$lowestPatientID = $this->getLowestPatientID($this->systemParamKey2);

		$patient = Doctrine_Query::create()
			->from('Patient p')
			->addWhere('p.client_id = (?)', $this->clientID)
			->addWhere('p.id > (?)', $lowestPatientID)
			->orderBy('p.id asc')
			->limit(1)
			->fetchOne();

		if ($patient instanceof Patient) {
			echo 'Step 2 - Updating Claims for Patient ID ' . $patient->id . ' [' . $patient->lname . ']' . "\n";
			$this->updateClaims2($patient);

			$this->setLowestPatientID($patient->id, $this->systemParamKey2);
			return true;
		}

		return false;
	}
	private function updateClaims($patient)
	{
		do {
 			$claims = Doctrine_Query::create()
 				->from('Claim c')
 				->addWhere('c.client_id = (?)', $this->clientID)
 				->addWhere('c.patient_id = (?)', $patient->id)
  				->addWhere('(c.dob != (?) OR c.sex != (?) OR c.ref_doc != (?))', array($patient->dob, $patient->sex, $patient->ref_doc_num))
  				->addWhere('c.updated_at < (?)', date('Y-m-d 00:00:00'))
 				->limit(10)
 				->execute();

  			foreach ($claims as $claim)
  			{
  				echo '  Updating claim ID ' . $claim->id . "\n";
  				if (!$claim->dob) {
  					$claim->dob = $patient->dob;
  				}

  				if (!$claim->sex) {
  					$claim->sex = $patient->sex;
  				}

  				if (!$claim->ref_doc) {
  					$claim->ref_doc = $patient->ref_doc_num;
  				}
  				$claim->save();
  			}

		} while (count($claims));
	}
	private function updateClaims2($patient)
	{
		$lowestID = 0;

		do {
 			$claims = Doctrine_Query::create()
 				->from('Claim c')
 				->addWhere('c.client_id = (?)', $this->clientID)
 				->addWhere('c.patient_id = (?)', $patient->id)
  				->addWhere('(c.admit_date != (?) OR c.facility_num != (?))', array($patient->admit_date, $patient->facility_num))
  				->addWhere('c.id > (?)', $lowestID)
 				->limit(10)
 				->orderBy('c.id asc')
 				->execute();

  			foreach ($claims as $claim)
  			{
  				echo '  Updating claim ID ' . $claim->id . "\n";

  				$lowestID = $claim->id;
  				$saveClaim = false;

  				if (!$claim->facility_num && $patient->facility_num) {
  					$claim->facility_num = $patient->facility_num;
  					$saveClaim = true;
  				}

  				if (!$claim->admit_date && $patient->facility_num) {
  					$claim->admit_date = $patient->admit_date;
  					$saveClaim = true;
  				}

  				if ($saveClaim) {
  					$claim->save();
  				}
  			}

		} while (count($claims));
	}
	private function getLowestPatientID($key)
	{
		$this->systemParam = Doctrine_Query::create()
		->from('SystemParam sp')
		->addWhere('sp.name = (?)', $key)
		->fetchOne();

		if (!$this->systemParam instanceof SystemParam) {
			return 0;
		}
		return $this->systemParam->getValue();
	}
	private function setLowestPatientID($val, $key)
	{
		if (!$this->systemParam instanceof SystemParam) {
			$this->systemParam = new SystemParam();
			$this->systemParam->name = $key;
		}
		$this->systemParam->value = $val;
		$this->systemParam->save();

		return $this->systemParam->value;
	}
}