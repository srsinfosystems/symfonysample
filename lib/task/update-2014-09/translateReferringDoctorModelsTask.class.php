<?php

class translateReferringDoctorModelsTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'updateDataModel';
		$this->name = '2014-09-translateRefDocs';
		$this->briefDescription = 'Translates Old Referring Doctor models to new format';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
		));
	}

	public function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$maxCount = 250;
		$count = 0;

		do {
			$continue = $this->processOneReferringDoctor();
			$count++;
			echo "\n";
		} while ($continue && $count <= $maxCount);

	}

	private function processOneReferringDoctor()
	{
		$refDocOld = Doctrine_Query::create()
			->from('ReferringDoctorOld r')
			->leftJoin('r.ReferringDoctorAddressOld rda')
			->addWhere('rda.new_id is null')
			->addWhere('r.provider_num != (?)', '')
			->orderBy('r.id asc, rda.city desc')
			->limit(1)
			->fetchOne();

		if ($refDocOld instanceof ReferringDoctorOld) {
			foreach ($refDocOld->ReferringDoctorAddressOld as $refDocAddrOld)
			{
				try {
					$con = Doctrine_Manager::getInstance()->getConnectionForComponent('ReferringDoctor');
					$con->beginTransaction();

					$newRefDoc = $this->translateRefDocAddrOld($refDocAddrOld);
					$newRefDoc = $this->findDuplicateNewRefDoc($newRefDoc);
					$newRefDoc->save();

					$refDocAddrOld->new_id = $newRefDoc->getId();
					$refDocAddrOld->save();

					$refDocOld->new_id = $newRefDoc->getId();
					$refDocOld->save();

					echo "Translated old Referring Doctor ID " . $refDocAddrOld->getId() . " to new ID " . $newRefDoc->getId() . "\n";

					do {
						$updated = Doctrine_Query::create()
							->update('Claim c')
							->set('ref_doc_id', '?', $refDocAddrOld->new_id)
							->addWhere('c.client_id = (?)', $refDocOld->client_id)
							->addWhere('c.old_ref_doc_addr_id = (?)', $refDocAddrOld->id)
							->addWhere('c.ref_doc_id is null')
							->limit(100)
							->execute();

						if ($updated) {
							echo 'Updated ' . $updated . ' claims to have new Referring Doctor ID ' . $refDocAddrOld->new_id . "\n";
						}
					} while ($updated);

					do {
						$updated = Doctrine_Query::create()
							->update('Patient p')
							->set('ref_doc_id', '?', $refDocAddrOld->new_id)
							->addWhere('p.client_id = (?)', $refDocOld->client_id)
							->addWhere('p.old_ref_doc_refdocaddr_id = (?)', $refDocAddrOld->id)
							->addWhere('p.ref_doc_id is null')
							->limit(100)
							->execute();

						if ($updated) {
							echo 'Updated ' . $updated . ' patients to have new Referring Doctor ID ' . $refDocAddrOld->new_id . "\n";
						}
					} while ($updated);

					do {
						$updated = Doctrine_Query::create()
							->update('Patient p')
							->set('fam_doc_id', '?', $refDocAddrOld->new_id)
							->addWhere('p.client_id = (?)', $refDocOld->client_id)
							->addWhere('p.old_fam_doc_refdocaddr_id = (?)', $refDocAddrOld->id)
							->addWhere('p.fam_doc_id is null')
							->limit(100)
							->execute();

						if ($updated) {
							echo 'Updated ' . $updated . ' patients to have new Family Doctor ID ' . $refDocAddrOld->new_id . "\n";
						}
					} while ($updated);

					$newRefDoc = null;

					$con->commit();
				}
				catch (Exception $e) {
					$con->rollback();
					throw $e;
				}
			}

			return true;
		}

		return false;
	}
	private function translateRefDocAddrOld($refDocAddrOld)
	{
		$refDoc = new ReferringDoctor();
		$refDoc->setClientId($refDocAddrOld->ReferringDoctorOld->getClientID());
		$refDoc->setSalutation($refDocAddrOld->ReferringDoctorOld->getSalutation());
		$refDoc->setFname($refDocAddrOld->ReferringDoctorOld->getFname());
		$refDoc->setLname($refDocAddrOld->ReferringDoctorOld->getLname());
		$refDoc->setProviderNum($refDocAddrOld->ReferringDoctorOld->getProviderNum());
		$refDoc->setSpecCodeId($refDocAddrOld->ReferringDoctorOld->getSpecCodeId());
		$refDoc->spec_code = $refDocAddrOld->ReferringDoctorOld->spec_code;
		$refDoc->setAddress($refDocAddrOld->getAddress());
		$refDoc->setAddress2($refDocAddrOld->getAddress2());
		$refDoc->setCity($refDocAddrOld->getCity());
		$refDoc->setProvince($refDocAddrOld->getProvince());
		$refDoc->setPostalCode($refDocAddrOld->getPostalCode());
		$refDoc->setFax($refDocAddrOld->getFax());
		$refDoc->setPhone($refDocAddrOld->getPhone());
		$refDoc->setEmail($refDocAddrOld->getEmail());
		$refDoc->setActive($refDocAddrOld->getActive());

		return $refDoc;
	}
	private function findDuplicateNewRefDoc($refDoc)
	{
		$q = Doctrine_Query::create()
			->from('ReferringDoctor rd')
			->addWhere('rd.provider_num = (?)', $refDoc->getProviderNum())
			->addWhere('rd.client_id = (?)', $refDoc->getClientID());

		if ($refDoc->fname) {
			$q->addWhere('rd.fname = (?)', $refDoc->fname);
		}
		if ($refDoc->lname) {
			$q->addWhere('rd.lname = (?)', $refDoc->lname);
		}
		if ($refDoc->spec_code_id) {
			$q->addWhere('rd.spec_code_id = (?)', $refDoc->spec_code_id);
		}
		if ($refDoc->address) {
			$q->addWhere('rd.address = (?)', $refDoc->address);
		}
		if ($refDoc->address2) {
			$q->addWhere('rd.address2 = (?)', $refDoc->address2);
		}
		if ($refDoc->city) {
			$q->addWhere('rd.city = (?)', $refDoc->city);
		}
		if ($refDoc->province) {
			$q->addWhere('rd.province = (?)', $refDoc->province);
		}
		if ($refDoc->postal_code) {
			$q->addWhere('rd.postal_code = (?)', $refDoc->postal_code);
		}
		if ($refDoc->fax) {
			$q->addWhere('rd.fax = (?)', $refDoc->fax);
		}
		if ($refDoc->phone) {
			$q->addWhere('rd.phone = (?)', $refDoc->phone);
		}
		if ($refDoc->email) {
			$q->addWhere('rd.email = (?)', $refDoc->email);
		}

		$dup = $q->fetchOne();
		if ($dup instanceof ReferringDoctor) {
			$refDoc = $dup;
		}
		return $refDoc;
	}
}