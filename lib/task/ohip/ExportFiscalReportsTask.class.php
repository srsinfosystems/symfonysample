<?php

class ExportFiscalReportsTask extends sfBaseTask
{
	private $exportFolder = "C:\\Users\\Shannon\\Desktop\\FiscalReports\\";

	/**
	 * HOW TO RUN THIS COMMAND:
	 * symfony ohip:exportFiscalReports
	 */
	protected function configure()
	{
		$this->namespace = 'ohip';
		$this->name = 'exportFiscalReports';
		$this->briefDescription = 'Exports fiscal reports from RA Reports tables';

		$this->addOptions(array(
				new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
				new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}

	private function executeTask($arguments, $options)
	{
 		$databaseManager = new sfDatabaseManager($this->configuration);
 		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

 		$minID = 0;
 		do {
 			$raMessage = Doctrine_Query::create()
 				->from('RaMessage rm')
 				->leftJoin('rm.RaFile rf')
 				->addWhere('rm.message LIKE (?)', 'FISCAL YEAR%')
 				->addWhere('rm.ra_file_id > (?)', $minID)
 				->orderBy('rm.ra_file_id asc')
 				->limit(1)
 				->fetchOne();

 			if ($raMessage instanceof RaMessage) {
 				$minID = $raMessage->getRaFileID();

 				$fiscalYear = $this->getFiscalYear($raMessage->getMessage());
 				$providerNum = $raMessage->RaFile->getProviderNum();

 				$fileName = $this->exportFolder . $providerNum . ' --- ' . $fiscalYear;
 				file_put_contents($fileName . '.txt', $raMessage->getMessage());
 			}

 		} while ($raMessage instanceof RaMessage);
	}

	private function getFiscalYear($txt)
	{
		$pos1 = strpos($txt, "APRIL");
		$pos2 = strpos($txt, "\n");

		$fiscalYear = substr($txt, $pos1, $pos2 - $pos1);
		$fiscalYear = str_replace('APRIL 1, ', '', $fiscalYear);
		$fiscalYear = str_replace('MARCH 31, ', '', $fiscalYear);
		$fiscalYear = str_replace(' - ', '-', $fiscalYear);

		return $fiscalYear;
	}
}