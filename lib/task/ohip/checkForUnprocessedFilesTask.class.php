<?php

class checkForUnprocessedFilesTask extends sfBaseTask
{
 	private $messageLog = '';
 	private $debug = false;
 	private $sendEmail = true;
	private $emailAddress = array(
		'shannon@hypesystems.com' => 'Shannon Patterson',
// 		'support@hypesystems.com' => 'Hype Support'
	); 

	/**
	 * HOW TO RUN THIS COMMAND:
	 * symfony ohip:checkForUnprocessedFiles
	 */
	protected function configure()
	{
		$this->namespace = 'ohip';
		$this->name = 'checkForUnprocessedFiles';
		$this->briefDescription = 'Sets the item_count value for unset claims';

		$this->addOptions(array(
				new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
				new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
 		$databaseManager = new sfDatabaseManager($this->configuration);
 		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

 			$reconciles = Doctrine_Query::create()
				->from('Reconcile r')
				->addWhere('r.status = (?)', ReconcileTable::FILE_STATUS_PROCESSING)
				->addWhere('r.date_received >= (?)', date('Y-m-01 00:00:00', strtotime('six months ago')))
 				->orderBy('r.id desc')
 				->limit(100)
				->execute();

 			if ($reconciles->count()) {
 				$reconcileFiles = array();

 				foreach ($reconciles as $reconcile)
 				{
					$reconcileFiles[] = $reconcile->getFileName() . ' - Received ' . $reconcile->getDateReceived();
 				}
 			}

 			$this->sendEmail($reconcileFiles);
 	}

 	private function getMessageText($reconcileFiles)
 	{
 		$rs = '';

 		$rs .= 'The following files are in a PROCESSING status. They probably got stuck.' . "\n\n\n";

 		foreach ($reconcileFiles as $reconcileFile)
 		{
 			$rs .= $reconcileFile . "\n";
 		}

 		return $rs;
 	}
 	private function sendEmail($reconcileFiles)
 	{
 		if (!class_exists('Swift_SmtpTransport')) {
 			require_once (dirname(__FILE__) . '../../../vendor/Swift-5.0.3/lib/swift_required.php');
 		}

 		$username = 'support@hypesystems.com';
 		$password = '3Hw1Kfj8';
 		$smtp = 'mail.hypesystems.com';
 		$port = 25;
 		$from = array('support@hypesystems.com' => 'Hype Support');
 		$to = $this->emailAddress;
 		$subject = 'Unprocessed Reconcile Files Task Report';

 		$transport = Swift_SmtpTransport::newInstance($smtp, $port)
 			->setUsername($username)
 			->setPassword($password);
 		$mailer = Swift_Mailer::newInstance($transport);

 		$message = Swift_Message::newInstance()
 			->setSubject($subject)
 			->setFrom($from)
 			->setTo($to)
 			->setBody($this->getMessageText($reconcileFiles));

 		$result = $mailer->send($message);
 	}
}