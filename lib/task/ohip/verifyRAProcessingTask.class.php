<?php

class verifyRAProcessingTask extends sfBaseTask
{
	private $systemParam = null;
	private $messageLog = '';
	private $debug = false;
	private $sendEmail = false;
	private $emailAddress = array(
		'scott@hypesystems.com' => 'Scott Gilmour',
// 		'support@hypesystems.com' => 'Hype Support'
	);
	private $sqlQueries = array();
 
	/**
	 * HOW TO RUN THIS COMMAND:
	 * symfony ohip:verifyRA[ --reconcileID=xx][ --debug=true][ --sendEmail=true]
	 */
	protected function configure()
	{
		$this->namespace = 'ohip';
		$this->name = 'verifyRA';
		$this->briefDescription = 'Sets the item_count value for unset claims';

		$this->addOptions(array(
				new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
				new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
				new sfCommandOption('reconcileID', null, sfCommandOption::PARAMETER_REQUIRED, 'Reconcile ID', null),
				new sfCommandOption('debug', null, sfCommandOption::PARAMETER_REQUIRED, 'Turn on debugging', false),
				new sfCommandOption('sendEmail', null, sfCommandOption::PARAMETER_REQUIRED, 'Send email results to ' . implode(', ', array_keys($this->emailAddress)), false),
				new sfCommandOption('resetReconcileID', null, sfCommandOption::PARAMETER_REQUIRED, 'Reset counter back to zero (go back to beginning)', false),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$this->debug = filter_var($options['debug'], FILTER_VALIDATE_BOOLEAN);
		$this->sendEmail = filter_var($options['sendEmail'], FILTER_VALIDATE_BOOLEAN);
		$resetID = filter_var($options['resetReconcileID'], FILTER_VALIDATE_BOOLEAN);

		$reconcileID = $options['reconcileID'];
		$doOnce = $reconcileID ? true : false;
		$limit = 100;

		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		if (!$reconcileID) {
			$reconcileID = $this->getReconcileTestID();
		}
		if ($resetID) {
			$reconcileID = 1;
		}

		$count = 1;
		do {
			$reconcile = Doctrine_Query::create()
				->from('Reconcile r')
				->addWhere('r.id >= (?)', $reconcileID)
				->addWhere('r.status = (?)', ReconcileTable::FILE_STATUS_PROCESSED)
				->addWhere('r.type = (?)', ReconcileTable::FILE_TYPE_REMITTANCE_ADVICE)
				->orderBy('r.id asc')
				->limit(1)
				->fetchOne();

			if ($reconcile instanceof Reconcile) {
				$this->logMessage(date(hype::DB_ISO_DATE) . ' ' . $_SERVER['COMPUTERNAME']);
				$this->logMessage('Perform RA Process Check - verify ReconcileClient and ReconcileDoctor are as they should be.');
				$this->logMessage('============================================================================================');

				$problemFound = $this->verifyRADoctorClientMappings($reconcile);

				if ($problemFound || $this->sendEmail) {
					$this->sendReportEmail($reconcile->file_name);
				}

				if (!$doOnce) {
					$reconcileID = $reconcile->getID();
					$reconcileID = $this->setReconcileTestID($reconcileID);
					$count++;
					$this->messageLog = '';
					$this->sqlQueries = array();
				}
			}
		} while ($count <= $limit && !$doOnce && $reconcile instanceof Reconcile && !$problemFound);
	}

	private function getReconcileTestID()
	{
		$this->systemParam = Doctrine_Query::create()
		->from('SystemParam sp')
		->addWhere('sp.name = (?)', 'ra_doctor_test_id')
		->fetchOne();

		if (!$this->systemParam instanceof SystemParam) {
			return 1;
		}
		return $this->systemParam->getValue();
	}
	private function setReconcileTestID($v)
	{
		if (!$this->systemParam instanceof SystemParam) {
			$this->systemParam = new SystemParam();
			$this->systemParam->name = 'ra_doctor_test_id';
		}
		$this->systemParam->value = $v + 1;
		$this->systemParam->save();

		return $this->systemParam->value;
	}
	private function verifyRADoctorClientMappings($reconcile)
	{
		$problemFound = false;
		$this->logMessage('Reconcile File to check: ' . $reconcile->getFileName() . ' [ID: ' . $reconcile->getID() . ']');

		$missedClientIDs = array();
		$missedDoctorIDs = array();

		// collect client IDs for RA ID
		$clientIDs = array();
		foreach ($reconcile->ReconcileClient as $rc)
		{
			$clientIDs[$rc['client_id']] = false;
		}
		$this->logMessage('Reconcile Clients in database: ' . implode(', ', array_keys($clientIDs)));

		// collect doctor IDs for RA ID
		$doctorIDs = array();
		foreach ($reconcile->ReconcileDoctor as $rd)
		{
			$doctorIDs[$rd['doctor_id']] = false;
		}
		$this->logMessage('Reconcile Doctors in database: ' . implode(', ', array_keys($doctorIDs)));

		// get RAFile ID
		$raFileID = $reconcile->RaFile[0]->getID();

		$this->logMessage('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=');
		$this->logMessage('Get Distinct doctor information from RA Items table');

		// get RAItems' distinct provider number, group number
		$rs = Doctrine_Query::create()
			->select('DISTINCT ra.provider_num, ra.group_num')
			->from('RaItem ra')
			->addWhere('ra.ra_file_id = (?)', $raFileID)
			->execute(array(), Doctrine::HYDRATE_SCALAR);

		// for each doctor & group number, get any records in the database
		// mark those client IDs and doctor IDs from their respective arrays
		foreach ($rs as $doctorInfo)
		{
			$this->logMessage('   Provider Number: ' . $doctorInfo['ra_provider_num'] . ', Group Number: ' . $doctorInfo['ra_group_num']);

			$doctors = Doctrine_Query::create()
				->from('Doctor d')
				->addWhere('d.provider_num = (?)', $doctorInfo['ra_provider_num'])
				->addWhere('d.group_num = (?)', $doctorInfo['ra_group_num'])
				->addWhere('d.active = (?)', true)
				->execute(array(), Doctrine::HYDRATE_ARRAY);

			foreach ($doctors as $doctor)
			{
				if (array_key_exists($doctor['client_id'], $clientIDs)) {
					if (!$clientIDs[$doctor['client_id']]) {
						$this->logMessage('      Found first instance of Client ID ' . $doctor['client_id'] . '.  Marking as found');
						$clientIDs[$doctor['client_id']] = true;
					}
				}
				else if (!array_key_exists($doctor['client_id'], $missedClientIDs)) {
					$this->logMessage('      Client ID ' . $doctor['client_id'] . ' is missing from ReconcileClient.  Marking as missing');
					$missedClientIDs[$doctor['client_id']] = $doctor['client_id'];
				}


				if (array_key_exists($doctor['id'], $doctorIDs)) {
					if (!$doctorIDs[$doctor['id']]) {
						$this->logMessage('      Found first instance of Doctor ID ' . $doctor['id'] . '.  Marking as found');
						$doctorIDs[$doctor['id']] = true;
					}
				}
				else if ($doctor['primary_profile'] && !array_key_exists($doctor['id'], $missedDoctorIDs)) {
					$this->logMessage('      Doctor ID ' . $doctor['id'] . ' is missing from ReconcileDoctor.  Marking as missing');
					$missedDoctorIDs[$doctor['id']] = $doctor['id'];
				}
			}
		}

		$this->logMessage('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=');
		$this->logMessage('Get Distinct doctor information from matched Claims');

		// get matched Claim's distinct Doctor IDs and Client IDs
		$rs = Doctrine_Query::create()
			->select('DISTINCT c.client_id, c.doctor_id')
			->from('Claim c')
			->leftJoin('c.RaItem ra')
			->addWhere('ra.ra_file_id = (?)', $raFileID)
			->execute(array(), Doctrine::HYDRATE_SCALAR);

		foreach ($rs as $claimInfo)
		{
			$this->logMessage('   Claim Client ID: ' . $claimInfo['c_client_id'] . ', Doctor ID: ' . $claimInfo['c_doctor_id']);

			if (array_key_exists($claimInfo['c_client_id'], $clientIDs)) {
				if (!$clientIDs[$claimInfo['c_client_id']]) {
					$this->logMessage('      Found first instance of Client ID ' . $claimInfo['c_client_id'] . '.  Marking as found');
					$clientIDs[$claimInfo['c_client_id']] = true;
				}
			}
			else if (!array_key_exists($claimInfo['c_client_id'], $missedClientIDs)) {
				$this->logMessage('      Client ID ' . $claimInfo['c_client_id'] . ' is missing from ReconcileClient.  Marking as missing');
				$missedClientIDs[$claimInfo['c_client_id']] = $claimInfo['c_client_id'];
			}

 			if (array_key_exists($claimInfo['c_doctor_id'], $doctorIDs)) {
 				if (!$doctorIDs[$claimInfo['c_doctor_id']]) {
 					$this->logMessage('      Found first instance of Doctor ID ' . $claimInfo['c_doctor_id'] . '.  Marking as found');
 					$doctorIDs[$doctor['id']] = true;
 				}
 			}
 			else if (!array_key_exists($claimInfo['c_doctor_id'], $missedClientIDs)) {
 				$this->logMessage('      Doctor ID ' . $claimInfo['c_doctor_id'] . ' is missing from ReconcileDoctor.  Marking as missing');
				$missedDoctorIDs[$claimInfo['c_doctor_id']] = $claimInfo['c_doctor_id'];
 			}
		}

		$this->logMessage('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=');
		$this->logMessage('Client IDs in ReconcileClient:');
		foreach ($clientIDs as $key => $found)
		{
			$this->logMessage('   ' . $key . ' -> ' . ($found ? 'found' : 'not found'));
			if ($found) {
				unset($clientIDs[$key]);
			}
			else {
				$this->sqlQueries[] = 'DELETE FROM reconcile_client WHERE reconcile_id = ' . $reconcile->getID() . ' and client_id = ' . $key;
				$problemFound = true;
			}
		}

		$this->logMessage('Client IDs missing from ReconcileClient:');
		foreach ($missedClientIDs as $clientID)
		{
			$problemFound = true;
			$this->logMessage('   ' . $clientID);
			$this->sqlQueries[] = 'INSERT INTO reconcile_client (reconcile_id, client_id) values (' . $reconcile->getID() . ', ' . $clientID . ')';
		}
		$this->logMessage('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=');
		$this->logMessage('Doctor IDs in ReconcileDoctor:');
		foreach ($doctorIDs as $key => $found)
		{
			$this->logMessage('   ' . $key . ' -> ' . ($found ? 'found' : 'not found'));
			if ($found) {
				unset($doctorIDs[$key]);
			}
			else {
				$problemFound = true;
				$this->sqlQueries[] = 'DELETE FROM reconcile_doctor WHERE reconcile_id = ' . $reconcile->getID() . ' and doctor_id = ' . $key;
			}
		}

		$this->logMessage('Doctor IDs missing from ReconcileDoctor:');
		foreach ($missedDoctorIDs as $doctorID)
		{
			$problemFound = true;
			$this->logMessage('   ' . $doctorID);
			$this->sqlQueries[] = 'INSERT INTO reconcile_doctor (reconcile_id, doctor_id) values (' . $reconcile->getID() . ', ' . $doctorID . ')';
		}

		$this->logMessage('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=');
		if ($problemFound) {
			$this->logMessage('Problem Found. SQL queries to resolve these issues: ');

			foreach ($this->sqlQueries as $query)
			{
				$this->logMessage('   ' . $query);
			}

			$this->logMessage('Emailing report to ' . implode(', ', array_keys($this->emailAddress)));
		}

		return $problemFound;
	}

	private function logMessage($msg)
	{
		$this->messageLog .= $msg . "\n";
		if ($this->debug) {
			echo $msg . "\n";
		}
	}
	private function sendReportEmail($filename)
	{
		if (!class_exists('Swift_SmtpTransport')) {
			require_once (dirname(__FILE__) . '../../../vendor/Swift-5.0.3/lib/swift_required.php');
		}

		$username = 'support@hypesystems.com';
		$password = '0w3n4l0n';
		$smtp = 'mail.hypesystems.com';
		$port = 587;
		$from = array('support@hypesystems.com' => 'Hype Support');
		$to = $this->emailAddress;
		$subject = 'RA Validation Test Report ' . $filename;

		$transport = Swift_SmtpTransport::newInstance($smtp, $port)
			->setUsername($username)
			->setPassword($password);
		$mailer = Swift_Mailer::newInstance($transport);

		$message = Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom($from)
			->setTo($to)
			->setBody($this->messageLog);
		;

		$result = $mailer->send($message);
	}
}