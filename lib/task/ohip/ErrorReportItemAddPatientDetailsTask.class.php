<?php

class ErrorReportItemAddPatientDetailsTask extends sfBaseTask
{
	/**
	 * HOW TO RUN THIS COMMAND:
	 * symfony ohip:errorReportAddPatients
	 */
	protected function configure()
	{
		$this->namespace = 'ohip';
		$this->name = 'errorReportAddPatients';
		$this->briefDescription = 'Sets the patient details for error report items';

		$this->addOptions(array(
				new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
				new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}

	private function executeTask($arguments, $options)
	{
 		$limit = 5000;

 		$databaseManager = new sfDatabaseManager($this->configuration);
 		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

 		$count = 1;
 		do {
 			$errorReportItem = Doctrine_Query::create()
 				->from('ErrorReportItem eri')
 				->leftJoin('eri.Claim c')
 				->addWhere('eri.claim_id is not null')
 				->addWhere('eri.patient_id is null')
 				->addWhere('eri.client_id is null')
 				->addWhere('eri.fname is null')
 				->addWhere('eri.lname is null')
 				->addWhere('eri.status = (?)', ErrorReportItemTable::STATUS_PARSED)
 				->limit(1)
 				->fetchOne();

 			if ($errorReportItem instanceof ErrorReportItem)
 			{
 				$errorReportItem->client_id = $errorReportItem->Claim->client_id;
 				$errorReportItem->patient_id = $errorReportItem->Claim->patient_id;
 				$errorReportItem->fname = $errorReportItem->Claim->fname;
 				$errorReportItem->lname = $errorReportItem->Claim->lname;
 				$errorReportItem->save();

 				echo $count . ': ERI ' . $errorReportItem->getId() . ' - ' . $errorReportItem->fname . ' ' . $errorReportItem->lname . "\n";
 				$count++;
 			}
 		} while ($count <= $limit && $errorReportItem instanceof ErrorReportItem);
	}
}