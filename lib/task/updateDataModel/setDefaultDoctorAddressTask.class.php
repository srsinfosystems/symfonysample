<?php

class setDefaultDoctorAddressTask extends sfBaseTask
{
	/**
	 * HOW TO RUN THIS COMMAND:
	 * symfony hype:setDefaultDoctorAddress
	 */
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'setDefaultDoctorAddress';
		$this->briefDescription = 'Sets the address_text field for doctors without a value set';

		$this->addOptions(array(
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		do {
			$doctor = Doctrine_Query::create()
				->from('Doctor d')
				->addWhere('d.address_text is null')
				->fetchOne();

			if ($doctor instanceof Doctor) {
				$doctor->generateAddressText();
				echo $doctor->getAddressText() . "\n\n";
				$doctor->save();
			}

		} while ($doctor instanceof Doctor);

		echo "Done!\n\n";
	}
}