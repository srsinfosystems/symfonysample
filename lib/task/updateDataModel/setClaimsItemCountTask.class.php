<?php

class setClaimsItemCountTask extends sfBaseTask
{
	/**
	 * HOW TO RUN THIS COMMAND:
	 * symfony hype:setClaimsItemCount
	 */
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'setClaimsItemCount';
		$this->briefDescription = 'Sets the item_count value for unset claims';

		$this->addOptions(array(
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$totalCount = 0;
		$maxCount = 10000;
		$limit = 100;

		do {
			$claimIDs = Doctrine_Query::create()
				->select('c.id')
				->from('Claim c')
				->addWhere('c.item_count is null')
				->limit($limit)
				->execute(array(), Doctrine::HYDRATE_ARRAY);

			foreach ($claimIDs as $key => $value)
			{
				$claimIDs[$key] = $value['id'];
			}

			foreach ($claimIDs as $claimID)
			{
				$count = Doctrine_Query::create()
					->from('ClaimItem ci')
					->addWhere('ci.claim_id = (?)', $claimID)
					->addWhere('ci.status != (?)', ClaimItemTable::STATUS_DELETED)
					->count();

				$x = Doctrine_Query::create()
					->update('Claim c')
					->addWhere('c.id = (?)', $claimID)
					->set('c.item_count', '?', $count)
					->execute();

 				$totalCount++;
			}

 			echo date(hype::DB_ISO_DATE) . ' -- Updated ' . $limit . ' claims.' . "\n";
		} while (count($claimIDs) && $totalCount < $maxCount);

		if ($totalCount == $maxCount) {
			echo 'Reached ' . $maxCount . ' (Maximum execution number).  Run again!' . "\n";
		}
		else {
			echo "Done!\n\n";
		}

		exit();
	}
}