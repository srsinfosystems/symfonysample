<?php

class setErrorReportItemCountTask extends sfBaseTask
{
	/**
	 * HOW TO RUN THIS COMMAND:
	 * symfony hype:setErrorReportItemCount
	 */
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'setErrorReportItemCount';
		$this->briefDescription = 'Sets the error_count field for reconciles without a value set';

		$this->addOptions(array(
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$minID = 0;
		$totalLimit = 10000;

		do {
			$reconcile = Doctrine_Query::create()
				->from('Reconcile r')
				->addWhere('r.error_count = (?)', 0)
				->addWhere('r.type = (?)', ReconcileTable::FILE_TYPE_ERROR_REPORT)
				->addWhere('r.id > (?)', $minID)
				->addWhere('r.status = (?)', ReconcileTable::FILE_STATUS_PROCESSED)
				->orderBy('r.id')
				->limit(1)
				->fetchOne();

			if ($reconcile instanceof Reconcile) {
				$count = Doctrine_Query::create()
					->from('ErrorReportItem eri')
					->addWhere('eri.reconcile_id = (?)', $reconcile->getID())
					->addWhere('eri.status = (?)', ErrorReportItemTable::STATUS_PARSED)
					->count();

				$reconcile->setErrorCount($count);
				$reconcile->save();

				echo $reconcile->getID() . ': ' . $count . "\n";
				$minID = $reconcile->getID();
			}
			$totalLimit--;
		} while ($reconcile instanceof Reconcile && $totalLimit);

		echo "Done!\n\n";
	}
}