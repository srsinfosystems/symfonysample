<?php

class setErrorReconcileIdTask extends sfBaseTask
{
	/**
	 * HOW TO RUN THIS COMMAND:
	 * symfony hype:setErrorReconcileId
	 */
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'setErrorReconcileId';
		$this->briefDescription = 'Sets the error_reconcile_id field for rejected items without a value set';

		$this->addOptions(array(
		new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
		new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
		new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$minID = 0;
		$totalLimit = 100;

		do {
			$claimItem = Doctrine_Query::create()
				->from('ClaimItem ci')
				->leftJoin('ci.ErrorReportItem eri')
				->addWhere('ci.status = (?)', ClaimItemTable::STATUS_REJECTED_OPEN)
				->addWhere('ci.id > (?)', $minID)
				->addWhere('eri.item_id is not null')
				->addWhere('eri.status = (?)', ErrorReportItemTable::STATUS_PARSED)
				->addWhere('ci.error_reconcile_id is null')
				->orderBy('ci.id asc, eri.id desc')
				->limit(1)
				->fetchOne();

			if ($claimItem instanceof ClaimItem) {
				if (count($claimItem->ErrorReportItem)) {
					$claimItem->setErrorReconcileId($claimItem->ErrorReportItem[0]->reconcile_id);
					$claimItem->save();

					echo $claimItem->getId() . ': ' . $claimItem->getErrorReconcileID() . "\n";
				}
				$minID = $claimItem->getId();
			}
			$totalLimit--;
		} while ($claimItem instanceof ClaimItem && $totalLimit);

		echo "Done!\n\n";
	}
}