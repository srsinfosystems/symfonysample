<?php

class setDefaultPatientTypeTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'setDefaultPatientType';
		$this->briefDescription = 'Updates empty patient types to the default patient type for that client.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$limit = 1000;

		$total_updates = 0;
		$clients = Doctrine_Query::create()
			->from('Client c')
			->execute();

		foreach ($clients as $client)
		{
			$cp = ClientParamTable::getParamForClient($client->id, 'patient_status_default');
			if ($cp instanceof ClientParam && $total_updates < $limit && $cp->value) {
				$default_patient_status_id = $cp->value;

				$patients = Doctrine_Query::create()
					->from('Patient p')
					->addWhere('p.client_id = (?)', $client->id)
					->addWhere('p.patient_status_id IS NULL')
					->addWhere('p.deleted = (?)', false)
					->limit($limit)
					->execute();

				if (count($patients)) {
					echo 'Updating ' . $client->name . ' patients... ';
					foreach ($patients as $key => $patient)
					{
						$patient->patient_status_id = $default_patient_status_id;
						$patient->save();
					}
				}

				$total_updates += count($patients);
				if (count($patients)) {
					echo ' Updated ' . count($patients) . ' records' . "\n";
					echo 'Last patient ID: ' . $patient->id . "\n";
				}
			}
		}
		echo 'Updated ' . $total_updates . ' total records. ' . "\n\n"
			. 'Run this process again if any records were updated.';

	}
}