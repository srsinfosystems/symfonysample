<?php

/**
 * execute task using: symfony hype:GiavedoniHL7IssuesFix --clientID=4
 */

class GiavedoniHL7IssuesFixTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'GiavedoniHL7IssuesFix';
		$this->briefDescription = '.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
			new sfCommandOption('clientID', null, sfCommandOption::PARAMETER_REQUIRED, 'The Client ID', null)
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		if (!array_key_exists('clientID', $options) || !$options['clientID']) {
			echo '--clientID is required';
			exit();
		}

		$clientID = $options['clientID'];

		$this->updateAppointmentsFromClearview($clientID);
		$this->updatePatientsWithPhoneNumberIssues($clientID);
	}
	private function updateAppointmentsFromClearview($clientID)
	{
		$minID = $this->getMinimumAppointmentID($clientID, 0);

		$attendees = Doctrine_Query::create()
			->from('AppointmentAttendee att')
			->leftJoin('att.Appointment a')
			->addWhere('a.location_id = (?)', 7)
			->addWhere('a.client_id = (?)', $clientID)
			->addWhere('att.client_id = (?)', array($clientID))
			->addWhere('att.id > (?)', $minID)
			->orderBy('att.id asc')
			->limit(1000)
			->execute();

		foreach ($attendees as $attendee)
		{
			HL7OutboundProcessorBase::deferSIU($attendee, false, 'GIAVEDONI');
			$this->setMinimumAppointmentID($clientID, $attendee->getId());

			$minID = $attendee->getId();
		}

		$count = Doctrine_Query::create()
			->from('AppointmentAttendee att')
			->leftJoin('att.Appointment a')
			->addWhere('a.location_id = (?)', 7)
			->addWhere('a.client_id = (?)', $clientID)
			->addWhere('att.client_id = (?)', array($clientID))
			->addWhere('att.id > (?)', $minID)
			->orderBy('att.id asc')
			->count()
		;

		echo $count . ' Appointments left to process' . "\n";
		if ($count) {
			exit();
		}
	}
	private function updatePatientsWithPhoneNumberIssues($clientID)
	{
		$minID = $this->getMinimumPatientID($clientID, 0);

		$patients = Doctrine_Query::create()
			->from('Patient p')
			->addWhere('p.client_id = (?)', $clientID)
			->addWhere('p.id > (?)', $minID)
			->addWhere('p.deleted = (?)', false)
			->addWhere('p.hphone LIKE (?) or p.hphone LIKE (?) or p.wphone LIKE (?) or p.wphone LIKE (?)', array('%)%', '%(%', '%)%', '%(%'))
			->orderBy('p.id asc')
			->limit(1000)
			->execute();

		foreach ($patients as $patient)
		{
			$patient->hphone .= ' ';
			$patient->hphone = trim($patient->hphone);
			$patient->save();
			HL7OutboundProcessorBase::deferADT($patient, false, 'GIAVEDONI');
			$this->setMinimumPatientID($clientID, $patient->getId());

			$minID = $patient->getId();
		}

		$count = Doctrine_Query::create()
			->from('Patient p')
			->addWhere('p.client_id = (?)', $clientID)
			->addWhere('p.id > (?)', $minID)
			->addWhere('p.deleted = (?)', false)
			->addWhere('p.hphone LIKE (?) or p.hphone LIKE (?) or p.wphone LIKE (?) or p.wphone LIKE (?)', array('%)%', '%(%', '%)%', '%(%'))
			->orderBy('p.id asc')
			->count();

		echo $count  . ' Patients left to process' . "\n";
		if ($count) {
			exit();
		}

	}
	private function getMinimumAppointmentID($clientID, $default = 0)
	{
		$cp = ClientParamTable::getParamForClient($clientID, 'hl7_fix_appointment_id');

		if (!$cp instanceof ClientParam) {
			return $default;
		}
		return $cp->getValue();

	}
	private function setMinimumAppointmentID($clientID, $value)
	{
		$cp = ClientParamTable::setParamForClient($clientID, 'hl7_fix_appointment_id', $value);
		return;
	}
	private function getMinimumPatientID($clientID, $default = 0)
	{
		$cp = ClientParamTable::getParamForClient($clientID, 'hl7_fix_patient_id');

		if (!$cp instanceof ClientParam) {
			return $default;
		}
		return $cp->getValue();

	}
	private function setMinimumPatientID($clientID, $value)
	{
		$cp = ClientParamTable::setParamForClient($clientID, 'hl7_fix_patient_id', $value);
		return;
	}
}
