<?php

class UpdatePaidByRejectedClaimsTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'updatePaidButRejected';
		$this->briefDescription = '.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();
		$q = Doctrine_Manager::getInstance()->getCurrentConnection();

		$sql_query = 'select top 100 id from items_version where id in (select id from items where (status = 1 or status = 0)) and status = 2';
		$result = $q->execute($sql_query);
		$result = $result->fetchAll();

		$version_ids = array();
		foreach ($result as $rs)
		{
			$version_ids[] = $rs['id'];
		}

		if (!count($version_ids)) {
			echo 'Nothing to do!' . "\n";
		}

		$items = Doctrine_Query::create()
			->from('ClaimItem ci')
			->leftJoin('ci.Claim c')
			->addWhere('c.pay_prog != (?)', ClaimTable::CLAIM_TYPE_THIRD_PARTY)
			->addWhere('c.pay_prog != (?)', ClaimTable::CLAIM_TYPE_DIRECT)
			->addWhere('ci.status = (?) or ci.status = (?)', array(ClaimItemTable::STATUS_UNSUBMITTED, ClaimItemTable::STATUS_REJECTED_OPEN))
			->whereIn('ci.id', $version_ids)
			->execute();

		foreach ($items as $item)
		{
			if ($item->fee_paid == 0.00) {
				$item_version = Doctrine_Query::create()
					->from('ClaimItemVersion civ')
					->addWhere('civ.id = (?)', $item->id)
					->addWhere('civ.status = (?)', ClaimItemTable::STATUS_PAID_CLOSED)
					->fetchOne();

				if ($item_version instanceof ClaimItemVersion) {
					$item->fee_paid = $item_version->fee_paid;
				}
			}

			$item->status = ClaimItemTable::STATUS_PAID_CLOSED;
			$item->save();

			echo 'Saved Item ID ' . $item->id . "\n";
		}
	}
}