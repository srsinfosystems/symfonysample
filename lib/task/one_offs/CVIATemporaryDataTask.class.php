<?php

/**
 * execute task using: symfony hype:CVIATemporaryData --clientID=3
 */

class CVIATemporaryDataTask extends sfBaseTask
{

	private $unmatched_service_codes = array();

	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'CVIATemporaryData';
		$this->briefDescription = '.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
			new sfCommandOption('clientID', null, sfCommandOption::PARAMETER_REQUIRED, 'The Client ID', null)
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		if (!array_key_exists('clientID', $options) || !$options['clientID']) {
			echo '--clientID is required';
			exit();
		}

		$clientID = $options['clientID'];
		$minID = $this->getMinimumTemporaryID($clientID, 0);

		$forms = Doctrine_Query::create()
			->from('TemporaryForm tf')
			->addWhere('tf.client_id = (?)', $clientID)
			->addWhere('tf.id > (?)', $minID)
			->addWhere('tf.status = (?)', TemporaryFormTable::STATUS_INCOMPLETE)
			->orderBy('tf.id asc')
			->limit(10000)
			->execute();

		foreach ($forms as $form)
		{
			if ($this->hasDuplicateService($form)) {
				$form->setStatus(TemporaryFormTable::STATUS_COMPLETE);
				$form->save();
				echo 'Updated Temporary Form ID ' . $form->id . ' to Completed' . "\n";
			}
			else {
				echo 'Skipped Temporary Form ID ' , $form->id . "\n";
			}
			$this->setMinimumTemporaryID($clientID, $form->getId());
			$minID = $form->getId();
		}

		$count = Doctrine_Query::create()
			->from('TemporaryForm tf')
			->addWhere('tf.client_id = (?)', $clientID)
			->addWhere('tf.id > (?)', $minID)
			->addWhere('tf.form_type = (?)', 'CLAIM')
			->addWhere('tf.status = (?)', TemporaryFormTable::STATUS_INCOMPLETE)
			->orderBy('tf.id asc')
			->count();

		echo $count  . ' Forms left to process' . "\n";
		if (!$count) {
			$this->setMinimumTemporaryID($clientID, 0);
		}

		ksort($this->unmatched_service_codes);

		echo 'Unmatched Service Codes & Counts: ' . "\n";
		print_r ($this->unmatched_service_codes);
	}
	private function hasDuplicateService($form)
	{
		$values = unserialize($form->getValues());

		foreach ($values['claim_items'] as $item)
		{
			if (strpos($item['service_date'], '/') !== false) {
				$item['service_date'] = date(hype::DB_DATE_FORMAT, hype::parseDateToInt($item['service_date'], 'd/m/Y'));
			}

			$claims = Doctrine_Query::create()
				->from('Claim c')
				->leftJoin('c.ClaimItem ci')
				->addWhere('c.pay_prog = (?) or c.pay_prog = (?)', array(ClaimTable::CLAIM_TYPE_DIRECT, ClaimTable::CLAIM_TYPE_THIRD_PARTY))
				->addWhere('ci.service_date = (?)', $item['service_date'])
				->addWhere('c.patient_id = (?)', $values['patient_id'])
				->addWhere('c.deleted = (?)', false)
				->addWhere('ci.status != (?)', ClaimItemTable::STATUS_DELETED)
				->execute();

			foreach ($claims as $claim)
			{
				foreach ($claim->ClaimItem as $claimItem)
				{
					if ($this->directServiceCodeMatches($item['service_code'], $claimItem->getServiceCode(), $claim->getClientId())) {
						return true;
					}
					else if ($item['service_code'] == $claimItem->getServiceCode()) {
						return true;
					}
					else {
						if (!array_key_exists($item['service_code'], $this->unmatched_service_codes)) {
							$this->unmatched_service_codes[$item['service_code']] = 0;
						}
						$this->unmatched_service_codes[$item['service_code']]++;
					}
				}
			}
		}
		return false;
	}
	private function directServiceCodeMatches($serviceCode, $claimItemService, $clientId)
	{
		$directService = Doctrine_Query::create()
			->from('DirectServiceCode dsc')
			->addWhere('dsc.service_code = (?)', $claimItemService)
			->addWhere('dsc.client_id = (?)', $clientId)
			->addWhere('dsc.active = (?)', true)
			->fetchOne();

		if ($directService instanceof DirectServiceCode) {
			if (in_array($serviceCode, $directService->getOhipCodesArray())) {
				return true;
			}
		}
		return false;
	}
	private function getMinimumTemporaryID($clientID, $default = 0)
	{
		$cp = ClientParamTable::getParamForClient($clientID, 'temporary_loop_minimum_value');

		if (!$cp instanceof ClientParam) {
			return $default;
		}
		return $cp->getValue();

	}
	private function setMinimumTemporaryID($clientID, $value)
	{
		$cp = ClientParamTable::setParamForClient($clientID, 'temporary_loop_minimum_value', $value);
		return;
	}
}
