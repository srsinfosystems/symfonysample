<?php

class RevertBrazelPaidTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'revertBrazelMarkAsPaid';
		$this->briefDescription = 'Deactivates patients based on preset system parameters.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$total_count = 0;

		$items = Doctrine_Query::create()
			->from('ClaimItem ci')
			->addWhere('ci.status = (?)', ClaimItemTable::STATUS_PAID_BY_USER)
			->addWhere('ci.updated_at >= (?)', '2013-03-12 17:00:00')
			->addWhere('ci.updated_at <= (?)', '2013-03-12 18:00:00')
			->execute();

		foreach ($items as $item)
		{
			$item->revert($item->getVersion() - 1);
			$item->save();

			echo $total_count . ' Reverted item ' . $item->id . "\n";
			$total_count++;
		}

		// get all claims with a batch ID <= 262
		do {
			$claims = Doctrine_Query::create()
			->from('Claim c')
			->leftJoin('c.ClaimItem ci')
			->addWhere('c.batch_id <= (?)', 262)
			->addWhere('c.deleted = (?)', false)
			->addWhere('ci.status != (?) OR ci.id IS NULL', ClaimItemTable::STATUS_DELETED)
			->limit(500)
			->execute();

			foreach ($claims as $claim)
			{
				$claim->batch_id = null;
				$claim->batch_number = ClaimTable::EMPTY_BATCH;

				foreach ($claim->ClaimItem as $item)
				{
					if ($item->status == ClaimItemTable::STATUS_UNSUBMITTED || $item->status == ClaimItemTable::STATUS_REJECTED_OPEN) {
						$item->status = ClaimItemTable::STATUS_PAID_CLOSED;
					}
				}

				$claim->save();
				echo $total_count . ' Updated claim ' . $claim->id . "\n";
				$total_count++;
			}
		} while ($total_count < 10000 && count($claims));

		if ($total_count > 10000) {
			exit();
		}

		$batches = Doctrine_Query::create()
			->from('Batch b')
			->leftJoin('b.ReconcileBatch rb')
			->where('b.id <= (?)', 262)
			->execute();

		foreach ($batches as $batch) {
			foreach ($batch->ReconcileBatch as $rb) {
				$rb->delete();
				echo 'Deleted ReconcileBatch ' . "\n";
			}
			$batch->delete();
			echo 'Deleted Batch ' . "\n";
		}
	}
}