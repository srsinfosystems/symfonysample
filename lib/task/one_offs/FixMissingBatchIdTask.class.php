<?php

class FixMissingBatchIdTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'fixMissingBatchId';
		$this->briefDescription = '.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$sql_query = "SELECT distinct batch_number from claims where batch_id is null limit 200";

		$connection = Doctrine_Manager::getInstance()->getCurrentConnection();
		$result = $connection->execute($sql_query);
		$batches = $result->fetchAll();

		foreach ($batches as $batch_number)
		{
 			echo "\n" . 'Querying database for batches with batch_number ' . $batch_number['batch_number'] . "\n";
			$bs = Doctrine_Query::create()
				->from('Batch b')
				->addWhere('b.batch_number = (?)', $batch_number['batch_number'])
				->execute();

			echo 'Found ' . $bs->count() . ' batches.' . "\n";

			foreach ($bs as $batch)
			{
				echo 'Looking for claims with batch_number ' . $batch->batch_number . ' and client_id ' . $batch->client_id . "\n";

				$claims = Doctrine_Query::create()
					->from('Claim c')
					->addWhere('c.client_id = (?)', $batch->client_id)
					->addWhere('c.batch_number = (?)', $batch->batch_number)
					->addWhere('c.batch_id is null')
					->limit(100)
					->execute();

				echo 'Found ' . $claims->count() . ' claims.' . "\n";

				if ($claims->count()) {
					echo 'Updating claims to have Batch ID ' . $batch->id . "\n";

					foreach ($claims as $claim)
					{
						$claim->batch_id = $batch->id;
						$claim->save();

						echo 'Updated claim ID ' . $claim->id . "\n";
					}
				}
			}
		}
	}
}