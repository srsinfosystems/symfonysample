<?php

/**
 * execute task using: symfony hype:performPrimaryProfileCheck
 */

class PerformPrimaryProfileCheck extends sfBaseTask
{
	protected $saves_count = 0;

	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'performPrimaryProfileCheck';
		$this->briefDescription = '.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		echo "\nRunning Primary Profile Check...\n\n";
		DoctorTable::performPrimaryProfileCheck();

		echo "Deleting Extra ReconcileBatch Records...\n";
		$this->deleteReconcileBatchForNonAcks();

		echo "Deleting extra ReconcileDoctor records...\n";
		$this->deleteExtraReconcileDoctors();

		echo "Updating Batch Statuses...\n";
		$this->updateBatchStatus();

		echo "Finding Missing ReconcileBatch records\n";
		$this->createReconcileBatchesForMissingAcks();

		echo "Updating Statuses on BatchEditReport\n";
		$this->updateBatchAcknowledgementStatus();

		echo "Updating ReconcileDoctor Records for Batch Acknowledgements\n";
		$this->updateBatchAckReconcileDoctors();
	}

	private function createReconcileBatchesForMissingAcks()
	{
		$con = Doctrine_Manager::getInstance()->getCurrentConnection();

		do {
			$query = 'select distinct ber.reconcile_id, b.id from batches b '
				. 'left join batch_edit_report ber on ber.batch_number = b.batch_number and ber.group_num = b.group_num and b.provider_num = ber.provider_num '
				. 'left join reconcile_batch rb on ber.reconcile_id = rb.reconcile_id and b.id = rb.batch_id '
				. 'where ber.id is not null and rb.reconcile_id is null and rb.batch_id is null';

 			$result = $con->execute($query);
 			$rs = $result->fetchAll();

 			foreach ($rs as $row)
 			{
 				$rb = new ReconcileBatch();
 				$rb->setReconcileId($row['reconcile_id']);
 				$rb->setBatchId($row['id']);

 				echo '  Creating ReconcileBatch record ' . $rb->getReconcileId() . '-' . $rb->getBatchId() . "\n";
 				$rb->save();
				$this->savesCountInc();
 			}
		} while (count($rs));
	}
	private function updateBatchAcknowledgementStatus()
	{
		do {
			$acks = Doctrine_Query::create()
				->from('BatchEditReport ber')
				->addWhere('ber.status is null')
				->orderBy('ber.id')
				->limit(100)
				->execute();
			foreach ($acks as $ack)
			{
				if (strpos($ack->getOhipMessage(), '(Rejected)') !== false) {
					$ack->setStatus(BatchEditReportTable::STATUS_REJECTED);
				}
				else {
					$ack->setStatus(BatchEditReportTable::STATUS_ACCEPTED);
				}

				echo '  Updating Batch Edit Report ' . $ack->getId() . ' to ' . $ack->getStatus() . "\n";
				$ack->save();
				$this->savesCountInc();
			}
			echo "\n";
		} while (count($acks));
	}
	private function updateBatchAckReconcileDoctors()
	{
		if (!$this->getHasDeletedAckDoctors(1)) {
			$count = Doctrine_Query::create()
				->delete()
				->from('ReconcileDoctor rd')
				->addWhere('rd.reconcile_id IN (select id from Reconcile where type = (?))', ReconcileTable::FILE_TYPE_BATCH_EDIT_REPORT)
				->execute();

			$count = Doctrine_Query::create()
				->delete()
				->from('ReconcileClient rc')
				->addWhere('rc.reconcile_id IN (select id from Reconcile where type = (?))', ReconcileTable::FILE_TYPE_BATCH_EDIT_REPORT)
				->execute();

			$this->setHasDeletedAckDoctors(1);
		}

		$doctors = Doctrine_Query::create()
			->from('Doctor d')
			->addWhere('d.primary_profile = (?)', true)
			->execute();

		foreach ($doctors as $doctor)
		{
			$id = 0;
			do {
				$acks = Doctrine_Query::create()
					->from('BatchEditReport ber')
					->addWhere('ber.group_num = (?)', $doctor->getGroupNum())
					->addWhere('ber.provider_num = (?)', $doctor->getProviderNum())
					->addWhere('ber.id >= (?)', $id)
					->orderBy('ber.reconcile_id asc')
					->limit(100)
					->execute();

				foreach ($acks as $ack)
				{
					$rd = new ReconcileDoctor();
					$rd->setReconcileId($ack->getReconcileId());
					$rd->setDoctorId($doctor->getId());

					$cs = Doctrine_Query::create()
						->from('ReconcileDoctor rd')
						->addWhere('rd.doctor_id = (?)', $rd->getDoctorId())
						->addWhere('rd.reconcile_id = (?)', $rd->getReconcileId())
						->count();

					if (!$cs) {
						echo '  Creating ReconcileDoctor record ' . $rd->getDoctorId() . '-' . $rd->getReconcileId() . "\n";
						$rd->save();
					}

					$rc = new ReconcileClient();
					$rc->setClientId($doctor->getClientId());
					$rc->setReconcileId($ack->getReconcileId());

					$ds = Doctrine_Query::create()
						->from('ReconcileClient rc')
						->addWhere('rc.client_id = (?)', $rc->getClientId())
						->addWhere('rc.reconcile_id = (?)', $rc->getReconcileId())
						->count();

					if (!$ds) {
						echo '  Creating ReconcileClient record ' . $rc->getClientId() . '-' . $rc->getReconcileId() . "\n";
						$rc->save();
					}

					if (!$cs && !$ds) {
						$this->savesCountInc();
					}

					$id = $ack->getReconcileId();
				}
			} while (count($acks));
		}

		// iterate through the doctors (primary profile only)
		// get batch acks that match the doctor / group number (100 at a time)
		// put into batch ack table

	}
	private function updateBatchStatus()
	{
		do {
			$minID = $this->getMinimumBatchId(1, 0);

			$batches = Doctrine_Query::create()
				->from('Batch b')
				->addWhere('b.id >= (?)', $minID)
				->addWhere('b.status != (?)', BatchTable::STATUS_DELETED)
				->addWhere('b.status != (?)', BatchTable::STATUS_ACKNOWLEDGED)
				->orderBy('b.id asc')
				->limit(300)
				->execute();

			foreach ($batches as $batch)
			{
				$rejected = false;

				$newestBatchProcessDate = Doctrine_Query::create()
					->select('ber.reconcile_id')
					->from('BatchEditReport ber')
					->addWhere('ber.batch_number = (?)', $batch->getBatchNumber())
					->addWhere('ber.group_num = (?)', $batch->getGroupNum())
					->addWhere('ber.provider_num = (?)', $batch->getProviderNum())
					->orderBy('ber.batch_process_date desc, created_at desc')
					->execute(array(), Doctrine::HYDRATE_SCALAR);

				if (count($newestBatchProcessDate)) {
					$batchEditReports = Doctrine_Query::create()
						->from('BatchEditReport ber')
						->addWhere('ber.batch_number = (?)', $batch->getBatchNumber())
						->addWhere('ber.group_num = (?)', $batch->getGroupNum())
						->addWhere('ber.provider_num = (?)', $batch->getProviderNum())
						->addWhere('ber.reconcile_id = (?)', $newestBatchProcessDate[0]['ber_reconcile_id'])
						->execute();

					foreach ($batchEditReports as $batchEditReport)
					{
						if (strpos($batchEditReport->getOhipMessage(), '(Rejected)') !== false) {
							$rejected = true;
						}
					}

					if ($rejected) {
						$batch->setStatus(BatchTable::STATUS_REJECTED);
					}
					else {
						$batch->setStatus(BatchTable::STATUS_ACKNOWLEDGED);
					}
					$batch->save();
					echo '  Updating Batch ' . $batch->getBatchNumber() . ' status to ' . $batch->getStatus() . "\n";
				}
				else {
					echo '  Batch ' . $batch->getBatchNumber() . ' has not been acknowledged or rejected' . "\n";
				}

				$this->setMinimumBatchId(1, $batch->getId() + 1);
				$this->savesCountInc();
			}
		} while (count($batches));
		echo "\n";
	}
	private function deleteExtraReconcileDoctors()
	{
 		$con = Doctrine_Manager::getInstance()->getCurrentConnection();

		$nonPrimaryDoctors = Doctrine_Query::create()
 			->from('Doctor d')
 			->addWhere('d.primary_profile = (?)', false)
 			->orderBy('d.id asc')
 			->execute();

 		foreach ($nonPrimaryDoctors as $doctor)
 		{
			$otherDoctor = Doctrine_Query::create()
				->from('Doctor d')
				->addWhere('d.primary_profile = (?)', true)
				->addWhere('d.client_id = (?)', $doctor->getClientId())
				->addWhere('d.group_num = (?)', $doctor->getGroupNum())
				->addWhere('d.provider_num = (?)', $doctor->getProviderNum())
				->fetchOne();

 			if ($otherDoctor instanceof Doctor) {
 				echo '  Doctor ID ' . $doctor->getId() . ' is child to doctor ID ' . $otherDoctor->getId() . "\n";

 				do {
 					$query = 'select rb1.reconcile_id from reconcile_doctor rb1 '
	 					. 'left join reconcile_doctor rb2 on rb1.reconcile_id = rb2.reconcile_id '
	 					. 'where '
	 					. 'rb1.doctor_id = (' . $doctor->getId() . ') '
	 					. 'and rb2.doctor_id = (' . $otherDoctor->getId() . ') limit 100';

 					$result = $con->execute($query);
 					$rs = $result->fetchAll();

 					foreach ($rs as $row)
 					{
 						echo '    deleting reconcileDoctor record ' . $row['reconcile_id'] . '-' . $doctor->getId() . "\n";

 						$rb = Doctrine_Query::create()
	 						->delete()
	 						->from('ReconcileDoctor rd')
	 						->addWhere('rd.reconcile_id = (?)', $row['reconcile_id'])
	 						->addWhere('rd.doctor_id = (?)', $doctor->getId())
	 						->execute();
 					}
 				} while (count($rs));

 				do {
 					$rds = Doctrine_Query::create()
	 					->from('ReconcileDoctor rd')
	 					->addWhere('rd.doctor_id = (?)', $doctor->getId())
	 					->execute();

 					foreach ($rds as $rd)
 					{
 						echo '    Updating ReconcileDoctor record ' . $rd->getReconcileId() . '-' . $rd->getDoctorId() . ' to ' . $rd->getReconcileId() . '-' . $otherDoctor->getId() . "\n";
 						$rd->doctor_id = $otherDoctor->getId();
 						$rd->save();
 						$this->savesCountInc();
 					}

 				} while (count($rds));

 			}
 		}
 		echo "\n";
	}
	private function deleteReconcileBatchForNonAcks()
	{
		$count = Doctrine_Query::create()
			->delete()
			->from('ReconcileBatch rb')
			->addWhere('rb.reconcile_id IN (select id from reconcile where type != (?))', ReconcileTable::FILE_TYPE_BATCH_EDIT_REPORT)
			->execute();

		echo $count . ' ReconcileBatch records deleted.' . "\n";
	}
	private function getMinimumBatchId($clientID, $default = 0)
	{
		$cp = ClientParamTable::getParamForClient($clientID, 'batch_ack_loop_minimum');

		if (!$cp instanceof ClientParam) {
			return $default;
		}
		return $cp->getValue();
	}
	private function setMinimumBatchId($clientID, $value)
	{
		$cp = ClientParamTable::setParamForClient($clientID, 'batch_ack_loop_minimum', $value);
		return;
	}
	private function getHasDeletedAckDoctors($clientID)
	{
		$cp = ClientParamTable::getParamForClient($clientID, 'has_deleted_batch_ack_docs');

		if (!$cp instanceof ClientParam) {
			return false;
		}
		return (bool) $cp->getValue();
	}
	private function setHasDeletedAckDoctors($clientID) {
		$cp = ClientParamTable::setParamForClient($clientID, 'has_deleted_batch_ack_docs', true);
		return;
	}
	private function savesCountInc()
	{
		$this->saves_count++;

		if ($this->saves_count >= 2500) {
			echo "\n" . 'Reached Execution Limit, Exiting.' . "\n";
			exit();
		}
	}
}
