<?php

/**
 * execute task using: symfony hype:exportCVIAPatients --fileName="C:\Users\Shannon\Desktop\CVIAPatients.txt" --clientID=3
 */

class ExportCVIAPatientsList extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'exportCVIAPatients';
		$this->briefDescription = '.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
			new sfCommandOption('fileName', null, sfCommandOption::PARAMETER_REQUIRED, 'The File Name to be created', 'doctrine'),
			new sfCommandOption('clientID', null, sfCommandOption::PARAMETER_REQUIRED, 'The Client ID', 'doctrine')
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		if (!array_key_exists('fileName', $options)) {
			echo '--fileName is required';
			exit();
		}
		if (!array_key_exists('clientID', $options)) {
			echo '--clientID is required';
			exit();
		}

		$clientID = $options['clientID'];

		// get lowest patient ID (0 by default)
		$minID = $this->getMinimumPatientID($clientID, 0);

		// get 1000 patients with an id hgher that the min, ordered by id
		$patients = Doctrine_Query::create()
			->from('Patient p')
			->addWhere('p.client_id = (?)', $clientID)
			->addWhere('p.id > (?)', $minID)
			->addWhere('p.deleted = (?)', false)
			->orderBy('p.id asc')
			->limit(10000)
			->execute();

		foreach ($patients as $patient)
		{
			// write CSV record to file
			// update minimum patient ID to patient->id
			$this->writePatientCSVRecord($patient, $options['fileName']);
			$this->setMinimumPatientID($clientID, $patient->getId());
			$minID = $patient->getId();
		}

		echo Doctrine_Query::create()
			->from('Patient p')
			->addWhere('p.client_id = (?)', $clientID)
			->addWhere('p.id > (?)', $minID)
			->addWhere('p.deleted = (?)', false)
			->orderBy('p.id asc')
			->count() . "\n";

	}
	private function getMinimumPatientID($clientID, $default = 0)
	{
		$cp = ClientParamTable::getParamForClient($clientID, 'patient_export_minimum_value');

		if (!$cp instanceof ClientParam) {
			return $default;
		}
		return $cp->getValue();

	}
	private function setMinimumPatientID($clientID, $value)
	{
		$cp = ClientParamTable::setParamForClient($clientID, 'patient_export_minimum_value', $value);
		return;
	}
	private function writePatientCSVRecord($patient, $file)
	{
		$data = $patient->getId()
			. '|' . $patient->getPatientNumber()
			. '|' . $patient->getLname()
			. '|' . $patient->getFname()
			. '|' . $patient->getDob()
			. '|' . $patient->getHCN()
			. '|' . $patient->getHcnVersionCode()
			. "\n";

		file_put_contents($file, $data, FILE_APPEND);
	}
}



