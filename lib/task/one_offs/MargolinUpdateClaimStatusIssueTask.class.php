<?php

class MargolinUpdateClaimStatusIssueTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'margolinUpdateAppointmentStatus';
		$this->briefDescription = 'Fix appointments.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$id_pairs = array(
			'64697' => '425764', '64587' => '425742', '63148' => '425572',
			'65667' => '425605', '65619' => '425408', '65483' => '425249',
			'35782' => '424922', '65179' => '424923', '64402' => '423407',
			'41648' => '415508', '58921' => '403290', '64362' => '415407',
			'58910' => '396754', '58905' => '396752', '58902' => '396756',
			'58810' => '396746', '59752' => '395841', '63458' => '395843',
			'58896' => '395838', '58893' => '395842', '58891' => '395836',
			'58892' => '395835', '58889' => '395826', '58887' => '395833',
			'58887' => '395834', '58888' => '395806', '58886' => '395808',
			'63931' => '395817', '63059' => '395809', '63058' => '395804',
			'58884' => '395801', '58882' => '395790', '58879' => '395794',
			'62999' => '395791', '58877' => '395788', '58878' => '395789',
			'42456' => '415916', '58867' => '395792', '59594' => '389421',
			'59596' => '389402', '58864' => '389393', '58862' => '389373',
			'58857' => '389388', '58853' => '389368', '58851' => '389370',
			'58852' => '389371', '58849' => '389362', '58850' => '389363',
			'58845' => '389372', '58846' => '389366', '58842' => '389352',
			'58836' => '389361', '58833' => '389338', '58834' => '389342',
			'63468' => '389337', '60022' => '389444', '59706' => '385094',
			'62942' => '384970', '59959' => '384846', '59726' => '425682',
			'59573' => '425688', '59553' => '425687', '59562' => '425689',
			'59544' => '425683', '60036' => '384109', '59704' => '389320',
			'59670' => '425686', '59303' => '425678', '59303' => '425679',
			'59299' => '425684', '59331' => '425674', '42735' => '425690',
			'59358' => '385139', '59249' => '425680', '59606' => '425685',
			'41427' => '425658', '63391' => '389321', '63395' => '389317',
			'63398' => '389319', '63393' => '389316', '41624' => '425657',
			'57963' => '384660', '41129' => '425659'
		);

		$attendees = Doctrine_Query::create()
			->from('AppointmentAttendee att')
			->leftJoin('att.Appointment a')
			->whereIn('att.id', array_keys($id_pairs))
			->addWhere('att.claim_id is null')
			->addWhere('a.status = (?)', AppointmentTable::STATUS_ATTENDED)
			->execute();

		foreach ($attendees as $att)
		{
			$att->claim_id = $id_pairs[$att->id];
			$att->Appointment->status = AppointmentTable::STATUS_CLAIM;

			$att->save();
		}
	}
}