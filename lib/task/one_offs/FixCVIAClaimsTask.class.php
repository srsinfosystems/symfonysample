<?php

class FixCVIAClaimsTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'fixPatients';
		$this->briefDescription = 'Hopefully does something with merges and patients corbin ';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
		));
	}

	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}

	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

        $original = array( array(544638,875949),array(544639,807550),array(544687,795789),array(544719,833100),array(544729,863884) );

        for ($i = 0; $i <= count($original)-1; $i++) {
            $this->executeMerge($original[$i][0], $original[$i][1], 56);
            print_r($original[$i][0] . ' saved, ' . $original[$i][1] . ' dead      ');
        }

        print_r('buena');
	}

    public function executeMerge($originalP, $duplicateP, $clientid)
    {
        $patient_to_delete = Doctrine_Query::create()
            ->from('Patient p')
            ->leftJoin('p.AppointmentAttendee att')
            ->leftJoin('p.PatientLocationHistory')
            ->leftJoin('p.FollowUp')
            ->leftJoin('p.Claim')
            ->leftJoin('p.Notes')
            ->leftJoin('p.PatientStatusHistory')
            ->addWhere('p.id = (?)', $duplicateP)
            ->addWhere('p.client_id = (?)', $clientid)
            ->addWhere('att.client_id = (?) or att.client_id is null', array($clientid))
            ->addWhere('p.deleted = (?)', array(false))
            ->fetchOne();

        $master_patient = Doctrine_Query::create()
            ->from('Patient p')
            ->leftJoin('p.AppointmentAttendee att')
            ->leftJoin('p.PatientLocationHistory')
            ->leftJoin('p.FollowUp')
            ->leftJoin('p.Claim')
            ->leftJoin('p.Notes')
            ->leftJoin('p.PatientStatusHistory')
            ->addWhere('p.id = (?)', $originalP)
            ->addWhere('p.client_id = (?)', $clientid)
            ->addWhere('att.client_id = (?) or att.client_id is null', array($clientid))
            ->addWhere('p.deleted = (?)', array(false))
            ->fetchOne();

        if (!$patient_to_delete instanceof Patient || !$master_patient instanceof Patient) {
            return 'something sucks';
        }

        if (!($patient_to_delete->dob === $master_patient->dob && $patient_to_delete->hcn_num === $master_patient->hcn_num)) {
            return 'nope.jpg';
        }

        foreach ($patient_to_delete->AppointmentAttendee as $attendee)
        {
            $attendee->patient_id = $master_patient->id;
            $master_patient->AppointmentAttendee->add($attendee);
        }

        foreach ($patient_to_delete->PatientLocationHistory as $history)
        {
            $history->patient_id = $master_patient->id;
            $master_patient->PatientLocationHistory->add($history);
        }

        foreach ($patient_to_delete->FollowUp as $followUp)
        {
            $followUp->patient_id = $master_patient->id;
            $master_patient->FollowUp->add($followUp);
        }

        foreach ($patient_to_delete->Claim as $claim)
        {
            $claim->patient_id = $master_patient->id;
            $master_patient->Claim->add($claim);
        }

        foreach ($patient_to_delete->Notes as $note)
        {
            $note->patient_id = $master_patient->id;
            $master_patient->Notes->add($note);
        }

        foreach ($patient_to_delete->PatientStatusHistory as $status)
        {
            $status->patient_id = $master_patient->id;
            $master_patient->PatientStatusHistory->add($status);
        }
        $patient_to_delete->deleted = true;

        $patient_to_delete->save();
        $master_patient->save();
    }
}

