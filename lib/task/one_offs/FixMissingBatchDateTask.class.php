<?php

class FixMissingBatchDateTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'fixMissingBatchDate';
		$this->briefDescription = '.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		do {
			$batches = Doctrine_Query::create()
				->from('Batch b')
				->addWhere('b.batch_date is null')
				->limit(20)
				->execute();

			foreach ($batches as $batch)
			{
				$bn = $batch->batch_number;
				$batch->batch_date = substr($bn, 0, 4) . '-' . substr($bn, 4, 2) . '-' . substr($bn, 6, 2);
				$batch->save();

				echo 'Updated Batch ' . $batch->id . "\n";
			}
		} while ($batches->count());
	}
}