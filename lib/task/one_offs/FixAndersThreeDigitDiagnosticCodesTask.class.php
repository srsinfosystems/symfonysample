<?php

class FixAndersThreeDigitDiagnosticCodesTask extends sfBaseTask
{
	private $clientID = 88;
	private $minID = 0;
	private $queryNumber = 1;

	private $fourCharCodes = array(
		'715', '235', '229', '220', '844', '244', '236', '227', '231', '726', '237', '222', '725', '223', '233', '242', '247',
		'722', '240', '243', 'LBO', '239', '225', '245', '814', 'MAS', '277', '847', 'WSI', '513', 'LBC', '276', '716', 'LE0',
		'RC0', '840', '824', 'ORT', '226', 'THU', 'SH0', '711', 'ORT', 'PT0', 'LE1', '224', 'PT1', '843', '237', 'RC1', '848',
		'LBC', '220', 'THO', 'OUT', '371', 'CER', '227', '619', '922', 'LEC', 'VER', 'TX', 'SR', 'SH0', 'RCC', 'PRI', 'PLA',
		'PIL', 'PHY', 'OTH', 'NO', 'MOD', 'LB0', 'LB', 'KNE', 'IF', 'GOO', 'GIF', 'GC', 'EXT', 'EX', 'BLO', 'ASS', '944', '926',
		'884', '831', '822', '756', '737', '729', '728', '724', '723', '721', '719', '717', '712', '711', '515', '45', '3 P',
	);

	private $threeCharCodes = array('', '934', 'PAF', 'FAF', '1/2', 'BLK', 'SEN', 'NEW', 'PER', 'PAD', '2ND', '937', 'MIG', 'D/C',
		'WAD', 'PAC', 'PAK', 'LST', 'LOC', '1ST', 'N/C',
	);

	private $sometimesThree = array(
		'MVA', 'FOR', 'OCF', 'POC', '935', 'FAE', 'F26', 'NON', 'EXE', '959', 'ACC', 'ACU', 'RX', 'NON',
	);

	private $weirdItemIDs = array(1315512, 1321354, 1332162, 1375558, 1377962, 1379322, 1391591, 1379239, 1266448, 1379400, 1379633, 1378397);
	private $database = null;

	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'fixAndersDiagnosticCodes';
		$this->briefDescription = "Fixes Anders Diagnostic Codes that are three digits";

		$this->addOptions(array(
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
				new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
		));
	}
	public function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$this->connectQuickClaimDatabase();
		echo 'Connected Quick Claim Database' . "\n";

		$count = 0;
		$limit = 10000;
		$saveCount = 0;
		$saveLimit = 10000;

		if (array_key_exists('min_id', $arguments) && intval($arguments['min_id'])) {
			$this->minID = $arguments['min_id'];
		}

		do {
			$ids = $this->doItemIDQuery();

			if (count($ids)) {
				foreach ($ids as $id)
				{
					$claimItem = Doctrine_Query::create()
						->from('ClaimItem ci')
						->leftJoin('ci.Claim c')
						->limit(1)
						->addWhere('c.client_id = (?)', $this->clientID)
						->addWhere('ci.id = (?)', $id['ci_id'])
						->fetchOne();

					if ($claimItem instanceof ClaimItem) {
						if ($this->queryNumber == 2) {
							$this->minID = $claimItem->id;
						}

						if (strlen($claimItem->diag_code) == 3 || strlen($claimItem->diag_code) == 2) {
							$newDiagCode = $this->getQuickClaimDiagnosticCode($claimItem);

							if ($newDiagCode && substr($newDiagCode, 0, strlen($claimItem->diag_code)) == $claimItem->getDiagCode() && $newDiagCode != $claimItem->getDiagCode()) {
								echo $claimItem->getId() . ': ' . $claimItem->diag_code . ' ';
								echo $newDiagCode;
								echo "\n";
								$claimItem->diag_code = $newDiagCode;
								$claimItem->save();

								$saveCount++;
							}
							else {
								echo $claimItem->getId() . ': ' . $claimItem->diag_code . ' ';
								echo $newDiagCode;
								echo "\n";

								if ($this->queryNumber == 1) {
									exit();
								}
							}
						}
					}
					$count++;
				}
			}
			echo "\n";
		} while (count ($ids) && $count < $limit && $saveCount < $saveLimit);
	}

	private function connectQuickClaimDatabase()
	{
		$filePath = 'C:\Users\Shannon\Desktop\anders\medical.mdb';

		$this->database = new PDO('odbc:Driver={Microsoft Access Driver (*.mdb)};Dbq=' . $filePath . ';Uid=Admin');
		$this->database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	private function getQuickClaimDiagnosticCode($claimItem)
	{
		// build SQL Query for Claim
		$sql = "select * from Claims where AcctNum = ? And HealthNum = ?";
		$stmt = $this->database->prepare($sql);
		$stmt->execute(array($claimItem->Claim->getAcctNum(), $claimItem->Claim->getHCN()));
		$claim = $stmt->fetch();

		if ($claim) {
			// build SQL Query for Item
			$sql = "select * from Items where ClaimID = ? and ServiceCode = ? and ServiceDate = ?";
			$stmt = $this->database->prepare($sql);
			$stmt->execute(array($claim['ClaimID'], $claimItem->getServiceCode(), $claimItem->getServiceDate()));
			$item = $stmt->fetch();

			if ($item) {
				return strtoupper(trim($item['DiagCode']));
			}
		}

		$sql = "select * from Claims where AcctNum = ? And PayProg = ?";
		$stmt = $this->database->prepare($sql);
		$stmt->execute(array($claimItem->Claim->getAcctNum(), $this->mapPayProg($claimItem->Claim->getPayProg())));
		$claim = $stmt->fetch();

		if ($claim) {
			// build SQL Query for Item
			$sql = "select * from Items where ClaimID = ? and ServiceCode = ? and ServiceDate = ?";
			$stmt = $this->database->prepare($sql);
			$stmt->execute(array($claim['ClaimID'], $claimItem->getServiceCode(), $claimItem->getServiceDate()));
			$item = $stmt->fetch();

			if ($item) {
				return strtoupper(trim($item['DiagCode']));
			}
			else {
				echo ' no match D ';
			}
		}
		else {
			echo ' no match C ';
		}

		return null;
	}
	private function mapPayProg($x)
	{
		if ($x == ClaimTable::CLAIM_TYPE_DIRECT) {
			return 'Direct';
		}
		if ($x == ClaimTable::CLAIM_TYPE_THIRD_PARTY) {
			return 'Third Party';
		}

		return $x;
	}
	private function doItemIDQUery()
	{
		switch ($this->queryNumber) {
			case 1:
				$ids = $this->doBigDiagCodeQuery();
				if (count($ids)) {
					return $ids;
				}
				$this->queryNumber++;
			case 2:
				$ids = $this->doSometimesThreeQuery();
				if (count($ids)) {
					return $ids;
				}
				$this->queryNumber++;
		}

	}
	private function doSometimesThreeQuery()
	{
		$ids = Doctrine_Query::create()
			->select('ci.id')
			->from('ClaimItem ci')
			->leftJoin('ci.Claim c')
			->addWhere('c.client_id = (?)', $this->clientID)
			->addWhere('length(ci.diag_code) <= (?)', 3)
			->whereIn('ci.diag_code', $this->sometimesThree)
			->addWhere('ci.id > (?)', $this->minID)
// 		->whereNotIn('ci.id', $this->weirdItemIDs)
			->orderBy('ci.id asc')
			->limit(15)
			->execute(array(), Doctrine::HYDRATE_SCALAR);

		if (count($ids)) {
			return $ids;
		}
	}
	private function doBigDiagCodeQuery()
	{
		foreach ($this->fourCharCodes as $idx => $code)
		{
			echo 'Looking for Diag Code ' . $code . "\n";

			$ids = Doctrine_Query::create()
				->select('ci.id')
				->from('ClaimItem ci')
				->leftJoin('ci.Claim c')
				->addWhere('c.client_id = (?)', $this->clientID)
				->whereNotIn('ci.id', $this->weirdItemIDs)
				->addWhere('ci.diag_code = (?)', $code)
				->limit(15)
				->execute(array(), Doctrine::HYDRATE_SCALAR);

			if (count($ids)) {
				return $ids;
			}
			unset ($this->fourCharCodes[$idx]);
		}

		echo 'No Four Char Diag Codes found.' . "\n";
	}
}