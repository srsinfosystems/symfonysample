<?php

class ODCListUnmarkedThirdPartyClaimsTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'odcListUnmarked';
		$this->briefDescription = '.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$items = Doctrine_Query::create()
			->from('ClaimItem ci')
			->leftJoin('ci.Claim c')
			->leftJoin('c.Doctor d')
			->leftJoin('c.ThirdParty tp')
			->addWhere('c.pay_prog = (?)', ClaimTable::CLAIM_TYPE_THIRD_PARTY)
			->addWhere('c.deleted = (?)', false)
			->addWhere('(c.fully_paid = (?) or c.fully_paid is null)', false)
			->addWhere('ci.fee_subm = ci.fee_paid')
			->orderBy('c.lname, c.fname')
			->execute()
		;

		$idx = 1;
		foreach ($items as $item)
		{
			$claim = $item->Claim;

			echo 'Claim ' . $idx . ':' . "\n";
			echo '  ID: ' . $claim->id . "\n";
			echo '  Patient: ' . $claim->lname . ', ' . $claim->fname . '   [PID: ' . $claim->patient_id . ']' . "\n";
			echo '  Doctor: ' . $claim->Doctor . '   [' . $claim->Doctor->group_num . '-' . $claim->Doctor->provider_num . "]\n";
			echo '  Third Party: ' . $claim->ThirdParty . "\n";
			echo '  Items:' . "\n";

			foreach ($claim->ClaimItem as $ci)
			{
				echo '    ' .  $ci->service_code . '   ' . date('d/m/Y', hype::parseDateToInt($ci->service_date, hype::DB_ISO_DATE))
					. '    Billed: $' . number_format($ci->fee_subm, 2) . ';   Paid: $' . number_format($ci->fee_paid, 2) . "\n";
			}

			echo "\n\n";
			$idx++;
		}
	}
}