<?php
class UpdateInPlacePaintsilTask extends sfBaseTask
{

    protected function configure()
    {
        $this->namespace = 'hype';
        $this->name = 'fixPaintsilClaims';
        $this->briefDescription = '.';

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
        ));
    }
    protected function execute($arguments = array(), $options = array())
    {
        try {
            sfContext::createInstance($this->configuration);
            $this->executeTask($arguments, $options);
        }
        catch (Exception $e) {
            $this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
            throw $e;
        }
    }

    private $originalClientID = 55;
    private $originalClientName = 'DR. JAMES PAINTSIL [INACTIVE]';
    private $newClientID = 78;
    private $newClientName = 'JAMES PAINTSIL';

    private $locationsMap = array(
        '73' => '97'
    );

    private $doctorsMap = array(
        546 => 659
    );

    private function executeTask($arguments, $options)
    {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase('doctrine')->getConnection();

        $this->checkClients();
        $this->updatePatients();
        $this->updateClaims();
    }

    private function checkClients()
    {
        $originalClient = Doctrine_Query::create()
            ->from('Client c')
            ->addWhere('c.id = (?)', array($this->originalClientID))
            ->addWhere('c.name = (?)', array($this->originalClientName))
            ->count();

        $newClient = Doctrine_Query::create()
            ->from('Client c')
            ->addWhere('c.id = (?)', array($this->newClientID))
            ->addWhere('c.name = (?)', array($this->newClientName))
            ->count();

        if ($originalClient == 0 || $newClient == 0) {
            echo 'Incorrct database, exiting' . "\n";
            exit();
        }
    }
    private function updatePatients()
    {
        do {
            $patient = Doctrine_Query::create()
                ->from('Patient p')
                ->addWhere('p.client_id = (?)', array($this->originalClientID))
                ->addWhere('p.created_by is not null')
                ->limit(1)
                ->fetchOne();

            if ($patient instanceof Patient) {
                $patient = $this->updatePatient($patient);
                $patient->save();

                echo 'Updated patient ID ' . $patient->getID() . "\n";
            }
        } while ($patient instanceof Patient);
    }
    private function updatePatient($patient)
    {
        $patient->setLocationId($this->getLocationMapping($patient->getLocationId()));
        $patient->setClientId($this->newClientID);

        return $patient;
    }
    private function updateClaims()
    {
        do {
            $claim = Doctrine_Query::create()
                ->from('Claim c')
                ->leftJoin('c.ClaimItem ci')
//                ->leftJoin('c.Patient p')
                ->addWhere('c.client_id = (?)', array($this->originalClientID))
                ->addWhere('c.created_by is not null')
                ->limit(1)
                ->fetchOne();

            if ($claim instanceof Claim) {
                $claim->setBatchId(null);
                $claim->setBatchNumber(ClaimTable::EMPTY_BATCH);
                $claim->setDoctorID($this->getDoctorMapping($claim->getDoctorId()));
                $claim->setLocationId($this->getLocationMapping($claim->getLocationId()));
                $claim->setClientID($this->newClientID);

                if ($claim->Patient->getClientID() == $this->originalClientID) {
                    $claim->Patient = $this->updatePatient($claim->Patient);
                }

                foreach ($claim->ClaimItem as $key => $item)
                {
                    $claim->ClaimItem[$key]->setReconcileID(null);
                    $claim->ClaimItem[$key]->setErrorReconcileID(null);
                    $claim->ClaimItem[$key]->setClientID($this->newClientID);
                }

                $claim->save();
                echo 'Updated Claim ID ' . $claim->getID() . "\n";
            }

        } while ($claim instanceof Claim);
    }

    private function getLocationMapping($originalLocationID)
    {
        if (array_key_exists($originalLocationID, $this->locationsMap)) {
            return $this->locationsMap[$originalLocationID];
        }

        echo 'No Location Mapping for Location ID ' . $originalLocationID . "\n";
        exit();

        return null;
    }
    private function getDoctorMapping($originalDoctorID)
    {
        if (array_key_exists($originalDoctorID, $this->doctorsMap)) {
            return $this->doctorsMap[$originalDoctorID];
        }

        echo 'No Location Mapping for Doctor ID ' . $originalDoctorID . "\n";
        exit();

        return null;

    }
}

