<?php

class FixCVIAPatientsTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'fixCVIAPatients';
		$this->briefDescription = '.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$patients = Doctrine_Query::create()
		->from('Patient p')
		->whereIn('p.id', array(
			72624, 78093, 78987, 85020, 88779, 104517, 107725, 112224, 115742, 118289, 121140, 126627, 132769, 134354, 142803, 142819,
			148611, 152129, 152409, 162430, 165614, 165687, 165790, 167671, 170301, 170936, 171460, 173533, 176416, 180050, 182554,
			182628, 187050, 187107, 188067, 188141, 197534, 198678, 199665, 200319, 200557, 201433, 203144, 203560, 205132, 207639,
			210745, 212448, 213738, 214399, 215813, 217125, 217144, 217171, 217188, 217191, 217201, 217242, 217244, 217254, 217260,
			217262, 217275, 222522, 222523, 222530, 222567, 222587
		))
		->execute();

		foreach ($patients as $patient)
		{
			$patient->revert(1);
			$patient->save();
		}
	}
}

