<?php

class ApplyNewClientPermissionTask extends sfBaseTask
{
	/**
	 * HOW TO RUN THIS COMMAND:
	 * - permission is the name of the permission that you want to apply
	 * - userTypes is a comma-separated list of user types to add the permission to.
	 *
	 * symfony hype:applyNewClientPermission --permission="Update Claim Payment Amounts" --userTypes="System Administrator,Billing Secretary"
	 * symfony hype:applyNewClientPermission --permission="Override Claim Save" --userTypes="System Administrator,Billing Secretary"
	 * symfony hype:applyNewClientPermission --permission="HL7 Reporting" --userTypes="System Administrator,Billing Secretary"
	 */
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'applyNewClientPermission';
		$this->briefDescription = 'Applies new permission to all client accounts and specific user types.';

		$this->addOptions(array(
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
			new sfCommandOption('userTypes', null, sfCommandOption::PARAMETER_REQUIRED, 'User Type(s) to apply permission to: Comma Separated', null),
			new sfCommandOption('permission', null, sfCommandOption::PARAMETER_REQUIRED, 'Permission', null),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		if (!isset($options['permission'])) {
			echo '--permission is required';
			exit();
		}

		$permission_suffix = hype::getUserPermissionSuffix($options['permission']);

		if (!$permission_suffix) {
			echo 'Invalid User Type provided';
			exit();
		}

		$userTypes = array();
		if (isset($options['userTypes'])) {
			if (strpos($options['userTypes'], ',') !== false) {
				$userTypes = explode(',', $options['userTypes']);
			}
			else {
				$userTypes[] = $otpions['userTypes'];
			}
		}

		$constant = constant('hype::PERMISSION_' . $permission_suffix);
		$name = constant('hype::PERMISSION_NAME_' . $permission_suffix);
		$module = constant('hype::PERMISSION_MOD_' . $permission_suffix);

		$clients = Doctrine_Query::create()
			->from('Client c')
			->leftJoin('c.ClientModule cm')
			->leftJoin('c.ClientPermissionNew cpn')
			->leftJoin('c.UserType ut')
			->leftJoin('ut.UserTypePermissionNew utp')
			->addWhere('cm.module_name = (?)', $module)
			->addWhere('cm.active = (?)', true)
			->whereIn('ut.name', $userTypes)
			->execute();

		foreach ($clients as $client)
		{
			$has_permission = false;
			foreach ($client->ClientPermissionNew as $cpn)
			{
				if ($cpn->permission_constant == $constant && $cpn->active) {
					$has_permission = true;
				}
			}

			if (!$has_permission) {
				$cpn = new ClientPermissionNew();
				$cpn->client_id = $client->id;
				$cpn->permission_constant = $constant;
				$cpn->permission_name = $name;
				$cpn->active = true;

				echo 'Added Permission to client ' . $client->name . "\n";
				$cpn->save();
			}

			foreach ($client->UserType as $ut)
			{
				$utpn = new UserTypePermissionNew();
				$utpn->client_id = $client->id;
				$utpn->user_type_id = $ut->id;
				$utpn->permission_constant = $constant;
				$utpn->permission_name = $name;
				$utpn->active = true;

				echo 'Added Permission to User Type Permission ' . $ut->name . "\n";
				$utpn->save();

				foreach ($ut->UserProfile as $up)
				{
					$upn = new UserPermissionNew();
					$upn->user_id = $up->id;
					$upn->permission_constant = $constant;
					$upn->permission_name = $name;
					$upn->active = true;

					echo 'Added Permission to User Profile ' . $up->first_name . ' ' . $up->last_name . "\n";
					$upn->save();
				}
			}

			echo "\n";
		}
	}
}