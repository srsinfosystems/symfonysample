<?php

class patientNullClientIDTask extends sfBaseTask
{
	private $minID = 0;
	private $database = null;

	protected function configure()
	{
		$this->namespace = 'updateDataModel';
		$this->name = '2014-07-patientNullClientID';
		$this->briefDescription = "Finds patients with a null client ID and fixes it";

		$this->addOptions(array(
				new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
				new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
		));
	}
	public function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$count = 0;
		$limit = 10000;

		if (array_key_exists('min_id', $arguments) && intval($arguments['min_id'])) {
			$this->minID = $arguments['min_id'];
		}

		do {
			$patient = Doctrine_Query::create()
				->from('Patient p')
				->leftJoin('p.Claim c')
				->addWhere('p.id > (?)', $this->minID)
				->addWhere('p.client_id is null')
				->fetchOne();

			if ($patient instanceof Patient) {
				echo date(hype::DB_ISO_DATE) . ' : Patient ID = ' . $patient->getId();

				if (count($patient->Claim)) {
					$patient->client_id = $patient->Claim[0]->client_id;
					$patient->save();

					echo ' Client ID = ' . $patient->client_id;
				}

				echo "\n";
				$this->minID = $patient->id;
			}
 			$count++;
		} while ($count < $limit && $patient instanceof Patient);
	}
}