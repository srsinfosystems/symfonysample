<?php

class encryptUnencryptedTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'patient';
		$this->name = 'encryptUnencrypted';
		$this->briefDescription = 'Encrypts health card numbers that are unencrypted.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();
		$client_ids = array();

		$patients = Doctrine_Query::create()
			->from('Patient p')
			->addWhere('len(p.hcn_num) < 20 and len(p.hcn_num) != 0')
			->addWhere('p.client_id is not null')
			->addWhere('p.deleted = (?)', false)
			->limit(400)
			->execute();

		try {
			$connection->beginTransaction();
			foreach ($patients as $key => $patient)
			{
				if (!array_key_exists($patient->client_id, $client_ids)) {
			    	$cp = ClientParamTable::getParamForClient($patient->client_id, 'has_hl7');
			    	if ($cp instanceof ClientParam) {
			    		$client_ids[$patient->client_id] = (bool) $cp->value;

			    		if ($cp->value == true) {
			    			$cp->value = false;
			    			$cp->save();
			    		}
			    	}
			    	else {
			    		$client_ids[$patient->client_id] = false;
			    	}
				}

				$patient->hcn_num = hype::encryptHCN($patient->hcn_num);
				$patient->save();
			}

			foreach ($client_ids as $client_id => $val)
			{
				if ($val == true) {
					$cp = ClientParamTable::getParamForClient($client_id, 'has_hl7');
					$cp->value = true;
					$cp->save();
				}
			}

			$connection->commit();
		}
		catch (Exception $e) {
			print_r ($patient->toArray());
			throw $e;
			$connection->rollback();
		}

		echo count($patients) . ' patients processed' . "\n";
	}
}