<?php

class buildClaimReportTask extends sfBaseTask
{
	private $customToStrings = array();

	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'buildClaimReport';
		$this->briefDescription = 'Updates or Builds Claim Report records for recently updated records.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('count', null, sfCommandOption::PARAMETER_OPTIONAL, 'Change number of records to build', 30),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		if (array_key_exists('count', $options) && intval($options['count'])) {
			$maximum = intval($options['count']);
		}
		else {
			$maximum = 30;
		}

		$batchMaximum = 30;
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$count = 0;
		do {
			echo $count . ' Looking for unbuilt Claim Item updates.' . "\n";
			$unbuiltClaimItemID = Doctrine_Query::create()
				->select('civ.id')
				->from('ClaimItemVersion civ')
				->where('civ.report_built = (?)', ClaimItemVersionTable::CLAIM_REPORT_UNPROCESSED)
				->limit(1)
				->fetchOne(array(), Doctrine::HYDRATE_SINGLE_SCALAR);

			if ($unbuiltClaimItemID) {
				echo 'Generate Claim Item Report for Claim Item ID ' . $unbuiltClaimItemID . "\n";
				$this->handleItemReporting($unbuiltClaimItemID);

				$count++;
			}
			echo "\n";
		} while ($unbuiltClaimItemID && $count <= $maximum);

		if ($count >= $maximum) {
			return;
		}

		do {
			echo $count . ' Looking for unbuilt Claim updates.' . "\n";

			$unbuiltClaimID = Doctrine_Query::create()
				->select('cv.id')
				->from('ClaimVersion cv')
				->where('cv.report_built = (?)', ClaimVersionTable::CLAIM_REPORT_UNPROCESSED)
				->limit(1)
				->fetchOne(array(), Doctrine::HYDRATE_SINGLE_SCALAR);

			if ($unbuiltClaimID) {
				echo 'Generate Claim Report for Claim ID ' . $unbuiltClaimID . "\n";
				$this->handleClaimReporting($unbuiltClaimID);

				$count++;
			}
			echo "\n";
		} while ($unbuiltClaimID && $count <= $maximum);

		if ($count >= $maximum) {
			return;
		}

		do {
			echo $count . ' Looking for unbuilt Batch updates.' . "\n";

			$unbuiltBatchID = Doctrine_Query::create()
				->select('bv.id')
				->from('BatchVersion bv')
				->where('bv.report_built = (?)', ClaimVersionTable::CLAIM_REPORT_UNPROCESSED)
				->fetchOne(array(), Doctrine::HYDRATE_SINGLE_SCALAR);

			if ($unbuiltBatchID) {
				echo 'Generate Batch Report for Batch ID ' . $unbuiltBatchID . "\n";
				$this->handleBatchReporting($unbuiltBatchID);

				$count++;
			}
			echo "\n";
		} while ($unbuiltBatchID && $count <= $batchMaximum);

		if ($count >= $batchMaximum) {
			return;
		}

		do {
			echo $count . ' Looking for unbuilt Batch Acknowledgement updates.' . "\n";

			$unbuiltAckID = Doctrine_Query::create()
				->select('ba.id')
				->from('BatchEditReportVersion ba')
				->where('ba.report_built = (?)', ClaimVersionTable::CLAIM_REPORT_UNPROCESSED)
				->fetchOne(array(), Doctrine::HYDRATE_SINGLE_SCALAR);

			if ($unbuiltAckID) {
				echo 'Generate Batch Report for Batch Edit Report ID ' . $unbuiltAckID . "\n";
				$this->handleBatchAcknowledgementReporting($unbuiltAckID);

				$count++;
			}
			echo "\n";
		} while ($unbuiltAckID && $count <= $maximum);

		if ($count >= $maximum) {
			return;
		}
	}
	private function handleBatchAcknowledgementReporting($batch_edit_report_id)
	{
		$batch_edit_report = Doctrine_Query::create()
			->from('BatchEditReport ber')
			->leftJoin('ber.Reconcile r')
			->leftJoin('r.ReconcileBatch rb')
			->leftJoin('rb.Batch b')
			->addWhere('ber.id = (?)', $batch_edit_report_id)
			->fetchOne();

		if ($batch_edit_report instanceof BatchEditReport) {
			if ($batch_edit_report->offsetExists('Reconcile')) {
				if (count($batch_edit_report->Reconcile->ReconcileBatch)) {
					foreach ($batch_edit_report->Reconcile->ReconcileBatch as $rec_batch)
					{
						$batch_id = $rec_batch->batch_id;

						$batch_report = $this->getReportBatchByBatchId($batch_id);
						$batch_report->updateAcknowledgementInformation($batch_edit_report);

						if ($batch_report->status == BatchTable::STATUS_ACKNOWLEDGED) {
							$claim_reports = $this->getReportClaimsByBatchId($batch_id);

							foreach ($claim_reports as $claim_report)
							{
								$claim_report->updateFromBatchAcknowledgementReport($batch_edit_report);
								$claim_report->save();
							}
						}
						$batch_report->save();
					}
				}
			}
		}

		$rs = Doctrine_Query::create()
			->update('BatchEditReportVersion')
			->set('report_built', ClaimVersionTable::CLAIM_REPORT_COMPLETED)
			->addWhere('id = (?)', $batch_edit_report_id)
			->execute();

	}
	private function handleBatchReporting($batch_id)
	{
		$batch = Doctrine_Query::create()
			->from('Batch b')
			->addWhere('b.id = (?)', $batch_id)
			->fetchOne();

		if ($batch instanceof Batch) {
			$reportClaims = $this->getReportClaimsByBatchId($batch->getId());
			foreach ($reportClaims as $reportClaim)
			{
				$reportClaim->updateFromBatch($batch);
				$reportClaim->save();
			}

			$report_batch = $this->getReportBatchByBatchId($batch->getId());
			if ($report_batch instanceof ReportBatch) {
				$report_batch->updateFromBatch($batch, $this->getCustomToString($batch->getClientId()));

				try {
					$report_batch->save();
				}
				catch (Exception $e) {
					print_r ($e->getTraceAsString());
					exit();
				}
			}
		}

		$rs = Doctrine_Query::create()
			->update('BatchVersion')
			->set('report_built', ClaimVersionTable::CLAIM_REPORT_COMPLETED)
			->addWhere('id = (?)', $batch_id)
			->execute();
	}
	private function handleClaimReporting($claim_id)
	{
		$claim = Doctrine_Query::create()
			->from('Claim c')
 			->leftJoin('c.Batch b')
			->addWhere('c.id = (?)', $claim_id)
			->fetchOne();

		if ($claim instanceof Claim) {
			if ($claim->deleted) {
				ReportClaimTable::deleteByClaimId($claim->getId());
			}
			else {
				$reportClaims = $this->getReportClaimsByClaimId($claim->getId());

				foreach ($reportClaims as $reportClaim)
				{
					$reportClaim->updateFromClaim($claim);
					$reportClaim->updateFromBatch($claim->Batch);

					$reportClaim->save();
				}
			}

			$mostRecentBatchId = Doctrine::getTable('ClaimVersion')->getMostRecentBatchId($claim->getId());
			if ($mostRecentBatchId) {
				$rs = Doctrine_Query::create()
					->update('BatchVersion')
					->set('report_built', ClaimItemVersionTable::CLAIM_REPORT_UNPROCESSED)
					->addWhere('id = (?)', $mostRecentBatchId)
					->execute();
			}
		}

		$rs = Doctrine_Query::create()
			->update('ClaimVersion')
			->set('report_built', ClaimVersionTable::CLAIM_REPORT_COMPLETED)
			->addWhere('id = (?)', $claim_id)
			->execute();
	}
	private function handleItemReporting($itemID)
	{
		$claimItem = Doctrine_Query::create()
			->from('ClaimItem ci')
			->leftJoin('ci.Claim c')
 			->leftJoin('c.Batch b')
			->addWhere('ci.id = (?)', $itemID)
			->fetchOne();

		if ($claimItem instanceof ClaimItem) {
			if ($claimItem->getStatus() == ClaimItemTable::STATUS_DELETED) {
				ReportClaimTable::deleteByClaimItemId($claimItem->getId());
			} else if ($claimItem->Claim->deleted) {
				ReportClaimTable::deleteByClaimId($claimItem->Claim->getId());
			}
			else {
				$reportClaim = $this->getReportClaimByItemId($claimItem->getId());

				$reportClaim->updateFromClaimItem($claimItem);
				$reportClaim->updateFromClaim($claimItem->Claim);
				$reportClaim->updateFromBatch($claimItem->Claim->Batch);

				try {
					$reportClaim->save();
				}
				catch (Exception $e) {
					if (strpos($e->getMessage(), "for key 'report_claim_item_id'" === false)) {
						throw $e;
					}

					echo $e->getMessage() . "\n";
				}
			}

			$mostRecentBatchId = Doctrine::getTable('ClaimVersion')->getMostRecentBatchId($claimItem->getClaimId());

			if ($mostRecentBatchId) {
				$rs = Doctrine_Query::create()
					->update('BatchVersion')
					->set('report_built', ClaimItemVersionTable::CLAIM_REPORT_UNPROCESSED)
					->addWhere('id = (?)', $mostRecentBatchId)
					->execute();
			}
		}

		$rs = Doctrine_Query::create()
			->update('ClaimItemVersion')
			->set('report_built', ClaimItemVersionTable::CLAIM_REPORT_COMPLETED)
			->addWhere('id = (?)', $itemID)
			->execute();
	}
	private function getCustomToString($clientID)
	{
		if (array_key_exists($clientID, $this->customToStrings)) {
			return $this->customToStrings[$clientID];
		}

		$rv = ClientParamTable::getDefaultForValue('doctor_title_format');

		$cp = ClientParamTable::getParamForClient($clientID, 'doctor_title_format');
		if ($cp instanceof ClientParam) {
			$rv = $cp->value;
		}

		$this->customToStrings[$clientID] = $rv;

		return $rv;
	}
	private function getReportClaimByItemId($itemID)
	{
		echo '    getReportClaimByItemID(' . $itemID . ') called' . "\n";

		$reportClaim = Doctrine_Query::create()
			->from('ReportClaim rc')
			->addWhere('rc.item_id = (?)', $itemID)
			->limit(1)
			->fetchOne();

		if (!$reportClaim instanceof ReportClaim) {
			$reportClaim = new ReportClaim();
		}
		return $reportClaim;
	}
	private function getReportClaimsByClaimId($claim_id)
	{
		$reportClaims = Doctrine_Query::create()
			->from('ReportClaim rc')
			->addWhere('rc.claim_id = (?)', $claim_id)
			->execute();
		return $reportClaims;
	}
	private function getReportClaimsByBatchId($batch_id)
	{
		$reportClaims = Doctrine_Query::create()
			->from('ReportClaim rc')
			->addWhere('rc.batch_id = (?)', $batch_id)
			->execute();
		return $reportClaims;
	}
	private function getReportBatchByBatchId($batch_id)
	{
		$report_batch = Doctrine_Query::create()
			->from('ReportBatch rb')
			->addWhere('rb.batch_id = (?)', $batch_id)
			->fetchOne();

		if (!$report_batch instanceof ReportBatch) {
			$report_batch = new ReportBatch();
		}
		return $report_batch;
	}
}
