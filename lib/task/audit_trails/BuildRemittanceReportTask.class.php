<?php

class buildRemittanceReportTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'buildRemittanceReport';
		$this->briefDescription = 'Updates or Builds Remittance Report records for recently updated records.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('debug', false, sfCommandOption::PARAMETER_OPTIONAL, 'Debugging on?', false),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$maximum = 50;
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$count = 0;
		do {
			$sql_query = 'SELECT TOP 1 id from ' . Doctrine::getTable('RaItemVersion')->getTableName()
				. ' WHERE report_built = ' .ClaimItemVersionTable::CLAIM_REPORT_UNPROCESSED
				. ' ORDER BY version desc';

			$connection = Doctrine_Manager::getInstance()->getCurrentConnection();
			$result = $connection->execute($sql_query);
			$result = $result->fetchAll();

			if (count($result)) {
				$this->handleRaItemReporting($result[0]['id']);
			}
			$count++;
		} while (count($result) && $count <= $maximum);

	}
	private function handleRaItemReporting($ra_item_id)
	{
		$ra_item = Doctrine_Query::create()
			->from('RaItem ri')
			->addWhere('ri.id = (?)', $ra_item_id)
			->fetchOne();

		// TODO: deal with the issue where the RA Item could belong to more than one CLIENT ID (through multiply defined doctor records

		if ($ra_item instanceof RaItem) {
			$report_remittance = $this->getReportRemittanceByRaItemId($ra_item->getId());

			$report_remittance->updateFromRaItem($ra_item);
			$report_remittance->save();

			$rs = Doctrine_Query::create()
				->update('RaItemVersion')
				->set('report_built', ClaimItemVersionTable::CLAIM_REPORT_COMPLETED)
				->addWhere('id = (?)', $ra_item_id)
				->execute();
		}
	}
	private function getReportRemittanceByRaItemId($item_id)
	{
		$report_remittance = Doctrine_Query::create()
			->from('ReportRemittance rr')
			->addWhere('rr.ra_item_id = (?)', $item_id)
			->fetchOne();

		if (!$report_remittance instanceof ReportRemittance) {
			$report_remittance = new ReportRemittance();
		}
		return $report_remittance;
	}
}