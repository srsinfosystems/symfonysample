<?php

class checkAppointmentForOwingTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'checkAppointmentForOwing';
		$this->briefDescription = 'Checks Appointment Attendees for Direct Claims that are still owing';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('debug', false, sfCommandOption::PARAMETER_OPTIONAL, 'Debugging on?', false),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$maximum = 10;
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$count = 0;
		do {
			$sql_query = 'SELECT TOP 1 patient_id, updated_at from ' . Doctrine::getTable('ClaimVersion')->getTableName()
			. ' WHERE checked_owes = ' . ClaimVersionTable::CHECK_UNDONE
			. ' ORDER BY updated_at desc';

			$connection = Doctrine_Manager::getInstance()->getCurrentConnection();
			$result = $connection->execute($sql_query);
			$result = $result->fetchAll();

			if (count($result)) {
				$patient_id = $result[0]['patient_id'];
				$updated_at = $result[0]['updated_at'];

				$appointment_attendees = Doctrine_Query::create()
				->from('AppointmentAttendee att')
				->addWhere('att.patient_id = ?', $patient_id)
				->execute();

				if (count($appointment_attendees)) {
					$owes_money = $this->hasUnpaidDirectClaims($patient_id);

					foreach ($appointment_attendees as $key => $att)
					{
						$appointment_attendees[$key]->owes_money = $owes_money;
					}

					$appointment_attendees->save();
				}

				$sql_query = 'UPDATE ' . Doctrine::getTable('ClaimVersion')->getTableName()
				. ' SET checked_owes = ' . ClaimVersionTable::CHECK_DONE
				. ' WHERE patient_id = ' . $patient_id
				. " AND updated_at <= '" . $updated_at . "'";

				$result2 = $connection->execute($sql_query);
			}
			$count++;
		} while ($count < $maximum && count($result));

		do {
			$sql_query = 'SELECT TOP 1 patient_id, updated_at from ' . Doctrine::getTable('AppointmentAttendeeVersion')->getTableName()
			. ' WHERE checked_owes = ' . ClaimVersionTable::CHECK_UNDONE
			. ' ORDER BY updated_at desc';

			$connection = Doctrine_Manager::getInstance()->getCurrentConnection();
			$result = $connection->execute($sql_query);
			$result = $result->fetchAll();

			if (count($result)) {
				$patient_id = $result[0]['patient_id'];
				$updated_at = $result[0]['updated_at'];

				$appointment_attendees = Doctrine_Query::create()
					->from('AppointmentAttendee att')
					->addWhere('att.patient_id = ?', $patient_id)
					->execute();

				if (count($appointment_attendees)) {
					$owes_money = $this->hasUnpaidDirectClaims($patient_id);

					foreach ($appointment_attendees as $key => $att)
					{
						$appointment_attendees[$key]->owes_money = $owes_money;
					}

					$appointment_attendees->save();
				}

				$sql_query = 'UPDATE ' . Doctrine::getTable('AppointmentAttendeeVersion')->getTableName()
				. ' SET checked_owes = ' . ClaimVersionTable::CHECK_DONE
				. ' WHERE patient_id = ' . $patient_id;

				$result2 = $connection->execute($sql_query);
			}
			$count++;
		} while ($count < $maximum && count($result));
	}
	protected function hasUnpaidDirectClaims($patient_id)
	{
		$rv = false;

		$claims = Doctrine_Query::create()
			->from('Claim c')
			->addWhere('c.pay_prog = (?)', ClaimTable::CLAIM_TYPE_DIRECT)
			->addWhere('c.patient_id = (?)', $patient_id)
			->addWhere('c.deleted = (?)', false)
			->execute();

		foreach ($claims as $claim)
		{
			if (!$claim->fully_paid) {
				return true;
			}
		}

		return $rv;
	}
}