<?php

class auditClaimItemTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'auditClaimItem';
		$this->briefDescription = 'Builds Claim Items Audit Trail.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('debug', false, sfCommandOption::PARAMETER_OPTIONAL, 'Debugging on?', false),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$processed = null;
		$chls = null;

		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$min_status = ClaimItemVersionTable::PROCESSED_INCOMPLETE;
		if ($options['debug']) {
			$min_status = ClaimItemVersionTable::PROCESSED_ERRORS;
		}

		$item_versions = Doctrine_Query::create()
			->from('ClaimItemVersion cv')
			->addWhere('cv.processed <= (?) OR cv.processed IS NULL', $min_status)
			->limit(50)
			->execute();

		foreach ($item_versions as $item_version)
		{
			if ($item_version instanceof ClaimItemVersion) {

				$chls = $item_version->createClaimItemHistoryLogs();

				try {
					if (count($chls)) {
						$processed = ClaimItemVersionTable::PROCESSED_COMPLETED;
						$chls->save();
						echo 'Processed Claim History Log for Claim Item ' . $item_version->getID() . ' version ' .  $item_version->version . "\n";
					}
					else if ($chls instanceof Doctrine_Collection) {
						$processed = ClaimItemVersionTable::PROCESSED_COMPLETED;
						echo 'Processed Claim History Log for Claim Item ' . $item_version->getID() . ' version ' .  $item_version->version . "\n";
					}
					else {
						$processed = ClaimItemVersionTable::PROCESSED_ERRORS;
					}

					$q = Doctrine_Query::create()
						->update('ClaimItemVersion cv')
						->set('cv.processed', '?', $processed)
						->where('cv.id = (?) AND cv.version = (?)', array($item_version->id, $item_version->version))
						->execute();
				}
				catch (Exception $e) {
					echo $e->getMessage() . "\n";
					echo $e->getTraceAsString();
					exit();
				}
			}
		}

	}
}
