<?php

class auditClaimTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'auditClaim';
		$this->briefDescription = 'Builds Claims Item Trail.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('debug', false, sfCommandOption::PARAMETER_OPTIONAL, 'Debugging on?', false),
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();

		$min_status = ClaimVersionTable::PROCESSED_INCOMPLETE;
		if ($options['debug']) {
			$min_status = ClaimVersionTable::PROCESSED_ERRORS;
		}

		$claim_versions = Doctrine_Query::create()
			->from('ClaimVersion cv')
			->addWhere('cv.processed <= (?) OR cv.processed IS NULL', $min_status)
			->limit(50)
			->execute();

		foreach ($claim_versions as $claim_version)
		{
			if ($claim_version instanceof ClaimVersion) {
				$chls = $claim_version->createClaimHistoryLogs();

				try {
//					$connection->beginTransaction();

					if ($chls === false) {
						$processed = ClaimVersionTable::PROCESSED_ERRORS;
					}
					else {
						foreach ($chls as $chl)
						{
							if ($chl instanceof ClaimHistoryLog) {
								$chl->save();
							}
						}
//						$chls->save();
						$processed = ClaimVersionTable::PROCESSED_COMPLETED;
						echo 'Processed Claim History Log for Claim ' . $claim_version->getID() . ' version ' .  $claim_version->version . "\n";
					}

					$q = Doctrine_Query::create()
						->update('ClaimVersion cv')
						->set('cv.processed', '?', $processed)
						->where('cv.id = (?) AND cv.version = (?)', array($claim_version->id, $claim_version->version))
						->execute();

//					$connection->commit();
				}
				catch (Exception $e) {
//					$connection->rollback();
					throw $e;
				}
			}
		}

	}
}
