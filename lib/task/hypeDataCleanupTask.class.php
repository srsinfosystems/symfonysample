<?php

class hypeDataCleanupTask extends sfBaseTask
{
    protected function configure()
    {
        $this->namespace = 'hype';
        $this->name = 'dataCleanup';
        $this->briefDescription = 'Deletes stuff from delete-able tables';

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
        ));
    }
    protected function execute($arguments = array(), $options = array())
    {
        try {
            sfContext::createInstance($this->configuration);
            $this->executeTask($arguments, $options);
        }
        catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
            throw $e;
        }
    }
    private function executeTask($arguments, $options)
    {
        // TODO: hl7_queue
        // TODO: hl7_inbound

        $this->deleteDataExportRequests();
        $this->deleteReminderEvents();
        $this->deleteDataImportRequests();
    }

    private function deleteDataExportRequests()
    {
        $dataExportRequests = Doctrine_Query::create()
            ->from('DataExportRequest de')
            ->addWhere('de.completion_date <= (?)', date(hype::DB_ISO_DATE, strtotime('today - 7 days')))
            ->execute();

        echo 'Deleting ' . $dataExportRequests->count() . ' Data Export Requests' . "\n";
        foreach ($dataExportRequests as $de)
        {
            $de->delete();
        }
    }

    private function deleteReminderEvents()
    {
        $reminders = Doctrine_Query::create()
            ->from('ReminderEvent re')
            ->addWhere('re.status = (?)', array(ReminderEventTable::STATUS_PROCESSED))
            ->addWhere('re.updated_at <= (?)', date(hype::DB_ISO_DATE, strtotime('today - 7 days')))
            ->limit(1000)
            ->execute();

        echo 'Deleting '. $reminders->count() . ' Reminder Events' . "\n";

        foreach ($reminders as $reminder)
        {
            $reminder->delete();
        }
    }

  
    private function deleteDataImportRequests()
    {
        $dataImporters = Doctrine_Query::create()
            ->from('DataImporterFile d')
            ->leftJoin('d.DataImporter di')
            ->addWhere('d.updated_at <= (?)', date(hype::DB_ISO_DATE, strtotime('today - 14 days')))
            ->execute();

        echo 'Deleting ' . $dataImporters->count() . ' Data Importer files' . "\n";
        foreach ($dataImporters as $dataImporter) {
            $dataImporter->DataImporter->delete();
            $dataImporter->delete();
        }
    }
}
