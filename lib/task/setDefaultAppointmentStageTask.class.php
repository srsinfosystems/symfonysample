<?php

class setDefaultAppointmentStageTask extends sfBaseTask
{
	protected function configure()
	{
		$this->namespace = 'hype';
		$this->name = 'setDefaultAppointmentStage';
		$this->briefDescription = 'Updates empty stage_client_ids to the default for that client.';

		$this->addOptions(array(
			new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
			new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
			new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
		));
	}
	protected function execute($arguments = array(), $options = array())
	{
		try {
			sfContext::createInstance($this->configuration);
			$this->executeTask($arguments, $options);
		}
		catch (Exception $e) {
			$this->configuration->getEventDispatcher()->notifyUntil(new sfEvent($e, 'application.throw_exception'));
			throw $e;
		}
	}
	private function executeTask($arguments, $options)
	{
		$databaseManager = new sfDatabaseManager($this->configuration);
		$connection = $databaseManager->getDatabase('doctrine')->getConnection();
		$limit = 1000;

		$total_updates = 0;
		$clients = Doctrine_Query::create()
			->from('Client c')
			->execute();

		foreach ($clients as $client)
		{
			if ($client->stage_default && $total_updates < $limit) {
				$default_stage_id = $client->stage_default;

				$appts = Doctrine_Query::create()
					->from('Appointment a')
					->addWhere('a.client_id = (?)', $client->id)
					->addWhere('a.stage_client_id IS NULL')
					->limit($limit)
					->execute();

				if (count($appts)) {
					echo 'Updating ' . $client->name . ' appointments... ';
					foreach ($appts as $key => $appt)
					{
						$appts[$key]->stage_client_id = $default_stage_id;
					}
					$appts->save();
				}

				$total_updates += count($appts);
				if (count($appts)) {
					echo ' Updated ' . count($appts) . ' records' . "\n";
					echo 'Last appointment ID: ' . $appt->id . "\n";
				}
			}
		}
		echo 'Updated ' . $total_updates . ' total records. ' . "\n\n"
			. 'Run this process again if any records were updated.';

	}
}