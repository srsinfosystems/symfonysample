<?php

class hypeActions extends sfActions
{
	/**
	 * @return sfWebResponse The current sfResponse implementation instance
	 */
	public function getResponse()
	{
		return parent::getResponse();
	}

	public function _nextInClaims()
	{
		$rs = array();
		$appointment_attendees = $this->getUser()->getAttribute('from_appointments', array(), 'ClaimForm'); // an array of appointments to loop through
		$claim_ids = $this->getUser()->getAttribute('from_claims', array(), 'ClaimForm'); // an array of claims to loop through
		$temporary_ids = $this->getUser()->getAttribute('from_temporary', array(), 'ClaimForm'); // an array of temporary claims to loop through
		$patients_ids = $this->getUser()->getAttribute('from_patients', array(), 'ClaimForm');

		if (count($appointment_attendees)) {
			$rs['attendees_count'] = count($appointment_attendees);

			$this->appointment_attendee_id = array_shift($appointment_attendees);
			$this->getUser()->setAttribute('from_appointments', $appointment_attendees, 'ClaimForm');

			$attendee = $this->getAppointmentAttendee($this->appointment_attendee_id);
			$this->claim = $this->getNextClaimInLoop();

			$rs['next_claim'] = ClaimForm::buildWidgetsJSON($this->claim, $attendee, $this->getUser()->getDateFormat());
			$rs['next_claim_information'] = $this->getAppointmentInformation($attendee);
			$rs['doctor_string'] = DoctorTable::getDropdownStringForID($this->claim->doctor_id, $this->getUser()->getDoctorTitleFormat());
		}
		else {
			if (count($claim_ids)) {
				$rs['attendees_count'] = count($claim_ids);

				$claim_id = array_shift($claim_ids);
				$this->getUser()->setAttribute('from_claims', $claim_ids, 'ClaimForm');

				$this->claim = Doctrine_Query::create()
					->from('Claim c')
					->leftJoin('c.ClaimItem ci')
					->leftJoin('c.ClaimDirectPayment cdp')
					->where('c.id = ?', $claim_id)
					->addWhere('c.client_id = ?', $this->getUser()->getClientID())
					->addWhere('ci.status != ?', ClaimItemTable::STATUS_DELETED)
					->addWhere('c.deleted = (?)', false)
					->fetchOne();

				if (!$this->claim instanceof Claim) {
					$this->claim = new Claim();
					$this->setSessionDetails();
					$this->claim->determinePayProgOntario();
				}
				else {
					if ($this->claim->health_num) {
						$this->claim->health_num = hype::decryptHCN($this->claim->health_num);
					}
				}

				$rs['next_claim'] = ClaimForm::buildWidgetsJSON($this->claim, null, $this->getUser()->getDateFormat());
				$rs['next_claim_information'] = $this->claim->id ? $this->claim->id . ' (' . $this->claim->lname . ')' : '';
				$rs['error_codes'] = $this->_buildErrorCodes($this->claim);
				$rs['payments'] = $this->_buildPayments($this->claim);
				$rs['doctor_string'] = DoctorTable::getDropdownStringForID($this->claim->doctor_id, $this->getUser()->getDoctorTitleFormat());
			}
			else {
				if (count($patients_ids)) {
					$rs['attendees_count'] = count($patients_ids);

					$form_id = array_shift($patients_ids);
					$this->getUser()->setAttribute('from_patients', $patients_ids, 'ClaimForm');

					$patient = Doctrine_Query::create()
						->from('Patient p')
						->addWhere('p.id = (?)', $form_id)
						->addWhere('p.client_id = (?)', $this->getUser()->getClientID())
						->addWhere('p.deleted = (?)', false)
						->fetchOne();

					if (!$patient instanceof Patient) {
						$patient = new Patient();
					}
					$this->claim = new Claim();

					$this->claim->client_id = $this->getUser()->getClientID();
					$this->claim->setPatientDetails($patient);

					$this->setSessionDetails();
					$this->claim->determinePayProgOntario();

					$rs['next_claim'] = ClaimForm::buildWidgetsJSON($this->claim, null, $this->getUser()->getDateFormat());
					$rs['next_claim_information'] = $this->claim->id ? $this->claim->id . ' (' . $this->claim->lname . ')' : '';
					$rs['error_codes'] = $this->_buildErrorCodes($this->claim);
					$rs['payments'] = $this->_buildPayments($this->claim);
					$rs['doctor_string'] = DoctorTable::getDropdownStringForID($this->claim->doctor_id, $this->getUser()->getDoctorTitleFormat());

				}

				else {
					if (count($temporary_ids)) {
						$rs['attendees_count'] = count($temporary_ids);

						$form_id = array_shift($temporary_ids);
						$this->getUser()->setAttribute('from_temporary', $temporary_ids, 'ClaimForm');

						$temp_form = Doctrine_Query::create()
							->from('TemporaryForm tf')
							->addWhere('tf.id = (?)', $form_id)
							->addWhere('tf.client_id = (?)', $this->getUser()->getClientID())
							->fetchOne();

						if (!$temp_form instanceof TemporaryForm) {
							$temp_form = new TemporaryForm();
						}
						$this->claim = new Claim();

						$values = $temp_form->getValuesForClaimForm(array('date_format' => $this->getUser()->getDateFormat()));

						$rs['next_claim'] = $values;
						$rs['next_claim_information'] = $temp_form->getMessage() . '';
						$rs['error_codes'] = $this->_buildErrorCodes($this->claim);
						$rs['payments'] = $this->_buildPayments($this->claim);
						$rs['doctor_string'] = DoctorTable::getDropdownStringForID($this->claim->doctor_id, $this->getUser()->getDoctorTitleFormat());
					}
				}
			}
		}

		$rs['hcn_mismatch'] = false;
		if (array_key_exists('next_claim', $rs) && array_key_exists('patient_id', $rs['next_claim'])) {
			$patient = Doctrine_Query::create()
				->from('Patient p')
				->addWhere('p.id = (?)', $rs['next_claim']['patient_id'])
				->addWhere('p.deleted = (?)', false)
				->fetchOne();

			if ($patient instanceof Patient) {
				if ($patient->getHcn() != $rs['next_claim']['health_num']) {
					$rs['hcn_mismatch'] = true;
				}
			}
		}

		return $rs;
	}

	/**
	 * @return hypeUser The current sfUser implementation instance
	 */
	public function getUser()
	{
		return $this->context->getUser();
	}

	private function getNextClaimInLoop()
	{
		$attendee = $this->getAppointmentAttendee($this->appointment_attendee_id);
		if ($attendee instanceof AppointmentAttendee) {
			if ($attendee->claim_id) {
				$this->claim = $attendee->Claim;
			}
			else {
				$this->claim = new Claim();
				$this->claim->setAppointmentDetails($attendee);
				$this->setSessionDetails();
				$this->claim->determinePayProgOntario();
			}
		}

		return $this->claim;
	}

	protected function getAppointmentInformation($attendee)
	{
		return date($this->getUser()->getDateTimeFormat(), hype::parseDateToInt($attendee->Appointment->start_date, hype::DB_ISO_DATE))
		. '-'
		. date($this->getUser()->getTimeFormat(), hype::parseDateToInt($attendee->Appointment->end_date, hype::DB_ISO_DATE))
		. '  '
		. $attendee->patient_fname . ' ' . $attendee->patient_lname
		. ' [' . $attendee->Appointment->AppointmentType->name . ']';
	}

	public function _buildErrorCodes($claim)
	{
		$rs = array();
		$error_codes = array();

		foreach ($claim->ClaimItem as $count => $item) {
			if (strpos($item->getErrors(), ',') !== false) {
				$rs[$count] = explode(',', $item->getErrors() . '');
			}
			else {
				$rs[$count][] = $item->getErrors() . '';
			}

			foreach ($rs[$count] as $error) {
				if ($error) {
					$error_codes[$error] = $error . '';
				}
			}
		}

		if (count($error_codes)) {
			$codes = Doctrine_Query::create()
				->from('ExplCodes e')
				->whereIn('e.expl_code', array_values($error_codes))
				->execute(array(), Doctrine::HYDRATE_ARRAY);

			foreach ($codes as $code) {
				$error_codes[trim($code['expl_code'] . '')] = htmlspecialchars($code['description'] . '');

			}
		}

		return array(
			'item_errors' => $rs,
			'error_codes' => $error_codes
		);
	}

	public function _buildPayments($claim)
	{
		$rs = array();
		$itemRes = array();
		foreach ($claim->ClaimItem as $count => $item) {
			if (floatval($item->fee_paid)) {
				//$rs[$count] = number_format($item->fee_paid, 2, '.', '');
				$rs[$item->id] = number_format($item->fee_paid, 2, '.', '');
			}
			else {
				$rs[$item->id] = '';
				//$rs[$count] = '';
			}
		}
		ksort($rs);
		foreach($rs as $count =>  $r)
		{
			$itemRes[] = $r;
		}
		return $itemRes;
	}

	protected function checkCredentials($c, $msg = 'You do not have permission to access this page')
	{
		if (!$this->getUser()->hasCredential($c)) {
			$this->getUser()->setFlash('notice', sprintf($msg));
			$this->getUser()->setFlash('notice_class', 'error');
			$this->redirect('default/main');
		}
	}

	protected function getFormErrors()
	{
		$this->errors = array();
		if ($this->form->hasGlobalErrors()) {
			$ges = array();
			foreach ($this->form->getErrorSchema()->getGlobalErrors() as $key => $value) {
				$ges[] = $value->getMessage();
			}
			$this->errors['_global_'] = implode('<br />', $ges);
		}
		$this->flattenErrorsArray($this->form->getErrorSchema()->getNamedErrors(), '');
	}

 	protected function flattenErrorsArray($errs, $prefix = '', $keys = array()) {
 		foreach ($errs as $key => $e)
 		{
 			if (!($e instanceof sfValidatorErrorSchema)) {
 				$pre = $prefix ? $prefix . '_' : '';
 				$label = $this->getFormLabel($keys, $key);
 				if (is_numeric($label)) {
 					$this->errors['_global_'] = array_key_exists('_global_', $this->errors) ? $this->errors['_global_'] . '<br />' . $e->getMessage() : $e->getMessage();
 				}
 				else {
 					$this->errors[$pre . $key] = array(
 						'label' => $label,
 						'message' => $e->getMessage(),
 					);
 				}
 			}
 			else {
 				$pre = $prefix ? $prefix . '_' : '';
 				$newkeys = $keys;
 				$newkeys[] = $key;

 				$this->flattenErrorsArray($e, $pre . $key, $newkeys);
 			}
 		}
 	}

 	private function getFormLabel($keys, $key) {
 		$subform = $this->form;
 		$count = null;
 		while (count($keys))
 		{
 			$idx = array_shift($keys);
 			try {
 				$subform = $subform->getEmbeddedForm($idx);
 			}
 			catch (InvalidArgumentException $e) {
 				return $subform[$idx]->renderLabelName() . (is_int($count) ? ' ' . ($count + 1) : '');
 			}

 			if (is_int($idx)) {
 				$count = $idx;
 			}
 		}

 		$labelName = $key;
 		try {
 			$labelName = $subform[$key]->renderLabelName();
 		}
 		catch (InvalidArgumentException $e) {
 			throw new Exception('Widget "' . $key . '" Does Not Exist, Errors Are: ' . $this->form->getErrorSchema()->__toString());
 		}

 		return $labelName . (is_int($count) ? ' ' . ($count + 1) : '');
 	}

	protected function getHL7InboundCounts()
 	{
 		$rs = array(
 			HL7InboundTable::STATUS_UNPROCESSED => array(
 				'status_name' => HL7InboundTable::getStatusString(HL7InboundTable::STATUS_UNPROCESSED, true),
 				'status_description' => HL7InboundTable::getStatusDescription(HL7InboundTable::STATUS_UNPROCESSED),
 				'status_count' => 0,
 			),
 			HL7InboundTable::STATUS_PROCESSED => array(
 				'status_name' => HL7InboundTable::getStatusString(HL7InboundTable::STATUS_PROCESSED, true),
 				'status_description' => HL7InboundTable::getStatusDescription(HL7InboundTable::STATUS_PROCESSED),
 				'status_count' => 0,
 			),
 			HL7InboundTable::STATUS_DUPLICATE => array(
 				'status_name' => HL7InboundTable::getStatusString(HL7InboundTable::STATUS_DUPLICATE, true),
 				'status_description' => HL7InboundTable::getStatusDescription(HL7InboundTable::STATUS_DUPLICATE),
 				'status_count' => 0,
 			),
 			HL7InboundTable::STATUS_FORM_ERROR => array(
 				'status_name' => HL7InboundTable::getStatusString(HL7InboundTable::STATUS_FORM_ERROR, true),
 				'status_description' => HL7InboundTable::getStatusDescription(HL7InboundTable::STATUS_FORM_ERROR),
 				'status_count' => 0,
 			),
 			HL7InboundTable::STATUS_FATAL_ERROR => array(
 				'status_name' => HL7InboundTable::getStatusString(HL7InboundTable::STATUS_FATAL_ERROR, true),
 				'status_description' => HL7InboundTable::getStatusDescription(HL7InboundTable::STATUS_FATAL_ERROR),
 				'status_count' => 0,
 			),
 			'outbound' => array(
 				'status_name' => 'Outbound Messages',
 				'status_description' => 'HL7 Messages we have queued to send to our HL7 interface that are awaiting processing',
 				'status_count' => $this->getHL7OutboundCount()
 			)
 		);

 		$results = Doctrine_Query::create()
 			->select('i.status, count(*) as count')
 			->from('HL7Inbound i')
 			->addWhere('i.client_id = (?)', $this->getUser()->getClientID())
 			->addWhere('i.status != (?)', HL7InboundTable::STATUS_PROCESSING)
 			->addWhere('i.acknowledged = (?)', false)
 			->addWhere('i.updated_at >= (?)', date(hype::DB_DATE_FORMAT, strtotime('today - 30 days')))
 			->groupBy('i.status')
 			->execute(array(), Doctrine::HYDRATE_SCALAR);

 		foreach ($results as $result)
 		{
 			$rs[$result['i_status']]['status_count'] += $result['i_count'];
 		}

 		try {
 			$dbh = new PDO(sfConfig::get('app_hl7_inbound_database_dsn'), sfConfig::get('app_hl7_inbound_database_username'), (!sfConfig::get('app_hl7_inbound_database_password') ? '' : sfConfig::get('app_hl7_inbound_database_password')));
 			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

 			$sql = 'select COUNT(*) as count from HYPE_HL7Data where Inbound = 1 and Processed = 0';
 			$stmt = $dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
 			$stmt->execute();
 			$result = $stmt->fetch(PDO::FETCH_ASSOC);

 			if ($result['count']) {
 				$rs[HL7InboundTable::STATUS_UNPROCESSED]['status_count'] =+ $result['count'];
 			}
 		}
 		catch (PDOException $e) {
 		}

 		if (!count($this->getUser()->getClientHL7InboundPartners())) {
 			unset($rs[HL7InboundTable::STATUS_UNPROCESSED]);
 			unset($rs[HL7InboundTable::STATUS_PROCESSED]);
 			unset($rs[HL7InboundTable::STATUS_DUPLICATE]);
 			unset($rs[HL7InboundTable::STATUS_FORM_ERROR]);
 			unset($rs[HL7InboundTable::STATUS_FATAL_ERROR]);
 		}
 		if (!count($this->getUser()->getClientHL7OutboundPartners())) {
 			unset($rs['outbound']);
 		}
 		return $rs;
 	}

	protected function getHL7OutboundCount()
 	{
 		$results = Doctrine_Query::create()
 			->select('count(*) as count')
 			->from('HL7Queue q')
 			->addWhere('q.client_id = (?)', $this->getUser()->getClientID())
 			->addWhere('q.status = (?)', HL7QueueTable::STATUS_UNPROCESSED)
 			->fetchOne(array(), Doctrine::HYDRATE_SCALAR);
 		return $results['q_count'];
 	}

	protected function getPatient($id)
 	{
 		return Doctrine_Query::create()
 			->select('*,admit_date as admit_dt')
 			->from('Patient p')
 			->where('p.id = ?', $id)
 			->addWhere('p.client_id = ?', $this->getUser()->getClientID())
 			->addWhere('p.deleted = (?)', false)
 			->fetchOne();
 	}

	protected function getPatientFollowUpQuery($locationIDs = array(),$location_search = null)
	{	
		
		$query = Doctrine_Query::create()
			->from('FollowUp f')
			->leftJoin('f.Patient p')
			->addWhere('f.client_id = ?', $this->getUser()->getClientID())
			->addWhere('f.completed = ?', false)
			->addWhere('p.deleted = (?)', false)
			->orderBy('f.due_date desc');

		if($location_search == 'location_search')
		{
			if(count($locationIDs)  == 1)
			{
				$query->addWhere('f.location_id = (?)', $locationIDs[0]);
			}
			else
			{
				$or_string = 'f.location_id IS NULL';
				foreach ($locationIDs as $loc_id)
				{
					$or_string .= ' OR f.location_id = (?)';
				}
				$query->addWhere($or_string, $locationIDs);
			}
			//$query->whereIn('f.location_id',$locationIDs);
		}	
		else
		{
			$or_string = 'f.location_id IS NULL';
			foreach ($locationIDs as $loc_id)
			{
				$or_string .= ' OR f.location_id = (?)';
			}
			$query->addWhere($or_string, $locationIDs);
		}
		// echo $query->getSQLQuery();
		// exit;
		return $query;
	}

    protected function getTemporaryFormClaimQueryFiltered($user_id)
    {
        return Doctrine_Query::create()
            ->from('TemporaryForm tf')
            ->addWhere('tf.client_id = (?)', $this->getUser()->getClientID())
            ->addWhere('tf.status = (?)', TemporaryFormTable::STATUS_INCOMPLETE)
            ->addWhere('tf.updated_by = (?)', $user_id);
    }

	protected function getTemporaryFormClaimQuery()
	{
		return Doctrine_Query::create()
			->from('TemporaryForm tf')
			->addWhere('tf.client_id = (?)', $this->getUser()->getClientID())
			->addWhere('tf.status = (?)', TemporaryFormTable::STATUS_INCOMPLETE)
            ->orderBy('tf.created_by');
	}

	protected function setupLocationPrinterDropdown()
 	{
 		$this->printersList = array();
 		if ($this->getUser()->getCurrentLocID()) {
 			$printers = Doctrine_Query::create()
	 			->select('lp.id, lp.printer_name, lp.hostname')
	 			->from('LocationPrinter lp')
	 			->addWhere('lp.client_id = (?)', $this->getUser()->getClientID())
	 			->addWhere('lp.location_id = (?)', $this->getUser()->getCurrentLocID())
	 			->addWhere('lp.last_seen >= (?)', date(hype::DB_DATE_FORMAT . ' 00:00:00'))
	 			->execute(array(), Doctrine::HYDRATE_ARRAY);

 			$this->printersList = array();
 			foreach ($printers as $printer)
 			{
 				$this->printersList[$printer['id']] = strtoupper($printer['printer_name'] . ' ON ' . $printer['hostname']);
 			}
 		}
 		$this->defaultPrinter = $this->getUser()->getDefaultPrinter();
 	}

	protected function _locationId()
	{
		return $this->getUser()->getLocations();
	}

	protected function getLocationsByOption($option)
	{
		if ($option == 'user_list') {
			return $this->getUser()->getLocations();
		}
		else if ($option == 'all_locations') {
			$locations = $this->getUser()->getLocations();

			if (count($locations)) {
				return $locations;
			}

			$locations = Doctrine_Core::getTable('Location')->getDropdown(
				'id',
				'getShortCode',
				false,
				array('client_id' => $this->getUser()->getClientID(), 'active' => true)
			);

			$this->getUser()->setAttribute('location_ids', array(), 'ClientParameters');
			return $locations;
		}
	}

	/**
	 * @param hypeWebRequest $request
	 * @return FacilityNumSearchForm
	 */
	protected function setUpFacilityNumSearch(hypeWebRequest $request)
	{

		$options = array(
			'actions' => array('edit')
		);
		$defaults = array(
			'size' => 25,
			'sort' => array(),
			'pageNumber' => 0,
		);
		$defaults = $this->getUser()->getFormDefaults('FacilityNumSearchForm', $defaults);

		if ($request->getParameter('facilitySearch', null)) {
			$params = $request->getParameter('facilitySearch', null);
			if (is_array($params)) {
				foreach ($params as $key => $value) {
					$defaults[$key] = $value;
				}
			}
		}

		if ($request->getParameter('size')) {
			$defaults['size'] = $request->getParameter('size');
		}

		$defaults['pageNumber'] = $request->getParameter('pageNumber');
		$sort = $request->getParameter('sort', array());
		if (is_array($sort) && count($sort)) {
			$defaults['sort'] = $sort;
		}
		

		$this->getUser()->setFormDefaults('FacilityNumSearchForm', $defaults, array());

		$form = new FacilityNumSearchForm($defaults, $options, false);
		$form->disableLocalCSRFProtection();
		$form->bind($defaults);
		return $form;
	}

	/**
	 * @return int
	 */
	protected function getFacilityCount()
	{
		return Doctrine_Query::create()
			->from('Facility f')
			->addWhere('f.name is not null')
			->count();
	}

	/**
	 * @return ExplCodeUploadForm
	 */
	protected function setUpExplCodeUploadForm()
	{
		return new ExplCodeUploadForm();
	}

	/**
	 * @return FacilityTypeUploadForm
	 */
	protected function setUpFacilityTypeUploadForm()
	{
		return new FacilityTypeUploadForm();
	}

	/**
	 * @return FacilityUploadForm
	 */
	protected function setUpFacilityNumUploadForm()
	{
		return new FacilityUploadForm();
	}

	/**
	 * @return ServiceCodeUploadForm
	 */
	protected function setUpServiceCodeUploadForm()
	{
		return new ServiceCodeUploadForm();
	}

	/**
	 * @return ServiceCodeSOBUploadForm
	 */
	protected function setUpServiceCodeSOBUploadForm()
	{
		return new ServiceCodeSOBUploadForm();
	}

	/**
	 * @return SpecCodeUploadForm
	 */
	protected function setUpSpecCodeUploadForm()
	{
		return new SpecCodeUploadForm();
	}

	/**
	 * @return ReferringDoctorUploadForm
	 */
	protected function setUpRefDocUploadForm()
	{
		return new ReferringDoctorUploadForm();
	}

    /**
     * @return VPLUploadForm
     */
	protected function setUpVPLUploadForm()
	{
		$options = array(
			'client_id' => $this->getUser()->getClientID()
		);

		return new VPLUploadForm(array(), $options);
	}

    /**
     * @param bool $jsonEncode
     * @return array
     * @throws Doctrine_Query_Exception
     */
    protected function getDataImporterFileData($jsonEncode = true)
    {
        $rs = array();

        $columns = array('id', 'location_name', 'importer_type_str', 'document_name', 'status_str', 'queue_place', 'total_record_count', 'new_record_count', 'error_count', 'update_count', 'acknowledged', 'actions');

        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_HYPE_ADMINISTRATOR)) {
            $columns[] = 'client_name';
        }

        $q = Doctrine_Query::create()
            ->from('DataImporterFile i')
            ->leftJoin('i.Location l');

        if ($this->getUser()->hasCredential(hype::PERMISSION_NAME_HYPE_ADMINISTRATOR)) {
            $q->leftJoin('i.Client c');
        }
        else {
            $q->addWhere('i.client_id = (?)', array($this->getUser()->getClientID()));
        }

        $importers = $q->execute();

        foreach ($importers as $importer) {
            if ($jsonEncode) {
                $rs[] = json_encode($importer->getGenericJSON($columns, array()));
            }
            else {
                $rs[] = $importer->getGenericJSON($columns, array());
            }
        }

        return $rs;
    }
}
