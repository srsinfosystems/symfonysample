<?php

abstract class hypeTXTFileImporterBase extends hypeFileImporterBase
{
    public function run()
    {
        $this->log('hypeTXTFileImporterBase.run() called');
        $this->log('File status is ' . $this->dataImporterFile->getStatusText());

        if ($this->dataImporterFile->getStatus() == DataImporterFileTable::FILE_STATUS_UPLOADED) {
            $importInformation = array(
                'parsingLine' => 1,
                'errors' => "",
            );

            $this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_PARSING_RECORDS);
            $this->dataImporterFile->setImportInformation(json_encode($importInformation));
            $this->dataImporterFile->save();

            $this->importRecords($importInformation);
        }

        else {
            if ($this->dataImporterFile->getStatus() == DataImporterFileTable::FILE_STATUS_PARSING_RECORDS) {
                $importInformation = json_decode($this->dataImporterFile->getImportInformation(), true);
                $this->importRecords($importInformation);
            }

            else {
                if ($this->dataImporterFile->getStatus() == DataImporterFileTable::FILE_STATUS_RECORDS_IMPORTED) {
                    $this->parseDataImporterRecord();
                }
            }
        }
    }

    protected function importRecords($importInformation)
    {
        $this->log('hypeTXTFileImporterBase.importRecords() called');

        $textFile = $this->openTextFileForReading();
        $emptyLinesCount = 0;

        do {
            $hasData = $this->importLineIntoDataImporter($importInformation['parsingLine'], $textFile);

            if ($hasData) {
                $emptyLinesCount = 0;
            }
            else {
                $emptyLinesCount++;
            }
            $importInformation['parsingLine']++;

            $this->dataImporterFile->setImportInformation(json_encode($importInformation));
            $this->dataImporterFile->save();

        } while ($emptyLinesCount <= 3);

        $this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_RECORDS_IMPORTED);
        $this->dataImporterFile->save();
    }

    private function openTextFileForReading()
    {
        $this->log('hypeTXTFileImporterBase.openTextFileForReading() called');

        $filename = ini_get('upload_tmp_dir') . '/' . time() . '.txt';
        file_put_contents($filename, $this->dataImporterFile->getDocument());

        $file = file($filename);

        return $file;
    }

    protected function importLineIntoDataImporter($lineNum, $file)
    {
        $this->log('hypeTXTFileImporterBase.importLineIntoDataImporter(' . $lineNum . ') called');

        if (array_key_exists($lineNum - 1, $file)) {

            if (trim($file[$lineNum - 1])) {
                $dataImporter = new DataImporter();
                $dataImporter->DataImporterFile = $this->dataImporterFile;
                $dataImporter->importer_name = $this->dataImporterFile->getDocumentName();
                $dataImporter->status = DataImporterTable::STATUS_UNPARSED;
                $dataImporter->line_number = $lineNum;
                $dataImporter->setDataText($file[$lineNum - 1]);

                $dataImporter->save();
                $this->dataImporterFile->setTotalRecordCount($this->dataImporterFile->getTotalRecordCount() + 1);

                return true;
            }
        }

        return false;
    }

    abstract protected function parseDataImporterRecord();
}