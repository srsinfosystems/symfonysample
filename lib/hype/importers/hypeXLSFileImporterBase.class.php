<?php

abstract class hypeXLSFileImporterBase extends hypeFileImporterBase
{
    public function run()
    {
        $this->log('hypeXLSFileImporterBase.run() called');
        $this->log('File status is ' . $this->dataImporterFile->getStatusText());

        if ($this->dataImporterFile->getStatus() == DataImporterFileTable::FILE_STATUS_UPLOADED) {
            $importInformation = array(
                'parsingLine' => 1,
                'errors' => "",
            );

            $this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_PARSING_RECORDS);
            $this->dataImporterFile->setImportInformation(json_encode($importInformation));
            $this->dataImporterFile->save();

            $this->importSpreadsheetRecords($importInformation);
        }

        else if ($this->dataImporterFile->getStatus() == DataImporterFileTable::FILE_STATUS_PARSING_RECORDS) {
            $importInformation = json_decode($this->dataImporterFile->getImportInformation(), true);
            $this->importSpreadsheetRecords($importInformation);
        }

        else if ($this->dataImporterFile->getStatus() == DataImporterFileTable::FILE_STATUS_RECORDS_IMPORTED) {
            $this->parseDataImporterRecord();
        }
    }

    protected function importSpreadsheetRecords($importInformation)
    {
        $this->log('hypeXLSFileImporterBase.importSpreadsheetRecords() called');

        $spreadsheet = $this->openSpreadsheetForReading();
        $emptyLinesCount = 0;

        do {
            $this->resetLog();
            $hasData = $this->importSpreadsheetRowIntoDataImporter($importInformation['parsingLine'], $spreadsheet);

            if ($hasData) {
                $emptyLinesCount = 0;
            }
            else {
                $emptyLinesCount++;
            }
            $importInformation['parsingLine']++;

            $this->dataImporterFile->setImportInformation(json_encode($importInformation));
            $this->dataImporterFile->save();
        } while($emptyLinesCount <= 3);

        $this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_RECORDS_IMPORTED);
        $this->dataImporterFile->save();
    }

    protected function openSpreadsheetForReading()
    {
        $this->log('hypeXLSFileImporterBase.openSpreadsheetForReading() called');

        $filename = ini_get('upload_tmp_dir') . '/' . time() . '.xls';
        file_put_contents($filename, $this->dataImporterFile->getDocument());

        $inputFileType = PHPExcel_IOFactory::identify($filename);

        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);

        $objPHPExcel = $objReader->load($filename);
        $objPHPExcel->setActiveSheetIndex(0);

        $sheet = $objPHPExcel->getActiveSheet();
        return $sheet;
    }

    abstract protected function importSpreadsheetRowIntoDataImporter($lineNum, PHPExcel_Worksheet $sheet);

    abstract protected function parseDataImporterRecord();

}