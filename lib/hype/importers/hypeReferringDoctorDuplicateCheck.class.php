<?php

class hypeReferringDoctorDuplicateCheck extends hypeFileImporterBase
{
 	public function run()
 	{
 		$this->log('hypeReferringDoctorDuplicateCheck.run() called');
 		$this->log('File status is ' . $this->dataImporterFile->getStatusText());

  		if ($this->dataImporterFile->getStatus() == DataImporterFileTable::FILE_STATUS_INITIAILIZING) {
 			$importInformation = array(
 				'minimumID' => 1,
 				'errors' => "",
 			);

  			$this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_BUILDING_RECORDS);
  			$this->dataImporterFile->setImportInformation(json_encode($importInformation));
  			$this->dataImporterFile->save();

  			$this->buildDupCheckRecord($importInformation);
  		}

  		else if ($this->dataImporterFile->getStatus() == DataImporterFileTable::FILE_STATUS_BUILDING_RECORDS) {
  			$importInformation = json_decode($this->dataImporterFile->getImportInformation(), true);
  			$this->buildDupCheckRecord($importInformation);

  		}

  		else if ($this->dataImporterFile->getStatus() == DataImporterFileTable::FILE_STATUS_CHECKING_FOR_DUPS) {
			$this->parseDataImporterRecord();
  		}
 	}

 	private function buildDupCheckRecord($data)
 	{
 		$this->log('hypeReferringDoctorDuplicateCheck.buildDupCheckRecord() called');

 		do {
 			$ids = Doctrine_Query::create()
 				->select('rd.id')
 				->from('ReferringDoctor rd')
 				->addWhere('rd.id >= (?)', $data['minimumID'])
 				->addWhere('rd.client_id = (?)', $this->dataImporterFile->getClientID())
 				->orderBy('rd.id asc')
 				->limit(100)
 				->execute(array(), Doctrine::HYDRATE_SCALAR);

 			foreach ($ids as $id)
 			{
 				$this->buildDataImporterRecord($id['rd_id']);

				$data['minimumID'] = $id['rd_id'] + 1;

				$this->dataImporterFile->setImportInformation(json_encode($data));
				$this->dataImporterFile->save();
 			}

 		} while (count($ids));

 		if (!count($ids)) {
 			$this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_CHECKING_FOR_DUPS);
 			$this->dataImporterFile->save();
 		}
 	}
 	private function buildDataImporterRecord($id)
 	{
 		$this->log('hypeReferringDoctorDuplicateCheck.buildDataImporterRecord() called');

 		$dataImporter = new DataImporter();

 		$dataImporter->DataImporterFile = $this->dataImporterFile;
 		$dataImporter->importer_name = $this->dataImporterFile->getDocumentName();
 		$dataImporter->status = DataImporterTable::STATUS_UNPARSED;
 		$dataImporter->line_number = $id;
 		$dataImporter->setDataText(json_encode(array('id' => $id)));
	    $this->dataImporterFile->setTotalRecordCount($this->dataImporterFile->getTotalRecordCount() + 1);

 		$dataImporter->save();
 	}
 	private function parseDataImporterRecord()
 	{
 		$this->log('hypeReferringDoctorDuplicateCheck.parseDataImporterRecord() called');

 		do {
		    $dataImporter = $this->getDataImporter($this->dataImporterFile->get('id'));

 			if ($dataImporter instanceof DataImporter) {
		 		$dataImporter->setStatus(DataImporterTable::STATUS_PARSING);
		 		$dataImporter->save();

 				$referringDoctor = Doctrine_Query::create()
 					->from('ReferringDoctor rd')
 					->addWhere('rd.id = (?)', $dataImporter->getLineNumber())
 					->limit(1)
 					->fetchOne();

 				if ($referringDoctor instanceof ReferringDoctor) {
					$referringDoctor->checkForDuplicates();
					$referringDoctor->save();
 				}

 				$dataImporter->setStatus(DataImporterTable::STATUS_COMPLETED);
 				$dataImporter->save();
 			}
 		} while ($dataImporter instanceof DataImporter);

 		$this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_COMPLETED);
 		$this->dataImporterFile->save();
 	}
}

