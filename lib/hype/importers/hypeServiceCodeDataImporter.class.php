<?php

class hypeServiceCodeDataImporter extends hypeXLSFileImporterBase
{
	protected function parseDataImporterRecord()
	{
		$this->log('hypeServiceCodeDataImporter.parseDataImporterRecord() called');

		do {
			$dataImporter = $this->getDataImporter($this->dataImporterFile->get('id'));

			if ($dataImporter instanceof DataImporter) {
				$this->turnDataImporterIntoServiceCode($dataImporter);
			}

		} while ($dataImporter instanceof DataImporter);


		$this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_COMPLETED);
		$this->dataImporterFile->save();
	}

	private function turnDataImporterIntoServiceCode(DataImporter $dataImporter)
	{
		$this->resetLog();
		$this->log('hypeServiceCodeDataImporter.turnDataImporterIntoServiceCode() called');

		$dataImporter->setStatus(DataImporterTable::STATUS_PARSING);
		$dataImporter->save();

		$data = json_decode($dataImporter->getDataText(), true);

		$name = strtoupper(iconv("UTF-8", "UTF-8//IGNORE", $data['name']));
		$description = iconv("UTF-8", "UTF-8//IGNORE", $data['description']);

		$serviceCode = $this->getServiceCode($name);

        if ($serviceCode instanceof ServiceCode) {
            if ($description != '') {
                $serviceCode->setDescription(strtoupper($description));
            }

            $dataImporter->setStatus(DataImporterTable::STATUS_COMPLETED);
	        $this->dataImporterFile->setUpdateCount($this->dataImporterFile->getUpdateCount() + 1);
        }
        else {
            $dataImporter->setStatus(DataImporterTable::STATUS_NO_RECORD);
	        $this->dataImporterFile->setErrorCount($this->dataImporterFile->getErrorCount() + 1);
        }

		try {
			$con = Doctrine_Manager::getInstance()->getConnectionForComponent('ServiceCode');
			$con->beginTransaction();

            if ($serviceCode instanceof ServiceCode) {
                $serviceCode->save();

	            $dataImporter->setRecordType('ServiceCode');
	            $dataImporter->setRecordId($serviceCode->get('id'));
            }

			$dataImporter->appendLog($this->getLog());
			$dataImporter->save();

            $this->dataImporterFile->save();

			$con->commit();
		}
		catch (Exception $e) {
			$con->rollback();
			$importInformation = json_decode($this->dataImporterFile->getImportInformation(), true);
			if (strlen($importInformation['errors'])) {
				$importInformation['errors'] .= "\r\n";
			}
			$importInformation['errors'] .= iconv("UTF-8", "UTF-8//IGNORE", 'Line ' . $dataImporter->getLineNumber() . ': ' . $e->getMessage());

			$this->dataImporterFile->setImportInformation(json_encode($importInformation));
			$this->dataImporterFile->setErrorCount($this->dataImporterFile->getErrorCount() + 1);
			$this->dataImporterFile->save();

			$dataImporter->setStatus(DataImporterTable::STATUS_ERROR);
			$dataImporter->save();

			throw $e;
		}
	}

	private function getServiceCode($name)
	{
		$serviceCode = Doctrine_Query::create()
			->from('ServiceCode sc')
			->addWhere('sc.service_code = (?)', $name)
			->limit(1)
			->fetchOne();

		return $serviceCode;
	}
	protected function importSpreadsheetRowIntoDataImporter($lineNum, PHPExcel_Worksheet $sheet)
	{
		$this->log('hypeServiceCodeDataImporter.importSpreadsheetRowIntoDataImporter() called');

		$name = $sheet->getCell('A' . $lineNum)->getValue();
		$description = $sheet->getCell('B' . $lineNum)->getValue();

		if ($name || $description) {
			$dataImporter = new DataImporter();

			$dataImporter->DataImporterFile = $this->dataImporterFile;
			$dataImporter->importer_name = $this->dataImporterFile->getDocumentName();
			$dataImporter->status = DataImporterTable::STATUS_UNPARSED;
			$dataImporter->line_number = $lineNum;
			$dataImporter->setDataText(json_encode(array('name' => $name, 'description' => $description)));
			$dataImporter->save();

			$this->dataImporterFile->setTotalRecordCount($this->dataImporterFile->getTotalRecordCount() + 1);
			return true;
		}
		return false;
	}

}

