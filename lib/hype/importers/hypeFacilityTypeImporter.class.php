<?php

class hypeFacilityTypeImporter extends hypeXLSFileImporterBase
{
	protected function parseDataImporterRecord()
	{
		$this->log('hypeFacilityTypeImporter.parseDataImporterRecord() called');

		do {
			$dataImporter = $this->getDataImporter($this->dataImporterFile->get('id'));

			if ($dataImporter instanceof DataImporter) {
				$this->turnDataImporterIntoFacilityType($dataImporter);
			}

		} while ($dataImporter instanceof DataImporter);


		$this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_COMPLETED);
		$this->dataImporterFile->save();
	}
	private function turnDataImporterIntoFacilityType(DataImporter $dataImporter)
	{
		$this->resetLog();
		$this->log('hypeFacilityTypeImporter.turnDataImporterIntoFacilityType() called');

		$dataImporter->setStatus(DataImporterTable::STATUS_PARSING);
		$dataImporter->save();

		$data = json_decode($dataImporter->getDataText(), true);

		$name = strtoupper(iconv("UTF-8", "UTF-8//IGNORE", $data['name']));
		$code = iconv("UTF-8", "UTF-8//IGNORE", $data['code']);

		$facilityType = $this->getFacilityType($code);
		$facilityType->setCode($code);
		$facilityType->setName($name);

		if ($facilityType->isNew()) {
			$this->dataImporterFile->setNewRecordCount($this->dataImporterFile->getNewRecordCount() + 1);
		}
		else {
			$this->dataImporterFile->setUpdateCount($this->dataImporterFile->getUpdateCount() + 1);
		}

		$dataImporter->setStatus(DataImporterTable::STATUS_COMPLETED);
		$dataImporter->appendLog($this->getLog());

		try {
			$con = Doctrine_Manager::getInstance()->getConnectionForComponent('FacilityType');
			$con->beginTransaction();

			$facilityType->save();

			$dataImporter->setRecordType('FacilityType');
			$dataImporter->setRecordId($facilityType->get('id'));
			$dataImporter->save();

            $this->dataImporterFile->save();
			$con->commit();
		}
		catch (Exception $e) {
			$con->rollback();
			$importInformation = json_decode($this->dataImporterFile->getImportInformation(), true);
			if (strlen($importInformation['errors'])) {
				$importInformation['errors'] .= "\r\n";
			}
			$importInformation['errors'] .= iconv("UTF-8", "UTF-8//IGNORE", 'Line ' . $dataImporter->getLineNumber() . ': ' . $e->getMessage());

			$this->dataImporterFile->setImportInformation(json_encode($importInformation));
			$this->dataImporterFile->setErrorCount($this->dataImporterFile->getErrorCount() + 1);
			$this->dataImporterFile->save();

			$dataImporter->setStatus(DataImporterTable::STATUS_ERROR);
			$dataImporter->save();

			throw $e;
		}
	}

	private function getFacilityType($code)
	{
		$this->log('hypeFacilityTypeImporter.getFacilityType(' . $code . ') called');
		$facilityType = Doctrine_Query::create()
			->from('FacilityType f')
			->addWhere('f.code = (?)', array($code))
			->limit(1)
			->fetchOne();

		if (!$facilityType instanceof FacilityType) {
			$facilityType = new FacilityType();

			$this->log('creating new Facility Type');
		}
		else {
			$this->log('updating existing Facility Type');
		}

		return $facilityType;
	}

	protected function importSpreadsheetRowIntoDataImporter($lineNum, PHPExcel_Worksheet $sheet)
	{
		$this->log('hypeFacilityTypeImporter.importSpreadsheetRowIntoDataImporter() called');

		$code = $sheet->getCell('A' . $lineNum)->getValue();
		$name = $sheet->getCell('B' . $lineNum)->getValue();

		if ($code && $name) {
			$dataImporter = new DataImporter();

			$dataImporter->DataImporterFile = $this->dataImporterFile;
			$dataImporter->importer_name = $this->dataImporterFile->getDocumentName();
			$dataImporter->status = DataImporterTable::STATUS_UNPARSED;
			$dataImporter->line_number = $lineNum;
			$dataImporter->setDataText(json_encode(array('code' => $code, 'name' => $name)));
			$dataImporter->save();

			$this->dataImporterFile->setTotalRecordCount($this->dataImporterFile->getTotalRecordCount() + 1);
			return true;
		}
		return false;
	}

}

