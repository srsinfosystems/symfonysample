<?php

class hypeFacilityNumberImporter extends hypeXLSFileImporterBase
{
    protected function importSpreadsheetRowIntoDataImporter($lineNum, PHPExcel_Worksheet $sheet)
    {
        $this->log('hypeFacilityNumberImporter.importSpreadsheetRowIntoDataImporter() called');

        $data = array(
            'LHIN' => $sheet->getCell('A' . $lineNum)->getValue(),
            'city' => $sheet->getCell('B' . $lineNum)->getValue(),
            'name' => $sheet->getCell('C' . $lineNum)->getValue(),
            'facilityType' => $sheet->getCell('D' . $lineNum)->getValue(),
            'masterNumber' => $sheet->getCell('E' . $lineNum)->getValue(),
            'facilityNumber' => $sheet->getCell('F' . $lineNum)->getValue(),
        );

        if ($data['masterNumber'] && $data['name']) {
			$dataImporter = new DataImporter();

			$dataImporter->DataImporterFile = $this->dataImporterFile;
			$dataImporter->importer_name = $this->dataImporterFile->getDocumentName();
			$dataImporter->status = DataImporterTable::STATUS_UNPARSED;
			$dataImporter->line_number = $lineNum;
			$dataImporter->setDataText(json_encode($data));
			$dataImporter->save();

            $this->dataImporterFile->setTotalRecordCount($this->dataImporterFile->getTotalRecordCount() + 1);
			return true;
        }
		return false;
    }

    protected function parseDataImporterRecord()
    {
        $this->log('hypeFacilityNumberImporter.parseDataImporterRecord() called');

		do {
			$dataImporter = $this->getDataImporter($this->dataImporterFile->get('id'));

			if ($dataImporter instanceof DataImporter) {
				$this->turnDataImporterIntoFacility($dataImporter);
			}
		} while ($dataImporter instanceof DataImporter);

		$this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_COMPLETED);
		$this->dataImporterFile->save();
    }

    private function turnDataImporterIntoFacility(DataImporter $dataImporter)
    {
        $this->resetLog();
        $this->log('hypeFacilityNumberImporter.turnDataImporterIntoFacility() called');

		$dataImporter->setStatus(DataImporterTable::STATUS_PARSING);
		$dataImporter->save();

		$data = json_decode($dataImporter->getDataText(), true);

        $facility = $this->getFacility($data['masterNumber']);
        $facility->setLhin($this->formatText($data['LHIN']));
        $facility->setCity($this->formatText($data['city']));
        $facility->setName($this->formatText($data['name']));
        $facility->setFacilityType($this->formatText($data['facilityType']));
        $facility->setFacilityTypeName(null); // this will complete in preSave
        $facility->setFacilityMasterNumber($this->formatText($data['masterNumber']));
        $facility->setFacilityNumber($this->formatText($data['facilityNumber']));

        if ($facility->isNew()) {
            $this->dataImporterFile->setNewRecordCount($this->dataImporterFile->getNewRecordCount() + 1);
        }
        else {
            $this->dataImporterFile->setUpdateCount($this->dataImporterFile->getUpdateCount() + 1);
        }

        $dataImporter->setStatus(DataImporterTable::STATUS_COMPLETED);
        $dataImporter->appendLog($this->getLog());

		try {
			$con = Doctrine_Manager::getInstance()->getConnectionForComponent('SpecCode');
			$con->beginTransaction();

            $facility->save();

            $dataImporter->setRecordType('Facility');
            $dataImporter->setRecordId($facility->get('id'));
			$dataImporter->save();

            $this->dataImporterFile->save();
			$con->commit();
		}
		catch (Exception $e) {
			$con->rollback();
			$importInformation = json_decode($this->dataImporterFile->getImportInformation(), true);
			if (strlen($importInformation['errors'])) {
				$importInformation['errors'] .= "\r\n";
			}
			$importInformation['errors'] .= $this->formatText('Line ' . $dataImporter->getLineNumber() . ': ' . $e->getMessage());

			$this->dataImporterFile->setImportInformation(json_encode($importInformation));
            $this->dataImporterFile->setErrorCount($this->dataImporterFile->getErrorCount() + 1);
			$this->dataImporterFile->save();

			$dataImporter->setStatus(DataImporterTable::STATUS_ERROR);
			$dataImporter->save();

			throw $e;
		}
    }

    private function getFacility($masterNumber)
    {
        $facility = Doctrine_Query::create()
            ->from('Facility')
            ->addWhere('facility_master_number = (?)', array($masterNumber))
            ->limit(1)
            ->fetchOne();

        if (!$facility instanceof Facility) {
            $facility = new Facility();
        }
        return $facility;
    }
}

