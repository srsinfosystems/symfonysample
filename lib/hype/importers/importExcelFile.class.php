<?php

require_once('../lib/vendor/phpExcel/Classes/PHPExcel.php');

class importExcelFiles {

    private $debug = false;
    private $client_id;
    private $doctors;
    private $location_id;
    private $locations;
    private $file;
    private $sheet;
    private $headers = array();
    private $headersRowIndex = 1;
    private $totalSkippedClaim1 = 0;
    private $totalSkippedClaim2 = 0;
    private $claim1Count = 0;
    private $claim2Count = 0;
    private $count = 0;
    private $saveTemp = 0;

    public function __construct($file, $client_id, $location_id, $doctor_id = null) {
        date_default_timezone_set('America/Toronto');

        $this->client_id = $client_id;
        $this->location_id = $location_id;
        $this->file = $file;
    }

    public function getCount() {
        return $this->count;
    }

    public function getDebug() {
        return $this->debug;
    }

    public function setDebug($val) {
        $this->debug = $val;
    }

    public function getMessage() {
        $s = 'Process Completed successfully. ';

        if ($this->claim1Count) {
            $s .= $this->claim1Count . ' Prof Claims created. ';
        }
        if ($this->claim2Count) {
            $s .= $this->claim2Count . ' Tech Claims created. ';
        }
        if ($this->totalSkippedClaim1) {
            $s .= $this->totalSkippedClaim1 . ' Prof Claims not processed. ';
        }
        if ($this->totalSkippedClaim2) {
            $s .= $this->totalSkippedClaim2 . ' Tech Claim not processed. ';
        }
        if ($this->saveTemp) {
            $s .= $this->saveTemp . ' Claims were saved as temp claims';
        }
        return $s;
    }

    public function run() {
        $this->openExcelSheetForReading();
        if (!$this->sheet instanceof PHPExcel_Worksheet) {
            throw new Exception('Unable to process file as an Excel Workbook');
        }

        $this->findHeaders();
        $this->getDoctors();
        $this->getLocations();

        $rowNum = $this->headersRowIndex + 1;

        $patient_type_id = $this->getPatientTypeID();
        $patient_status_id = $this->getPatientStatusID();

        $consecutiveEmptyRowCount = 0;

        do {
            if ($this->sheet->getCell('A' . $rowNum)->getValue()) {
                $consecutiveEmptyRowCount = 0;

                $claim1 = null;
                $claim2 = null;

                $createClaim1 = true;
                $createClaim2 = true;

                $saveTemporary1 = true;
                $saveTemporary2 = true;

                $hcn = '';

                $data = $this->populateData($rowNum);

                if ($data['facility'] == 'Burlington') {
                    $data['technical_group'] = 'AG23';
                } else if ($data['facility'] == 'Fairview') {
                    $data['technical_group'] = 'AN73';
                }

                if ($data['hcn']) {
                    $hcn = hype::encryptHCN($data['hcn']);
                } else {
                    $createClaim1 = false;
                    $createClaim2 = false;
                    $this->log('No HCN found, claims will not be created');
                }

                /** Patient * */
                $patient = $this->getPatient($hcn, $this->client_id);
                if ($patient instanceof Patient) {
                    $this->log('Patient ' . $patient->getID() . ' already exist');
                } else {
                    $patient = new Patient();
                    $this->log('New Patient Created');
                }
                $patient = $this->updatePatient($patient, $data, $hcn, $patient_type_id, $patient_status_id);
                $patient->save();

                /** Claim 1 * */
                if ($data['prof_code']) {
                    $claim1 = $this->buildClaim1($data, $patient);
                } else {
                    $createClaim1 = false;
                    $saveTemporary1 = false;
                    $this->log('No Professional Service Code, Claim 1 will not be created');
                }

                if ($claim1 instanceof Claim) {
                    if ($this->checkClaim($claim1->ClaimItem[0])) {
                        $createClaim1 = false;
                        $saveTemporary1 = false;
                    } else if (!$claim1->location_id) {
                        echo 'Claim 1' . "\n";
                        print_r($data);
                        print_r($claim1->toArray());
                        exit();
                    }
                }

                /** Claim 2 * */
                if (array_key_exists('tech_code', $data) && $data['tech_code']) {
                    $claim2 = $this->buildClaim2($data, $patient);
                } else {
                    $createClaim2 = false;
                    $saveTemporary2 = false;
                    $this->log('No Technical Service Code, Claim 2 will not be created');
                }

                if (!$data['technical_group']) {
                    $createClaim2 = false;
                    $this->log('No Technical Group specified. Claim 2 will not be created');
                }

                if ($createClaim2 && $claim2 instanceof Claim && $this->checkClaim($claim2->ClaimItem[0])) {
                    $createClaim2 = false;
                    $saveTemporary2 = false;
                } else if ($createClaim2 && $claim2 instanceof Claim && !$claim2->location_id) {
                    echo 'Claim 2' . "\n";
                    print_r($data);
                    print_r($claim2->toArray());
                    exit();
                }


                /** Save Claim 1 * */
                if ($createClaim1) {
                    $claim1->save();
                    $this->log('Claim 1 for patient -> ' . $patient->getID() . ' saved');
                } else {
                    $this->log('No service code for Claim 1 or Claim already exist. Ignoring it.');
                    $this->totalSkippedClaim1++;

                    if ($saveTemporary1) {
                        $temporaryForm = TemporaryFormTable::createHL7Claim($claim1);
                        $this->saveTemp++;
                    }
                }

                /** Save Claim 2 * */
                if ($createClaim2) {
                    $claim2->save();
                    $this->log('Claim 2 for patient -> ' . $patient->getID() . ' saved');
                } else {
                    $this->log('No service code for Claim 2 or Claim already exist. Ignoring it.');
                    $this->totalSkippedClaim2++;

                    if ($claim2 instanceof Claim && $saveTemporary2) {
                        $temporaryForm = TemporaryFormTable::createHL7Claim($claim2);
                        $this->saveTemp++;
                    }
                }
            } else {
                $consecutiveEmptyRowCount++;
            }

            $this->count++;
            $rowNum++;
        } while ($consecutiveEmptyRowCount < 3);

        $this->log($this->totalSkippedClaim1 . ' claims of the professional groups were not saved');
        $this->log($this->totalSkippedClaim2 . ' claims of the technical groups were not saved');
    }

    private function openExcelSheetForReading() {
        ini_set("memory_limit", "1024M");
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true);
        $excel = $objReader->load($this->file);

        //Set the active page the one that actually have the data that we need
        $excel->setActiveSheetIndex(0);
        $this->sheet = $excel->getActiveSheet();
    }

    private function findHeaders() {
        if ($this->sheet instanceof PHPExcel_Worksheet) {
            $rows = $this->sheet->getRowIterator();
            $rowNum = 1;
            $headersFound = false;
            $headRow = null;

            foreach ($rows as $row) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                foreach ($cellIterator as $cell) {
                    if (stristr($cell->getValue(), 'patient name') != null) {
                        $this->headersRowIndex = $rowNum;
                        $headersFound = true;
                        $headRow = $row;
                        break;
                    }
                }
                if ($headersFound) {
                    break;
                }
                $rowNum++;
            }

            $cellIterator = $headRow->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            foreach ($cellIterator as $cell) {
                $value = strtolower($cell->getValue());

                if (strstr($value, '(last)') !== false) {
                    $this->headers['lname'] = $cell->getColumn();
                } else if (strstr($value, '(first)') !== false) {
                    $this->headers['fname'] = $cell->getColumn();
                } else if (strstr($value, 'name') !== false) {
                    $this->headers['name'] = $cell->getColumn();
                } else if (strstr($value, 'birth') !== false) {
                    $this->headers['dob'] = $cell->getColumn();
                } else if (strstr($value, 'sex') !== false) {
                    $this->headers['sex'] = $cell->getColumn();
                } else if (strstr($value, 'phone') != false) {
                    $this->headers['phone'] = $cell->getColumn();
                } else if (strstr($value, 'version') != false) {
                    $this->headers['version_code'] = $cell->getColumn();
                } else if (strstr($value, 'number') != false) {
                    $this->headers['hcn'] = $cell->getColumn();
                } else if (strstr($value, 'date') != false) {
                    $this->headers['service_date'] = $cell->getColumn();
                } else if (strstr($value, 'professional') != false) {
                    $this->headers['prof_code'] = $cell->getColumn();
                } else if (strstr($value, 'technical') != false) {
                    $this->headers['tech_code'] = $cell->getColumn();
                } else if (strstr($value, 'referring') != false) {
                    $this->headers['ref_doc'] = $cell->getColumn();
                } else if (strstr($value, 'reading') != false || strstr($value, 'reporting') != false) {
                    $this->headers['doctor'] = $cell->getColumn();
                } else if (strstr($value, 'facility') != false) {
                    $this->headers['facility'] = $cell->getColumn();
                } else if (strstr($value, 'method') != false || strstr($value, 'province') != false) {
                    $this->headers['province'] = $cell->getColumn();
                } else if ($value == 'side or level') {
                    $this->headers['num_serv'] = $cell->getColumn();
                }
            }
        }
    }

    private function getDoctors() {
        $this->doctors = Doctrine_Query::create()
                ->from('Doctor d')
                ->addWhere('d.client_id = (?)', $this->client_id)
                ->execute();
    }

    private function getLocations() {
        $locs = Doctrine_Query::create()
                ->from('Location l')
                ->addWhere('client_id = (?)', $this->client_id)
                ->execute();

        $this->locations = array();
        foreach ($locs as $l) {
            if ($l instanceof Location) {
                $this->locations[$l->code] = $l;
            }
        }
    }

    private function getPatientTypeID() {
        $rv = null;

        $cp = ClientParamTable::getParamForClient($this->client_id, 'default_patient_type_id');
        if ($cp instanceof ClientParam) {
            $rv = $cp->value;
        }

        return $rv;
    }

    private function getPatientStatusID() {
        $rv = null;

        $cp = ClientParamTable::getParamForClient($this->client_id, 'patient_status_default');
        if ($cp instanceof ClientParam) {
            $rv = $cp->value;
        }

        return $rv;
    }

    private function populateData($rowNum) {
        $data = array();
        $data['technical_group'] = null;
        $data['professional_group'] = '0000';

        /** Patient Name * */
        if (array_key_exists('name', $this->headers)) {
            $name = explode(',', $this->sheet->getCell($this->headers['name'] . $rowNum)->getValue());
            $data['lname'] = $name[0];
            $data['fname'] = $name[1];
        } else {
            $data['lname'] = $this->sheet->getCell($this->headers['lname'] . $rowNum)->getValue();
            $data['fname'] = $this->sheet->getCell($this->headers['fname'] . $rowNum)->getValue();
        }

        // TODO: this doesn't appear to be used anywhere
        //$data['name'] = $data['fname'] . ' ' . $data['lname'];

        /** Patient DOB * */
        $dob = $this->sheet->getCell($this->headers['dob'] . $rowNum)->getFormattedValue();
        if (strlen($dob) > 7) {
            $date = str_replace(" ", "/", $dob);
            $dateParts = explode('/', $date);

            if (strlen($dateParts[1]) == 3) {
                $month = $this->getMonth($dateParts[1]);
                $date = str_replace($dateParts[1], $month, $date);
            }

            if (strlen($dateParts[0] == 2)) {
                $oldDateTimeFormat = DateTime::createFromFormat('m/d/Y', $date);
            } else {
                $oldDateTimeFormat = DateTime::createFromFormat('Y/m/d', $date);
            }

            if (!$oldDateTimeFormat) {
                $date = hype::getFormattedDate(hype::DB_DATE_FORMAT, $date, 'm/d/Y');
//                var_dump($date);exit();
            } else {
                $date = $oldDateTimeFormat->format(hype::DB_DATE_FORMAT);
            }
        } else {
            $date = PHPExcel_Shared_Date::ExcelToPHPObject($dob);
            $date = $date->format(hype::DB_DATE_FORMAT);
        }
        $data['dob'] = $date;

        /** Patient Phone * */
        $p = $this->getCellValue($this->headers['phone'], $rowNum);
        if (strlen($p) > 8) {
            $data['phone'] = hype::processPhone($p);
        }

        /** Patient HC # * */
        $data['hcn'] = $this->sheet->getCell($this->headers['hcn'] . $rowNum)->getValue();
        $data['version_code'] = $this->sheet->getCell($this->headers['version_code'] . $rowNum)->getValue();

        /** Service Date * */
        $exDate = $this->sheet->getCell($this->headers['service_date'] . $rowNum)->getFormattedValue();
        if ((strlen($exDate) > 7) && !(strpos($exDate, '.') != 0)) {
            $date = str_replace(" ", "/", $exDate);
            $dateParts = explode('/', $date);

            if (strlen($dateParts[1]) == 3) {
                $month = $this->getMonth($dateParts[1]);
                $date = str_replace($dateParts[1], $month, $date);
            }

            if (strlen($dateParts[0] == 2)) {
                $oldDateTimeFormat = DateTime::createFromFormat('m/d/Y', $date);
            } else {
                $oldDateTimeFormat = DateTime::createFromFormat('Y/m/d', $date);
            }
            $date = $oldDateTimeFormat->format(hype::DB_DATE_FORMAT);
        } else {
            $date = PHPExcel_Shared_Date::ExcelToPHPObject($exDate);
            $date = $date->format(hype::DB_DATE_FORMAT);
        }
        $data['exam_date'] = $date;

        /** Referring Doctor * */
        $data['ref_doc'] = $this->getCellValue($this->headers['ref_doc'], $rowNum);
        if (strlen($data['ref_doc']) > 6) {
            $data['ref_doc'] = substr($data['ref_doc'], 0, 6);
        }

        /** Billing Doctor * */
        $data['reporting_dr'] = $this->getCellValue($this->headers['doctor'], $rowNum);

        /** Service Code [Technical] * */
        if (array_key_exists('tech_code', $this->headers)) {
            $data['tech_code'] = $this->getCellValue($this->headers['tech_code'], $rowNum);
        }

        /** Service Code [Professional] * */
        $data['prof_code'] = $this->getCellValue($this->headers['prof_code'], $rowNum);

        /** Facility Number * */
        $data['facility'] = $this->getCellValue($this->headers['facility'], $rowNum);

        /** Patient Sex * */
        $data['sex'] = $this->getCellValue($this->headers['sex'], $rowNum);
        if (strlen($data['sex']) > 1) {
            $data['sex'] = substr($data['sex'], 0, 1);
        }

        /** Patient Province * */
        $province = $this->getCellValue($this->headers['province'], $rowNum);
        if (($province == '1. Ontario HCP') || ($province == 'HCP')) {
            $province = 'ON';
        } else {
            $province = 'NA';
        }
        $data['province'] = $province;

        /** Number Of Services * */
        if (isset($this->headers['num_serv'])) {
            $data['num_serv'] = $this->getCellValue($this->headers['num_serv'], $rowNum);
        }

        return $data;
    }

    private function getMonth($month) {
        if ($month == 'Jan') {
            $month = '01';
        } else if ($month == 'Feb') {
            $month = '02';
        } else if ($month == 'Mar') {
            $month = '03';
        } else if ($month == 'Apr') {
            $month = '04';
        } else if ($month == 'May') {
            $month = '05';
        } else if ($month == 'Jun') {
            $month = '06';
        } else if ($month == 'Jul') {
            $month = '07';
        } else if ($month == 'Aug') {
            $month = '08';
        } else if ($month == 'Sep') {
            $month = '09';
        } else if ($month == 'Oct') {
            $month = '10';
        } else if ($month == 'Nov') {
            $month = '11';
        } else if ($month == 'Dec') {
            $month = '12';
        }
        return $month;
    }

    private function getCellValue($column, $row) {
        $value = "";
        try {
            $v = $this->sheet->getCell($column . $row)->getValue();
        } catch (Exception $e) {
            $v = null;
        }
        if ($v) {
            $value = $v;
        }
        return $value;
    }

    public function log($text) {
        if ($this->debug) {
            echo $text . "\n";
        }
    }

    private function getPatient($hcn, $client_Id) {
        $patient = Doctrine_Query::create()
                ->from('Patient p')
                ->addWhere('p.hcn_num = (?)', $hcn)
                ->addWhere('p.client_id = (?)', $client_Id)
                ->fetchOne();
        return $patient;
    }

    private function updatePatient($patient, $data, $hcn, $patient_type_id, $patient_status_id) {
        $patient->sex = $data['sex'];
        $patient->moh_province = $data['province'];
        $patient->hcn_num = $hcn;
        $patient->hcn_version_code = $data['version_code'];
        $patient->client_id = $this->client_id;
        $patient->patient_type_id = $patient_type_id;
        $patient->patient_status_id = $patient_status_id;
        $patient->lname = $data['lname'];
        $patient->fname = $data['fname'];
        $patient->dob = $data['dob'];
        $patient->province = 'ON';

        if (array_key_exists('phone', $data)) {
            $patient->hphone = $data['phone'];
        }

        $patient->ref_doc_num = $data['ref_doc'];
        while (strlen($patient->ref_doc_num) < 6) {
            $patient->ref_doc_num = '0' . $patient->ref_doc_num;
        }

        return $patient;
    }

    private function buildClaim1($data, $patient) {
        $group = $data['professional_group'];
        $techGroup = $data['technical_group'];

        $claim = new Claim();
        $claim->setPatientDetails($patient);
        $claim->pay_prog = ClaimTable::CLAIM_TYPE_HCPP;
        $claim->batch_number = ClaimTable::EMPTY_BATCH;
        $claim->date_created = date(hype::DB_ISO_DATE);

        $doctor = $this->getDoctor($this->doctors, $data['professional_group'], $data['reporting_dr']);

        if ($doctor instanceof Doctor) {
            $claim->Doctor = $doctor;
            $claim->doctor_id = $claim->Doctor->getID();
            $claim->doctor_code = $claim->Doctor->qc_doctor_code;
        }

        $claim->location_id = $this->location_id;
        if ($techGroup && array_key_exists($techGroup, $this->locations)) {
            $claim->location_id = $this->locations[$techGroup]->getID();
        }

        $specCode = null;
        if ($doctor instanceof Doctor) {
            $specCode = $doctor->spec_code_id;
        }

        $item = new ClaimItem();
        $item->client_id = $this->client_id;
        $item->item_id = 1;
        $item->service_code = $data['prof_code'];
        $item->service_date = $data['exam_date'];

        $item->num_serv = 1;
        if (array_key_exists('num_serv', $data) && $data['num_serv']) {
            $item->num_serv = $data['num_serv'];
        }
        $item->modifier = $item->num_serv * 100;

        $sc = substr($item->service_code, 0, 4);
        $mode = substr($item->service_code, 4, 1);

        if (!$mode) {
            $mode = 'A';
            $item->service_code .= 'A';
        }

        $scr = $this->getServiceCodeRecord($sc);
        if ($scr instanceof ServiceCode) {
            $item->fee_subm = $item->num_serv * $scr->determineFee($mode, $specCode);
            $item->base_fee = $scr->determineFee($mode, $specCode);
        }

        $claim->ClaimItem->add($item);
        return $claim;
    }

    private function getDoctor($docs, $group, $prov_num) {
        $doc = null;
        foreach ($docs as $d) {
            if ($d instanceof Doctor) {
                if ($d->group_num == $group && $d->provider_num == $prov_num) {
                    $doc = $d;
                }
            }
        }
        return $doc;
    }

    private function getServiceCodeRecord($service_code) {
        return Doctrine_Query::create()
                        ->from('ServiceCode sc')
                        ->addWhere('sc.service_code = (?)', $service_code)
                        ->fetchOne();
    }

    private function checkClaim($item) {
        $hcn = hype::encryptHCN($item->Claim->health_num);

        $cl = Doctrine_Query::create()
                ->from('ClaimItem ci')
                ->leftJoin('ci.Claim c')
                ->addWhere('c.health_num = (?)', $hcn)
                ->addWhere('ci.service_code = (?)', $item->service_code)
                ->addWhere('ci.service_date = (?)', $item->service_date)
                ->addWhere('c.client_id = (?)', $this->client_id)
                ->addWhere('c.deleted = (?)', array(false))
                ->fetchOne();

        if ($cl['Claim']['health_num'] != null) {
            $this->log('Claim already exists. ID: ' . $cl->Claim->getID());
        }

        return ($cl['Claim']['health_num'] != null);
    }

    private function buildClaim2($data, $patient) {
        $group = $data['technical_group'];

        $claim = new Claim();
        $claim->setPatientDetails($patient);
        $claim->pay_prog = ClaimTable::CLAIM_TYPE_HCPP;
        $claim->batch_number = ClaimTable::EMPTY_BATCH;
        $claim->date_created = date(hype::DB_ISO_DATE);

        $doctor = $this->getDoctor($this->doctors, $group, $data['reporting_dr']);
        if ($doctor instanceof Doctor) {
            $claim->Doctor = $doctor;
            $claim->doctor_id = $doctor->getID();
            $claim->doctor_code = $doctor->qc_doctor_code;
        }

        $claim->location_id = $this->location_id;
        if ($group) {
            $claim->location_id = $this->locations[$group]->getID();
        }

        $specCode = null;
        if ($doctor instanceof Doctor) {
            $specCode = $doctor->spec_code_id;
        }

        $item = new ClaimItem();
        $item->client_id = $this->client_id;
        $item->item_id = 1;
        if (array_key_exists('tech_code', $data) && $data['tech_code']) {
            $item->service_code = $data['tech_code'];
        }

        $item->service_date = $data['exam_date'];

        $item->num_serv = 1;
        if (array_key_exists('num_serv', $data) && $data['num_serv']) {
            $item->num_serv = $data['num_serv'];
        }
        $item->modifier = $item->num_serv * 100;

        $sc = substr($item->service_code, 0, 4);
        $mode = substr($item->service_code, 4, 1);
        if (!$mode) {
            $mode = 'A';
            $item->service_code .= 'A';
        }

        $scr = $this->getServiceCodeRecord($sc);
        if ($scr instanceof ServiceCode) {
            $item->fee_subm = $item->num_serv * $scr->determineFee($mode, $specCode);
            $item->base_fee = $scr->determineFee($mode, $specCode);
        }
        $claim->ClaimItem->add($item);

        return $claim;
    }

}
