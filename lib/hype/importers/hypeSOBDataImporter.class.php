<?php

class hypeSOBDataImporter extends hypeTXTFileImporterBase
{
    protected function parseDataImporterRecord()
	{
		$this->log('hypeSOBDataImporter.parseDataImporterRecord() called');

        $baseAna = floatval(SystemParamTable::getValue('service_code_ana'));
        $baseAssistant = floatval(SystemParamTable::getValue('service_code_assistant'));

		do {
            $dataImporter = $this->getDataImporter($this->dataImporterFile->get('id'));

			if ($dataImporter instanceof DataImporter) {
				$this->turnDataImporterIntoServiceCode($dataImporter, $baseAna, $baseAssistant);
			}

		} while ($dataImporter instanceof DataImporter);

		$this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_COMPLETED);
		$this->dataImporterFile->save();
	}

    /**
     * @param DataImporter $dataImporter
     * @param float $base_ana
     * @param float $base_assistant
     * @throws Exception
     */
	private function turnDataImporterIntoServiceCode($dataImporter, $base_ana, $base_assistant)
	{
        $this->resetLog();
		$this->log('hypeSOBDataImporter.turnDataImporterIntoServiceCode() called');

		$dataImporter->setStatus(DataImporterTable::STATUS_PARSING);
		$dataImporter->save();

		$line = $dataImporter->getDataText();
        $data = array();

        $data['field_name'] = substr($line, 0, 4);
        $data['effective_date'] = substr($line, 4, 8);
        $data['termination_date'] = substr($line, 12, 8);
        $data['provider_fee'] = intval(substr($line, 20, 11)) / 10000;
        $data['assistant_fee'] = intval(substr($line, 31, 11)) / 10000;
        $data['specialist_fee'] = intval(substr($line, 42, 11)) / 10000;
        $data['ana_fee'] = intval(substr($line, 53, 11)) / 10000;
        $data['non_ana_fee'] = intval(substr($line, 65, 11)) / 10000;

        $serviceCode = $this->getServiceCode($data['field_name']);

        if (!$serviceCode->get('id')) {
            $serviceCode['service_code'] = $data['field_name'];
            $serviceCode['description'] = 'New Code';
            $serviceCode['sc_type'] = ServiceCodeTable::SC_TYPE_NORMAL;
        }

        if ($serviceCode->sc_type != ServiceCodeTable::SC_TYPE_NORMAL) {
            $dataImporter->setStatus(DataImporterTable::STATUS_COMPLETED);
        }
        else {
            $serviceCode->units_assistant = 1;
            $serviceCode->units_ana = 1;
            $serviceCode->units_non_ana = 1;

            if ($data['effective_date']) {
                $serviceCode->date_start = $this->formatAsDate($data['effective_date']);
            }

            if ($data['termination_date'] != '99999999') {
                $serviceCode->date_term = $this->formatAsDate($data['termination_date']);
            }

            if ($data['provider_fee']) {
                $serviceCode->fee_general = $data['provider_fee'];
            }

            if ($data['assistant_fee']) {
                $serviceCode->fee_assistant = $data['assistant_fee'];
                if ($base_assistant && ($data['assistant_fee'] / $base_assistant) == intval($data['assistant_fee'] / $base_assistant)) {
                    $serviceCode->units_assistant = $data['assistant_fee'] / $base_assistant;
                    $serviceCode->fee_assistant = $base_assistant;
                }
            }

            if ($data['specialist_fee']) {
                $serviceCode->fee_specialist = $data['specialist_fee'];
            }

            if ($data['ana_fee']) {
                $serviceCode->fee_ana = $data['ana_fee'];

                if ($base_ana && ($data['ana_fee'] / $base_ana) == intval($data['ana_fee'] / $base_ana)) {
                    $serviceCode->units_ana = $data['ana_fee'] / $base_ana;
                    $serviceCode->fee_ana = $base_ana;
                }
            }

            if ($data['non_ana_fee']) {
                $serviceCode->fee_non_ana = $data['non_ana_fee'];

                if ($base_ana && ($data['non_ana_fee'] / $base_ana) == intval($data['non_ana_fee'] / $base_ana)) {
                    $serviceCode->units_non_ana = $data['non_ana_fee'] / $base_ana;
                    $serviceCode->fee_non_ana = $base_ana;
                }
            }
        }

        if ($serviceCode->isNew()) {
            $this->dataImporterFile->setNewRecordCount($this->dataImporterFile->getNewRecordCount() + 1);
        }
        else {
            $this->dataImporterFile->setUpdateCount($this->dataImporterFile->getUpdateCount() + 1);
        }

        $dataImporter->appendLog($this->getLog());

		try {
			$con = Doctrine_Manager::getInstance()->getConnectionForComponent('ServiceCode');
			$con->beginTransaction();

            if ($serviceCode instanceof ServiceCode) {
                $serviceCode->save();

                $dataImporter->setRecordType('ServiceCode');
                $dataImporter->setRecordId($serviceCode->get('id'));
            }

            $dataImporter->setStatus(DataImporterTable::STATUS_COMPLETED);
			$dataImporter->save();

            $this->dataImporterFile->save();
			$con->commit();
		}
		catch (Exception $e) {
			$con->rollback();
			$importInformation = json_decode($this->dataImporterFile->getImportInformation(), true);
			if (strlen($importInformation['errors'])) {
				$importInformation['errors'] .= "\r\n";
			}
			$importInformation['errors'] .= iconv("UTF-8", "UTF-8//IGNORE", 'Line ' . $dataImporter->getLineNumber() . ': ' . $e->getMessage());

			$this->dataImporterFile->setImportInformation(json_encode($importInformation));
			$this->dataImporterFile->save();

			$dataImporter->setStatus(DataImporterTable::STATUS_ERROR);
			$dataImporter->save();

			throw $e;
		}
	}

    /**
     * @param string
     * @return ServiceCode
     */
	private function getServiceCode($name)
	{
		$serviceCode = Doctrine_Query::create()
			->from('ServiceCode sc')
			->addWhere('sc.service_code = (?)', $name)
			->limit(1)
			->fetchOne();

		if (!$serviceCode instanceof ServiceCode) {
			$serviceCode = new ServiceCode();
		}
		return $serviceCode;
	}

    public function formatAsDate($date)
    {
        $y = intval(substr($date, 0, 4));
        $m = intval(substr($date, 4, 2));
        $d = intval(substr($date, 6));
        return  $y . '-' . ($m ? $m : '01') . '-' . ($d ? $d : '01');
    }
}

