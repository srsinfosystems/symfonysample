<?php

class hypeSpecCodeDataImporter extends hypeXLSFileImporterBase
{
	protected function parseDataImporterRecord()
	{
		$this->log('hypeSpecCodeDataImporter.parseDataImporterRecord() called');

		do {
			$dataImporter = $this->getDataImporter($this->dataImporterFile->get('id'));

			if ($dataImporter instanceof DataImporter) {
				$this->turnDataImporterIntoSpecCode($dataImporter);
			}

		} while ($dataImporter instanceof DataImporter);


		$this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_COMPLETED);
		$this->dataImporterFile->save();
	}

	private function turnDataImporterIntoSpecCode(DataImporter $dataImporter)
	{
		$this->resetLog();
		$this->log('hypeSpecCodeDataImporter.turnDataImporterIntoSpecCode() called');

		$dataImporter->setStatus(DataImporterTable::STATUS_PARSING);
		$dataImporter->save();

		$data = json_decode($dataImporter->getDataText(), true);

		$name = strtoupper(iconv("UTF-8", "UTF-8//IGNORE", $data['name']));
		$description = iconv("UTF-8", "UTF-8//IGNORE", $data['description']);

		$specCode = $this->getSpecCode($name);

		$specCode->setMohSpecCode(strtoupper($name));
		$specCode->setDescription(strtoupper($description));
		$specCode->active = true;

		$dataImporter->setStatus(DataImporterTable::STATUS_COMPLETED);
		$dataImporter->appendLog($this->getLog());

		if ($specCode->isNew()) {
			$this->dataImporterFile->setNewRecordCount($this->dataImporterFile->getNewRecordCount() + 1);
		}
		else {
			$this->dataImporterFile->setUpdateCount($this->dataImporterFile->getUpdateCount() + 1);
		}

		try {
			$con = Doctrine_Manager::getInstance()->getConnectionForComponent('SpecCode');
			$con->beginTransaction();

			$specCode->save();

			$dataImporter->setRecordType('SpecCode');
			$dataImporter->setRecordId($specCode->get('id'));

			$dataImporter->save();
            $this->dataImporterFile->save();

			$con->commit();
		}
		catch (Exception $e) {
			$con->rollback();
			$importInformation = json_decode($this->dataImporterFile->getImportInformation(), true);
			if (strlen($importInformation['errors'])) {
				$importInformation['errors'] .= "\r\n";
			}
			$importInformation['errors'] .= iconv("UTF-8", "UTF-8//IGNORE", 'Line ' . $dataImporter->getLineNumber() . ': ' . $e->getMessage());

			$this->dataImporterFile->setImportInformation(json_encode($importInformation));
			$this->dataImporterFile->setErrorCount($this->dataImporterFile->getErrorCount() + 1);
			$this->dataImporterFile->save();

			$dataImporter->setStatus(DataImporterTable::STATUS_ERROR);
			$dataImporter->save();

			throw $e;
		}
	}

	private function getSpecCode($name)
	{
		$specCode = Doctrine_Query::create()
			->from('SpecCode s')
			->addWhere('s.moh_spec_code = (?)', $name)
			->limit(1)
			->fetchOne();

		if (!$specCode instanceof SpecCode) {
			$specCode = new SpecCode();
		}
		return $specCode;
	}

	protected function importSpreadsheetRowIntoDataImporter($lineNum, PHPExcel_Worksheet $sheet)
	{
		$this->log('hypeSpecCodeDataImporter.importSpreadsheetRowIntoDataImporter() called');

		$name = $sheet->getCell('A' . $lineNum)->getValue();
		$description = $sheet->getCell('B' . $lineNum)->getValue();

		if ($name || $description) {
			$dataImporter = new DataImporter();

			$dataImporter->DataImporterFile = $this->dataImporterFile;
			$dataImporter->importer_name = $this->dataImporterFile->getDocumentName();
			$dataImporter->status = DataImporterTable::STATUS_UNPARSED;
			$dataImporter->line_number = $lineNum;
			$dataImporter->setDataText(json_encode(array('name' => $name, 'description' => $description)));
			$dataImporter->save();

			$this->dataImporterFile->setTotalRecordCount($this->dataImporterFile->getTotalRecordCount() + 1);
			return true;
		}
		return false;
	}

}

