<?php

require_once('./lib/vendor/phpExcel/Classes/PHPExcel.php');

class hypeDataImporter
{
	public function __construct()
	{
	}
	public function run()
	{
		$this->log('hypeDataImporter.run() called');
		$dataImporter = $this->getDataImporterFile();

		if (!$dataImporter instanceof DataImporterFile) {
			$this->log('no data to import');
			return;
		}
		$this->log('File to handle: ' . $dataImporter->getDocumentName());

		if ($dataImporter->getImporterType() == DataImporterFileTable::FILE_TYPE_EXPL_CODE_LIST) {
			$importer = new hypeExplCodeDataImporter($dataImporter);
			$importer->run();
		}
		else if ($dataImporter->getImporterType() == DataImporterFileTable::FILE_TYPE_SPEC_CODE_LIST) {
			$importer = new hypeSpecCodeDataImporter($dataImporter);
			$importer->run();
		}
		else if ($dataImporter->getImporterType() == DataImporterFileTable::FILE_TYPE_REFDOC_LIST) {
			$importer = new hypeReferringDoctorDataImporter($dataImporter);
			$importer->run();
		}
		else if ($dataImporter->getImporterType() == DataImporterFileTable::FILE_TYPE_REFDOC_DUP_CHECK) {
			$importer = new hypeReferringDoctorDuplicateCheck($dataImporter);
			$importer->run();
		}
		else if ($dataImporter->getImporterType() == DataImporterFileTable::FILE_TYPE_SERVICE_CODE_LIST) {
			$importer = new hypeServiceCodeDataImporter($dataImporter);
			$importer->run();
		}
		else if ($dataImporter->getImporterType() == DataImporterFileTable::FILE_TYPE_SERVICE_CODE_SOB) {
			$importer = new hypeSOBDataImporter($dataImporter);
			$importer->run();
		}
		else if ($dataImporter->getImporterType() == DataImporterFileTable::FILE_TYPE_DAYSHEET_VPL) {
			$importer = new hypeDaySheetVPLImporter($dataImporter);
			$importer->run();
		}
		else if ($dataImporter->getImporterType() == DataImporterFileTable::FILE_TYPE_FACILITY_TYPE_LIST) {
			$importer = new hypeFacilityTypeImporter($dataImporter);
			$importer->run();
		}
		else if ($dataImporter->getImporterType() == DataImporterFileTable::FILE_TYPE_FACILITY_LIST) {
			$importer = new hypeFacilityNumberImporter($dataImporter);
			$importer->run();
		}

	}

	private function log($str)
	{
		echo $str . "\n";
	}

	private function getDataImporterFile()
	{
        $file = Doctrine_Query::create()
            ->from('DataImporterFile d')
            ->addWhere('d.status != (?)', array(DataImporterFileTable::FILE_STATUS_COMPLETED))
            ->orderBy('d.id asc')
            ->limit(1)
            ->fetchOne();





		$this->log('hypeDataImporter.getDataImporterFile() called');
		return Doctrine_Query::create()
			->from('DataImporterFile d')
			->addWhere('d.status != (?)', array(DataImporterFileTable::FILE_STATUS_COMPLETED))
			->orderBy('d.id asc')
			->limit(1)
			->fetchOne();
	}
}
