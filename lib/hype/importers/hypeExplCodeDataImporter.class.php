<?php

class hypeExplCodeDataImporter extends hypeXLSFileImporterBase
{
	protected function parseDataImporterRecord()
	{
		$this->log('hypeExplCodeDataImporter.parseDataImporterRecord() called');

		do {
			$dataImporter = $this->getDataImporter($this->dataImporterFile->get('id'));

			if ($dataImporter instanceof DataImporter) {
				$this->turnDataImporterIntoExplCode($dataImporter);
			}

		} while ($dataImporter instanceof DataImporter);


		$this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_COMPLETED);
		$this->dataImporterFile->save();
	}

	private function turnDataImporterIntoExplCode(DataImporter $dataImporter)
	{
		$this->resetLog();
		$this->log('hypeExplCodeDataImporter.turnDataImporterIntoExplCode() called');

		$dataImporter->setStatus(DataImporterTable::STATUS_PARSING);
		$dataImporter->save();

		$data = json_decode($dataImporter->getDataText(), true);

		$name = strtoupper(iconv("UTF-8", "UTF-8//IGNORE", $data['name']));
		$section = strtoupper(iconv("UTF-8", "UTF-8//IGNORE", $data['section']));
		$description = iconv("UTF-8", "UTF-8//IGNORE", $data['description']);

		$explCode = $this->getExplCode($name);

		$explCode->setExplCode($name);
		$explCode->setSection($section);
		$explCode->setDescription($description);

		if ($explCode->isNew()) {
			$this->dataImporterFile->setNewRecordCount($this->dataImporterFile->getNewRecordCount() + 1);
		}
		else {
			$this->dataImporterFile->setUpdateCount($this->dataImporterFile->getUpdateCount() + 1);
		}

		$dataImporter->setStatus(DataImporterTable::STATUS_COMPLETED);
		$dataImporter->appendLog($this->getLog());

		try {
			$con = Doctrine_Manager::getInstance()->getConnectionForComponent('ExplCodes');
			$con->beginTransaction();

			$explCode->save();

			$dataImporter->setRecordType('ExplCode');
			$dataImporter->setRecordId($explCode->get('id'));

			$dataImporter->save();

			$con->commit();
		}
		catch (Exception $e) {
			$con->rollback();
			$importInformation = json_decode($this->dataImporterFile->getImportInformation(), true);
			if (strlen($importInformation['errors'])) {
				$importInformation['errors'] .= "\r\n";
			}
			$importInformation['errors'] .= iconv("UTF-8", "UTF-8//IGNORE", 'Line ' . $dataImporter->getLineNumber() . ': ' . $e->getMessage());

			$this->dataImporterFile->setImportInformation(json_encode($importInformation));
			$this->dataImporterFile->setErrorCount($this->dataImporterFile->getErrorCount() + 1);
			$this->dataImporterFile->save();

			$dataImporter->setStatus(DataImporterTable::STATUS_ERROR);
			$dataImporter->save();

			throw $e;
		}
	}

	private function getExplCode($name)
	{
		$explCode = Doctrine_Query::create()
			->from('ExplCodes e')
			->addWhere('e.expl_code = (?)', $name)
			->limit(1)
			->fetchOne();

		if (!$explCode instanceof ExplCodes) {
			$explCode = new ExplCodes();
		}
		return $explCode;
	}
	protected function importSpreadsheetRowIntoDataImporter($lineNum, PHPExcel_Worksheet $sheet)
	{
		$this->log('hypeExplCodeDataImporter.importSpreadsheetRowIntoDataImporter() called');

		$name = $sheet->getCell('A' . $lineNum)->getValue();
		$section = $sheet->getCell('B' . $lineNum)->getValue();
		$description = $sheet->getCell('C' . $lineNum)->getValue();

		if ($name || $section || $description) {
			$dataImporter = new DataImporter();

			$dataImporter->DataImporterFile = $this->dataImporterFile;
			$dataImporter->importer_name = $this->dataImporterFile->getDocumentName();
			$dataImporter->status = DataImporterTable::STATUS_UNPARSED;
			$dataImporter->line_number = $lineNum;
			$dataImporter->setDataText(json_encode(array('name' => $name, 'section' => $section, 'description' => $description)));
			$dataImporter->save();

			$this->dataImporterFile->setTotalRecordCount($this->dataImporterFile->getTotalRecordCount() + 1);
			return true;
		}
		return false;
	}

}

