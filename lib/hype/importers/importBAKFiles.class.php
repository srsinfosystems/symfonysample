<?php
class importBAKFiles
{
 	private $client_id;
 	private $location_id;
 	private $doctor_id;
 	private $file;
 	private $patients_ID;
 	private $count;

 	private $isATask = false;
 	private $debug = false;

 	public function __construct($file, $client_id, $location_id, $doctor_id = null)
 	{
 		$this->client_id = $client_id;
 		$this->file = $file;
 		$this->location_id = $location_id;
 		$this->doctor_id = $doctor_id;
 		$this->patients_ID = array();
 	}
 	public function setDebug($v)
 	{
 		$this->debug = $v;
 	}
 	public function setisATask($v)
 	{
 		$this->isATask = $v;
 	}
 	public function getDebug()
 	{
 		return $this->debug;
 	}
 	public function getPatiendsId()
 	{
 		return $this->patients_ID;
 	}
 	private function setCount($val)
 	{
 		$this->count = $val;
 	}
 	public function getCount()
 	{
 		return $this->count;
 	}
 	public function testPrint($p)
 	{
 		echo 'Chart Num   -> '. $p['patient_number'] . "\n";
 		echo 'Fist Name   -> ' . $p['fname'] . "\n";
 		echo 'Last Name   -> ' . $p['lname'] . "\n";
 		echo 'Sex         -> ' . $p['sex'] . "\n";
 		echo 'hcn         -> ' . $p['hcn_num'] . "\n";
 		echo 'hcn exp     -> ' . $p['hcn_exp_year'] . "\n";
 		echo 'DOB         -> ' . $p['dob'] . "\n";
 		echo 'Address     -> ' . $p['address'] . "\n";
 		echo 'City        -> ' . $p['city'] . "\n";
 		echo 'Postal C    -> ' . $p['postal_code'] . "\n";
 		echo 'Province    -> ' . $p['province'] . "\n";
 		echo 'Home Phone  -> ' . $p['hphone'] . "\n";
 		echo 'HCN vc      -> ' . $p['hcn_version_code']. "\n";
 		echo 'P type id   -> ' . $p['patient_type_id'] . "\n";
 		echo 'P. Sts id   -> ' . $p['patient_status_id'] . "\n";
 		echo 'Notes       -> ' . $p['Notes'][0]['note'] . "\n";

 		echo "\n" . '----------------------------------------------------------------' . "\n";

 	}
 	public function findDuplicated($hcn, $client_Id)
 	{
 		$patients = Doctrine_Query::create()
 			->from('Patient p')
 			->addWhere('p.hcn_num = (?)', $hcn)
 			->addWhere('p.client_id = (?)', $client_Id)
 			->addWhere('p.deleted = (?)', false)
 			->execute();
 		return $patients;
 	}
 	public function run()
 	{
 		$total=0;

 		if ($this->isATask) {
 			if ($this->debug) {
 				echo ' File to insert --> ' . $this->file . "\n";
 				echo 'Processing file --> ' . $this->file . "\n";
 			}
 			$txt_file = file_get_contents( $this->file);
 		}else{
 			if ($this->debug){
 				echo ' File to insert --> ' . $this->file->getOriginalName() . "\n";
 				echo 'Processing file --> ' . $this->file->getOriginalName() . "\n";
 			}
 			$txt_file = file_get_contents( $this->file->getTempName());
 		}

 		$rows = explode("\n", $txt_file);

 		$patient_type_id = null;
 		$patient_status_id = null;

 		$cp = ClientParamTable::getParamForClient($this->client_id, 'patient_status_default');
 		if ($cp instanceof ClientParam) {
 			$patient_status_id = $cp->value;
 		}

 		$cp = ClientParamTable::getParamForClient($this->client_id, 'default_patient_type_id');
 		if ($cp instanceof ClientParam) {
 			$patient_type_id = $cp->value;
 		}

 		$i = 0;
 		foreach ($rows as $row)
 		{
 			$duplicates = array();

 			if (strlen($row)>170) {

 				$patient = new Patient();
 				//---------------------------/
 				$patient->client_id=$this->client_id;
 				$patient->location_id = $this->location_id;
 				//---------------------------/

 				$patient->patient_number = strtoupper(trim(substr($row, 1,8)));
 				$name = strtoupper(trim(substr($row, 9,41)));
 				$names = preg_split('/\s+/', $name);

 				$patient->lname = $names[0];
 				$patient->fname = $names[1];
 				$patient->sex = strtoupper(trim(substr($row, 52,1)));
 				$dob = strtoupper(trim(substr($row, 53,8)));
 				$patient->dob = substr($dob,0,4) . '-' . substr($dob,4,2) . '-' . substr($dob,6,2);
 				$patient->address = strtoupper(trim(substr($row, 82,30)));
 				$patient->city = strtoupper(trim(substr($row, 116,20)));

 				$patient->postal_code = strtoupper(trim(substr($row, 143,7)));
 				$patient->hphone = strtoupper(trim(substr($row, 154,10)));

 				$patient->hcn_version_code = strtoupper(trim(substr($row, 192,2)));
 				$date = strtoupper(trim(substr($row, 202,10)));
 				if ($date) {
 					$patient->hcn_exp_year = substr($date,0,4) . '-' . substr($date,4,2) . '-' . substr($date,6,2);
 				}
 				$patient->patient_type_id = $patient_type_id;
 				$patient->patient_status_id = $patient_status_id;

 				$hcnum = strtoupper(trim(substr($row, 180,10)));
 				if ($this->debug) echo "\n" . 'HC num    -> ' . $hcnum . "\n";
 				$note = new Notes();
 				$note->client_id = $this->client_id;
 				if ($this->isATask){
 					$note->note = 'Imported from file ' . $this->file;
 				}else {
 					$note->note = 'Imported from file ' . $this->file->getOriginalName();
 				}
 				if (strlen($hcnum) > 1) {
 					$patient->hcn_num = hype::encryptHCN($hcnum);
 					$duplicates = $this->findDuplicated($patient->hcn_num, $patient->getClientId());
 					$patient->province = strtoupper(trim(substr($row, 220,2)));;// 'ON';
 				}
 				else {
 					$note->note = $note->note . "\n" . '  There was no Health Card Number to import.';
 				}
 				$note->note = $note->note . "\n" . 'Contact info: ' . substr($row, 143,27);
 				$patient->Notes->add($note);
 				foreach ($duplicates as $patient_row)
 				{
 					$this->patients_ID[] = $patient_row->getId();
 				}
 				if(!count($duplicates))
 				{
 					if ($this->debug) $this->testPrint($patient);

 					try {
 						//$patient->save();
 						if ($this->debug) echo "Patient saved. \n";
 					}
 					catch (Exception $e) {
 						if ($this->debug) echo $e->getMessage() . "\n";
 						if ($this->debug) print_r($patient->toarray());
 						if ($this->debug) echo ' Records inserted --> ' . $i . "\n";
 						exit();
 					}
 				}
 				else {

 					if ($this->debug) echo 'already in data base ...... ' . "\n";
 				}
 			}
 			$i++;
 			$total++;
 			if ($this->debug) echo 'Total of procesed records == ' . $total . "\n";
 			if ($this->debug) echo 'Current file == ' . $this->file . "\n";
 			if ($this->debug) echo 'Total of procesed records in this file == ' . $i . "\n";
 			//exit();
 		}
 		//amount of patients proccesed
 		$this->setCount($i);
 		if ($this->debug) echo ' Records inserted --> ' . $i . "\n";
 	}
}