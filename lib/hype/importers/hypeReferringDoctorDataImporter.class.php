<?php

class hypeReferringDoctorDataImporter extends hypeXLSFileImporterBase
{
 	protected function parseDataImporterRecord()
 	{
 		$this->log('hypeReferringDoctorDataImporter.parseDataImporterRecord() called');

 		do {
		    $dataImporter = $this->getDataImporter($this->dataImporterFile->get('id'));

 			if ($dataImporter instanceof DataImporter) {
 				$this->turnDataImporterIntoReferringDoctor($dataImporter, $this->dataImporterFile->getClientId());
 			}
 		} while ($dataImporter instanceof DataImporter);

		$this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_COMPLETED);
 		$this->dataImporterFile->save();
 	}

	private function turnDataImporterIntoReferringDoctor(DataImporter $dataImporter, $clientID)
 	{
	    $this->resetLog();
 		$this->log('hypeReferringDoctorDataImporter.turnDataImporterIntoReferringDoctor() called');

 		$saveRecord = true;
 		$dataImporter->setStatus(DataImporterTable::STATUS_PARSING);
 		$dataImporter->save();

 		$data = json_decode($dataImporter->getDataText(), true);

		foreach ($data as $col => $val)
		{
			if ($col != 'email') {
				$data[$col] = strtoupper(trim(iconv('UTF-8', 'UTF-8//IGNORE', $val)));
			}
		}

		$referringDoctor = new ReferringDoctor();
		$referringDoctor->setClientId($clientID);
		$referringDoctor->setFname($data['first']);
		$referringDoctor->setLname($data['last']);
		$referringDoctor->setProviderNum($data['provider']);
		$referringDoctor->spec_code = $data['spec'];
		$referringDoctor->setAddress($data['address']);
		$referringDoctor->setAddress2($data['address2']);
		$referringDoctor->setCity($data['city']);
		$referringDoctor->setProvince('ON');
		$referringDoctor->setPostalCode($data['postal']);
		$referringDoctor->fax = hype::processPhone($data['fax']);
		$referringDoctor->phone = hype::processPhone($data['phone']);
		$referringDoctor->setEmail($data['email']);
		$referringDoctor->checkForDuplicates();

		$dataImporter->setStatus(DataImporterTable::STATUS_COMPLETED);
	    $dataImporter->appendLog($this->getLog());

		if ($referringDoctor->hasIdenticalRecords()) {
			$this->log('Exact Referring Doctor details already exist, nothing to import');
			$this->dataImporterFile->setErrorCount($this->dataImporterFile->getErrorCount() + 1);

			$saveRecord = false;
		}
		else {
			if ($referringDoctor->isNew()) {
				$this->dataImporterFile->setNewRecordCount($this->dataImporterFile->getNewRecordCount() + 1);
			}
			else {
				$this->dataImporterFile->setUpdateCount($this->dataImporterFile->getUpdateCount() + 1);
			}
		}

 		try {
			$con = Doctrine_Manager::getInstance()->getConnectionForComponent('ReferringDoctor');
			$con->beginTransaction();

			if ($saveRecord) {
				$referringDoctor->save();

				$dataImporter->setRecordId($referringDoctor->get('id'));
				$dataImporter->setRecordType('ReferringDoctor');
			}

			$dataImporter->save();

            $this->dataImporterFile->save();
			$con->commit();
		}
		catch (Exception $e) {
			$con->rollback();
			$importInformation = json_decode($this->dataImporterFile->getImportInformation(), true);
			if (strlen($importInformation['errors'])) {
				$importInformation['errors'] .= "\r\n";
			}
			$importInformation['errors'] .= iconv("UTF-8", "UTF-8//IGNORE", 'Line ' . $dataImporter->getLineNumber() . ': ' . $e->getMessage());

			$this->dataImporterFile->setImportInformation(json_encode($importInformation));
			$this->dataImporterFile->save();

			$dataImporter->setStatus(DataImporterTable::STATUS_ERROR);
			$dataImporter->save();

			throw $e;
		}
 	}

 	protected function importSpreadsheetRowIntoDataImporter($lineNum, PHPExcel_Worksheet $sheet)
 	{
 		$this->log('hypeReferringDoctorDataImporter.importSpreadsheetRowIntoDataImporter() called');

 		$data = array(
 			'last' => $sheet->getCell('A' . $lineNum)->getValue(),
 			'first' => $sheet->getCell('B' . $lineNum)->getValue(),
 			'provider' => $sheet->getCell('C' . $lineNum)->getValue(),
 			'spec' => $sheet->getCell('D' . $lineNum)->getValue(),
 			'address' => $sheet->getCell('E' . $lineNum)->getValue(),
 			'address2' => $sheet->getCell('F' . $lineNum)->getValue(),
 			'city' => $sheet->getCell('G' . $lineNum)->getValue(),
 			'postal' => $sheet->getCell('H' . $lineNum)->getValue(),
 			'fax' => $sheet->getCell('I' . $lineNum)->getValue(),
 			'phone' => $sheet->getCell('J' . $lineNum)->getValue(),
 			'email' => $sheet->getCell('K' . $lineNum)->getValue(),
 		);

		if ($data['last'] || $data['provider']) {
 			$dataImporter = new DataImporter();

 			$dataImporter->DataImporterFile = $this->dataImporterFile;
 			$dataImporter->importer_name = $this->dataImporterFile->getDocumentName();
			$dataImporter->status = DataImporterTable::STATUS_UNPARSED;
			$dataImporter->line_number = $lineNum;
			$dataImporter->setDataText(json_encode($data));
 			$dataImporter->save();

            $this->dataImporterFile->setTotalRecordCount($this->dataImporterFile->getTotalRecordCount() + 1);
 			return true;
		}
 		return false;
 	}

}

