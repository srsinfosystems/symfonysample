<?php

class hypeDaySheetVPLImporter extends hypeTXTFileImporterBase
{
    /* todo:
    // - interface for displaying logs, going to claims
    // if new patient set referring doctor if it exists
    // notification about new doctor profiles that need to be verified
    */

    const PATIENT_LNAME = 1;
    const PATIENT_FNAME = 0;
    const PATIENT_DOB = 2;
    const PATIENT_SEX = 3;
    const PATIENT_PROV = 4;
    const PATIENT_HCN = 5;
    const PATIENT_VERSION_CODE = 6;
    const PATIENT_HCN_EXP = 7;
    const ITEM_SERVICE_DATE = 8;
    const ITEM_SERVICE_CODE = 9;
    const ITEM_NUM_SERV = 10;
    const ITEM_DIAG_CODE = 12;
    const CLAIM_MANUAL_REVIEW = 13;
    const CLAIM_DOCTOR_NUM = 15;
    const CLAIM_DOCTOR_GROUP = 16;
    const CLAIM_DOCTOR_SPECIALTY = 18;
    const CLAIM_REFDOC_NUM = 19;
    const CLAIM_REFDOC_FNAME = 20;
    const CLAIM_REFDOC_LNAME = 21;
    const CLAIM_FACILITY_NUM = 23;
    const CLAIM_ADMISSION_DATE = 25;
    const CLAIM_SLI = 26;
//    const CLAIM_MANUAL_REVIEW_COMMENT = 14;
//    const CLAIM_LOCATION_CODE = 17;
//    const CLAIM_DEPARTMENT = 22;
//    const CLAIM_ADMISSION_TYPE = 24;
//    const CLAIM_PAY_PROG = 27;
//    const CLAIM_COMMENT = 28;

    /**
     * @var Claim
     */
    private $claim = null;
    private $is_valid = true;
    private $is_duplicate = false;

	public function run()
	{
		$this->log('hypeDaySheetVPLImporter.run() called');
		$this->log('File status is ' . $this->dataImporterFile->getStatusText());

		if ($this->dataImporterFile->getStatus() == DataImporterFileTable::FILE_STATUS_UPLOADED) {
			$importInformation = array(
				'parsingLine' => 1,
				'errors' => "",
			);

			$this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_PARSING_RECORDS);
			$this->dataImporterFile->setImportInformation(json_encode($importInformation));
			$this->dataImporterFile->save();

			$this->importRecords($importInformation);
		}

		else if ($this->dataImporterFile->getStatus() == DataImporterFileTable::FILE_STATUS_PARSING_RECORDS) {
			$importInformation = json_decode($this->dataImporterFile->getImportInformation(), true);
			$this->importRecords($importInformation);
		}

		else if ($this->dataImporterFile->getStatus() == DataImporterFileTable::FILE_STATUS_RECORDS_IMPORTED) {
			$this->parseDataImporterRecord();
		}
	}

    protected function parseDataImporterRecord()
    {
        $this->log('hypeDaySheetVPLImporter.parseDataImporterRecord() called');

        do {
            $dataImporter = $this->getDataImporter($this->dataImporterFile->get('id'));

            if ($dataImporter instanceof DataImporter) {
                $this->turnDataImporterIntoClaim($dataImporter);
            }

        } while ($dataImporter instanceof DataImporter);

        $this->dataImporterFile->setStatus(DataImporterFileTable::FILE_STATUS_COMPLETED);
        $this->dataImporterFile->save();
    }

    private function turnDataImporterIntoClaim(DataImporter $dataImporter)
    {
        $this->resetLog();
        $this->log('hypeDaySheetVPLImporter.turnDataImporterIntoClaim() called');

        $line = iconv('UTF-8', 'UTF-8//IGNORE', $dataImporter->getDataText());
        $line = str_getcsv($line, ',', '"');

        try {
            $line[hypeDaySheetVPLImporter::PATIENT_DOB] = $this->translateDate($line[hypeDaySheetVPLImporter::PATIENT_DOB]);
        }
        catch (Exception $e) {
            $this->log('Unable to convert Patient DOB from expected date format.');
            $this->is_valid = false;
        }

        try {
            $line[hypeDaySheetVPLImporter::PATIENT_HCN_EXP] = $this->translateDate($line[hypeDaySheetVPLImporter::PATIENT_HCN_EXP]);
        }
        catch (Exception $e) {
            $this->log('Unable to convert Patient HCN Expiry Date to expected format.');
            $this->is_valid = false;
        }

        try {
            $line[hypeDaySheetVPLImporter::CLAIM_ADMISSION_DATE] = $this->translateDate($line[hypeDaySheetVPLImporter::CLAIM_ADMISSION_DATE]);
        }
        catch (Exception $e) {
            $this->log('Unable to convert claim admission date to expected format');
            $this->is_valid = false;
        }

        try {
            $line[hypeDaySheetVPLImporter::ITEM_SERVICE_DATE] = $this->translateDate($line[hypeDaySheetVPLImporter::ITEM_SERVICE_DATE]);
        }
        catch (Exception $e) {
            $this->log('Unable to convert claim item service date to expected format');
            $this->is_valid = false;
        }

        foreach ($line as $key => $value) {
            $line[$key] = strtoupper(trim($value));
        }

        if ($line[hypeDaySheetVPLImporter::CLAIM_REFDOC_NUM]) {
            while (strlen($line[hypeDaySheetVPLImporter::CLAIM_REFDOC_NUM]) < 6) {
                $line[hypeDaySheetVPLImporter::CLAIM_REFDOC_NUM] = '0' . $line[hypeDaySheetVPLImporter::CLAIM_REFDOC_NUM];
            }
        }

        while (strlen($line[hypeDaySheetVPLImporter::CLAIM_DOCTOR_GROUP]) < 4) {
            $line[hypeDaySheetVPLImporter::CLAIM_DOCTOR_GROUP] = '0' . $line[hypeDaySheetVPLImporter::CLAIM_DOCTOR_GROUP];
        }

        //Create a new claim with no items as of yet
        $this->claim = new Claim();
        $patient = $this->getPatient($line);

        $this->log('Patient Name: ' . $patient->full_name . ' (ID: ' . $patient->id . ')');
        $this->claim->Patient = $patient;
        $output = $this->claim->setPatientDetails($patient, true);

        foreach ($output as $rec) {
            $this->log($rec);
        }

        $this->claim->Location = $this->dataImporterFile->Location;

        $doctor = $this->getDoctor($line);
        if ($doctor instanceof Doctor) {
            $this->claim->Doctor = $doctor;
            $this->claim->setDoctorCode($doctor->getQcDoctorCode());
        }

        if ($line[hypeDaySheetVPLImporter::CLAIM_REFDOC_NUM]) {
            $ref_doc = $this->getReferringDoctor($line);
            if ($ref_doc instanceof ReferringDoctor) {
                $this->claim->ref_doc_id = $ref_doc->get('id');
                $this->claim->setRefDoc($ref_doc->provider_num);

                $this->log("The Referring Doctor is " . $ref_doc->getProviderNum() . ' ' . $ref_doc->getFname() . ' ' . $ref_doc->getLname());
                $this->log('Claim referring doctor id set to ' . $this->claim->ref_doc_id);
                $this->log('Claim referring doctor number set to ' . $this->claim->ref_doc);
            }
        }

        $this->claim->setBatchNumber(ClaimTable::EMPTY_BATCH);
        $this->claim->setDateCreated(date(hype::DB_ISO_DATE));

        if (array_key_exists(hypeDaySheetVPLImporter::CLAIM_SLI, $line) && $line[hypeDaySheetVPLImporter::CLAIM_SLI]) {
            $this->claim->setSli($line[hypeDaySheetVPLImporter::CLAIM_SLI]);
            $this->log('Claim SLI set to ' . $this->claim->getSli());
        }

        if ($this->claim->Patient->getMohProvince() != 'ON') {
            $this->claim->setPayProg(ClaimTable::CLAIM_TYPE_RMB);
        }
        else {
            $this->claim->setPayProg(ClaimTable::CLAIM_TYPE_HCPP);
        }
        $this->log('Claim Pay Program set to ' . $this->claim->getPayProg());

        if ($line[hypeDaySheetVPLImporter::CLAIM_FACILITY_NUM]) {
            $this->claim->setFacilityNum($line[hypeDaySheetVPLImporter::CLAIM_FACILITY_NUM]);
            $this->log('Claim Facility Number set to ' . $this->claim->getFacilityNum());
        }

        if (array_key_exists(hypeDaySheetVPLImporter::CLAIM_ADMISSION_DATE, $line) && $line[hypeDaySheetVPLImporter::CLAIM_ADMISSION_DATE]) {
            $this->claim->setAdmitDate($line[hypeDaySheetVPLImporter::CLAIM_ADMISSION_DATE]);
            $this->log('Claim Admit Date set to ' . $line[hypeDaySheetVPLImporter::CLAIM_ADMISSION_DATE]);
        }

        if ($line[hypeDaySheetVPLImporter::CLAIM_MANUAL_REVIEW] == 'M') {
            $this->claim->setManualReview(true);
            $this->log('Claim Manual Review set to true');
        }

        //Creates a claim item based on the current line
        //Get the service code, if there is two, call twice with different codes?
        if (array_key_exists(hypeDaySheetVPLImporter::ITEM_SERVICE_CODE, $line)) {
            $service_c = $line[hypeDaySheetVPLImporter::ITEM_SERVICE_CODE];
            $qsc = hypeDaySheetVPLImporter::getQuickServiceCode($service_c);
            $qscs = explode(',', $qsc);
            if (count($qscs) > 1) {
                $this->log('numerous service codes found, creating additional claim items');

                foreach ($qscs as $code) {
                    $this->log('Service code: ' . $code);
                    $line[hypeDaySheetVPLImporter::ITEM_SERVICE_CODE] = $code;
                    $this->log('line array for service code: ' . $line[hypeDaySheetVPLImporter::ITEM_SERVICE_CODE]);
                    $this->buildClaimItem($line);
                }
            } else {
                $this->buildClaimItem($line);
            }
        }

        if (!$this->claim->getHealthNum()) {
            $this->is_valid = false;
            $this->log('Claim is invalid due to missing health card number');
        }

        foreach ($this->claim->ClaimItem as $key => $claimItem) {
            if (!$this->duplicateServicesCheck($claimItem)) {
                $this->is_duplicate;
                unset($this->claim->ClaimItem[$key]);
            }
        }

        $this->claim = $this->mergeIntoExistingClaim($this->claim);

        if (!count($this->claim->ClaimItem)) {
            $this->is_valid = false;
            $this->log('ClaimItems all merged into existing claims');
        }

        $attendees = $this->getAppointmentAttendeesForClaim();

        $con = Doctrine_Manager::getInstance()->getConnectionForComponent('Claim');
        try {
            $con->beginTransaction();

            $dataImporter->appendLog($this->log);

            if ($this->is_valid) {
                if ($this->claim->isNew()) {
                    $this->dataImporterFile->setNewRecordCount($this->dataImporterFile->getNewRecordCount() + 1);
                }
                else {
                    $this->dataImporterFile->setUpdateCount($this->dataImporterFile->getUpdateCount() + 1);
                }

                $this->claim->save();
                foreach ($attendees as $attendee) {
                    $attendee->claim_id = $this->claim->get('id');
                    $attendee->Appointment->status = AppointmentTable::STATUS_CLAIM;

                    $attendee->save();
                }

                $this->log('Finished Processing the claim. Claim ID: ' . $this->claim->get('id'));

                $dataImporter->setStatus(DataImporterTable::STATUS_COMPLETED);
                $dataImporter->setRecordType('Claim');
                $dataImporter->setRecordId($this->claim->get('id'));
            }

            else {
                if (!$this->is_duplicate && count($this->claim->ClaimItem)) {
                    $attendee_id = null;
                    if (count($attendees)) {
                        $attendee_id = $attendees[0]->getId();
                    }

                    $this->log('Finished Processing the claim with errors.');
                    $temporaryForm = TemporaryFormTable::createHL7Claim($this->claim, $attendee_id);

                    $dataImporter->setRecordType('TemporaryForm');
                    $dataImporter->setRecordId($temporaryForm->get('id'));
                    $dataImporter->setStatus(DataImporterTable::STATUS_ERROR);

                    $this->dataImporterFile->setErrorCount($this->dataImporterFile->getErrorCount() + 1);
                }
                else {
                    $this->log('Finished Processing the claim. No claim created due to duplication.');
                    $dataImporter->setStatus(DataImporterTable::STATUS_NO_RECORD);
                    $this->dataImporterFile->setUpdateCount($this->dataImporterFile->getUpdateCount() + 1);
                }
            }

            $dataImporter->appendLog($this->getLog());
            $dataImporter->save();

            $this->dataImporterFile->save();

            $con->commit();
        }
        catch (Exception $e) {
            $con->rollback();
            $importInformation = json_decode($this->dataImporterFile->getImportInformation(), true);
            if (strlen($importInformation['errors'])) {
                $importInformation['errors'] .= "\r\n";
            }
            $importInformation['errors'] .= iconv("UTF-8", "UTF-8//IGNORE", 'Line ' . $dataImporter->getLineNumber() . ': ' . $e->getMessage());

            $this->dataImporterFile->setImportInformation(json_encode($importInformation));
            $this->dataImporterFile->save();

            $dataImporter->setStatus(DataImporterTable::STATUS_ERROR);
            $dataImporter->appendLog($this->getLog());
            $dataImporter->save();

            throw $e;
        }
    }

    private function translateDate($inDate)
    {
        $rs = null;

        if ($inDate == '/  /') {
            return $rs;
        }

        if (!$inDate) {
            return $rs;
        }

        $regex = '/([a-z]+)/i';
        if (preg_match($regex, $inDate, $matches)) {
            return $rs;
        }

        return hype::getFormattedDate(hype::DB_DATE_FORMAT, $inDate, 'm/d/Y');
    }

    private function getPatient($data)
    {
        $this->log('hypeDaySheetVPLImporter.getPatient called');

        $fname = $data[hypeDaySheetVPLImporter::PATIENT_FNAME];
        $lname = $data[hypeDaySheetVPLImporter::PATIENT_LNAME];
        $dob = $data[hypeDaySheetVPLImporter::PATIENT_DOB];
        $hcn = $data[hypeDaySheetVPLImporter::PATIENT_HCN];
        $vc = $data[hypeDaySheetVPLImporter::PATIENT_VERSION_CODE];

        $this->log('Patient Identified as ' . $fname . ' ' . $lname);

        $patient = $this->getPatientFromNameAndHealthCardNumber($fname, $lname, $hcn, $vc);

        // check against fname, lname, dob
        if (!$patient instanceof Patient && $dob) {
            $patient = $this->getPatientFromNameAndDob($fname, $lname, $dob);
        }

        if ($patient instanceof Patient) {
            $this->log('Patient Identified as ' . $fname . ' ' . $lname . ' (ID: ' . $patient->get('id') . ')');
            $patient = $this->updatePatient($data, $patient);
        }
        else {
            $this->log('No patient found.');
            $patient = $this->createPatient($data);
        }

        return $patient;
    }

    private function getPatientFromNameAndHealthCardNumber($fname, $lname, $hcn, $version_code) {
        $this->log('hypeDaySheetVPLImporter.getPatientFromNameAndHealthCardNumber(' . $fname . ', ' . $lname . ', ' . $hcn . ', ' . $version_code . ') called');

        return Doctrine_Query::create()
            ->from('Patient p')
            ->addWhere('p.client_id = (?)', array($this->dataImporterFile->getClientID()))
            ->addWhere('p.fname = (?)', $fname)
            ->addWhere('p.lname = (?)', $lname)
            ->addWhere('p.hcn_num = (?)', hype::encryptHCN($hcn))
            ->addWhere('p.hcn_version_code = (?)', $version_code)
            ->fetchOne();
    }

    private function getPatientFromNameAndDob($fname, $lname, $dob)
    {
        $this->log('hypeDaySheetVPLImporter.getPatientFromNameAndDob(' . $fname . ', ' . $lname . ', ' . $dob . ') called');

        if ($fname && $lname && $dob) {
            return Doctrine_Query::create()
                ->from('Patient p')
                ->addWhere('p.client_id = (?)', array($this->dataImporterFile->getClientID()))
                ->addWhere('p.fname = (?)', $fname)
                ->addWhere('p.lname = (?)', $lname)
                ->addWhere('p.dob = (?)', $dob)
                ->fetchOne();
        }

        return null;
    }

    private function updatePatient($data, Patient $patient)
    {
        $this->log('hypeDaySheetVPLImporter.updatePatient() called.');

        $fname = $data[hypeDaySheetVPLImporter::PATIENT_FNAME];
        $lname = $data[hypeDaySheetVPLImporter::PATIENT_LNAME];
        $dob = $data[hypeDaySheetVPLImporter::PATIENT_DOB];
        $sex = $data[hypeDaySheetVPLImporter::PATIENT_SEX];
        $province = $data[hypeDaySheetVPLImporter::PATIENT_PROV];
        $hcn = $data[hypeDaySheetVPLImporter::PATIENT_HCN];
        $vc = $data[hypeDaySheetVPLImporter::PATIENT_VERSION_CODE];
        $expiryDate = $data[hypeDaySheetVPLImporter::PATIENT_HCN_EXP];


        if ($fname && $fname != $patient->getFname()) {
            $this->log('Patient First Name set to ' . $fname);
            $patient->setFname($fname);
        }

        if ($lname && $lname != $patient->getLname()) {
            $patient->setLname($lname);
            $this->log('Patient Last Name set to ' . $lname);
        }

        if ($sex && $sex != $patient->getSex()) {
            $patient->setSex($sex[0]);
            $this->log('Patient Sex set to ' . $sex[0]);
        }

        if ($province && $province != $patient->getProvince()) {
            $patient->setProvince($province);
            $this->log('Patient Province set to ' . $province);
        }
        else if (!$patient->getProvince()) {
            $patient->setProvince('ON');
            $this->log('Patient Province set to ON');
        }

        if (!$patient->getMohProvince()) {
            $patient->setMohProvince($patient->getProvince());
        }

        if (!$patient->getCountry()) {
            $patient->setCountry('CAN');
            $this->log('Patient Country set to CAN');
        }

        if ($dob) {
            $patient->setDob($dob);
            $this->log('Patient DOB set to ' . $dob);
        }

        if ($hcn) {
            $patient->last_4_hcn = substr($hcn, -4);
            $patient->setHcnNum(hype::encryptHCN($hcn));
            $this->log('Patient HC # set to ' . $hcn);
        }

        if ($vc) {
            $patient->setHcnVersionCode($vc);
            $this->log('Patient Version Code set to ' . $vc);
        }

        if ($expiryDate) {
            $patient->setHcnExpYear($expiryDate);
            $this->log('Patient HCN Expiry set to ' . $expiryDate);
        }

        $patient->save();
        return $patient;
    }

    private function createPatient($data)
    {
        $this->log('hypeDaySheetVPLImporter.createPatient() called');

        $patient = new Patient();
        $patient->setClientID($this->dataImporterFile->getClientID());
        $this->log('Patient client ID set to ' . $patient->getClientID());

        $cp = ClientParamTable::getParamForClient($this->dataImporterFile->getClientID(), 'patient_status_default');
        if ($cp instanceof ClientParam) {
            $patient->setPatientStatusId($cp->value);
        }

        $cp = ClientParamTable::getParamForClient($this->dataImporterFile->getClientID(), 'default_patient_type_id');
        if ($cp instanceof ClientParam) {
            $patient->setPatientTypeId($cp->value);
        }

        $patient = $this->updatePatient($data, $patient);

        return $patient;
    }

    private function getDoctor($data)
    {
        $groupNum = $data[hypeDaySheetVPLImporter::CLAIM_DOCTOR_GROUP];
        $providerNum = $data[hypeDaySheetVPLImporter::CLAIM_DOCTOR_NUM];

        if (strlen($providerNum) == 5) {
            $providerNum = 0 . $providerNum;
        }

        $this->log('hypeDaySheetVPLImporter.getDoctor(' . $groupNum . ', ' . $providerNum . ') called');

        $doctor = Doctrine_Query::create()
            ->from('Doctor d')
            ->addWhere('d.client_id = (?)', array($this->dataImporterFile->getClientID()))
            ->addWhere('d.group_num = (?)', $groupNum)
            ->addWhere('d.provider_num = (?)', $providerNum)
            ->fetchOne();

        if (!$doctor instanceof Doctor) {
            $doctor = $this->createNewDoctor($data);
        }
        $this->log('Doctor: ' . $doctor->getDropdownString() . ' (ID: ' . $doctor->get('id') . ')');

        $inRegion = false;
        $inLocation = false;

        foreach ($doctor->DoctorRegion as $dr)
        {
            if ($dr->region_id == $this->claim->Location->region_id) {
                $inRegion = true;
            }
        }

        foreach ($doctor->DoctorLocation as $dl)
        {
            if ($dl->location_id == $this->claim->Location->get('id')) {
                $inLocation = true;
            }
        }

        if (!$inRegion) {
            $dr = new DoctorRegion();
            $dr->region_id = $this->claim->Location->region_id;

            $doctor->DoctorRegion->add($dr);
        }
        if (!$inLocation) {
            $dl = new DoctorLocation();
            $dl->location_id = $this->claim->Location->get('id');

            $doctor->DoctorLocation->add($dl);
        }

        if (!$inRegion || !$inLocation) {
            $doctor->save();
        }
        return $doctor;
    }

    private function createNewDoctor($data)
    {
        $this->log('hypeDaySheetVPLImporter.createNewDoctor() called.');

        $groupNum = $data[hypeDaySheetVPLImporter::CLAIM_DOCTOR_GROUP];
        $providerNum = $data[hypeDaySheetVPLImporter::CLAIM_DOCTOR_NUM];
        $specialty = $data[hypeDaySheetVPLImporter::CLAIM_DOCTOR_SPECIALTY];

        if (strlen($providerNum) == 5) {
            $providerNum = 0 . $providerNum;
        }

        $doctor = new Doctor();
        $doctor->setClientId($this->dataImporterFile->getClientID());
        $doctor->setQcDoctorCode(DoctorTable::checkDoctorCode('MD', $this->dataImporterFile->getClientID(), 1));
        $doctor->setLname('DOCTOR');
        $doctor->setFname('TEMPORARY');
        $doctor->setGroupNum($groupNum);
        $doctor->setProviderNum($providerNum);
        $doctor->setActive(true);
        $doctor->setSetupStage(6);
        $doctor->setProvince('ON');

        if ($specialty) {
            $doctor->setSpecCodeId($specialty);
        }

        $mohOffice = Doctrine_Query::create()
            ->from('MohOffice m')
            ->addWhere('m.city = (?)', array('KINGSTON'))
            ->limit(1)
            ->fetchOne();

        if ($mohOffice instanceof MOHOffice) {
            $doctor->moh_office_id = $mohOffice->get('id');
        }
        else {
            $mohOffice = Doctrine_Query::create()
                ->from('MohOffice m')
                ->orderBy('m.city asc')
                ->limit(1)
                ->fetchOne();

            if ($mohOffice instanceof MOHOffice) {
                $doctor->moh_office_id = $mohOffice->get('id');
            }
        }

        $doctor->save();
        return $doctor;
    }

    private function getReferringDoctor($data)
    {
        $this->log('hypeDaySheetVPLImporter.getReferringDoctor() called');

        $providerNum = $data[hypeDaySheetVPLImporter::CLAIM_REFDOC_NUM];
        $lname = $data[hypeDaySheetVPLImporter::CLAIM_REFDOC_LNAME];
        $fname = $data[hypeDaySheetVPLImporter::CLAIM_REFDOC_FNAME];

        $referringDoctor = Doctrine_Query::create()
            ->from('ReferringDoctor r')
            ->addWhere('r.client_id = (?)', array($this->dataImporterFile->getClientID()))
            ->addWhere('r.provider_num = (?)', $providerNum)
            ->fetchOne();

        if (!$referringDoctor instanceof ReferringDoctor) {
            $referringDoctor = new ReferringDoctor();
            $referringDoctor->setClientID($this->dataImporterFile->getClientID());
            $referringDoctor->setFname($fname);
            $referringDoctor->setLname($lname);
            $referringDoctor->setProviderNum($providerNum);
            $referringDoctor->checkForDuplicates();

            $referringDoctor->save();
        }

        return $referringDoctor;
    }

    private function buildClaimItem($data)
    {
        $this->log('hypeDaySheetVPLImporter.buildClaimItem() called.');

        $check_interval = false;
        $cp = ClientParamTable::getParamForClient($this->dataImporterFile->getClientID(), 'claim_item_interval_check');
        if ($cp instanceof ClientParam) {
            $check_interval = (bool) $cp->value;
        }

        $req_ref_doc = false;
        $interval = false;
        $req_diag_code = false;

        $item = new ClaimItem();
        $item->client_id = $this->dataImporterFile->getClientID();
        $item->service_code = $data[hypeDaySheetVPLImporter::ITEM_SERVICE_CODE];
        $item->diag_code = $data[hypeDaySheetVPLImporter::ITEM_DIAG_CODE];
        $item->service_date = $data[hypeDaySheetVPLImporter::ITEM_SERVICE_DATE];
        $item->item_id = count($this->claim->ClaimItem) + 1;
        $item->num_serv = $data[hypeDaySheetVPLImporter::ITEM_NUM_SERV];

        $item->service_code = $this->getQuickServiceCode($item->service_code);
        if (strlen($item->service_code) == 4) {
            $item->service_code .= 'A';
        }

        $this->log('Claim Item service code set to ' . $item->getServiceCode());
        $mode  = substr($item->service_code, 4, 1);
        $modifier = 100 * $item->num_serv;
        $item->setStatus(ClaimItemTable::STATUS_UNSUBMITTED);
        $item->setModifier($modifier);

        $service_code = $this->getServiceCode($item->service_code);
        if ($service_code instanceof ServiceCode) {
            $req_diag_code = $service_code->getReqDiagCode();
            $req_ref_doc = $service_code->getReqRefDoctor();
            $interval = $service_code->getInterVal();
        }
        else {
            $this->is_valid = false;
            $this->log('Service Code ' . $item->getServiceCode() . ' is invalid.');
        }

        $this->log('Claim Item diagnostic code set to ' . $item->getDiagCode());
        if ($req_diag_code && !$item->getDiagCode()) {
            $this->is_valid = false;
            $this->log('A diagnostic code is required for this service code to be valid.');
        }

        if ($req_ref_doc && !$this->claim->ref_doc_id) {
            $this->is_valid = false;
            $this->log('A referring doctor is required for this service code to be valid.');
        }

        $this->log('Claim Item service date set to ' . $data[hypeDaySheetVPLImporter::ITEM_SERVICE_DATE]);
        $this->log('Claim Item status set to unsubmitted.');
        $this->log('Claim Item item id set to ' . $item->getItemId());
        $this->log('Claim Item number of services set to ' . $item->getNumServ());
        $this->log('Claim Item modifier set to ' . $item->getModifier());

        if ($interval && $check_interval) {
            $prev_items = $this->claim->Patient->checkForPreviousServices($item->service_date, $service_code->inter_val, $service_code->service_code);

            if (count($prev_items)) {
                $this->is_valid = false;
                $this->log('Cannot bill this service again within a ' . $interval . ' interval');
            }
        }

        if ($service_code instanceof ServiceCode) {
            $item->setBaseFee($service_code->determineFee($mode, $this->claim->Doctor->spec_code_id));
            $item->setFeeSubm(number_format($item->getModifier() * $item->getBaseFee() / 100, 2, '.', ''));
            $this->log('Claim Item fee_subm set to ' . $item->getFeeSubm());
            $this->log('Claim Item base_fee set to ' . $item->getBaseFee());
        }

        $this->claim->ClaimItem->add($item);
    }

    private function getServiceCode($service_code)
    {
        $this->log('hypeDaySheetVPLImporter.getServiceCode(' . $service_code . ') called');

        if ($service_code) {
            return Doctrine_Query::create()
                ->from('ServiceCode sc')
                ->addWhere('sc.service_code = (?)', substr($service_code, 0, 4))
                ->fetchOne();
        }

        return null;
    }

    private function duplicateServicesCheck(ClaimItem $item)
    {
        $this->log('hypeDaySheetVPLImporter.duplicateServicesCheck called');

        $query = Doctrine_Query::create()
            ->from('ClaimItem ci')
            ->leftJoin('ci.Claim c')
            ->addWhere('c.client_id = (?)', array($this->dataImporterFile->getClientID()))
            ->addWhere('c.deleted = (?)', array(false))
            ->addWhere('ci.status != (?)', array(ClaimItemTable::STATUS_DELETED))
            ->addWhere('ci.service_date = (?)', array($item->getServiceDate()))
            ->addWhere('ci.service_code = (?)', array($item->service_code));

        if ($item->Claim->patient_id) {
            $query->addWhere('c.patient_id = (?)', array($item->Claim->patient_id));
        }
        else {
            $query->addWhere('c.health_num = (?)', hype::encryptHCN($item->Claim->health_num));
            $query->addWhere('c.lname = (?)', array($item->Claim->getLname()));
            $query->addWhere('c.fname = (?)', array($item->Claim->getFname()));
        }
        $duplicates = $query->execute();

        if (count($duplicates)) {
            $this->log(count($duplicates) . ' possible duplicates found on same day.');

            foreach ($duplicates as $dup) {
                if ($dup->getServiceCode() == $item->getServiceCode()) {
                    $this->log('Claim Item ' . ($item->item_id) . ': ' . $dup->getServiceCode() . ' is a duplicate. Removing claim item.');

                    return false;
                }
            }
        }
        $this->log('Claim Item ' . ($item->item_id) . ': ' . $item->getServiceCode() . '; no duplicates found.');

        return true;
    }

    private function mergeIntoExistingClaim(Claim $claim)
    {
        $this->log('hypeDaySheetVPLImporter.mergeIntoExistingClaim() called');

        if (!count($claim->ClaimItem)) {
            return $claim;
        }

        $canMerge = true;
        foreach ($claim->ClaimItem as $claimItem) {
            if ($claimItem->getServiceCode() == 'Z771A') {
                $canMerge = false;
            }
        }

        if (!$canMerge) {
            return $claim;
        }

        $q = Doctrine_Query::create()
            ->from('Claim c')
            ->leftJoin('c.ClaimItem ci')
            ->addWhere('c.health_num = (?)', array(hype::encryptHCN($claim->health_num)))
            ->addWhere('c.patient_id = (?)', array($claim->getPatientId()))
            ->addWhere('c.doctor_id = (?)', array($claim->getDoctorId()))
            ->addWhere('c.pay_prog = (?)', array($claim->getPayProg()))
            ->addWhere('c.batch_id is null')
            ->addWhere('ci.service_date = (?)', array($claim->ClaimItem[0]->getServiceDate()))
            ->addWhere('c.deleted = (?)', array(false));

        if ($claim->getSli()) {
            $q->addWhere('c.sli = (?)', $claim->getSli());
        }
        else {
            $q->addWhere('c.sli = (?) OR c.sli IS NULL', '');
        }

        $oldClaims = $q->execute();

        foreach ($oldClaims as $oldClaim) {
            foreach ($claim->ClaimItem as $claimItem) {
                $claimItem->setItemId(count($oldClaim->ClaimItem) + 1);
                $oldClaim->ClaimItem->add($claimItem);

                $this->log('Claim Item Added to Claim ID ' . $oldClaim->getId());
            }

            return $oldClaim;
        }

        return $claim;
    }

    private function getAppointmentAttendeesForClaim()
    {
        $this->log('hypeDaySheetVPLImporter.getAppointmentAttendeesForClaim() called');

        $rs = new Doctrine_Collection('AppointmentAttendee');

        $patient_id = $this->claim->Patient->get('id');
        $doctor_id = $this->claim->Doctor->get('id');
        $fname = $this->claim->getFname();
        $lname = $this->claim->getLname();

        foreach ($this->claim->ClaimItem as $item) {
            $date = $item->getServiceDate();

            $appointment = null;
            if ($patient_id) {
                $appointment = $this->getAttendeeByPatientID($patient_id, $doctor_id, $this->claim->getClientId(), $date);
            }
            else {
                $appointment = $this->getAttendeeByPatientName($fname, $lname, $doctor_id, $this->claim->getClientId(), $date);
            }

            if ($appointment instanceof AppointmentAttendee) {
                $rs->add($appointment);
            }
        }

        return $rs;
    }

    private function getAttendeeByPatientID($patient_id, $doctor_id, $client_id, $date)
    {
        $this->log('hypeDaySheetVPLImporter.getAttendeeByPatientID(' . $patient_id . ', ' . $doctor_id . ', ' . $client_id . ', ' . $date . ')');

        return Doctrine_Query::create()
            ->from('AppointmentAttendee att')
            ->leftJoin('att.Appointment a')
            ->addWhere('a.client_id = (?)', $client_id)
            ->addWhere('att.client_id = (?)', array($client_id))
            ->addWhere('att.patient_id = (?)', $patient_id)
            ->addWhere('a.start_date between (?) and (?)', array($date . ' 00:00:00', $date . ' 23:59:59'))
            ->addWhere('att.claim_id IS NULL')
            ->addWhere('a.status = (?) OR a.status = (?)', array(AppointmentTable::STATUS_ATTENDED, AppointmentTable::STATUS_POSTED))
            ->fetchOne();
    }

    private function getAttendeeByPatientName($fname, $lname, $doctor_id, $client_id, $date)
    {
        $this->log('hypeDaySheetVPLImporter.getAttendeeByPatientName(' . $fname . ', ' . $lname . ', ' . $doctor_id . ', ' . $client_id . ', ' . $date . ')');

        return Doctrine_Query::create()
            ->from('AppointmentAttendee att')
            ->leftJoin('att.Appointment a')
            ->addWhere('a.client_id = (?)', $client_id)
            ->addWhere('att.client_id = (?)', array($client_id))
            ->addWhere('att.patient_fname = (?)', $fname)
            ->addWhere('att.patient_lname = (?)', $lname)
            ->addWhere('a.start_date between (?) and (?)', array($date . ' 00:00:00', $date . ' 23:59:59'))
            ->addWhere('att.claim_id IS NULL')
            ->addWhere('a.status = (?) OR a.status = (?)', array(AppointmentTable::STATUS_ATTENDED, AppointmentTable::STATUS_POSTED))
            ->fetchOne();
    }

    private function getQuickServiceCode($service_code)
    {
        $this->log('getQuickServiceCode called upon ' . $service_code);

        $qsc = Doctrine_Query::create()
            ->from('QuickServiceCode qsc')
            ->addWhere('qsc.client_id = (?)', array($this->dataImporterFile->getClientId()))
            ->addWhere('qsc.quick_code = (?)', $service_code)
            ->fetchOne();

        if ($qsc instanceof QuickServiceCode) {
            $qscs = explode(',', $qsc);

            if (count($qscs) > 1) {

                //If there is more than one quick service code
                $this->log('Multi codes for qsc ' . $service_code);

                $this->log('First QSC: ' . $qscs[0]);
                $this->log('Second QSC: ' . $qscs[1]);

                $cols = array('id', 'code');
                $quick_service_code = $qscs[0]->getGenericJSON($cols);

                return $quick_service_code['code'];
            }

            $cols = array('id', 'code');
            $quick_service_code = $qsc->getGenericJSON($cols);

            return $quick_service_code['code'];
        } else {
            return $service_code;
        }
    }

}

