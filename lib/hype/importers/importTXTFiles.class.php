<?php
class importTXTFiles
{
    private $client_id;
    private $location_id;
    private $doctor_id;
    private $file;
    private $patients_ID;
    private $count;
    private $headersFound = false;
    private $headers = array ();
    private $headLen = array ();
    private $debug = false;
    public function __construct($file, $client_id, $location_id, $doctor_id) {

        ini_set ('memory_limit', -1);
        date_default_timezone_set ( 'America/Toronto' );
        $this->client_id = $client_id;
        $this->file = $file;
        $this->location_id = $location_id;
        $this->doctor_id = $doctor_id;
        $this->patients_ID = array ();
    }
    private function setupEtobicokeHeaders() {
        $this->headers = array (
            'name' => 20,
            'sex' => 50,
            'dob' => 51,
            'address' => 65,
            'city' => 94,
            'postal_code' => 109,
            'phone' => 116,
            'hcn' => 130,
            'pat_number' => 1
        );
        $this->headLen = array (
            'name' => 30,
            'sex' => 1,
            'dob' => 10,
            'address' => 29,
            'city' => 15,
            'postal_code' => 7,
            'phone' => 12,
            'hcn' => 13,
            'pat_number' => 9
        );
    }
    public function setDebug($v) {
        $this->debug = $v;
    }
    public function getDebug() {
        return $this->debug;
    }
    public function getPatientsId() {
        return $this->patients_ID;
    }
    private function setCount($val) {
        $this->count = $val;
    }
    public function getCount() {
        return $this->count;
    }
    public function findDuplicated($hcn, $client_id) {
        if (strlen ( $hcn ) > 10) {
            $patients = Doctrine_Query::create ()->from ( 'Patient p' )
                ->addWhere ( 'p.hcn_num = (?)', $hcn )
                ->addWhere ( 'p.client_id = (?)', $client_id )
                ->addWhere ( 'p.deleted = (?)', false )
                ->execute ();
            return $patients;
        } else
            return null;
    }
    public function findDuplicatedByPatientNumber($patient) {
        if ($patient instanceof Patient) {
            $patients = Doctrine_Query::create ()
                ->from ( 'Patient p' )
                ->addWhere ( 'p.patient_number = (?)', $patient->patient_number )
                ->addWhere ( 'p.lname = (?)', $patient->lname )
                ->addWhere ( 'p.fname = (?)', $patient->fname )
                ->addWhere ( 'p.client_id = (?)', $patient->client_id )
                ->addWhere ( 'p.deleted = (?)', false )
                ->execute ();
            return $patients;
        } else {
            return null;
        }
    }
    public function findHeaders($row) {
        if (substr ( $row, 0, 4 ) == 'NAME') {
            $this->headersFound = true;
            $this->headers ['name'] = 0;

            if (stripos ( $row, 'sex' ) !== false) {
                $this->headers ['sex'] = stripos ( $row, 'sex' );
                $this->headLen ['sex'] = 1;
                $this->headLen ['name'] = $this->headers ['sex'];
            }
            if (stripos ( $row, 'BIRTHDATE' ) !== false) {
                $this->headers ['dob'] = stripos ( $row, 'BIRTHDATE' );
                $this->headLen ['dob'] = 8;
            }
            if (stripos ( $row, 'AGE' ) !== false)
                $this->headers ['age'] = stripos ( $row, 'AGE' );
            if (stripos ( $row, 'DATE' ) !== false)
                $this->headers ['service_date'] = stripos ( $row, 'DATE' );
            if (stripos ( $row, 'INS' ) !== false)
                $this->headers ['province'] = stripos ( $row, 'INS' );
            if (stripos ( $row, 'HEALTH CARE' ) !== false)
                $this->headers ['hcn'] = stripos ( $row, 'HEALTH CARE' );
        }
    }
    public function run() {
        switch ($this->client_id) {
            //case 11 :
            //    $this->processAndruku ();
            //    break;
            case 54 :
            case 55 :
            case 56 :
            case 57 :
            case 47 :
            case 35 :
                $this->processDonnaCurtis ();
                break;
            case 64 :
                $this->processBlecher ();
                break;
            case 11 :
            case 67 :
                $this->processEtobicoke ();
                break;
        }
    }
    public function processBlecher() {
        $txt_file = file_get_contents ( $this->file->getTempName () );
        $rows = explode ( "\n", $txt_file );
        $patient_type_id = null;
        $patient_status_id = null;
        $cp = Doctrine::getTable ( 'ClientParam' )->getParamForClient ( $this->client_id, 'patient_status_default' );
        if ($cp instanceof ClientParam) {
            $patient_status_id = $cp->value;
        }
        $cp = Doctrine::getTable ( 'ClientParam' )->getParamForClient ( $this->client_id, 'default_patient_type_id' );
        if ($cp instanceof ClientParam) {
            $patient_type_id = $cp->value;
        }
        $lastRow = false;
        $this->patients_ID = null;
        foreach ( $rows as $row ) {
            if (! $this->headersFound) {
                $this->findHeaders ( $row );
            } else {
                $duplicates = array ();
                if (($this->isValidRow ( $row )) && ! $lastRow) {
                    // print_r($row);
                    $patient = new Patient ();
                    // ---------------------------/
                    $patient->client_id = $this->client_id;
                    $patient->location_id = $this->location_id;
                    // ---------------------------/
                    $names = explode ( ',', substr ( $row, $this->headers ['name'], $this->headers ['sex'] ) );
                    if (isset ( $names [0] ))
                        $patient->lname = $names [0];
                    if (isset ( $names [1] ))
                        $patient->fname = $names [1];
                    $patient->full_name = $patient->fname . ' ' . $patient->lname;
                    $patient->sex = substr ( $row, $this->headers ['sex'] + 1, 1 );
                    $date = substr ( $row, $this->headers ['dob'], 8 );
                    $day = substr ( $date, 0, 2 );
                    $month = substr ( $date, 3, 2 );
                    $year = substr ( $date, 6, 2 );
                    $y = date ( 'y' );
                    if ($year > $y)
                        $year = 1900 + $year;
                    if ($year == '00')
                        $year = '2000';
                    $patient->dob = date ( hype::DB_DATE_FORMAT, mktime ( 0, 0, 0, $month, $day, $year ) );

                    $patient->deleted = false;
                    $patient->active = true;
                    $patient->patient_type_id = $patient_type_id;
                    $patient->patient_status_id = $patient_status_id;
                    if ((stripos ( $row, 'OHIP' ) !== false) || (stripos ( $row, 'WCB' ) !== false)) {
                        $patient->province = 'ON';
                        $patient->country = 'CAN';
                    }
                    $note = new Notes ();
                    $note->client_id = $this->client_id;
                    $note->note = 'Imported from file' . $this->file;
                    $hcnum = explode ( '-', substr ( $row, $this->headers ['hcn'] ) );

                    if (isset ( $hcnum [0] )) {
                        $patient->hcn_num = hype::encryptHCN ( $hcnum [0] );
                        if (isset ( $hcnum [1] ))
                            $patient->hcn_version_code = $hcnum [1];
                        $duplicates = $this->findDuplicated ( $patient->hcn_num, $patient->getClientId () );
                        // if ($this->debug) echo 'amount of duplicates entries for this patient --> ' . count($duplicates) . "\n";
                    } else {
                        $note->note = $note->note . "\n" . '  There was no Health Card Number to import.';
                    }
                    $patient->Notes->add ( $note );
                    if ($duplicates) {
                        foreach ( $duplicates as $patient_row ) {
                            $this->patients_ID [] = $patient_row->getId ();
                        }
                    }

                    if (!count($duplicates)) {
                        $duplicates = $this->findDuplicatedByPatientNumber($patient);
                        if (!count($duplicates)) {
                            try {
                                $patient->save ();
                                $this->patients_ID [] = $patient->getId ();
                            } catch ( Exception $e ) {
                                if ($this->debug) {
                                    echo $e->getMessage () . "\n";
                                    print_r ( $patient->toarray () );
                                    exit ();
                                }
                            }
                        }
                    } else {
                        if ($this->debug)
                            echo 'already in data base. ' . "\n";
                    }
                    if (count($duplicates)){
                        //updating version code
                        foreach ($duplicates as $pat)
                        {
                            if  ($pat instanceof Patient){
                                $pat->hcn_version_code = $patient->hcn_version_code;
                                $pat->save();
                            }
                        }
                    }
                    $this->count ++;
                    if ($this->debug){
                        echo 'Patient Name: ' . $patient->full_name . "\n";
                        echo 'Patient HCN  : ' . $patient->hcn_num . "\n";
                        echo 'Patient DOB  : ' . $patient->dob . "\n";}
                } else {
                    if (substr ( $row, 0, 5 ) == 'TOTAL') {
                        $lastRow = true;
                        break;
                    }
                }
            }
        }
        if ($this->debug) {
            echo 'Total Patients Processed: ' . $this->count . "\n";
            // var_dump($this->patients_ID);
            // exit;
        }
    }
    public function processDonnaCurtis() {
        print_r ($this->doctor_id);
        $txt_file = file_get_contents ( $this->file->getTempName () );
        $rows = explode ( "\n", $txt_file );

        $patient_type_id = null;
        $patient_status_id = null;

        $cp = Doctrine::getTable ( 'ClientParam' )->getParamForClient ( $this->client_id, 'patient_status_default' );
        if ($cp instanceof ClientParam) {
            $patient_status_id = $cp->value;
        }

        $cp = Doctrine::getTable ( 'ClientParam' )->getParamForClient ( $this->client_id, 'default_patient_type_id' );
        if ($cp instanceof ClientParam) {
            $patient_type_id = $cp->value;
        }

        foreach ( $rows as $row ) {
            $duplicates = array ();

            if (strlen ( $row ) > 100) {
                $patient = new Patient ();
                // ---------------------------/
                $patient->client_id = $this->client_id;
                $patient->location_id = $this->location_id;
                // ---------------------------/

                $patient->patient_number = strtoupper ( trim ( substr ( $row, 0, 9 ) ) );
                $patient->lname = strtoupper ( trim ( substr ( $row, 9, 24 ) ) );
                $patient->fname = strtoupper ( trim ( substr ( $row, 33, 16 ) ) );
                $patient->sex = strtoupper ( trim ( substr ( $row, 52, 1 ) ) );
                $dob = strtoupper ( trim ( substr ( $row, 53, 8 ) ) );
                $patient->dob = substr ( $dob, 0, - 4 ) . '-' . substr ( $dob, 4, - 2 ) . '-' . substr ( $dob, - 2 );
                $patient->address = strtoupper ( trim ( substr ( $row, 82, 34 ) ) );
                $patient->city = strtoupper ( trim ( substr ( $row, 116, 20 ) ) );
                $patient->province = strtoupper ( trim ( substr ( $row, 139, 4 ) ) );
                $patient->postal_code = strtoupper ( trim ( substr ( $row, 143, 10 ) ) );
                $patient->hphone = strtoupper ( trim ( substr ( $row, 154, 10 ) ) );
                $patient->wphone = strtoupper ( trim ( substr ( $row, 164, 10 ) ) );
                $patient->hcn_version_code = strtoupper ( trim ( substr ( $row, 192, 5 ) ) );
                $service_date = strtoupper ( trim ( substr ( $row, 260, 8 ) ) );
                if ($service_date) {
                    $service_date = substr ( $service_date, 0, - 4 ) . '-' . substr ( $service_date, 4, - 2 ) . '-' . substr ( $service_date, - 2 );
                }
                $date = strtoupper ( trim ( substr ( $row, 202, 10 ) ) );
                if ($date) {
                    $patient->hcn_exp_year = substr ( $date, 0, - 4 ) . '-' . substr ( $date, 4, - 2 ) . '-' . substr ( $date, - 2 );
                }
                $patient->patient_type_id = $patient_type_id;
                $patient->patient_status_id = $patient_status_id;

                $hcnum = strtoupper ( trim ( substr ( $row, 180, 11 ) ) );
                if ($this->debug)
                    echo 'HC num ' . $hcnum . "\n";
                $note = new Notes ();
                $note->client_id = $this->client_id;
                $note->note = 'Imported from file' . $this->file->getOriginalName ();
                if (strlen ( $hcnum ) > 1) {
                    $patient->hcn_num = hype::encryptHCN ( $hcnum );
                    $duplicates = $this->findDuplicated ( $patient->hcn_num, $patient->getClientId () );
                    if ($this->debug)
                        echo 'amount of duplicates entries for this patient --> ' . count ( $duplicates ) . "\n";
                } else {
                    $note->note = $note->note . "\n" . '  There was no Health Card Number to import.';
                }
                $patient->Notes->add ( $note );

                if (count ($duplicates)) {

                    $this->createAppointment($this->client_id, $this->location_id, $this->doctor_id, $duplicates[0], $service_date);
                    foreach ( $duplicates as $patient_row ) {
                        $this->patients_ID [] = $patient_row->getId ();
                    }
                }
                if ($this->debug)
                    echo 'Duplicates = ' . count ( $duplicates ) . "\n";

                if (!count ( $duplicates )) {
                    $duplicates = $this->findDuplicatedByPatientNumber($patient);
                    if (count ($duplicates)){
                        $this->createAppointment($this->client_id, $this->location_id, $this->doctor_id, $duplicates[0], $service_date);
                    }

                    if (!count($duplicates)) {
                        try {
                            $patient->save ();
                            $this->patients_ID [] = $patient->getId ();
                            $this->createAppointment($this->client_id, $this->location_id, $this->doctor_id, $patient, $service_date);
                        } catch ( Exception $e ) {
                            if ($this->debug) {
                                echo $e->getMessage () . "\n";
                                print_r ( $patient->toarray () );
                                exit ();
                            }
                        }
                    }
                } else {
                    if ($this->debug)
                        echo 'already in data base. ' . "\n";
                }
                if (count($duplicates)){
                    //updating version code
                    foreach ($duplicates as $pat)
                    {
                        if  ($pat instanceof Patient){
                            $pat->hcn_version_code = $patient->hcn_version_code;
                            $pat->save();
                        }
                    }
                }
                $this->count ++;
            }
        }
    }
    /*
    public function processAndruku() {
        $txt_file = file_get_contents ( $this->file->getTempName () );
        $rows = explode ( "\n", $txt_file );

         $patient_type_id = null;
         $patient_status_id = null;

        $cp = Doctrine::getTable ( 'ClientParam' )->getParamForClient ( $this->client_id, 'patient_status_default' );
         if ($cp instanceof ClientParam) {
             $patient_status_id = $cp->value;
         }

        $cp = Doctrine::getTable ( 'ClientParam' )->getParamForClient ( $this->client_id, 'default_patient_type_id' );
         if ($cp instanceof ClientParam) {
             $patient_type_id = $cp->value;
         }

        foreach ( $rows as $row ) {
            $duplicates = array ();

            if (strlen ( $row ) > 100) {
                $patient = new Patient ();
                // ---------------------------/
                $patient->client_id = $this->client_id;
                 $patient->location_id = $this->location_id;
                // ---------------------------/

                $patient->patient_number = strtoupper ( trim ( substr ( $row, 0, 9 ) ) );
                //$patient->lname = strtoupper ( trim ( substr ( $row, 9, 24 ) ) );
                //$patient->fname = strtoupper ( trim ( substr ( $row, 33, 16 ) ) );
                $name = explode(',', trim ( substr ( $row, 20, 28 ) ) );
                $patient->lname = strtoupper($name[0]);
                $patient->fname = strtoupper($name[1]);


                $patient->sex = strtoupper ( trim ( substr ( $row, 48, 1 ) ) );
                $dob = strtoupper ( trim ( substr ( $row, 49, 10 ) ) );
                $patient->dob = substr ( $dob, 6, 4 ) . '-' . substr ( $dob, 3, 2 ) . '-' . substr ( $dob, 0, 2 );
                $patient->address = strtoupper ( trim ( substr ( $row, 62, 30 ) ) );
                $patient->city = strtoupper ( trim ( substr ( $row, 92, 13 ) ) );
                //$patient->province = strtoupper ( trim ( substr ( $row, 139, 4 ) ) );
                $patient->province = 'ON';
                $patient->postal_code = strtoupper ( trim ( substr ( $row, 105, 7 ) ) );
                $homephone = strtoupper ( trim ( substr ( $row, 112, 13 ) ) );

                $patient->hphone = substr ( $homephone, 1, 3 ) . substr ( $homephone, 5, 3 ) . substr ( $homephone, 9, 4 );
                //$patient->wphone = strtoupper ( trim ( substr ( $row, 164, 10 ) ) );
                $patient->hcn_version_code = strtoupper ( trim ( substr ( $row, 137, 2 ) ) );
                //$date = strtoupper ( trim ( substr ( $row, 202, 10 ) ) );
                 //if ($date) {
                //	$patient->hcn_exp_year = substr ( $date, 0, - 4 ) . '-' . substr ( $date, 4, - 2 ) . '-' . substr ( $date, - 2 );
                 //}
                 $patient->patient_type_id = $patient_type_id;
                 $patient->patient_status_id = $patient_status_id;

                $hcnum = strtoupper ( trim ( substr ( $row, 126, 10 ) ) );
                if ($this->debug)
                    echo 'HC num ' . $hcnum . "\n";
                $note = new Notes ();
                 $note->client_id = $this->client_id;
                $note->note = 'Imported from file' . $this->file->getOriginalName ();
                if (strlen ( $hcnum ) > 1) {
                    $patient->hcn_num = hype::encryptHCN ( $hcnum );
                    $duplicates = $this->findDuplicated ( $patient->hcn_num, $patient->getClientId () );
                    if ($this->debug)
                        echo 'amount of duplicates entries for this patient --> ' . count ( $duplicates ) . "\n";
                } else {
                    $note->note = $note->note . "\n" . '  There was no Health Card Number to import.';
                }
                $patient->Notes->add ( $note );
                foreach ( $duplicates as $patient_row ) {
                    $this->patients_ID [] = $patient_row->getId ();
                }
                if ($this->debug)
                    echo 'Duplicates = ' . count ( $duplicates ) . "\n";

                if (! count ( $duplicates )) {
                    $duplicates = $this->findDuplicatedByPatientNumber($patient);
                    if (!count($duplicates)) {
                        try {
                            $patient->save ();
                            $this->patients_ID [] = $patient->getId ();
                            //$this->createAppointment($this->client_id, $this->location_id, $patient->getId());
                        } catch ( Exception $e ) {
                            if ($this->debug) {
                                echo $e->getMessage () . "\n";
                                print_r ( $patient->toarray () );
                                exit ();
                            }
                        }
                 }
                } else {
                    if ($this->debug)
                        echo 'already in data base. ' . "\n";
                 }
                if (count($duplicates)){
                    //updating version code
                    foreach ($duplicates as $pat)
                 {
                        if  ($pat instanceof Patient){
                            $pat->hcn_version_code = $patient->hcn_version_code;
                            $pat->save();
                        }
                    }
                }
                $this->count ++;
            }
        }
    }
    */

    public function processEtobicoke() {
        $this->setupEtobicokeHeaders ();
        $txt_file = file_get_contents ( $this->file->getTempName () );
        $rows = explode ( "\n", $txt_file );
        $patient_type_id = null;
        $patient_status_id = null;

        $cp = Doctrine::getTable ( 'ClientParam' )->getParamForClient ( $this->client_id, 'patient_status_default' );
        if ($cp instanceof ClientParam) {
            $patient_status_id = $cp->value;
        }

        $cp = Doctrine::getTable ( 'ClientParam' )->getParamForClient ( $this->client_id, 'default_patient_type_id' );
        if ($cp instanceof ClientParam) {
            $patient_type_id = $cp->value;
        }
        foreach ( $rows as $row ) {
            if (strlen ( $row ) > 100) {

                $hcn = strtoupper ( trim ( substr ( $row, $this->headers ['hcn'], $this->headLen ['hcn'] ) ) );
                $parts = explode ( '-', $hcn );
                if (isset ( $parts [0] )) {
                    // $patient->hcn_num = hype::encryptHCN($parts[0]);
                    $hcn = hype::encryptHCN ( $parts [0] );
                    $patients = $this->findDuplicated ( $hcn, $this->client_id );
                    if (count ( $patients ) > 0) {
                        $note = '';
                        $i = 0;
                        foreach ( $patients as $p ) {
                            if ($i > 0) {
                                $note = $note . ' patient info may be duplicated in patient with id = ' . $p->getId () . "\n";
                                echo 'patient -> ' . $i . "\n";
                            }
                            $i ++;
                        }
                        $this->log ( $patients [0]->full_name . ' already exist' );
                        $patient = $patients [0];
                        // print_r($patient->toArray());
                        // exit;
                    } else {
                        $patient = new Patient ();
                        $this->log ( 'New Patient' );
                        $patient->last_4_hcn = substr ( $parts [0], - 4 );
                        $patient->location_id = $this->location_id;
                    }
                }

                // $patient = new Patient();
                if (isset ( $this->doctor_id ))
                    $patient->doctor_id = $this->doctor_id;
                // ---------------------------/
                $patient->client_id = $this->client_id;
                $patient->location_id = $this->location_id;
                // ---------------------------/

                $patient->patient_number = strtoupper ( trim ( substr ( $row, $this->headers ['pat_number'], $this->headLen ['pat_number'] ) ) );
                if (isset ( $this->headers ['name'] )) {
                    $names = strtoupper ( trim ( substr ( $row, $this->headers ['name'], $this->headLen ['name'] ) ) );
                    $names = explode ( ',', $names );
                    $patient->lname = $names [0];
                    $patient->fname = $names [1];
                } else {
                    $patient->lname = strtoupper ( trim ( substr ( $row, $this->headers ['lname'], $this->headLen ['lname'] ) ) );
                    $patient->fname = strtoupper ( trim ( substr ( $row, $this->headers ['fname'], $this->headLen ['fname'] ) ) );
                }
                $patient->full_name = $patient->fname . ' ' . $patient->lname;
                $patient->sex = strtoupper ( trim ( substr ( $row, $this->headers ['sex'], $this->headLen ['sex'] ) ) );
                $date = strtoupper ( trim ( substr ( $row, $this->headers ['dob'], $this->headLen ['dob'] ) ) );
                $oldDateTimeFormat = DateTime::createFromFormat ( 'd/m/Y', $date );
                $date = $oldDateTimeFormat->format ( hype::DB_DATE_FORMAT );

                $patient->dob = $date;
                $patient->address = strtoupper ( trim ( substr ( $row, $this->headers ['address'], $this->headLen ['address'] ) ) );
                $patient->city = strtoupper ( trim ( substr ( $row, $this->headers ['city'], $this->headLen ['city'] ) ) );
                $patient->province = 'ON';
                $patient->postal_code = strtoupper ( trim ( substr ( $row, $this->headers ['postal_code'], $this->headLen ['postal_code'] ) ) );
                $patient->hphone = strtoupper ( trim ( substr ( $row, $this->headers ['phone'], $this->headLen ['phone'] ) ) );
                $hcn = strtoupper ( trim ( substr ( $row, $this->headers ['hcn'], $this->headLen ['hcn'] ) ) );
                $parts = explode ( '-', $hcn );
                if (isset ( $parts [0] )) {
                    $patient->hcn_num = hype::encryptHCN ( $parts [0] );
                    $patient->last_4_hcn = substr ( $parts [0], - 4 );
                }
                if (isset ( $parts [1] ))
                    $patient->hcn_version_code = $parts [1];
                if (isset ( $patient->hcn_num )) {
                    $patient->province = 'ON';
                    $patient->country = 'CAN';
                }

                $patient->save ();
                $this->patients_ID[] = $patient->getId ();
                // print_r($patient->toArray());
                // exit;
                // print_r($patient->toArray());
                // exit;
            }
        }
    }
    public function log($text) {
        if ($this->debug)
            echo $text . "\n";
    }
    public function isValidRow($row) {
        $valid = false;
        if (strlen ( $row ) < 100) {
            return $valid;
        }
        $date = substr ( $row, $this->headers ['dob'], 8 );
        $day = substr ( $date, 0, 2 );
        $month = substr ( $date, 3, 2 );
        $year = substr ( $date, 6, 2 );
        $y = date ( 'y' );
        if ($year > $y)
            $year = 1900 + $year;
        if ($year == '00')
            $year = '2000';
        $valid = checkdate ( $month, $day, $year );
        if (! $valid) {
            print_r ( $row );
        }
        return $valid;
    }
    public function getMessage() {
        $s = 'Process completed successfully.';
        if ($this->count > 0)
            $s = $s . ' ' . $this->count . ' Patients were processed.';
        return $s;
    }

    public function createAppointment($client_id, $location_id, $doctor_id, $patient, $service_date)
    {
        $appointmentTypeId = $this->getAppointmentTypeID($client_id);
        $appointmentLength = $this->getAppointmentLength($client_id);

        //All appointments will have a status of attended
        $attendedStageClientID = $this->getAttendedStageClientID($client_id);

        $appointment = new Appointment();
        $appmtattendee = new AppointmentAttendee();
        $appmtattendee->setClientId($client_id);
        $appointment->client_id = $client_id;
        $appointment->slot_type = AppointmentTable::SLOT_TYPE_NORMAL;
        $appointment->location_id = $this->location_id;
        $appointment->appointment_type_id = $appointmentTypeId;

        $appstarttime = '00:00:00';
        $appendtime = '00:00:' . $appointmentLength;
        $appointment->start_date = $service_date . ' ' . $appstarttime;
        $appointment->end_date = $service_date . ' ' .$appendtime;
        $appointment->status = AppointmentTable::STATUS_ATTENDED;
        $appointment->stage_client_id = $attendedStageClientID;

        $appointment->doctor_id = $this->doctor_id;
        $appointment->doctor_code = $this->getDoctorCode($doctor_id);

        $appmtattendee->patient_fname = $patient->fname;
        $appmtattendee->patient_lname = $patient->lname;
        $appmtattendee->patient_phone = $patient->hphone;
        $appmtattendee->patient_id = $patient->getId();
        $appmtattendee->patient_email = $patient->email;

        $appmtattendee->status = AppointmentAttendeeTable::STATUS_ATTENDED;


        $appointment->AppointmentAttendee->add($appmtattendee);
        try{
            $appointment->save();
        } catch (Exception $e)
        {
            print_r($appointment->toArray());
            echo $e;
            exit;
        }
    }




    private function getAppointmentTypeID($client_id) {
        $appointmentType = Doctrine_Query::create()
            ->from('AppointmentType at')
            ->addWhere('at.client_id = (?)', $this->client_id)
            ->addWhere('at.name = (?)', 'Default Appointment')
            ->fetchOne();

        if (!$appointmentType instanceof AppointmentType) {
            $appointmentType = new AppointmentType();
            $appointmentType->client_id = $this->client_id;
            $appointmentType->slot_type = AppointmentTable::SLOT_TYPE_NORMAL;
            $appointmentType->name = 'Default Appointment';
            $appointmentType->appt_lengths = 10;
            $appointmentType->colour = '#ff0000';
            $appointmentType->title_colour = '#bb0000';
            $appointmentType->save();
        }

        return $appointmentType->id;
    }

    private function getAppointmentLength($client_id) {
        $appointmentType = Doctrine_Query::create()
            ->from('AppointmentType at')
            ->addWhere('at.client_id = (?)', $this->client_id)
            ->addWhere('at.name = (?)', 'Default Appointment')
            ->fetchOne();

        return $appointmentType->getApptLengths();

    }

    private function getAttendedStageClientID($client_id)
    {
        $stageClient = Doctrine_Query::create()
            ->from('StageClient sc')
            ->addWhere('sc.client_id = (?)', $this->client_id)
            ->addWhere('sc.stage_name = (?)', 'ATTENDED')
            ->fetchOne();

        if (!$stageClient instanceof StageClient) {
            echo 'No ATTENDED Stage Client record. Go make one.' . "\n";
            exit();
        }

        return $stageClient->id;
    }

    private function getDoctorCode($doctor_id) {
        $doctor = Doctrine_Query::create()
            ->from('Doctor d')
            ->addWhere('d.id = (?)', $this->doctor_id)
            ->fetchOne();

        return $doctor->qc_doctor_code;

    }

}
