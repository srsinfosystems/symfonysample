<?php

abstract class hypeFileImporterBase
{
    protected $dataImporterFile = null;

    /*
     * @param DataImporterFile
     */
    protected $log = array();

    /*
     * @param String[]
     */

    public function __construct(DataImporterFile $dataImporterFile)
    {
        $this->dataImporterFile = $dataImporterFile;
    }

    abstract public function run();

    protected function getDataImporter($id)
    {
        $this->log('hypeFileImporterBase.getDataImporter() called');

        return Doctrine_Query::create()
            ->from('DataImporter d')
            ->addWhere('d.data_importer_file_id = (?)', array($id))
            ->addWhere('d.status = (?)', array(DataImporterTable::STATUS_UNPARSED))
            ->orderBy('d.id asc')
            ->limit(1)
            ->fetchOne();
    }

    protected function log($str)
    {
        echo $str . "\n";
        $this->log[] = $str;
    }

    protected function getLog()
    {
        return $this->log;
    }

    protected function resetLog()
    {
        echo "\n";
        $this->log = array();
    }

    protected function formatText($str)
    {
        return strtoupper(iconv("UTF-8", "UTF-8//IGNORE", $str));
    }

}