<?php

class ClaimHistoryLogCSV
{
	private $file = '';
	private $date_format;
	private $datetime_format;

	public function __construct($claim_history_logs, $options = array())
	{
		$this->date_format = $options['date_format'];
		$this->datetime_format = $options['datetime_format'];
		$file = '';

		$columns = array(
			'version_table_str' => 'Record Type',
			'type_str' => 'Event Type',
			'event_date' => 'Event Date',
			'username' => 'Username',
			'claim_id' => 'Claim ID',
			'claim_item_id' => 'Claim Item ID',
			'action_description' => 'Action Description',
		);

		foreach ($columns as $key => $name)
		{
			$file .= '"' . $name . '"' . ',';
		}
		$file .= chr(13);

		$columns = array_keys($columns);
		$options = array('event_date_format' => $this->datetime_format, 'strip_html' => true);

		foreach ($claim_history_logs as $claim_history_log)
		{
			$data = $claim_history_log->getGenericJSON($columns, $options);

			foreach ($columns as $col_name)
			{
				$file .= '"' . $data[$col_name] . '"' . ',';
			}
			$file .= chr(13);
		}

		$this->file = $file;
	}
	public function getFile()
	{
		return $this->file;
	}

}