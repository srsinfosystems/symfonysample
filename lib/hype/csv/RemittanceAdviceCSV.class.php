<?php

class RemittanceAdviceCSV
{
	private $file = '';
	private $date_format;

	public function __construct($reconcile, $options)
	{
		$this->date_format = $options['date_format'];

		$reconcile_file = Doctrine_Query::create()
			->from('RaFile rf')
			->leftJoin('rf.RaTransaction rt')
			->leftJoin('rf.RaItem ri')
			->addWhere('rf.reconcile_id = (?)', $reconcile->getId())
			->orderBy('ri.acct_num')
			->fetchOne();

		$file = '';

 		$general = array(
			'RA File Name' => $reconcile->getFileName(),
			'Provider Number' => $reconcile_file->getProviderNum(),
			'Group Number' => $reconcile_file->getGroupNum(),
			'Payment Date' => $reconcile_file->getPaymentDate() ? date($this->date_format, hype::parseDateToInt($reconcile_file->getPaymentDate(), hype::DB_ISO_DATE)) : '',
			'Billing Agent' => $reconcile_file->getBillingAgent() . '',
			'Payee Name' => $reconcile_file->getPayeeName() . '',
			'Address' => str_replace("\n", '  ', $reconcile_file->getAddress()) . '',
			'Amount Payable' => $reconcile_file->getOhipAmountPayable() ? '$' . number_format($reconcile_file->getOhipAmountPayable(), 2, '.', ',') : '',
			'Cheque Number' => $reconcile_file->getChequeNum() . '',
			'Total Accepted Claims' => $reconcile_file->getTotalAcceptedClaims() . '',
			'Total Rejected Claims' => $reconcile_file->getTotalRejectedClaims() . '',
			'Accepted Claims Amount' => $reconcile_file->getAcceptedClaimsAmount() ? '$' . number_format($reconcile_file->getAcceptedClaimsAmount(), 2, '.', ',') : '',
			'Rejected Claims Amount' => $reconcile_file->getRejectedClaimsAmount() ? '$' . number_format($reconcile_file->getRejectedClaimsAmount(), 2, '.', ',') : '',
		);

 		foreach ($general as $key => $name)
 		{
 			$file .= '"' . $key . '"' . ',';
 			$file .= '"' . $name . '"' . ',';
 			$file .= chr(13);
 		}
 		$file .= chr(13);
 		$file .= chr(13);
 		$file .= chr(13);

 		if (count($reconcile_file->RaTransaction)) {
 			$file .= '"Transaction Type"' . ',';
 			$file .= '"Date"' . ',';
 			$file .= '"Amount"' . ',';
 			$file .= '"Description"' . ',';
 			$file .= chr(13);

 			foreach ($reconcile_file->RaTransaction as $rt)
 			{
	 			$file .= '"' . $rt->getTransactionType() . '"' . ',';
	 			$file .= '"' . ($rt->getTransactionDate() ? date($this->date_format, hype::parseDateToInt($rt->getTransactionDate(), hype::DB_ISO_DATE)) : '') . '"' . ',';
	 			$file .= '"' . ($rt->getTransactionAmount() ? '$' . number_format($rt->getTransactionAmount(), 2, '.', ',') : '') . '"' . ',';
	 			$file .= '"' . $rt->getDescription() . '"' . ',';
	 			$file .= chr(13);
 			}
 		}

 		$file .= chr(13);
 		$file .= chr(13);
 		$file .= chr(13);

 		$columns = array('group_num', 'provider_num', 'doctor_name', 'claim_num', 'claim_id', 'item_id', 'acct_num', 'health_num_vc', 'first_name', 'last_name', 'serv_code', 'serv_date', 'amount_submitted', 'amount_paid', 'expl_code');

 		$accepted = '';
 		$rejected = '';

 		$doctors = array();

 		foreach ($reconcile_file->RaItem as $ri)
 		{
 			$doctors_key = $ri->group_num . '-' . $ri->provider_num;
 			if (!array_key_exists($doctors_key, $doctors)) {
 				$doctor = Doctrine_Query::create()
 					->from('Doctor d')
 					->addWhere('d.client_id = (?)', $options['client_id'])
 					->addWhere('d.group_num = (?)', $ri->group_num)
 					->addWhere('d.provider_num = (?)', $ri->provider_num)
 					->fetchOne();

 				if ($doctor instanceof Doctor) {
 					$doctors[$doctors_key] = $doctor->getCustomToString($options['doctor_name_format']);
 				}
 				else {
 					$doctors[$doctors_key] = '';
 				}
 			}

 			$data = $ri->getGenericJSON($columns, array('date_format' => $this->date_format, 'doctor_name' => $doctors[$doctors_key]));
 			$line = '';

 			foreach ($columns as $col_name)
 			{
 				$line .= '"' . $data[$col_name] . '"' . ',';
 			}
 			$line .= chr(13);

 			if ($ri->getRaClaimType() == 1) {
 				$accepted .= $line;
 			}
 			else {
 				$rejected .= $line;
 			}
 		}

 		$columns = array('Group Number', 'Provider Number', 'Dr Name', 'OHIP Claim Number', 'Hype Claim ID', 'Hype Claim Item ID', 'Account Num', 'Health Card', 'Last Name', 'First Name', 'Service Code', 'Service Date', 'Amount Submitted', 'Amount Paid', 'Expl Code');

 		if ($file['accepted']) {
 			$file .= '"Accepted Claims:",' . chr(13);

 			foreach ($columns as $col_name)
 			{
 				$file .= '"' . $col_name . '"' . ',';
 			}
 			$file .= chr(13);

 			$file .= $accepted;

 			$file .= chr(13);
	 		$file .= chr(13);
	 		$file .= chr(13);
 		}

 		if ($file['rejected']) {
 			$file .= '"Rejected Claims:",' . chr(13);

 			foreach ($columns as $col_name)
 			{
 				$file .= '"' . $col_name . '"' . ',';
 			}
 			$file .= chr(13);
 			$file .= $rejected;

 			$file .= chr(13);
 			$file .= chr(13);
 			$file .= chr(13);
 		}

 		$ra_messages = Doctrine_Query::create()
	 		->from('RaMessage rm')
	 		->addWhere('rm.ra_file_id = (?)', $reconcile_file->getId())
	 		->execute(array(), Doctrine::HYDRATE_ARRAY);

 		foreach ($ra_messages as $rm)
 		{
 			$lines = explode("\n", $rm['message']);
 			foreach ($lines as $line)
 			{
 				$file .= '"' . $line . '",' . chr(13);

 			}
 			$file .= chr(13);
 			$file .= chr(13);
 			$file .= chr(13);
 			$file .= chr(13);
 		}

		$this->file = $file;
	}
	public function getFile()
	{
		return $this->file;
	}
}
