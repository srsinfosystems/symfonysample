<?php

class ErrorFileCSV
{
	private $file = '';
	private $date_format;

	public function __construct($reconcile, $options)
	{
		$this->date_format = $options['date_format'];

		$error_items = Doctrine_Query::create()
			->from('ErrorReportItem e')
			->addWhere('e.reconcile_id = ?', $reconcile->getId())
			->addWhere('e.status = (?)', ErrorReportItemTable::STATUS_PARSED)
			->execute();

		$file = '';

		$columns = array(
			'claim_id' => 'Claim ID',
			'item_id' => 'Item ID',
			'group_num' => 'Group Num',
			'provider_num' => 'Provider Num',
			'spec_code_id' => 'Spec Code',
			'account_num' => 'Account Num',
			'hcn' => 'HC #',
			'version_code' => 'Version Code',
			'claim_type' => 'Claim Type',
			'serv_code' => 'Service Code',
			'diag_code' => 'Diagnostic Code',
			'serv_date' => 'Service Date',
			'fee_submitted' => 'Fee Submitted',
			'explan_code' => 'Explanation Code',
			'error_code' => 'Error Codes',
			'ref_doc' => 'Referring Doctor',
			'facility_num' => 'Facility Number',
			'admit_date' => 'Admit Date',
			'ref_lab' => 'Referring Lab',
			'sli' => 'SLI',
			'message' => 'Message',
		);

		foreach ($columns as $key => $name)
		{
			$file .= '"' . $name . '",';
		}
		$file .= chr(13);

		$columns = array_keys($columns);
		$options = array('date_format' => $this->date_format);

		foreach ($error_items as $err)
		{
			$data = $err->getGenericJSON($columns, $options);
			foreach ($columns as $col_name)
			{
				$file .= '"' . $data[$col_name] . '",';
			}
			$file .= chr(13);
		}
		$this->file = $file;
	}
	public function getFile()
	{
		return $this->file;
	}
}