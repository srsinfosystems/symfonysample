<?php

class ErrorDetailsReportCSV
{
	protected $dateTimeFormat;
	protected $dateFormat;
	protected $footerText;
	protected $query;
	protected $reportHeader;
	protected $reportColumns = array();
	protected $report_headers = array();

	protected $fileText = '';

	public function __construct($options = array())
	{
		$this->dateTimeFormat = array_key_exists('datetime_format', $options) ? $options['datetime_format'] : hype::DB_ISO_DATE;
		$this->dateFormat = array_key_exists('date_format', $options) ? $options['date_format'] : hype::DB_DATE_FORMAT;
		$this->footerText = array_key_exists('footer_text', $options) ? $options['footer_text'] : '';

  		return $this;
	}

	public function setQuery($q)
	{
		$this->query = $q;
	}
	public function setReportHeader($v)
	{
		$this->reportHeader = $v;
	}

	public function setReportColumns($columns)
	{
		foreach ($columns as $info)
		{
			if ($info['rs_key'] !== 'actions' && $info['rs_key'] != 'still_rejected') {
				$this->reportColumns[] = $info['rs_key'];
				$this->report_headers[$info['rs_key']] = $info['header_text'];
			}
		}
	}

	public function build()
	{
		$this->fileText .= '"' . $this->reportHeader . '",' . chr(13);
		$this->fileText .= chr(13);

		$this->writeMainTableHeader();

 		$data = $this->query->execute();

 		$options = array(
 				'error_codes_format' => 'string',
 				'date_format' => $this->dateFormat,
 		);
 		foreach ($data as $record)
 		{
 			$rs = $record->getGenericJSON($this->reportColumns, $options);
 			$this->writeMainTableRow($rs);
 		}
 		$this->fileText .= chr(13);
 		$this->fileText .= chr(13);

 		$this->writeFooterText();

 		return $this->fileText;
	}

	protected function writeFooterText()
	{
		if ($this->footerText) {
			$this->fileText .= '"' . $this->footerText . '",' . chr(13);
		}

		$this->fileText .= '"' .  date($this->dateTimeFormat) . '",' . chr(13);
	}
	protected function writeMainTableHeader()
	{
 		foreach ($this->reportColumns as $col_num => $column)
 		{
 			$data = array_key_exists($column, $this->report_headers) ? $this->report_headers[$column] : $column;
 			$this->fileText .= '"' . $data . '",';
 		}
 		$this->fileText .= chr(13);
	}

	protected function writeMainTableRow($data)
	{
		foreach ($this->reportColumns as $col_num => $column)
		{
			$this->fileText .= '"' . $data[$column] . '",';
		}
		$this->fileText .= chr(13);
	}
}


