<?php

class hypeWidgetDate extends sfWidgetFormInputText
{
 	protected function configure($options = array(), $attributes = array())
 	{
 		$this->addOption('date_format', array_key_exists('date_format', $options) ? $options['date_format'] : hype::DB_DATE_FORMAT);
     	$this->setOption('date_format', array_key_exists('date_format', $options) ? $options['date_format'] : hype::DB_DATE_FORMAT);
 	}
 	public function render($name, $value = null, $attributes = array('type' => 'text'), $errors = array())
 	{
 		if ($value && !is_int($value)) {
 			if ((strlen($value) == 23 || strlen($value) == 19) && $value[4] == '-') {
 				$date = DateTime::createFromFormat(hype::DB_ISO_DATE, substr($value, 0, 19));
 			}
 			else {
 				$date = DateTime::createFromFormat($this->getOption('date_format'), $value);
 			}
 			if ($date instanceof DateTime) {
 				$value = $date->format($this->getOption('date_format'));
 			}
 		}
 		else if ($value && is_int($value)) {
 			$value = date($this->getOption('date_format'), $value);
 		}
 		else {
 			$value = '';
 		}
                $attributes = array_merge($attributes,array('type' => 'text'));
 		return parent::render($name, $value, $attributes, $errors);
 	}
}
