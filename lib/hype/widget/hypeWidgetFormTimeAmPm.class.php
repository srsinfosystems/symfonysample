<?php

class hypeWidgetFormTimeAmPm extends sfWidgetFormTime
{
 	/**
 	 * Constructor.
 	 *
 	 * Available options:
 	 *
 	 *  * format:                 The time format string (%hour%:%minute%:%second% %ampm%)
 	 *  * format_without_seconds: The time format string without seconds (%hour%:%minute%)
 	 *  * with_seconds:           Whether to include a select for seconds (false by default)
 	 *  * hours:                  An array of hours for the hour select tag (optional)
 	 *  * minutes:                An array of minutes for the minute select tag (optional)
 	 *  * seconds:                An array of seconds for the second select tag (optional)
 	 *  * can_be_empty:           Whether the widget accept an empty value (true by default)
 	 *  * empty_values:           An array of values to use for the empty value (empty string for hours, minutes, and seconds by default)
 	 *
 	 * @param array $options     An array of options
 	 * @param array $attributes  An array of default HTML attributes
 	 *
 	 * @see sfWidgetForm
 	 */
 	protected function configure($options = array(), $attributes = array())
 	{
 		$this->addOption('interval', 1);
 		$interval = array_key_exists('interval', $options) ? $options['interval'] : 1;

 		$this->addOption('format', '%hour%:%minute%:%second%');
 		$this->addOption('format_without_seconds', '%hour%:%minute% %ampm%');
 		$this->addOption('with_seconds', false);
 		$this->addOption('hours', parent::generateTwoCharsRange(1, 12));
 		$this->addOption('minutes', self::generateTwoCharsRangeWithInterval(0, 59, $interval));
 		$this->addOption('seconds', parent::generateTwoCharsRange(0, 59));
 		$this->addOption('ampm', array('am' => 'AM', 'pm' => 'PM'));
 		$this->addOption('string_format', null);

 		$this->addOption('can_be_empty', true);
 		$this->addOption('empty_values', array('hour' => '', 'minute' => '', 'second' => '', 'ampm' => ''));
 	}

 	/**
 	 * @param  string $name        The element name
 	 * @param  string $value       The time displayed in this widget
 	 * @param  array  $attributes  An array of HTML attributes to be merged with the default HTML attributes
 	 * @param  array  $errors      An array of errors for the field
 	 *
 	 * @return string An HTML tag string
 	 *
 	 * @see sfWidgetForm
 	 */
 	public function render($name, $value = null, $attributes = array(), $errors = array())
 	{
 		// convert value to an array
 		$default = array('hour' => null, 'minute' => null, 'second' => null, 'ampm' => null);
 		if (is_array($value))
 		{
 			$value = array_merge($default, $value);
 		}
 		else if ($this->getOption('string_format')) {
 			if ($value) {
 				$hour = substr($value, 0, 2);
 				$minute = substr($value, 3, 2);

 				$ampm = $hour >= 12 ? 'pm' : 'am';
 				$hour = ($hour == 12 ? 0 : ($hour > 12 ? $hour - 12 : $hour));

 				$value = array(
 						'hour' => intval($hour),
 						'minute' => intval($minute),
 						'second' => null,
 						'ampm' => $ampm
 				);
 			}
 		}
 		else
 		{
 			$value = ctype_digit($value) ? (integer) $value : strtotime($value);
 			if (false === $value)
 			{
 				$value = $default;
 			}
 			else
 			{
 				// int cast required to get rid of leading zeros
 				$value = array('hour' => (int) date('H', $value), 'minute' => (int) date('i', $value), 'second' => (int) date('s', $value), 'ampm' => date('a', $value));
 			}
 		}

 		$time = array();
 		$emptyValues = $this->getOption('empty_values');

 		// hours
 		$widget = new sfWidgetFormSelect(array('choices' => $this->getOption('can_be_empty') ? array('' => $emptyValues['hour']) + $this->getOption('hours') : $this->getOption('hours'), 'id_format' => $this->getOption('id_format')), array_merge($this->attributes, $attributes));
 		$time['%hour%'] = $widget->render($name.'[hour]', $value['hour']);

 		// minutes
 		$widget = new sfWidgetFormSelect(array('choices' => $this->getOption('can_be_empty') ? array('' => $emptyValues['minute']) + $this->getOption('minutes') : $this->getOption('minutes'), 'id_format' => $this->getOption('id_format')), array_merge($this->attributes, $attributes));
 		$time['%minute%'] = $widget->render($name.'[minute]', $value['minute']);

 		if ($this->getOption('with_seconds'))
 		{
 			// seconds
 			$widget = new sfWidgetFormSelect(array('choices' => $this->getOption('can_be_empty') ? array('' => $emptyValues['second']) + $this->getOption('seconds') : $this->getOption('seconds'), 'id_format' => $this->getOption('id_format')), array_merge($this->attributes, $attributes));
 			$time['%second%'] = $widget->render($name.'[second]', $value['second']);
 		}

 		// ampm
 		$widget = new sfWidgetFormSelect(array('choices' => $this->getOption('can_be_empty') ? array('' => $emptyValues['ampm']) + $this->getOption('ampm') : $this->getOption('ampm'), 'id_format' => $this->getOption('id_format')), array_merge($this->attributes, $attributes));
 		$time['%ampm%'] = $widget->render($name.'[ampm]', $value['ampm']);

 		return strtr($this->getOption('with_seconds') ? $this->getOption('format') : $this->getOption('format_without_seconds'), $time);
 	}

 	/**
 	 * Generates a two chars range
 	 *
 	 * @param  int  $start
 	 * @param  int  $stop
 	 * @return array
 	 */
 	static protected function generateTwoCharsRangeWithInterval($start, $stop, $interval)
 	{
 		$results = array();
 		for ($i = $start; $i <= $stop; $i += $interval)
 		{
 			$results[$i] = sprintf('%02d', $i);
 		}
 		return $results;
 	}
}
