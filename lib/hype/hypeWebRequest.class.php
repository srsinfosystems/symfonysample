<?php

class hypeWebRequest extends sfWebRequest
{
	public function getParameter($name, $default = null)
	{
		$v = parent::getParameter($name, $default);

		if (is_array($v)) {
			foreach ($v as $key => $val)
			{
				if ($val == 'null') {
					$v[$key] = null;
				}
			}
		}

		return $v;
	}
}
