<?php

class hypeHCV
{
	public static function writeHCVFile ($client_id, $user, $hc, $vc, $patient_id)
	{

		$patient = null;
		if ($patient_id) {
			$patient = Doctrine_Query::create()
				->from('Patient p')
				->addWhere('p.client_id = (?)', $client_id)
				->addWhere('p.id = (?)', $patient_id)
				->addWhere('p.deleted = (?)', false)
				->fetchOne();
		}

		$hcvParam = Doctrine_Query::create()
			->from('HCVParams h')
			->addWhere('h.client_id = (?)', $client_id)
			->fetchone();

		if (!$hcvParam instanceof HCVParams || !$hcvParam->user_name || !$hcvParam->host) {
			return null;
		}

		$s = $client_id . '+' . $user . '|';

		$s = $s . 'RPVR0300 ';

		$s = $s . self::padString($hc, 10);
		$s = $s . self::padString($vc, 2);

		$s = $s . self::padString('********', 8);
		$s = $s . self::padString($hcvParam->facility . ''  , 7);
		$s = $s . self::padString($hcvParam->provider . '', 10);

		$s = $s . self::padString($hcvParam->user_name . '', 8);
		$s = $s . self::padString('', 8);

		$s = $s . '####################'; //Out Server Needs to Fill this in

		$file_name = date('Ymdhms') . '.txt';

		// insert hcv request into outgoing requests
		$ip = array_key_exists('REMOTE_ADDR', $_SERVER) ? $_SERVER["REMOTE_ADDR"] : '';
		$host = $hcvParam->host ;
		$port = 14092;
		$timeout = 3;
		$dati = false;
		$sk = null;

		$attempts = 0;
		do {
			$sk = @fsockopen($host, $port, $errnum, $errstr, $timeout);
			$attempts++;
		} while (!is_resource($sk) && $attempts < 5);

		if (!is_resource($sk)) {
			return null;
		} else {
			fputs($sk, hype::encryptHCN('hype_medical+' . $ip . '+' . $s)) ;
			stream_set_timeout($sk, 5);
			$dati = fgets ($sk, 1024); //stream_get_line($sk,128,"WTF");
		}

		fclose($sk) ;

		if ($dati === false) {
			return false;
		}

		$dati = hype::decryptHCN($dati);

		$response = trim(substr($dati, strpos($dati ,'Response Code: ') + 15));
		$response = substr($response,0,strpos($response,'</td>'));

		if ($patient instanceof Patient) {
			if ($response >= 50 && $response <= 55) {
				$patient->status = 1;
			}
			else if ($response == 90 || $response == 99) {
				$patient->status = 4;
			}
			else if ($response.substr(0, 1) == "9") {
				$patient->status = 5;
			}
			else {
				$patient->status = 2;
			}
			$patient->hcn_check_date = date(hype::DB_ISO_DATE);
			$patient->response = $response;

			$patient->save();
		}
        //print_r($dati); exit();
        return $dati;
    }
	public static function padString($s, $length)
	{
		if (strlen($s) > $length) {
			return substr($s, 0, $length);
		}

		while (strlen($s) < $length)
		{
			$s = $s . ' ';
		}
		return $s;
	}
}
