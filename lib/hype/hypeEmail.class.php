<?php

class hypeEmail {
 
    protected function sendEmail() {
        if (!class_exists('Swift_SmtpTransport')) {
            require_once ('../lib/vendor/Swift-5.0.3/lib/swift_required.php');
        }

        $transport = Swift_SmtpTransport::newInstance($this->smtp, $this->port)
                ->setUsername($this->username)
                ->setPassword($this->password);

        $mailer = Swift_Mailer::newInstance($transport);

        $message = Swift_Message::newInstance()
                ->setSubject($this->subject)
                ->setFrom($this->from)
                ->setTo($this->to)
                ->setBody($this->body)
                ->addPart($this->content, 'text/html');

        $result = $mailer->send($message);
    }

    protected function setDefaults($client_id) {
        $client = Doctrine::getTable('Client')->findOneById($client_id);

        $this->subject = ClientParamTable::getParamOrDefaultValueForClient($client_id, 'mdc_send_email_subject');
        $this->to = array('eitan@hypesystems.com' => 'Eitan');
        $this->body = '';

        $this->port = $client->email_port;
        $this->username = $client->email_username;
        $this->from = $client->email_username;
        $this->password = $client->email_password;
        $this->smtp = $client->email_server;
    }

    public function writeNotificationEmail($email, $id, $subject) {             
        $client = Doctrine::getTable('Client')->findOneById(1);

        $this->subject = ClientParamTable::getParamOrDefaultValueForClient($client_id, 'mdc_send_email_subject');
        $this->to = array('sunil@srs-infosystems.com' => 'support');
        $this->body = '';
        $this->port = $client->email_port;
        $this->username = $client->email_username;
        $this->from = $client->email_username;
        $this->password = $client->email_password;
        $this->smtp = $client->email_server;
        $this->subject = $subject;
        $this->from = $email;
        $this->body = 'New client registered';
        $this->sendEmail();
    }

    public function writeEmail($attendee, $dev = false) {
        if (!$attendee instanceof AppointmentAttendee) {
            $attendee = Doctrine_Query::create()
                    ->from('AppointmentAttendee aa')
                    ->leftJoin('aa.Appointment a')
                    ->leftJoin('aa.Patient p')
                    ->where('aa.id = ?', $attendee)
                    ->addWhere('p.deleted = (?) OR p.deleted is null', false)
                    ->fetchOne();
        }

        if ($attendee instanceof AppointmentAttendee) {
            $doctor_id = $attendee->Appointment->doctor_id;
            $doctor_email = $attendee->Appointment->Doctor->email;

            $timestamp = hype::parseDateToInt($attendee->Appointment->start_date, hype::DB_ISO_DATE);

            if ($doctor_email != '' && $doctor_email != null) {
                $this->setDefaults($attendee->Appointment->client_id);

                $appointments = Doctrine_Query::create()
                        ->from('Appointment a')
                        ->where('a.doctor_id = ?', $doctor_id)
                        ->addWhere('a.status != ?', AppointmentTable::STATUS_DELETED)
                        ->addWhere('a.slot_type = ?', AppointmentTable::SLOT_TYPE_NORMAL)
                        ->addWhere('a.start_date BETWEEN (?) and (?)', array(date('Y-m-d 00:00:00', $timestamp), date('Y-m-d 23:59:59', $timestamp)))
                        ->orderby('id')
                        ->execute();

                $count = 0;
                foreach ($appointments as $appt) {
                    $count++;
                    if ($appt->id == $attendee->appointment_id) {
                        break;
                    }
                }

                if ($dev == false) {
                    $this->to = array($doctor_email => 'Dr. ' . $attendee->Appointment->Doctor->lname);
                }

                $code = date('Ymd', $timestamp) . '-' . $count;
                $created_time = date('Ymd h:i A', hype::parseDateToInt($attendee->Appointment->start_date, hype::DB_ISO_DATE));
                $hphone = $attendee->Patient->getDisplayHphone();
                $wphone = $attendee->Patient->getDisplayWphone();
                $cphone = $attendee->Patient->getDisplayCellphone();
                $ophone = $attendee->Patient->getDisplayOtherPhone();
                $address = $attendee->Patient->address . ', ' . $attendee->Patient->city;
                $notes = $attendee->Appointment->notes;
                $dob = date(hype::DB_DATE_FORMAT, hype::parseDateToInt($attendee->Patient->dob, hype::DB_ISO_DATE));
                $name = $attendee->Patient->lname . ', ' . $attendee->Patient->fname;
                $hc = $attendee->Patient->getHCN() . ' ' . $attendee->Patient->hcn_version_code;
                $status = AppointmentTable::getStatusString($attendee->Appointment->status, true);

                $footer = '<br /><br />Confidentiality Notice: This e-mail message (including any attachments or embedded documents) is intended for the exclusive and confidential use of the individual or entity to which this message is addressed, and unless otherwise expressly indicated, is confidential and privileged information of My Doctor Care Housecall Service Inc. Any dissemination, distribution or copying of the enclosed material is prohibited. If you receive this transmission in error, please notify us immediately by e-mail at support@hypesystems.com and delete the original message. Your cooperation is appreciated.';

                $gmap = 'http://maps.google.ca/?q=' . str_replace(' ', '+', $attendee->Patient->address) . '+' . str_replace(' ', '+', $attendee->Patient->city);

                $this->content = $status . ' * ' . $code . ' * ' . $created_time . ' * '
                        . $hphone . ' [H] * ' . $wphone . ' [W] * ' . $cphone . ' [C] * ' . $ophone . ' [O] * '
                        . $address . ' * ' . $notes . ' * ' . $dob . ' * ' . $name . ' * ' . $hc . '<br /><br />' . $gmap . $footer;

                $this->sendEmail();
            }
        }
    }

}
