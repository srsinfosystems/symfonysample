<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class DobLabelPDF
{
	const BROTHER_LABEL_WIDTH       = 90;
	const BROTHER_LABEL_HEIGHT      = 29;
	const LEFT_MARGIN 				= 2;
	const RIGHT_MARGIN				= 2;
	const TOP_MARGIN                = 1;

	protected $patient             = null;
	protected $dateFormat          = null;

	protected $dobBoxWidth         = null;
	protected $genderBoxWidth      = null;
	protected $nameBoxWidth        = null;
	protected $patientNumberWidth  = null;

	protected $heightRow1          = null;
	protected $heightRow2          = null;

	public function __construct($pdf_options)
	{
 		$this->patient = array_key_exists('patient', $pdf_options) ? $pdf_options['patient'] : null;
 		$this->dateFormat = array_key_exists('date_format', $pdf_options) ? $pdf_options['date_format'] : ClientParamTable::getDefaultForValue('date_format');

 		$this->pdf = new TCPDF('L', 'mm', array(self::BROTHER_LABEL_HEIGHT, self::BROTHER_LABEL_WIDTH), true, 'UTF-8', false);

		$this->pdf->SetCreator('HYPE Systems Inc');
		$this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$this->pdf->SetAutoPageBreak(true, 0);
		$this->pdf->setPrintHeader(false);
		$this->pdf->setPrintFooter(false);
		$this->pdf->setMargins(self::LEFT_MARGIN, self::TOP_MARGIN, self::RIGHT_MARGIN, true);

	}
	public function generatePDF()
	{
		$this->pdf->SetFont('helvetica', '', 12);

		$this->dobBoxWidth = self::BROTHER_LABEL_WIDTH - self::LEFT_MARGIN - self::RIGHT_MARGIN;  	// 100%
		$this->patientNumberWidth = $this->pdf->getStringWidth($this->patient->patient_number) + 1;
		$this->nameBoxWidth = $this->dobBoxWidth - $this->patientNumberWidth;

		$this->genderBoxWidth = $this->pdf->getStringWidth($this->patient->sex) + 1;
		$this->dobBoxWidth = $this->dobBoxWidth - $this->genderBoxWidth;


		$workableHeight = floor((self::BROTHER_LABEL_HEIGHT - self::TOP_MARGIN - self::TOP_MARGIN)/2);
		$this->heightRow1 = floor($workableHeight * .60);
		$this->heightRow2 = $workableHeight - $this->heightRow1;

		$this->pdf->AddPage();
		$this->pdf->setCellPadding(0);
		$this->pdf->setCellMargins(0, 0, 0, 0);


        //$js = 'print(true);';

       // $this->pdf->IncludeJS($js);

		$this->buildLabelPage();
		$this->buildLabelPage();

		return $this->pdf;
	}
	public function buildLabelPage()
	{
		$nameStr = '<b><font size="14pt">' . $this->patient->getLName() . ',</font> <font size="12pt">' . $this->patient->getFName() . '</font></b>';
		$dobStr = $this->patient->dob ? date($this->dateFormat, hype::parseDateToInt($this->patient->dob, hype::DB_ISO_DATE)) : '';

		$y = $this->pdf->GetY();
		$x = $this->pdf->GetX();

		$this->pdf->SetFont('helvetica', '', 14);
		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->nameBoxWidth, $this->heightRow1, 'CNZ');
		$this->pdf->writeHTMLCell(0, $this->heightRow1, $x, $y, $nameStr);
		$this->pdf->StopTransform();

		$x = $x + $this->nameBoxWidth;

		$this->pdf->SetFont('helvetica', '', 12);
		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->patientNumberWidth, $this->heightRow1, 'CNZ');
		$this->pdf->writeHTMLCell($this->patientNumberWidth, $this->heightRow1, $x, $y, $this->patient->patient_number, 0, 1, 0, true, 'R');
		$this->pdf->StopTransform();

		$y = $this->pdf->GetY();
		$x = $this->pdf->GetX();

		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->dobBoxWidth, $this->heightRow2, 'CNZ');
		$this->pdf->writeHTMLCell($this->dobBoxWidth, $this->heightRow2, $x, $y, $dobStr, 0, 0, 0, true, 'L');
		$this->pdf->StopTransform();

		$x = $x + $this->dobBoxWidth;

		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->genderBoxWidth, $this->heightRow2, 'CNZ');
		$this->pdf->writeHTMLCell(0, $this->heightRow2, $x, $y, $this->patient->sex, 0, 1, 0, true, 'L');
		$this->pdf->StopTransform();
	}
}
