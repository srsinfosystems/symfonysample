<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class ChartRefDocLabelPDF
{
	const BROTHER_LABEL_WIDTH       = 90;
	const BROTHER_LABEL_HEIGHT      = 29;
	const LEFT_MARGIN 				= 2;
	const RIGHT_MARGIN				= 2;
	const TOP_MARGIN                = 1;

	protected $patient             = null;
	protected $dateFormat          = null;
	protected $lineWidth           = null;
	protected $row1Height          = null;
	protected $rowHeight           = null;
	protected $patientNumberWidth  = null;
	protected $nameBoxWidth        = null;
	protected $leftWidth           = null;
	protected $rightWidth          = null;

	public function __construct($pdf_options)
	{
 		$this->patient = array_key_exists('patient', $pdf_options) ? $pdf_options['patient'] : null;
 		$this->dateFormat = array_key_exists('date_format', $pdf_options) ? $pdf_options['date_format'] : ClientParamTable::getDefaultForValue('date_format');

 		$this->pdf = new TCPDF('L', 'mm', array(self::BROTHER_LABEL_HEIGHT, self::BROTHER_LABEL_WIDTH), true, 'UTF-8', false);

		$this->pdf->SetCreator('HYPE Systems Inc');
		$this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$this->pdf->SetAutoPageBreak(true, 0);
		$this->pdf->setPrintHeader(false);
		$this->pdf->setPrintFooter(false);
		$this->pdf->setMargins(self::LEFT_MARGIN, self::TOP_MARGIN, self::RIGHT_MARGIN, true);

	}
	public function generatePDF()
	{
		$this->pdf->SetFont('helvetica', '', 12);

		$this->rowHeight = 4;
		$this->row1Height = 7;

		$this->buildStrings();
		$this->setupLineWidths();

		$this->pdf->AddPage();
		$this->pdf->setCellPadding(0);
		$this->pdf->setCellMargins(0, 0, 0, 0);

		$this->buildChartLabel();

		return $this->pdf;
	}
	public function buildStrings()
	{
		$this->strings = array(
			'name' => '<b><font size="14pt">' . $this->patient->getLName() . ',</font> <font size="12pt">' . $this->patient->getFName() . '</font></b>',
			'patient_number' => $this->patient->patient_number,

			'address' => ucwords(strtolower($this->patient->address)),
			'dob' => ($this->patient->dob ? date($this->dateFormat, hype::parseDateToInt($this->patient->dob, hype::DB_ISO_DATE)) : '')	. ' ' . $this->patient->sex,

			'city' => ucwords(strtolower($this->patient->city)) . ' ' . $this->patient->province . ' ' . $this->patient->getDisplayPostalCode(),
			'homeph' => 'H: ' . $this->patient->getDisplayHphone(),

			'hcn' => ($this->patient->moh_province ? $this->patient->moh_province : $this->patient->province) . ' ' . $this->patient->getHCN() . ' ' . $this->patient->hcn_version_code,
			'cellph' => 'C: ' . $this->patient->getDisplayCellphone(),

			'refdoc' =>  'Ref: ' . $this->patient->getRefDocDescription(),
			'famdoc' => 'Fam: ' . $this->patient->getFamDocDescription(),
		);
	}
	public function setupLineWidths()
	{
		$this->lineWidth = self::BROTHER_LABEL_WIDTH - self::LEFT_MARGIN - self::RIGHT_MARGIN;

		$this->patientNumberWidth = $this->pdf->getStringWidth($this->strings['patient_number']) + 1;
		$this->nameBoxWidth = $this->lineWidth - $this->patientNumberWidth;

		$this->pdf->SetFont('helvetica', '', 10);

		if ($this->patient->getRefDocDescription() && $this->patient->getFamDocDescription()) {
			$this->rightWidth = floor($this->lineWidth / 2);
		}
		else {
			$this->rightWidth = max($this->pdf->getStringWidth($this->strings['famdoc']), $this->pdf->getStringWidth($this->strings['dob']), $this->pdf->getStringWidth($this->strings['homeph']), $this->pdf->getStringWidth($this->strings['cellph'])) + 1;
		}

		$this->leftWidth = $this->lineWidth - $this->rightWidth;
	}
	public function buildChartLabel()
	{
		$y = $this->pdf->GetY();
		$x = $this->pdf->GetX();

		$this->pdf->SetFont('helvetica', '', 14);
		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->nameBoxWidth, $this->row1Height, 'CNZ');
		$this->pdf->writeHTMLCell(0, $this->row1Height, $x, $y, $this->strings['name']);
		$this->pdf->StopTransform();

		$x = $x + $this->nameBoxWidth;

		$this->pdf->SetFont('helvetica', '', 12);
		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->patientNumberWidth, $this->row1Height, 'CNZ');
		$this->pdf->writeHTMLCell($this->patientNumberWidth, $this->row1Height, $x, $y, $this->strings['patient_number'], 0, 1, 0, true, 'R');
		$this->pdf->StopTransform();

		$y = $this->pdf->GetY();
		$x = $this->pdf->GetX();

		$this->pdf->SetFont('helvetica', '', 9.5);
		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->leftWidth, $this->rowHeight, 'CNZ');
		$this->pdf->writeHTMLCell(0, $this->rowHeight, $x, $y, $this->strings['address']);
		$this->pdf->StopTransform();

		$x = $x + $this->leftWidth;

		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->rightWidth, $this->rowHeight, 'CNZ');
		$this->pdf->writeHTMLCell(0, $this->rowHeight, $x, $y, $this->strings['dob'], 0, 1, 0, true, 'R');
		$this->pdf->StopTransform();

		$y = $this->pdf->GetY();
		$x = $this->pdf->GetX();

		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->leftWidth, $this->rowHeight, 'CNZ');
		$this->pdf->writeHTMLCell(0, $this->rowHeight, $x, $y, $this->strings['city']);
		$this->pdf->StopTransform();

		$x = $x + $this->leftWidth;

		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->rightWidth, $this->rowHeight, 'CNZ');
		$this->pdf->writeHTMLCell(0, $this->rowHeight, $x, $y, $this->strings['homeph'], 0, 1, 0, true, 'R');
		$this->pdf->StopTransform();

		$y = $this->pdf->GetY();
		$x = $this->pdf->GetX();

		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->leftWidth, $this->rowHeight, 'CNZ');
		$this->pdf->writeHTMLCell(0, $this->rowHeight, $x, $y, $this->strings['hcn']);
		$this->pdf->StopTransform();

		$x = $x + $this->leftWidth;

		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->rightWidth, $this->rowHeight, 'CNZ');
		$this->pdf->writeHTMLCell(0, $this->rowHeight, $x, $y, $this->strings['cellph'], 0, 1, 0, true, 'R');
		$this->pdf->StopTransform();

		$y = $this->pdf->GetY();
		$x = $this->pdf->GetX();

		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->leftWidth, $this->rowHeight, 'CNZ');
		$this->pdf->writeHTMLCell(0, $this->rowHeight, $x, $y, $this->strings['refdoc']);
		$this->pdf->StopTransform();

		$x = $x + $this->leftWidth;

		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->rightWidth, $this->rowHeight, 'CNZ');
		$this->pdf->writeHTMLCell(0, $this->rowHeight, $x, $y, $this->strings['famdoc'], 0, 1, 0, true, 'R');
		$this->pdf->StopTransform();
	}
}
