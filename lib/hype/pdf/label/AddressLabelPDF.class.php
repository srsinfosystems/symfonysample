<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class AddressLabelPDF
{
	const BROTHER_LABEL_WIDTH       = 90;
	const BROTHER_LABEL_HEIGHT      = 29;
	const LEFT_MARGIN 				= 2;
	const RIGHT_MARGIN				= 2;
	const TOP_MARGIN                = 1;

	protected $patient             = null;
	protected $lineWidth           = null;
	protected $rowHeight           = null;

	public function __construct($pdf_options)
	{
 		$this->patient = array_key_exists('patient', $pdf_options) ? $pdf_options['patient'] : null;

 		$this->pdf = new TCPDF('L', 'mm', array(self::BROTHER_LABEL_HEIGHT, self::BROTHER_LABEL_WIDTH), true, 'UTF-8', false);

		$this->pdf->SetCreator('HYPE Systems Inc');
		$this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$this->pdf->SetAutoPageBreak(true, 0);
		$this->pdf->setPrintHeader(false);
		$this->pdf->setPrintFooter(false);
		$this->pdf->setMargins(self::LEFT_MARGIN, self::TOP_MARGIN, self::RIGHT_MARGIN, true);

	}
	public function generatePDF()
	{
		$workableHeight = floor((self::BROTHER_LABEL_HEIGHT - self::TOP_MARGIN - self::TOP_MARGIN));

		$this->rowHeight = 5;
		$this->lineWidth = self::BROTHER_LABEL_WIDTH - self::LEFT_MARGIN - self::RIGHT_MARGIN;

		$this->pdf->SetFont('helvetica', '', 12);

		$this->pdf->AddPage();
		$this->pdf->setCellPadding(0);
		$this->pdf->setCellMargins(0, 0, 0, 0);

		$this->buildAddressLabel();

		return $this->pdf;
	}
 	protected function buildAddressLabel()
 	{
		$nameStr = '<b><font size="14pt">' . $this->patient->getLName() . ',</font> <font size="12pt">' . $this->patient->getFName() . '</font></b>';
		$addrStr = $this->patient->city . ', ' . $this->patient->province . ' ' . $this->patient->getDisplayPostalCode();

		$y = $this->pdf->GetY();
		$x = $this->pdf->GetX();

		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->lineWidth, $this->rowHeight + 1, 'CNZ');
		$this->pdf->writeHTMLCell($this->lineWidth, $this->rowHeight + 1, $x, $y, $nameStr, 0, 1, 0, true, 'L');
		$this->pdf->StopTransform();

		$y += $this->rowHeight + 1;

		$this->pdf->SetFont('helvetica', '', 12);
		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->lineWidth, $this->rowHeight, 'CNZ');
		$this->pdf->writeHTMLCell($this->lineWidth, $this->rowHeight, $x, $y, $this->patient->address, 0, 0, 0, true, 'L');
		$this->pdf->StopTransform();

		$y += $this->rowHeight;

		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->lineWidth, $this->rowHeight, 'CNZ');
		$this->pdf->writeHTMLCell($this->lineWidth, $this->rowHeight, $x, $y, $addrStr, 0, 1, 0, true, 'L');
		$this->pdf->StopTransform();

		$y += $this->rowHeight;

		$this->pdf->StartTransform();
		$this->pdf->Rect($x, $y, $this->lineWidth, $this->rowHeight, 'CNZ');
		$this->pdf->writeHTMLCell($this->lineWidth, $this->rowHeight, $x, $y, $this->patient->country, 0, 1, 0, true, 'L');
		$this->pdf->StopTransform();
 	}
}


/***
 *
 */