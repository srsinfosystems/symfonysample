<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class labelPDF extends TCPDF
{
 	protected $label_type = null;
 	protected $client_id = null;
 	protected $patient = null;
 	protected $refdoc = null;
 	protected $date_format;

 	const BROTHER_LABEL_WIDTH       = 29;
 	const BROTHER_LABEL_HEIGHT      = 90;
 	const LABEL_WIDTH  			    = 29;
 	const LABEL_HEIGHT      		= 81;
 	const LEFT_MARGIN 				= 1;
 	const RIGHT_MARGIN				= 2.6;

 	public function __construct($pdf_options)
 	{
 		$this->label_type = array_key_exists('type', $pdf_options) ? $pdf_options['type'] : $this->label_type;
 		$this->client_id = array_key_exists('client_id', $pdf_options) ? $pdf_options['client_id'] : $this->client_id;
 		$this->patient = array_key_exists('patient', $pdf_options) ? $pdf_options['patient'] : null;
 		$this->refdocaddr = array_key_exists('refdocaddr', $pdf_options) ? $pdf_options['refdocaddr'] : null;
 		$this->date_format = array_key_exists('date_format', $pdf_options) ? $pdf_options['date_format'] : ClientParamTable::getDefaultForValue('date_format');

 		parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

 		$this->SetCreator(PDF_CREATOR);
 		$this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
 		$this->SetAutoPageBreak(TRUE, 0);
 		$this->setImageScale(PDF_IMAGE_SCALE_RATIO);
 		$this->setPrintHeader(false);
 		$this->setPrintFooter(false);
 		$this->SetMargins(self::LEFT_MARGIN, 1, 2.6, true);

 		switch ($this->label_type) {
 			case 'refdocaddr':
 				$this->SetFont('helvetica', '', 12, '', true);
 				break;
 			default:
 				break;
 		}
 		$this->AddPage('L', array('MediaBox' => array(
 			'llx' => '0',
 			'lly' => '0',
 			'urx' => self::LABEL_HEIGHT,
 			'ury' => self::LABEL_WIDTH
 		)));

 		$this->line_width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;
 		return $this;
 	}
 	public function generatePDF()
 	{
 		switch ($this->label_type)
 		{
 			case 'chart':
 				return $this->getChartLabel();
 			case 'name':
 				return $this->getNameLabel();
 			case 'refdocaddr':
 				return $this->getRefDocAddrLabel();
 			default:
 				return;
 		}
 	}
 	protected function getRefDocAddrLabel()
 	{
 		if (!$this->refdocaddr instanceof ReferringDoctorAddressOld) {
 			$this->refdocaddr = new ReferringDoctorAddressOld();
 		}

 		$this->Cell(0, 0, $this->refdocaddr->ReferringDoctorOld->lname . ', ' . $this->refdocaddr->ReferringDoctorOld->fname);
 		$this->Ln();
 		if ($this->refdocaddr->address) {
 			$this->Cell(0, 0, $this->refdocaddr->address . ' ');
 			$this->Ln();
 		}
 		if ($this->refdocaddr->address2) {
 			$this->Cell(0, 0, $this->refdocaddr->address2 . ' ');
 			$this->Ln();
 		}

 		$this->Cell(0, 0, $this->refdocaddr->city . ', ' . $this->refdocaddr->province . ' ' . $this->refdocaddr->postal_code);
 		$this->Ln();
 	}
}

