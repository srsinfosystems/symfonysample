<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class reportPdf extends TCPDF {

    protected $orientation = 'P';
    protected $show_header = false;
    protected $show_subheader = false;
    protected $header_text = '';
    protected $subheader_text = '';
    protected $report_columns = array();
    protected $report_headers = array();
    protected $column_widths = array();
    protected $footer_widths = array();
    protected $line_width = null;
    protected $line_height_factor = 5.5;
    protected $footer_text;
    protected $doctor_title;
    protected $includeDateRange;
    protected $dateRangeStart;
    protected $dateRangeEnd;
    protected $pagerText = '';
    protected $date_format;
    protected $datetime_format;

    public function __construct($pdf_options) {
        $this->report_headers = array_key_exists('report_headers', $pdf_options) ? $pdf_options['report_headers'] : array();
        $this->orientation = array_key_exists('orientation', $pdf_options) ? $pdf_options['orientation'] : $this->orientation;
        $this->header_text = array_key_exists('header', $pdf_options) ? $pdf_options['header'] : $this->header_text;
        $this->subheader_text = array_key_exists('subheader', $pdf_options) ? $pdf_options['subheader'] : $this->subheader_text;
        $this->report_columns = $pdf_options['columns'];
        $this->show_subheader = $pdf_options['show_subheader'] ? $pdf_options['show_subheader'] : $this->show_subheader;
        $this->show_header = (bool) $this->header_text;
        $this->footer_text = array_key_exists('footer_text', $pdf_options) ? $pdf_options['footer_text'] : '';
        $this->doctor_title = array_key_exists('doctor_title', $pdf_options) ? $pdf_options['doctor_title'] : '__toString';
        $this->date_format = array_key_exists('date_format', $pdf_options) ? $pdf_options['date_format'] : ClientParamTable::getDefaultForValue('date_format');
        $this->datetime_format = array_key_exists('datetime_format', $pdf_options) ? $pdf_options['datetime_format'] : ClientParamTable::getDefaultForValue('date_format') . ' ' . ClientParamTable::getDefaultForValue('time_format');
        $this->includeDateRange = array_key_exists('include_date_range', $pdf_options) ? $pdf_options['include_date_range'] : ClientParamTable::getDefaultForValue('appointment_search_pdf_include_date_range');
        $this->dateRangeStart = array_key_exists('start_date', $pdf_options) ? $pdf_options['start_date'] : '';
        $this->dateRangeEnd = array_key_exists('end_date', $pdf_options) ? $pdf_options['end_date'] : '';
        $this->pagerText = array_key_exists('pagerText', $pdf_options) ? $pdf_options['pagerText'] : '';

        parent::__construct($this->orientation, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $this->setFontSubsetting(false);
        $this->SetCreator(PDF_CREATOR);
        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $this->SetAutoPageBreak(TRUE, PDF_MARGIN_TOP - 10);
        $this->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $this->setPrintHeader(false);
        $this->setPrintFooter(true);

        $this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP - 20, PDF_MARGIN_RIGHT - 10);
        $this->AddPage();


        $this->line_width = $this->LineWidth;
        return $this;
    }

    public function addHeading() {
        if ($this->show_header) {
            $this->SetFont('dejavusans', 'B', 20);
            $this->multiCell(0, 0, $this->header_text, '0', 'C', 0, 1, '', '', true, 0);
        }

        if ($this->show_subheader) {
            $this->SetFont('dejavusans', 'B', 15);
            $this->multiCell(0, 0, $this->subheader_text, '0', 'C', 0, 1, '', '', true, 0);
        }

        if ($this->includeDateRange) {
            $text = '';
            $start = date($this->date_format, $this->dateRangeStart);
            $end = date($this->date_format, $this->dateRangeEnd);

            if ($start == $end) {
                $text = $start;
            } else {
                $text = $start . ' - ' . $end;
            }

            $this->SetFont('dejavusans', 'B', 9);
            $this->multiCell(0, 0, $text, '0', 'C', 0, 1, '', '', true, 0);
        }

        $this->SetFont('dejavusans', '', 8, '', true);
    }

    protected function writeMainTableHeader() {
        $max_lines = 0;
        $count = count($this->report_columns) - 1;

        foreach ($this->report_columns as $column) {
            $data = array_key_exists($column, $this->report_headers) ? $this->report_headers[$column] : $column;
            $max_lines = max($max_lines, $this->getNumLines($data, $this->column_widths[$column]));
        }
        foreach ($this->report_columns as $col_num => $column) {
            $data = array_key_exists($column, $this->report_headers) ? $this->report_headers[$column] : $column;
            $border = $count == $col_num ? 'B' : 'BR';
            $this->MultiCell($this->column_widths[$column], $max_lines * $this->line_height_factor, $data, $border, 'L', 0, 0);
        }
        $this->Ln();
    }

    protected function getMainTableRowLineCount($data) {
        $dimensions = $this->getPageDimensions();
        $startY = $this->getY();
        $max_lines = 0;

        foreach ($this->report_columns as $column) {
            if (array_key_exists($column, $data)) {
                if (strlen($data[$column])) {
                    $max_lines = max($max_lines, $this->getNumLines($data[$column], $this->column_widths[$column]));
                }
                if (($startY + $max_lines * $this->line_height_factor) + $dimensions['bm'] > ($dimensions['hk'])) {
                    return -1;
                }
            }
        }
        return $max_lines;
    }

    protected function writeMainTableRow($data) {
        $max_lines = $this->getMainTableRowLineCount($data);
        $count = count($this->report_columns) - 1;

        if ($max_lines == -1) {
            $this->AddPage();
            $this->writeMainTableHeader();
        }

        $this->SetFont('dejavusans', '', 8, '', true);
        foreach ($this->report_columns as $col_num => $column) {
            if (array_key_exists($column, $this->column_widths) && array_key_exists($column, $data)) {
                $border = $count == $col_num ? 'B' : 'TR';
                $align = array_key_exists($column, $this->column_alignments) ? $this->column_alignments[$column] : 'L';
                $this->MultiCell($this->column_widths[$column], $max_lines * $this->line_height_factor, $data[$column], $border, $align, 0, 0);
            }
        }
        $this->Ln();
    }

    public function Footer() {

        $this->SetY(-15);
        $this->Cell(20, 10, 'Printed on:' .date($this->datetime_format), 0, false, 'L', 0, '', 0, false, 'T', 'M');
        if ($this->footer_text) {
        $this->Cell(150, 10, $this->footer_text,  0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
        $this->Cell(0, 10,  'Page ' . $this->getAliasNumPage() . ' of ' . $this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        
        // $this->SetY(-15);
        // if ($this->footer_text) {
        //     $this->Cell(.5, 10, $this->footer_text, 0, false, 'L', 0, '', 0, false, 'T', 'M');
        // }

        // $pageText = 'Page ' . $this->getAliasNumPage() . ' of ' . $this->getAliasNbPages();
        // if ($this->pagerText) {
        //     $pageText .= '   [' . $this->pagerText . ']';
        // }

        // $this->Cell(0, 10, $pageText, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        // $this->Cell(0, 10, date($this->datetime_format), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }

    protected function calculateColumnWidths($column_settings) {
        $column_widths = array();
        $page_width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;

        $ratio_total = 0;

        foreach ($column_settings['fixed'] as $col => $width) {
            if (in_array($col, $this->report_columns)) {
                $column_widths[$col] = $width;
                $page_width = $page_width - $width;
            }
        }

        foreach ($column_settings['ratio'] as $col => $ratio) {
            if (in_array($col, $this->report_columns)) {
                $ratio_total += $ratio;
            }
        }
        foreach ($column_settings['ratio'] as $col => $ratio) {
            if (in_array($col, $this->report_columns)) {
                $column_widths[$col] = round(($ratio / $ratio_total) * $page_width);
            }
        }
        $this->column_alignments = $column_settings['aligns'];

        $last_col = null;
        $page_width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;
        foreach ($column_widths as $col => $width) {
            $page_width = $page_width - $width;
            $last_col = $col;
        }
        $column_widths[$last_col] += $page_width;

        $this->column_widths = $column_widths;
    }

    public static function getFooter($pdf) {
        // $pdf->SetY(-15);
        // $pdf->Cell(0, 10, 'Page ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        // $pdf->Cell(0, 10, date($pdf->datetime_format), 0, false, 'R', 0, '', 0, false, 'T', 'M');

        $this->SetY(-15);
        $this->Cell(20, 10, 'Printed on:' .date($pdf->datetime_format), 0, false, 'L', 0, '', 0, false, 'T', 'M');
        if ($pdf->footer_text) {
        $this->Cell(150, 10, $pdf->footer_text,  0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
        $this->Cell(0, 10,  'Page ' . $pdf->getAliasNumPage() . ' of ' . $pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }

    public static function getPageOrienatationOptions() {
        return array('P' => 'Portrait', 'L' => 'Landscape');
    }

}
