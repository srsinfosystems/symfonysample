<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class PastClaimsPdf extends reportPdf
{
 	protected $show_totals = false;
 	protected $writing_table = false;

 	public function showTotals($v)
 	{
 		$this->show_totals = $v;
 	}
 	protected function writeReportSubtotalRow($title, $fee_paid, $fee_subm)
 	{
 		$dimensions = $this->getPageDimensions();
 		$startY = $this->GetY();
 		$max_lines = 0;
 		$max_lines = max ($max_lines, $this->getNumLines($fee_subm, $this->footer_widths['fee_subm']));
 		$max_lines = max ($max_lines, $this->getNumLines($fee_paid, $this->footer_widths['fee_paid']));
 		$max_lines = max ($max_lines, $this->getNumLines($title, $this->footer_widths['left']));

 		if (($startY + $max_lines * $this->line_height_factor) + $dimensions['bm'] > ($dimensions['hk'])) {
 			$this->AddPage();
 			$this->writeMainTableHeader();
 		}

 		$this->MultiCell($this->footer_widths['left'], $max_lines * $this->line_height_factor, $title, 0, 'L', 0, 0);

 		if (in_array('fee_subm', $this->report_columns)) {
 			$align = array_key_exists('fee_subm', $this->column_alignments) ? $this->column_alignments['fee_subm'] : 'L';
 			$this->MultiCell($this->footer_widths['fee_subm'], $max_lines * $this->line_height_factor, $fee_subm, 0, $align, 0, 0);
 		}

 		if (in_array('fee_paid', $this->report_columns)) {
 			$align = array_key_exists('fee_paid', $this->column_alignments) ? $this->column_alignments['fee_paid'] : 'L';
 			$this->MultiCell($this->footer_widths['fee_paid'], $max_lines * $this->line_height_factor, $fee_paid, 0, $align, 0, 0);
 		}
 		if ($this->footer_widths['right']) {
 			$this->MultiCell($this->footer_widths['right'], $max_lines * $this->line_height_factor, ' ', 0, 'L', 0, 0);
 		}
 		$this->Ln();
 	}
 	protected function writeReportSubtotal($paids, $submitteds)
 	{
 		foreach ($paids as $pay_prog => $total)
 		{
 			$this->writeReportSubtotalRow($pay_prog . ' Total', '$' . number_format($paids[$pay_prog], 2), '$' . number_format($submitteds[$pay_prog], 2));
 		}
 	}
 	public function writeTotalsTable($subtotals, $totals)
 	{
        ini_set('memory_limit','-1');
 		$width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;

 		$columnWidths = array(
 			'title' => floor(1/6 * $width),
 			'fee_subm' => floor(1/6 * $width),
 			'fee_paid' => floor(1/6 * $width),
 			'num_claims' => floor(1/6 * $width),
 			'num_items' => floor(1/6 * $width),
 			'num_patients' => $width - (5 * floor(1/6 * $width))
 		);
 		$columnTitles = array(
 			'title' => '',
 			'fee_subm' => 'Fee Submitted',
 			'fee_paid' => 'Fee Paid',
 			'num_claims' => '# Claims',
 			'num_items' => '# Claim Items',
 			'num_patients' => '# Patients'
 		);

 		$this->Ln();
 		foreach ($columnTitles as $colName => $text)
 		{
 			$border = $colName == 'num_patients' ? 'B' : 'BR';
 			$this->MultiCell($columnWidths[$colName], $this->line_height_factor, $text, $border, 'L', 0, 0);
 		}
 		$this->Ln();

 		if (array_key_exists('pay_progs', $subtotals)) {
 			foreach ($subtotals['pay_progs'] as $pay_prog => $data)
 			{
 				$this->MultiCell($columnWidths['title'], $this->line_height_factor, $pay_prog, 'BR', 'L', 0, 0);
 				$this->MultiCell($columnWidths['fee_subm'], $this->line_height_factor, '$' . number_format($data['submitted'], 2, '.', ','), 'BR', 'L', 0, 0);
 				$this->MultiCell($columnWidths['fee_paid'], $this->line_height_factor, '$' . number_format($data['paid'], 2, '.', ','), 'BR', 'L', 0, 0);
 				$this->MultiCell($columnWidths['num_claims'], $this->line_height_factor, $data['claims'] , 'BR', 'L', 0, 0);
 				$this->MultiCell($columnWidths['num_items'], $this->line_height_factor, $data['claimItemCount'], 'BR', 'L', 0, 0);
 				$this->MultiCell($columnWidths['num_patients'], $this->line_height_factor, 'n/a', 'B', 'L', 0, 0);

 				$this->Ln();
 			}
 		}

 		$this->MultiCell($columnWidths['title'], $this->line_height_factor, 'Report Subtotal', 'BR', 'L', 0, 0);
 		$this->MultiCell($columnWidths['fee_subm'], $this->line_height_factor, '$' . number_format($subtotals['submitted'], 2, '.', ','), 'BR', 'L', 0, 0);
 		$this->MultiCell($columnWidths['fee_paid'], $this->line_height_factor, '$' . number_format($subtotals['paid'], 2, '.', ','), 'BR', 'L', 0, 0);
 		$this->MultiCell($columnWidths['num_claims'], $this->line_height_factor, $subtotals['claims'] , 'BR', 'L', 0, 0);
 		$this->MultiCell($columnWidths['num_items'], $this->line_height_factor, $subtotals['claimItems'], 'BR', 'L', 0, 0);
 		$this->MultiCell($columnWidths['num_patients'], $this->line_height_factor, $subtotals['patients'], 'B', 'L', 0, 0);
 		$this->Ln();
 		$this->Ln();

 		foreach ($columnTitles as $colName => $text)
 		{
 			$border = $colName == 'num_patients' ? 'B' : 'BR';
 			$this->MultiCell($columnWidths[$colName], $this->line_height_factor, $text, $border, 'L', 0, 0);
 		}
 		$this->Ln();

 		if (array_key_exists('pay_progs', $totals)) {
 			foreach ($totals['pay_progs'] as $pay_prog => $data)
 			{
 				$this->MultiCell($columnWidths['title'], $this->line_height_factor, $pay_prog, 'BR', 'L', 0, 0);
 				$this->MultiCell($columnWidths['fee_subm'], $this->line_height_factor, '$' . number_format($data['submitted'], 2, '.', ','), 'BR', 'L', 0, 0);
 				$this->MultiCell($columnWidths['fee_paid'], $this->line_height_factor, '$' . number_format($data['paid'], 2, '.', ','), 'BR', 'L', 0, 0);
 				$this->MultiCell($columnWidths['num_claims'], $this->line_height_factor, $data['claims'] , 'BR', 'L', 0, 0);
 				$this->MultiCell($columnWidths['num_items'], $this->line_height_factor, $data['claimItems'], 'BR', 'L', 0, 0);
 				$this->MultiCell($columnWidths['num_patients'], $this->line_height_factor, 'n/a', 'B', 'L', 0, 0);

 				$this->Ln();
 			}
 		}

 		$this->MultiCell($columnWidths['title'], $this->line_height_factor, 'Grand Total (All Results)', 'BR', 'L', 0, 0);
 		$this->MultiCell($columnWidths['fee_subm'], $this->line_height_factor, '$' . number_format($totals['submitted'], 2, '.', ','), 'BR', 'L', 0, 0);
 		$this->MultiCell($columnWidths['fee_paid'], $this->line_height_factor, '$' . number_format($totals['paid'], 2, '.', ','), 'BR', 'L', 0, 0);
 		$this->MultiCell($columnWidths['num_claims'], $this->line_height_factor, $totals['claims'] , 'BR', 'L', 0, 0);
 		$this->MultiCell($columnWidths['num_items'], $this->line_height_factor, $totals['claimItems'], 'BR', 'L', 0, 0);
 		$this->MultiCell($columnWidths['num_patients'], $this->line_height_factor, $totals['patients'], 'B', 'L', 0, 0);
 	}
 	public function buildReportTable($rs)
 	{

        $columnHeaders = array(
            'num_serv' => 30,
            'account_num' => 80,
            'fee_subm' => 85,
            'fee_paid' => 85,
            'service_date' => 85,
            'admit_date' => 85,
            'health_card_vc' => 110,
            'spec_code' => 40,
            'dob' => 80,
            'patient_name' => 100,
            'sex' => 34,
            'claim_status_code' => 34,
            'date_created' => 120,
            'fname' => 80,
            'lname' => 80,
            'diag_code' => 40,
            'service_code' => 50,
            'errors' => 60,
            'error_reconcile' => 102,
            'reconcile' => 102,
            'updated_user_id' => 68,
            'created_user_id' => 68,
            'facility_num' => 34,
            'group_num' => 42,
            'batch_num' => 110,
            'charge_to' => 60
        );

        $myfile = fopen("tmpFile.txt", "w") or die("Unable to open file!");


        $styling = '<style>' . file_get_contents("styleforpdf.txt") . '</style>';
        fwrite($myfile, $styling);

        $tbl = '<table border="1" class="table table-condensed">
                <thead>
                <tr>';

        foreach ($this->report_columns as $header) {
            if (array_key_exists($header, $this->report_headers) && array_key_exists($header, $columnHeaders)) {
                $tbl .= '<td style=" width: ' . $columnHeaders[$header] . ';"><b>' . $this->report_headers[$header] . '</b></td>';
            } else {
                $tbl .= '<td ><b>' . $this->report_headers[$header] . '</b></td>';
            }
        }

        $tbl .= '</tr>';

        fwrite($myfile, $tbl);
        foreach ($rs['data'] as $data)
        {

            $tmp = '<tr>';
            foreach ($this->report_columns as $column) {
                if (array_key_exists($column, $data)) {
                    $tmp .= '<td style="font-size: 1.2em;">' . $data[$column] . '</td>';
                }
            }

            $tmp .= '</tr>';

            fwrite($myfile, $tmp);

        }
        fwrite($myfile, '</table>');
        fclose($myfile);


        if ($this->show_totals) {

            $totals = '<br><hr><br><table border="1" style="margin: 5 5 5 5px;"><tr>';

            $totals .= '<td style="font-size: 1.6em;"> Grand Total Submitted: $' . number_format($rs['totals']['submitted'], 2) . ' </td>';
            $totals .= '<td style="font-size: 1.6em;"> Grand Total Paid: $' . number_format($rs['totals']['paid'], 2) . ' </td>';
            $totals .= '</tr><tr>';
            $totals .= '<td style="font-size: 1.6em;"> Claims: ' . $rs['totals']['claims'] . ' </td>';
            $totals .= '<td style="font-size: 1.6em;"> Claim Items: ' . $rs['totals']['claimItems'] . ' </td>';
            $totals .= '<td style="font-size: 1.6em;"> Patients: ' . $rs['totals']['patients'] . ' </td>';


            $totals .= '</tr></table>';

        }

        $res = file_get_contents("tmpFile.txt");

        $this->writeHTML($res);
        //$this->writeHTML($totals);



        /*

        $columnHeaders = array(
            'num_serv' => 6,
            'claim_id' => 18,
            'account_num' => 18,
            'fee_subm' => 20,
            'fee_paid' => 20,
            'service_date' => 20,
            'admit_date' => 20,
            'date_created' => 20,
            'provider_number' => 18,
            'health_card_vc' => 18,
            'group_num' => 14,
            'spec_code' => 6,
            'dob' => 20,
            'expiry_date' => 20,
            'facility_num' => 6,
            'claim_status_code' => 10,
            'refdoc_num' => 10
        );

        $tbl = '<table border="1">
                <thead>
                <tr>';

        foreach ($this->report_columns as $header) {
            if (array_key_exists($header, $this->report_headers) && array_key_exists($header, $columnHeaders)) {
                $tbl .= '<td style="text-align: center; width: ' . $columnHeaders[$header] . ';"><b>' . $this->report_headers[$header] . '</b></td>';
            } else {
                $tbl .= '<td style="text-align: center;"><b>' . $this->report_headers[$header] . '</b></td>';
            }
        }

        $tbl .= '</tr>';

        foreach ($rs['data'] as $data)
        {
            $tbl .= '<tr>';

            foreach ($this->report_columns as $column) {
                if (array_key_exists($column, $data)) {
                    $tbl .= '<td style="text-align: center;">' . $data[$column] . '</td>';
                }
            }

            $tbl .= '</tr>';
        }

        $tbl .= '</table>';

        if ($this->show_totals) {
            $tbl .= '<br><hr><br><table border="1"><tr>';

            $tbl .= '<td> Submitted: $' . $rs['totals']['submitted'] . ' </td>';
            $tbl .= '<td> Paid: $' . $rs['totals']['paid'] . ' </td>';
            $tbl .= '<td> Claims: ' . $rs['totals']['claims'] . ' </td>';
            $tbl .= '<td> Claim Items: ' . $rs['totals']['claimItems'] . ' </td>';
            $tbl .= '<td> Patients: ' . $rs['totals']['patients'] . ' </td>';


            $tbl .= '</tr></table><br><br>';
        }

        $this->writeHTML($tbl);



        $this->writeTotalsTable($rs['subtotals'], $rs['totals']);

        return;
        /*
 		$header_count = 0;
 		$this->calculateColumnWidths($column_settings);

 		$claim_item_count = 0;
 		$submitted = 0;
 		$paid = 0;
 		$pay_prog_submitted = array();
 		$pay_prog_paid = array();
 		$width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;

 		$this->writeMainTableHeader();

 		foreach ($rs['data'] as $data)
 		{
 			$this->writeMainTableRow($data);
 		}
 		$this->MultiCell($width, 3, ' ', 'T', 'L', 0, 0);
 		$this->Ln();

  		if ($this->show_totals) {
  			$this->writeTotalsTable($rs['subtotals'], $rs['totals']);
  		}


 		return;
        */

        return;
 	}

 	protected function calculateColumnWidths($column_settings)
 	{
 		parent::calculateColumnWidths($column_settings);

 		$this->footer_widths = array('left' => 0, 'fee_subm' => 0, 'fee_paid' => 0, 'right' => 0);
 		foreach ($this->report_columns as $col) {
 			$width = $this->column_widths[$col];
 			if ($col == 'fee_subm') {
 				$this->footer_widths['fee_subm'] = $width;
 			}
 			else if ($col == 'fee_paid') {
 				$this->footer_widths['fee_paid'] = $width;
 			}
 			else if ($this->footer_widths['fee_subm'] || $this->footer_widths['fee_paid']) {
 				$this->footer_widths['right'] += $width;
 			}
 			else {
 				$this->footer_widths['left'] += $width;
 			}
 		}
 	}
}