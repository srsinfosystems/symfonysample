<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class appointmentTrackReportPdf extends reportPdf
{
 	public function buildReportTable($appointments, $column_settings)
 	{
 		$header_count = 0;
 		$this->calculateColumnWidths($column_settings);

 		$width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;

 		$this->writeMainTableHeader();

 		foreach ($appointments as $appointment)
 		{
 			$this->writeMainTableRow($appointment);
 		}

 		return;
 	}
 	public function addReportTableSummary($counts)
 	{
 		$page_width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;

 		$this->MultiCell($page_width, 0, '', 'T', 'L', 0, 0);
 		$this->Ln();

 		foreach ($counts as $label => $count)
 		{
 			$text = $label . ' ' . $count;
 			$this->MultiCell($page_width, $this->getNumLines($text, $page_width) * $this->line_height_factor, $text, 0, 'L', 0, 0);
 			$this->Ln();
 		}

 	}

}