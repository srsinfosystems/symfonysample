<?php
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/2/2016
 * Time: 10:49 AM
 */

require_once '../lib/vendor/dompdf/autoload.inc.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class pastClaimsPdfNew extends Dompdf {

    public function __construct($pdf_options)
    {

        $this->date_format = $pdf_options['date_format'];
        $this->subheader = $pdf_options['subheader'];
        if (array_key_exists('header', $pdf_options)) {
            $this->header = $pdf_options['header'];
        } else {
            $this->header = "Past Claims";
        }
        $this->datetime_format = $pdf_options['datetime_format'];
        $this->columns = $pdf_options['columns'];
        $this->report_headers = $pdf_options['report_headers'];

        if (array_key_exists('orientation', $pdf_options)) {
            $this->orientation = $pdf_options['orientation'];
        }


        parent::__construct();

        return $this;
    }

    public function generatePDFNoWK($rs, $report_columns) {
        ini_set('memory_limit','-1');

        if (!$this->report_headers) {
            $this->report_headers = $report_columns['report_headers'];
        }

        if (!$this->columns) {
            $this->columns = $report_columns['columns'];
        }


        $tbl = '<style>' . file_get_contents('css/bootstrap.min.css') . '</style>';
        $tbl .= '<style>' . file_get_contents('css/bootstrap-theme.min.css') . '</style>';
        $tbl .= '<style>
                    .table {
                        border-radius: 5px;
                        width: 100%;
                        margin: auto;
                        float: none;
                    }

					thead, tfoot {
						display: table-row-group;
					}
	</style>';


        $tbl .= "<h1 class=\"text-center\" style=\"color: black\">" . $this->header . " <br /></h1><p class=\"text-center\">" . $this->subheader . " | " . date($this->datetime_format) . "</p>";
        $tbl .= "<table class=\"table \">";



        $tbl .= '<thead><tr>';
        foreach ($this->columns as $header) {
            if (array_key_exists($header, $this->report_headers)) {
                $tbl .= '<th style="word-break:keep-all; font-size: 14px;">' . $this->report_headers[$header] . '</th>';
            }
        }
        $tbl .= '</tr></thead><tbody>';


        foreach ($rs['data'] as $service)
        {
            $tbl .= '<tr style="page-break-inside: avoid;">';

            foreach ($this->columns as $column) {
                if (array_key_exists($column, $service)) {
                    $tbl .= '<td style="word-break:keep-all; font-size: 12px;">' . $service[$column] . '</td>';
                }
            }

            $tbl .= '</tr>';
        }

        $tbl2 = "</tbody></table>";



        $tbl2 .= "<br>";

        if (array_key_exists('totals', $rs)) {
            $tbl2 .= "<table class=\"table\" style=\"page-break-before: always;\">";
            $tbl2 .= "<thead>";
            $tbl2 .= "<tr>";
            $tbl2 .= "<th>&nbsp;</th>";
            $tbl2 .= "<th>Fee Submitted</th>";
            $tbl2 .= "<th>Fee Paid</th>";
            $tbl2 .= "<th># Claims</th>";
            $tbl2 .= "<th>Claim Items</th>";
            $tbl2 .= "<th>Patient</th>";
            $tbl2 .= "</tr>";
            $tbl2 .= "</thead>";

            $tbl2 .= "<tbody >";

            $tbl2 .= "<tr>";

            $tbl2 .= "<td>DIRECT</td>";
            $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['DIRECT']['submitted'], 2) . "</td>";
            $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['DIRECT']['paid'], 2) . "</td>";
            $tbl2 .= "<td>" . $rs['totals']['pay_progs']['DIRECT']['claims'] . "</td>";
            $tbl2 .= "<td>" . $rs['totals']['pay_progs']['DIRECT']['claimItems'] . "</td>";
            $tbl2 .= "<td>N/A</td>";

            $tbl2 .= "</tr>";

            $tbl2 .= "<tr>";

            $tbl2 .= "<td>HCP -P</td>";
            $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['HCP - P']['submitted'], 2) . "</td>";
            $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['HCP - P']['paid'], 2) . "</td>";
            $tbl2 .= "<td>" . $rs['totals']['pay_progs']['HCP - P']['claims'] . "</td>";
            $tbl2 .= "<td>" . $rs['totals']['pay_progs']['HCP - P']['claimItems'] . "</td>";
            $tbl2 .= "<td>N/A</td>";

            $tbl2 .= "</tr>";

            $tbl2 .= "<tr>";

            $tbl2 .= "<td>RMB</td>";
            $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['RMB']['submitted'], 2) . "</td>";
            $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['RMB']['paid'], 2) . "</td>";
            $tbl2 .= "<td>" . $rs['totals']['pay_progs']['RMB']['claims'] . "</td>";
            $tbl2 .= "<td>" . $rs['totals']['pay_progs']['RMB']['claimItems'] . "</td>";
            $tbl2 .= "<td>N/A</td>";

            $tbl2 .= "</tr>";

            $tbl2 .= "<tr>";

            $tbl2 .= "<td>THIRD PARTY</td>";
            $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['THIRD PARTY']['submitted'], 2) . "</td>";
            $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['THIRD PARTY']['paid'], 2) . "</td>";
            $tbl2 .= "<td>" . $rs['totals']['pay_progs']['THIRD PARTY']['claims'] . "</td>";
            $tbl2 .= "<td>" . $rs['totals']['pay_progs']['THIRD PARTY']['claimItems'] . "</td>";
            $tbl2 .= "<td>N/A</td>";

            $tbl2 .= "</tr>";

            $tbl2 .= "<tr >";

            $tbl2 .= "<td ><b>Grand Totals</b></td>";
            $tbl2 .= "<td><b>" . number_format($rs['totals']['submitted'], 2) . "</b></td>";
            $tbl2 .= "<td><b>" . number_format($rs['totals']['paid'], 2) . "</b></td>";
            $tbl2 .= "<td><b>" . $rs['totals']['claims'] . "</b></td>";
            $tbl2 .= "<td><b>" . $rs['totals']['claimItems'] . "</b></td>";
            $tbl2 .= "<td><b>" . $rs['totals']['patients'] . "</b></td>";

            $tbl2 .= "</tr>";

            $tbl2 .= "</tbody>";

            $tbl2 .= "</table>";
        }

        $tbl2 .= "<footer class=\"footer\">


                  <br /> <br />
                     <p class=\"text-muted text-center\">Solution designed by Hype Systems Inc.</p>
                    </footer>";

        $tbl = $tbl . $tbl2;

        $this->loadHtml($tbl);
        $this->render();

    }


     public function generatePdfAppointmentNoWK($appointments, $report_columns, $headers) {
        ini_set('memory_limit','-1');

        $tbl = '<style>' . file_get_contents('css/bootstrap.css') . '</style>';
        $tbl .= '<style>' . file_get_contents('css/bootstrap-theme.css') . '</style>';
        $tbl .= '<style>
                    .table {
                        border-radius: 5px;
                        width: 100%;
                        margin: auto;
                        float: none;
                    }

                    thead, tfoot {
                        display: table-row-group;
                    }
                    </style>';

        if ($this->header) {
            $tbl .= "<h1 class=\"text-center\" style=\"color: black\">" . $this->header . " <br /></h1><p>" . $this->subheader . " | " . date($this->datetime_format) . "</p>";
        } else {
            $tbl .= "<h1 class=\"text-center\" style=\"color: black\">Appointment History </h1><br /><b>" . $this->subheader . ' ' . date($this->datetime_format) . "</b>";
        }
        $tbl .= "<table class=\"table\">";

        $tbl .= '<thead><tr>';
        foreach ($report_columns as $header) {
            if (array_key_exists($header, $headers)) {
                $tbl .= '<th style="word-break:keep-all; font-size: 14px;">' . $headers[$header] . '</th>';
            }
        }
        $tbl .= '</tr></thead><tbody>';


        foreach ($appointments as $appointment)
        {
            $tbl .= '<tr style="page-break-inside: avoid;">';

            foreach ($report_columns as $column) {
                if (array_key_exists($column, $appointment)) {
                    $tbl .= '<td style="word-break:keep-all; font-size: 14px;">' . $appointment[$column] . '</td>';
                }
            }

            $tbl .= '</tr>';
        }

        $tbl .= "</tbody></table>";
        $tbl .= "<footer class=\"footer\">
              <br /> <br />
                 <p class=\"text-muted text-center\">Solution designed by Hype Systems Inc.</p>
                </footer>";

        $this->loadHtml($tbl);
        $this->render();

    }

    public function generatePDF($rs, $report_columns)
    {
        ini_set('memory_limit','-1');
        $myfile = fopen("tmpFilePDF.html", "w") or die("Unable to open file!");

        if (!$this->report_headers) {
            $this->report_headers = $report_columns['report_headers'];
        }

        if (!$this->columns) {
            $this->columns = $report_columns['columns'];
        }

        //$styling = '<style>' . file_get_contents("styleforpdf.txt") . '</style>';
        //fwrite($myfile, $styling);

        $tbl = '<style>' . file_get_contents('css/bootstrap.min.css') . '</style>';
        $tbl .= '<style>' . file_get_contents('css/bootstrap-theme.min.css') . '</style>';
        $tbl .= '<style>
                    .table {
                        border-radius: 5px;
                        width: 100%;
                        margin: auto;
                        float: none;
                    }

					thead, tfoot {
						display: table-row-group;
					}
	</style>';


        $tbl .= "<h1 class=\"text-center\">" . $this->header . " <br /><small>" . $this->subheader . " | " . date($this->datetime_format) . "</small></h1>";
        //$tbl .= "<div class=\"container\">";
        $tbl .= "<table class=\"table \">";



        $tbl .= '<thead><tr>';
        foreach ($this->columns as $header) {
            if (array_key_exists($header, $this->report_headers)) {
                $tbl .= '<th style="word-break:keep-all; font-size: 16px;">' . $this->report_headers[$header] . '</th>';
            }
        }
        $tbl .= '</tr></thead><tbody>';

        fwrite($myfile, $tbl);

        foreach ($rs['data'] as $service)
        {
            $tmp = '<tr style="page-break-inside: avoid;">';

            foreach ($this->columns as $column) {
                if (array_key_exists($column, $service)) {
                    $tmp .= '<td style="word-break:keep-all; font-size: 14px;">' . $service[$column] . '</td>';
                }
            }

            $tmp .= '</tr>';

            fwrite($myfile, $tmp);
        }

        $tbl2 = "</tbody></table>";

        $tbl2 .= "<br>";

        $tbl2 .= "<table class=\"table\" style=\"page-break-before: always;\">";
        $tbl2 .= "<thead>";
        $tbl2 .= "<tr>";
        $tbl2 .= "<th>&nbsp;</th>";
        $tbl2 .= "<th>Fee Submitted</th>";
        $tbl2 .= "<th>Fee Paid</th>";
        $tbl2 .= "<th># Claims</th>";
        $tbl2 .= "<th>Claim Items</th>";
        $tbl2 .= "<th>Patient</th>";
        $tbl2 .= "</tr>";
        $tbl2 .= "</thead>";

        $tbl2 .= "<tbody >";

        $tbl2 .= "<tr>";

        $tbl2 .= "<td>DIRECT</td>";
        $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['DIRECT']['submitted'], 2) . "</td>";
        $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['DIRECT']['paid'], 2) . "</td>";
        $tbl2 .= "<td>" . $rs['totals']['pay_progs']['DIRECT']['claims'] . "</td>";
        $tbl2 .= "<td>" . $rs['totals']['pay_progs']['DIRECT']['claimItems'] . "</td>";
        $tbl2 .= "<td>N/A</td>";

        $tbl2 .= "</tr>";

        $tbl2 .= "<tr>";

        $tbl2 .= "<td>HCP -P</td>";
        $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['HCP - P']['submitted'], 2) . "</td>";
        $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['HCP - P']['paid'], 2) . "</td>";
        $tbl2 .= "<td>" . $rs['totals']['pay_progs']['HCP - P']['claims'] . "</td>";
        $tbl2 .= "<td>" . $rs['totals']['pay_progs']['HCP - P']['claimItems'] . "</td>";
        $tbl2 .= "<td>N/A</td>";

        $tbl2 .= "</tr>";

        $tbl2 .= "<tr>";

        $tbl2 .= "<td>RMB</td>";
        $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['RMB']['submitted'], 2) . "</td>";
        $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['RMB']['paid'], 2) . "</td>";
        $tbl2 .= "<td>" . $rs['totals']['pay_progs']['RMB']['claims'] . "</td>";
        $tbl2 .= "<td>" . $rs['totals']['pay_progs']['RMB']['claimItems'] . "</td>";
        $tbl2 .= "<td>N/A</td>";

        $tbl2 .= "</tr>";

        $tbl2 .= "<tr>";

        $tbl2 .= "<td>THIRD PARTY</td>";
        $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['THIRD PARTY']['submitted'], 2) . "</td>";
        $tbl2 .= "<td>" . number_format($rs['totals']['pay_progs']['THIRD PARTY']['paid'], 2) . "</td>";
        $tbl2 .= "<td>" . $rs['totals']['pay_progs']['THIRD PARTY']['claims'] . "</td>";
        $tbl2 .= "<td>" . $rs['totals']['pay_progs']['THIRD PARTY']['claimItems'] . "</td>";
        $tbl2 .= "<td>N/A</td>";

        $tbl2 .= "</tr>";

        $tbl2 .= "<tr >";

        $tbl2 .= "<td ><b>Grand Totals</b></td>";
        $tbl2 .= "<td><b>" . number_format($rs['totals']['submitted'], 2) . "</b></td>";
        $tbl2 .= "<td><b>" . number_format($rs['totals']['paid'], 2) . "</b></td>";
        $tbl2 .= "<td><b>" . $rs['totals']['claims'] . "</b></td>";
        $tbl2 .= "<td><b>" . $rs['totals']['claimItems'] . "</b></td>";
        $tbl2 .= "<td><b>" . $rs['totals']['patients'] . "</b></td>";

        $tbl2 .= "</tr>";

        $tbl2 .= "</tbody>";

        $tbl2 .= "</table>";



        $tbl2 .= "<footer class=\"footer\">


                  <br /> <br />
                     <p class=\"text-muted text-center\">Solution designed by Hype Systems Inc.</p>
                    </footer>";


        fwrite($myfile, $tbl2);
        //fwrite($myfile, $totals);

        //$this->loadHtml(file_get_contents("tmpFilePDF.html"));
        //$this->render();

        fclose($myfile);
    }

}