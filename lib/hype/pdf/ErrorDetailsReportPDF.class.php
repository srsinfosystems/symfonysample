<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class ErrorDetailsReportPDF extends TCPDF
{
	protected $dateTimeFormat;
	protected $dateFormat;
	protected $footerText;
	protected $lineWidth;
	protected $reportColumns = array();
	protected $reportHeader;
	protected $query;
	protected $line_height_factor = 4.5;
	protected $clientID;

	public function __construct($options = array())
	{
		$this->dateTimeFormat = array_key_exists('datetime_format', $options) ? $options['datetime_format'] : hype::DB_ISO_DATE;
		$this->dateFormat = array_key_exists('date_format', $options) ? $options['date_format'] : hype::DB_DATE_FORMAT;
		$this->footerText = array_key_exists('footer_text', $options) ? $options['footer_text'] : '';
		$this->clientID = $options['current_client_id'];

		parent::__construct('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

 		$this->setFontSubsetting(false);
 		$this->SetCreator(PDF_CREATOR);
 		$this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
 		$this->SetAutoPageBreak(TRUE, PDF_MARGIN_TOP - 10);
 		$this->setImageScale(PDF_IMAGE_SCALE_RATIO);

 		$this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
 		$this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

 		$this->setPrintHeader(false);
 		$this->setPrintFooter(true);

 		$this->SetMargins(5, PDF_MARGIN_TOP - 20, 5);
 		$this->AddPage();

 		$this->lineWidth = $this->LineWidth;
 		return $this;
	}

	public function setReportHeader($v)
	{
		$this->reportHeader = $v;
	}
	public function setReportColumns($columns)
	{
		$column_settings = array(
			'fixed' => array(
            ),
			'ratio' => array(),
			'aligns' => array(),
		);

		foreach ($columns as $info)
		{
			if ($info['rs_key'] !== 'actions' && $info['rs_key'] != 'still_rejected' && $info['rs_key'] !== 'HX8Lines') {
				list ($widthType, $widthSize) = explode(':', $info['width']);
				$this->reportColumns[] = $info['rs_key'];
				$this->report_headers[$info['rs_key']] = $info['header_text'];

				$column_settings[$widthType][$info['rs_key']] = $widthSize;
			}
		}

		$this->calculateColumnWidths($column_settings);
	}
	public function setQuery($q)
	{
		$this->query = $q;
	}
	public function Footer()
	{
		$this->SetY(-15);
		if ($this->footerText) {
			$this->Cell(.5, 10, $this->footerText, 0, false, 'L', 0, '', 0, false, 'T', 'M');
		}

		$pageText = 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages();

		$this->Cell(0, 10, $pageText, 0, false, 'C', 0, '', 0, false, 'T', 'M');
		$this->Cell(0, 10, date($this->dateTimeFormat), 0, false, 'R', 0, '', 0, false, 'T', 'M');

	}

	public function build()
	{
		$this->SetFont('dejavusans', 'B', 20);
		$this->multiCell(0, 0, $this->reportHeader, '0', 'C', 0, 1, '', '', true, 0);

		$this->SetFont('dejavusans', 'B', 15);
		$this->multiCell(0, 0, '', '0', 'C', 0, 1, '', '', true, 0);

		$this->SetFont('dejavusans', '', 8, '', true);

		$this->writeMainTableHeader();

		$data = $this->query->execute();

		$options = array(
			'error_codes_format' => 'string',
			'date_format' => $this->dateFormat,
			'current_client_id' => $this->clientID,
		);
		foreach ($data as $record)
		{
			$rs = $record->getGenericJSON($this->reportColumns, $options);
			$this->writeMainTableRow($rs);
		}
	}

 	protected function calculateColumnWidths($column_settings)
 	{
 		$column_widths = array();
 		$page_width = $this->getPageWidth();

 		$ratio_total = 0;

 		foreach ($column_settings['fixed'] as $col => $width)
 		{
 			if (in_array($col, $this->reportColumns)) {
 				$column_widths[$col] = $width;
 				$page_width = $page_width - $width;
 			}
 		}

 		foreach ($column_settings['ratio'] as $col => $ratio)
 		{
 			if (in_array($col, $this->reportColumns)) {
 				$ratio_total += $ratio;
 			}
 		}

 		foreach ($column_settings['ratio'] as $col => $ratio)
 		{
 			if (in_array($col, $this->reportColumns)) {
 				$column_widths[$col] = round(($ratio / $ratio_total) * $page_width);
 			}
 		}
 		$this->column_alignments = $column_settings['aligns'];

 		$last_col = null;
 		$page_width = $this->getPageWidth();
 		foreach ($column_widths as $col => $width)
 		{
 			$page_width = $page_width - $width;
 			$last_col = $col;
 		}
 		$column_widths[$last_col] += $page_width;

 		$this->column_widths = $column_widths;
 	}
	protected function writeMainTableHeader()
 	{
 		$max_lines = 0;
 		$count = count($this->reportColumns) - 1;

 		foreach ($this->reportColumns as $column)
 		{
 			$data = array_key_exists($column, $this->report_headers) ? $this->report_headers[$column] : $column;
 			$max_lines = max($max_lines, $this->getNumLines($data, $this->column_widths[$column]));
 		}
 		foreach ($this->reportColumns as $col_num => $column)
 		{
 			$data = array_key_exists($column, $this->report_headers) ? $this->report_headers[$column] : $column;
 			$border = $count == $col_num ? 'B' : 'BR';
 			$this->MultiCell($this->column_widths[$column], $max_lines * $this->line_height_factor, $data, $border, 'L', 0, 0);
 		}
 		$this->Ln();
 	}
 	protected function writeMainTableRow($data)
 	{
 		$max_lines = $this->getMainTableRowLineCount($data);
 		$count = count($this->reportColumns) - 1;

 		if ($max_lines == -1) {
 			$this->AddPage();
 			$this->writeMainTableHeader();
 		}

        $appendedMessage = '';
 		foreach ($this->reportColumns as $col_num => $column)
 		{

                $border = $count == $col_num ? '' : 'R';
                $align = array_key_exists($column, $this->column_alignments) ? $this->column_alignments[$column] : 'L';
                $this->MultiCell($this->column_widths[$column], $max_lines * $this->line_height_factor, $data[$column], $border, $align, 0, 0);

        }



        $this->Ln();

        if ($appendedMessage) {
            $this->MultiCell(0, 0, $appendedMessage, 0, 0, 0, 0);
        }

 	}
 	protected function getMainTableRowLineCount($data)
 	{
 		$dimensions = $this->getPageDimensions();
 		$startY = $this->getY();
 		$max_lines = 0;

 		foreach ($this->reportColumns as $column)
 		{
 			if (strlen($data[$column])) {
 				$max_lines = max($max_lines, $this->getNumLines($data[$column], $this->column_widths[$column]));
 			}
 			if (($startY + $max_lines * $this->line_height_factor) + $dimensions['bm'] > ($dimensions['hk'])) {
 				return -1;
 			}
 		}
 		return $max_lines;
 	}

}
