<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class claimOhipPdf extends TCPDF
{
 	private $claim = null;
 	private $line_height_factor = 4.5;
 	protected $footer_text;

 	protected $date_format;
 	protected $datetime_format;
 	protected $orientation = 'P';

 	public function __construct($claim, $footer, $date_format, $datetime_format)
 	{
 		$this->date_format = $date_format;
 		$this->datetime_format = $datetime_format;
 		$this->footer_text = $footer;
 		parent::__construct($this->orientation, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

 		$this->SetCreator(PDF_CREATOR);
 		$this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
 		$this->SetAutoPageBreak(TRUE, PDF_MARGIN_TOP - 10);
 		$this->setImageScale(PDF_IMAGE_SCALE_RATIO);

 		$this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
 		$this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

 		$this->setPrintHeader(false);
 		$this->setPrintFooter(true);

 		$this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP - 20, PDF_MARGIN_RIGHT);
 		$this->setCellHeightRatio(1);
 		$this->AddPage();

 		$this->claim = $claim;

 		$this->addClaimDetailsTable();
 		$this->addServiceCodeTable();

 		return $this;
 	}
 	private function addClaimDetailsTable()
 	{
 		$toString = ClientParamTable::getDefaultForValue('doctor_title_format');

 		$cp = ClientParamTable::getParamForClient($this->claim->client_id, 'doctor_title_format');
 		if ($cp instanceof ClientParam) {
 			$toString = $cp->value;
 		}

 		$rows = array(
 			'Patient ID'         => $this->claim->patient_id . ' ',
 			'Health Card Number' => hype::decryptHCN($this->claim->health_num) . ' ',
 			'Version Code'       => $this->claim->version_code . ' ',
 			'First Name'         => $this->claim->fname . ' ',
 			'Last Name'          => $this->claim->lname . ' ',
 			'Date of Birth'      => $this->claim->dob ? date($this->date_format, hype::parseDateToInt($this->claim->dob, hype::DB_ISO_DATE)) : ' ',
 			'Province'           => $this->claim->province . ' ',
 			'Sex'                => $this->claim->sex . ' ',
 			'Referred By'        => $this->claim->getRefDocDescription() . ' ',
 			'Note'               => $this->claim->note . ' ',
 			'Billing Doctor'     => $this->claim->Doctor->getCustomToString($toString),
 			' '                  => $this->claim->Doctor->getOhipClaimPDFString(),
 			'Claim Type'         => $this->claim->pay_prog . ' ',
 			'Facility Number'    => $this->claim->facility_num . ' ',
 			'Admit Date'         => $this->claim->admit_date ? date($this->date_format, hype::parseDateToInt($this->claim->admit_date, hype::DB_ISO_DATE)) : ' ',
 			'Referring Lab'      => $this->claim->ref_lab . ' ',
 			'Manual Review' => $this->claim->manual_review ? 'Yes' : 'No'
 		);

 		$this->SetFont('dejavusans', '', 7);
 		foreach ($rows as $key => $value)
 		{
 			$line_width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;

 			$this->MultiCell($line_width * .5, 0, $key, 'LTRB', 'L', 0, 0, '', '', true, 0);
 			$this->MultiCell(0, 0, $value, 'LTRB', 'L', 0, 1, '', '', true, 0);
 		}
 	}
 	private function addServiceCodeTable()
 	{
 		$line_width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;

 		$this->Ln();
 		$this->SetFont('dejavusans', 'B', 8, '');
 		$this->MultiCell($line_width * .2, 0, 'Service Code', 0, 'C', 0, 0);
 		$this->MultiCell($line_width * .2, 0, 'Diagnostic Code', 0, 'C', 0, 0);
 		$this->MultiCell($line_width * .2, 0, '# of Services', 0, 'C', 0, 0);
 		$this->MultiCell($line_width * .2, 0, 'Fee Billed', 0, 'C', 0, 0);
 		$this->MultiCell($line_width * .2, 0, 'Service Date', 0, 'C', 0, 0);
 		$this->Ln();

 		$this->SetFont('dejavusans', '', 8, '');
 		foreach ($this->claim->ClaimItem as $claim_item)
 		{
 			$desc = $claim_item->serviceCodeDescription();

 			$this->MultiCell($line_width * .2, 0, $claim_item->service_code, 'LTRB', 'L', 0, 0);
 			$this->MultiCell($line_width * .2, 0, $claim_item->diag_code, 'LTRB', 'L', 0, 0);
 			$this->MultiCell($line_width * .2, 0, $claim_item->num_serv, 'LTRB', 'R', 0, 0);
 			$this->MultiCell($line_width * .2, 0, '$' . number_format($claim_item->fee_subm, 2), 'LTRB', 'R', 0, 0);
 			$this->MultiCell($line_width * .2, 0, date($this->date_format, hype::parseDateToInt($claim_item->service_date, hype::DB_ISO_DATE)), 'LTRB', 'L', 0, 0);
 			$this->Ln();
 		}

 	}
 	public function Footer()
 	{
 		  $this->SetY(-15);
        $this->Cell(20, 10, 'Printed on:' .date($this->datetime_format), 0, false, 'L', 0, '', 0, false, 'T', 'M');
        if ($this->footer_text) {
        $this->Cell(150, 10, $this->footer_text,  0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
        $this->Cell(0, 10,  'Page ' . $this->getAliasNumPage() . ' of ' . $this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');

 	}
}
 