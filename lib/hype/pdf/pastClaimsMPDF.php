<?php
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 2/13/2017
 * Time: 10:58 AM
 */

require_once '../lib/vendor/vendor/autoload.php';


class pastclaimsMPDF
{

    public function __construct()
    {


    }

    public function generatePDF($html) {
        $mpdf = new mPDF();

        $mpdf->debug = true;


        //$stylesheet1 = file_get_contents('../lib/vendor/mpdf/theme.blue.css');
        //$stylesheet2 = file_get_contents('../lib/vendor/mpdf/layout.css');

        //$mpdf->WriteHTML($stylesheet1, 1);
       //$mpdf->WriteHTML($stylesheet2, 1);
        $mpdf->WriteHTML($html);

        $filePath = $mpdf->Output();
        return $filePath;
    }
}

