<?php

//require_once '../lib/vendor/dompdf/autoload.inc.php';

// reference the Dompdf namespace
//use Dompdf\Dompdf;

class appointmentReportPdf extends reportPdf
{
    public function __construct($pdf_options)
    {
        $this->date_format = $pdf_options['date_format'];
        $this->subheader = $pdf_options['subheader'];
        $this->header = $pdf_options['header'];
        $this->datetime_format = $pdf_options['datetime_format'];
        $this->columns = $pdf_options['columns'];
        $this->report_headers = $pdf_options['report_headers'];
        $this->orientation = $pdf_options['orientation'];

        $this->datetime_format = $pdf_options['datetime_format'];

        $this->start_date = $pdf_options['start_date'];
        $this->end_date = $pdf_options['end_date'];

        parent::__construct();


        return $this;

    }

 	public function buildReportTableNonTCPDF($appointments, $column_settings)
 	{

        ini_set('memory_limit', -1);

        $tbl = '<table border="1" >
                <thead>
                <tr>';

        foreach ($this->report_columns as $header) {
            if (array_key_exists($header, $this->report_headers)) {
                $tbl .= '<td style="text-align: center;"><b>' . $this->report_headers[$header] . '</b></td>';
            }
        }

        $tbl .= '</tr>';

        foreach ($appointments as $appointment)
        {
            $tbl .= '<tr>';

            foreach ($this->report_columns as $column) {
                if (array_key_exists($column, $appointment)) {
                    $tbl .= '<td style="text-align: center; font-size: large;">' . $appointment[$column] . '</td>';
                }
            }

            $tbl .= '</tr>';
        }


        $tbl .= '</table>';

        $this->writeHTML($tbl, true, false, false, false, '');


        return;
 	}

    public function buildReportTable($appointments, $column_settings)
    {
        $header_count = 0;
        $this->calculateColumnWidths($column_settings);

        $width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;

        $this->writeMainTableHeader();

        foreach ($appointments as $appointment)
        {
            $this->writeMainTableRow($appointment);
        }

        return;
    }

 	public function addReportTableSummary($counts)
 	{
 		$page_width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;

 		$this->MultiCell($page_width, 0, '', 'T', 'L', 0, 0);
 		$this->Ln();

 		foreach ($counts as $label => $count)
 		{
 			$text = $label . ' ' . $count;
 			$this->MultiCell($page_width, $this->getNumLines($text, $page_width) * $this->line_height_factor, $text, 0, 'L', 0, 0);
 			$this->Ln();
 		}

 	}

    public function generatePdfNoWK($appointments, $report_columns, $headers) {
        ini_set('memory_limit','-1');

        $tbl = '<style>' . file_get_contents('css/bootstrap.css') . '</style>';
        $tbl .= '<style>' . file_get_contents('css/bootstrap-theme.css') . '</style>';
        $tbl .= '<style>
                    .table {
                        border-radius: 5px;
                        width: 100%;
                        margin: auto;
                        float: none;
                    }

                    thead, tfoot {
						display: table-row-group;
					}
                    </style>';

        if ($this->header) {
            $tbl .= "<h1 class=\"text-center\" style=\"color: black\">" . $this->header . " <br /></h1><p>" . $this->subheader . " | " . date($this->datetime_format) . "</p>";
        } else {
            $tbl .= "<h1 class=\"text-center\" style=\"color: black\">Appointment History </h1><br /><b>" . $this->subheader . ' ' . date($this->datetime_format) . "</b>";
        }
        $tbl .= "<table class=\"table\">";

        $tbl .= '<thead><tr>';
        foreach ($report_columns as $header) {
            if (array_key_exists($header, $headers)) {
                $tbl .= '<th style="word-break:keep-all; font-size: 14px;">' . $headers[$header] . '</th>';
            }
        }
        $tbl .= '</tr></thead><tbody>';


        foreach ($appointments as $appointment)
        {
            $tbl .= '<tr style="page-break-inside: avoid;">';

            foreach ($report_columns as $column) {
                if (array_key_exists($column, $appointment)) {
                    $tbl .= '<td style="word-break:keep-all; font-size: 14px;">' . $appointment[$column] . '</td>';
                }
            }

            $tbl .= '</tr>';
        }

        $tbl .= "</tbody></table>";
        $tbl .= "<footer class=\"footer\">
              <br /> <br />
                 <p class=\"text-muted text-center\">Solution designed by Hype Systems Inc.</p>
                </footer>";

        $this->loadHtml($tbl);
        $this->render();

    }

    //$rs, AppointmentSearchForm::getPdfColumnSettings()
    public function generatePdf($appointments, $report_columns, $headers) {
        ini_set('memory_limit','-1');

        $myfile = fopen("tmpFilePDF" . count($appointments) . ".html", "w") or die("Unable to open file!");

        $tbl = '<style>' . file_get_contents('css/bootstrap.css') . '</style>';
        $tbl .= '<style>' . file_get_contents('css/bootstrap-theme.css') . '</style>';
        $tbl .= '<style>
                    .table {
                        border-radius: 5px;
                        width: 100%;
                        margin: auto;
                        float: none;

                    }

                    thead, tfoot {
						display: table-row-group;
					}

					.ellipsis-text {

                        overflow: hidden;
                        text-overflow: ellipsis;
                        max-width: 200px;
					}
                    </style>';

        if ($this->header) {
            $tbl .= "<h1 class=\"text-center\">" . $this->header . " (" . count($appointments) . ")" .
                "<br />";

            if ($this->start_date) {
                $tbl .= "<small>" . date($this->datetime_format, $this->start_date) . ' - ' . date($this->datetime_format, $this->end_date) . "</small>";
            }

            $tbl .= "</h1>";

        } else {
            $tbl .= "<h1 class=\"text-center\">Appointment History <br /><small>" . date($this->datetime_format) . "</small></h1>";
        }
        $tbl .= "<table class=\"table\">";

        $tbl .= '<thead><tr>';
        foreach ($report_columns as $header) {
            if (array_key_exists($header, $headers)) {
                $tbl .= '<th style="word-break:keep-all; font-size: 14px;">' . $headers[$header] . '</th>';
            }
        }
        $tbl .= '</tr></thead><tbody>';

        fwrite($myfile, $tbl);


        foreach ($appointments as $appointment)
        {
            $apptrow = '<tr style="page-break-inside: avoid;">';

            foreach ($report_columns as $column) {
                if (array_key_exists($column, $appointment)) {
                    if ($column == "notes" || $column == "patient_notes") {
                        $apptrow .= '<td class="ellipsis-text" style="word-break:keep-all; font-size: 14px;">' . $appointment[$column] . '</td>';
                    } else {
                        $apptrow .= '<td style="white-space: nowrap; word-break:keep-all; font-size: 14px;">' . $appointment[$column] . '</td>';
                    }
                }
            }

            $apptrow .= '</tr>';

            fwrite($myfile, $apptrow);
        }

        $footer = "</tbody></table>";
        $footer .= "<footer class=\"footer\">
              <br /> <br />
                 <p class=\"text-muted text-center\">Solution designed by Hype Systems Inc.</p>
                </footer>";

        //$this->loadHtml($footer);
        //$this->render();

        fwrite($myfile, $footer);
        fclose($myfile);
    }

}