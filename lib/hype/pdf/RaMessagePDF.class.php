<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class RaMessagePDF extends TCPDF
{
 	protected $footerText;
 	protected $dateTimeFormat;
 	protected $raMessages;

	public function __construct($pdf_options)
	{
		$this->raMessages = $pdf_options['RaMessages'];
		$this->footerText = array_key_exists('footerText', $pdf_options) ? $pdf_options['footerText'] : '';
		$this->dateTimeFormat = $pdf_options['dateTimeFormat'];


		parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$this->SetCreator(PDF_CREATOR);
		$this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$this->SetAutoPageBreak(TRUE, 0);
		$this->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->setPrintHeader(false);
		$this->setPrintFooter(true);
		$this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

		$this->line_width = $this->LineWidth;
		return $this;
	}

	public function build()
	{
		foreach ($this->raMessages as $msg)
		{
			$this->SetFont('helvetica', '', 11, '', true);
			$this->AddPage();

			$this->MultiCell(0, 1, $msg->getMessage(), 0, 'L', 0, 0, 0, 0);
		}
	}

	private function buildPatientHeader(Patient $patient, Appointment $appt)
	{
		$margins = $this->getMargins();

		$name = $patient->getLname() . ', ' . $patient->getFname();
		$hcn = $patient->getHCN() . ' ' . $patient->getHcnVersionCode();
		$pid = $patient->getID();
		$chartNo = $patient->getPatientNumber();
		$sex = $patient->getSex();
		$dob = $patient->getDobString($this->date_format, '');
		$age = $dob ? '[' . hype::getAgeOnDate($patient->getDob(), $appt->getStartDate()) . ' yrs]' : '';
		$address = $patient->getSmartStreetAddress("\n");
		$refDoc = $patient->getRefDocDescription();
		$famDoc = $patient->getFamDocDescription();

		$x = $margins['left'];
		$y = $this->getY();
		$pageWidth = $this->getPageWidth() - $margins['right'] - $margins['left'];

		$col1Width = $col3Width = intval($pageWidth / 7);
		$col2Width = intval(($pageWidth - $col1Width - $col3Width) / 2);
		$col4Width = $pageWidth - $col1Width - $col2Width - $col3Width;

		$lineHeight = $this->lineHeights['11'];

		$line2Y = $y + $lineHeight;
		$line3Y = $line2Y + $lineHeight;
		$line4Y = $line3Y + $lineHeight;
		$line5Y = $line4Y + $lineHeight;
		$line6Y = $line5Y + $lineHeight;
		$line7Y = $line6Y + $lineHeight;

 		$this->setFont('helvetica', 'B', 11);
 		$this->writeTextBox($x, $y, $col1Width, $lineHeight, 'Name:', 'R', '');
 		$this->setFont('helvetica', '', 11);
		$this->writeTextBox($x + $col1Width, $y, $col2Width, $lineHeight, $name, 'L', '');

		$this->setFont('helvetica', 'B', 11);
		$this->writeTextBox($x + $col1Width + $col2Width, $y, $col3Width, $lineHeight, 'Patient ID:', 'R', '');
		$this->setFont('helvetica', '', 11);
		$this->writeTextBox($x + $col1Width + $col2Width + $col3Width, $y, $col4Width, $lineHeight, $pid, 'L', '');

 		$this->setFont('helvetica', 'B', 11);
		$this->writeTextBox($x, $line2Y, $col1Width, $lineHeight, 'HC #:', 'R', '');
 		$this->setFont('helvetica', '', 11);
 		$this->writeTextBox($x + $col1Width, $line2Y, $col2Width, $lineHeight, $hcn, 'L', '');

 		$this->setFont('helvetica', 'B', 11);
 		$this->writeTextBox($x + $col1Width + $col2Width, $line2Y, $col3Width, $lineHeight, 'Chart No:', 'R', '');
 		$this->setFont('helvetica', '', 11);
 		$this->writeTextBox($x + $col1Width + $col2Width + $col3Width, $line2Y, $col4Width, $lineHeight, $chartNo, 'L', '');

 		$this->setFont('helvetica', 'B', 11);
 		$this->writeTextBox($x, $line3Y, $col1Width, $lineHeight, 'DOB:', 'R', '');
 		$this->setFont('helvetica', '', 11);
 		$this->writeTextBox($x + $col1Width, $line3Y, $col2Width, $lineHeight, $dob . ' ' . $age, 'L', '');

 		$this->setFont('helvetica', 'B', 11);
 		$this->writeTextBox($x + $col1Width + $col2Width, $line3Y, $col3Width, $lineHeight, 'Sex:', 'R', '');
 		$this->setFont('helvetica', '', 11);
 		$this->writeTextBox($x + $col1Width + $col2Width + $col3Width, $line3Y, $col4Width, $lineHeight, $sex, 'L', '');

 		$this->setFont('helvetica', 'B', 11);
 		$this->writeTextBox($x, $line4Y, $col1Width, $lineHeight, 'Home PH:', 'R', '');
 		$this->setFont('helvetica', '', 11);
 		$this->writeTextBox($x + $col1Width, $line4Y, $col2Width, $lineHeight, $patient->getDisplayHphone(), 'L', '');

 		$this->setFont('helvetica', 'B', 11);
 		$this->writeTextBox($x + $col1Width + $col2Width, $line4Y, $col3Width, $lineHeight, 'Address:', 'R', '');
 		$this->setFont('helvetica', '', 11);
 		$this->writeTextBox($x + $col1Width + $col2Width + $col3Width, $line4Y, $col4Width, $lineHeight * 3, $address, 'L', '');

 		$this->setFont('helvetica', 'B', 11);
 		$this->writeTextBox($x, $line5Y, $col1Width, $lineHeight, 'Cell PH:', 'R', '');
 		$this->setFont('helvetica', '', 11);
 		$this->writeTextBox($x + $col1Width, $line5Y, $col2Width, $lineHeight, $patient->getDisplayCellphone(), 'L', '');

 		$this->setFont('helvetica', 'B', 11);
 		$this->writeTextBox($x, $line6Y, $col1Width, $lineHeight, 'Work PH:', 'R', '');
 		$this->setFont('helvetica', '', 11);
 		$this->writeTextBox($x + $col1Width, $line6Y, $col2Width, $lineHeight, $patient->getDisplayWphone(), 'L', '');

 		$this->setFont('helvetica', 'B', 11);
 		$this->writeTextBox($x, $line7Y, $col1Width, $lineHeight, 'Ref Doc:', 'R', '');
 		$this->setFont('helvetica', '', 11);
		$this->writeTextBox($x + $col1Width, $line7Y, $col2Width, $lineHeight, $refDoc, 'L', '');

		$this->setFont('helvetica', 'B', 11);
		$this->writeTextBox($x + $col1Width + $col2Width, $line7Y, $col3Width, $lineHeight, 'Fam Doc:', 'R', '');
		$this->setFont('helvetica', '', 11);
		$this->writeTextBox($x + $col1Width + $col2Width + $col3Width, $line7Y, $col4Width, $lineHeight, $famDoc, 'L', '');

		$this->setY($line7Y + $lineHeight);
	}
	private function buildPreviousDetails(Patient $patient, Appointment $appt)
	{
		$margins = $this->getMargins();

		$lastServiceDate = $patient->getLastServiceDate(hype::parseDateToInt($appt->getStartDate(), hype::DB_ISO_DATE), $this->date_format);
		$lastAppointmentDate = $patient->getLastAppointmentDate(hype::parseDateToInt($appt->getStartDate(), hype::DB_ISO_DATE), $this->date_format);

		$x = $margins['left'];
		$y = $this->getY();
		$pageWidth = $this->getPageWidth() - $margins['right'] - $margins['left'];

		$col1Width = $col3Width = intval($pageWidth / 2);
		$col2Width = $pageWidth - $col1Width;

		$lineHeight = $this->lineHeights['11'];
		$notesHeight = $this->getNumLines('Notes: ' . $appt->getNotes(), $pageWidth);

		$line2Y = $y + $lineHeight;
		$line3Y = $line2Y + $lineHeight;

		$this->setFont('helvetica', '', 11);
		$this->writeTextBox($x, $y, $col1Width, $lineHeight, 'Last Service(s): ' . $lastServiceDate, 'L', '');
		$this->writeTextBox($x, $line2Y, $col1Width, $lineHeight, 'Last Appointment: ' . $lastAppointmentDate, 'L', '');

		if ($this->includeNotes) {
			$this->writeTextBox($x, $line3Y, $pageWidth, $lineHeight * $notesHeight, 'Notes: ' . $appt->getNotes(), 'L', '');
		}
		else {
			$notesHeight = 0;
		}


		$this->setY($line3Y + ($lineHeight * $notesHeight));
	}
	private function buildAppointmentHeader($appointment)
	{
 		$margins = $this->getMargins();

 		$doctorName = 'DR ' . $appointment->Doctor->fname . ' ' . $appointment->Doctor->lname;
 		$dateAndTime = date($this->dateTimeFormat, strtotime($appointment->start_date));
 		$locationName = $appointment->Location->getName();
 		$locationAddress = $appointment->Location->getSmartStreetAddress("\n");

 		$x = $margins['left'];
 		$y = $this->getY();
 		$pageWidth = $this->getPageWidth() - $margins['right'] - $margins['left'];

 		$col1Width = intval($pageWidth / 2);
 		$col2Width = $pageWidth - $col1Width;

 		$doctorHeight = $this->lineHeights['14']; 		// 1 line max
 		$locationHeight = $this->lineHeights['11'];		// 1 line max
 		$addressHeight =  3 * $this->lineHeights['11'];	// 3 lines max
 		$dateHeight = $this->lineHeights['11'];			// 1 line max

 		$this->setFont('helvetica', 'B', 14);
 		$this->writeTextBox($x, $y, $col1Width, $doctorHeight, $doctorName, 'L', '');

 		$this->setFont('helvetica', '', 11);
 		$this->writeTextBox($x, $y + $doctorHeight, $col1Width, $dateHeight, $dateAndTime, 'L', '');
 		$this->writeTextBox($x, $y + $doctorHeight + $dateHeight, $col1Width, $dateHeight, $appointment->AppointmentType->getName(), 'L', '');

 		$this->setFont('helvetica', 'B', 11);
 		$this->writeTextBox($col1Width + $x, $y, $col2Width, $locationHeight, $locationName, 'R', '');

 		$this->setFont('helvetica', '', 11);
 		$this->writeTextBox($col1Width + $x, $y + $locationHeight, $col2Width, $addressHeight, $locationAddress, 'R', '');

 		$this->setX($margins['left']);
 		$this->setY($y + $locationHeight + $addressHeight);
	}

	private function writeTextBox ($x, $y, $width, $height, $text, $align, $border)
	{
		$this->StartTransform();
		$this->Rect($x, $y, $width, $height, 'CNZ', $border);
		$this->MultiCell($width, $height, $text, $border, $align, 0, 0, $x, $y);
		$this->StopTransform();
	}

 	public function Footer()
 	{
 		$this->SetY(-15);
 		if ($this->footerText) {
 			$this->Cell(.5, 10, $this->footerText, 0, false, 'L', 0, '', 0, false, 'T', 'M');
 		}
 		$this->Cell(0, 10, 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
 		$this->Cell(0, 10, date($this->dateTimeFormat), 0, false, 'R', 0, '', 0, false, 'T', 'M');

 	}
}