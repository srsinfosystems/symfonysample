<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class patientVisitSheetPDF extends TCPDF
{
    /*
     * @param Patient
     */
    protected $patient = null;
    protected $footer_text;

    protected $datetime_format;
    protected $date_format;

    public function __construct($pdf_options)
    {
        $this->footer_text = array_key_exists('footer_text', $pdf_options) ? $pdf_options['footer_text'] : '';
        $this->patient = array_key_exists('patient', $pdf_options) ? $pdf_options['patient'] : null;
        $this->datetime_format = $pdf_options['datetime_format'];
        $this->date_format = $pdf_options['date_format'];

        parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $this->SetCreator(PDF_CREATOR);
        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $this->SetAutoPageBreak(true, 0);
        $this->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $this->setPrintHeader(false);
        $this->setPrintFooter(true);
        $this->SetMargins(PDF_MARGIN_LEFT, 12, PDF_MARGIN_RIGHT);

        $this->SetFont('helvetica', '', 12, '', true);
        $this->AddPage();

        $this->line_width = $this->LineWidth;

        return $this;
    }

    public function generatePDF()
    {
        $this->SetFont('dejavusans', '', 12);
        $line_width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;

        if ($this->patient instanceof Patient) {

            $this->writeHTMLCell(100, 5, 5, 10, 'DOB: <b>' . hype::getFormattedDate($this->date_format, $this->patient->getDob(), hype::DB_ISO_DATE) . '</b> SEX: <b>' . $this->patient->getSex() . '</b>');
            $this->writeHTMLCell(100, 5, 5, 15, 'Last: <b>' . $this->patient->getLname() . '</b> First: <B>' . $this->patient->getFname() . '</B>');
            $this->writeHTMLCell(100, 5, 5, 20, 'Date: <b>' . hype::getFormattedDate($this->date_format, date('Y-m-d'), hype::DB_ISO_DATE) . '</b>');
            $this->writeHTMLCell(100, 5, 5, 25, 'HN: <b>' . $this->patient->getHCN() . '</b> VC: <b>' . $this->patient->getVersionCode() .
                '</b> EXP: <b>' . hype::getFormattedDate($this->date_format, $this->patient->getHcnExpYear(), hype::DB_ISO_DATE) . '</b>');
            $this->writeHTMLCell(100, 5, 5, 30, 'Status: <b>' . $this->patient->getHCNLastStatus() . '</b>');
            /*
            $this->MultiCell($line_width * .5, 0, 'DOB: ' . hype::getFormattedDate($this->date_format, $this->patient->getDob(), hype::DB_ISO_DATE) . ' SEX: ' . $this->patient->getSex(), '', 'L', 0, 1, '', '', true, 0);
            $this->MultiCell($line_width * .5, 0, 'Last: ' . $this->patient->getLname() . ' First: ' . $this->patient->getFname(), '', 'L', 0, 1, '', '', true, 0);
            $this->MultiCell($line_width * .5, 0, 'Date: ' . hype::getFormattedDate($this->date_format, date('Y-m-d'), hype::DB_ISO_DATE), '', 'L', 0, 1, '', '', true, 0);
            $this->MultiCell($line_width * .5, 0, 'HN: ' . $this->patient->getHCN() . ' VC: ' . $this->patient->getVersionCode() .
            ' EXP: ' . hype::getFormattedDate($this->date_format, $this->patient->getHcnExpYear(), hype::DB_ISO_DATE), '', 'L', 0, 1, '', '', true, 0);
            */


            $this->Ln();

        }
    }

    public function Footer()
    {
          $this->SetY(-15);
        $this->Cell(20, 10, 'Printed on:' .date($this->datetime_format), 0, false, 'L', 0, '', 0, false, 'T', 'M');
        if ($this->footer_text) {
        $this->Cell(150, 10, $this->footer_text,  0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
        $this->Cell(0, 10,  'Page ' . $this->getAliasNumPage() . ' of ' . $this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');

    }
}

