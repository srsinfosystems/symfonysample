<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class ThirdPartyInvoicePdf extends TCPDF
{
 	private $datetime_format = null;
 	private $date_format = null;

 	private $invoice = null;
 	private $sort = null;
 	private $line_height_factor = 4.5;

 	private $doctor_title = '__toString';

 	protected $orientation = 'P';
 	protected $footer_text;

 	protected $headers = array(
 		'claim_id' => 'Claim #',
 		'patient_doctor_etc' => ' ',
 		'fee_subm' => 'Fee',
 		'fee_paid' => 'Paid',
 		'fee_due' => 'Due',
 	);
 	protected $payment_headers = array(
 		'payment_date' => 'Date',
 		'payment_amount' => 'Amount',
 	);

 	protected $widths = array(
 		'claim_id' => 20,
 		'patient_doctor_etc' => 4,
 		'fee_subm' => 25,
 		'fee_paid' => 25,
 		'fee_due' => 25
 	);
 	protected $payment_widths = array(
 		'payment_date' => 30,
 		'payment_amount' => 30,
 	);

 	protected $aligns = array(
 		'claim_id' => 'L',
 		'patient_doctor_etc' => 'L',
 		'fee_subm' => 'R',
 		'fee_paid' => 'R',
 		'fee_due' => 'R'
 	);
 	protected $payment_aligns = array(
 		'payment_date' => 'L',
 		'payment_amount' => 'R',
 	);

 	public function __construct($invoice, $sort, $footer_text, $datetime_format, $date_format)
 	{
 		$this->invoice = $invoice;
 		$this->sort = $sort;
 		$this->footer_text = $footer_text;
 		$this->datetime_format = $datetime_format;
 		$this->date_format = $date_format;

 		parent::__construct($this->orientation, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

 		$this->SetCreator(PDF_CREATOR);
 		$this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
 		$this->SetAutoPageBreak(TRUE, PDF_MARGIN_TOP - 10);
 		$this->setImageScale(PDF_IMAGE_SCALE_RATIO);

 		$this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
 		$this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

 		$this->setPrintHeader(false);
 		$this->setPrintFooter(true);

 		$this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP - 10, PDF_MARGIN_RIGHT);
 		$this->setCellHeightRatio(1.25);
 		$this->AddPage();

 		$this->widths['patient_doctor_etc'] = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT - 20 - 25 - 25 - 25;

 		$this->addInvoiceHeader();
 		$this->addClaimsTable();

 		if (count($this->invoice->ThirdPartyInvoicePayment)) {
 			$this->addPaymentsTable();
 		}
  		return $this;
 	}
 	private function addClaimsTable()
 	{
 		$this->writeMainTableHeader();

 		switch ($this->sort) {
 			case 'date':
 				$sort = 'ci.service_date';
 				break;
 			case 'unpaid_first':
 				$sort = 'ci.fee_paid asc';
 				break;
 			case 'unpaid_last':
 				$sort = 'ci.fee_paid desc';
 				break;
 			case 'name':
 			default:
 				$sort = 'c.lname, c.fname';
 		}

 		$invoice_items = Doctrine_Query::create()
 			->from('ClaimItem ci')
 			->leftJoin('ci.Claim c')
 			->leftJoin('c.Doctor d')
 			->addWhere('c.client_id = (?)', $this->invoice->getClientId())
 			->addWhere('c.invoice_num LIKE (?)', '%' . $this->invoice->getId() . ';%')
 			->addWhere('ci.status != (?)', ClaimItemTable::STATUS_DELETED)
 			->orderBy($sort)
 			->execute();

 		$total_subm = 0;
 		$total_paid = 0;

 		foreach ($invoice_items as $item)
 		{
 			$this->writeMainTableRow($item);

 			$total_subm += $item->fee_subm;
 			$total_paid += $item->fee_paid;
 		}

 		$total_due = $total_subm - $total_paid;

 		$this->writeMainTableTotals($total_subm, $total_paid, $total_due);
 	}
 	private function addInvoiceHeader()
 	{
 		$title = 'Invoice';
 		$left_data = 'Invoice Number: ' . $this->invoice->getInvoiceNumber() . "\n"
 		. 'Invoice Date: ' . date($this->date_format, hype::parseDateToInt($this->invoice->created_at, hype::DB_ISO_DATE));
 		$payee_address = strtoupper($this->invoice->ThirdPartyPayee->getStreetAddress());
 		$third_party_address = strtoupper($this->invoice->ThirdParty->getStreetAddress());

 		while (substr_count($payee_address, "\n") < substr_count($third_party_address, "\n"))
 		{
 			$payee_address .= "\n";
 		}
 		while (substr_count($payee_address, "\n") > substr_count($third_party_address, "\n"))
 		{
 			$third_party_address .= "\n";
 		}

 		
 		// $html = '<img src="clipone/hype-logo.png" style="margin-right:-150px;" width="50">';
 		// $this->writeHTML($html, true, false, true, false, '');
 		$this->Image('clipone/hype-logo.png','2', '5', 50, 0, '', '', 'T', false, 300, 'R', false, false, 1, false, false, false);
 		$this->SetFont('dejavusans', '', 18);
 		//$this->writeHTML('<p>Invoice</p>', true, false, true, false, '');
 		$this->MultiCell(150, 1, '', '110', 'C', 0, 1, '', '', true, 0);


 		//$this->Image('clipone/hype-logo.png',2, 5, 40, 0);
 		//$this->Image('clipone/hype-logo.png','2', '5', 30, 0, '', '', 'T', false, 300, 'R', false, false, 1, false, false, false);

 		$this->SetFont('dejavusans', '', 18);
 		$this->MultiCell(150, 1, $title, '0', 'C', 0, 1, '', '', true, 0);

 		$this->SetFont('dejavusans', '', 8, '');
 		$this->MultiCell(0, 0, $left_data, '0', 'L', 0, 1, '', '', true, 0);

 		$this->SetFont('dejavusans', 'B', 10, '');
 		$this->MultiCell(0, 0, 'Remit Payment to:', '0', 'R', 0, 1, '', '', true, 0);

 		$this->SetFont('dejavusans', '', 8, '');
 		$this->MultiCell(50, 0, $third_party_address, '0', 'L', 0, 0, '', '', true, 0);
 		$this->MultiCell(0, 0, $payee_address, '0', 'R', 0, 0, '', '', true, 0);

 		$this->Ln();
 		$this->SetFont('dejavusans', '', 5, '');
 		$this->MultiCell(0, 0, ' ', '', 'L', 0, 0);
 		$this->Ln();
 	}
 	private function addNewReportPage()
 	{
 		$this->addPage();
 		$this->writeMainTableHeader();
 	}
 	private function addPaymentsTable()
 	{
 		$this->writePaymentsTableHeader();

 		foreach ($this->invoice->ThirdPartyInvoicePayment as $payment)
 		{
 			$this->writePaymentTableRow($payment);
 		}
 		$this->writePaymentTableRow();
 	}
 	private function writeMainTableHeader()
 	{
 		$this->Ln();
 		$this->SetFont('dejavusans', '', 10);

 		foreach ($this->headers as $key => $title)
 		{
 			$text = $title;
 			$this->MultiCell($this->widths[$key], $this->getNumLines($text, $this->widths[$key]) * $this->line_height_factor, $text, 'T', $this->aligns[$key], 0, 0);
 		}
 		$this->Ln();
 	}
 	private function writeMainTableRow($item)
 	{
 		$dimensions = $this->getPageDimensions();
 		$startY = $this->getY();

 		$data = array(
 			'claim_id' => $item->Claim->id . '-' . $item->item_id,
 			'patient_doctor_etc' => $item->Claim->fname . ' ' . $item->Claim->lname . "\n"
 				. 'DOB: ' . date($this->date_format, hype::parseDateToInt($item->Claim->dob, hype::DB_ISO_DATE)) . "\n"
 				. 'Doctor: ' . $item->Claim->Doctor->fname . ' ' . $item->Claim->Doctor->lname . "\n"
 				. 'Date of Service: ' . date($this->date_format, hype::parseDateToInt($item->service_date, hype::DB_ISO_DATE)) . "\n"
 				. 'Service: ' . $item->service_code,
 			'fee_subm' => '$' . number_format($item->fee_subm, 2, '.', ','),
 			'fee_paid' => '$' . number_format($item->fee_paid, 2, '.', ','),
 			'fee_due' => '$' . number_format($item->fee_subm - $item->fee_paid, 2, '.', ','),
 		);

 		$this->SetFont('dejavusans', '', 10);
 		$max_lines = 0;

 		foreach ($this->headers as $key => $text)
 		{
 			$max_lines = max($max_lines, $this->getNumLines($data[$key], $this->widths[$key]));
 		}

 		if (($startY + $max_lines * $this->line_height_factor) + $dimensions['bm'] > ($dimensions['hk'])) {
 			$this->addNewReportPage();
 		}

 		foreach ($this->headers as $key => $text)
 		{
 			$this->MultiCell($this->widths[$key], $max_lines * $this->line_height_factor, $data[$key], 'T', $this->aligns[$key], 0, 0);
 		}

 		$this->Ln();
 		$this->SetFont('dejavusans', '', 5, '');
 		$this->MultiCell(0, 0, ' ', '', 'L', 0, 0);
 		$this->Ln();
 	}
 	private function writeMainTableTotals($subm, $paid, $due)
 	{
 		$this->SetFont('dejavusans', 'B', 10);
 		$max_lines = 1;

 		$data = array(
 				'claim_id' => 'Totals: ',
 				'patient_doctor_etc' => ' ',
 				'fee_subm' => '$' . number_format($subm, 2, '.', ','),
 				'fee_paid' => '$' . number_format($paid, 2, '.', ','),
 				'fee_due' => '$' . number_format($due, 2, '.', ','),
 		);

 		foreach ($data as $key => $text)
 		{
 			$this->MultiCell($this->widths[$key], $max_lines * $this->line_height_factor, $data[$key], 'TB', $this->aligns[$key], 0, 0);
 		}

 		$this->Ln();
 		$this->SetFont('dejavusans', '', 12, '');
 		$this->MultiCell(0, 0, ' ', '', '', 0, 0);
 		$this->Ln();

 	}
 	private function writePaymentsTableHeader()
 	{
 		$this->Ln();
 		$this->SetFont('dejavusans', 'B', 10);
 		$this->MultiCell(0, 0, 'Payments', '', 'L', 0, 0);
 		$this->Ln();

 		$this->SetFont('dejavusans', '', 10);

 		foreach ($this->payment_headers as $key => $title)
 		{
 			$text = $title;
 			$this->MultiCell($this->payment_widths[$key], $this->getNumLines($text, $this->payment_widths[$key]) * $this->line_height_factor, $text, 'T', $this->payment_aligns[$key], 0, 0);
 		}
 		$this->Ln();
 	}
 	private function writePaymentTableRow($payment = array())
 	{
 		$dimensions = $this->getPageDimensions();
 		$startY = $this->getY();

 		if ($payment instanceof ThirdPartyInvoicePayment) {
 			$data = array(
 				'payment_date' => date($this->date_format, hype::parseDateToInt($payment->payment_date, hype::DB_ISO_DATE)),
 				'payment_amount' => '$' . number_format($payment->payment_amount, 2, '.', ','),
 			);
 		}
 		else {
 			$data = array(
 				'payment_date' => ' ',
 				'payment_amount' => ' ',
 			);
 		} 

 		$this->SetFont('dejavusans', '', 10);
 		$max_lines = 0;

 		foreach ($this->payment_headers as $key => $text)
 		{
 			$max_lines = max($max_lines, $this->getNumLines($data[$key], $this->payment_widths[$key]));
 		}

 		if (($startY + $max_lines * $this->line_height_factor) + $dimensions['bm'] > ($dimensions['hk'])) {
 			$this->addNewReportPage();
 		}

 		foreach ($this->payment_headers as $key => $text)
 		{
 			$this->MultiCell($this->payment_widths[$key], $max_lines * $this->line_height_factor, $data[$key], 'T', $this->payment_aligns[$key], 0, 0);
 		}

 		$this->Ln();
 		$this->SetFont('dejavusans', '', 5, '');
 		$this->MultiCell(0, 0, ' ', '', 'L', 0, 0);
 		$this->Ln();
 	}

 	public function Footer() {
        $this->SetY(-15);
        $this->Cell(20, 10, 'Printed on:' .date($this->datetime_format), 0, false, 'L', 0, '', 0, false, 'T', 'M');
        if ($this->footer_text) {
        $this->Cell(150, 10, $this->footer_text,  0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
        $this->Cell(0, 10,  'Page ' . $this->getAliasNumPage() . ' of ' . $this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }
}
