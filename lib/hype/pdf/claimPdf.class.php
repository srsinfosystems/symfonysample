<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class claimPdf extends TCPDF {

    private $claim = null;
    private $claim_total = 0;
    private $amount_paid = 0;
    private $line_height_factor = 4.5;
    private $doctor_title = '__toString';
    private $item_columns = array(.15, .30, .15, .15, .10, .15);
    private $footer_columns = array(.55, .30, .15);
    protected $date_format;
    protected $datetime_format;
    protected $orientation = 'P';
    protected $footer_text;

    public function __construct(Claim $claim, $pdf_type, $footer_text, $date_format, $datetime_format) {
        $this->date_format = $date_format;
        $this->datetime_format = $datetime_format;
        $this->footer_text = $footer_text;
        parent::__construct($this->orientation, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $this->SetCreator(PDF_CREATOR);
        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $this->SetAutoPageBreak(TRUE, 40);
        $this->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $this->setPrintHeader(false);
        $this->setPrintFooter(true);


        $this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP - 20, PDF_MARGIN_RIGHT);
        $this->setCellHeightRatio(1);
        $this->AddPage();


        $this->claim = $claim;

        $this->doctor_title = ClientParamTable::getDefaultForValue('doctor_title_format');

        $cp = ClientParamTable::getParamForClient($this->claim->client_id, 'doctor_title_format');
        if ($cp instanceof ClientParam) {
            $this->doctor_title = $cp->value;
        }

        if ($pdf_type == 'details') {
            $this->addClaimHeading();
            $this->Ln();
            $this->addClaimAddress();
            $this->Ln();
            $this->addClaimItemTable();

            if (count($this->claim->ClaimDirectPayment)) {
                $this->addPaymentTable();
            }
            $this->Ln();
            $this->Ln();
            $this->addErrorMessage();
        } else if ($pdf_type == 'receipt') {
            $this->addInvoiceHeading('Receipt');
            $this->Ln();
            $this->addClaimItemTable();
            $this->Ln();
            $this->addPaymentTable();
        } else if ($pdf_type == 'invoice') {
            $this->addInvoiceHeading('Invoice');
            $this->Ln();
            $this->addClaimItemTable();
            $this->Ln();
            $this->addPaymentTable();
        }

        return $this;
    }

    private function addPaymentTable() {
        $this->SetTopMargin($this->GetY());

        $line_width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;

        $this->SetFont('Helvetica', 'B', 10, '');
        $this->Ln();


        $this->MultiCell(0, 0, 'Payment History', '0', 'L', 0, 1, '', '', true, 0);

        $this->SetFont('Helvetica', 'B', 8, '');
        $this->MultiCell($line_width * $this->item_columns[0], 0, ' ', 0, 'L', 0, 0);
        $this->MultiCell($line_width * $this->item_columns[1], 0, 'Payment Method', 0, 'L', 0, 0);
        $this->MultiCell($line_width * $this->item_columns[2], 0, 'Date', 0, 'L', 0, 0);
        $this->MultiCell($line_width * $this->item_columns[3], 0, ' ', 0, 'L', 0, 0);
        $this->MultiCell($line_width * $this->item_columns[4], 0, 'Amount', 0, 'R', 0, 0);
        $this->Ln();

        $this->SetFont('Helvetica', '', 8, '');
        foreach ($this->claim->ClaimDirectPayment as $payment) {
            $this->MultiCell($line_width * $this->item_columns[0], 0, ' ', 0, 'L', 0, 0);
            $this->MultiCell($line_width * $this->item_columns[1], 0, $payment->payment_method, 0, 'L', 0, 0);
            $this->MultiCell($line_width * $this->item_columns[2], 0, date($this->date_format, hype::parseDateToInt($payment->payment_date, hype::DB_ISO_DATE)), 0, 'L', 0, 0);
            $this->MultiCell($line_width * $this->item_columns[3], 0, ' ', 0, 'L', 0, 0);
            $this->MultiCell($line_width * $this->item_columns[4], 0, '$' . number_format($payment->total_paid, 2), 0, 'R', 0, 0);
            $this->Ln();

            $this->amount_paid += $payment->total_paid;
        }

        if ($this->claim->pay_prog == ClaimTable::CLAIM_TYPE_THIRD_PARTY) {
            foreach ($this->claim->ClaimItem as $claim_item) {
                $this->amount_paid += $claim_item->fee_paid;
            }
        }

        $this->SetFont('Helvetica', 'B', 8, '');
        $this->MultiCell($line_width * $this->footer_columns[0], 0, ' ', 'T', 'L', 0, 0);
        $this->MultiCell($line_width * $this->footer_columns[1], 0, 'Total Paid:', 'T', 'L', 0, 0);
        $this->MultiCell($line_width * $this->footer_columns[2], 0, '$' . number_format($this->amount_paid, 2), 'T', 'R', 0, 0);
        $this->Ln();

        $this->SetFont('Helvetica', 'B', 10, '');
        if ($this->claim->pay_prog == ClaimTable::CLAIM_TYPE_DIRECT || $this->claim->pay_prog == ClaimTable::CLAIM_TYPE_THIRD_PARTY) {
            // amount outstanding
            $this->MultiCell($line_width * $this->footer_columns[0], 0, ' ', 0, 'L', 0, 0);
            $this->MultiCell($line_width * $this->footer_columns[1], 0, 'Outstanding Total:', 0, 'L', 0, 0);
            $this->MultiCell($line_width * $this->footer_columns[2], 0, '$' . number_format(($this->claim_total + $this->claim->tax_amount) - $this->amount_paid, 2), 0, 'R', 0, 0);
        }
    }

    private function addClaimItemTable() {
        // create some HTML content

        $line_width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;

        $this->SetFont('Helvetica', 'B', 12, '');
        $this->Ln();
        $this->MultiCell(0, 0, 'Services', '0', 'L', 0, 1, '', '', true, 0);
        $this->Ln();

        $html = '
        <table>
            <tr style="font-weight: bold;">
                <th align="left">Service Code</th>
                <th align="left" width="20%">Description</th>
                <th align="left">MOH Claim #</th>
                <th align="left">Service Date</th>
                <th align="left"># of Services</th>
                <th align="left">Amount</th>
            </tr><br>';

        $this->SetFont('dejavusans', '', 10, '');
        foreach ($this->claim->ClaimItem as $claim_item) {   
        $desc = $claim_item->serviceCodeDescription() . ' - ' . $claim_item->getDiagCode();  

         if (strlen($desc) == 0) {
            $lines = 1;
        } else {
            $lines = $this->getNumLines($desc, $line_width * $this->item_columns[1]);
        }

        $html .= '<tr>
                   <td  align="left">'.$claim_item->service_code.'</td>';
                    if ($claim_item->discounted) {
                    $discount = $claim_item->getDiscount();
                    if ($discount) {
        $html .=   '<td  align="left">'.$discount->description . ' (original fee: $' . number_format($claim_item->original_fee, 2, '.', ',') . ')'       .'</td>';
                    } } else {
                    $html .=  '<td  align="left">'.$desc.'</td>';  
                    }
        $html .=  '<td  align="left">'.$claim_item->moh_claim.'</td>
                  <td  align="left">'.date($this->date_format, hype::parseDateToInt($claim_item->service_date, hype::DB_ISO_DATE)).'</td>
                  <td  align="left">'.$claim_item->num_serv.'</td>
                 <td  align="left">'.'$' . number_format($claim_item->fee_subm, 2).'</td>
                  </tr><br>';
         if ($claim_item->discounted) {
                $discount = $claim_item->getDiscount();

                if ($discount) {
                    $this->MultiCell($line_width * $this->item_columns[0], $lines * $this->line_height_factor, '', 0, 'L', 0, 0);
                    $this->MultiCell($line_width * $this->item_columns[1], $lines * $this->line_height_factor, $discount->description . ' (original fee: $' . number_format($claim_item->original_fee, 2, '.', ',') . ')', 0, 'L', 0, 0);
                    $this->Ln();
                }
            }
        $this->claim_total += $claim_item->fee_subm; 
        }
        if ($this->claim->pay_prog == ClaimTable::CLAIM_TYPE_DIRECT && $this->claim->tax_amount) {
            $taxes = explode(';', $this->claim->taxable_text);
            foreach ($taxes as $key => $value) {
                if (trim($value)) {
                    $taxes[] = explode(':', $value);
                }
                unset($taxes[$key]);
            }

            $line = 'T';
            foreach ($taxes as $tax) {

             $html .= '
                 <tr>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td>'.trim($tax[0]).'</td>
                 <td>'.'$' . number_format(floatval(trim($tax[1])), 2).'</td>
                 </tr>';
                // $this->MultiCell($line_width * $this->footer_columns[0], 0, ' ', $line, 'L', 0, 0);
                // $this->MultiCell($line_width * $this->footer_columns[1], 0, trim($tax[0]) . ':', $line, 'R', 0, 0);
                // $this->MultiCell($line_width * $this->footer_columns[2], 0, '$' . number_format(floatval(trim($tax[1])), 2), $line, 'R', 0, 0);
                // $this->Ln();
                // $line = '';
            }
           }

        $html .= '
                 <tr>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td style="font-weight:bold;">Total</td>
                 <td style="font-weight:bold;">'.'$' . number_format($this->claim_total + $this->claim->tax_amount, 2).'</td>
                 </tr>';

        $html .= '</table>';
        
        // $this->SetFont('dejavusans', 'B', 10, '');

        // echo $html;
        // output the HTML content
        $this->writeHTML($html, true, false, true, false, '');
        //exit;
        // Print some HTML Cells
        // reset pointer to the last page
        $this->lastPage();



        // $line_width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;

        // $this->SetFont('Helvetica', 'B', 12, '');
        // $this->Ln();
        // $this->MultiCell(0, 0, 'Services', '0', 'L', 0, 1, '', '', true, 0);
        // $this->Ln();
        // $this->SetFont('Helvetica', 'B', 10, '');
        // $this->MultiCell($line_width * $this->item_columns[0], 0, 'Service Code', 0, 'L', 0, 0);
        // $this->MultiCell($line_width * $this->item_columns[1], 0, 'Description', 0, 'L', 0, 0);
        // $this->MultiCell($line_width * $this->item_columns[5], 0, 'MOH Claim #', 0, 'L', 0, 0);
        // $this->MultiCell($line_width * $this->item_columns[2], 0, 'Service Date', 0, 'L', 0, 0);
        // $this->MultiCell($line_width * $this->item_columns[3], 0, '# of Services', 0, 'L', 0, 0);
        // $this->MultiCell($line_width * $this->item_columns[4], 0, 'Amount', 0, 'R', 0, 0);
        // $this->Ln();
        // $this->Ln();
        // $this->Ln();

        // $this->SetFont('dejavusans', '', 10, '');
        // $i = 1;
        // foreach ($this->claim->ClaimItem as $claim_item) {

        //     $desc = 'Family Practice & Practice i Family Practice & Practice test';

        //     //$claim_item->serviceCodeDescription() . ' - ' . $claim_item->getDiagCode();

        //     if (strlen($desc) == 0) {
        //         $lines = 1;
        //     } else {
        //         $lines = $this->getNumLines($desc, $line_width * $this->item_columns[1]);
        //     }
        //     $this->MultiCell($line_width * $this->item_columns[0], $lines * $this->line_height_factor, $claim_item->service_code, 0, 'L', 0, 0);
        //     $this->MultiCell($line_width * $this->item_columns[1], $lines * $this->line_height_factor, $desc, 0, 'L', 0, 0);
        //     $this->MultiCell($line_width * $this->item_columns[5], $lines * $this->line_height_factor, $claim_item->moh_claim, 0, 'L', 0, 0);
        //     $this->MultiCell($line_width * $this->item_columns[2], $lines * $this->line_height_factor, date($this->date_format, hype::parseDateToInt($claim_item->service_date, hype::DB_ISO_DATE)), 0, 'L', 0, 0);
        //     $this->MultiCell($line_width * $this->item_columns[3], $lines * $this->line_height_factor, $claim_item->num_serv, 0, 'L', 0, 0);
        //     $this->MultiCell($line_width * $this->item_columns[4], $lines * $this->line_height_factor, '$' . number_format($claim_item->fee_subm, 2), 0, 'R', 0, 0);
        //     $this->Ln();

        //     if ($claim_item->discounted) {
        //         $discount = $claim_item->getDiscount();

        //         if ($discount) {
        //             $this->MultiCell($line_width * $this->item_columns[0], $lines * $this->line_height_factor, '', 0, 'L', 0, 0);
        //             $this->MultiCell($line_width * $this->item_columns[1], $lines * $this->line_height_factor, $discount->description . ' (original fee: $' . number_format($claim_item->original_fee, 2, '.', ',') . ')', 0, 'L', 0, 0);
        //             $this->Ln();
        //         }
        //     }

        //     $this->claim_total += $claim_item->fee_subm;
        // }

        // if ($this->claim->pay_prog == ClaimTable::CLAIM_TYPE_DIRECT && $this->claim->tax_amount) {
        //     $taxes = explode(';', $this->claim->taxable_text);
        //     foreach ($taxes as $key => $value) {
        //         if (trim($value)) {
        //             $taxes[] = explode(':', $value);
        //         }
        //         unset($taxes[$key]);
        //     }

        //     $line = 'T';
        //     foreach ($taxes as $tax) {
        //         $this->MultiCell($line_width * $this->footer_columns[0], 0, ' ', $line, 'L', 0, 0);
        //         $this->MultiCell($line_width * $this->footer_columns[1], 0, trim($tax[0]) . ':', $line, 'R', 0, 0);
        //         $this->MultiCell($line_width * $this->footer_columns[2], 0, '$' . number_format(floatval(trim($tax[1])), 2), $line, 'R', 0, 0);
        //         $this->Ln();
        //         $line = '';
        //     }
        // }

        // $this->SetFont('dejavusans', 'B', 10, '');
        // $this->MultiCell($line_width * $this->footer_columns[0], 0, ' ', 'T', 'L', 0, 0);
        // $this->MultiCell($line_width * $this->footer_columns[1], 0, 'Total:', 'T', 'R', 0, 0);
        // $this->MultiCell($line_width * $this->footer_columns[2], 0, '$' . number_format($this->claim_total + $this->claim->tax_amount, 2), 'T', 'R', 0, 0);
        // $this->Ln();
    }

    private function addClaimAddress() {
        $line_width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;
        $half = $line_width * .5;
        $quarter = ($line_width - $half) / 2;
        $remainder = $line_width - $quarter - $half;

        $bill_to = 'Patient:';
        $patient_name = $this->claim->Patient->getNameString();
        $address1 = $this->claim->Patient->address;
        $address2 = $this->claim->Patient->city . ', ' . $this->claim->Patient->province . ' ' . $this->claim->Patient->postal_code . ' ';
        $acct_no = $this->claim->acct_num;
        $bill_date = date($this->date_format, hype::parseDateToInt($this->claim->date_created, hype::DB_ISO_DATE));
//      $moh_claim_id = $this->claim->moh_claim . ' ';
        $hcn_str = hype::decryptHCN($this->claim->health_num) . ' ' . $this->claim->version_code;
        $bill_doc = $this->claim->doctor_id ? $this->claim->Doctor->getCustomToString($this->doctor_title) : '';

        $this->SetFont('Helvetica', '', 12, '');
        $this->MultiCell(0, 0, $bill_to, '0', 'L', 0, 1, '', '', true, 0);

        $this->SetFont('Helvetica', 'B', 10, '');
        $this->MultiCell($half, 0, $patient_name, 0, 'L', 0, 0, PDF_MARGIN_LEFT);

        $this->SetFont('Helvetica', '', 10, '');
        $this->MultiCell($quarter, 0, 'Account #:', 0, 'R', 0, 0);
        $this->MultiCell($remainder, 0, $acct_no, 0, 'L', 0, 1);

        $this->MultiCell($half, 0, $address1, 0, 'L', 0, 0, PDF_MARGIN_LEFT);
        $this->MultiCell($quarter, 0, 'Date:', 0, 'R', 0, 0);
        $this->MultiCell($remainder, 0, $bill_date, 0, 'L', 0, 1);

        $this->MultiCell($half, 0, $address2, 0, 'L', 0, 0, PDF_MARGIN_LEFT);
        $this->MultiCell($quarter, 0, 'Health Card #: ', 0, 'R', 0, 0);
        $this->MultiCell($remainder, 0, $hcn_str, 0, 'L', 0, 1);

        $this->MultiCell($half, 0, ' ', 0, 'L', 0, 0, PDF_MARGIN_LEFT);
        $this->MultiCell($quarter, 0, 'Billing Doctor:', 0, 'R', 0, 0);
        $this->MultiCell(($remainder+20), 0, $bill_doc, 0, 'L', 0, 1);

        $this->MultiCell($half, 0, ' ', 0, 'L', 0, 0, PDF_MARGIN_LEFT);
    }

    private function addInvoiceHeading($title) {
        $this->SetFont('dejavusans', '', 20);
        $this->MultiCell(0, 0, $title, '0', 'C', 0, 1, '', '', true, 0);
        $this->Ln();

        $patient_address = $this->claim->Patient->getNameString() . "\n"
                . $this->claim->Patient->address . "\n"
                . $this->claim->Patient->city . ', ' . $this->claim->Patient->province . ' ' . $this->claim->Patient->postal_code;

//      $pdf_payee = 'doctor';
        $pdf_payee_id = null;
        $payee_address = '';

        $pdf_payee = ClientParamTable::getParamOrDefaultValueForClient($this->claim->client_id, 'claim_pdf_payee');

        if ($pdf_payee == 'payee') {
            $cp = ClientParamTable::getParamForClient($this->claim->client_id, 'claim_pdf_payee_id');
            if ($cp instanceof ClientParam) {
                $pdf_payee_id = $cp->value;
            }
        }

        if ($pdf_payee_id) {
            $payee = Doctrine_Query::create()
                    ->from('ThirdPartyPayee p')
                    ->addWhere('p.id = (?)', array($pdf_payee_id))
                    ->addWhere('p.client_id = (?)', array($this->claim->client_id))
                    ->fetchOne();

            if ($payee instanceof ThirdPartyPayee) {
                $payee_address = $payee->getStreetAddress();
            }
        }

        if (!$payee_address) {
            if ($this->claim->doctor_id) {
                $payee_address = $this->claim->Doctor->getAddressText();
            } else {
                $location = $this->claim->location_id ? $this->claim->Location : null;
                if ($location instanceof Location) {
                    $payee_address = $location->getName() . "\n"
                            . $location->address1 . "\n"
                            . ($location->address2 ? $location->address2 . "\n" : '')
                            . $location->city . ', ' . $location->prov_code . ', ' . $location->postal_code . "\n";
                }
            }
        }

        while (substr_count($patient_address, "\n") < substr_count($payee_address, "\n")) {
            $patient_address .= "\n";
        }
        while (substr_count($patient_address, "\n") > substr_count($payee_address, "\n")) {
            $payee_address .= "\n";
        }

        $this->SetFont('dejavusans', '', 10, '');
        if ($this->claim->fully_paid) {
            $payment_address = 'Payee';
        } else {
            $payment_address = 'Remit Payment To';
        }

        $line_width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;
        $half = $line_width * .5;
        $this->MultiCell($half, 0, $payment_address, '0', 'L', 0, 0, '', '', true, 0);
        $this->MultiCell($half, 0, 'Patient', '0', 'R', 0, 1, '', '', true, 0);

        $this->MultiCell(50, 0, $payee_address, '0', 'L', 0, 0, '', '', true, 0);
        $this->MultiCell(0, 0, $patient_address, '0', 'R', 0, 1, '', '', true, 0);
        $this->Ln();


        //2nd row




        if ($this->claim->pay_prog == ClaimTable::CLAIM_TYPE_THIRD_PARTY) {
            $thirdParty = $this->claim->ThirdParty->getName();
            $thirdPartyAddr = $this->claim->ThirdParty->getAddress();
            $thirdPartyRest = $this->claim->ThirdParty->getCity() . ' ' . $this->claim->ThirdParty->getPostalCode() . ' ' . $this->claim->ThirdParty->getProvince() . ', ' . $this->claim->ThirdParty->getCountry();
            $policyNum = $this->claim->policy_number;
            $contact = $this->claim->ThirdParty->getContact();

            $this->SetFont('dejavusans', '', 12, '');
            $this->MultiCell(0, 0, $contact, 0, 'L', 0, 1);
            $this->MultiCell(0, 0, $thirdParty, 0, 'L', 0, 1);
            $this->MultiCell(0, 0, $thirdPartyAddr, 0, 'L', 0, 1);
            $this->MultiCell(0, 0, $thirdPartyRest, 0, 'L', 0, 1);
            $this->MultiCell(0, 0, 'Policy Number:  ' . $policyNum, 0, 'L', 0, 1);
        } else if ($this->claim->pay_prog == ClaimTable::CLAIM_TYPE_DIRECT) {
            
        }

        $acct_no = $this->claim->acct_num;
        $bill_date = date($this->date_format, hype::parseDateToInt($this->claim->date_created, hype::DB_ISO_DATE));

        $this->SetFont('dejavusans', '', 12, '');
        if ($this->claim->getHCN()) {
            $this->MultiCell(0, 0, 'Patient HC: ' . $this->claim->getHCN() . ' ' . $this->claim->getVersionCode(), 0, 'L', 0, 1);
        }
        $this->MultiCell(0, 0, 'Account #:  ' . $acct_no, 0, 'L', 0, 1);
        $this->MultiCell(0, 0, 'Date:  ' . $bill_date, 0, 'L', 0, 1);
        if ($this->claim->ReferringDoctor) {
            $this->MultiCell(0, 0, 'Referred By:  ' . $this->claim->getRefDocDescription(), 0, 'L', 0, 1);
        }
        // patient Health Card #, VC
        // referring doctor informaiton

        $this->Ln();
        $this->SetFont('dejavusans', '', 5, '');
        $this->MultiCell(0, 0, ' ', '', 'L', 0, 0);
        $this->Ln();
    }

    private function addClaimHeading() {
        $title = 'Claim Details';

        $this->SetFont('Helvetica', '', 20);
        $this->MultiCell(0, 0, $title, '0', 'L', 0, 1, '', '', true, 0);
        $this->Image('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA5UAAACnCAYAAACB8njoAAAACXBIWXMAAC4jAAAuIwF4pT92AAAgAElEQVR42u3dfXBc5WHv8d/qzW/glXmLcQ0SJgQuwW9JeAkkkTxpCbSlkrNF3dtp481NMjfT2zuIy72dO8kMrP9oJ5O2gzxJKZ00ZU0y7bLpXuRLU0guvV6FJIVAYslOiCHBlsAYA8bW4mvZlmXv/WMfCVmWdvc5e3b3vHw/MxqwvXu055znnH1+53mLFAoFAQAAAADgRBOHAAAAAABAqAQAAAAAECoBAAAAAIRKAAAAAAChEgAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAAIRKAAAAAAChEgAAAABAqAQAAAAAECoBAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAABAqAQAAAAA1FFLWHc8ncl2S+o0P+2SNph/ape0foG35SUNz/pzbtZ/R+N9sVGKFAAAAIAwiRQKhTAEyA2Sus3PBkkdNfx1QyZ45iQNEzQBAAAAECr9FyI7TYDsNf+NNvDjjJiAORjvi+UocgAAAAAIld4Mku0mRPZr4e6rjZaXNCgpRcAEAAAAQKj0RpjslpSQtMVnH31M0oAJmOMURQAAAACEyvqGyYSkpGo7PrJetktKMv4SAAAAAKGSMEm4BAAAAECoDFOYbD52WC3Hj0iSml57peRrC8tXqBC9SJJ0auUHCJcAAAAACJUeD5MbVBx72OXG9hYdellNr72i5jffUNORvJpfeqeq7Z2Ntursmkt1duXlOnPp5Tq96v06c+ElbnzUvNnvAcZcAgAAACBU2ofJdhVbJu+pZjutRw+o5dd71DK2Xy27Dtbls59ZvUxnrunQmSvW6NRV61RoW1rN5sYkJZgtFgAAAAChsvJA2S0pJYddXVuPHlDbL55X8569aj5wvOH7c/qjazR1zfXVBsxtKnaJpdUSAAAAAKGyRKAckMPWycUvPau25/696i6ttXI22qqpD12jyQ9/QqdXrHayiTFJvfG+2DBFFwAAAACh8tww2SlpUNJ6qx2YnNDikSG17fyJmvKnfXPgpzau0uTHftPpZD/3xvtiAxRfAAAAAIRKzXR3HZQUDXqYdDFc7lBxrCXdYQEAAACEN1SapUIesXnPkt05tT015OswOV+4nPhUn+3MsSOSugmWAAAAAEIZKtOZbErSlkpfv+jQy1r0/e96dsykG0723qaT67tsJvTJm2DJOEsAAAAA4QmVNoEyMjmhpT/5ntqe/GkoTsqZ1ct0anPMpksswRIAAABAQzR5PVC2Hj2gC9N/F5pAKUnNB45r6dce1bIfPl7pW6KSculMdgNFGgAa8r3WHtD96uTsAgA8FyptAuWS3Tld8FcPB7q7ayltT/5Uyx99UM3HDhMsAcC7watbUiKggTLBGQbg8XtVO0chZKHSJlAue/oftfixp0N/gppfekcX/O3faNGhlwmWAOBNGyT1B3C/kmbfAMCrgbJXEsvshSlUVhooI5MTWv7og2rb+SJnZ/ok5U9r6dce1eKXniVYAoA3Q2WHmc08KBW1TvOdzXcJAC/rl7SF1sqQhErzRVs2UE6Pnwxrd9dyljz6L1ry/JOVBstBLjAAqFuolILVVXS65bWD0wvAi8zQg6459yw0SEsdTnivKliHsvXoAS39xjcDtfZkLSwe/JGa8kd1/Df/sNxLOyTlxFNmAKi19ea/XelMtjveF8v5vKLWPjsgB2GfAB9ehxskdZp6XHuF9blhSeOm/jca74uNBvwwJWf9f/+cP6POarqkiLkgciq2nBEoXTS56fpKgqUkbY/3xRK+L6iRSKe5uVZqtFAojDb4M1f6JVDxZ45EItNfLraGC4XCuEfOpaN9KBQKOZePty/KkQvnvhE8c9zqUPHrlrQzSPfcdCablPTArL/6bLwvlmpQpdovZX443hcbd7iftt9vVoL0QMBBmXB8Xhq0b72SuvVe61u1xkw9PCdp0C/HwuK62T/nrxtyr6piH2zrLeNeXj6wpcYHKuV2oIzeuka3bP+u1Wd5dsvvKP/jfXU7qFMbV+nY7/+J1Xsu/OeH1LLrYMWvL445/cdKguWWdCab89NFtoDEnEpOOVvV+CdWG+ZUNt34zJ2SHnfwWbxwPKaD0S4Hb91hvhTdPN6Vfubp/x3RrCfAknJ1Dk4DLlY0as0T5a2O1/nce27S5y0EiTL7SJk/36YK7lFufb/Z1sem/zevYkvWqPkZNqHLT2XVtkxUc17qFST7TZiM1uBXdKg4/GyLpEfSmewOEy79XifUAt8xSZM9/PT9YVNvGTIPHcIVKs2JXV+ysjY5oSVPfIcWSofadr6os5fldGJd2fI1kM5kh738dAOVKRQKg5FIZMhBReuBSCSS8kDrkdMZ2rwwVmKmi+OswDkmaVDSQFha5lA2VE6XV1+O7zFzIHR4JFTCXVFz/+qac84D25rl8essqfqPWe6R1JPOZAdM+Brw4wOwWROJnRei05lsb7wvNkgpq7+mGp3sXkn3lAuUTMpTvcWPPV3JrLBR+evJDWoTsBo65XYkEumVs1aHrR4ObB3mXrc/EomkTDdthMt85zzh44nSkhUGZwTHdGvWI5KOpjPZwSDNZOy1MJnOZEfNsW7kJFjR6e+udCabMiHN7/epautI8FqonNXttaSlPxgkULpkyaP/otajB8q9bL0ZJwOfKxQKw5K2O3hrTyQS6W7gR3cSavPyz/pTW0y4HDDjOxEOXQtU2HxXKTfjQ+er6EaZTTxUelTsKjmezmSTnHt3ri2PhMkFv7v8cq7NZ+wtdU9mWb2AhEpTASzZL3zJ7hzrULps6Te+qcjkRLmXPcCFFhj9JnDVI9hVLRKJJB1+kfZ7ZYIhC/dIypnxowh2RXFDmWvUb5Il/o3yHD5RFcd6jhIunQegdCY7qOK4Oa8vz/OAT67zfpUff0prpd9DpXnKWXI9ytajB7T4sac58m6fyPxpXfC/U54NFXCXCVpOzuX6SCSSqHOgbHd4gx8pFAopn56i9QTLUCh1fjvMUBC/VH67Vbp7ejenO/ThcthPZdoj19Soii2/cCmkV1if2OLDLr2EyjlKVgAjkxNakv4WR71GWnYd1JLduXIv62KsRGCCZVLF6cJt1bt7ZtneCwvw+5PGKMEy1KHSb2W43PcCFTR0SHrcjLmk1bJ0+Emq2DoZ5Wi4fp+q9JjSWunXUJnOZPtVpml/6U++p+YDxznqNbT4sacrGV85wBdCYDi5aUbrdbM1gWqLg7duL7cuJcESPgiVvhjbU2ImRUIl5tOjYpdY7mvnX0vt6Uw2pRouEUOdp/IASl3Xh6HSnLRkqdcsOvSy2p78KUe8DpY88R3PhArUVqFQGFRx3SJbD9RpplKnk/MkA3SaopIGmbwnkCqZzdgP99qkS/uK8IhK2kXPp/Pqwjk5e5CK8sc3Ibtxqb6cMC30oVJlBs1GJie06PEsR7tOml96p5JusP08wQkMTy4xUsUSIkFc87FDLOsTtApOpa00nh7bU8FMirNf28mZxxyPmDUPCZTFQLm+Rr9iSNIOSVsX+BmSNBLww5ysY/0IDrS4dCGVPGmLR4bo9lpnbU8N6eR1N6nQtnShl0y3ViY5Wv5WKBSGI5HINpVZG3YePZFIpLsW3UxNq5yTisaY6jeZVF7ScIl/d7tlpicSifSa1mX4n03Xv4SH77WVzKQ4e59HOfWY4550Jtse74slCJSu2WG2mYv3xYYtP0+3ihNr9dYw5Nb7GHfL2ey5HelMNhHvi6W4TH0QKst9ITUfO6y2nT/hSNdZU/60lv5gUMd/8w9Lnrt0JjsQ74uNc8R8Lym7AezTBlSbKcTLjrFeaD/quITIcKFQ6K4gIHebY9St6mfxG5BUr1C5KSDjUoMQKj15r7WYSXH2Pnv5ocimeF8sDGV+a7wvlqzg/HaqOBa23Zy76Z9aLG2xJZ3JKqTB0q1AOf1QNVXNvcJcAzlJSVMG+h3WD7xWx3EqIXoK+SZUlryBLH7u+2rKn+ZIN0Dbzhd18ubDOnPhJQu9ZLq/OcuM+FyhUBg3a0E+aPnW9ZFIJOHm0h1mrKaTLidDXlxCxASznN6bNbfXfME5emrq9vGGL0Jl1JQbr533XsuKJhOz+Ei8Lzaq91qWB+c8TOjVe61ZboWNLelMdjzeFwtNl0MzKU+1gXJMUrIWrWmmDPSb2Wir+e5q5DHuVnU9h7rSmWx3SB44NVRTlSc6UapwFlspX+QoN9Di575f7iX0Nw9OsByQN5YYSSqgS4gUCoXxQqGQKhQKnSqOY3F6fOB/XQE477afqZPTHoiwOR7vi6XifbFEvC/WLumzcjbh23zuCcvkPWbVg2on5dka74t11rp75vQ5V/HB0FYVh3/4Rb9HtoFahkpV0EqJxmrb+aKajx0u9ZIOFjMOFCdf5q7NBmy6ijpdQmTYTwfarBO6ycGXcwdLjPi+MukkXHnqXutgJkUpIOOzcF7gSMX7Yt3mfuZGuHwk6MuNmNazB6vYxIikjZV0Y65BuEyacLnDJ/faHhc21cNEYx4OlebkLPikNjI54YtWyuita9TxxR6t2/ZlffgfvnreT8cXe3TR7Tf4+iRXEO4TXArBYLpqNnKJESdfkHn59CmiOd5Orh+uOX9zWmH2Ujl3VAZNZRrBDJe5WeFyrMrNDQZ1hnmzX6kqNrFdUrftBDwun+vReF+sV9JmebvVMunRbWEe1YypLDvjq2d3esUi/cYf3KErP/0ZLb3q+pKvveTjd0mSJsff1r5v/rVef+wpTR095a+T/LNfKfKJiVIzwfaYmduYsCcYEpL2O3jfgCpcWmA+kUgkIedLiPi27BUKhcFIJLJddi20VMzDGSq70plspxnn1MhKcbecj1Hq5PQHP1yalsYBOe/e2WEq8UHsdpiS83GJ2700mVG8LzZozvWgPNYTwTReubnmZy913dqqpvtrycpn6wve7MkWvXWNPvrYP+q6+75SNlDO1tZ+qa677yv6+Pf/j6K3rvHXSc6f1qL9u6s6n/BVyBmVtM3BW3tM91UngbJdzp4CjplupH5nuw90I/S37jqWlVqopqJPqAxHsBw34efeKjZzT9Bats3+OO2OudWLs+OaVssNKrageonbDySiYmyl90Kleaqx4FOaxa/u9uS6lJdtvlm3bP+uVZicL1zesv27umzzzb460W3P/TuhMlySctalxelMwE6XEAnEDd4EeavxKU4DPDyhmvFiWxrZLdCFMUqU23CFywEVJ/JxKmizy6ccvm97vcdPOjjXCa/MkGrukbUI4IRKr4XKcie6+eWfe25HL7r9Bm38asq17X3wS3+pZWtX+uZEN7/0jlqPHij1kp6gjn8II9Od1MkX2HrTjdUmHHXK+RIigwE67Lb7wvXmQyaUVbsEQyMrNtVWbJlkKnzBMlVFsFwflNlgzbIcTh6ebg/p+p3V3iNrsa5mNCyzE/spVHaX+seWn/3KW4HqgqVa/+cPubrNtvZLdd29/81XJ7vtF89XdV7hu2BZryVGkg5v/kG7sY9SOQ8FN85bQ0KlS2OUojyADG2wDO0ySqbMO7luR0TrWL2OdWjKY2BCpTnZC44HWvzqbjXlT3tqJz/437eqrf1S17d7ycfv8tX4yuY9e8u9hC6wweMkuFU87qCKJUS2mS6jQTJMcSNUWgSzhE/uB7U6BvBfsEzK2TIUHQFoHXLScpaX1MvEMNZ6LY913kF57OYweyBUlgseXuz6Ws0YynIuv+MO/4TKA8fLrVnJRRYwZskLJ5WASpcYSTrYdl4BfFLo5xlsYcWt+2RdrwGXn/4TKsMrIWfj9YMQKq2v8UbP9OxTtvfGAdlPMpTkMHsjVJb8Mmn+1VioDuClt3zSV5+3bX/J0N/B4rCB5LQiWXKChSqWEEkSwOBjbgWqej8tT8i9MUqEypAyrW5OAmKXX+sXppXV9toZMpMcwf5Y245bHZD9hFBd1He9ESoX/BJsPnbYk7O+1tLSq67Xos7l5x7UfW979vM2v7qPykLImG6mTsbCLLjESJVLiATyi5bZXENR4emUu5NH1HOslZu/i8pYuIPloKQhj5f3Rl87SUpKXY71drP8zbCDMsk58kCoXHA8ZevBX/tip9/4129r71//z5mfsX/6mibHnQfBxasuOfegemxM6Tmh8sXXCJXhNCB3lxhxuoRIIsDH2HbyElpr/cftMNVTj6flDp/+l9JFUQg9JxVy383bYK5P23WFh7yyNIfPjnW3g2M9uxymLN+7hdbKBoZKsz7lwoHltX2e3tn87h/qh5/epN33/LnGHt4x87P3/of0zO2/pfzuHzra7rKrr6r6s7XsOlifE54/zbjKEDLdTZ08bT1viREz1vIBB9vaYcZ4BpXttcPEPsE/x7WqnNtyvYWIyli4mdBk2zLkxyE2vR69pnlQUWylHJ1VJlOyn/E+wWFvUKhUmVaspnHvPnif2P+iXvj8n+r4nkPz/vvU0VN64fN/6qjFsnXZBb466a1HSwZYKgrBDZYpFac3tzV3iRGn3VeDPq26beVjlFLpO7XoydFbyyU6HD79b9SxgL+k6hTSGsk2dIzQSun4PtXlQvmzrZ/0s0RS40JlycBRr9Y2J176269q6uipkq+ZOnpKr33nG678vkWHXvbssYi8+Xqpf+7gsgg0J8FuZokRM26wx8E2tgZwCZH3rqlia67NtZMP8vEgVDq7vmok6aNjAR8xLUO2wyq6fRR0Si6h52LQhn14X6iLsW2ZjIrl9FzT4taXSJkulQ01sf9FvfX4cxW99sjwLl1d7wrp5ERdf1/zm2+Uu5FuMIOeETCFQiEXiUR2OAiGD0QikZSctVLm5bx10w+B0smkRbk6fLSBSCTSiO4jKdMqHiimglmrh26JWoQ/M2SlVuMfvRgqB9KZbEPKvAlYYTQou7WK/fQwotvh8YDdfapT9utdz1uniPfFxtOZrG2ZTPIwoDGhcsEm4pbjRzy7k28/+28Vv/bM/5uo++drO3KgvpXgkycdn2cfeSASiTzAJT6vfjlrbcw5rFT3B3wJkZSD41KPULm+QccjF9DzXMvKcEc6k03UIJjUsgW004PniDLfmH23qcB3BPiaH2FdSkeSlq8fMzMQl9qeVZms0f03dFzr/tr0lne7vp44+HrFrz3+y9eDf9LLL3lCt6YAq2KJESeVgZEgtlpJxRbKSCQy6DCg8zTbf7otX287iYmrAdDh03+bBcTXUyTgJFDXeX3Wel7zOYpDXe5TJUOoCfY7LLeZ4GzUP1QuXKk8dcKzO3ls3ysVv7bcuMuKD+xrr3j3pJdf8oRBy8E3IPtZ0pwI5OQ8ZmzpsMNAuYPxlL5k87At76CSst7lyrbttTdkWyn2UThAjZgKvO24Sr/UMToJlTVne5/MV9iiaDvkpov7Wf1DpS81okurVQV18mQDfucEpT/ETHfUZI1/zfYgLSFiWiYTkUhkWNJOOe/GNUAJDHyoHHb4tNyVhzBm/KdtZS0p+xmJOykWkP3ySH7pDWV7jx+lKFjfp2zveRV9f5pJfFhexK+hstzkLyhRWX2z/l1u6z2OE54MlinZd9GrVF4+XKsrEol0z/lJRiKRARMkj0p6RNV1+wv6Wp1BrvzYVDCHHT5A6HFpHb9+FWc1rNT0Mgi24YBQCSeh0g/XvHXZZoLDmt+nbCf9s62DbGH9XY+EygomfwnXgT3l6+PBmMrwqFXwG/BBF8+uSCRSmP2jYgvk7J8HJN0jd8aP5RX8tTqDyvaeOGoqmTnZrw3rRhlx9PQ/3hcbl11Xxm6KBiTZTsTmh4q77WfMUwxqfp9KmXtUpSE/5eC8JDktdQiV9DW2PLCHfN1yy5jKkDCtZttd3uyY6OI57xcoYyl9y/b7b3huYLOQqGYx7nQmm5Dd0/+xOWOUbFpbOikaqENg8wNaKWt7n3JyL3Xynt5q7r+ESjT+JOSPchDQSEm5+5Q1GfAlRJzYHtRZcKkEn2/2otwmsNmM7YmqurE9ySorXTaV4w4qYBBjCVH7+9R2h8u12H7vRkWPorqESiqJtToJ4xxaNI5pPXOrZXGI8DRvoExwGHzNpvvrmAsVG0eVmnQm2yu7sZ/5eT7beA2PDQiVCDnTSmk7CZKjOooJora9sfi+rnWoZACyncjEKT9/fFJu+Li1xAhP+AiUQWQzpnZ0gevLpjdAh6l41fr6G5hnjFKOUAlLtFajlqFtqMoMkrJ8vdP7b+i1uLWhwuLFHM1Zml96x88fnwcIIVMoFMYjkUhSxdlNqwlQlJ33bC0UCslGBlo1pgUhF6ST6GA+gfP2P94XG09nsoOyW+Q7YVMZMp+zy4XKlm2Z6fTQ6aLMNwYPFlDL+1RV36PxvlgunckOWf7efgdhlFDp1obOvO9ytWofR9SByNv1nzTs1MoPBP2wbvfADWGDpAd9FCxTkUgk4eCGLzGz6WxjkhIeWDokxfIlDakwD5eoGNmEyq50Jts9e3xmBSHU6h453xileF9sNJ3J+jVQpCyOFxpnNID7RGttbQLimEvXdMqybrPe8v4LN0MlnGs+cJyDUIMvrUZXqCORiF9v+DsdvG+AyXmKa2g1uHUSjQ+V81aYTVizfVqeUAWtYGZttS0OrvWF2HzOLopI6LW7cY343HqKQdn71AbVuZVy1v03lc5kk7Iby5kUyyZZsZ39dcH1tgrLV3A054aKyQlPfq6z0dYw3vBRAYdLjIyFPEiNSdoqqZNASagsM/bHtnxUuhi37XaHysykaPUdwILhXCMB3CfroRzMhFyWbW+mucsdVct2W13c22obKhdsiShEL+JoztF25IA3Q+WaSwmVcPPGmwrhMRqTtE3SpkKh0FkoFFhGJbhsWiCGygTOnOwnxOqvoCLrZiulk+8AKl7hZnv+PT/2fp4JrMIarl3hsDeF2+td206YVsm9ElWEygW/aKaWESqdaD52uP6hsr3swzQqx0DRiAkKO1Rsjdws6SoTJPsZsxj4ilC3W9+RVVRSEmVaQGyf/g9VME7IttLfTWkJdViwXR7CL3WMEUKla2zve/Mtd+TGg4JBy7dtoQW6AaHyzIWXcDQdaDl+pP6hMrqi3IXHDJ4Ig6FCoRAp87OhUCh0FwqFXtMaOWjW9UQ4uDKecs79NSW7p+VRSb0LVOjbHYTKlBv7QWUaTh8o+Gjyk9FaH4uQPHhw0ptiwGFrsdvhVmISwsozjeXrS4aNqY2r1LLrIEfViOSPSCu997nOXnF1qX8e48wBgKOwVGlleUDSA5YVofnCYK8JnZWqaIxSvC82bDkDbCdFhVBZaRn00b4NS+ohVFbNSSjrNBPr1ELe8r7Zn85kaxVyQx0qR0uGlZWXSyJUTmvd8zNF3j1a8jVN+aN1/1xluiqPcuYAwFFYqrSXh22o7Ehnsr3xvtjgPGHThs3rR1T5eFJmvgyvXsvX53y0bznL6zS6wHUaWg57U0j2LZu1NN1bJMUZLc2q+2u5bpFnLr2cIzo7se86qMWDPyr507bzxbp+pjOrl5XrqpzjzAGAJLvp7/OVPsk2r7OdZbl/TmUtIbuxbHnZjScataw80gU2fIEhIbsWH8kHk/RU+Vl7KRnn3beiAdiPJKfS5VBpLDi73elV7+eIetyZa8rWQRhPCYAKs31Isr132lZSuuZ8Jtun/7bdt2z3h1AZPgkH7/FNK565XoZsQyUTu1RdRryoI53J8sCgBqFywS+aMxdeojOrl3FUvRwqr1hT7iU5jhIAqKah0qwTaVth7TeBt1v2XU5tp+cftXx9J0UmPEwZtF3IfqzM+qheZBuCo2Jil+kykpD9zMBexnmtQagsGTrOrL2Oo+phZVqTRxiIDAC1D5VG0vL1W8wSDrbv2+7g3m5b+e+myIRK0sF7Uj7cTyctq/20VjouI17WRTf/eofKK6/mqHrUmWsvZjwlANQmVNqGsOmlFWxnw0zKvoUo6fCz2eikyISDaYHqcvBW34VKhz0KQt9aabqKdgRw12itdDNUmqedCy4Ie/LKdTobbQ31QT0bbdXUxlVWP/U4Zqc3bKzqgQEAhIhVpbmKtfdsA5/trIg7quhyaBN4O2idCUVYaJd9V2pJGvJh19dqwvADpldBWAU1fG0J+XktqaWKC+zBhf5x6kPX1H1WU0+FyjWX6tjv/4nVey7854fUVOM1PievuqFcpYhpsAFQcbbv4jRSxa8bNJX0Ws2QOFDFe0dl19qwQTycDLpBh2V1wK87HO+LpcyaiR0OjlXouks6HG/rt8BMi+U8mhy+r+SXxtQNH+HIeszUxlXlur7u4CgBwEw4sg1fTius4zWscA9V0YJa9rveheMGf4WFlMOwMBaAh9ZOrtH16Ux2IIRFJRnw/UvQK8PFUGnWq1ywW8yplR/w/CywizqX68KbO0Jzok+v/VC5l9BKCQDOwlG1SzGlarQf1W7XdnKfTopOYANlv5wvSB+EVp2U7Mc/S9I9ZgxqWMpJp4LdSikVW+oT3BXO11LFewdUogvs6ds+qubHnvbkTi9bu1JXf/5zamu/WHveTOrU6LuBPslno606ee0tpV5iuyg2ABAqXQqV8b7YaDqT3V5FpX0+Y/G+WLWhkrUqMT0xz4MO3z4UhKE18b7YuOkC+4iT+nI6kx02DTJePs/t0/taxWaSTsqIR+75Nt26++XjLt1eDJWDpW4yJ6+7SW3RITXlT3tup6/+/Od0+W//kSTp2nvf0e57/jzQJ3ly001lzyVLiQDADNsn7W5UFgdcDpVJF7YxXOPjBu8HylSV5TIwY8/M2MqEg3IelZRLZ7LdXg2WJlDmzPnKOdxGp4OyMhTvi3V7YP+Tkh6weEtHOpNNuPDgLlCaqri4RlViHF6hbWklYaZuFnUu16o//qQ+/A9f1cTrr878/cTrr2rdti9r1R9/Uos6lwfyJJ9cX/b+x0UBAHI0SY/cmNXSVDbdemLvSu8T87Ax76BiCf9fB+3pTDZXZaDc6vXWuTqG5NnBFlsAAA/YSURBVJlg6cFz3a3iuPD1VW4qWaf31EKqjmWBUOnkJJxc3+WZ5UVOjb6rtfd/XZd8/C5d/YUvzfz91V/4ki7/7T/S2vu/HshusJObrlehbWmpl4xVOZEDAASJbShys+uWW92pBlzsfTJc4+MH74WMXhMyqml5Hon3xZJBOzYmJG+tIlju9NIYS9NCt1NVzj5tWjp7Ld/mmfqneTC43fJt6734kMC3odL0k19w4LKXWiujt64p+5pla1cG6uSejbbq5M23l3tZkssAAGbUe5Keir9TGxBOnewflSz/hsnOdCY7KOnxKkNG3kHA8FOwTKq6h0mPpDPZVCNnEDXnOie7Lp+l9DsoM16rf6Yc7jfcCJWVFAqvtFbmf7xPz/+XuzU5/vZ5/zY5/rZ2/VlCx/ccCtTJndx0U7llRPL0BweAqkLRqMu/v9qK1naXx8gzA2w4wmRK0n5JPS5ssteNLuEe1yvLruFzbJE03IiWLjOT77BcGgNtwrFtuBrzWv3TtJrarjncQ5d/F0OlKRSlWyvv8MbY/SPf/7l+8Rf/47y/H/nyn+itx58L1Ik9G22tZCwlM1cBwLka1lJpDFZZWU26/HlyNT5+aFyQTJjWqv1yb5Koz4ZhSI15cNNd5bXaoWJ32MF6BJN0JtudzmRHVZxkM+riphMOtpfy6Kl1Ui9OCpKqm/117gFdcJrlE+u61Tq8S80vvdPwHc7v+qX+7aYNet/vfkySdPiZ5wN5Yk/FN5cbS5knVALAuRVtB5UjV0OlWbpgQM66pe2oQQuR7fbWU5I8V643SGo3IajT/LcWC3V/Nky9n+J9sWHT6vdIlZvqUbHFa7uklNuh3IzhTKh2szPbtlJ6tv5pZvgdsPwe6E1nsu2souBSqDQnIVnqJnXirrt1wUsPNz5szZmMJ4iT80xtXKWTV64r+yCACwAItIFIJNKoazxVKBT8WLm0bWUbq9F9NOUwVLpeUTNraFqHmAbN+jmQzmQbVeb767jPiQq6TXbWKDgSKM+v/8qFYCkVW4u3pDPZMRV7LAw6DZimfPSan5qVAxNYbbc/4PH6p+1DvagJ1sl6fEeZ3gU1+d6u9hpucfHDJEtdVKdXrNbknR9W25M/reqXHP/l63p2y+9Yv2euwz9YuIXSdvsnDx4+589N+97Whf/8kNU2mva97cpJOBtt1cSn+iqpCNFKCQRbI1uMcj49ZrahcrRGFdVR02ph0yVxqIbdDodk18rRKfe7BXu9zNdz0pWOOgdGAmX9guX0+b1H0j1muyPmXjO8wP11uiW60/zUc8yZkyDl9bLipKdIvUJltIbnt+rvjxaXL6pEqZ09/rHNat43WlU32Kmjp5T/8b6qP++psYVbKKvdflP+tJp2HWzIlXCq51PlJueZLvwAgHN1eyg8pyxDZS0fFI5aVmQ2yIV1MuF5eRUn5cmF/UDUIFjOtt78TE+i9IAX9tm0hto+3Nju9UmczBAE24d60XQmmwj75JctLm+vX9KuUi84cdfdWnrom2rKn3b8SyY3Xa+m8WLL+VTHVZKk5jffUOTkyZk/S1LTqZNqe/KnOtl7W3Fnx/arZddBtaxYpPff+zm1RVdo/MXdmpqY0DvPPK+Vd2xS/qWXdGVvTJP5o8r/8hdqixYfPh5/7VUtu+JKSdL47j1qX7dWJ948pLcef04dX+zRiTcP6UhueGa7r/z9N3V8zyFdtvnmYth78221r1urQ0/t1Mo7Nmns4R266PYbtPp3epT/5c819vCOqg/+5KbrdfLaW8q9bMhMWw8AOD8M2YatWlVsculMttIWwrEa39dt97ObohR4IwrHLK+2wXLUPFCJhmCXk3V6TyMMyH7yqqS83wpbU00uX1DDkraVes3pFat1qudTVf2eqRs+oqmOqzTVcZVO3HinCstX6NRtt+vkJ39PheUrdOLGOzV53Y06/rHNmrzzwzpx452aev9anW0vBsTf+IM7dPmdcS1d3amWxUu09v6v69p7/6vWfO4+Xdkb08W3fkrtH9yo6LX/ofh3n/6Mlr//Gl1331d0yU23adH7LtV1931FH/zSX2rZ2pW67r6v6Iq7Yur8Qt/MdqddcVdMV9wVU/u6tcXX9fXouvu+okWdy3Xj33xHS1d36vLf+l21rFhU1TE5c+3FmvhE2WWh8ioO1gYAzOKFSXrmUWkFpdYVNdv97KREBdq2eF9sA4Fy3npwTsWHKiNB3k/TSmnbDXOHX8qMyTO2a5F2NGKJmMCGyllfbiUXbz557S2avPPDVf2Syetu1OR1N0qSWvf8TKdXrNbpFavVuudnxb87+GtFJid0dtFiSVLzWwdmWjclqa39Ur06mNarj+7Q5Pjbuvy3/0ivfecbM//+8t9t097kw3p3z7N69X89qneef0GSdPgnP1J+1y9ntnHF3Z8+77Pt+/bfL7jm5RV3f0GSdPHHi5/9hc//qZ6NfUFTR085PhZno606cdfd5WZ7lYqT8/AlAADns14Ko9YTs5Rbssuox3rDtt8bHRSnQBqTtCneF2MITfn7Qrek7QHeTSdlwG9zeTi5ryYJle5eTOOqoDXs+Mc26/RH17jyO1t2HVTzscNqPnZYLWYs48lrb1HL8SNqfaH4nX/mstUzLZWvP/aU3vjXb2vt/V/XRd0b9MaTaUnSa5kdeuXvv6l39zyrG//mO1q2duV5v+uSm25Ty4XvhbfL74xrcrw4yc7oNzJ6d8+z2vjVlC66/YZ5P2tb+6XF0LvsAkmqKkzOhPTP/EedXrG63MuGmJwHAFwLlfVqiRio8t/dqiRbCfsT+4DJS9oa74t1Mn6y8rpwvC+WkLRZ1a1l6TmmV0eP5duG/FZ2KnyoN1eXWcKHUOniichJ2lo2WN4e15lrL3b0O9r2Pq+2ve/N4Npy/Ihajh+Z+fOS55/UBX/1sJoPHJdUbKmcdlH3Bk28/mrxde9bqRMHi7PDnhp9V5fcdrOOHxgtbvPC81v+Dv/kRzN/f/iZJ9TWfqne+fH3Zrab37tHkrT4fe87/73PPKGJ/S8W//9Hz0mS1m37stZt+7Iuuv0GdXyxx/o4nPjM7+rUyg9U8oXQKwCAW6GyXrObpkpUSuu53ttIjY8nPBomJXXG+2JJDoej+vCgit3Bt/ng426r8L7mpCykfHoKnXzu0LbkN9XwQkqqTH/kQttSHYv/Z+tg2fTWQTXlj6opf1SLDr383t+9VWylXHToZTXlj868ftGhl1WIXqSzl62aCZJX3P0FHX7mCb3+2FM68eYhHX7mieK/rfoNXX5nXG/867eV//E+5ffu0Yk3D2nq2IQOP/PEzJjKw888odeeyOrwM0/oUO5p5ffuOWe7b/3LD4t35L17ZraR37tHr3zrIR1+5gkd33NIr3zjL3TxrcXxpRdceaValy+3DpQVTMwjSQnWpAQAV0PQaJ0qpeMlKjaDdby32+5vJ0XKt8Yk3TsdJqk/VH8Nmy7DV0na4cGPuF3SVfG+WH+5c21aKW0nsBnz8ayoTh7abTHHKXRaarz9XvNFFC0XLC9M/13FS40sfuzpmf9v2/nieX+39GuPnvP6uX8ee3jHObOtvvX4c3rr8WLL4d77H9Le+99bY/LXX/3WzP//9D/92TnvOee/em5m27PNfv/0aw5+699m/m32v9coUG5ltlcAKFlRapf9OMBcnSs298zz98k6foZh2XV5o6XSX0ZUnLV0sNZjhUMcLkcl9ZrAkXQQztw03cshZTnXhpNWuKSPz5mT5UWk4jDAZNjKeFOtT4aKg5VL9id32mIZVhaBcgddVgCgJgFotF4fzlT65k76Ue/13kbrcExRH0OmPG2VtEnSCjOba5JAWZ/r2Yy3XKFii3A9Z4rdIWlzvC/Wbs53xde1efiWsA2vAVi70UlrZb85XqHSUoeLZzidyfarzIKwTlosCZQljShYy4fkavz6WlXCtvrsM4fpc4/67BpIeehY+6VM2LApO2rATNopnfu0PNWAc7qVMl+T+0muDtfheEACo22Z8Ox93jS8DEgaMK2XvSo2xHTLvXUuR8zxyrnQa63dQcDyfZkzOeZes/+2x2u8gvK51SO7WvV9KFIoFOrySdOZbKJcsJSkyOSElv5gcKZbK4rORlt1Kr5ZJ69cV8nLxyRtYBwEAARHOpPNqbg23FC8L9bNEQECe613qjguuduEk9kt/11z6nujc4LBuKRhZupFvdUtVJqLJKUK+yUvef5JLR78EWdI0pnVy3Qi/seVLBsiFbsad9OFBQACV9FMqPhwdjNj5QEAoQ2VtsFy8au7tSj9uJryp0N7gk5/dI2O3x5XoW0pgRIACJY5WikBAKEPlbbBsvnYYS39XkYtuw6G6sScjbZq8o4unVhXcd2BQAkAAAAgHKHSBMuFpkif15LdObU9NRSKVsupjat08pO/V2l3VwIlAAAAgPCFShMsE6pg8p5pQW+1dNA6KRVn9upmUh4AAAAAoQuVJlj2qjhFdMXTJy869LIWPZ5V84HjgTkRk5uu18QneisdOzltSFIvgRIAAABAaEOlCZYbJA1K6rB53+KXnlXb/93p63A5uel6nbz5dp258BLbt26L98X6KcIAAAAAQh8qTbBsV7HFssf2vX4Ml1WEybykBNPJAwAAACBUzh8u+yUlZdEddtqiQy+r5ecvqG3ni5482GdWL9Ppj2zQyfVdtt1cp42o2N11lKILAAAAgFC5cLDcoGKr5XpHOzU5oUX7d6t1z88aPqnP2Wirpj50jaZu+IhOrfxANZvaGu+LJSmyAAAAAAiVlYdLx62WcwNm82v71Pyrsbp0kZ3auEpTHVdp6v1rbZYFWciQpH6WCwEAAABAqHQWLDtNsNzixvaajx1W69GDirz5ulrG9ivydr6qoDm1cZXOtrfr7GWrdPayVdW2Rs6WN2EyRTEFAAAAQKisPlx2m3DZVYvtNx87rJbjR2b+3PTaK+f8e2H5ChWiF8382cXwOF+YHJA0wFIhAAAAAAiVPguXDUSYBAAAAECorHO47JeDJUgIkwAAAAAQ8lA5K1x2mnCZUBUT+jTAkKQUYyYBAAAAECq9EzB7JU3/eDFgjqi4VMoga00CAAAAIFR6P2B2m5/1DfoYeUk580OQBAAAAECo9GnAbDfhcsOs/9aiJXNE0vD0T7wvlqOIAQAAACBUBjdobpDUaX5kAmclRs2PVGyFHI/3xYYpTgAAAAAIlQAAAAAAVKiJQwAAAAAAIFQCAAAAAAiVAAAAAABCJQAAAACAUAkAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAABCJQAAAACAUAkAAAAAIFQCAAAAAAiVAAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAIFQCAAAAAAiVAAAAAABCJQAAAAAg0P4//U9DqP/Pc9YAAAAASUVORK5CYII=','10', '5', 50, 0, '', '', 'T', false, 300, 'R', false, false, 1, false, false, false);
        $this->Ln();
    }

    public function Footer() {
        $this->SetY(-15);
        $this->Cell(20, 10, 'Printed on:' .date($this->datetime_format), 0, false, 'L', 0, '', 0, false, 'T', 'M');
        if ($this->footer_text) {
        $this->Cell(150, 10, $this->footer_text,  0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
        $this->Cell(0, 10,  'Page ' . $this->getAliasNumPage() . ' of ' . $this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }

    private function addErrorMessage() {
        $this->MultiCell(0, 0, 'Message', 0, 'L', 0, 1);

        $this->SetFont('Helvetica', '', 14);
        $this->Ln();

        foreach ($this->claim->ErrorReportItem as $key => $value) {

            $this->MultiCell(0, 0, $value, 0, 'L', 0, 1);
            $this->Ln();
        }
    }

}
