<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 6/14/2016
 * Time: 10:58 AM
 */

class ReferralFormPDF extends TCPDF {

    protected $patient = null;
    protected $companyName = null;
    protected $companyAddress = null;
    protected $footer_text;

    protected $date_format;
    protected $datetime_format;
    protected $font_size;

    public function __construct($pdf_options)
    {

        parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $this->SetCreator(PDF_CREATOR);
        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $this->SetAutoPageBreak(TRUE, 0);
        $this->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $this->setCellHeightRatio(1.2);

        $this->setPrintHeader(false);
        $this->setPrintFooter(true);
        $this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $this->SetFont('helvetica', '', 10, '', true);
        $this->AddPage();

        $this->line_width = $this->LineWidth;
        return $this;
    }

    public function generatePDF($patient, $referring_doctor, $doctor)
    {

        if ($doctor instanceof Doctor) {
            $pdfAddressText = $doctor->getAddressText();

            $pdfAddressTextArray = explode("\r\n", $pdfAddressText);

            $this->setFontSize(18);
            $this->Cell(0, 0, $pdfAddressTextArray[0], 0, 1, 'C');

            if (count($pdfAddressTextArray) > 1) {
                $this->setFontSize(14);
                $this->Cell(0, 0, "" . $pdfAddressTextArray[1], 0, 1, 'C');
            }

            if (count($pdfAddressTextArray) > 2) {
                $this->setFontSize(12);
                $this->Cell(0, 0, "" . $pdfAddressTextArray[2], 0, 1, 'C');
            }




        }

        $this->setFontSize(12);
        $this->Cell(0, 0, 'Print Date: ' .  date("m/d/Y"), 0, 1, 'C');

        $this->writeHTML("<hr>", true, false, false, false, '');


        $this->setCellPaddings(3, 3, 3, 3);
        $this->Ln(2);
        $this->setFontSize(18);
        $this->Cell(0, 0, 'Referral Form' , 1, 1, 'C');

        //$this->Ln();

        //$this->setCellMargins(1, 1, 0, 1);

        $txt = '';
        if ($patient instanceof Patient) {
            $txt .= "Re: Patient" . "\n";
            $txt .= $patient->getFname() . " " . $patient->getLname() . "\n";
            $txt .= $patient->getAddress() . "\n";
            $txt .= $patient->getCity() . ", " . $patient->getProvince() . "\n";
            $txt .= $patient->getPostalCode() . "\n";
            $txt .= "Home Phone: " . $patient->getHphone() . "\n";
            $txt .= "Work Phone: " . $patient->getWphone() . "\n";
            $txt .= "Date of Birth: " . date("m/d/Y", hype::parseDateToInt($patient->getDob(), hype::DB_ISO_DATE)) . '  [' . $patient->getAge() . ']' . "\n";
            $txt .= "Health card: " . $patient->getHCN() . " " . $patient->getVersionCode() . "\n";

        }

        $this->setFontSize(12);

        $this->MultiCell(90, 5, $txt, 1, 'L', 0, 0, '', '', true);

        $txt = '';
        if ($referring_doctor instanceof ReferringDoctor) {
            $txt .= "Referred To: " . "\n";
            $txt .= $referring_doctor->getFname() . " " . $referring_doctor->getLname() . "\n";
            $txt .= $referring_doctor->getAddress() . "\n";
            $txt .= $referring_doctor->getCity() . " " . $referring_doctor->getProvince() . "\n";
            $txt .= $referring_doctor->getPostalCode() . "\n";
            $txt .= "Phone: " . $referring_doctor->getPhone() . "\n";
            $txt .= "Fax: " . $referring_doctor->getFax() . "\n";
            $txt .= "\n";
            $txt .= "\n";
        }

        $this->MultiCell(90, 5, $txt, 1, 'R', 0, 0, '', '', true);
/*
 *
 *


        if ($patient instanceof Patient) {
            $this->setFontSize(14);
            $this->Cell(0, 0, 'Patient ' . $patient->getFname() . ' ' . $patient->getLname(), 0, 1, 'C');
        }
*/





        //$tbl = "<table style='padding-left: 5em;' cellspacing=\"0\" cellpadding=\"1\" border=\"0\">";


        //$this->SetFont('dejavusans', '', $this->font_size);


        //$this->writeHTML($tbl, true, false, false, false, '');

    }

}