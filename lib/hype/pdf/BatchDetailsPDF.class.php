<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class BatchDetailsPDF extends reportPdf {

    public function __construct($pdf_options) {
        $options = array(
            'orientation' => 'L',
            'show_header' => true,
            'header' => $pdf_options['report_title'],
            'subheader' => '',
            'show_subheader' => true,
            'doctor_title' => array_key_exists('doctor_title', $pdf_options) ? $pdf_options['doctor_title'] : '__toString',
            'date_format' => array_key_exists('date_format', $pdf_options) ? $pdf_options['date_format'] : ClientParamTable::getDefaultForValue('date_format'),
            'report_headers' => $pdf_options['report_headers'],
            'columns' => array_keys($pdf_options['report_headers']),
            'footer_text' => array_key_exists('footer_text', $pdf_options) ? $pdf_options['footer_text'] : ClientParamTable::getDefaultForValue('pdf_footer_hype_systems'),
            'subheader' => $pdf_options['subheader'],
        );

        return parent::__construct($options);
    }

    public function buildReportTableNonTCPDF($batches) {

        ini_set('max_execution_time', '120');
        ini_set('memory_limit', '-1');

        $tbl = '<table border="1">
                <thead>
                <tr>';

        foreach ($this->report_columns as $header) {
            if (array_key_exists($header, $this->report_headers)) {
                $tbl .= '<td style="text-align: center;"><b>' . $this->report_headers[$header] . '</b></td>';
            }
        }

        $tbl .= '</tr>';

        foreach ($batches as $batch) {
            $tbl .= '<tr>';

            foreach ($this->report_columns as $column) {
                if (array_key_exists($column, $batch)) {
                    $tbl .= '<td style="text-align: center; font-size: large;">' . $batch[$column] . '</td>';
                }
            }

            $tbl .= '</tr>';
        }


        $tbl .= '</table>';

        $this->writeHTML($tbl, true, false, false, false, '');


        return;
    }

    public function buildReportTable($batches) {
        $column_settings = array(
            'ratio' => array(
                'account_num' => 1,
                'claim_id' => 1,
                'id' => 1,
                'pay_prog' => 1,
                'last_first' => 2,
                'service_code' => 1,
                'fee_subm' => 1,
                'fee_paid' => 1,
                'ra' => 1,
            ),
            'fixed' => array(
                'status_text' => 25,
                'service_date' => 20,
                'batch_number' => 25,
            ),
            'aligns' => array(
                'fee_subm' => 'R',
                'fee_paid' => 'R',
            ),
        );

        $width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;

        $this->calculateColumnWidths($column_settings);

        $this->Ln();
        $this->writeMainTableHeader();

        foreach ($batches as $batch) {

            $this->writeMainTableRow($batch);
        }

        $this->MultiCell($width, 3, ' ', 'T', 'L', 0, 0);
        $this->Ln();

        return;
    }
}
