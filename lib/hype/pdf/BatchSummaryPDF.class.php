<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class BatchSummaryPDF extends reportPdf {

    public function __construct($pdf_options) {
        $options = array(
            'orientation' => 'P',
            'show_header' => true,
            'header' => 'Submission Summary',
            'subheader' => '',
            'show_subheader' => true,
            'doctor_title' => array_key_exists('doctor_title', $pdf_options) ? $pdf_options['doctor_title'] : '__toString',
            'date_format' => array_key_exists('date_format', $pdf_options) ? $pdf_options['date_format'] : ClientParamTable::getDefaultForValue('date_format'),
            'columns' => array('doctor_name', 'file_name', 'batch_date', 'batch_number', 'h', 't', 'total'),
            'report_headers' => array(
                'batch_date' => 'Date',
                'file_name' => 'File Name',
                'batch_number' => 'Batch ID',
                'doctor_name' => 'Doctor',
                'h' => '# Claims',
                't' => '# Records',
                'total' => 'Total'
            ),
            'footer_text' => array_key_exists('footer_text', $pdf_options) ? $pdf_options['footer_text'] : ClientParamTable::getDefaultForValue('pdf_footer_hype_systems'),
        );

        return parent::__construct($options);
    }

    public function buildReportTable($batches) {
        $column_settings = array(
            'ratio' => array(
            ),
            'fixed' => array(
                'batch_date' => 21,
                'file_name' => 26,
                'batch_number' => 27,
                'doctor_name' => 53,
                'h' => 15,
                't' => 17,
                'total' => 15,
            ),
            'aligns' => array(
                'h' => 'R',
                't' => 'R',
                'total' => 'R',
            ),
        );
        $width = $this->getPageWidth() - PDF_MARGIN_LEFT - PDF_MARGIN_RIGHT;

        $this->calculateColumnWidths($column_settings);
        $this->writeMainTableHeader();

        foreach ($batches as $batch) {
            $data = $batch->getGenericJSON(
                    array('batch_date', 'file_name', 'batch_number', 'doctor_name', 'h', 't', 'total'), array(
                'date_format' => $this->date_format,
                'doctor_title_format' => $this->doctor_title,
            ));
            $this->writeMainTableRow($data);
        }

        $this->MultiCell($width, 3, ' ', 'T', 'L', 0, 0);
        $this->Ln();

        return;
    }

}
