<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class doctorSchedulePdf extends TCPDF
{
 	protected $orientation = 'P';
 	protected $datetime_format;

 	protected $last_day;
 	protected $first_day;
 	protected $summaries;
 	protected $location_title;

 	protected $regions;
 	protected $locations;
 	protected $doctors;

 	protected $dates;
 	protected $events;
 	protected $holidays;

 	protected $footer_text;

 	public function __construct($options)
 	{
 		$this->datetime_format = $options['datetime_format'];
 		$this->orientation = $options['orientation'];
 		$this->last_day = $options['last_day'];
 		$this->first_day = $options['first_day'];
 		$this->summaries = $options['summaries'];

 		$this->regions = $options['regions'];
 		$this->locations = $options['locations'];
 		$this->doctors = $options['doctors'];

 		$this->dates = $options['dates'];
 		$this->events = $options['events'];
 		$this->holidays = $options['holidays'];

 		$this->location_title = $options['location_title'];
 		$this->footer_text = $options['footer_text'];

 		parent::__construct($this->orientation, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
 		$this->SetCreator(PDF_CREATOR);
 		$this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
 		$this->SetAutoPageBreak(TRUE, 0);
 		$this->setImageScale(PDF_IMAGE_SCALE_RATIO);
 		$this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
 		$this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
 		$this->setPrintHeader(false);
 		$this->setPrintFooter(true);

 		$this->SetMargins(PDF_MARGIN_LEFT - 10, PDF_MARGIN_TOP - 20, PDF_MARGIN_RIGHT - 10);
 		$this->SetFont('dejavusans', '', 10, '', true);
 		$this->AddPage();

 		return $this;
 	}
 	public function generateContent()
 	{
 		$content = '
 			<img src="clipone/hype-logo.png" style="width:250px;">
 			<h2 align="center"><span style="font-weight: bold;"> Doctor Schedule: '
 			. date('M d, Y', $this->first_day) . ' - '
 			. date('M d, Y', $this->last_day) . '</span></h2>'

 			. $this->generateSearchTermsTable()
 			. '<table border="1" width="100%" cellpadding="4">'
 			. '<tr>';

 		foreach ($this->dates[0] as $day)
 		{
 			$content .= '<td bgcolor="#dddddd"><h3 align="center">'
 			. date('l', strtotime($day))
 			. '</h3></td>';
 		}
 		$content .= '</tr>';

 		$next_appt = count($this->events) ? array_shift($this->events) : array();
 		usort($this->events, array('build_scheduleActions', 'dateSort'));

 		$next_holiday = count($this->holidays) ? array_shift($this->holidays) : array();

 		$locations = array();
 		$doctors = array();

 		foreach ($this->dates as $week)
 		{
 			$content .= '<tr>';

 			foreach ($week as $day)
 			{
 				$datestamp = strtotime($day);

 				while (array_key_exists('date', $next_appt) && $next_appt['date'] < date(hype::DB_DATE_FORMAT, $datestamp))
 				{
 					$next_appt = count($this->events) ? array_shift($this->events) : array();
 				}

 				while (array_key_exists('date', $next_holiday) && $next_holiday['date'] < date(hype::DB_DATE_FORMAT, $datestamp))
 				{
 					$next_holiday = count($this->holidays) ? array_shift($this->holidays) : array();
 				}

 				$content .= '<td><h4 align="right" style="font-weight: bold;">' . date('M d', $datestamp) . '</h4>';

 				while (array_key_exists('date', $next_holiday) && $next_holiday['date'] == date(hype::DB_DATE_FORMAT, $datestamp))
 				{
 					$content .= '<h5 align="center">' . $next_holiday['name'] . '</h5>';
 					$next_holiday = count($this->holidays) ? array_shift($this->holidays) : array();
 				}

 				$content .= '<div style="font-size: 8pt">';
 				while (array_key_exists('date', $next_appt) && $next_appt['date'] == date(hype::DB_DATE_FORMAT, $datestamp))
 				{
 					$content .= $next_appt['open_tag'] . $next_appt['title'] . $next_appt['close_tag'];

 					if (array_key_exists('location_name', $next_appt)) {
 						$locations[$next_appt['location_id']] = $next_appt['location_name'];
 					}
 					if (array_key_exists('doctor_id', $next_appt)) {
 						$doctors[$next_appt['doctor_id']] = $next_appt['doctor_name'];
 					}
 					$next_appt = count($this->events) ? array_shift($this->events) : array();

 					if (array_key_exists('date', $next_appt) && $next_appt['date'] == date(hype::DB_DATE_FORMAT, $datestamp)) {
 						$content .= '<br />';
 					}
 				}
 				$content .= '</div></td>';
 			}
 			$content .= '</tr>';
 		}

 		$content .= '</table><br /><br />';

 		if (count($this->summaries)) {
 			$content .= '<table><tr>';

 			if (in_array('location', $this->summaries)) {
 				$content .= '<td style="font-size: 25px;">' . $this->location_title . ' in this schedule:</td>';
 			}
 			if (in_array('doctor', $this->summaries)) {
 				$content .= '<td style="font-size: 25px;">Doctors in this schedule:</td>';
 			}
 			if (in_array('legend', $this->summaries)) {
 				$content .= '<td style="font-size: 25px;">Legend:</td>';
 			}

 			$content .= '</tr><tr>';

 			if (in_array('location', $this->summaries)) {
 				asort($locations);

 				$content .= '<td style="font-size: 23px;">';
 				foreach ($locations as $loc_code => $location)
 				{
 					$content .= $location . '<br />';
 				}
 				$content .= '</td>';
 			}
 			if (in_array('doctor', $this->summaries)) {
 				asort($doctors);

 				$content .= '<td style="font-size: 23px;">';
 				foreach ($doctors as $doctor)
 				{
 					$content .= $doctor . '<br />';
 				}
 				$content .= '</td>';
 			}
 			if (in_array('legend', $this->summaries)) {
 				$content .= '<td>'
 				. '<div style="font-size: 23px">' . DoctorScheduleBlockTable::getStatusLegendChart() . '</div>'
 				. '</td>';
 			}

 			$content .= '</tr></table>';
 		}

 		$this->writeHTML($content);
 	}
 	protected function generateSearchTermsTable()
 	{
 		$rs = '';
 		if (count($this->regions) + count($this->locations) + count($this->doctors)) {
 			$rs = '<table>';

 			if (count($this->regions)) {
 				$rs .= '<tr>'
 				. '<td width="7%" style="font-size: 25px;">Regions</td>'
 				. '<td width="93%" style="font-size: 23px;">' .  implode(', ', $this->regions) . '</td>'
 				. '</tr>';
 			}

 			if (count($this->locations)) {
 				$rs .= '<tr>'
 				. '<td width="7%"  style="font-size: 25px;">' . $this->getUser()->getLocationTitle(true) . '</td>'
 				. '<td width="93%" style="font-size: 23px;">' .  implode(', ', $this->locations) . '</td>'
 				. '</tr>';
 			}

 			if (count($this->doctors)) {
 				$rs .= '<tr>'
 				. '<td width="7%"  style="font-size: 25px;">Doctors</td>'
 				. '<td width="93%" style="font-size: 23px;">' .  implode(', ', $this->doctors) . '</td>'
 				. '</tr>';
 			}

 			$rs .= '</table>'
 			. '<br />&nbsp;';
 		}

 		return $rs;
 	}
 	public function Footer()
 	{
 		  $this->SetY(-15);
        $this->Cell(20, 10, 'Printed on:' .date($this->datetime_format), 0, false, 'L', 0, '', 0, false, 'T', 'M');
        if ($this->footer_text) {
        $this->Cell(150, 10, $this->footer_text,  0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
        $this->Cell(0, 10,  'Page ' . $this->getAliasNumPage() . ' of ' . $this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');

 		// $this->SetY(-15);
 		// $this->Cell(.5, 10, $this->footer_text, 0, false, 'L', 0, '', 0, false, 'T', 'M');
 		// $this->Cell(0, 10, 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
 		// $this->Cell(0, 10, date($this->datetime_format), 0, false, 'R', 0, '', 0, false, 'T', 'M');

 	}
}
