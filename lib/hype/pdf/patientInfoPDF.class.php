<?php

require_once '../lib/vendor/dompdf/autoload.inc.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

class patientInfoPdf extends Dompdf
{
 	protected $patient = null;
 	protected $companyName = null;
 	protected $companyAddress = null;
 	protected $footer_text;

 	protected $date_format;
 	protected $datetime_format;
    protected $font_size;


 	public function __construct($pdf_options)
 	{

 		$this->footer_text = array_key_exists('footer_text', $pdf_options) ? $pdf_options['footer_text'] : '';
 		$this->patient = array_key_exists('patient', $pdf_options) ? $pdf_options['patient'] : null;
 		$this->date_format = $pdf_options['date_format'];
 		$this->datetime_format = $pdf_options['datetime_format'];
        $this->font_size = $pdf_options['font_size'];

 		parent::__construct();


 		return $this;

 	}

 	public function generatePDF()
 	{

        if ($this->patient instanceof Patient) {
            $rows = array(
                'PID'                => $this->patient->id . ' ',
                'HCN'                => $this->patient->getHCN() . ' ' . $this->patient->getVersionCode(),
                'Name'               => $this->patient->lname . ', ' . $this->patient->fname . ' ',
                'Date of Birth'      => $this->patient->dob ? date($this->date_format, hype::parseDateToInt($this->patient->dob, hype::DB_ISO_DATE)) . '  [' . $this->patient->getAge() . ']' : ' ',
                'Sex'                => $this->patient->sex . ' ',
                'Address'            => $this->patient->address . ' ',
                'City'               => $this->patient->city . ', '. $this->patient->province . ' ' . $this->patient->postal_code . ' ',
                'Home Phone'         => $this->patient->getDisplayHphone() . ' ',
                'Work Phone'         => $this->patient->getDisplayWphone() . ' ',
                'Referring Doctor'   => $this->patient->ref_doc_num . ' ',
                'Family Doctor'      => $this->patient->fam_doc_num . ' ',
                'HCV Status'         => $this->patient->getHCNLastStatus(),
            );
        }


        $tbl = '<style>' . file_get_contents('css/bootstrap.css') . '</style>';
        $tbl .= '<style>' . file_get_contents('css/bootstrap-theme.css') . '</style>';
        $tbl .= '<style>
                    .table {
                        border-radius: 5px;
                        width: 50%;
                    }
                    </style>';


        $tbl .= "<h1 class=\"text-left\" style=\"color: black\">General Patient Information</h1> <b>" . date($this->datetime_format) . ", solution designed by Hype Systems</b>";

        //$tbl .= "<div class=\"container\">";
        $tbl .= "<table class=\"table table-condensed \">";
        $tbl .= "
                <tbody>";

 		if ($this->patient->ref_doc_id) {
 			$rows['Referring Doctor'] = $this->patient->ReferringDoctor->getAutocomplete();
 		}
 		if ($this->patient->fam_doc_id) {
 			$rows['Family Doctor'] = $this->patient->FamilyDoctor->getAutocomplete();
 		}

 		//$this->SetFont('dejavusans', '', $this->font_size);
 		foreach ($rows as $key => $value)
 		{
            $tbl .= "<tr>
                        <td><b>" . $key . "</b></td>
                        <td>" . $value . "</td>
                     </tr>";
 		}


        $tbl .= "</tbody></table>";
        $tbl .= "<table class=\"table table-condensed\">";
        $tbl .= "<thead>
                  <tr>
                    <th>Note</th>
                  </tr>
                </thead>";
        $tbl .= "<tbody>";

        $notes = Doctrine_Query::create()
                    ->from('Notes n')
                    ->addWhere('n.client_id = (?)', $this->patient->getClientId())
                    ->addWhere('n.patient_id = (?)', $this->patient->id)
                    ->execute();


        foreach ($notes as $note) {

            if ($note instanceof Notes) {
                $tbl .= "<tr>
                        <td>" . $note->getNote() . "</td>
                     </tr>";
            }
        }

        $tbl .= "</tbody></table>";

        //$tbl .= "</div>";


        $this->loadHtml($tbl);
        $this->render();

        //return $this;

        //$this->writeHTML($tbl, true, false, false, false, '');

 		//$this->Ln();
/*
 		$this->MultiCell(0, 0, 'NOTES', '', 'L', 0, 1, '', '', true, 0);
 		foreach ($this->patient->Notes as $note)
 		{
            $this->Ln();
            $this->Cell(0, 0, $note->getNote(), 0, 1, 'C');
            $this->Ln();
 			//todo
 		}
*/
 	}

 	public function Footer()
 	{
        /*
 		$this->SetY(-15);
 		if ($this->footer_text) {
 			$this->Cell(.5, 10, $this->footer_text, 0, false, 'L', 0, '', 0, false, 'T', 'M');
 		}
 		$this->Cell(0, 10, 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
 		$this->Cell(0, 10, date($this->datetime_format), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        */
 	}
}

