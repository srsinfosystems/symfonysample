<?php

class hype {

    const DB_DATETIME_FORMAT = 'Y-m-d H:i:s.u';
    const DB_DATE_FORMAT = 'Y-m-d';
    const DB_ISO_DATE = 'Y-m-d H:i:s';
    const DB_TIMESTAMP = 'TIMESTAMP';
    const MODULE_APPT_BOOK = 'appointment_book';
    const MODULE_PATIENT_CLAIM = 'patient_profile_ohip';
    const MODULE_DOCTOR_SCHEDULE = 'doctor_scheduling';
    const MODULE_MULTI_LOCATION = 'multi_location';
    const MODULE_HL7 = 'hl7';
    const MODULE_EDT = 'edt';
    const MODULE_MCEDT = 'mcedt';
    const MODULE_HCV = 'hcv';
    const MODULE_MOBILE_APP = 'mobile_app';
    const MODULE_CONSULTATION = 'consultation';
    const MODULE_APPT_REVIEW = 'appointment_review';
    const MODULE_MYDBR_REPORTS = 'mydbr_reporting';
    const MODULE_MAPPING = 'mapping';
    const MODULE_DASHBOARD_WORKFLOW = 'dashboard_workflow';
    const MODULE_REBILLING = 'rebilling';
    const MODULE_MESSAGES = 'messages';
    const MODULE_REPLACEDATA = 'replacedata';
    const MODULE_EMAIL_APPOINTMENT_REMINDERS = 'email_appointment_reminders';
    const PERMISSION_APPOINTMENT_STAGES = 1;
    const PERMISSION_NAME_APPOINTMENT_STAGES = 'AppointmentStages';
    const PERMISSION_MOD_APPOINTMENT_STAGES = hype::MODULE_APPT_BOOK;
    const PERMISSION_DESC_APPOINTMENT_STAGES = 'Manage How Appointments Move Through From Booked to Completed';
    const PERMISSION_APPOINTMENT_STAGE_TRACK = 2;
    const PERMISSION_NAME_APPOINTMENT_STAGE_TRACK = 'AppointmentStageTrack';
    const PERMISSION_MOD_APPOINTMENT_STAGE_TRACK = hype::MODULE_APPT_BOOK;
    const PERMISSION_DESC_APPOINTMENT_STAGE_TRACK = 'Access The Appointment Stage Tracking Report';
    const PERMISSION_APPOINTMENT_TYPE = 3;
    const PERMISSION_NAME_APPOINTMENT_TYPE = 'AppointmentType';
    const PERMISSION_MOD_APPOINTMENT_TYPE = hype::MODULE_APPT_BOOK;
    const PERMISSION_DESC_APPOINTMENT_TYPE = 'Manage Appointment Types';
    const PERMISSION_CALL_CENTRE_VIEW = 4;
    const PERMISSION_NAME_CALL_CENTRE_VIEW = 'CallCentreView';
    const PERMISSION_MOD_CALL_CENTRE_VIEW = hype::MODULE_CONSULTATION;
    const PERMISSION_DESC_CALL_CENTRE_VIEW = 'Only See Consultations In The Appointment Book';
    const PERMISSION_CLAIM_CREATE = 5;
    const PERMISSION_NAME_CLAIM_CREATE = 'ClaimCreate';
    const PERMISSION_MOD_CLAIM_CREATE = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_CLAIM_CREATE = 'Create New Claims';
    const PERMISSION_CLAIM_DELETE = 6;
    const PERMISSION_NAME_CLAIM_DELETE = 'ClaimDelete';
    const PERMISSION_MOD_CLAIM_DELETE = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_CLAIM_DELETE = 'Delete Existing Claims [Claim must be in UNSUBMITTED status]';
    const PERMISSION_CLAIM_DETAIL_EDIT = 7;
    const PERMISSION_NAME_CLAIM_DETAIL_EDIT = 'ClaimDetailEdit';
    const PERMISSION_MOD_CLAIM_DETAIL_EDIT = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_CLAIM_DETAIL_EDIT = 'Edit Existing Claim Details';
    const PERMISSION_CLAIM_RESUBMIT = 39;
    const PERMISSION_NAME_CLAIM_RESUBMIT = 'ClaimResubmit';
    const PERMISSION_MOD_CLAIM_RESUBMIT = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_CLAIM_RESUBMIT = 'Re-Submit Claims (Reverse submission to make claims editable again)';
    const PERMISSION_BATCH_REVERSE = 40;
    const PERMISSION_NAME_BATCH_REVERSE = 'BatchReverse';
    const PERMISSION_MOD_BATCH_REVERSE = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_BATCH_REVERSE = 'Reverse A Batch';
    const PERMISSION_CLAIM_RECONCILE = 8;
    const PERMISSION_NAME_CLAIM_RECONCILE = 'ClaimReconcile';
    const PERMISSION_MOD_CLAIM_RECONCILE = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_CLAIM_RECONCILE = 'Reconcile Claims from the Ministry of Health';
    const PERMISSION_CLAIM_SUBMIT = 9;
    const PERMISSION_NAME_CLAIM_SUBMIT = 'ClaimSubmit';
    const PERMISSION_MOD_CLAIM_SUBMIT = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_CLAIM_SUBMIT = 'Submit Claims to the Ministry of Health';
    const PERMISSION_CLIENT_PARAM_EDIT = 10;
    const PERMISSION_NAME_CLIENT_PARAM_EDIT = 'ClientParamEdit';
    const PERMISSION_MOD_CLIENT_PARAM_EDIT = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_CLIENT_PARAM_EDIT = 'Manage System Options';
    const PERMISSION_CONSULTATION_ADMIN = 11;
    const PERMISSION_NAME_CONSULTATION_ADMIN = 'ConsultationAdmin';
    const PERMISSION_MOD_CONSULTATION_ADMIN = hype::MODULE_CONSULTATION;
    const PERMISSION_DESC_CONSULTATION_ADMIN = 'Manage Consultation Templates and Calendar Appointments';
    const PERMISSION_CONSULTATION_EDIT = 12;
    const PERMISSION_NAME_CONSULTATION_EDIT = 'ConsultationEdit';
    const PERMISSION_MOD_CONSULTATION_EDIT = hype::MODULE_CONSULTATION;
    const PERMISSION_DESC_CONSULTATION_EDIT = '';
    const PERMISSION_CREATE_PATIENT = 13;
    const PERMISSION_NAME_CREATE_PATIENT = 'CreatePatient';
    const PERMISSION_MOD_CREATE_PATIENT = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_CREATE_PATIENT = 'Create new Patients';
    const PERMISSION_DOCTOR_SCHEDULE = 14;
    const PERMISSION_NAME_DOCTOR_SCHEDULE = 'DoctorSchedule';
    const PERMISSION_MOD_DOCTOR_SCHEDULE = hype::MODULE_DOCTOR_SCHEDULE;
    const PERMISSION_DESC_DOCTOR_SCHEDULE = 'Access the Doctor Scheduling module';
    const PERMISSION_DOCTOR_TEMPLATE = 15;
    const PERMISSION_NAME_DOCTOR_TEMPLATE = 'DoctorTemplate';
    const PERMISSION_MOD_DOCTOR_TEMPLATE = hype::MODULE_DOCTOR_SCHEDULE;
    const PERMISSION_DESC_DOCTOR_TEMPLATE = 'Manage Doctor Scheduling Templates';
    const PERMISSION_GONET_EDIT = 16;
    const PERMISSION_NAME_GONET_EDIT = 'GonetEdit';
    const PERMISSION_MOD_GONET_EDIT = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_GONET_EDIT = 'Manage Ministry of Health Data Submission Information';
    const PERMISSION_HOLIDAY_ADMIN = 17;
    const PERMISSION_NAME_HOLIDAY_ADMIN = 'HolidayAdmin';
    const PERMISSION_MOD_HOLIDAY_ADMIN = hype::MODULE_APPT_BOOK;
    const PERMISSION_DESC_HOLIDAY_ADMIN = 'Manage Holidays';
    const PERMISSION_MANAGE_LOCATIONS = 18;
    const PERMISSION_NAME_MANAGE_LOCATIONS = 'ManageClinics';
    const PERMISSION_MOD_MANAGE_LOCATIONS = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_MANAGE_LOCATIONS = 'Manage Company Locations/Clinics';
    const PERMISSION_MODEM_EDIT = 19;
    const PERMISSION_NAME_MODEM_EDIT = 'ModemEdit';
    const PERMISSION_MOD_MODEM_EDIT = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_MODEM_EDIT = 'Manage Modem details';
    const PERMISSION_PAST_CLAIMS_TOTAL = 22;
    const PERMISSION_NAME_PAST_CLAIMS_TOTAL = 'PastClaimsTotal';
    const PERMISSION_MOD_PAST_CLAIMS_TOTAL = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_PAST_CLAIMS_TOTAL = 'Allows the user to view the totals on Billing Cycle Reports.';
    const PERMISSION_PAST_CLAIM_VIEW = 23;
    const PERMISSION_NAME_PAST_CLAIM_VIEW = 'PastClaimView';
    const PERMISSION_MOD_PAST_CLAIM_VIEW = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_PAST_CLAIM_VIEW = 'Access the Billing Cycle Reports';
    const PERMISSION_PATIENT_STATUS = 24;
    const PERMISSION_NAME_PATIENT_STATUS = 'PatientStatus';
    const PERMISSION_MOD_PATIENT_STATUS = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_PATIENT_STATUS = 'Manage Patient Statuses';
    const PERMISSION_PROVIDER_EDIT = 25;
    const PERMISSION_NAME_PROVIDER_EDIT = 'ProviderEdit';
    const PERMISSION_MOD_PROVIDER_EDIT = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_PROVIDER_EDIT = 'Manage Doctors';
    const PERMISSION_QUICK_SERVICE_CODE_EDIT = 26;
    const PERMISSION_NAME_QUICK_SERVICE_CODE_EDIT = 'QuickServiceCodeEdit';
    const PERMISSION_MOD_QUICK_SERVICE_CODE_EDIT = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_QUICK_SERVICE_CODE_EDIT = 'Manage Quick Service Codes';
    const PERMISSION_REFERRING_DOCTOR_EDIT = 27;
    const PERMISSION_NAME_REFERRING_DOCTOR_EDIT = 'ReferringDoctorEdit';
    const PERMISSION_MOD_REFERRING_DOCTOR_EDIT = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_REFERRING_DOCTOR_EDIT = 'Manage Referring Doctors';
    const PERMISSION_RELEASE_NOTES = 28;
    const PERMISSION_NAME_RELEASE_NOTES = 'ReleaseNotes';
    const PERMISSION_MOD_RELEASE_NOTES = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_RELEASE_NOTES = 'View Release Notes';
    const PERMISSION_REPEATING_APPOINTMENT = 29;
    const PERMISSION_NAME_REPEATING_APPOINTMENT = 'RepeatingAppointment';
    const PERMISSION_MOD_REPEATING_APPOINTMENT = hype::MODULE_APPT_BOOK;
    const PERMISSION_DESC_REPEATING_APPOINTMENT = 'Access the Repeating Appointments interface';
    const PERMISSION_REVERSE_APPOINTMENT_STAGE = 30;
    const PERMISSION_NAME_REVERSE_APPOINTMENT_STAGE = 'ReverseAppointmentStage';
    const PERMISSION_MOD_REVERSE_APPOINTMENT_STAGE = hype::MODULE_APPT_BOOK;
    const PERMISSION_DESC_REVERSE_APPOINTMENT_STAGE = 'Reverse Appointment Stages';
    const PERMISSION_SETUP_CLIENT = 31;
    const PERMISSION_NAME_SETUP_CLIENT = 'SetupClient';
    const PERMISSION_MOD_SETUP_CLIENT = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_SETUP_CLIENT = 'Manage Clients';
    const PERMISSION_THIRD_PARTY_EDIT = 33;
    const PERMISSION_NAME_THIRD_PARTY_EDIT = 'ThirdPartyEdit';
    const PERMISSION_MOD_THIRD_PARTY_EDIT = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_THIRD_PARTY_EDIT = 'Manage Third Party Providers information';
    const PERMISSION_USER_PROFILE_EDIT = 34;
    const PERMISSION_NAME_USER_PROFILE_EDIT = 'UserProfileEdit';
    const PERMISSION_MOD_USER_PROFILE_EDIT = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_USER_PROFILE_EDIT = 'Manage User Profiles';
    const PERMISSION_USER_TYPE_PERMISSION_EDIT = 35;
    const PERMISSION_NAME_USER_TYPE_PERMISSION_EDIT = 'UserTypePermissionEdit';
    const PERMISSION_MOD_USER_TYPE_PERMISSION_EDIT = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_USER_TYPE_PERMISSION_EDIT = 'Manage User Types';
    const PERMISSION_VACATION_ADMIN = 36;
    const PERMISSION_NAME_VACATION_ADMIN = 'VacationAdmin';
    const PERMISSION_MOD_VACATION_ADMIN = hype::MODULE_DOCTOR_SCHEDULE;
    const PERMISSION_DESC_VACATION_ADMIN = 'Manage Doctor Vacations';
    const PERMISSION_VIEW_PATIENT_PROFILE = 37;
    const PERMISSION_NAME_VIEW_PATIENT_PROFILE = 'ViewPatientProfile';
    const PERMISSION_MOD_VIEW_PATIENT_PROFILE = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_VIEW_PATIENT_PROFILE = 'Edit & View Patient Profiles';
    const PERMISSION_DIRECT_SERVICE_CODE_EDIT = 38;
    const PERMISSION_NAME_DIRECT_SERVICE_CODE_EDIT = 'DirectServiceCode';
    const PERMISSION_MOD_DIRECT_SERVICE_CODE_EDIT = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_DIRECT_SERVICE_CODE_EDIT = 'Edit & Manage Direct Service Codes';
    const PERMISSION_APPOINTMENT_BOOK = 41;
    const PERMISSION_NAME_APPOINTMENT_BOOK = 'AppointmentBook';
    const PERMISSION_MOD_APPOINTMENT_BOOK = hype::MODULE_APPT_BOOK;
    const PERMISSION_DESC_APPOINTMENT_BOOK = 'Access The Appointment Book';
    const PERMISSION_HL7_PARTNER_ID = 42;
    const PERMISSION_NAME_HL7_PARTNER_ID = 'HL7PartnerID';
    const PERMISSION_MOD_HL7_PARTNER_ID = hype::MODULE_HL7;
    const PERMISSION_DESC_HL7_PARTNER_ID = 'Update HL7 Parter IDs';
    const PERMISSION_REMINDER_SETUP = 43;
    const PERMISSION_NAME_REMINDER_SETUP = 'ReminderSetup';
    const PERMISSION_MOD_REMINDER_SETUP = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_REMINDER_SETUP = 'Setup Automated Patient Reminder Emails and User Reports';
    const PERMISSION_CLAIM_MARK_AS_PAID = 44;
    const PERMISSION_NAME_CLAIM_MARK_AS_PAID = 'ClaimMarkAsPaid';
    const PERMISSION_MOD_CLAIM_MARK_AS_PAID = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_CLAIM_MARK_AS_PAID = 'Mark Claims as Paid or Closed';
    const PERMISSION_IMPORT_DAY_SHEET = 45;
    const PERMISSION_NAME_IMPORT_DAY_SHEET = 'ImportDaySheet';
    const PERMISSION_MOD_IMPORT_DAY_SHEET = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_IMPORT_DAY_SHEET = 'Import Day Sheet Into Hype Medical';
    const PERMISSION_HYPE_ADMINISTRATOR = 100;
    const PERMISSION_NAME_HYPE_ADMINISTRATOR = 'HypeAdministrator';
    const PERMISSION_MOD_HYPE_ADMINISTRATOR = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_HYPE_ADMINISTRATOR = 'Hype Systems Administrator user';
    const PERMISSION_HYPE_PARAMS = 101;
    const PERMISSION_NAME_HYPE_PARAMS = 'HypeParameters';
    const PERMISSION_MOD_HYPE_PARAMS = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_HYPE_PARAMS = 'System-Wide Parameters Administration';
    const PERMISSION_APPOINTMENT_REVIEW = 46;
    const PERMISSION_NAME_APPOINTMENT_REVIEW = 'AppointmentReview';
    const PERMISSION_MOD_APPOINTMENT_REVIEW = hype::MODULE_APPT_REVIEW;
    const PERMISSION_DESC_APPOINTMENT_REVIEW = 'Review And Approve Appointments Before Submission';
    const PERMISSION_EXECUTIVE_REPORTS = 47;
    const PERMISSION_NAME_EXECUTIVE_REPORTS = 'Executive Reports';
    const PERMISSION_MOD_EXECUTIVE_REPORTS = hype::MODULE_MYDBR_REPORTS;
    const PERMISSION_DESC_EXECUTIVE_REPORTS = 'View Executive-Level Reporting Tools';
    const PERMISSION_APPOINTMENT_BOOK_NOTES = 48;
    const PERMISSION_NAME_APPOINTMENT_BOOK_NOTES = 'Appointment Book Notes';
    const PERMISSION_MOD_APPOINTMENT_BOOK_NOTES = hype::MODULE_APPT_BOOK;
    const PERMISSION_DESC_APPOINTMENT_BOOK_NOTES = 'Create and Modify Appointment Book Notes';
    const PERMISSION_PATIENT_MERGE = 49;
    const PERMISSION_NAME_PATIENT_MERGE = 'Merge Patients';
    const PERMISSION_MOD_PATIENT_MERGE = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_PATIENT_MERGE = 'Allows a user to merge two Patient Profiles together.';
    const PERMISSION_OVERRIDE_CLAIM_SAVE = 50;
    const PERMISSION_NAME_OVERRIDE_CLAIM_SAVE = 'Override Claim Save';
    const PERMISSION_MOD_OVERRIDE_CLAIM_SAVE = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_OVERRIDE_CLAIM_SAVE = 'Allows users to save a claim despite some of the validation errors';
    const PERMISSION_HL7_REPORTS = 51;
    const PERMISSION_NAME_HL7_REPORTS = 'HL7 Reporting';
    const PERMISSION_MOD_HL7_REPORTS = hype::MODULE_HL7;
    const PERMISSION_DESC_HL7_REPORTS = 'View Dashboard and other Reports related to sending and receiving HL7 Messages';
    const PERMISSION_CLAIM_UPDATE_PAYMENT_AMOUNTS = 52;
    const PERMISSION_NAME_CLAIM_UPDATE_PAYMENT_AMOUNTS = 'Update Claim Payment Amounts';
    const PERMISSION_MOD_CLAIM_UPDATE_PAYMENT_AMOUNTS = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_CLAIM_UPDATE_PAYMENT_AMOUNTS = 'Manually update Claim Payment Amounts';
    const PERMISSION_FIX_ERROR_REPORT_PARSE_ERRORS = 53;
    const PERMISSION_NAME_FIX_ERROR_REPORT_PARSE_ERRORS = 'Fix Error Report Items';
    const PERMISSION_MOD_FIX_ERROR_REPORT_PARSE_ERRORS = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_FIX_ERROR_REPORT_PARSE_ERRORS = 'Manually update Parse Errors found in Error Report Files from OHIP';
    const PERMISSION_EXPORT_DATA = 54;
    const PERMISSION_NAME_EXPORT_DATA = 'Export Data';
    const PERMISSION_MOD_EXPORT_DATA = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_EXPORT_DATA = 'Allows users to request a .CSV export of entire table sets. Only give to extremely trusted sources [Patients, Claims, Appointment, Referring Doctors]';
    const PERMISSION_DIRECT_BILLING = 55;
    const PERMISSION_NAME_DIRECT_BILLING = 'Direct Billing';
    const PERMISSION_MOD_DIRECT_BILLING = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_DIRECT_BILLING = 'Allows a user to only create claims with direct billing options';
    const PERMISSION_MONTHLY_TOTALS = 56;
    const PERMISSION_NAME_MONTHLY_TOTALS = 'Monthly Totals';
    const PERMISSION_MOD_MONTHLY_TOTALS = hype::MODULE_PATIENT_CLAIM;
    const PERMISSION_DESC_MONTHLY_TOTALS = 'Allows a user to see monthly totals';
    const PERMISSION_MESSAGES = 57;
    const PERMISSION_NAME_MESSAGES = 'Messages';
    const PERMISSION_MOD_MESSAGES = hype::MODULE_MESSAGES;
    const PERMISSION_DESC_MESSAGES = 'Access The Messages';

    const PERMISSION_REPLACE_DOCTOR = 58;
    const PERMISSION_NAME_REPLACE_DOCTOR = 'ReplaceDoctor';
    const PERMISSION_MOD_REPLACE_DOCTOR = hype::MODULE_REPLACEDATA;
    const PERMISSION_DESC_REPLACE_DOCTOR = 'Claim Doctor Replace';

    const PERMISSION_REPLACE_SLI = 59;
    const PERMISSION_NAME_REPLACE_SLI = 'ReplaceSLI';
    const PERMISSION_MOD_REPLACE_SLI = hype::MODULE_REPLACEDATA;
    const PERMISSION_DESC_REPLACE_SLI = 'Claim SLI Replace';

    const PERMISSION_REPLACE_FACILITY = 60;
    const PERMISSION_NAME_REPLACE_FACILITY = 'ReplaceFacility';
    const PERMISSION_MOD_REPLACE_FACILITY = hype::MODULE_REPLACEDATA;
    const PERMISSION_DESC_REPLACE_FACILITY = 'Claim Facility Replace';

    const PERMISSION_REPLACE_ADMIT_DATE = 61;
    const PERMISSION_NAME_REPLACE_ADMIT_DATE = 'ReplaceAdmitDate';
    const PERMISSION_MOD_REPLACE_ADMIT_DATE = hype::MODULE_REPLACEDATA;
    const PERMISSION_DESC_REPLACE_ADMIT_DATE = 'Claim Admit Date Replace';

    const PERMISSION_REPLACE_LOCATION = 62;
    const PERMISSION_NAME_REPLACE_LOCATION = 'ReplaceLocation';
    const PERMISSION_MOD_REPLACE_LOCATION = hype::MODULE_REPLACEDATA;
    const PERMISSION_DESC_REPLACE_LOCATION = 'Claim Location Replace';

    public static function decryptHCN($text) {
        if (strpos(strtolower(PHP_OS), 'linux') === false) {
            return hype::decryptHCNWindows($text);
        } else {
            return hype::decryptHCNLinux($text);
        }
    }

    public static function decryptHCNWindows($text) {
        $crypt = new COM("Chilkat.Crypt2");

        $success = $crypt->UnlockComponent('EITANRCrypt_32Xrkq72SMJQ');
        if ($success != true) {
            exit;
        }
        $password = '!*owenalon20100714*!';

        $crypt->CryptAlgorithm = 'aes';
        $crypt->CipherMode = 'ecb';
        $crypt->KeyLength = 128;

        //  Generate a binary secret key from a password string of any length.  For 128-bit encryption, GenEncodedSecretKey
        //  generates the MD5 hash of the password and returns it in the encoded form requested.  The 2nd param can be
        //  "hex", "base64", "url", "quoted-printable", etc.
        $hexKey = $crypt->genEncodedSecretKey($password, 'hex');
        $crypt->SetEncodedKey($hexKey, 'hex');
        $crypt->EncodingMode = 'base64';

        //  Encrypt a string and return the binary encrypted data in a base-64 encoded string.
        return $crypt->decryptStringENC($text);
    }

    public static function decryptHCNLinux($text) {
        // require_once("chilkat_9_4_1.php");
        $crypt = new CkCrypt2();

        $success = $crypt->UnlockComponent('EITANRCrypt_32Xrkq72SMJQ');
        if ($success != true) {
            exit;
        }
        $password = '!*owenalon20100714*!';

        $crypt->put_CryptAlgorithm('aes');
        $crypt->put_CipherMode('ecb');
        $crypt->put_KeyLength(128);
        $crypt->put_PaddingScheme(0);
        $crypt->put_EncodingMode('hex');

        //  Generate a binary secret key from a password string of any length.  For 128-bit encryption, GenEncodedSecretKey
        //  generates the MD5 hash of the password and returns it in the encoded form requested.  The 2nd param can be
        //  "hex", "base64", "url", "quoted-printable", etc.
        $hexKey = $crypt->genEncodedSecretKey($password, 'hex');
        $crypt->SetEncodedKey($hexKey, 'hex');
        $crypt->put_EncodingMode('base64');

        //  Encrypt a string and return the binary encrypted data in a base-64 encoded string.
        return $crypt->decryptStringENC($text);
    }

    public static function encryptHCN($text) {
        if (strpos(strtolower(PHP_OS), 'linux') === false) {
            return hype::encryptHCNWindows($text);
        } else {
            return hype::encryptHCNLinux($text);
        }
    }

    public static function encryptHCNWindows($text) {
        $crypt = new COM("Chilkat.Crypt2");
        $success = $crypt->UnlockComponent('EITANRCrypt_32Xrkq72SMJQ');

        if ($success != true) {
            exit;
        }
        $password = '!*owenalon20100714*!';

        $crypt->CryptAlgorithm = 'aes';
        $crypt->CipherMode = 'ecb';
        $crypt->KeyLength = 128;
        $crypt->PaddingScheme = 0;
        $crypt->EncodingMode = 'hex';

        //  Generate a binary secret key from a password string of any length.  For 128-bit encryption, GenEncodedSecretKey
        //  generates the MD5 hash of the password and returns it in the encoded form requested.  The 2nd param can be
        //  "hex", "base64", "url", "quoted-printable", etc.
        $hexKey = $crypt->genEncodedSecretKey($password, 'hex');
        $crypt->SetEncodedKey($hexKey, 'hex');
        $crypt->EncodingMode = 'base64';

        //  Encrypt a string and return the binary encrypted data in a base-64 encoded string.
        return $crypt->encryptStringENC(trim($text));
    }

    public static function encryptHCNLinux($text) {
        $crypt = new CkCrypt2();
        $success = $crypt->UnlockComponent('EITANRCrypt_32Xrkq72SMJQ');

        if ($success != true) {
            exit;
        }
        $password = '!*owenalon20100714*!';

        $crypt->put_CryptAlgorithm('aes');
        $crypt->put_CipherMode('ecb');
        $crypt->put_KeyLength(128);
        $crypt->put_PaddingScheme(0);
        $crypt->put_EncodingMode('hex');

        //  Generate a binary secret key from a password string of any length.  For 128-bit encryption, GenEncodedSecretKey
        //  generates the MD5 hash of the password and returns it in the encoded form requested.  The 2nd param can be
        //  "hex", "base64", "url", "quoted-printable", etc.
        $hexKey = $crypt->genEncodedSecretKey($password, 'hex');
        $crypt->SetEncodedKey($hexKey, 'hex');
        $crypt->put_EncodingMode('base64');

        //  Encrypt a string and return the binary encrypted data in a base-64 encoded string.
        return $crypt->encryptStringENC(trim($text));
    }

    public static function isValidPermission($permission_id) {
        switch ($permission_id) {
            case self::PERMISSION_APPOINTMENT_BOOK:
            case self::PERMISSION_APPOINTMENT_STAGES:
            case self::PERMISSION_APPOINTMENT_STAGE_TRACK:
            case self::PERMISSION_APPOINTMENT_TYPE:
            case self::PERMISSION_CALL_CENTRE_VIEW:
            case self::PERMISSION_CLAIM_CREATE:
            case self::PERMISSION_CLAIM_DELETE:
            case self::PERMISSION_CLAIM_DETAIL_EDIT:
            case self::PERMISSION_CLAIM_RECONCILE:
            case self::PERMISSION_CLAIM_RESUBMIT:
            case self::PERMISSION_CLAIM_SUBMIT:
            case self::PERMISSION_CLIENT_PARAM_EDIT:
            case self::PERMISSION_CONSULTATION_ADMIN:
            case self::PERMISSION_CREATE_PATIENT:
            case self::PERMISSION_DOCTOR_SCHEDULE:
            case self::PERMISSION_DOCTOR_TEMPLATE:
            case self::PERMISSION_GONET_EDIT:
            case self::PERMISSION_HOLIDAY_ADMIN:
            case self::PERMISSION_MANAGE_LOCATIONS:
            case self::PERMISSION_MODEM_EDIT:
            case self::PERMISSION_PAST_CLAIMS_TOTAL:
            case self::PERMISSION_PAST_CLAIM_VIEW:
            case self::PERMISSION_PATIENT_STATUS:
            case self::PERMISSION_PROVIDER_EDIT:
            case self::PERMISSION_QUICK_SERVICE_CODE_EDIT:
            case self::PERMISSION_REFERRING_DOCTOR_EDIT:
            case self::PERMISSION_RELEASE_NOTES:
            case self::PERMISSION_REPEATING_APPOINTMENT:
            case self::PERMISSION_REVERSE_APPOINTMENT_STAGE:
            case self::PERMISSION_SETUP_CLIENT:
            case self::PERMISSION_THIRD_PARTY_EDIT:
            case self::PERMISSION_USER_PROFILE_EDIT:
            case self::PERMISSION_USER_TYPE_PERMISSION_EDIT:
            case self::PERMISSION_VACATION_ADMIN:
            case self::PERMISSION_VIEW_PATIENT_PROFILE:
            case self::PERMISSION_HYPE_ADMINISTRATOR:
            case self::PERMISSION_DIRECT_SERVICE_CODE_EDIT:
            case self::PERMISSION_BATCH_REVERSE:
            case self::PERMISSION_HL7_PARTNER_ID:
            case self::PERMISSION_REMINDER_SETUP:
            case self::PERMISSION_HYPE_PARAMS:
            case self::PERMISSION_CLAIM_MARK_AS_PAID:
            case self::PERMISSION_IMPORT_DAY_SHEET:
            case self::PERMISSION_APPOINTMENT_REVIEW:
            case self::PERMISSION_EXECUTIVE_REPORTS:
            case self::PERMISSION_APPOINTMENT_BOOK_NOTES:
            case self::PERMISSION_PATIENT_MERGE:
            case self::PERMISSION_OVERRIDE_CLAIM_SAVE:
            case self::PERMISSION_HL7_REPORTS:
            case self::PERMISSION_CLAIM_UPDATE_PAYMENT_AMOUNTS:
            case self::PERMISSION_FIX_ERROR_REPORT_PARSE_ERRORS:
            case self::PERMISSION_EXPORT_DATA:
            case self::PERMISSION_DIRECT_BILLING:
            case self::PERMISSION_MONTHLY_TOTALS:
            case self::PERMISSION_MESSAGES:
            case self::PERMISSION_REPLACE_DOCTOR:
            case self::PERMISSION_REPLACE_SLI:
            case self::PERMISSION_REPLACE_FACILITY:
            case self::PERMISSION_REPLACE_ADMIT_DATE:
            case self::PERMISSION_REPLACE_LOCATION:
                return true;
                break;
        }

        return false;
    }

    public static function getUserPermissionName($permission_id) {
        switch ($permission_id) {
            case hype::PERMISSION_OVERRIDE_CLAIM_SAVE:
                return hype::PERMISSION_NAME_OVERRIDE_CLAIM_SAVE;
            case hype::PERMISSION_APPOINTMENT_BOOK:
                return hype::PERMISSION_NAME_APPOINTMENT_BOOK;
            case hype::PERMISSION_APPOINTMENT_STAGES:
                return hype::PERMISSION_NAME_APPOINTMENT_STAGES;
            case hype::PERMISSION_APPOINTMENT_STAGE_TRACK:
                return hype::PERMISSION_NAME_APPOINTMENT_STAGE_TRACK;
            case hype::PERMISSION_APPOINTMENT_TYPE:
                return hype::PERMISSION_NAME_APPOINTMENT_TYPE;
            case hype::PERMISSION_CALL_CENTRE_VIEW:
                return hype::PERMISSION_NAME_CALL_CENTRE_VIEW;
            case hype::PERMISSION_CLAIM_CREATE:
                return hype::PERMISSION_NAME_CLAIM_CREATE;
            case hype::PERMISSION_CLAIM_DELETE:
                return hype::PERMISSION_NAME_CLAIM_DELETE;
            case hype::PERMISSION_CLAIM_DETAIL_EDIT:
                return hype::PERMISSION_NAME_CLAIM_DETAIL_EDIT;
            case hype::PERMISSION_CLAIM_RECONCILE:
                return hype::PERMISSION_NAME_CLAIM_RECONCILE;
            case hype::PERMISSION_CLAIM_RESUBMIT:
                return hype::PERMISSION_NAME_CLAIM_RESUBMIT;
            case hype::PERMISSION_CLAIM_SUBMIT:
                return hype::PERMISSION_NAME_CLAIM_SUBMIT;
            case hype::PERMISSION_CLIENT_PARAM_EDIT:
                return hype::PERMISSION_NAME_CLIENT_PARAM_EDIT;
            case hype::PERMISSION_CONSULTATION_ADMIN:
                return hype::PERMISSION_NAME_CONSULTATION_ADMIN;
            case hype::PERMISSION_CREATE_PATIENT:
                return hype::PERMISSION_NAME_CREATE_PATIENT;
            case hype::PERMISSION_DOCTOR_SCHEDULE:
                return hype::PERMISSION_NAME_DOCTOR_SCHEDULE;
            case hype::PERMISSION_DOCTOR_TEMPLATE:
                return hype::PERMISSION_NAME_DOCTOR_TEMPLATE;
            case hype::PERMISSION_GONET_EDIT:
                return hype::PERMISSION_NAME_GONET_EDIT;
            case hype::PERMISSION_HOLIDAY_ADMIN:
                return hype::PERMISSION_NAME_HOLIDAY_ADMIN;
            case hype::PERMISSION_MANAGE_LOCATIONS:
                return hype::PERMISSION_NAME_MANAGE_LOCATIONS;
            case hype::PERMISSION_MODEM_EDIT:
                return hype::PERMISSION_NAME_MODEM_EDIT;
            case hype::PERMISSION_PAST_CLAIMS_TOTAL:
                return hype::PERMISSION_NAME_PAST_CLAIMS_TOTAL;
            case hype::PERMISSION_PAST_CLAIM_VIEW:
                return hype::PERMISSION_NAME_PAST_CLAIM_VIEW;
            case hype::PERMISSION_PATIENT_STATUS:
                return hype::PERMISSION_NAME_PATIENT_STATUS;
            case hype::PERMISSION_PROVIDER_EDIT:
                return hype::PERMISSION_NAME_PROVIDER_EDIT;
            case hype::PERMISSION_QUICK_SERVICE_CODE_EDIT:
                return hype::PERMISSION_NAME_QUICK_SERVICE_CODE_EDIT;
            case hype::PERMISSION_REFERRING_DOCTOR_EDIT:
                return hype::PERMISSION_NAME_REFERRING_DOCTOR_EDIT;
            case hype::PERMISSION_RELEASE_NOTES:
                return hype::PERMISSION_NAME_RELEASE_NOTES;
            case hype::PERMISSION_REPEATING_APPOINTMENT:
                return hype::PERMISSION_NAME_REPEATING_APPOINTMENT;
            case hype::PERMISSION_REVERSE_APPOINTMENT_STAGE:
                return hype::PERMISSION_NAME_REVERSE_APPOINTMENT_STAGE;
            case hype::PERMISSION_SETUP_CLIENT:
                return hype::PERMISSION_NAME_SETUP_CLIENT;
            case hype::PERMISSION_THIRD_PARTY_EDIT:
                return hype::PERMISSION_NAME_THIRD_PARTY_EDIT;
            case hype::PERMISSION_USER_PROFILE_EDIT:
                return hype::PERMISSION_NAME_USER_PROFILE_EDIT;
            case hype::PERMISSION_USER_TYPE_PERMISSION_EDIT:
                return hype::PERMISSION_NAME_USER_TYPE_PERMISSION_EDIT;
            case hype::PERMISSION_VACATION_ADMIN:
                return hype::PERMISSION_NAME_VACATION_ADMIN;
            case hype::PERMISSION_VIEW_PATIENT_PROFILE:
                return hype::PERMISSION_NAME_VIEW_PATIENT_PROFILE;
            case hype::PERMISSION_DIRECT_SERVICE_CODE_EDIT:
                return hype::PERMISSION_NAME_DIRECT_SERVICE_CODE_EDIT;
            case hype::PERMISSION_HYPE_ADMINISTRATOR:
                return hype::PERMISSION_NAME_HYPE_ADMINISTRATOR;
            case hype::PERMISSION_BATCH_REVERSE:
                return hype::PERMISSION_NAME_BATCH_REVERSE;
            case hype::PERMISSION_HL7_PARTNER_ID:
                return hype::PERMISSION_NAME_HL7_PARTNER_ID;
            case hype::PERMISSION_REMINDER_SETUP:
                return hype::PERMISSION_NAME_REMINDER_SETUP;
            case hype::PERMISSION_HYPE_PARAMS:
                return hype::PERMISSION_NAME_HYPE_PARAMS;
            case hype::PERMISSION_CLAIM_MARK_AS_PAID:
                return hype::PERMISSION_NAME_CLAIM_MARK_AS_PAID;
            case hype::PERMISSION_IMPORT_DAY_SHEET:
                return hype::PERMISSION_NAME_IMPORT_DAY_SHEET;
            case hype::PERMISSION_APPOINTMENT_REVIEW:
                return hype::PERMISSION_NAME_APPOINTMENT_REVIEW;
            case hype::PERMISSION_EXECUTIVE_REPORTS:
                return hype::PERMISSION_NAME_EXECUTIVE_REPORTS;
            case hype::PERMISSION_APPOINTMENT_BOOK_NOTES:
                return hype::PERMISSION_NAME_APPOINTMENT_BOOK_NOTES;
            case hype::PERMISSION_PATIENT_MERGE:
                return hype::PERMISSION_NAME_PATIENT_MERGE;
            case hype::PERMISSION_HL7_REPORTS:
                return hype::PERMISSION_NAME_HL7_REPORTS;
            case hype::PERMISSION_CLAIM_UPDATE_PAYMENT_AMOUNTS:
                return hype::PERMISSION_NAME_CLAIM_UPDATE_PAYMENT_AMOUNTS;
            case hype::PERMISSION_FIX_ERROR_REPORT_PARSE_ERRORS:
                return hype::PERMISSION_NAME_FIX_ERROR_REPORT_PARSE_ERRORS;
            case hype::PERMISSION_EXPORT_DATA:
                return hype::PERMISSION_NAME_EXPORT_DATA;
            case hype::PERMISSION_DIRECT_BILLING:
                return hype::PERMISSION_NAME_DIRECT_BILLING;
            case hype::PERMISSION_MONTHLY_TOTALS:
                return hype::PERMISSION_NAME_MONTHLY_TOTALS;
            case hype::PERMISSION_MESSAGES:
                return hype::PERMISSION_NAME_MESSAGES;
            case hype::PERMISSION_REPLACE_DOCTOR:
                return hype::PERMISSION_NAME_REPLACE_DOCTOR;
            case hype::PERMISSION_REPLACE_SLI:
                return hype::PERMISSION_NAME_REPLACE_SLI;
            case hype::PERMISSION_REPLACE_ADMIT_DATE:
                return hype::PERMISSION_NAME_REPLACE_ADMIT_DATE;
            case hype::PERMISSION_REPLACE_FACILITY:
                return hype::PERMISSION_NAME_REPLACE_FACILITY;
            case hype::PERMISSION_REPLACE_LOCATION:
                return hype::PERMISSION_NAME_REPLACE_LOCATION;
            default:
                return $permission_id;
        }
    }

    public static function getUserPermissionConstantFromName($permission_name) {
        switch ($permission_name) {
            case hype::PERMISSION_NAME_OVERRIDE_CLAIM_SAVE:
                return hype::PERMISSION_OVERRIDE_CLAIM_SAVE;
            case hype::PERMISSION_NAME_APPOINTMENT_BOOK:
                return hype::PERMISSION_APPOINTMENT_BOOK;
            case hype::PERMISSION_NAME_APPOINTMENT_STAGES:
                return hype::PERMISSION_APPOINTMENT_STAGES;
            case hype::PERMISSION_NAME_APPOINTMENT_STAGE_TRACK:
                return hype::PERMISSION_APPOINTMENT_STAGE_TRACK;
            case hype::PERMISSION_NAME_APPOINTMENT_TYPE:
                return hype::PERMISSION_APPOINTMENT_TYPE;
            case hype::PERMISSION_NAME_CALL_CENTRE_VIEW:
                return hype::PERMISSION_CALL_CENTRE_VIEW;
            case hype::PERMISSION_NAME_CLAIM_CREATE:
                return hype::PERMISSION_CLAIM_CREATE;
            case hype::PERMISSION_NAME_CLAIM_DELETE:
                return hype::PERMISSION_CLAIM_DELETE;
            case hype::PERMISSION_NAME_CLAIM_DETAIL_EDIT:
                return hype::PERMISSION_CLAIM_DETAIL_EDIT;
            case hype::PERMISSION_NAME_CLAIM_RECONCILE:
                return hype::PERMISSION_CLAIM_RECONCILE;
            case hype::PERMISSION_NAME_CLAIM_RESUBMIT:
                return hype::PERMISSION_CLAIM_RESUBMIT;
            case hype::PERMISSION_NAME_CLAIM_SUBMIT:
                return hype::PERMISSION_CLAIM_SUBMIT;
            case hype::PERMISSION_NAME_CLIENT_PARAM_EDIT:
                return hype::PERMISSION_CLIENT_PARAM_EDIT;
            case hype::PERMISSION_NAME_CONSULTATION_ADMIN:
                return hype::PERMISSION_CONSULTATION_ADMIN;
            case hype::PERMISSION_NAME_CONSULTATION_EDIT:
                return hype::PERMISSION_CONSULTATION_EDIT;
            case hype::PERMISSION_NAME_CREATE_PATIENT:
                return hype::PERMISSION_CREATE_PATIENT;
            case hype::PERMISSION_NAME_DOCTOR_SCHEDULE:
                return hype::PERMISSION_DOCTOR_SCHEDULE;
            case hype::PERMISSION_NAME_DOCTOR_TEMPLATE:
                return hype::PERMISSION_DOCTOR_TEMPLATE;
            case hype::PERMISSION_NAME_GONET_EDIT:
                return hype::PERMISSION_GONET_EDIT;
            case hype::PERMISSION_NAME_HOLIDAY_ADMIN:
                return hype::PERMISSION_HOLIDAY_ADMIN;
            case hype::PERMISSION_NAME_MANAGE_LOCATIONS:
                return hype::PERMISSION_MANAGE_LOCATIONS;
            case hype::PERMISSION_NAME_MODEM_EDIT:
                return hype::PERMISSION_MODEM_EDIT;
            case hype::PERMISSION_NAME_PAST_CLAIMS_TOTAL:
                return hype::PERMISSION_PAST_CLAIMS_TOTAL;
            case hype::PERMISSION_NAME_PAST_CLAIM_VIEW:
                return hype::PERMISSION_PAST_CLAIM_VIEW;
            case hype::PERMISSION_NAME_PATIENT_STATUS:
                return hype::PERMISSION_PATIENT_STATUS;
            case hype::PERMISSION_NAME_PROVIDER_EDIT:
                return hype::PERMISSION_PROVIDER_EDIT;
            case hype::PERMISSION_NAME_QUICK_SERVICE_CODE_EDIT:
                return hype::PERMISSION_QUICK_SERVICE_CODE_EDIT;
            case hype::PERMISSION_NAME_REFERRING_DOCTOR_EDIT:
                return hype::PERMISSION_REFERRING_DOCTOR_EDIT;
            case hype::PERMISSION_NAME_RELEASE_NOTES:
                return hype::PERMISSION_RELEASE_NOTES;
            case hype::PERMISSION_NAME_REPEATING_APPOINTMENT:
                return hype::PERMISSION_REPEATING_APPOINTMENT;
            case hype::PERMISSION_NAME_REVERSE_APPOINTMENT_STAGE:
                return hype::PERMISSION_REVERSE_APPOINTMENT_STAGE;
            case hype::PERMISSION_NAME_SETUP_CLIENT:
                return hype::PERMISSION_SETUP_CLIENT;
            case hype::PERMISSION_NAME_THIRD_PARTY_EDIT:
                return hype::PERMISSION_THIRD_PARTY_EDIT;
            case hype::PERMISSION_NAME_USER_PROFILE_EDIT:
                return hype::PERMISSION_USER_PROFILE_EDIT;
            case hype::PERMISSION_NAME_USER_TYPE_PERMISSION_EDIT:
                return hype::PERMISSION_USER_TYPE_PERMISSION_EDIT;
            case hype::PERMISSION_NAME_VACATION_ADMIN:
                return hype::PERMISSION_VACATION_ADMIN;
            case hype::PERMISSION_NAME_VIEW_PATIENT_PROFILE:
                return hype::PERMISSION_VIEW_PATIENT_PROFILE;
            case hype::PERMISSION_NAME_DIRECT_SERVICE_CODE_EDIT:
                return hype::PERMISSION_DIRECT_SERVICE_CODE_EDIT;
            case hype::PERMISSION_NAME_HYPE_ADMINISTRATOR:
                return hype::PERMISSION_HYPE_ADMINISTRATOR;
            case hype::PERMISSION_NAME_BATCH_REVERSE:
                return hype::PERMISSION_BATCH_REVERSE;
            case hype::PERMISSION_NAME_HL7_PARTNER_ID:
                return hype::PERMISSION_HL7_PARTNER_ID;
            case hype::PERMISSION_NAME_REMINDER_SETUP:
                return hype::PERMISSION_REMINDER_SETUP;
            case hype::PERMISSION_NAME_HYPE_PARAMS:
                return hype::PERMISSION_HYPE_PARAMS;
            case hype::PERMISSION_NAME_CLAIM_MARK_AS_PAID:
                return hype::PERMISSION_CLAIM_MARK_AS_PAID;
            case hype::PERMISSION_NAME_IMPORT_DAY_SHEET:
                return hype::PERMISSION_IMPORT_DAY_SHEET;
            case hype::PERMISSION_NAME_APPOINTMENT_REVIEW:
                return hype::PERMISSION_APPOINTMENT_REVIEW;
            case hype::PERMISSION_NAME_EXECUTIVE_REPORTS:
                return hype::PERMISSION_EXECUTIVE_REPORTS;
            case hype::PERMISSION_NAME_APPOINTMENT_BOOK_NOTES:
                return hype::PERMISSION_APPOINTMENT_BOOK_NOTES;
            case hype::PERMISSION_NAME_PATIENT_MERGE:
                return hype::PERMISSION_PATIENT_MERGE;
            case hype::PERMISSION_NAME_HL7_REPORTS:
                return hype::PERMISSION_HL7_REPORTS;
            case hype::PERMISSION_NAME_CLAIM_UPDATE_PAYMENT_AMOUNTS:
                return hype::PERMISSION_CLAIM_UPDATE_PAYMENT_AMOUNTS;
            case hype::PERMISSION_NAME_FIX_ERROR_REPORT_PARSE_ERRORS:
                return hype::PERMISSION_FIX_ERROR_REPORT_PARSE_ERRORS;
            case hype::PERMISSION_NAME_EXPORT_DATA:
                return hype::PERMISSION_EXPORT_DATA;
            case hype::PERMISSION_NAME_DIRECT_BILLING:
                return hype::PERMISSION_DIRECT_BILLING;
            case hype::PERMISSION_NAME_MONTHLY_TOTALS:
                return hype::PERMISSION_MONTHLY_TOTALS;
            case hype::PERMISSION_NAME_MESSAGES:
                return hype::PERMISSION_MESSAGES;
            case hype::PERMISSION_NAME_REPLACE_DOCTOR:
                return hype::PERMISSION_REPLACE_DOCTOR;

            case hype::PERMISSION_NAME_REPLACE_SLI:
                return hype::PERMISSION_REPLACE_SLI;
            case hype::PERMISSION_NAME_REPLACE_FACILITY:
                return hype::PERMISSION_REPLACE_FACILITY;
            case hype::PERMISSION_NAME_REPLACE_ADMIT_DATE:
                return hype::PERMISSION_REPLACE_ADMIT_DATE;
            case hype::PERMISSION_NAME_REPLACE_LOCATION:
                return hype::PERMISSION_REPLACE_LOCATION;
            default:
                return $permission_name;
        }
    }

    public static function getUserPermissionDescription($permission_id, $prefix) {
        switch ($permission_id) {
            case hype::PERMISSION_APPOINTMENT_BOOK:
                return $prefix . hype::PERMISSION_DESC_APPOINTMENT_BOOK;
            case hype::PERMISSION_APPOINTMENT_STAGES:
                return $prefix . hype::PERMISSION_DESC_APPOINTMENT_STAGES;
            case hype::PERMISSION_APPOINTMENT_STAGE_TRACK:
                return $prefix . hype::PERMISSION_DESC_APPOINTMENT_STAGE_TRACK;
            case hype::PERMISSION_APPOINTMENT_TYPE:
                return $prefix . hype::PERMISSION_DESC_APPOINTMENT_TYPE;
            case hype::PERMISSION_CALL_CENTRE_VIEW:
                return $prefix . hype::PERMISSION_DESC_CALL_CENTRE_VIEW;
            case hype::PERMISSION_CLAIM_CREATE:
                return $prefix . hype::PERMISSION_DESC_CLAIM_CREATE;
            case hype::PERMISSION_CLAIM_DELETE:
                return $prefix . hype::PERMISSION_DESC_CLAIM_DELETE;
            case hype::PERMISSION_CLAIM_DETAIL_EDIT:
                return $prefix . hype::PERMISSION_DESC_CLAIM_DETAIL_EDIT;
            case hype::PERMISSION_CLAIM_RECONCILE:
                return $prefix . hype::PERMISSION_DESC_CLAIM_RECONCILE;
            case hype::PERMISSION_CLAIM_RESUBMIT:
                return $prefix . hype::PERMISSION_DESC_CLAIM_RESUBMIT;
            case hype::PERMISSION_CLAIM_SUBMIT:
                return $prefix . hype::PERMISSION_DESC_CLAIM_SUBMIT;
            case hype::PERMISSION_CLIENT_PARAM_EDIT:
                return $prefix . hype::PERMISSION_DESC_CLIENT_PARAM_EDIT;
            case hype::PERMISSION_CONSULTATION_ADMIN:
                return $prefix . hype::PERMISSION_DESC_CONSULTATION_ADMIN;
            case hype::PERMISSION_CREATE_PATIENT:
                return $prefix . hype::PERMISSION_DESC_CREATE_PATIENT;
            case hype::PERMISSION_DOCTOR_SCHEDULE:
                return $prefix . hype::PERMISSION_NAME_DOCTOR_SCHEDULE;
            case hype::PERMISSION_DOCTOR_TEMPLATE:
                return $prefix . hype::PERMISSION_DESC_DOCTOR_TEMPLATE;
            case hype::PERMISSION_GONET_EDIT:
                return $prefix . hype::PERMISSION_DESC_GONET_EDIT;
            case hype::PERMISSION_HOLIDAY_ADMIN:
                return $prefix . hype::PERMISSION_DESC_HOLIDAY_ADMIN;
            case hype::PERMISSION_MANAGE_LOCATIONS:
                return $prefix . hype::PERMISSION_DESC_MANAGE_LOCATIONS;
            case hype::PERMISSION_MODEM_EDIT:
                return $prefix . hype::PERMISSION_DESC_MODEM_EDIT;
            case hype::PERMISSION_PAST_CLAIMS_TOTAL:
                return $prefix . hype::PERMISSION_DESC_PAST_CLAIMS_TOTAL;
            case hype::PERMISSION_PAST_CLAIM_VIEW:
                return $prefix . hype::PERMISSION_DESC_PAST_CLAIM_VIEW;
            case hype::PERMISSION_PATIENT_STATUS:
                return $prefix . hype::PERMISSION_DESC_PATIENT_STATUS;
            case hype::PERMISSION_PROVIDER_EDIT:
                return $prefix . hype::PERMISSION_DESC_PROVIDER_EDIT;
            case hype::PERMISSION_QUICK_SERVICE_CODE_EDIT:
                return $prefix . hype::PERMISSION_DESC_QUICK_SERVICE_CODE_EDIT;
            case hype::PERMISSION_REFERRING_DOCTOR_EDIT:
                return $prefix . hype::PERMISSION_DESC_REFERRING_DOCTOR_EDIT;
            case hype::PERMISSION_RELEASE_NOTES:
                return $prefix . hype::PERMISSION_DESC_RELEASE_NOTES;
            case hype::PERMISSION_REPEATING_APPOINTMENT:
                return $prefix . hype::PERMISSION_DESC_REPEATING_APPOINTMENT;
            case hype::PERMISSION_REVERSE_APPOINTMENT_STAGE:
                return $prefix . hype::PERMISSION_DESC_REVERSE_APPOINTMENT_STAGE;
            case hype::PERMISSION_SETUP_CLIENT:
                return $prefix . hype::PERMISSION_DESC_SETUP_CLIENT;
            case hype::PERMISSION_THIRD_PARTY_EDIT:
                return $prefix . hype::PERMISSION_DESC_THIRD_PARTY_EDIT;
            case hype::PERMISSION_USER_PROFILE_EDIT:
                return $prefix . hype::PERMISSION_DESC_USER_PROFILE_EDIT;
            case hype::PERMISSION_USER_TYPE_PERMISSION_EDIT:
                return $prefix . hype::PERMISSION_DESC_USER_TYPE_PERMISSION_EDIT;
            case hype::PERMISSION_VACATION_ADMIN:
                return $prefix . hype::PERMISSION_DESC_VACATION_ADMIN;
            case hype::PERMISSION_VIEW_PATIENT_PROFILE:
                return $prefix . hype::PERMISSION_DESC_VIEW_PATIENT_PROFILE;
            case hype::PERMISSION_DIRECT_SERVICE_CODE_EDIT:
                return $prefix . hype::PERMISSION_DESC_DIRECT_SERVICE_CODE_EDIT;
            case hype::PERMISSION_HYPE_ADMINISTRATOR:
                return $prefix . hype::PERMISSION_DESC_HYPE_ADMINISTRATOR;
            case hype::PERMISSION_BATCH_REVERSE:
                return $prefix . hype::PERMISSION_DESC_BATCH_REVERSE;
            case hype::PERMISSION_HL7_PARTNER_ID:
                return $prefix . hype::PERMISSION_DESC_HL7_PARTNER_ID;
            case hype::PERMISSION_REMINDER_SETUP:
                return $prefix . hype::PERMISSION_DESC_REMINDER_SETUP;
            case hype::PERMISSION_HYPE_PARAMS:
                return $prefix . hype::PERMISSION_DESC_HYPE_PARAMS;
            case hype::PERMISSION_CLAIM_MARK_AS_PAID:
                return $prefix . hype::PERMISSION_DESC_CLAIM_MARK_AS_PAID;
            case hype::PERMISSION_IMPORT_DAY_SHEET:
                return $prefix . hype::PERMISSION_DESC_IMPORT_DAY_SHEET;
            case hype::PERMISSION_APPOINTMENT_REVIEW:
                return $prefix . hype::PERMISSION_DESC_APPOINTMENT_REVIEW;
            case hype::PERMISSION_EXECUTIVE_REPORTS:
                return $prefix . hype::PERMISSION_DESC_EXECUTIVE_REPORTS;
            case hype::PERMISSION_APPOINTMENT_BOOK_NOTES:
                return $prefix . hype::PERMISSION_DESC_APPOINTMENT_BOOK_NOTES;
            case hype::PERMISSION_PATIENT_MERGE:
                return $prefix . hype::PERMISSION_DESC_PATIENT_MERGE;
            case hype::PERMISSION_OVERRIDE_CLAIM_SAVE:
                return $prefix . hype::PERMISSION_DESC_OVERRIDE_CLAIM_SAVE;
            case hype::PERMISSION_HL7_REPORTS:
                return $prefix . hype::PERMISSION_DESC_HL7_REPORTS;
            case hype::PERMISSION_CLAIM_UPDATE_PAYMENT_AMOUNTS:
                return $prefix . hype::PERMISSION_DESC_CLAIM_UPDATE_PAYMENT_AMOUNTS;
            case hype::PERMISSION_FIX_ERROR_REPORT_PARSE_ERRORS:
                return $prefix . hype::PERMISSION_DESC_FIX_ERROR_REPORT_PARSE_ERRORS;
            case hype::PERMISSION_EXPORT_DATA:
                return $prefix . hype::PERMISSION_DESC_EXPORT_DATA;
            case hype::PERMISSION_DIRECT_BILLING:
                return $prefix . hype::PERMISSION_DESC_DIRECT_BILLING;
            case hype::PERMISSION_MONTHLY_TOTALS:
                return $prefix . hype::PERMISSION_DESC_MONTHLY_TOTALS;
            case hype::PERMISSION_MESSAGES:
                return $prefix . hype::PERMISSION_DESC_MESSAGES;
            case hype::PERMISSION_REPLACE_DOCTOR:
                return $prefix . hype::PERMISSION_DESC_REPLACE_DOCTOR;
             case hype::PERMISSION_REPLACE_SLI:
                return $prefix . hype::PERMISSION_DESC_REPLACE_SLI;
             case hype::PERMISSION_REPLACE_FACILITY:
                return $prefix . hype::PERMISSION_DESC_REPLACE_FACILITY;
             case hype::PERMISSION_REPLACE_ADMIT_DATE:
                return $prefix . hype::PERMISSION_DESC_REPLACE_ADMIT_DATE;
             case hype::PERMISSION_REPLACE_LOCATION:
                return $prefix . hype::PERMISSION_DESC_REPLACE_LOCATION;
            default:
                var_dump($permission_id);
//				exit();
                return '';
        }
    }

    public static function getUserPermissionCategory($permission_id) {
        switch ($permission_id) {
            case hype::PERMISSION_APPOINTMENT_BOOK:
            case hype::PERMISSION_APPOINTMENT_STAGES:
            case hype::PERMISSION_APPOINTMENT_STAGE_TRACK:
            case hype::PERMISSION_APPOINTMENT_TYPE:
            case hype::PERMISSION_REPEATING_APPOINTMENT:
            case hype::PERMISSION_REVERSE_APPOINTMENT_STAGE:
            case hype::PERMISSION_APPOINTMENT_REVIEW:
            case hype::PERMISSION_APPOINTMENT_BOOK_NOTES:
                return 'Appointment Book';

            case hype::PERMISSION_CALL_CENTRE_VIEW:
            case hype::PERMISSION_CONSULTATION_ADMIN:
                return 'Consultation';

            case hype::PERMISSION_CLAIM_CREATE:
            case hype::PERMISSION_CLAIM_DELETE:
            case hype::PERMISSION_CLAIM_DETAIL_EDIT:
            case hype::PERMISSION_CLAIM_RECONCILE:
            case hype::PERMISSION_CLAIM_RESUBMIT:
            case hype::PERMISSION_CLAIM_SUBMIT:
            case hype::PERMISSION_PAST_CLAIMS_TOTAL:
            case hype::PERMISSION_PAST_CLAIM_VIEW:
            case hype::PERMISSION_BATCH_REVERSE:
            case hype::PERMISSION_CLAIM_MARK_AS_PAID:
            case hype::PERMISSION_EXECUTIVE_REPORTS:
            case hype::PERMISSION_OVERRIDE_CLAIM_SAVE:
            case hype::PERMISSION_CLAIM_UPDATE_PAYMENT_AMOUNTS:
            case hype::PERMISSION_FIX_ERROR_REPORT_PARSE_ERRORS:
            case hype::PERMISSION_DIRECT_BILLING:
            case hype::PERMISSION_MONTHLY_TOTALS:
                return 'Claims';

            case hype::PERMISSION_CLIENT_PARAM_EDIT:
            case hype::PERMISSION_GONET_EDIT:
            case hype::PERMISSION_MODEM_EDIT:
            case hype::PERMISSION_HOLIDAY_ADMIN:
            case hype::PERMISSION_MANAGE_LOCATIONS:
            case hype::PERMISSION_QUICK_SERVICE_CODE_EDIT:
            case hype::PERMISSION_REFERRING_DOCTOR_EDIT:
            case hype::PERMISSION_RELEASE_NOTES:
            case hype::PERMISSION_THIRD_PARTY_EDIT:
            case hype::PERMISSION_USER_PROFILE_EDIT:
            case hype::PERMISSION_USER_TYPE_PERMISSION_EDIT:
            case hype::PERMISSION_DIRECT_SERVICE_CODE_EDIT:
            case hype::PERMISSION_EXPORT_DATA:
                return 'System Administration';

            case hype::PERMISSION_VIEW_PATIENT_PROFILE:
            case hype::PERMISSION_CREATE_PATIENT:
            case hype::PERMISSION_PATIENT_STATUS:
            case hype::PERMISSION_REMINDER_SETUP:
            case hype::PERMISSION_IMPORT_DAY_SHEET:
            case hype::PERMISSION_PATIENT_MERGE:
                return 'Patient Profile';

            case hype::PERMISSION_PROVIDER_EDIT:
            case hype::PERMISSION_DOCTOR_SCHEDULE:
            case hype::PERMISSION_DOCTOR_TEMPLATE:
            case hype::PERMISSION_VACATION_ADMIN:
                return 'Doctors';

            case hype::PERMISSION_HL7_PARTNER_ID:
            case hype::PERMISSION_HL7_REPORTS:
                return 'HL7';

            case hype::PERMISSION_SETUP_CLIENT:
            case hype::PERMISSION_HYPE_ADMINISTRATOR:
            case hype::PERMISSION_HYPE_PARAMS:
                return 'Hype Administration';

            case hype::PERMISSION_MESSAGES:
                return 'Messages';

            case hype::PERMISSION_REPLACE_SLI:
            case hype::PERMISSION_REPLACE_FACILITY:
            case hype::PERMISSION_REPLACE_ADMIT_DATE:
            case hype::PERMISSION_REPLACE_LOCATION:
            case hype::PERMISSION_REPLACE_DOCTOR:
                return 'REPLACE DATA';
            default:
        }
        return 'MISC';
    }

    public static function getUserPermissionsByCategory($include_hype_only = false) {
        $rs = array();

        $rs['AppointmentBook'] = array(
            'name' => 'Appointment Book',
            'permissions' => array(
                hype::PERMISSION_APPOINTMENT_BOOK => hype::PERMISSION_NAME_APPOINTMENT_BOOK,
                hype::PERMISSION_APPOINTMENT_STAGES => hype::PERMISSION_NAME_APPOINTMENT_STAGES,
                hype::PERMISSION_APPOINTMENT_STAGE_TRACK => hype::PERMISSION_NAME_APPOINTMENT_STAGE_TRACK,
                hype::PERMISSION_APPOINTMENT_TYPE => hype::PERMISSION_NAME_APPOINTMENT_TYPE,
                hype::PERMISSION_REPEATING_APPOINTMENT => hype::PERMISSION_NAME_REPEATING_APPOINTMENT,
                hype::PERMISSION_REVERSE_APPOINTMENT_STAGE => hype::PERMISSION_NAME_REVERSE_APPOINTMENT_STAGE,
                hype::PERMISSION_APPOINTMENT_REVIEW => hype::PERMISSION_NAME_APPOINTMENT_REVIEW,
                hype::PERMISSION_APPOINTMENT_BOOK_NOTES => hype::PERMISSION_NAME_APPOINTMENT_BOOK_NOTES,
            )
        );

        $rs['Consultation'] = array(
            'name' => 'Consultation',
            'permissions' => array(
                hype::PERMISSION_CONSULTATION_ADMIN => hype::PERMISSION_NAME_CONSULTATION_ADMIN,
                hype::PERMISSION_CALL_CENTRE_VIEW => hype::PERMISSION_NAME_CALL_CENTRE_VIEW,
            )
        );

        $rs['Claims'] = array(
            'name' => 'Claims',
            'permissions' => array(
                hype::PERMISSION_CLAIM_CREATE => hype::PERMISSION_NAME_CLAIM_CREATE,
                hype::PERMISSION_CLAIM_DELETE => hype::PERMISSION_NAME_CLAIM_DELETE,
                hype::PERMISSION_CLAIM_DETAIL_EDIT => hype::PERMISSION_NAME_CLAIM_DETAIL_EDIT,
                hype::PERMISSION_CLAIM_RECONCILE => hype::PERMISSION_NAME_CLAIM_RECONCILE,
                hype::PERMISSION_CLAIM_RESUBMIT => hype::PERMISSION_NAME_CLAIM_RESUBMIT,
                hype::PERMISSION_CLAIM_SUBMIT => hype::PERMISSION_NAME_CLAIM_SUBMIT,
                hype::PERMISSION_CLAIM_MARK_AS_PAID => hype::PERMISSION_NAME_CLAIM_MARK_AS_PAID,
                hype::PERMISSION_MODEM_EDIT => hype::PERMISSION_NAME_MODEM_EDIT,
                hype::PERMISSION_PAST_CLAIMS_TOTAL => hype::PERMISSION_NAME_PAST_CLAIMS_TOTAL,
                hype::PERMISSION_PAST_CLAIM_VIEW => hype::PERMISSION_NAME_PAST_CLAIM_VIEW,
                hype::PERMISSION_BATCH_REVERSE => hype::PERMISSION_NAME_BATCH_REVERSE,
                hype::PERMISSION_EXECUTIVE_REPORTS => hype::PERMISSION_NAME_EXECUTIVE_REPORTS,
                hype::PERMISSION_OVERRIDE_CLAIM_SAVE => hype::PERMISSION_NAME_OVERRIDE_CLAIM_SAVE,
                hype::PERMISSION_CLAIM_UPDATE_PAYMENT_AMOUNTS => hype::PERMISSION_NAME_CLAIM_UPDATE_PAYMENT_AMOUNTS,
                hype::PERMISSION_FIX_ERROR_REPORT_PARSE_ERRORS => hype::PERMISSION_NAME_FIX_ERROR_REPORT_PARSE_ERRORS,
            )
        );

        $rs['PatientProfile'] = array(
            'name' => 'Profile Profile',
            'permissions' => array(
                hype::PERMISSION_VIEW_PATIENT_PROFILE => hype::PERMISSION_NAME_VIEW_PATIENT_PROFILE,
                hype::PERMISSION_CREATE_PATIENT => hype::PERMISSION_NAME_CREATE_PATIENT,
                hype::PERMISSION_REMINDER_SETUP => hype::PERMISSION_NAME_REMINDER_SETUP,
                hype::PERMISSION_IMPORT_DAY_SHEET => hype::PERMISSION_NAME_IMPORT_DAY_SHEET,
                hype::PERMISSION_PATIENT_MERGE => hype::PERMISSION_NAME_PATIENT_MERGE,
            )
        );

        $rs['Doctors'] = array(
            'name' => 'Doctors',
            'permissions' => array(
                hype::PERMISSION_PROVIDER_EDIT => hype::PERMISSION_NAME_PROVIDER_EDIT,
                hype::PERMISSION_DOCTOR_SCHEDULE => hype::PERMISSION_NAME_DOCTOR_SCHEDULE,
                hype::PERMISSION_DOCTOR_TEMPLATE => hype::PERMISSION_NAME_DOCTOR_TEMPLATE,
            )
        );

        $rs['hl7'] = array(
            'name' => 'HL7',
            'permissions' => array(
                hype::PERMISSION_HL7_PARTNER_ID => hype::PERMISSION_NAME_HL7_PARTNER_ID,
            )
        );

        $rs['Messages'] = array(
            'name' => 'Messages',
            'permissions' => array(
                hype::PERMISSION_MESSAGES => hype::PERMISSION_NAME_MESSAGES,
            )
        );
        
        $rs['SystemAdministration'] = array(
            'name' => 'System Administration',
            'permissions' => array(
                hype::PERMISSION_CLIENT_PARAM_EDIT => hype::PERMISSION_NAME_CLIENT_PARAM_EDIT,
                hype::PERMISSION_GONET_EDIT => hype::PERMISSION_NAME_GONET_EDIT,
                hype::PERMISSION_HOLIDAY_ADMIN => hype::PERMISSION_NAME_HOLIDAY_ADMIN,
                hype::PERMISSION_MANAGE_LOCATIONS => hype::PERMISSION_NAME_MANAGE_LOCATIONS,
                hype::PERMISSION_PATIENT_STATUS => hype::PERMISSION_NAME_PATIENT_STATUS,
                hype::PERMISSION_QUICK_SERVICE_CODE_EDIT => hype::PERMISSION_NAME_QUICK_SERVICE_CODE_EDIT,
                hype::PERMISSION_REFERRING_DOCTOR_EDIT => hype::PERMISSION_NAME_REFERRING_DOCTOR_EDIT,
                hype::PERMISSION_RELEASE_NOTES => hype::PERMISSION_NAME_RELEASE_NOTES,
                hype::PERMISSION_THIRD_PARTY_EDIT => hype::PERMISSION_NAME_THIRD_PARTY_EDIT,
                hype::PERMISSION_USER_PROFILE_EDIT => hype::PERMISSION_NAME_USER_PROFILE_EDIT,
                hype::PERMISSION_USER_TYPE_PERMISSION_EDIT => hype::PERMISSION_NAME_USER_TYPE_PERMISSION_EDIT,
                hype::PERMISSION_VACATION_ADMIN => hype::PERMISSION_NAME_VACATION_ADMIN,
                hype::PERMISSION_DIRECT_SERVICE_CODE_EDIT => hype::PERMISSION_DIRECT_SERVICE_CODE_EDIT,
                hype::PERMISSION_EXPORT_DATA => hype::PERMISSION_NAME_EXPORT_DATA,
                hype::PERMISSION_DIRECT_BILLING => hype::PERMISSION_NAME_DIRECT_BILLING,
                hype::PERMISSION_MONTHLY_TOTALS => hype::PERMISSION_NAME_MONTHLY_TOTALS,
            )
        );

        if ($include_hype_only) {
            $rs['HypeAdministration'] = array(
                'name' => 'Hype Administration',
                'permissions' => array(
                    hype::PERMISSION_SETUP_CLIENT => hype::PERMISSION_NAME_SETUP_CLIENT,
                    hype::PERMISSION_HYPE_ADMINISTRATOR => hype::PERMISSION_NAME_HYPE_ADMINISTRATOR,
                    hype::PERMISSION_HYPE_PARAMS => hype::PERMISSION_NAME_HYPE_PARAMS,
                )
            );
        }

        return $rs;
    }

    public static function getUserPermissionsByAvailableModules($modules, $include_description = false) {
        $all_modules = hype::getUserPermissions(true, $include_description);

       
        foreach ($all_modules as $const => $mod_name) {
            $suffix = hype::getUserPermissionSuffix($const);

           

            $mod_name = 'hype::PERMISSION_MOD_' . $suffix;

            //echo '<pre>';
           // print_r($mod_name);
            $description = constant('hype::PERMISSION_DESC_' . $suffix);
            $mod_name = constant($mod_name);

            if (!in_array($mod_name, $modules)) {
               unset($all_modules[$const]);
            } else if ($include_description && $description) {
                $all_modules[$const] .= ' &nbsp; - &nbsp; ' . $description;
            }
        }
        return $all_modules;
    }

    public static function getUserPermissions($include_hype_only = false) {
        $rs = array(
            hype::PERMISSION_APPOINTMENT_BOOK => hype::PERMISSION_NAME_APPOINTMENT_BOOK,
            hype::PERMISSION_APPOINTMENT_STAGES => hype::PERMISSION_NAME_APPOINTMENT_STAGES,
            hype::PERMISSION_APPOINTMENT_STAGE_TRACK => hype::PERMISSION_NAME_APPOINTMENT_STAGE_TRACK,
            hype::PERMISSION_APPOINTMENT_TYPE => hype::PERMISSION_NAME_APPOINTMENT_TYPE,
            hype::PERMISSION_CALL_CENTRE_VIEW => hype::PERMISSION_NAME_CALL_CENTRE_VIEW,
            hype::PERMISSION_CONSULTATION_ADMIN => hype::PERMISSION_NAME_CONSULTATION_ADMIN,
            hype::PERMISSION_REPEATING_APPOINTMENT => hype::PERMISSION_NAME_REPEATING_APPOINTMENT,
            hype::PERMISSION_REVERSE_APPOINTMENT_STAGE => hype::PERMISSION_NAME_REVERSE_APPOINTMENT_STAGE,
            hype::PERMISSION_CLAIM_CREATE => hype::PERMISSION_NAME_CLAIM_CREATE,
            hype::PERMISSION_CLAIM_DELETE => hype::PERMISSION_NAME_CLAIM_DELETE,
            hype::PERMISSION_CLAIM_DETAIL_EDIT => hype::PERMISSION_NAME_CLAIM_DETAIL_EDIT,
            hype::PERMISSION_CLAIM_RECONCILE => hype::PERMISSION_NAME_CLAIM_RECONCILE,
            hype::PERMISSION_CLAIM_RESUBMIT => hype::PERMISSION_NAME_CLAIM_RESUBMIT,
            hype::PERMISSION_CLAIM_SUBMIT => hype::PERMISSION_NAME_CLAIM_SUBMIT,
            hype::PERMISSION_MODEM_EDIT => hype::PERMISSION_NAME_MODEM_EDIT,
            hype::PERMISSION_PAST_CLAIMS_TOTAL => hype::PERMISSION_NAME_PAST_CLAIMS_TOTAL,
            hype::PERMISSION_PAST_CLAIM_VIEW => hype::PERMISSION_NAME_PAST_CLAIM_VIEW,
            hype::PERMISSION_BATCH_REVERSE => hype::PERMISSION_NAME_BATCH_REVERSE,
            hype::PERMISSION_VIEW_PATIENT_PROFILE => hype::PERMISSION_NAME_VIEW_PATIENT_PROFILE,
            hype::PERMISSION_CREATE_PATIENT => hype::PERMISSION_NAME_CREATE_PATIENT,
            hype::PERMISSION_PROVIDER_EDIT => hype::PERMISSION_NAME_PROVIDER_EDIT,
            hype::PERMISSION_DOCTOR_SCHEDULE => hype::PERMISSION_NAME_DOCTOR_SCHEDULE,
            hype::PERMISSION_DOCTOR_TEMPLATE => hype::PERMISSION_NAME_DOCTOR_TEMPLATE,
            hype::PERMISSION_CLIENT_PARAM_EDIT => hype::PERMISSION_NAME_CLIENT_PARAM_EDIT,
            hype::PERMISSION_GONET_EDIT => hype::PERMISSION_NAME_GONET_EDIT,
            hype::PERMISSION_HOLIDAY_ADMIN => hype::PERMISSION_NAME_HOLIDAY_ADMIN,
            hype::PERMISSION_MANAGE_LOCATIONS => hype::PERMISSION_NAME_MANAGE_LOCATIONS,
            hype::PERMISSION_PATIENT_STATUS => hype::PERMISSION_NAME_PATIENT_STATUS,
            hype::PERMISSION_QUICK_SERVICE_CODE_EDIT => hype::PERMISSION_NAME_QUICK_SERVICE_CODE_EDIT,
            hype::PERMISSION_REFERRING_DOCTOR_EDIT => hype::PERMISSION_NAME_REFERRING_DOCTOR_EDIT,
            hype::PERMISSION_RELEASE_NOTES => hype::PERMISSION_NAME_RELEASE_NOTES,
            hype::PERMISSION_THIRD_PARTY_EDIT => hype::PERMISSION_NAME_THIRD_PARTY_EDIT,
            hype::PERMISSION_USER_PROFILE_EDIT => hype::PERMISSION_NAME_USER_PROFILE_EDIT,
            hype::PERMISSION_USER_TYPE_PERMISSION_EDIT => hype::PERMISSION_NAME_USER_TYPE_PERMISSION_EDIT,
            hype::PERMISSION_VACATION_ADMIN => hype::PERMISSION_NAME_VACATION_ADMIN,
            hype::PERMISSION_DIRECT_SERVICE_CODE_EDIT => hype::PERMISSION_NAME_DIRECT_SERVICE_CODE_EDIT,
            hype::PERMISSION_HL7_PARTNER_ID => hype::PERMISSION_NAME_HL7_PARTNER_ID,
            hype::PERMISSION_REMINDER_SETUP => hype::PERMISSION_NAME_REMINDER_SETUP,
            hype::PERMISSION_CLAIM_MARK_AS_PAID => hype::PERMISSION_NAME_CLAIM_MARK_AS_PAID,
            hype::PERMISSION_IMPORT_DAY_SHEET => hype::PERMISSION_NAME_IMPORT_DAY_SHEET,
            hype::PERMISSION_APPOINTMENT_REVIEW => hype::PERMISSION_NAME_APPOINTMENT_REVIEW,
            hype::PERMISSION_EXECUTIVE_REPORTS => hype::PERMISSION_NAME_EXECUTIVE_REPORTS,
            hype::PERMISSION_APPOINTMENT_BOOK_NOTES => hype::PERMISSION_NAME_APPOINTMENT_BOOK_NOTES,
            hype::PERMISSION_PATIENT_MERGE => hype::PERMISSION_NAME_PATIENT_MERGE,
            hype::PERMISSION_OVERRIDE_CLAIM_SAVE => hype::PERMISSION_NAME_OVERRIDE_CLAIM_SAVE,
            hype::PERMISSION_HL7_REPORTS => hype::PERMISSION_NAME_HL7_REPORTS,
            hype::PERMISSION_CLAIM_UPDATE_PAYMENT_AMOUNTS => hype::PERMISSION_NAME_CLAIM_UPDATE_PAYMENT_AMOUNTS,
            hype::PERMISSION_FIX_ERROR_REPORT_PARSE_ERRORS => hype::PERMISSION_NAME_FIX_ERROR_REPORT_PARSE_ERRORS,
            hype::PERMISSION_EXPORT_DATA => hype::PERMISSION_NAME_EXPORT_DATA,
            hype::PERMISSION_DIRECT_BILLING => hype::PERMISSION_NAME_DIRECT_BILLING,
            hype::PERMISSION_MONTHLY_TOTALS => hype::PERMISSION_NAME_MONTHLY_TOTALS,
            hype::PERMISSION_MESSAGES => hype::PERMISSION_NAME_MESSAGES,
            hype::PERMISSION_REPLACE_DOCTOR => hype::PERMISSION_NAME_REPLACE_DOCTOR,
            hype::PERMISSION_REPLACE_SLI => hype::PERMISSION_NAME_REPLACE_SLI,
            hype::PERMISSION_REPLACE_FACILITY => hype::PERMISSION_NAME_REPLACE_FACILITY,
            hype::PERMISSION_REPLACE_ADMIT_DATE => hype::PERMISSION_NAME_REPLACE_ADMIT_DATE,
            hype::PERMISSION_REPLACE_LOCATION => hype::PERMISSION_NAME_REPLACE_LOCATION,
        );

        if ($include_hype_only) {
            $rs[hype::PERMISSION_SETUP_CLIENT] = hype::PERMISSION_NAME_SETUP_CLIENT;
            $rs[hype::PERMISSION_HYPE_ADMINISTRATOR] = hype::PERMISSION_NAME_HYPE_ADMINISTRATOR;
            $rs[hype::PERMISSION_HYPE_PARAMS] = hype::PERMISSION_NAME_HYPE_PARAMS;
        }

        return $rs;
    }

    public static function getUserPermissionSuffix($permission_value) {
        $reflect = new ReflectionClass('hype');
        $constant_name = array_search($permission_value, $reflect->getConstants());
        $constant_name = str_replace(array('PERMISSION_NAME_', 'PERMISSION_MOD_', 'PERMISSION_DESC_'), '', $constant_name);

        // remove first 11 characters if 'PERMISSION_'
        if (strpos($constant_name, 'PERMISSION_') === 0) {
            $constant_name = substr($constant_name, 11);
        }

        return $constant_name;
    }

    public static function getLastDayOfWeek($date, $wkday) {
        if (date('w', $date) > $wkday) {
            $date = strtotime(date(hype::DB_ISO_DATE, $date) . ' - 1 day');
            return $date;
        }
        while (date('w', $date) != $wkday) {
            $date = strtotime(date(hype::DB_ISO_DATE, $date) . ' + 1 day');
        }

        return $date;
    }

    public static function buildDatesArray($start, $end, $open_weekends) {
        $current_day = hype::getFirstDayOfWeek($start, '0');
        $dates = array();

        while ($current_day < $end) {
            $dows = array();
            for ($a = 0; $a <= 6; $a++) {
                $wkday = date('w', $current_day);
                if ($open_weekends || ($wkday != '0' && $wkday != '6')) {
                    $dows[] = date(hype::DB_ISO_DATE, $current_day);
                }

                $current_day = strtotime(date(hype::DB_ISO_DATE, $current_day) . ' + 1 day');
            }
            $dates[] = $dows;
        }

        return $dates;
    }

    public static function getFirstDayOfWeek($date, $wkday) {
        while (date('w', $date) != $wkday) {
            $date = strtotime(date(hype::DB_ISO_DATE, $date) . ' - 1 day');
        }

        return $date;
    }

    public static function displayPhone($p) {
        $p = hype::processPhone($p);
        $newPhone = $p;

        $len = strlen($p);

        if ($len == 7) {
            $newPhone = substr($p, 0, 3) . '-' . substr($p, 3, 4);
        } else if ($len == 10) {
            $newPhone = '(' . substr($p, 0, 3) . ') ' . substr($p, 3, 3) . '-' . substr($p, 6, 4);
        } else if ($len == 11 && $p[0] == 1) {
            $newPhone = substr($p, 0, 1) . '-' . substr($p, 0, 3) . '-' . substr($p, 3, 3) . '-' . substr($p, 6, 4);
        } else if ($len > 10) {
            $newPhone = '(' . substr($p, 0, 3) . ') ' . substr($p, 3, 3) . '-' . substr($p, 6, 4) . ' x ' . substr($p, 10);
        }
        return $newPhone;
    }

    public static function processPhone($p) {
        $newPhone = '';
        for ($i = 0; $i < strlen($p); $i++) {
            if (is_numeric($p[$i])) {
                $newPhone .= $p[$i];
            }
        }

        return $newPhone;
    }

    public static function displayPostalCode($v) {
        if (strlen($v) == 6) {
            return substr($v, 0, 3) . ' ' . substr($v, 3);
        }
        return $v;
    }

    public static function getAge($dob, $label_s = null, $label_e = null, $null_str = '') {
        if (!$dob) {
            return $null_str;
        }

        $year = substr($dob, 0, 4);
        $month = substr($dob, 5, 2);
        $day = substr($dob, 8, 2);

        $yeardiff = date('Y') - $year;
        $monthdiff = date('m') - $month;
        $daydiff = date('d') - $day;

        if ($monthdiff < 0) {
            $yeardiff--;
        } else if ($monthdiff == 0 && $daydiff < 0) {
            $yeardiff--;
        }

        return ($label_s ? $label_s : '') . $yeardiff . ($label_e ? $label_e : '');
    }

    public static function getDateFormats() {
        return array(
            'd/m/Y' => 'dd/mm/YYYY',
            'm/d/Y' => 'mm/dd/YYYY',
            hype::DB_DATE_FORMAT => 'YYYY-mm-dd',
        );
    }

    public static function getTimeFormats() {
        return array(
            'H:i' => 'HH:ii [24 hour]',
            'h:i a' => 'hh:ii aa [12 hour]'
        );
    }

    public static function getFormattedDateTime($outFormat, $date, $inFormat) {
        if ($date === null) {
            return '';
        }

        $date = DateTime::createFromFormat($inFormat, $date);
        if (!$date) {
            throw new Exception('Warning(s) or Error(s) found while converting date ' . $date . ' from format ' . $inFormat . ' to format ' . $outFormat);
        }

        if ($outFormat === hype::DB_TIMESTAMP) {
            return $date->getTimestamp();
        }

        return $date->format($outFormat);
    }

    public static function getFormattedDate($outFormat, $date, $inFormat) {
        if ($date === null) {
            return '';
        }
        if ($date == '') {
            return '';
        }

        if ($inFormat === hype::DB_ISO_DATE) {
            $inFormat = hype::DB_DATE_FORMAT;
        }

        if (strlen($date) > 10) {
            $date = substr($date, 0, 10);
        }
        
        $rs = DateTime::createFromFormat($inFormat, $date);
        if (!$rs) {
            throw new Exception('Warning(s) or Error(s) found while converting date ' . $date . ' from format ' . $inFormat . ' to format ' . $outFormat);
        }

        // set time to midnight - only interested in the date, not the time
        $rs->setTime(0, 0, 0);

        if ($outFormat === hype::DB_TIMESTAMP) {
            return $rs->getTimestamp();
        }

        return $rs->format($outFormat);
    }

    public static function parseDateToInt($v, $format = hype::DB_DATETIME_FORMAT) {
        if (!$v) {
            return;
        }

        // 2014-07-22: I don't know how these dates made it into our data sets but they are causing problems. [Ticket #1335]
        if ($v == '0000-00-00 00:00:00') {
            return null;
        }

        if ($format == hype::DB_DATETIME_FORMAT && strpos($v, '.') === false) {
            $format = hype::DB_ISO_DATE;
        }
        if ($format == hype::DB_ISO_DATE && strlen($v) == 16) {
            $format = 'Y-m-d H:i';
        }
        
        $date = date_parse_from_format($format, $v);
        if ($date['warning_count'] || $date['error_count']) {
            throw new Exception('Warning(s) or Error(s) found while converting date ' . $v . ' from format ' . $format);
        }

        $date = mktime($date['hour'], $date['minute'], 0, $date['month'], $date['day'], $date['year']);
        return $date;
    }

    public static function buildServiceCodeArray($sc, $mode, $spec_code, $base_ana, $base_asst) {
        $fee = null;
        $units = 1;

        if ($mode == 'B') {
            $fee = $sc->fee_assistant;
            $units = $sc->units_assistant;
        } else if ($mode == 'C') {
            $fee = $sc->fee_ana;

            if ($fee == 0) {
                $fee = $sc->fee_non_ana;
                $units = $sc->units_non_ana;
            } else {
                $units = $sc->units_ana;
            }
        } else {
            $mode = 'A';
            if ($spec_code == '12' || $spec_code == 0) {
                $fee = $sc->fee_general;
                if ($fee == 0) {
                    $fee = $sc->fee_specialist;
                }
            } else {
                $fee = $sc->fee_specialist;
                if ($fee == 0) {
                    $fee = $sc->fee_general;
                }
            }
        }

        return array(
            'service_code' => $sc->service_code . $mode,
            'fee_subm' => number_format($fee, 2),
            'base_fee' => number_format($fee, 2),
            'units' => $units,
            'cache_data' => $sc->buildQscCacheObject($base_ana, $base_asst),
        );
    }

    public static function getAgeOnDate($dob, $date) {
        // convert to timestamps
        if (!is_int($dob)) {
            $dob = strtotime($dob);
        }

        if (!is_int($date)) {
            $date = strtotime($date);
        }

        if (date('Y', $date) == date('Y', $dob)) {
            return 0;
        }
        $years = date('Y', $date) - date('Y', $dob);

        return (date('m-d', $date) < date('m-d', $dob)) ? $years - 1 : $years;
    }

    public static function getTextColourForHex($hex) {
        if (!$hex) {
            return 'black';
        }
        if ($hex == 'transparent') {
            return 'black';
        }

        $group_1 = substr($hex, 1, 2);
        $group_2 = substr($hex, 3, 2);
        $group_3 = substr($hex, 5, 2);

        $group_1 = intval($group_1, 16);
        $group_2 = intval($group_2, 16);
        $group_3 = intval($group_3, 16);

        if ($group_1 + $group_2 + $group_3 < 400) {
            return 'white';
        }
        return 'black';
    }

    public static function mssql_escape($str) {
        return str_replace("'", "''", $str);
    }

    public static function formatImportString($str, $toUpper = true) {
        $str = iconv("UTF-8", "UTF-8//IGNORE", trim($str));

        if ($toUpper) {
            $str = strtoupper($str);
        }

        return $str;
    }

    public static function formatImportPhone($str) {
        $str = iconv("UTF-8", "UTF-8//IGNORE", trim($str));

        return hype::processPhone($str);
    }

}
