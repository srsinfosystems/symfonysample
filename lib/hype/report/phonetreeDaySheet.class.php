<?php

class phonetreeDaySheet
{
 	protected $client_id;
 	protected $date_start;
 	protected $date_end;
 	protected $date_format;

 	private $file_text;

 	public function phonetreeDaySheet($options = array())
 	{
 		if (array_key_exists('client_id', $options)) {
 			$this->client_id = $options['client_id'];
 		}
 		if (array_key_exists('date_start', $options)) {
 			$this->date_start = $options['date_start'];
 		}
 		if (array_key_exists('date_end', $options)) {
 			$this->date_end = $options['date_end'];
 		}
 		if (array_key_exists('date_format', $options)) {
 			$this->date_format = $options['date_format'];
 		}
 		else {
 			$this->date_format = ClientParamTable::getDefaultForValue('date_format');
 		}
 	}
 	public function setClientId($v)
 	{
 		$this->client_id = $v;
 	}
 	public function setDateStart($v)
 	{
 		$this->date_start = $v;
 	}
 	public function setDateEnd($v)
 	{
 		$this->date_end = $v;
 	}
 	private function isOk()
 	{
 		if (!$this->client_id) {
 			return false;
 		}
 		if (!$this->date_start) {
 			return false;
 		}
 		if (!$this->date_end) {
 			return false;
 		}
 		return true;
 	}
 	public function generateFileText()
 	{
 		if (!$this->isOk()) {
 			throw new Exception ("Required parameters: client_id, date_start, date_end");
 		}

 		// get appointments
 		$attendees = Doctrine_Query::create()
 			->from('AppointmentAttendee att')
 			->leftJoin('att.Appointment a')
 			->leftJoin('att.Patient p')
 			->addWhere('a.client_id = (?)', $this->client_id)
			->addWhere('att.client_id = (?)', array($this->client_id))
 			->addWhere('a.start_date >= (?)', $this->date_start)
 			->addWhere('a.start_date <= (?)', $this->date_end)
 			->addWhere('a.status = (?)', AppointmentTable::STATUS_POSTED)
 			->addWhere('p.deleted = (?) OR p.deleted is null', false)
 			->orderBy('a.start_date asc')
 			->execute();

 		$file_text = "";
 		foreach ($attendees as $attendee)
 		{
 			$date = hype::parseDateToInt($attendee->Appointment->start_date, hype::DB_ISO_DATE);
 			$file_text .= date('h:i A', $date)
 			. '|'
 			. $attendee->patient_lname
 			. '|'
 			. $attendee->patient_fname
 			. '|';

 			if ($attendee->Patient->hcn_num) {
 				$file_text .= hype::decryptHCN($attendee->Patient->hcn_num)
 				. '-'
 				. $attendee->Patient->hcn_version_code;
 			}

 			$file_text .= '|'
 			. str_replace(array("\r\n", "\r", "\n"), '   ', $attendee->Appointment->notes)
 			. '|'
 			. hype::displayPhone($attendee->patient_phone)
 			. '|'
 			. date($this->date_format, $date)
 			. "\r\n";
 		}

 		$this->file_text = $file_text;
 	}
 	public function getFileText()
 	{
 		return $this->file_text;
 	}
}