<?php

class ErrorDetailsReport
{
 	protected $includeSelectCell = true;
  	protected $resultColumns = array(
  		array(
  			'column' => 'e.claim_id',
  			'rs_key' => 'claim_id',
  			'header_text' => 'Claim ID',
  			'width' => 'ratio:1',
  		),
  		array(
  			'column' => 'e.patient_id',
  			'rs_key' => 'patient_id',
  			'header_text' => 'Patient ID',
  			'width' => 'ratio:1',
  		),
  		array(
  			'column' => 'e.fname',
  			'rs_key' => 'fname',
  			'header_text' => 'First',
  			'width' => 'ratio:1',
  		),
  		array(
  			'column' => 'e.lname',
  			'rs_key' => 'lname',
  			'header_text' => 'Last',
  			'width' => 'ratio:1',
  		),
  		array(
 			'column' => array('e.group_num', 'e.provider_num', 'e.spec_code_id'),
 			'rs_key' => 'doctor_info',
 			'header_text' => 'Doctor',
  			'width' => 'ratio:1',
 		),
 		array(
 			'column' => 'e.ref_doc',
 			'rs_key' => 'ref_doc',
 			'header_text' => 'Ref. Doctor',
  			'width' => 'ratio:1',
 		),
 		array(
 			'column' => 'e.account_num',
 			'rs_key' => 'account_num',
 			'header_text' => 'Account Number',
  			'width' => 'ratio:1',
 		),
 		array(
 			'column' => 'e.hcn',
 			'rs_key' => 'hcn',
 			'header_text' => 'HCN',
  			'width' => 'ratio:1',
 		),
 		array(
 			'column' => 'e.serv_code',
 			'rs_key' => 'serv_code',
 			'header_text' => 'SC',
  			'width' => 'ratio:1',
 		),
 		array(
 			'column' => 'e.serv_date',
 			'rs_key' => 'serv_date',
 			'header_text' => 'Service Date',
  			'width' => 'ratio:1',
 		),
  		array(
 			'column' => 'e.fee_submitted',
 			'rs_key' => 'fee_submitted',
 			'header_text' => 'Fee Submitted',
  			'width' => 'ratio:1',
  		),
 		array(
 			'column' => 'e.error_code',
 			'rs_key' => 'error_codes',
 			'header_text' => 'Error Codes',
  			'width' => 'ratio:1',
 		),
  		array(
  			'column' => 'e.HX8Lines',
  			'rs_key' => 'HX8Lines',
  			'header_text' => 'Message',
  			'width' => 'ratio:2',
  		),
  		array(
  			'column' => 'e.still_rejected',
  			'rs_key' => 'still_rejected',
  			'header_text' => 'Still Rejected?',
  			'width' => 'ratio:1',
  		),
  		array(
  			'column' => 'e.actions',
  			'rs_key' => 'actions',
  			'header_text' => 'Actions',
            'width' => 'ratio:1',
  		)
  	);
  	protected $values;
  	protected $pdf = false;

  	public function __construct($values = array())
 	{
 		$this->values = $values;
 	}
 	public function setPDF()
 	{
 		$this->pdf = true;
 	}
 	public function getPDFFileName()
 	{
 		$rs = $this->getReconcileFileName() . '-';

 		if ($this->values['listType'] == 'hype') {
 			$rs .= 'hype';
 		}
 		else if ($this->values['listType'] == 'non_hype') {
 			$rs .= 'nonHype';
 		}
 		else if ($this->values['listType'] == 'current') {
 			$rs .= 'current';
 		}
 		else {
 			$rs .= 'all';
 		}
		$rs .= '.pdf';

		return $rs;
 	}
 	public function getCSVFileName()
 	{
 		return str_replace('.pdf', '.csv', $this->getPDFFileName());
 	}
 	public function getReconcileFileName()
 	{
		$reconcile = Doctrine_Query::create()
			->from('Reconcile r')
			->addWhere('r.id = (?)', $this->values['reconcileID'])
			->fetchOne(array(), Doctrine::HYDRATE_ARRAY);

		return $reconcile['file_name'];
 	}
 	public function getListType()
 	{
 		if ($this->values['listType'] == 'hype') {
 			return 'Hype Records Only';
 		}
 		else if ($this->values['listType'] == 'non_hype') {
 			return 'Non-Hype Records Only';
 		}
 		else if ($this->values['listType'] == 'current') {
 			return 'Currently Rejected Records Only';
 		}
		return 'All Records';
 	}

 	public function getCSV($options = array())
 	{
 		$v = $this->values;
 		$csv = new ErrorDetailsReportCSV($options);
 		$csv->setReportHeader($this->getReconcileFileName() . '  -  ' . $this->getListType());
 		$csv->setReportColumns($this->resultColumns);
 		$csv->setQuery($this->getQuery($v));
 		$csvFile = $csv->build();

 		return $csvFile;
 	}
 	public function getPDF($options = array())
 	{
 		$v = $this->values;
 		$pdf = new ErrorDetailsReportPDF($options);
 		$pdf->setReportHeader($this->getReconcileFileName() . '  -  ' . $this->getListType());
 		$pdf->setReportColumns($this->resultColumns);
 		$pdf->setQuery($this->getQuery($v));
 		$pdf->build();

 		return $pdf;
 	}
  	public function getData()
  	{
  		$rs = array();
  		$v = $this->values;

  		$columns = array('claim_id', 'item_id');
  		$options = array(
  			'date_format' => $v['date_format'],
  			'current_client_id' => $v['client_id'],
  			'available_actions' => array(
  				'edit' => 'Edit Claim',
  				'details' => 'Claim Details',
  				'pdf' => 'Claim PDF',
  				'patient_profile' => 'Patient Profile'
  			)
  		);

  		if ($this->includeSelectCell) {
  			$rs['include_select'] = true;
  		}

  		$rs['headers'] = array();
  		$rs['data'] = array();
  		$rs['patientIDs'] = array();
  		$rs['editableClaimIDs'] = array();

  		foreach ($this->resultColumns as $column)
  		{
  			$rs['headers'][$column['rs_key']] = $column['header_text'];
  			$columns[] = $column['rs_key'];
  		}

  		$pager = new Doctrine_Pager($this->getQuery($v), $v['pageNumber'] + 1, $v['size']);
  		$data = $pager->execute();

 		foreach ($data as $row)
 		{
 			$rs['data'][] = $row->getGenericJSON($columns, $options);
 			if ($row->item_id) {
 				$rs['patientIDs'][$row->claim_id] = $row->ClaimItem->Claim->patient_id;
 				if ($row->ClaimItem->status == ClaimItemTable::STATUS_REJECTED_OPEN || $row->ClaimItem->status == ClaimItemTable::STATUS_UNSUBMITTED) {
 					$rs['editableClaimIDs'][$row->claim_id] = $row->claim_id;
 				}
 			}
 		}
 		$rs['count'] = $pager->getNumResults();

 		$rs['errorCodes'] = $this->getExplCodes($this->getQuery($v));

  		return $rs;
  	}

  	private function getExplCodes($query)
  	{
  		$errors = $query->select('e.error_code')
  			->distinct()
  			->execute(array(), Doctrine::HYDRATE_SCALAR);

  		$codes = array();
  		foreach ($errors as $error)
  		{
			if (strpos($error['e_error_code'], ',') === false) {
				$codes[] = $error['e_error_code'];
			}
			else {
				$errs = explode(',', $error['e_error_code']);
				foreach ($errs as $e) {
					$codes[] = $e;
				}
			}
  		}

  		$codes = array_unique($codes);

  		$explCodes = Doctrine_Query::create()
  			->from('ExplCodes e')
  			->whereIn('e.expl_code', $codes)
  			->execute(array(), Doctrine::HYDRATE_ARRAY);

  		$rs = array();
  		foreach ($explCodes as $expl)
  		{
  			$rs[$expl['expl_code']] = str_replace(array('"', "'"), '', $expl['description']);
  		}

  		return $rs;
  	}
  	private function getQuery($values, $use_sorting = true)
  	{
  		$q = Doctrine_Query::create()
	  		->from('ErrorReportItem e')
	  		->leftJoin('e.ClaimItem ci')
	  		->addWhere('e.reconcile_id = (?)', $values['reconcileID'])
  			->addWhere('e.status = (?)', ErrorReportItemTable::STATUS_PARSED);

  		if ($values['listType'] == 'hype') {
  			$q->addWhere('e.claim_id is not null');
  		}
  		else if ($values['listType'] == 'non_hype') {
  			$q->addWhere('e.item_id is null');
  			$q->addWhere('e.claim_id is null');
  		}
  		else if ($values['listType'] == 'current') {
  			$q->addWhere('ci.id is not null');
  			$q->addWhere('ci.error_reconcile_id = e.reconcile_id');
  		}

  		if ($use_sorting && array_key_exists('sort', $values) && is_array($values['sort'])) {
 				$cols = $this->resultColumns;
 				foreach ($values['sort'] as $colNum => $order)
 				{
 					if (array_key_exists($colNum, $cols)) {
 						if (is_array($cols[$colNum]['column'])) {
 							foreach ($cols[$colNum]['column'] as $sortCol)
 							{
 								$q->addOrderBy($sortCol. ' ' . ($order ? 'desc' : 'asc'));
 							}
 						}
 						else {
 							$q->addOrderBy($cols[$colNum]['column'] . ' ' . ($order ? 'desc' : 'asc'));
 						}
 					}
 				}
  		}
  		else {
  			$q->orderBy('e.id asc');
  		}
  		return $q;
  	}
}