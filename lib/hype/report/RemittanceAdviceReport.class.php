<?php

require_once('../lib/vendor/tcpdf6/tcpdf.php');

class RemittanceAdviceReport {

    // PUBLICLY SET VARIABLES
    protected $clientID = null;
    protected $dateTimeFormat = null;
    protected $dateFormat = null;
    protected $includeMessageCount = null;
    protected $footerText = null;
    protected $outputType = null;
    protected $raItemDefaults = null;
    protected $reconcileID = null;
    protected $sectionsToDisplay = null;
    // PRIVATE VARIABLES
    protected $csvQuote = '"';
    protected $csvSeparator = ',';
    protected $csvNewLine = "\n";
    protected $csvDocument = '';
    protected $doctors = array();
    protected $JSONResultSet = null;
    protected $headerText = null;
    protected $pageOrientation = 'P';
    protected $pdfDocument = null;
    protected $raFile = null;
    protected $raItemResultColumns = array(
        array(
            'column' => 'ri.group_num',
            'rs_key' => 'group_num',
            'header_text' => 'Group Num'
        ),
        array(
            'column' => 'ri.provider_num',
            'rs_key' => 'provider_num',
            'header_text' => 'Provider Num',
        ),
        array(
            'rs_key' => 'doctor_name',
            'header_text' => 'Doctor Name'
        ),
        array(
            'column' => 'ri.claim_num',
            'rs_key' => 'claim_num',
            'header_text' => 'OHIP Claim Number'
        ),
        array(
            'column' => 'ri.claim_id',
            'rs_key' => 'claim_id',
            'header_text' => 'HYPE Claim ID'
        ),
        array(
            'column' => 'ri.item_id',
            'rs_key' => 'item_id',
            'header_text' => 'HYPE Item ID'
        ),
        array(
            'column' => 'ri.acct_num',
            'rs_key' => 'acct_num',
            'header_text' => 'Account Number'
        ),
        array(
            'rs_key' => 'hcn_vc',
            'header_text' => 'Health Card Number'
        ),
        array(
            'column' => 'ri.first_name',
            'rs_key' => 'first_name',
            'header_text' => 'First Name'
        ),
        array(
            'column' => 'ri.last_name',
            'rs_key' => 'last_name',
            'header_text' => 'Last Name'
        ),
        array(
            'column' => 'ri.serv_num',
            'rs_key' => 'serv_num',
            'header_text' => '#Serv'
        ),
        array(
            'column' => 'ri.serv_code',
            'rs_key' => 'serv_code',
            'header_text' => 'Service Code'
        ),
        array(
            'column' => 'ri.serv_date',
            'rs_key' => 'serv_date',
            'header_text' => 'Service Date'
        ),
        array(
            'column' => 'ri.amount_submitted',
            'rs_key' => 'amount_submitted',
            'header_text' => 'Amt subm'
        ),
        array(
            'column' => 'ri.amount_paid',
            'rs_key' => 'amount_paid',
            'header_text' => 'Amount Paid'
        ),
        array(
            'column' => 'ri.expl_code',
            'rs_key' => 'expl_code',
            'header_text' => 'Expl Code'
        ),
    );

    public function __construct($options) {
        if (array_key_exists('outputType', $options)) {
            $this->outputType = strtoupper($options['outputType']);
        }
        if (array_key_exists('reconcileID', $options)) {
            $this->reconcileID = $options['reconcileID'];
        }
        if (array_key_exists('sections', $options)) {
            $this->setSectionsToDisplay($options['sections']);
        }
        if (array_key_exists('clientID', $options)) {
            $this->clientID = $options['clientID'];
        }
        if (array_key_exists('dateFormat', $options)) {
            $this->dateFormat = $options['dateFormat'];
        }
        if (array_key_exists('dateTimeFormat', $options)) {
            $this->dateTimeFormat = $options['dateTimeFormat'];
        }
        if (array_key_exists('raItemDefaults', $options)) {
            $this->raItemDefaults = $options['raItemDefaults'];
        }
        if (array_key_exists('footerText', $options)) {
            $this->footerText = $options['footerText'];
        }
    }

    // PUBLIC SETTER FUNCTIONS
    public function setClientID($v) {
        $this->clientID = $v;
    }

    public function setDateFormat($v) {
        $this->dateFormat = $options['dateFormat'];
    }

    public function setDateTimeFormat($v) {
        $this->dateTimeFormat = $v;
    }

    public function setIncludeMessageCount($v) {
        $this->includeMessageCount = $v;
    }

    public function setFooterText($v) {
        $this->footerText = $v;
    }

    public function setOutputType($v) {
        $this->outputType = strtoupper($v);
    }

    public function setRaItemDefaults($v) {
        $this->raItemDefaults = $v;
    }

    public function setReconcileID($v) {
        $this->reconcileID = $v;
    }

    public function setSectionsToDisplay($v) {
        if (is_array($v)) {
            $this->sectionsToDisplay = $v;
        } else if (strpos($v, ',') !== false) {
            $this->sectionsToDisplay = explode(',', $v);
        } else {
            $this->sectionsToDisplay = array($v);
        }
    }

    // PUBLIC GETTER FUNCTIONS
    public function getOutput() {

        if (!$this->clientID) {
            throw new Exception('Required Value clientID is not set');
        }
        if (!$this->reconcileID) {
            throw new Exception('Required value reconcileID is not set');
        }

        $this->getRaFile();

        if ($this->outputType == 'PDF') {
            $this->buildPDF();
            return $this->pdfDocument;
        } else if ($this->outputType == 'JSON') {
            $this->buildJSON();
            return $this->JSONResultSet;
        } else if ($this->outputType == 'CSV') {
            $this->buildCSV();
            return $this->csvDocument;
        }
    }

    // PRIVATE PDF FUNCTIONS
    private function buildPDF() {
        //max excuation time increase
        set_time_limit(0);
        $hasData = false;
        if (in_array('general', $this->sectionsToDisplay)) {
            $this->pageOrientation = 'P';
            $this->writeDocumentHeader();
            $this->writeGeneralInfoAndTransactionsPage();
            $hasData = true;
        }

        if (in_array('accepted', $this->sectionsToDisplay)) {
            $this->pageOrientation = 'L';
            if (!$hasData) {
                $this->writeDocumentHeader();
                $hasData = true;
            } else {
                $this->pdfDocument->AddPage($this->pageOrientation);
            }
            $this->pdfDocument->setFont('helvetica', 'B', 15);
            $this->pdfDocument->multiCell(0, 0, 'Accepted Claims', '0', 'L', 0, 1, '', '', true, 0);
            $this->pdfDocument->ln();
            $this->writeRaItemPages('accepted');
        }

        if (in_array('rejected', $this->sectionsToDisplay)) {
            $this->pageOrientation = 'L';
            if (!$hasData) {
                $this->writeDocumentHeader();
                $hasData = true;
            } else {
                $this->pdfDocument->AddPage($this->pageOrientation);
            }
            $this->pdfDocument->setFont('helvetica', 'B', 15);
            $this->pdfDocument->multiCell(0, 0, 'Rejected Claims', '0', 'L', 0, 1, '', '', true, 0);
            $this->pdfDocument->ln();
            $this->writeRaItemPages('rejected');
        }

        if (in_array('messages', $this->sectionsToDisplay)) {
            $this->pageOrientation = 'P';
            if (!$hasData) {
                $this->writeDocumentHeader();
                $hasData = true;
            } else {
                $this->pdfDocument->AddPage($this->pageOrientation);
            }

            $this->pdfDocument->setFont('helvetica', 'B', 15);
            $this->pdfDocument->multiCell(0, 0, 'Messages', '0', 'L', 0, 1, '', '', true, 0);
            $this->pdfDocument->ln();
            $this->writeMessagePages();
        }

        if (in_array('statistics', $this->sectionsToDisplay)) {
            $this->pageOrientation = 'P';
            if (!$hasData) {
                $this->writeDocumentHeader();
                $hasData = true;
            } else {
                $this->pdfDocument->AddPage($this->pageOrientation);
            }

            $this->pdfDocument->setFont('helvetica', 'B', 15);
            $this->pdfDocument->multiCell(0, 0, 'Service Code Statistics', '0', 'L', 0, 1, '', '', true, 0);
            $this->pdfDocument->ln();
            $this->writeStatisticsTable();
        }
    }

    private function createPDFDocument() {
        $this->pdfDocument = new RemittanceAdviceReportPDF($this->pageOrientation, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->pdfDocument->setHeaderText($this->headerText);
        $this->pdfDocument->setFooterText($this->footerText);
        $this->pdfDocument->setDateTimeFormat($this->dateTimeFormat);

        $this->pdfDocument->setFontSubsetting(false);
        $this->pdfDocument->setCreator('Hype Systems Inc');
        $this->pdfDocument->setDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $this->pdfDocument->setAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $this->pdfDocument->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->pdfDocument->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $this->pdfDocument->setPrintHeader(false);
        $this->pdfDocument->setPrintFooter(true);

        $this->pdfDocument->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $this->pdfDocument->AddPage($this->pageOrientation);

        $this->pdfDocument->setPrintHeader(true);
    }

    private function getCellHeight($data, $widths) {
        $dims = $this->pdfDocument->getPageDimensions();
        $startY = $this->pdfDocument->getY();

        $maxLines = 0;
        foreach ($widths as $colName => $width) {
            $maxLines = max($maxLines, $this->pdfDocument->getStringHeight($width, $data[$colName]));

            if ($startY + ($maxLines) + $dims['bm'] > $dims['hk']) {
                return -1;
            }
        }

        return $maxLines;
    }

    private function writeDocumentHeader() {
        if (is_null($this->pdfDocument)) {
            $this->createPDFDocument();
        }

        $headerText = 'Remittance Advice - ' . $this->raFile->Reconcile->getFileName();

//        $this->pdfDocument->setFont('helvetica', 'B', 20);
//        $this->pdfDocument->multiCell(0, 0, $headerText, '0', 'C', 0, 1, '', '', true, 0);
//        $this->pdfDocument->ln();
        
        $this->pdfDocument->SetFont('Helvetica', '', 14);
        $this->pdfDocument->MultiCell(0, 0, $headerText, '0', 'L', 0, 1, '15', '8', true, 0);
        $this->pdfDocument->Image('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA5UAAACnCAYAAACB8njoAAAACXBIWXMAAC4jAAAuIwF4pT92AAAgAElEQVR42u3dfXBc5WHv8d/qzW/glXmLcQ0SJgQuwW9JeAkkkTxpCbSlkrNF3dtp481NMjfT2zuIy72dO8kMrP9oJ5O2gzxJKZ00ZU0y7bLpXuRLU0guvV6FJIVAYslOiCHBlsAYA8bW4mvZlmXv/WMfCVmWdvc5e3b3vHw/MxqwvXu055znnH1+53mLFAoFAQAAAADgRBOHAAAAAABAqAQAAAAAECoBAAAAAIRKAAAAAAChEgAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAAIRKAAAAAAChEgAAAABAqAQAAAAAECoBAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAABAqAQAAAAA1FFLWHc8ncl2S+o0P+2SNph/ape0foG35SUNz/pzbtZ/R+N9sVGKFAAAAIAwiRQKhTAEyA2Sus3PBkkdNfx1QyZ45iQNEzQBAAAAECr9FyI7TYDsNf+NNvDjjJiAORjvi+UocgAAAAAIld4Mku0mRPZr4e6rjZaXNCgpRcAEAAAAQKj0RpjslpSQtMVnH31M0oAJmOMURQAAAACEyvqGyYSkpGo7PrJetktKMv4SAAAAAKGSMEm4BAAAAECoDFOYbD52WC3Hj0iSml57peRrC8tXqBC9SJJ0auUHCJcAAAAACJUeD5MbVBx72OXG9hYdellNr72i5jffUNORvJpfeqeq7Z2Ntursmkt1duXlOnPp5Tq96v06c+ElbnzUvNnvAcZcAgAAACBU2ofJdhVbJu+pZjutRw+o5dd71DK2Xy27Dtbls59ZvUxnrunQmSvW6NRV61RoW1rN5sYkJZgtFgAAAAChsvJA2S0pJYddXVuPHlDbL55X8569aj5wvOH7c/qjazR1zfXVBsxtKnaJpdUSAAAAAKGyRKAckMPWycUvPau25/696i6ttXI22qqpD12jyQ9/QqdXrHayiTFJvfG+2DBFFwAAAACh8tww2SlpUNJ6qx2YnNDikSG17fyJmvKnfXPgpzau0uTHftPpZD/3xvtiAxRfAAAAAIRKzXR3HZQUDXqYdDFc7lBxrCXdYQEAAACEN1SapUIesXnPkt05tT015OswOV+4nPhUn+3MsSOSugmWAAAAAEIZKtOZbErSlkpfv+jQy1r0/e96dsykG0723qaT67tsJvTJm2DJOEsAAAAA4QmVNoEyMjmhpT/5ntqe/GkoTsqZ1ct0anPMpksswRIAAABAQzR5PVC2Hj2gC9N/F5pAKUnNB45r6dce1bIfPl7pW6KSculMdgNFGgAa8r3WHtD96uTsAgA8FyptAuWS3Tld8FcPB7q7ayltT/5Uyx99UM3HDhMsAcC7watbUiKggTLBGQbg8XtVO0chZKHSJlAue/oftfixp0N/gppfekcX/O3faNGhlwmWAOBNGyT1B3C/kmbfAMCrgbJXEsvshSlUVhooI5MTWv7og2rb+SJnZ/ok5U9r6dce1eKXniVYAoA3Q2WHmc08KBW1TvOdzXcJAC/rl7SF1sqQhErzRVs2UE6Pnwxrd9dyljz6L1ry/JOVBstBLjAAqFuolILVVXS65bWD0wvAi8zQg6459yw0SEsdTnivKliHsvXoAS39xjcDtfZkLSwe/JGa8kd1/Df/sNxLOyTlxFNmAKi19ea/XelMtjveF8v5vKLWPjsgB2GfAB9ehxskdZp6XHuF9blhSeOm/jca74uNBvwwJWf9f/+cP6POarqkiLkgciq2nBEoXTS56fpKgqUkbY/3xRK+L6iRSKe5uVZqtFAojDb4M1f6JVDxZ45EItNfLraGC4XCuEfOpaN9KBQKOZePty/KkQvnvhE8c9zqUPHrlrQzSPfcdCablPTArL/6bLwvlmpQpdovZX443hcbd7iftt9vVoL0QMBBmXB8Xhq0b72SuvVe61u1xkw9PCdp0C/HwuK62T/nrxtyr6piH2zrLeNeXj6wpcYHKuV2oIzeuka3bP+u1Wd5dsvvKP/jfXU7qFMbV+nY7/+J1Xsu/OeH1LLrYMWvL445/cdKguWWdCab89NFtoDEnEpOOVvV+CdWG+ZUNt34zJ2SHnfwWbxwPKaD0S4Hb91hvhTdPN6Vfubp/x3RrCfAknJ1Dk4DLlY0as0T5a2O1/nce27S5y0EiTL7SJk/36YK7lFufb/Z1sem/zevYkvWqPkZNqHLT2XVtkxUc17qFST7TZiM1uBXdKg4/GyLpEfSmewOEy79XifUAt8xSZM9/PT9YVNvGTIPHcIVKs2JXV+ysjY5oSVPfIcWSofadr6os5fldGJd2fI1kM5kh738dAOVKRQKg5FIZMhBReuBSCSS8kDrkdMZ2rwwVmKmi+OswDkmaVDSQFha5lA2VE6XV1+O7zFzIHR4JFTCXVFz/+qac84D25rl8essqfqPWe6R1JPOZAdM+Brw4wOwWROJnRei05lsb7wvNkgpq7+mGp3sXkn3lAuUTMpTvcWPPV3JrLBR+evJDWoTsBo65XYkEumVs1aHrR4ObB3mXrc/EomkTDdthMt85zzh44nSkhUGZwTHdGvWI5KOpjPZwSDNZOy1MJnOZEfNsW7kJFjR6e+udCabMiHN7/epautI8FqonNXttaSlPxgkULpkyaP/otajB8q9bL0ZJwOfKxQKw5K2O3hrTyQS6W7gR3cSavPyz/pTW0y4HDDjOxEOXQtU2HxXKTfjQ+er6EaZTTxUelTsKjmezmSTnHt3ri2PhMkFv7v8cq7NZ+wtdU9mWb2AhEpTASzZL3zJ7hzrULps6Te+qcjkRLmXPcCFFhj9JnDVI9hVLRKJJB1+kfZ7ZYIhC/dIypnxowh2RXFDmWvUb5Il/o3yHD5RFcd6jhIunQegdCY7qOK4Oa8vz/OAT67zfpUff0prpd9DpXnKWXI9ytajB7T4sac58m6fyPxpXfC/U54NFXCXCVpOzuX6SCSSqHOgbHd4gx8pFAopn56i9QTLUCh1fjvMUBC/VH67Vbp7ejenO/ThcthPZdoj19Soii2/cCmkV1if2OLDLr2EyjlKVgAjkxNakv4WR71GWnYd1JLduXIv62KsRGCCZVLF6cJt1bt7ZtneCwvw+5PGKMEy1KHSb2W43PcCFTR0SHrcjLmk1bJ0+Emq2DoZ5Wi4fp+q9JjSWunXUJnOZPtVpml/6U++p+YDxznqNbT4sacrGV85wBdCYDi5aUbrdbM1gWqLg7duL7cuJcESPgiVvhjbU2ImRUIl5tOjYpdY7mvnX0vt6Uw2pRouEUOdp/IASl3Xh6HSnLRkqdcsOvSy2p78KUe8DpY88R3PhArUVqFQGFRx3SJbD9RpplKnk/MkA3SaopIGmbwnkCqZzdgP99qkS/uK8IhK2kXPp/Pqwjk5e5CK8sc3Ibtxqb6cMC30oVJlBs1GJie06PEsR7tOml96p5JusP08wQkMTy4xUsUSIkFc87FDLOsTtApOpa00nh7bU8FMirNf28mZxxyPmDUPCZTFQLm+Rr9iSNIOSVsX+BmSNBLww5ysY/0IDrS4dCGVPGmLR4bo9lpnbU8N6eR1N6nQtnShl0y3ViY5Wv5WKBSGI5HINpVZG3YePZFIpLsW3UxNq5yTisaY6jeZVF7ScIl/d7tlpicSifSa1mX4n03Xv4SH77WVzKQ4e59HOfWY4550Jtse74slCJSu2WG2mYv3xYYtP0+3ihNr9dYw5Nb7GHfL2ey5HelMNhHvi6W4TH0QKst9ITUfO6y2nT/hSNdZU/60lv5gUMd/8w9Lnrt0JjsQ74uNc8R8Lym7AezTBlSbKcTLjrFeaD/quITIcKFQ6K4gIHebY9St6mfxG5BUr1C5KSDjUoMQKj15r7WYSXH2Pnv5ocimeF8sDGV+a7wvlqzg/HaqOBa23Zy76Z9aLG2xJZ3JKqTB0q1AOf1QNVXNvcJcAzlJSVMG+h3WD7xWx3EqIXoK+SZUlryBLH7u+2rKn+ZIN0Dbzhd18ubDOnPhJQu9ZLq/OcuM+FyhUBg3a0E+aPnW9ZFIJOHm0h1mrKaTLidDXlxCxASznN6bNbfXfME5emrq9vGGL0Jl1JQbr533XsuKJhOz+Ei8Lzaq91qWB+c8TOjVe61ZboWNLelMdjzeFwtNl0MzKU+1gXJMUrIWrWmmDPSb2Wir+e5q5DHuVnU9h7rSmWx3SB44NVRTlSc6UapwFlspX+QoN9Di575f7iX0Nw9OsByQN5YYSSqgS4gUCoXxQqGQKhQKnSqOY3F6fOB/XQE477afqZPTHoiwOR7vi6XifbFEvC/WLumzcjbh23zuCcvkPWbVg2on5dka74t11rp75vQ5V/HB0FYVh3/4Rb9HtoFahkpV0EqJxmrb+aKajx0u9ZIOFjMOFCdf5q7NBmy6ijpdQmTYTwfarBO6ycGXcwdLjPi+MukkXHnqXutgJkUpIOOzcF7gSMX7Yt3mfuZGuHwk6MuNmNazB6vYxIikjZV0Y65BuEyacLnDJ/faHhc21cNEYx4OlebkLPikNjI54YtWyuita9TxxR6t2/ZlffgfvnreT8cXe3TR7Tf4+iRXEO4TXArBYLpqNnKJESdfkHn59CmiOd5Orh+uOX9zWmH2Ujl3VAZNZRrBDJe5WeFyrMrNDQZ1hnmzX6kqNrFdUrftBDwun+vReF+sV9JmebvVMunRbWEe1YypLDvjq2d3esUi/cYf3KErP/0ZLb3q+pKvveTjd0mSJsff1r5v/rVef+wpTR095a+T/LNfKfKJiVIzwfaYmduYsCcYEpL2O3jfgCpcWmA+kUgkIedLiPi27BUKhcFIJLJddi20VMzDGSq70plspxnn1MhKcbecj1Hq5PQHP1yalsYBOe/e2WEq8UHsdpiS83GJ2700mVG8LzZozvWgPNYTwTReubnmZy913dqqpvtrycpn6wve7MkWvXWNPvrYP+q6+75SNlDO1tZ+qa677yv6+Pf/j6K3rvHXSc6f1qL9u6s6n/BVyBmVtM3BW3tM91UngbJdzp4CjplupH5nuw90I/S37jqWlVqopqJPqAxHsBw34efeKjZzT9Bats3+OO2OudWLs+OaVssNKrageonbDySiYmyl90Kleaqx4FOaxa/u9uS6lJdtvlm3bP+uVZicL1zesv27umzzzb460W3P/TuhMlySctalxelMwE6XEAnEDd4EeavxKU4DPDyhmvFiWxrZLdCFMUqU23CFywEVJ/JxKmizy6ccvm97vcdPOjjXCa/MkGrukbUI4IRKr4XKcie6+eWfe25HL7r9Bm38asq17X3wS3+pZWtX+uZEN7/0jlqPHij1kp6gjn8II9Od1MkX2HrTjdUmHHXK+RIigwE67Lb7wvXmQyaUVbsEQyMrNtVWbJlkKnzBMlVFsFwflNlgzbIcTh6ebg/p+p3V3iNrsa5mNCyzE/spVHaX+seWn/3KW4HqgqVa/+cPubrNtvZLdd29/81XJ7vtF89XdV7hu2BZryVGkg5v/kG7sY9SOQ8FN85bQ0KlS2OUojyADG2wDO0ySqbMO7luR0TrWL2OdWjKY2BCpTnZC44HWvzqbjXlT3tqJz/437eqrf1S17d7ycfv8tX4yuY9e8u9hC6wweMkuFU87qCKJUS2mS6jQTJMcSNUWgSzhE/uB7U6BvBfsEzK2TIUHQFoHXLScpaX1MvEMNZ6LY913kF57OYweyBUlgseXuz6Ws0YynIuv+MO/4TKA8fLrVnJRRYwZskLJ5WASpcYSTrYdl4BfFLo5xlsYcWt+2RdrwGXn/4TKsMrIWfj9YMQKq2v8UbP9OxTtvfGAdlPMpTkMHsjVJb8Mmn+1VioDuClt3zSV5+3bX/J0N/B4rCB5LQiWXKChSqWEEkSwOBjbgWqej8tT8i9MUqEypAyrW5OAmKXX+sXppXV9toZMpMcwf5Y245bHZD9hFBd1He9ESoX/BJsPnbYk7O+1tLSq67Xos7l5x7UfW979vM2v7qPykLImG6mTsbCLLjESJVLiATyi5bZXENR4emUu5NH1HOslZu/i8pYuIPloKQhj5f3Rl87SUpKXY71drP8zbCDMsk58kCoXHA8ZevBX/tip9/4129r71//z5mfsX/6mibHnQfBxasuOfegemxM6Tmh8sXXCJXhNCB3lxhxuoRIIsDH2HbyElpr/cftMNVTj6flDp/+l9JFUQg9JxVy383bYK5P23WFh7yyNIfPjnW3g2M9uxymLN+7hdbKBoZKsz7lwoHltX2e3tn87h/qh5/epN33/LnGHt4x87P3/of0zO2/pfzuHzra7rKrr6r6s7XsOlifE54/zbjKEDLdTZ08bT1viREz1vIBB9vaYcZ4BpXttcPEPsE/x7WqnNtyvYWIyli4mdBk2zLkxyE2vR69pnlQUWylHJ1VJlOyn/E+wWFvUKhUmVaspnHvPnif2P+iXvj8n+r4nkPz/vvU0VN64fN/6qjFsnXZBb466a1HSwZYKgrBDZYpFac3tzV3iRGn3VeDPq26beVjlFLpO7XoydFbyyU6HD79b9SxgL+k6hTSGsk2dIzQSun4PtXlQvmzrZ/0s0RS40JlycBRr9Y2J176269q6uipkq+ZOnpKr33nG678vkWHXvbssYi8+Xqpf+7gsgg0J8FuZokRM26wx8E2tgZwCZH3rqlia67NtZMP8vEgVDq7vmok6aNjAR8xLUO2wyq6fRR0Si6h52LQhn14X6iLsW2ZjIrl9FzT4taXSJkulQ01sf9FvfX4cxW99sjwLl1d7wrp5ERdf1/zm2+Uu5FuMIOeETCFQiEXiUR2OAiGD0QikZSctVLm5bx10w+B0smkRbk6fLSBSCTSiO4jKdMqHiimglmrh26JWoQ/M2SlVuMfvRgqB9KZbEPKvAlYYTQou7WK/fQwotvh8YDdfapT9utdz1uniPfFxtOZrG2ZTPIwoDGhcsEm4pbjRzy7k28/+28Vv/bM/5uo++drO3KgvpXgkycdn2cfeSASiTzAJT6vfjlrbcw5rFT3B3wJkZSD41KPULm+QccjF9DzXMvKcEc6k03UIJjUsgW004PniDLfmH23qcB3BPiaH2FdSkeSlq8fMzMQl9qeVZms0f03dFzr/tr0lne7vp44+HrFrz3+y9eDf9LLL3lCt6YAq2KJESeVgZEgtlpJxRbKSCQy6DCg8zTbf7otX287iYmrAdDh03+bBcTXUyTgJFDXeX3Wel7zOYpDXe5TJUOoCfY7LLeZ4GzUP1QuXKk8dcKzO3ls3ysVv7bcuMuKD+xrr3j3pJdf8oRBy8E3IPtZ0pwI5OQ8ZmzpsMNAuYPxlL5k87At76CSst7lyrbttTdkWyn2UThAjZgKvO24Sr/UMToJlTVne5/MV9iiaDvkpov7Wf1DpS81okurVQV18mQDfucEpT/ETHfUZI1/zfYgLSFiWiYTkUhkWNJOOe/GNUAJDHyoHHb4tNyVhzBm/KdtZS0p+xmJOykWkP3ySH7pDWV7jx+lKFjfp2zveRV9f5pJfFhexK+hstzkLyhRWX2z/l1u6z2OE54MlinZd9GrVF4+XKsrEol0z/lJRiKRARMkj0p6RNV1+wv6Wp1BrvzYVDCHHT5A6HFpHb9+FWc1rNT0Mgi24YBQCSeh0g/XvHXZZoLDmt+nbCf9s62DbGH9XY+EygomfwnXgT3l6+PBmMrwqFXwG/BBF8+uSCRSmP2jYgvk7J8HJN0jd8aP5RX8tTqDyvaeOGoqmTnZrw3rRhlx9PQ/3hcbl11Xxm6KBiTZTsTmh4q77WfMUwxqfp9KmXtUpSE/5eC8JDktdQiV9DW2PLCHfN1yy5jKkDCtZttd3uyY6OI57xcoYyl9y/b7b3huYLOQqGYx7nQmm5Dd0/+xOWOUbFpbOikaqENg8wNaKWt7n3JyL3Xynt5q7r+ESjT+JOSPchDQSEm5+5Q1GfAlRJzYHtRZcKkEn2/2otwmsNmM7YmqurE9ySorXTaV4w4qYBBjCVH7+9R2h8u12H7vRkWPorqESiqJtToJ4xxaNI5pPXOrZXGI8DRvoExwGHzNpvvrmAsVG0eVmnQm2yu7sZ/5eT7beA2PDQiVCDnTSmk7CZKjOooJora9sfi+rnWoZACyncjEKT9/fFJu+Li1xAhP+AiUQWQzpnZ0gevLpjdAh6l41fr6G5hnjFKOUAlLtFajlqFtqMoMkrJ8vdP7b+i1uLWhwuLFHM1Zml96x88fnwcIIVMoFMYjkUhSxdlNqwlQlJ33bC0UCslGBlo1pgUhF6ST6GA+gfP2P94XG09nsoOyW+Q7YVMZMp+zy4XKlm2Z6fTQ6aLMNwYPFlDL+1RV36PxvlgunckOWf7efgdhlFDp1obOvO9ytWofR9SByNv1nzTs1MoPBP2wbvfADWGDpAd9FCxTkUgk4eCGLzGz6WxjkhIeWDokxfIlDakwD5eoGNmEyq50Jts9e3xmBSHU6h453xileF9sNJ3J+jVQpCyOFxpnNID7RGttbQLimEvXdMqybrPe8v4LN0MlnGs+cJyDUIMvrUZXqCORiF9v+DsdvG+AyXmKa2g1uHUSjQ+V81aYTVizfVqeUAWtYGZttS0OrvWF2HzOLopI6LW7cY343HqKQdn71AbVuZVy1v03lc5kk7Iby5kUyyZZsZ39dcH1tgrLV3A054aKyQlPfq6z0dYw3vBRAYdLjIyFPEiNSdoqqZNASagsM/bHtnxUuhi37XaHysykaPUdwILhXCMB3CfroRzMhFyWbW+mucsdVct2W13c22obKhdsiShEL+JoztF25IA3Q+WaSwmVcPPGmwrhMRqTtE3SpkKh0FkoFFhGJbhsWiCGygTOnOwnxOqvoCLrZiulk+8AKl7hZnv+PT/2fp4JrMIarl3hsDeF2+td206YVsm9ElWEygW/aKaWESqdaD52uP6hsr3swzQqx0DRiAkKO1Rsjdws6SoTJPsZsxj4ilC3W9+RVVRSEmVaQGyf/g9VME7IttLfTWkJdViwXR7CL3WMEUKla2zve/Mtd+TGg4JBy7dtoQW6AaHyzIWXcDQdaDl+pP6hMrqi3IXHDJ4Ig6FCoRAp87OhUCh0FwqFXtMaOWjW9UQ4uDKecs79NSW7p+VRSb0LVOjbHYTKlBv7QWUaTh8o+Gjyk9FaH4uQPHhw0ptiwGFrsdvhVmISwsozjeXrS4aNqY2r1LLrIEfViOSPSCu997nOXnF1qX8e48wBgKOwVGlleUDSA5YVofnCYK8JnZWqaIxSvC82bDkDbCdFhVBZaRn00b4NS+ohVFbNSSjrNBPr1ELe8r7Zn85kaxVyQx0qR0uGlZWXSyJUTmvd8zNF3j1a8jVN+aN1/1xluiqPcuYAwFFYqrSXh22o7Ehnsr3xvtjgPGHThs3rR1T5eFJmvgyvXsvX53y0bznL6zS6wHUaWg57U0j2LZu1NN1bJMUZLc2q+2u5bpFnLr2cIzo7se86qMWDPyr507bzxbp+pjOrl5XrqpzjzAGAJLvp7/OVPsk2r7OdZbl/TmUtIbuxbHnZjScataw80gU2fIEhIbsWH8kHk/RU+Vl7KRnn3beiAdiPJKfS5VBpLDi73elV7+eIetyZa8rWQRhPCYAKs31Isr132lZSuuZ8Jtun/7bdt2z3h1AZPgkH7/FNK565XoZsQyUTu1RdRryoI53J8sCgBqFywS+aMxdeojOrl3FUvRwqr1hT7iU5jhIAqKah0qwTaVth7TeBt1v2XU5tp+cftXx9J0UmPEwZtF3IfqzM+qheZBuCo2Jil+kykpD9zMBexnmtQagsGTrOrL2Oo+phZVqTRxiIDAC1D5VG0vL1W8wSDrbv2+7g3m5b+e+myIRK0sF7Uj7cTyctq/20VjouI17WRTf/eofKK6/mqHrUmWsvZjwlANQmVNqGsOmlFWxnw0zKvoUo6fCz2eikyISDaYHqcvBW34VKhz0KQt9aabqKdgRw12itdDNUmqedCy4Ie/LKdTobbQ31QT0bbdXUxlVWP/U4Zqc3bKzqgQEAhIhVpbmKtfdsA5/trIg7quhyaBN4O2idCUVYaJd9V2pJGvJh19dqwvADpldBWAU1fG0J+XktqaWKC+zBhf5x6kPX1H1WU0+FyjWX6tjv/4nVey7854fUVOM1PievuqFcpYhpsAFQcbbv4jRSxa8bNJX0Ws2QOFDFe0dl19qwQTycDLpBh2V1wK87HO+LpcyaiR0OjlXouks6HG/rt8BMi+U8mhy+r+SXxtQNH+HIeszUxlXlur7u4CgBwEw4sg1fTius4zWscA9V0YJa9rveheMGf4WFlMOwMBaAh9ZOrtH16Ux2IIRFJRnw/UvQK8PFUGnWq1ywW8yplR/w/CywizqX68KbO0Jzok+v/VC5l9BKCQDOwlG1SzGlarQf1W7XdnKfTopOYANlv5wvSB+EVp2U7Mc/S9I9ZgxqWMpJp4LdSikVW+oT3BXO11LFewdUogvs6ds+qubHnvbkTi9bu1JXf/5zamu/WHveTOrU6LuBPslno606ee0tpV5iuyg2ABAqXQqV8b7YaDqT3V5FpX0+Y/G+WLWhkrUqMT0xz4MO3z4UhKE18b7YuOkC+4iT+nI6kx02DTJePs/t0/taxWaSTsqIR+75Nt26++XjLt1eDJWDpW4yJ6+7SW3RITXlT3tup6/+/Od0+W//kSTp2nvf0e57/jzQJ3ly001lzyVLiQDADNsn7W5UFgdcDpVJF7YxXOPjBu8HylSV5TIwY8/M2MqEg3IelZRLZ7LdXg2WJlDmzPnKOdxGp4OyMhTvi3V7YP+Tkh6weEtHOpNNuPDgLlCaqri4RlViHF6hbWklYaZuFnUu16o//qQ+/A9f1cTrr878/cTrr2rdti9r1R9/Uos6lwfyJJ9cX/b+x0UBAHI0SY/cmNXSVDbdemLvSu8T87Ax76BiCf9fB+3pTDZXZaDc6vXWuTqG5NnBFlsAAA/YSURBVJlg6cFz3a3iuPD1VW4qWaf31EKqjmWBUOnkJJxc3+WZ5UVOjb6rtfd/XZd8/C5d/YUvzfz91V/4ki7/7T/S2vu/HshusJObrlehbWmpl4xVOZEDAASJbShys+uWW92pBlzsfTJc4+MH74WMXhMyqml5Hon3xZJBOzYmJG+tIlju9NIYS9NCt1NVzj5tWjp7Ld/mmfqneTC43fJt6734kMC3odL0k19w4LKXWiujt64p+5pla1cG6uSejbbq5M23l3tZkssAAGbUe5Keir9TGxBOnewflSz/hsnOdCY7KOnxKkNG3kHA8FOwTKq6h0mPpDPZVCNnEDXnOie7Lp+l9DsoM16rf6Yc7jfcCJWVFAqvtFbmf7xPz/+XuzU5/vZ5/zY5/rZ2/VlCx/ccCtTJndx0U7llRPL0BweAqkLRqMu/v9qK1naXx8gzA2w4wmRK0n5JPS5ssteNLuEe1yvLruFzbJE03IiWLjOT77BcGgNtwrFtuBrzWv3TtJrarjncQ5d/F0OlKRSlWyvv8MbY/SPf/7l+8Rf/47y/H/nyn+itx58L1Ik9G22tZCwlM1cBwLka1lJpDFZZWU26/HlyNT5+aFyQTJjWqv1yb5Koz4ZhSI15cNNd5bXaoWJ32MF6BJN0JtudzmRHVZxkM+riphMOtpfy6Kl1Ui9OCpKqm/117gFdcJrlE+u61Tq8S80vvdPwHc7v+qX+7aYNet/vfkySdPiZ5wN5Yk/FN5cbS5knVALAuRVtB5UjV0OlWbpgQM66pe2oQQuR7fbWU5I8V643SGo3IajT/LcWC3V/Nky9n+J9sWHT6vdIlZvqUbHFa7uklNuh3IzhTKh2szPbtlJ6tv5pZvgdsPwe6E1nsu2souBSqDQnIVnqJnXirrt1wUsPNz5szZmMJ4iT80xtXKWTV64r+yCACwAItIFIJNKoazxVKBT8WLm0bWUbq9F9NOUwVLpeUTNraFqHmAbN+jmQzmQbVeb767jPiQq6TXbWKDgSKM+v/8qFYCkVW4u3pDPZMRV7LAw6DZimfPSan5qVAxNYbbc/4PH6p+1DvagJ1sl6fEeZ3gU1+d6u9hpucfHDJEtdVKdXrNbknR9W25M/reqXHP/l63p2y+9Yv2euwz9YuIXSdvsnDx4+589N+97Whf/8kNU2mva97cpJOBtt1cSn+iqpCNFKCQRbI1uMcj49ZrahcrRGFdVR02ph0yVxqIbdDodk18rRKfe7BXu9zNdz0pWOOgdGAmX9guX0+b1H0j1muyPmXjO8wP11uiW60/zUc8yZkyDl9bLipKdIvUJltIbnt+rvjxaXL6pEqZ09/rHNat43WlU32Kmjp5T/8b6qP++psYVbKKvdflP+tJp2HWzIlXCq51PlJueZLvwAgHN1eyg8pyxDZS0fFI5aVmQ2yIV1MuF5eRUn5cmF/UDUIFjOtt78TE+i9IAX9tm0hto+3Nju9UmczBAE24d60XQmmwj75JctLm+vX9KuUi84cdfdWnrom2rKn3b8SyY3Xa+m8WLL+VTHVZKk5jffUOTkyZk/S1LTqZNqe/KnOtl7W3Fnx/arZddBtaxYpPff+zm1RVdo/MXdmpqY0DvPPK+Vd2xS/qWXdGVvTJP5o8r/8hdqixYfPh5/7VUtu+JKSdL47j1qX7dWJ948pLcef04dX+zRiTcP6UhueGa7r/z9N3V8zyFdtvnmYth78221r1urQ0/t1Mo7Nmns4R266PYbtPp3epT/5c819vCOqg/+5KbrdfLaW8q9bMhMWw8AOD8M2YatWlVsculMttIWwrEa39dt97ObohR4IwrHLK+2wXLUPFCJhmCXk3V6TyMMyH7yqqS83wpbU00uX1DDkraVes3pFat1qudTVf2eqRs+oqmOqzTVcZVO3HinCstX6NRtt+vkJ39PheUrdOLGOzV53Y06/rHNmrzzwzpx452aev9anW0vBsTf+IM7dPmdcS1d3amWxUu09v6v69p7/6vWfO4+Xdkb08W3fkrtH9yo6LX/ofh3n/6Mlr//Gl1331d0yU23adH7LtV1931FH/zSX2rZ2pW67r6v6Iq7Yur8Qt/MdqddcVdMV9wVU/u6tcXX9fXouvu+okWdy3Xj33xHS1d36vLf+l21rFhU1TE5c+3FmvhE2WWh8ioO1gYAzOKFSXrmUWkFpdYVNdv97KREBdq2eF9sA4Fy3npwTsWHKiNB3k/TSmnbDXOHX8qMyTO2a5F2NGKJmMCGyllfbiUXbz557S2avPPDVf2Syetu1OR1N0qSWvf8TKdXrNbpFavVuudnxb87+GtFJid0dtFiSVLzWwdmWjclqa39Ur06mNarj+7Q5Pjbuvy3/0ivfecbM//+8t9t097kw3p3z7N69X89qneef0GSdPgnP1J+1y9ntnHF3Z8+77Pt+/bfL7jm5RV3f0GSdPHHi5/9hc//qZ6NfUFTR085PhZno606cdfd5WZ7lYqT8/AlAADns14Ko9YTs5Rbssuox3rDtt8bHRSnQBqTtCneF2MITfn7Qrek7QHeTSdlwG9zeTi5ryYJle5eTOOqoDXs+Mc26/RH17jyO1t2HVTzscNqPnZYLWYs48lrb1HL8SNqfaH4nX/mstUzLZWvP/aU3vjXb2vt/V/XRd0b9MaTaUnSa5kdeuXvv6l39zyrG//mO1q2duV5v+uSm25Ty4XvhbfL74xrcrw4yc7oNzJ6d8+z2vjVlC66/YZ5P2tb+6XF0LvsAkmqKkzOhPTP/EedXrG63MuGmJwHAFwLlfVqiRio8t/dqiRbCfsT+4DJS9oa74t1Mn6y8rpwvC+WkLRZ1a1l6TmmV0eP5duG/FZ2KnyoN1eXWcKHUOniichJ2lo2WN4e15lrL3b0O9r2Pq+2ve/N4Npy/Ihajh+Z+fOS55/UBX/1sJoPHJdUbKmcdlH3Bk28/mrxde9bqRMHi7PDnhp9V5fcdrOOHxgtbvPC81v+Dv/kRzN/f/iZJ9TWfqne+fH3Zrab37tHkrT4fe87/73PPKGJ/S8W//9Hz0mS1m37stZt+7Iuuv0GdXyxx/o4nPjM7+rUyg9U8oXQKwCAW6GyXrObpkpUSuu53ttIjY8nPBomJXXG+2JJDoej+vCgit3Bt/ng426r8L7mpCykfHoKnXzu0LbkN9XwQkqqTH/kQttSHYv/Z+tg2fTWQTXlj6opf1SLDr383t+9VWylXHToZTXlj868ftGhl1WIXqSzl62aCZJX3P0FHX7mCb3+2FM68eYhHX7mieK/rfoNXX5nXG/867eV//E+5ffu0Yk3D2nq2IQOP/PEzJjKw888odeeyOrwM0/oUO5p5ffuOWe7b/3LD4t35L17ZraR37tHr3zrIR1+5gkd33NIr3zjL3TxrcXxpRdceaValy+3DpQVTMwjSQnWpAQAV0PQaJ0qpeMlKjaDdby32+5vJ0XKt8Yk3TsdJqk/VH8Nmy7DV0na4cGPuF3SVfG+WH+5c21aKW0nsBnz8ayoTh7abTHHKXRaarz9XvNFFC0XLC9M/13FS40sfuzpmf9v2/nieX+39GuPnvP6uX8ee3jHObOtvvX4c3rr8WLL4d77H9Le+99bY/LXX/3WzP//9D/92TnvOee/em5m27PNfv/0aw5+699m/m32v9coUG5ltlcAKFlRapf9OMBcnSs298zz98k6foZh2XV5o6XSX0ZUnLV0sNZjhUMcLkcl9ZrAkXQQztw03cshZTnXhpNWuKSPz5mT5UWk4jDAZNjKeFOtT4aKg5VL9id32mIZVhaBcgddVgCgJgFotF4fzlT65k76Ue/13kbrcExRH0OmPG2VtEnSCjOba5JAWZ/r2Yy3XKFii3A9Z4rdIWlzvC/Wbs53xde1efiWsA2vAVi70UlrZb85XqHSUoeLZzidyfarzIKwTlosCZQljShYy4fkavz6WlXCtvrsM4fpc4/67BpIeehY+6VM2LApO2rATNopnfu0PNWAc7qVMl+T+0muDtfheEACo22Z8Ox93jS8DEgaMK2XvSo2xHTLvXUuR8zxyrnQa63dQcDyfZkzOeZes/+2x2u8gvK51SO7WvV9KFIoFOrySdOZbKJcsJSkyOSElv5gcKZbK4rORlt1Kr5ZJ69cV8nLxyRtYBwEAARHOpPNqbg23FC8L9bNEQECe613qjguuduEk9kt/11z6nujc4LBuKRhZupFvdUtVJqLJKUK+yUvef5JLR78EWdI0pnVy3Qi/seVLBsiFbsad9OFBQACV9FMqPhwdjNj5QEAoQ2VtsFy8au7tSj9uJryp0N7gk5/dI2O3x5XoW0pgRIACJY5WikBAKEPlbbBsvnYYS39XkYtuw6G6sScjbZq8o4unVhXcd2BQAkAAAAgHKHSBMuFpkif15LdObU9NRSKVsupjat08pO/V2l3VwIlAAAAgPCFShMsE6pg8p5pQW+1dNA6KRVn9upmUh4AAAAAoQuVJlj2qjhFdMXTJy869LIWPZ5V84HjgTkRk5uu18QneisdOzltSFIvgRIAAABAaEOlCZYbJA1K6rB53+KXnlXb/93p63A5uel6nbz5dp258BLbt26L98X6KcIAAAAAQh8qTbBsV7HFssf2vX4Ml1WEybykBNPJAwAAACBUzh8u+yUlZdEddtqiQy+r5ecvqG3ni5482GdWL9Ppj2zQyfVdtt1cp42o2N11lKILAAAAgFC5cLDcoGKr5XpHOzU5oUX7d6t1z88aPqnP2Wirpj50jaZu+IhOrfxANZvaGu+LJSmyAAAAAAiVlYdLx62WcwNm82v71Pyrsbp0kZ3auEpTHVdp6v1rbZYFWciQpH6WCwEAAABAqHQWLDtNsNzixvaajx1W69GDirz5ulrG9ivydr6qoDm1cZXOtrfr7GWrdPayVdW2Rs6WN2EyRTEFAAAAQKisPlx2m3DZVYvtNx87rJbjR2b+3PTaK+f8e2H5ChWiF8382cXwOF+YHJA0wFIhAAAAAAiVPguXDUSYBAAAAECorHO47JeDJUgIkwAAAAAQ8lA5K1x2mnCZUBUT+jTAkKQUYyYBAAAAECq9EzB7JU3/eDFgjqi4VMoga00CAAAAIFR6P2B2m5/1DfoYeUk580OQBAAAAECo9GnAbDfhcsOs/9aiJXNE0vD0T7wvlqOIAQAAACBUBjdobpDUaX5kAmclRs2PVGyFHI/3xYYpTgAAAAAIlQAAAAAAVKiJQwAAAAAAIFQCAAAAAAiVAAAAAABCJQAAAACAUAkAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAABCJQAAAACAUAkAAAAAIFQCAAAAAAiVAAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAIFQCAAAAAAiVAAAAAABCJQAAAAAg0P4//U9DqP/Pc9YAAAAASUVORK5CYII=','10', '5', 50, 0, '', '', 'T', false, 300, 'R', false, false, 1, false, false, false);
        $this->pdfDocument->Ln();

        $this->writeHorizontalLine();
    }

    private function writeGeneralInfoAndTransactionsPage() {
        if (is_null($this->pdfDocument)) {
            $this->createPDFDocument();
        }

        $columns = array(
            'provider_num', 'group_num', 'payment_date', 'billing_agent', 'payee_name', 'ohip_amount_payable',
            'address', 'cheque_num', 'total_accepted_claims', 'accepted_claims_amount', 'total_rejected_claims',
            'rejected_claims_amount'
        );
        $options = array('date_format' => $this->getDateFormat(), 'address_break' => "\n");

        $data = $this->raFile->getGenericJSON($columns, $options);

        $this->writeGeneralInfoRow(array('Group Number', $data['group_num'], 'Provider Number', $data['provider_num']));
        $this->writeGeneralInfoRow(array('Amount Payable', $data['ohip_amount_payable'], 'Payment Date', $data['payment_date']));
        $this->writeGeneralInfoRow(array('Accepted (' . $data['total_accepted_claims'] . ')', $data['accepted_claims_amount'], 'Rejected (' . $data['total_rejected_claims'] . ')', $data['rejected_claims_amount']));
        $this->writeGeneralInfoRow(array('Payee', $data['payee_name'], 'Address', $data['address']));
        $this->writeGeneralInfoRow(array('Cheque Number', $data['cheque_num'], 'Billing Agent', $data['billing_agent']));

        $this->pdfDocument->ln();
        $this->writeHorizontalLine();

        if (count($this->raFile->RaTransaction)) {
            $this->pdfDocument->setFont('helvetica', 'B', 15);
            $this->pdfDocument->multiCell(0, 0, 'Accounting Transactions', '0', 'L', 0, 1, '', '', true, 0);
            $this->pdfDocument->ln();
            $this->writeTransactionsTable();
        }
    }

    private function writeGeneralInfoRow($cols) {
        $widths = array(40, 49, 40, 49);
        $maxLines = $this->getCellHeight($cols, $widths);

        $this->pdfDocument->setFont('helvetica', 'B', 10);
        $this->pdfDocument->multiCell($widths[0], $maxLines, $cols[0], 0, 'L', 0, 0);

        $this->pdfDocument->setFont('helvetica', '', 10);
        $this->pdfDocument->multiCell($widths[1], $maxLines, $cols[1], 0, 'L', 0, 0);

        $this->pdfDocument->setFont('helvetica', 'B', 10);
        $this->pdfDocument->multiCell($widths[2], $maxLines, $cols[2], 0, 'L', 0, 0);

        $this->pdfDocument->setFont('helvetica', '', 10);
        $this->pdfDocument->multiCell($widths[3], $maxLines, $cols[3], 0, 'L', 0, 0);
        $this->pdfDocument->ln();
    }

    private function writeHorizontalLine() {
        $y = $this->pdfDocument->getY() + 5;
        $this->pdfDocument->Line(PDF_MARGIN_LEFT, $y, $this->pdfDocument->getPageWidth() - PDF_MARGIN_RIGHT, $y, array('width' => 0.2));
        $this->pdfDocument->ln();
        $this->pdfDocument->setY($y + 10);
    }

    private function writeMessagePages() {
        $c = 1;
        foreach ($this->raFile->RaMessage as $rm) {
            $text = trim($rm['message']);
            if ($text) {
                if ($c > 1) {
                    $y = $this->pdfDocument->getY();
                    $this->pdfDocument->setY($y + 10);
                }

                $this->pdfDocument->setFont('helvetica', 'B', 13);
                $this->pdfDocument->multiCell(0, 0, 'Message ' . $c, '0', 'L', 0, 1, '', '', true, 0);

                $this->pdfDocument->setFont('helvetica', '', 10);
                $this->pdfDocument->writeHTML( '<pre>'.$text.'</pre>', true, false, true, false, '');
                //$this->pdfDocument->multiCell(0, 0, $text, '0', 'L', 0, 1, '', '', true, 0);
                $c++;
            }
        }
    }

    private function writeRaItemHeader($headers, $cws) {
        $this->pdfDocument->setFont('helvetica', 'B', 10);
        $maxLines = $this->getCellHeight($headers, $cws);

        foreach ($headers as $key => $txt) {
            $border = 'TBR' . ($key == 0 ? 'L' : '');
            $this->pdfDocument->multiCell($cws[$key], $maxLines, $txt, $border, 'L', 0, 0);
        }

        $this->pdfDocument->ln();
    }

    private function writeRaItemRow($data, $cws, $headers) {
        $this->pdfDocument->setFont('helvetica', '', 10);
        $maxLines = $this->getCellHeight($data, $cws);

        if ($maxLines == -1) {
            $this->pdfDocument->AddPage($this->pageOrientation);
            $this->writeRaItemHeader($headers, $cws);
            $this->pdfDocument->setFont('helvetica', '', 10);
            $maxLines = $this->getCellHeight($data, $cws);
        }

        foreach ($data as $key => $txt) {
            $border = 'BR' . ($key == 'group_num' ? 'L' : '');
            $align = ($key == 'amount_paid' || $key == 'amount_submitted') ? 'R' : 'L';

            $this->pdfDocument->multiCell($cws[$key], $maxLines, $txt, $border, $align, 0, 0);
        }

        $this->pdfDocument->ln();
    }

    private function writeRaItemPages($type) {
        $q = $this->getRaItemQuery($type);

        $columns = array(
            'group_num',
            'provider_num',
            'claim_num',
            'acct_num',
            'hcn_vc',
            'first_name',
            'last_name',
            'serv_num',
            'serv_code',
            'serv_date',
            'amount_submitted',
            'amount_paid',
            'expl_code'
        );
        $headers = array(
            'group_num' => 'Group #',
            'provider_num' => 'Prov #',
            'claim_num' => 'OHIP Claim #',
            'acct_num' => 'Act #',
            'hcn_vc' => 'HCN',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'serv_num' => 'NS',
            'serv_code' => 'Service Code',
            'serv_date' => 'Service Date',
            'amount_submitted' => 'Amt Submitted',
            'amount_paid' => 'Amt Paid',
            'expl_code' => 'Error/Expl Code'
        );
        $widths = array(
            'group_num' => 20,
            'provider_num' => 20,
            'claim_num' => 30,
            'acct_num' => 20,
            'hcn_vc' => 30,
            'first_name' => 25,
            'last_name' => 25,
            'serv_num' => 6,
            'serv_code' => 20,
            'serv_date' => 20,
            'amount_submitted' => 17,
            'amount_paid' => 17,
            'expl_code' => 0
        );

        $options = array('date_format' => $this->getDateFormat());

        $pageNumber = 0;
        $size = 10;
        $builtDataCount = 0;

        $this->writeRaItemHeader($headers, $widths);
        do {
            ini_set('max_execution_time', '120');
            ini_set('memory_limit', '-1');

            $pager = new Doctrine_Pager($q, $pageNumber + 1, $size);
            $data = $pager->execute();
            $totalCount = $pager->getNumResults();

            foreach ($data as $row) {
                $builtDataCount++;
                $this->writeRaItemRow($row->getGenericJSON($columns, $options), $widths, $headers);
            }

            $pageNumber++;
            unset($data);
        } while ($builtDataCount < $totalCount);
    }

    private function writeStatisticsTableHeader($headers, $cws) {
        $this->pdfDocument->setFont('helvetica', 'B', 10);
        $maxLines = $this->getCellHeight($headers, $cws);

        foreach ($headers as $key => $txt) {
            $border = 'TBR' . ($key == 0 ? 'L' : '');
            $this->pdfDocument->multiCell($cws[$key], $maxLines, $txt, $border, 'L', 0, 0);
        }

        $this->pdfDocument->ln();
    }

    private function writeStatisticsTableRow($data, $cws, $headers) {
        $this->pdfDocument->setFont('helvetica', '', 10);
        $maxLines = $this->getCellHeight($data, $cws);

        if ($maxLines == -1) {
            $this->pdfDocument->AddPage($this->pageOrientation);
            $this->writeStatisticsTableHeader($headers, $cws);
            $this->pdfDocument->setFont('helvetica', '', 10);
            $maxLines = $this->getCellHeight($data, $cws);
        }

        foreach ($data as $key => $txt) {
            $border = 'BR' . ($key == 'service_code' ? 'L' : '');
            $this->pdfDocument->multiCell($cws[$key], $maxLines, $txt, $border, 'L', 0, 0);
        }

        $this->pdfDocument->ln();
    }

    private function writeStatisticsTable() {
        $columns = array('service_code', 'accepted', 'rejected');
        $cws = array('service_code' => 25, 'accepted' => 25, 'rejected' => 25);
        $headers = array('service_code' => 'Service Code', 'accepted' => 'Accepted', 'rejected' => 'Rejected');

        $this->writeStatisticsTableHeader($headers, $cws);

        $rs = $this->getStatistics();
        foreach ($rs as $serviceCode => $stats) {
            $this->writeStatisticsTableRow(array('service_code' => $serviceCode) + $stats, $cws, $headers);
        }
    }

    private function writeTransactionsTable() {
        $columns = array('transaction_type', 'transaction_date', 'transaction_amount', 'description');
        $options = array('date_format' => $this->getDateFormat());

        $cws = array(
            'description' => 80,
            'transaction_type' => 50,
            'transaction_date' => 25,
            'transaction_amount' => 0
        );
        $this->writeTransactionTableHeader($cws);
        foreach ($this->raFile->RaTransaction as $rt) {
            $data = $rt->getGenericJSON($columns, $options);
            $this->writeTransactionTableRow($data, $cws);
        }
    }

    private function writeTransactionTableHeader($cws) {
        $maxLines = $this->getCellHeight($data, $cws);

        $this->pdfDocument->setFont('helvetica', 'B', 10);
        $this->pdfDocument->multiCell($cws['description'], $maxLines, 'Description', 'TBLR', 'L', 0, 0);
        $this->pdfDocument->multiCell($cws['transaction_type'], $maxLines, 'Transaction Type', 'TBR', 'L', 0, 0);
        $this->pdfDocument->multiCell($cws['transaction_date'], $maxLines, 'Date', 'TRB', 'L', 0, 0);
        $this->pdfDocument->multiCell($cws['transaction_amount'], $maxLines, 'Amount', 'TRB', 'R', 0, 0);

        $this->pdfDocument->ln();
    }

    private function writeTransactionTableRow($data, $cws) {
        $this->pdfDocument->setFont('helvetica', '', 10);
        $maxLines = $this->getCellHeight($data, $cws);

        if ($maxLines == -1) {
            $this->pdfDocument->AddPage($this->pageOrientation);
            $this->writeTransactionTableHeader($cws);
            $this->pdfDocument->setFont('helvetica', '', 10);
            $maxLines = $this->getCellHeight($data, $cws);
        }

        $this->pdfDocument->multiCell($cws['description'], $maxLines, $data['description'], 'BLR', 'L', 0, 0);
        $this->pdfDocument->multiCell($cws['transaction_type'], $maxLines, $data['transaction_type'], 'BR', 'L', 0, 0);
        $this->pdfDocument->multiCell($cws['transaction_date'], $maxLines, $data['transaction_date'], 'BR', 'L', 0, 0);
        $this->pdfDocument->multiCell($cws['transaction_amount'], $maxLines, $data['transaction_amount'], 'BR', 'R', 0, 0);

        $this->pdfDocument->ln();
    }

    // PRIVATE JSON FUNCTIONS
    private function buildJSON() {
        $this->JSONResultSet = array();

        if (in_array('general', $this->sectionsToDisplay)) {
            $this->buildGeneralInfoJSON();
            $this->buildTransactionJSON();
        }
        if (in_array('accepted', $this->sectionsToDisplay)) {
            $this->buildRaItemJSON('accepted');
        }
        if (in_array('rejected', $this->sectionsToDisplay)) {
            $this->buildRaItemJSON('rejected');
        }
        if (in_array('messages', $this->sectionsToDisplay)) {
            $this->buildRaMessageJSON();
        }
        if (in_array('statistics', $this->sectionsToDisplay)) {
            $this->buildStatisticsJSON();
        }

        if ($this->includeMessageCount) {
            $this->JSONResultSet['messages_count'] = $this->getRaMessageCount();
        }
    }

    private function buildGeneralInfoJSON() {
        $columns = array(
            'provider_num', 'group_num', 'payment_date', 'billing_agent', 'payee_name', 'ohip_amount_payable',
            'address', 'cheque_num', 'total_accepted_claims', 'accepted_claims_amount', 'total_rejected_claims',
            'rejected_claims_amount', 'ra_file_name'
        );
        $options = array('date_format' => $this->getDateFormat());

        $this->JSONResultSet['general_info'] = $this->raFile->getGenericJSON($columns, $options);
    }

    private function buildRaItemJSON($type) {
        $q = $this->getRaItemQuery($type);

        $columns = array('id');
        $options = array('date_format' => $this->getDateFormat());

        $this->JSONResultSet['ra_file_id'] = $this->raFile->getID();
        $this->JSONResultSet['headers'] = array();
        $this->JSONResultSet['data'] = array();

        foreach ($this->raItemResultColumns as $column) {
            $this->JSONResultSet['headers'][$column['rs_key']] = $column['header_text'];
            $columns[] = $column['rs_key'];
        }
        //$this->raItemDefaults['size']
        //$this->raItemDefaults['pageNumber']

        $pager1 = new Doctrine_Pager($q, $this->raItemDefaults['pageNumber'] + 1, $this->raItemDefaults['size']);
        $data1 = $pager1->execute();

        //echo $pager1->getNumResults();

        $pager = new Doctrine_Pager($q, $this->raItemDefaults['pageNumber'] + 1, $pager1->getNumResults());
        $data = $pager->execute();

      


        foreach ($data as $row) {
            $doctorName = $this->getDoctorCode($row, $this->clientID, $this->raItemDefaults['doctor_title_format']);
            
            $this->JSONResultSet['data'][] = $row->getGenericJSON($columns, $options + array('doctor_name' => $doctorName));
        }

        $this->JSONResultSet['count'] = $pager1->getNumResults();
    }

    private function buildRaMessageJSON() {
        foreach ($this->raFile->RaMessage as $rm) {
            $this->JSONResultSet['messages'][] = str_replace("\n", '<br />', $rm['message']);
        }
    }

    private function buildStatisticsJSON() {
        $this->JSONResultSet['statistics'] = $this->getStatistics();
    }

    private function buildTransactionJSON() {
        $columns = array('id', 'transaction_type', 'transaction_date', 'transaction_amount', 'description');
        $options = array('date_format' => $this->getDateFormat());

        $this->JSONResultSet['transactions'] = array();

        foreach ($this->raFile->RaTransaction as $rt) {
            $this->JSONResultSet['transactions'][] = $rt->getGenericJSON($columns, $options);
        }
    }

    // PRIVATE CSV FUNCTIONS
    private function buildCSV() {
        $this->csvNewLine = chr(13);
        $hasData = false;
        $this->writeCSVDocumentHeader();

        if (in_array('general', $this->sectionsToDisplay)) {
            $hasData = true;
            $this->writeCSVGeneralInfoAndTransactions();
        }

        if (in_array('accepted', $this->sectionsToDisplay)) {
            if ($hasData) {
                $this->writeCSVHorizontalLine();
            }
            $hasData = true;

            $this->writeCSVRaItems('accepted');
        }

        if (in_array('rejected', $this->sectionsToDisplay)) {
            if ($hasData) {
                $this->writeCSVHorizontalLine();
            }
            $hasData = true;

            $this->writeCSVRaItems('rejected');
        }

        if (in_array('messages', $this->sectionsToDisplay)) {
            if ($hasData) {
                $this->writeCSVHorizontalLine();
            }
            $hasData = true;

            $this->writeCSVMessages();
        }

        if (in_array('statistics', $this->sectionsToDisplay)) {
            if ($hasData) {
                $this->writeCSVHorizontalLine();
            }
            $hasData = true;

            $this->writeCSVStatistics();
        }
    }

    private function writeCSVDocumentHeader() {
        $headerText = 'Remittance Advice - ' . $this->raFile->Reconcile->getFileName();

        $this->csvDocument .= $headerText . $this->csvNewLine;
        $this->writeCSVHorizontalLine();
    }

    private function writeCSVGeneralInfoAndTransactions() {
        $columns = array(
            'provider_num', 'group_num', 'payment_date', 'billing_agent', 'payee_name', 'ohip_amount_payable',
            'address', 'cheque_num', 'total_accepted_claims', 'accepted_claims_amount', 'total_rejected_claims',
            'rejected_claims_amount'
        );
        $options = array('date_format' => $this->getDateFormat(), 'address_break' => " -- ");
        $data = $this->raFile->getGenericJSON($columns, $options);

        $this->writeCSVText(array('Group Number', $data['group_num']));
        $this->writeCSVText(array('Provider Number', $data['provider_num']));
        $this->writeCSVText(array('Amount Payable', $data['ohip_amount_payable']));
        $this->writeCSVText(array('Payment Date', $data['payment_date']));
        $this->writeCSVText(array('Accepted (' . $data['total_accepted_claims'] . ')', $data['accepted_claims_amount']));
        $this->writeCSVText(array('Rejected (' . $data['total_rejected_claims'] . ')', $data['rejected_claims_amount']));
        $this->writeCSVText(array('Payee', $data['payee_name']));
        $this->writeCSVText(array('Address', $data['address']));
        $this->writeCSVText(array('Cheque Number', $data['cheque_num']));
        $this->writeCSVText(array('Billing Agent', $data['billing_agent']));

        $this->writeCSVHorizontalLine();

        if (count($this->raFile->RaTransaction)) {
            $this->writeCSVText(array('Accounting Transactions'));
            $this->csvDocument .= $this->csvNewLine;
            $this->writeCSVTransactionsTable();
        }
    }

    private function writeCSVText($data) {
        foreach ($data as $idx => $col) {
            if ($idx != 0) {
                $this->csvDocument .= $this->csvSeparator;
            }

            $this->csvDocument .= $this->csvQuote . $col . $this->csvQuote;
        }
        $this->csvDocument .= $this->csvNewLine;
    }

    private function writeCSVTransactionsTable() {
        $columns = array('description', 'transaction_type', 'transaction_date', 'transaction_amount');
        $options = array('date_format' => $this->getDateFormat());

        $this->writeCSVText(array('Description', 'Transaction Type', 'Date', 'Amount'));
        foreach ($this->raFile->RaTransaction as $rt) {
            $data = $rt->getGenericJSON($columns, $options);
            $this->writeCSVText(array_values($data));
        }
    }

    private function writeCSVHorizontalLine() {
        $this->csvDocument .= $this->csvNewLine;
        for ($a = 0; $a <= 100; $a++) {
            $this->csvDocument .= '* ';
        }
        $this->csvDocument .= $this->csvNewLine;
        $this->csvDocument .= $this->csvNewLine;
    }

    private function writeCSVMessages() {
        $c = 1;
        foreach ($this->raFile->RaMessage as $rm) {
            $text = trim($rm['message']);
            if ($text) {
                if ($c > 1) {
                    $this->csvDocument .= $this->csvNewLine;
                }

                $this->csvDocument .= 'Message ' . $c . $this->csvNewLine;

                // break the text up by newline
                if (strpos($text, "\n") !== false) {
                    $lines = explode("\n", $text);
                } else {
                    $lines = array($text);
                }
                foreach ($lines as $line) {
                    $this->csvDocument .= $this->csvQuote . str_replace($this->csvQuote, $this->csvQuote . $this->csvQuote, $line) . $this->csvQuote . $this->csvNewLine;
                }
                $c++;
            }
        }
    }

    private function writeCSVRaItemHeader($type, $headers) {
        if ($type == 'accepted') {
            $this->csvDocument .= 'Accepted Claims' . $this->csvNewLine;
        } else {
            $this->csvDocument .= 'Rejected Claims' . $this->csvNewLine;
        }

        $this->writeCSVText($headers);
    }

    private function writeCSVRaItems($type) {
        $q = $this->getRaItemQuery($type);

        $columns = array(
            'group_num',
            'provider_num',
            'claim_num',
            'acct_num',
            'hcn_vc',
            'first_name',
            'last_name',
            'serv_num',
            'serv_code',
            'serv_date',
            'amount_submitted',
            'amount_paid',
            'expl_code'
        );
        $headers = array(
            'group_num' => 'Group #',
            'provider_num' => 'Prov #',
            'claim_num' => 'OHIP Claim #',
            'acct_num' => 'Act #',
            'hcn_vc' => 'HCN',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'serv_num' => 'NS',
            'serv_code' => 'Service Code',
            'serv_date' => 'Service Date',
            'amount_submitted' => 'Amt Submitted',
            'amount_paid' => 'Amt Paid',
            'expl_code' => 'Expl Code'
        );

        $options = array('date_format' => $this->getDateFormat());

        $pageNumber = 0;
        $size = 10;
        $builtDataCount = 0;

        $this->writeCSVRaItemHeader($type, array_values($headers));

        do {
            ini_set('max_execution_time', '120');
            ini_set('memory_limit', '-1');

            $pager = new Doctrine_Pager($q, $pageNumber + 1, $size);
            $data = $pager->execute();
            $totalCount = $pager->getNumResults();

            foreach ($data as $row) {
                $builtDataCount++;
                $this->writeCSVText(array_values($row->getGenericJSON($columns, $options)));
            }
            $pageNumber++;
            unset($data);
        } while ($builtDataCount < $totalCount);
    }

    private function writeCSVStatistics() {
        $this->csvDocument .= 'Service Code Statistics' . $this->csvNewLine;

        $headers = array('service_code' => 'Service Code', 'accepted' => 'Accepted', 'rejected' => 'Rejected');
        $columns = array('service_code', 'accepted', 'rejected');

        $this->writeCSVText(array_values($headers));

        $rs = $this->getStatistics();
        foreach ($rs as $serviceCode => $stats) {
            $this->writeCSVText(array_values(array('service_code' => $serviceCode) + $stats));
        }
    }

    // PRIVATE DATABASE FUNCTIONS
    private function getDateFormat() {
        if ($this->dateFormat) {
            return $this->dateFormat;
        } else {
            $this->dateFormat = ClientParamTable::getParamOrDefaultValueForClient($this->clientID, 'date_format');
            return $this->dateFormat;
        }
    }

    private function getDoctorCode($raItem, $clientID, $titleFormat) {
        $doctors_key = '';

        if (!array_key_exists($doctors_key, $this->doctors)) {
            $doctor = Doctrine_Query::create()
                    ->from('Doctor d')
                    ->addWhere('d.client_id = (?)', $clientID)
                    ->addWhere('d.group_num = (?)', $raItem->group_num)
                    ->addWhere('d.provider_num = (?)', $raItem->provider_num)
                    ->orderBy('d.primary_profile')
                    ->fetchOne();

            if ($doctor instanceof Doctor) {
                $this->doctors[$doctors_key] = $doctor->getCustomToString('getDoctorCode');
            } else {
                $this->doctors[$doctors_key] = '';
            }
        }

        return $this->doctors[$doctors_key];
    }
    
    private function getDoctorName($raItem, $clientID, $titleFormat) {
        $doctors_key = $raItem->group_num . '-' . $raItem->provider_num;

        if (!array_key_exists($doctors_key, $this->doctors)) {
            $doctor = Doctrine_Query::create()
                    ->from('Doctor d')
                    ->addWhere('d.client_id = (?)', $clientID)
                    ->addWhere('d.group_num = (?)', $raItem->group_num)
                    ->addWhere('d.provider_num = (?)', $raItem->provider_num)
                    ->orderBy('d.primary_profile')
                    ->fetchOne();

            if ($doctor instanceof Doctor) {
                $this->doctors[$doctors_key] = $doctor->getCustomToString($titleFormat);
            } else {
                $this->doctors[$doctors_key] = '';
            }
        }

        return $this->doctors[$doctors_key];
    }

    private function getRaFile() {

        $q = Doctrine_Query::create()
                ->from('RaFile rf')
                ->leftJoin('rf.Reconcile r')
                ->leftJoin('r.ReconcileClient rc')
                ->addWhere('rf.reconcile_id = (?)', $this->reconcileID)
                ->addWhere('r.status = (?)', ReconcileTable::FILE_STATUS_PROCESSED)
                ->addWhere('rc.client_id = (?)', $this->clientID);

        if (in_array('general', $this->sectionsToDisplay)) {
            $q->leftJoin('rf.RaTransaction rt');
        }
        if (in_array('messages', $this->sectionsToDisplay)) {
            $q->leftJoin('rf.RaMessage rm');
        }
        $this->raFile = $q->fetchOne();

        if (!$this->raFile instanceof RaFile) {
            throw new Exception('Invalid RaFile object requested');
        }

        $this->headerText = $this->raFile->Reconcile->getFileName();
    }

    private function getRaItemQuery($type) {
        $q = Doctrine_Query::create()
                ->from('RaItem ri')
                ->addWhere('ri.ra_file_id = (?)', $this->raFile->getId())
                ->addWhere('ri.ra_claim_type = (?)', ($type == 'accepted' ? 1 : 2));

        if (is_array($this->raItemDefaults) && array_key_exists('sort', $this->raItemDefaults) && is_array($this->raItemDefaults['sort']) && count($this->raItemDefaults['sort'])) {
            $cols = $this->raItemResultColumns;
            foreach ($this->raItemDefaults['sort'] as $colNum => $order) {
                if (array_key_exists($colNum, $cols)) {
                    if (is_array($cols[$colNum]['column'])) {
                        foreach ($cols[$colNum]['column'] as $sortCol) {
                            $q->addOrderBy($sortCol . ' ' . ($order ? 'desc' : 'asc'));
                        }
                    } else if (array_key_exists($colNum, $cols) && array_key_exists('column', $cols[$colNum])) {
                        $q->addOrderBy($cols[$colNum]['column'] . ' ' . ($order ? 'desc' : 'asc'));
                    }
                }
            }
        } else {
            $q->orderBy('ri.id asc');
        }
        return $q;
    }

    private function getRaMessageCount() {
        return Doctrine_Query::create()
                        ->from('RaMessage rm')
                        ->addWhere('rm.ra_file_id = (?)', $this->raFile->getId())
                        ->count();
    }

    private function getStatistics() {
        $rs = array();

        $stats = Doctrine_Query::create()
                ->select('ri.serv_code, ri.ra_claim_type, count(*)')
                ->from('RaItem ri')
                ->addWhere('ri.ra_file_id = (?)', $this->raFile->getId())
                ->groupBy('ri.serv_code, ri.ra_claim_type')
                ->orderBy('ri.serv_code')
                ->execute(array(), Doctrine::HYDRATE_SCALAR);

        foreach ($stats as $stat) {
            $key = ($stat['ri_ra_claim_type'] == 1 ? 'accepted' : 'rejected');

            if (!array_key_exists($stat['ri_serv_code'], $rs)) {
                $rs[$stat['ri_serv_code']] = array(
                    'accepted' => 0,
                    'rejected' => 0
                );
            }
            $rs[$stat['ri_serv_code']][$key] = $stat['ri_count'];
        }

        return $rs;
    }

}

class RemittanceAdviceReportPDF extends TCPDF {

    protected $footerText = null;
    protected $headerText = null;
    protected $datetime_format = null;

    public function setFooterText($v) {
        $this->footerText = $v;
    }

    public function setHeaderText($v) {
        $this->headerText = $v;
    }

    public function setDateTimeFormat($v) {
        $this->datetime_format = $v;
    }

    public function Header() {
        $title = 'Remittance Advice ' . $this->headerText;

        $this->SetFont('Helvetica', '', 20);
        $this->MultiCell(0, 0, $title, '0', 'L', 0, 1, '', '', true, 0);
        $this->Image('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA5UAAACnCAYAAACB8njoAAAACXBIWXMAAC4jAAAuIwF4pT92AAAgAElEQVR42u3dfXBc5WHv8d/qzW/glXmLcQ0SJgQuwW9JeAkkkTxpCbSlkrNF3dtp481NMjfT2zuIy72dO8kMrP9oJ5O2gzxJKZ00ZU0y7bLpXuRLU0guvV6FJIVAYslOiCHBlsAYA8bW4mvZlmXv/WMfCVmWdvc5e3b3vHw/MxqwvXu055znnH1+53mLFAoFAQAAAADgRBOHAAAAAABAqAQAAAAAECoBAAAAAIRKAAAAAAChEgAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAAIRKAAAAAAChEgAAAABAqAQAAAAAECoBAAAAACBUAgAAAAAIlQAAAAAAQiUAAAAAgFAJAAAAACBUAgAAAABAqAQAAAAA1FFLWHc8ncl2S+o0P+2SNph/ape0foG35SUNz/pzbtZ/R+N9sVGKFAAAAIAwiRQKhTAEyA2Sus3PBkkdNfx1QyZ45iQNEzQBAAAAECr9FyI7TYDsNf+NNvDjjJiAORjvi+UocgAAAAAIld4Mku0mRPZr4e6rjZaXNCgpRcAEAAAAQKj0RpjslpSQtMVnH31M0oAJmOMURQAAAACEyvqGyYSkpGo7PrJetktKMv4SAAAAAKGSMEm4BAAAAECoDFOYbD52WC3Hj0iSml57peRrC8tXqBC9SJJ0auUHCJcAAAAACJUeD5MbVBx72OXG9hYdellNr72i5jffUNORvJpfeqeq7Z2Ntursmkt1duXlOnPp5Tq96v06c+ElbnzUvNnvAcZcAgAAACBU2ofJdhVbJu+pZjutRw+o5dd71DK2Xy27Dtbls59ZvUxnrunQmSvW6NRV61RoW1rN5sYkJZgtFgAAAAChsvJA2S0pJYddXVuPHlDbL55X8569aj5wvOH7c/qjazR1zfXVBsxtKnaJpdUSAAAAAKGyRKAckMPWycUvPau25/696i6ttXI22qqpD12jyQ9/QqdXrHayiTFJvfG+2DBFFwAAAACh8tww2SlpUNJ6qx2YnNDikSG17fyJmvKnfXPgpzau0uTHftPpZD/3xvtiAxRfAAAAAIRKzXR3HZQUDXqYdDFc7lBxrCXdYQEAAACEN1SapUIesXnPkt05tT015OswOV+4nPhUn+3MsSOSugmWAAAAAEIZKtOZbErSlkpfv+jQy1r0/e96dsykG0723qaT67tsJvTJm2DJOEsAAAAA4QmVNoEyMjmhpT/5ntqe/GkoTsqZ1ct0anPMpksswRIAAABAQzR5PVC2Hj2gC9N/F5pAKUnNB45r6dce1bIfPl7pW6KSculMdgNFGgAa8r3WHtD96uTsAgA8FyptAuWS3Tld8FcPB7q7ayltT/5Uyx99UM3HDhMsAcC7watbUiKggTLBGQbg8XtVO0chZKHSJlAue/oftfixp0N/gppfekcX/O3faNGhlwmWAOBNGyT1B3C/kmbfAMCrgbJXEsvshSlUVhooI5MTWv7og2rb+SJnZ/ok5U9r6dce1eKXniVYAoA3Q2WHmc08KBW1TvOdzXcJAC/rl7SF1sqQhErzRVs2UE6Pnwxrd9dyljz6L1ry/JOVBstBLjAAqFuolILVVXS65bWD0wvAi8zQg6459yw0SEsdTnivKliHsvXoAS39xjcDtfZkLSwe/JGa8kd1/Df/sNxLOyTlxFNmAKi19ea/XelMtjveF8v5vKLWPjsgB2GfAB9ehxskdZp6XHuF9blhSeOm/jca74uNBvwwJWf9f/+cP6POarqkiLkgciq2nBEoXTS56fpKgqUkbY/3xRK+L6iRSKe5uVZqtFAojDb4M1f6JVDxZ45EItNfLraGC4XCuEfOpaN9KBQKOZePty/KkQvnvhE8c9zqUPHrlrQzSPfcdCablPTArL/6bLwvlmpQpdovZX443hcbd7iftt9vVoL0QMBBmXB8Xhq0b72SuvVe61u1xkw9PCdp0C/HwuK62T/nrxtyr6piH2zrLeNeXj6wpcYHKuV2oIzeuka3bP+u1Wd5dsvvKP/jfXU7qFMbV+nY7/+J1Xsu/OeH1LLrYMWvL445/cdKguWWdCab89NFtoDEnEpOOVvV+CdWG+ZUNt34zJ2SHnfwWbxwPKaD0S4Hb91hvhTdPN6Vfubp/x3RrCfAknJ1Dk4DLlY0as0T5a2O1/nce27S5y0EiTL7SJk/36YK7lFufb/Z1sem/zevYkvWqPkZNqHLT2XVtkxUc17qFST7TZiM1uBXdKg4/GyLpEfSmewOEy79XifUAt8xSZM9/PT9YVNvGTIPHcIVKs2JXV+ysjY5oSVPfIcWSofadr6os5fldGJd2fI1kM5kh738dAOVKRQKg5FIZMhBReuBSCSS8kDrkdMZ2rwwVmKmi+OswDkmaVDSQFha5lA2VE6XV1+O7zFzIHR4JFTCXVFz/+qac84D25rl8essqfqPWe6R1JPOZAdM+Brw4wOwWROJnRei05lsb7wvNkgpq7+mGp3sXkn3lAuUTMpTvcWPPV3JrLBR+evJDWoTsBo65XYkEumVs1aHrR4ObB3mXrc/EomkTDdthMt85zzh44nSkhUGZwTHdGvWI5KOpjPZwSDNZOy1MJnOZEfNsW7kJFjR6e+udCabMiHN7/epautI8FqonNXttaSlPxgkULpkyaP/otajB8q9bL0ZJwOfKxQKw5K2O3hrTyQS6W7gR3cSavPyz/pTW0y4HDDjOxEOXQtU2HxXKTfjQ+er6EaZTTxUelTsKjmezmSTnHt3ri2PhMkFv7v8cq7NZ+wtdU9mWb2AhEpTASzZL3zJ7hzrULps6Te+qcjkRLmXPcCFFhj9JnDVI9hVLRKJJB1+kfZ7ZYIhC/dIypnxowh2RXFDmWvUb5Il/o3yHD5RFcd6jhIunQegdCY7qOK4Oa8vz/OAT67zfpUff0prpd9DpXnKWXI9ytajB7T4sac58m6fyPxpXfC/U54NFXCXCVpOzuX6SCSSqHOgbHd4gx8pFAopn56i9QTLUCh1fjvMUBC/VH67Vbp7ejenO/ThcthPZdoj19Soii2/cCmkV1if2OLDLr2EyjlKVgAjkxNakv4WR71GWnYd1JLduXIv62KsRGCCZVLF6cJt1bt7ZtneCwvw+5PGKMEy1KHSb2W43PcCFTR0SHrcjLmk1bJ0+Emq2DoZ5Wi4fp+q9JjSWunXUJnOZPtVpml/6U++p+YDxznqNbT4sacrGV85wBdCYDi5aUbrdbM1gWqLg7duL7cuJcESPgiVvhjbU2ImRUIl5tOjYpdY7mvnX0vt6Uw2pRouEUOdp/IASl3Xh6HSnLRkqdcsOvSy2p78KUe8DpY88R3PhArUVqFQGFRx3SJbD9RpplKnk/MkA3SaopIGmbwnkCqZzdgP99qkS/uK8IhK2kXPp/Pqwjk5e5CK8sc3Ibtxqb6cMC30oVJlBs1GJie06PEsR7tOml96p5JusP08wQkMTy4xUsUSIkFc87FDLOsTtApOpa00nh7bU8FMirNf28mZxxyPmDUPCZTFQLm+Rr9iSNIOSVsX+BmSNBLww5ysY/0IDrS4dCGVPGmLR4bo9lpnbU8N6eR1N6nQtnShl0y3ViY5Wv5WKBSGI5HINpVZG3YePZFIpLsW3UxNq5yTisaY6jeZVF7ScIl/d7tlpicSifSa1mX4n03Xv4SH77WVzKQ4e59HOfWY4550Jtse74slCJSu2WG2mYv3xYYtP0+3ihNr9dYw5Nb7GHfL2ey5HelMNhHvi6W4TH0QKst9ITUfO6y2nT/hSNdZU/60lv5gUMd/8w9Lnrt0JjsQ74uNc8R8Lym7AezTBlSbKcTLjrFeaD/quITIcKFQ6K4gIHebY9St6mfxG5BUr1C5KSDjUoMQKj15r7WYSXH2Pnv5ocimeF8sDGV+a7wvlqzg/HaqOBa23Zy76Z9aLG2xJZ3JKqTB0q1AOf1QNVXNvcJcAzlJSVMG+h3WD7xWx3EqIXoK+SZUlryBLH7u+2rKn+ZIN0Dbzhd18ubDOnPhJQu9ZLq/OcuM+FyhUBg3a0E+aPnW9ZFIJOHm0h1mrKaTLidDXlxCxASznN6bNbfXfME5emrq9vGGL0Jl1JQbr533XsuKJhOz+Ei8Lzaq91qWB+c8TOjVe61ZboWNLelMdjzeFwtNl0MzKU+1gXJMUrIWrWmmDPSb2Wir+e5q5DHuVnU9h7rSmWx3SB44NVRTlSc6UapwFlspX+QoN9Di575f7iX0Nw9OsByQN5YYSSqgS4gUCoXxQqGQKhQKnSqOY3F6fOB/XQE477afqZPTHoiwOR7vi6XifbFEvC/WLumzcjbh23zuCcvkPWbVg2on5dka74t11rp75vQ5V/HB0FYVh3/4Rb9HtoFahkpV0EqJxmrb+aKajx0u9ZIOFjMOFCdf5q7NBmy6ijpdQmTYTwfarBO6ycGXcwdLjPi+MukkXHnqXutgJkUpIOOzcF7gSMX7Yt3mfuZGuHwk6MuNmNazB6vYxIikjZV0Y65BuEyacLnDJ/faHhc21cNEYx4OlebkLPikNjI54YtWyuita9TxxR6t2/ZlffgfvnreT8cXe3TR7Tf4+iRXEO4TXArBYLpqNnKJESdfkHn59CmiOd5Orh+uOX9zWmH2Ujl3VAZNZRrBDJe5WeFyrMrNDQZ1hnmzX6kqNrFdUrftBDwun+vReF+sV9JmebvVMunRbWEe1YypLDvjq2d3esUi/cYf3KErP/0ZLb3q+pKvveTjd0mSJsff1r5v/rVef+wpTR095a+T/LNfKfKJiVIzwfaYmduYsCcYEpL2O3jfgCpcWmA+kUgkIedLiPi27BUKhcFIJLJddi20VMzDGSq70plspxnn1MhKcbecj1Hq5PQHP1yalsYBOe/e2WEq8UHsdpiS83GJ2700mVG8LzZozvWgPNYTwTReubnmZy913dqqpvtrycpn6wve7MkWvXWNPvrYP+q6+75SNlDO1tZ+qa677yv6+Pf/j6K3rvHXSc6f1qL9u6s6n/BVyBmVtM3BW3tM91UngbJdzp4CjplupH5nuw90I/S37jqWlVqopqJPqAxHsBw34efeKjZzT9Bats3+OO2OudWLs+OaVssNKrageonbDySiYmyl90Kleaqx4FOaxa/u9uS6lJdtvlm3bP+uVZicL1zesv27umzzzb460W3P/TuhMlySctalxelMwE6XEAnEDd4EeavxKU4DPDyhmvFiWxrZLdCFMUqU23CFywEVJ/JxKmizy6ccvm97vcdPOjjXCa/MkGrukbUI4IRKr4XKcie6+eWfe25HL7r9Bm38asq17X3wS3+pZWtX+uZEN7/0jlqPHij1kp6gjn8II9Od1MkX2HrTjdUmHHXK+RIigwE67Lb7wvXmQyaUVbsEQyMrNtVWbJlkKnzBMlVFsFwflNlgzbIcTh6ebg/p+p3V3iNrsa5mNCyzE/spVHaX+seWn/3KW4HqgqVa/+cPubrNtvZLdd29/81XJ7vtF89XdV7hu2BZryVGkg5v/kG7sY9SOQ8FN85bQ0KlS2OUojyADG2wDO0ySqbMO7luR0TrWL2OdWjKY2BCpTnZC44HWvzqbjXlT3tqJz/437eqrf1S17d7ycfv8tX4yuY9e8u9hC6wweMkuFU87qCKJUS2mS6jQTJMcSNUWgSzhE/uB7U6BvBfsEzK2TIUHQFoHXLScpaX1MvEMNZ6LY913kF57OYweyBUlgseXuz6Ws0YynIuv+MO/4TKA8fLrVnJRRYwZskLJ5WASpcYSTrYdl4BfFLo5xlsYcWt+2RdrwGXn/4TKsMrIWfj9YMQKq2v8UbP9OxTtvfGAdlPMpTkMHsjVJb8Mmn+1VioDuClt3zSV5+3bX/J0N/B4rCB5LQiWXKChSqWEEkSwOBjbgWqej8tT8i9MUqEypAyrW5OAmKXX+sXppXV9toZMpMcwf5Y245bHZD9hFBd1He9ESoX/BJsPnbYk7O+1tLSq67Xos7l5x7UfW979vM2v7qPykLImG6mTsbCLLjESJVLiATyi5bZXENR4emUu5NH1HOslZu/i8pYuIPloKQhj5f3Rl87SUpKXY71drP8zbCDMsk58kCoXHA8ZevBX/tip9/4129r71//z5mfsX/6mibHnQfBxasuOfegemxM6Tmh8sXXCJXhNCB3lxhxuoRIIsDH2HbyElpr/cftMNVTj6flDp/+l9JFUQg9JxVy383bYK5P23WFh7yyNIfPjnW3g2M9uxymLN+7hdbKBoZKsz7lwoHltX2e3tn87h/qh5/epN33/LnGHt4x87P3/of0zO2/pfzuHzra7rKrr6r6s7XsOlifE54/zbjKEDLdTZ08bT1viREz1vIBB9vaYcZ4BpXttcPEPsE/x7WqnNtyvYWIyli4mdBk2zLkxyE2vR69pnlQUWylHJ1VJlOyn/E+wWFvUKhUmVaspnHvPnif2P+iXvj8n+r4nkPz/vvU0VN64fN/6qjFsnXZBb466a1HSwZYKgrBDZYpFac3tzV3iRGn3VeDPq26beVjlFLpO7XoydFbyyU6HD79b9SxgL+k6hTSGsk2dIzQSun4PtXlQvmzrZ/0s0RS40JlycBRr9Y2J176269q6uipkq+ZOnpKr33nG678vkWHXvbssYi8+Xqpf+7gsgg0J8FuZokRM26wx8E2tgZwCZH3rqlia67NtZMP8vEgVDq7vmok6aNjAR8xLUO2wyq6fRR0Si6h52LQhn14X6iLsW2ZjIrl9FzT4taXSJkulQ01sf9FvfX4cxW99sjwLl1d7wrp5ERdf1/zm2+Uu5FuMIOeETCFQiEXiUR2OAiGD0QikZSctVLm5bx10w+B0smkRbk6fLSBSCTSiO4jKdMqHiimglmrh26JWoQ/M2SlVuMfvRgqB9KZbEPKvAlYYTQou7WK/fQwotvh8YDdfapT9utdz1uniPfFxtOZrG2ZTPIwoDGhcsEm4pbjRzy7k28/+28Vv/bM/5uo++drO3KgvpXgkycdn2cfeSASiTzAJT6vfjlrbcw5rFT3B3wJkZSD41KPULm+QccjF9DzXMvKcEc6k03UIJjUsgW004PniDLfmH23qcB3BPiaH2FdSkeSlq8fMzMQl9qeVZms0f03dFzr/tr0lne7vp44+HrFrz3+y9eDf9LLL3lCt6YAq2KJESeVgZEgtlpJxRbKSCQy6DCg8zTbf7otX287iYmrAdDh03+bBcTXUyTgJFDXeX3Wel7zOYpDXe5TJUOoCfY7LLeZ4GzUP1QuXKk8dcKzO3ls3ysVv7bcuMuKD+xrr3j3pJdf8oRBy8E3IPtZ0pwI5OQ8ZmzpsMNAuYPxlL5k87At76CSst7lyrbttTdkWyn2UThAjZgKvO24Sr/UMToJlTVne5/MV9iiaDvkpov7Wf1DpS81okurVQV18mQDfucEpT/ETHfUZI1/zfYgLSFiWiYTkUhkWNJOOe/GNUAJDHyoHHb4tNyVhzBm/KdtZS0p+xmJOykWkP3ySH7pDWV7jx+lKFjfp2zveRV9f5pJfFhexK+hstzkLyhRWX2z/l1u6z2OE54MlinZd9GrVF4+XKsrEol0z/lJRiKRARMkj0p6RNV1+wv6Wp1BrvzYVDCHHT5A6HFpHb9+FWc1rNT0Mgi24YBQCSeh0g/XvHXZZoLDmt+nbCf9s62DbGH9XY+EygomfwnXgT3l6+PBmMrwqFXwG/BBF8+uSCRSmP2jYgvk7J8HJN0jd8aP5RX8tTqDyvaeOGoqmTnZrw3rRhlx9PQ/3hcbl11Xxm6KBiTZTsTmh4q77WfMUwxqfp9KmXtUpSE/5eC8JDktdQiV9DW2PLCHfN1yy5jKkDCtZttd3uyY6OI57xcoYyl9y/b7b3huYLOQqGYx7nQmm5Dd0/+xOWOUbFpbOikaqENg8wNaKWt7n3JyL3Xynt5q7r+ESjT+JOSPchDQSEm5+5Q1GfAlRJzYHtRZcKkEn2/2otwmsNmM7YmqurE9ySorXTaV4w4qYBBjCVH7+9R2h8u12H7vRkWPorqESiqJtToJ4xxaNI5pPXOrZXGI8DRvoExwGHzNpvvrmAsVG0eVmnQm2yu7sZ/5eT7beA2PDQiVCDnTSmk7CZKjOooJora9sfi+rnWoZACyncjEKT9/fFJu+Li1xAhP+AiUQWQzpnZ0gevLpjdAh6l41fr6G5hnjFKOUAlLtFajlqFtqMoMkrJ8vdP7b+i1uLWhwuLFHM1Zml96x88fnwcIIVMoFMYjkUhSxdlNqwlQlJ33bC0UCslGBlo1pgUhF6ST6GA+gfP2P94XG09nsoOyW+Q7YVMZMp+zy4XKlm2Z6fTQ6aLMNwYPFlDL+1RV36PxvlgunckOWf7efgdhlFDp1obOvO9ytWofR9SByNv1nzTs1MoPBP2wbvfADWGDpAd9FCxTkUgk4eCGLzGz6WxjkhIeWDokxfIlDakwD5eoGNmEyq50Jts9e3xmBSHU6h453xileF9sNJ3J+jVQpCyOFxpnNID7RGttbQLimEvXdMqybrPe8v4LN0MlnGs+cJyDUIMvrUZXqCORiF9v+DsdvG+AyXmKa2g1uHUSjQ+V81aYTVizfVqeUAWtYGZttS0OrvWF2HzOLopI6LW7cY343HqKQdn71AbVuZVy1v03lc5kk7Iby5kUyyZZsZ39dcH1tgrLV3A054aKyQlPfq6z0dYw3vBRAYdLjIyFPEiNSdoqqZNASagsM/bHtnxUuhi37XaHysykaPUdwILhXCMB3CfroRzMhFyWbW+mucsdVct2W13c22obKhdsiShEL+JoztF25IA3Q+WaSwmVcPPGmwrhMRqTtE3SpkKh0FkoFFhGJbhsWiCGygTOnOwnxOqvoCLrZiulk+8AKl7hZnv+PT/2fp4JrMIarl3hsDeF2+td206YVsm9ElWEygW/aKaWESqdaD52uP6hsr3swzQqx0DRiAkKO1Rsjdws6SoTJPsZsxj4ilC3W9+RVVRSEmVaQGyf/g9VME7IttLfTWkJdViwXR7CL3WMEUKla2zve/Mtd+TGg4JBy7dtoQW6AaHyzIWXcDQdaDl+pP6hMrqi3IXHDJ4Ig6FCoRAp87OhUCh0FwqFXtMaOWjW9UQ4uDKecs79NSW7p+VRSb0LVOjbHYTKlBv7QWUaTh8o+Gjyk9FaH4uQPHhw0ptiwGFrsdvhVmISwsozjeXrS4aNqY2r1LLrIEfViOSPSCu997nOXnF1qX8e48wBgKOwVGlleUDSA5YVofnCYK8JnZWqaIxSvC82bDkDbCdFhVBZaRn00b4NS+ohVFbNSSjrNBPr1ELe8r7Zn85kaxVyQx0qR0uGlZWXSyJUTmvd8zNF3j1a8jVN+aN1/1xluiqPcuYAwFFYqrSXh22o7Ehnsr3xvtjgPGHThs3rR1T5eFJmvgyvXsvX53y0bznL6zS6wHUaWg57U0j2LZu1NN1bJMUZLc2q+2u5bpFnLr2cIzo7se86qMWDPyr507bzxbp+pjOrl5XrqpzjzAGAJLvp7/OVPsk2r7OdZbl/TmUtIbuxbHnZjScataw80gU2fIEhIbsWH8kHk/RU+Vl7KRnn3beiAdiPJKfS5VBpLDi73elV7+eIetyZa8rWQRhPCYAKs31Isr132lZSuuZ8Jtun/7bdt2z3h1AZPgkH7/FNK565XoZsQyUTu1RdRryoI53J8sCgBqFywS+aMxdeojOrl3FUvRwqr1hT7iU5jhIAqKah0qwTaVth7TeBt1v2XU5tp+cftXx9J0UmPEwZtF3IfqzM+qheZBuCo2Jil+kykpD9zMBexnmtQagsGTrOrL2Oo+phZVqTRxiIDAC1D5VG0vL1W8wSDrbv2+7g3m5b+e+myIRK0sF7Uj7cTyctq/20VjouI17WRTf/eofKK6/mqHrUmWsvZjwlANQmVNqGsOmlFWxnw0zKvoUo6fCz2eikyISDaYHqcvBW34VKhz0KQt9aabqKdgRw12itdDNUmqedCy4Ie/LKdTobbQ31QT0bbdXUxlVWP/U4Zqc3bKzqgQEAhIhVpbmKtfdsA5/trIg7quhyaBN4O2idCUVYaJd9V2pJGvJh19dqwvADpldBWAU1fG0J+XktqaWKC+zBhf5x6kPX1H1WU0+FyjWX6tjv/4nVey7854fUVOM1PievuqFcpYhpsAFQcbbv4jRSxa8bNJX0Ws2QOFDFe0dl19qwQTycDLpBh2V1wK87HO+LpcyaiR0OjlXouks6HG/rt8BMi+U8mhy+r+SXxtQNH+HIeszUxlXlur7u4CgBwEw4sg1fTius4zWscA9V0YJa9rveheMGf4WFlMOwMBaAh9ZOrtH16Ux2IIRFJRnw/UvQK8PFUGnWq1ywW8yplR/w/CywizqX68KbO0Jzok+v/VC5l9BKCQDOwlG1SzGlarQf1W7XdnKfTopOYANlv5wvSB+EVp2U7Mc/S9I9ZgxqWMpJp4LdSikVW+oT3BXO11LFewdUogvs6ds+qubHnvbkTi9bu1JXf/5zamu/WHveTOrU6LuBPslno606ee0tpV5iuyg2ABAqXQqV8b7YaDqT3V5FpX0+Y/G+WLWhkrUqMT0xz4MO3z4UhKE18b7YuOkC+4iT+nI6kx02DTJePs/t0/taxWaSTsqIR+75Nt26++XjLt1eDJWDpW4yJ6+7SW3RITXlT3tup6/+/Od0+W//kSTp2nvf0e57/jzQJ3ly001lzyVLiQDADNsn7W5UFgdcDpVJF7YxXOPjBu8HylSV5TIwY8/M2MqEg3IelZRLZ7LdXg2WJlDmzPnKOdxGp4OyMhTvi3V7YP+Tkh6weEtHOpNNuPDgLlCaqri4RlViHF6hbWklYaZuFnUu16o//qQ+/A9f1cTrr878/cTrr2rdti9r1R9/Uos6lwfyJJ9cX/b+x0UBAHI0SY/cmNXSVDbdemLvSu8T87Ax76BiCf9fB+3pTDZXZaDc6vXWuTqG5NnBFlsAAA/YSURBVJlg6cFz3a3iuPD1VW4qWaf31EKqjmWBUOnkJJxc3+WZ5UVOjb6rtfd/XZd8/C5d/YUvzfz91V/4ki7/7T/S2vu/HshusJObrlehbWmpl4xVOZEDAASJbShys+uWW92pBlzsfTJc4+MH74WMXhMyqml5Hon3xZJBOzYmJG+tIlju9NIYS9NCt1NVzj5tWjp7Ld/mmfqneTC43fJt6734kMC3odL0k19w4LKXWiujt64p+5pla1cG6uSejbbq5M23l3tZkssAAGbUe5Keir9TGxBOnewflSz/hsnOdCY7KOnxKkNG3kHA8FOwTKq6h0mPpDPZVCNnEDXnOie7Lp+l9DsoM16rf6Yc7jfcCJWVFAqvtFbmf7xPz/+XuzU5/vZ5/zY5/rZ2/VlCx/ccCtTJndx0U7llRPL0BweAqkLRqMu/v9qK1naXx8gzA2w4wmRK0n5JPS5ssteNLuEe1yvLruFzbJE03IiWLjOT77BcGgNtwrFtuBrzWv3TtJrarjncQ5d/F0OlKRSlWyvv8MbY/SPf/7l+8Rf/47y/H/nyn+itx58L1Ik9G22tZCwlM1cBwLka1lJpDFZZWU26/HlyNT5+aFyQTJjWqv1yb5Koz4ZhSI15cNNd5bXaoWJ32MF6BJN0JtudzmRHVZxkM+riphMOtpfy6Kl1Ui9OCpKqm/117gFdcJrlE+u61Tq8S80vvdPwHc7v+qX+7aYNet/vfkySdPiZ5wN5Yk/FN5cbS5knVALAuRVtB5UjV0OlWbpgQM66pe2oQQuR7fbWU5I8V643SGo3IajT/LcWC3V/Nky9n+J9sWHT6vdIlZvqUbHFa7uklNuh3IzhTKh2szPbtlJ6tv5pZvgdsPwe6E1nsu2souBSqDQnIVnqJnXirrt1wUsPNz5szZmMJ4iT80xtXKWTV64r+yCACwAItIFIJNKoazxVKBT8WLm0bWUbq9F9NOUwVLpeUTNraFqHmAbN+jmQzmQbVeb767jPiQq6TXbWKDgSKM+v/8qFYCkVW4u3pDPZMRV7LAw6DZimfPSan5qVAxNYbbc/4PH6p+1DvagJ1sl6fEeZ3gU1+d6u9hpucfHDJEtdVKdXrNbknR9W25M/reqXHP/l63p2y+9Yv2euwz9YuIXSdvsnDx4+589N+97Whf/8kNU2mva97cpJOBtt1cSn+iqpCNFKCQRbI1uMcj49ZrahcrRGFdVR02ph0yVxqIbdDodk18rRKfe7BXu9zNdz0pWOOgdGAmX9guX0+b1H0j1muyPmXjO8wP11uiW60/zUc8yZkyDl9bLipKdIvUJltIbnt+rvjxaXL6pEqZ09/rHNat43WlU32Kmjp5T/8b6qP++psYVbKKvdflP+tJp2HWzIlXCq51PlJueZLvwAgHN1eyg8pyxDZS0fFI5aVmQ2yIV1MuF5eRUn5cmF/UDUIFjOtt78TE+i9IAX9tm0hto+3Nju9UmczBAE24d60XQmmwj75JctLm+vX9KuUi84cdfdWnrom2rKn3b8SyY3Xa+m8WLL+VTHVZKk5jffUOTkyZk/S1LTqZNqe/KnOtl7W3Fnx/arZddBtaxYpPff+zm1RVdo/MXdmpqY0DvPPK+Vd2xS/qWXdGVvTJP5o8r/8hdqixYfPh5/7VUtu+JKSdL47j1qX7dWJ948pLcef04dX+zRiTcP6UhueGa7r/z9N3V8zyFdtvnmYth78221r1urQ0/t1Mo7Nmns4R266PYbtPp3epT/5c819vCOqg/+5KbrdfLaW8q9bMhMWw8AOD8M2YatWlVsculMttIWwrEa39dt97ObohR4IwrHLK+2wXLUPFCJhmCXk3V6TyMMyH7yqqS83wpbU00uX1DDkraVes3pFat1qudTVf2eqRs+oqmOqzTVcZVO3HinCstX6NRtt+vkJ39PheUrdOLGOzV53Y06/rHNmrzzwzpx452aev9anW0vBsTf+IM7dPmdcS1d3amWxUu09v6v69p7/6vWfO4+Xdkb08W3fkrtH9yo6LX/ofh3n/6Mlr//Gl1331d0yU23adH7LtV1931FH/zSX2rZ2pW67r6v6Iq7Yur8Qt/MdqddcVdMV9wVU/u6tcXX9fXouvu+okWdy3Xj33xHS1d36vLf+l21rFhU1TE5c+3FmvhE2WWh8ioO1gYAzOKFSXrmUWkFpdYVNdv97KREBdq2eF9sA4Fy3npwTsWHKiNB3k/TSmnbDXOHX8qMyTO2a5F2NGKJmMCGyllfbiUXbz557S2avPPDVf2Syetu1OR1N0qSWvf8TKdXrNbpFavVuudnxb87+GtFJid0dtFiSVLzWwdmWjclqa39Ur06mNarj+7Q5Pjbuvy3/0ivfecbM//+8t9t097kw3p3z7N69X89qneef0GSdPgnP1J+1y9ntnHF3Z8+77Pt+/bfL7jm5RV3f0GSdPHHi5/9hc//qZ6NfUFTR085PhZno606cdfd5WZ7lYqT8/AlAADns14Ko9YTs5Rbssuox3rDtt8bHRSnQBqTtCneF2MITfn7Qrek7QHeTSdlwG9zeTi5ryYJle5eTOOqoDXs+Mc26/RH17jyO1t2HVTzscNqPnZYLWYs48lrb1HL8SNqfaH4nX/mstUzLZWvP/aU3vjXb2vt/V/XRd0b9MaTaUnSa5kdeuXvv6l39zyrG//mO1q2duV5v+uSm25Ty4XvhbfL74xrcrw4yc7oNzJ6d8+z2vjVlC66/YZ5P2tb+6XF0LvsAkmqKkzOhPTP/EedXrG63MuGmJwHAFwLlfVqiRio8t/dqiRbCfsT+4DJS9oa74t1Mn6y8rpwvC+WkLRZ1a1l6TmmV0eP5duG/FZ2KnyoN1eXWcKHUOniichJ2lo2WN4e15lrL3b0O9r2Pq+2ve/N4Npy/Ihajh+Z+fOS55/UBX/1sJoPHJdUbKmcdlH3Bk28/mrxde9bqRMHi7PDnhp9V5fcdrOOHxgtbvPC81v+Dv/kRzN/f/iZJ9TWfqne+fH3Zrab37tHkrT4fe87/73PPKGJ/S8W//9Hz0mS1m37stZt+7Iuuv0GdXyxx/o4nPjM7+rUyg9U8oXQKwCAW6GyXrObpkpUSuu53ttIjY8nPBomJXXG+2JJDoej+vCgit3Bt/ng426r8L7mpCykfHoKnXzu0LbkN9XwQkqqTH/kQttSHYv/Z+tg2fTWQTXlj6opf1SLDr383t+9VWylXHToZTXlj868ftGhl1WIXqSzl62aCZJX3P0FHX7mCb3+2FM68eYhHX7mieK/rfoNXX5nXG/867eV//E+5ffu0Yk3D2nq2IQOP/PEzJjKw888odeeyOrwM0/oUO5p5ffuOWe7b/3LD4t35L17ZraR37tHr3zrIR1+5gkd33NIr3zjL3TxrcXxpRdceaValy+3DpQVTMwjSQnWpAQAV0PQaJ0qpeMlKjaDdby32+5vJ0XKt8Yk3TsdJqk/VH8Nmy7DV0na4cGPuF3SVfG+WH+5c21aKW0nsBnz8ayoTh7abTHHKXRaarz9XvNFFC0XLC9M/13FS40sfuzpmf9v2/nieX+39GuPnvP6uX8ee3jHObOtvvX4c3rr8WLL4d77H9Le+99bY/LXX/3WzP//9D/92TnvOee/em5m27PNfv/0aw5+699m/m32v9coUG5ltlcAKFlRapf9OMBcnSs298zz98k6foZh2XV5o6XSX0ZUnLV0sNZjhUMcLkcl9ZrAkXQQztw03cshZTnXhpNWuKSPz5mT5UWk4jDAZNjKeFOtT4aKg5VL9id32mIZVhaBcgddVgCgJgFotF4fzlT65k76Ue/13kbrcExRH0OmPG2VtEnSCjOba5JAWZ/r2Yy3XKFii3A9Z4rdIWlzvC/Wbs53xde1efiWsA2vAVi70UlrZb85XqHSUoeLZzidyfarzIKwTlosCZQljShYy4fkavz6WlXCtvrsM4fpc4/67BpIeehY+6VM2LApO2rATNopnfu0PNWAc7qVMl+T+0muDtfheEACo22Z8Ox93jS8DEgaMK2XvSo2xHTLvXUuR8zxyrnQa63dQcDyfZkzOeZes/+2x2u8gvK51SO7WvV9KFIoFOrySdOZbKJcsJSkyOSElv5gcKZbK4rORlt1Kr5ZJ69cV8nLxyRtYBwEAARHOpPNqbg23FC8L9bNEQECe613qjguuduEk9kt/11z6nujc4LBuKRhZupFvdUtVJqLJKUK+yUvef5JLR78EWdI0pnVy3Qi/seVLBsiFbsad9OFBQACV9FMqPhwdjNj5QEAoQ2VtsFy8au7tSj9uJryp0N7gk5/dI2O3x5XoW0pgRIACJY5WikBAKEPlbbBsvnYYS39XkYtuw6G6sScjbZq8o4unVhXcd2BQAkAAAAgHKHSBMuFpkif15LdObU9NRSKVsupjat08pO/V2l3VwIlAAAAgPCFShMsE6pg8p5pQW+1dNA6KRVn9upmUh4AAAAAoQuVJlj2qjhFdMXTJy869LIWPZ5V84HjgTkRk5uu18QneisdOzltSFIvgRIAAABAaEOlCZYbJA1K6rB53+KXnlXb/93p63A5uel6nbz5dp258BLbt26L98X6KcIAAAAAQh8qTbBsV7HFssf2vX4Ml1WEybykBNPJAwAAACBUzh8u+yUlZdEddtqiQy+r5ecvqG3ni5482GdWL9Ppj2zQyfVdtt1cp42o2N11lKILAAAAgFC5cLDcoGKr5XpHOzU5oUX7d6t1z88aPqnP2Wirpj50jaZu+IhOrfxANZvaGu+LJSmyAAAAAAiVlYdLx62WcwNm82v71Pyrsbp0kZ3auEpTHVdp6v1rbZYFWciQpH6WCwEAAABAqHQWLDtNsNzixvaajx1W69GDirz5ulrG9ivydr6qoDm1cZXOtrfr7GWrdPayVdW2Rs6WN2EyRTEFAAAAQKisPlx2m3DZVYvtNx87rJbjR2b+3PTaK+f8e2H5ChWiF8382cXwOF+YHJA0wFIhAAAAAAiVPguXDUSYBAAAAECorHO47JeDJUgIkwAAAAAQ8lA5K1x2mnCZUBUT+jTAkKQUYyYBAAAAECq9EzB7JU3/eDFgjqi4VMoga00CAAAAIFR6P2B2m5/1DfoYeUk580OQBAAAAECo9GnAbDfhcsOs/9aiJXNE0vD0T7wvlqOIAQAAACBUBjdobpDUaX5kAmclRs2PVGyFHI/3xYYpTgAAAAAIlQAAAAAAVKiJQwAAAAAAIFQCAAAAAAiVAAAAAABCJQAAAACAUAkAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAABCJQAAAACAUAkAAAAAIFQCAAAAAAiVAAAAAAAQKgEAAAAAhEoAAAAAAKESAAAAAECoBAAAAAAQKgEAAAAAIFQCAAAAAAiVAAAAAABCJQAAAAAg0P4//U9DqP/Pc9YAAAAASUVORK5CYII=','10', '5', 50, 0, '', '', 'T', false, 300, 'R', false, false, 1, false, false, false);
        $this->Ln();
    } 

    public function Footer() {
        $this->SetY(-15);
        $this->Cell(20, 10, 'Printed on:' .date($this->datetime_format), 0, false, 'L', 0, '', 0, false, 'T', 'M');
        if ($this->footerText) {
        $this->Cell(150, 10, $this->footerText,  0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
        $this->Cell(0, 10,  'Page ' . $this->getAliasNumPage() . ' of ' . $this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }
 
    // public function Footer() {
    //     $this->SetY(-15);
    //     if ($this->footerText) {
    //         $this->Cell(.5, 10, $this->footerText, 0, false, 'L', 0, '', 0, false, 'T', 'M');
    //     }
    //     $this->Cell(0, 10, 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    //     $this->Cell(0, 10, date($this->datetime_format), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    // }

}
