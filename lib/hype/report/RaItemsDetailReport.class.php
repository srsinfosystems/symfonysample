<?php

class RaItemsDetailReport
{
 	protected $includeSelectCell = false;
 	protected $resultColumns = array(
 		array(
 			'column' => 'ri.group_num',
 			'rs_key' => 'group_num',
 			'header_text' => 'Group Num'
 		),
 		array(
 			'column' => 'ri.provider_num',
 			'rs_key' => 'provider_num',
 			'header_text' => 'Provider Num',
 		),
 		array(
 			'rs_key' => 'doctor_name',
 			'header_text' => 'Doctor Name'
 		),
 		array(
 			'column' => 'ri.claim_num',
 			'rs_key' => 'claim_num',
 			'header_text' => 'OHIP Claim Number'
 		),
 		array(
 			'column' => 'ri.claim_id',
 			'rs_key' => 'claim_id',
 			'header_text' => 'HYPE Claim ID'
 		),
 		array(
 			'column' => 'ri.item_id',
 			'rs_key' => 'item_id',
 			'header_text' => 'HYPE Item ID'
 		),
 		array(
 			'column' => 'ri.acct_num',
 			'rs_key' => 'acct_num',
 			'header_text' => 'Account Number'
 		),
 		array(
 			'rs_key' => 'hcn_vc',
 			'header_text' => 'Health Card Number'
 		),
 		array(
 			'column' => 'ri.first_name',
 			'rs_key' => 'first_name',
 			'header_text' => 'First Name'
 		),
 		array(
 			'column' => 'ri.last_name',
 			'rs_key' => 'last_name',
 			'header_text' => 'Last Name'
 		),
 		array(
 			'column' => 'ri.serv_code',
 			'rs_key' => 'serv_code',
 			'header_text' => 'Service Code'
 		),
 		array(
 			'column' => 'ri.serv_date',
 			'rs_key' => 'serv_date',
 			'header_text' => 'Service Date'
 		),
 		array(
 			'column' => 'ri.amount_submitted',
 			'rs_key' => 'amount_submitted',
 			'header_text' => 'Amount Submitted'
 		),
 		array(
 			'column' => 'ri.amount_paid',
 			'rs_key' => 'amount_paid',
 			'header_text' => 'Amount Paid'
 		),
 		array(
 			'column' => 'ri.expl_code',
 			'rs_key' => 'expl_code',
 			'header_text' => 'Expl Code'
 		),
 	);
 	protected $values;
 	protected $doctors = array();

 	public function __construct($values = array())
 	{
 		$this->values = $values;
 	}
 	public function getData($allRecords = false)
 	{
  		$v = $this->values;

  		$v['ra_file_id'] = $this->getReconcileFileID($v['reconcile_id'], $v['client_id']);

  		if (!$v['ra_file_id']) {
  			return false;
  		}

  		$options = array(
  			'date_format' => $v['date_format'],
  		);
  		$columns = array('id');

  		$rs = array();
  		$rs['ra_file_id'] = $v['ra_file_id'];
  		$rs['headers'] = array();
  		$rs['data'] = array();

  		foreach ($this->resultColumns as $column)
  		{
  			$rs['headers'][$column['rs_key']] = $column['header_text'];
  			$columns[] = $column['rs_key'];
  		}

  		if ($allRecords) {
  			$data = $this->getQuery($v)->execute();
  		}
  		else {
  			$pager = new Doctrine_Pager($this->getQuery($v), $v['pageNumber'] + 1, $v['size']);
  			$data = $pager->execute();
  		}

  		foreach ($data as $row)
  		{
  			$doctorName = $this->getDoctorName($row, $v['client_id'], $v['doctor_title_format']);
  			$rs['data'][] = $row->getGenericJSON($columns, $options + array('doctor_name' => $doctorName));
  		}

 		if (!$allRecords) {
 			$rs['count'] = $pager->getNumResults();
 		}

 		return $rs;
 	}


 	private function getDoctorName($raItem, $clientID, $titleFormat)
 	{
 		$doctors_key = $raItem->group_num . '-' . $raItem->provider_num;

 		if (!array_key_exists($doctors_key, $this->doctors)) {
 			$doctor = Doctrine_Query::create()
	 			->from('Doctor d')
	 			->addWhere('d.client_id = (?)', $clientID)
	 			->addWhere('d.group_num = (?)', $raItem->group_num)
	 			->addWhere('d.provider_num = (?)', $raItem->provider_num)
	 			->orderBy('d.primary_profile')
	 			->fetchOne();

 			if ($doctor instanceof Doctor) {
 				$this->doctors[$doctors_key] = $doctor->getCustomToString($titleFormat);
 			}
 			else {
 				$this->doctors[$doctors_key] = '';
 			}
 		}

 		return $this->doctors[$doctors_key];

 		/*
 		{

 		$arr = $raItem->getRaClaimType() == 1 ? 'accepted' : 'rejected';
 		if (!array_key_exists($raItem->getServCode(), $rs['statistics'])) {
 		$rs['statistics'][$raItem->getServCode()] = array('accepted' => 0, 'rejected' => 0);
 		}
 		$rs['statistics'][$raItem->getServCode()][$arr]++;

 		$rs[$arr][] = $raItem->getGenericJSON($columns, array('date_format' => $this->getUser()->getDateFormat()));
 		}

 		*
 		*/

 	}
 	private function getReconcileFileID($reconcileID, $clientID)
 	{
		$reconcileFile = Doctrine_Query::create()
			->from('RaFile rf')
			->leftJoin('rf.Reconcile r')
			->leftJoin('r.ReconcileClient rc')
			->addWhere('rc.client_id = (?)', $clientID)
			->addWhere('rf.reconcile_id = (?)', $reconcileID)
			->addWhere('r.status = (?)', ReconcileTable::FILE_STATUS_PROCESSED)
			->fetchOne(array(), Doctrine::HYDRATE_ARRAY);

		if ($reconcileFile) {
			return $reconcileFile['id'];
		}
 	}

/*

 /*
 	public function getVisibleFields()
 	{
 		return array('doctor_id', 'group_number', 'provider_number', 'start_date', 'end_date', 'status', 'ack_status');
 	}
 	public function getHeaderColumns()
 	{
 		$rs = array();

 		if ($this->includeSelectCell) {
 			$rs['select'] = '';
 		}
 		foreach ($this->batchesResultColumns as $column)
 		{
 			$rs[$column['rs_key']] = $column['header_text'];
 		}
 		return $rs;
 	}
 	public function getPageSize()
 	{
 		return $this->getValue('size') ? $this->getValue('size') : 25;
 	}
 	public function getSortOrder()
 	{
 		$s = $this->getValue('sort');

 		$rs = '[';
 		if (is_array($s)) {
 			foreach ($s as $a => $b)
 			{
 				if (strlen($rs) > 1) {
 					$rs .= ',';
 				}
 				$rs .= '[' . $a . ',' . $b . ']';
 			}
 		}

 		$rs .= ']';
 		return $rs;
 	}
 	private function getAppropriateClaimItemIDs($values)
 	{
 		$claimItemIDs = array();
 		$claimIDs = array();
 		$date = null;

 		$batchDate = Doctrine_Query::create()
 			->select('b.id, b.batch_date, b.created_at')
 			->from('Batch b')
 			->addWhere('b.id = (?)', ($values['batchID']))
 			->fetchOne(array(), Doctrine::HYDRATE_SCALAR);

 		if (is_array($batchDate)) {
 			if (substr($batchDate['b_batch_date'], 0, 10) == substr($batchDate['b_created_at'], 0, 10)) {
 				$date = $batchDate['b_created_at'];
 			}
 			else {
 				$date = $batchDate['b_batch_date'] . ' 23:59:59.000';
 			}
 		}

 		$rs = Doctrine_Query::create()
	 		->select('cv.id')
	 		->distinct()
	 		->from('ClaimVersion cv')
	 		->addWhere('cv.batch_id = (?)', $values['batchID'])
	 		->addWhere('cv.deleted = (?) OR cv.deleted is null', false)
	 		->execute(array(), Doctrine::HYDRATE_SCALAR);

 		foreach ($rs as $row)
 		{
 			$claimIDs[] = $row['cv_id'];
 		}

 		if (count($claimIDs)) {
 			$q = Doctrine_Query::create()
	 			->select('civ.id')
	 			->distinct()
	 			->from('ClaimItemVersion civ')
	 			->addWhere('civ.status != (?)', ClaimItemTable::STATUS_DELETED)
	 			->whereIn('civ.claim_id', $claimIDs);

 			if ($date) {
 				$q->addWhere('civ.created_at <= (?)', $date);
 			}

 			foreach ($q->execute(array(), Doctrine::HYDRATE_SCALAR) as $row)
 			{
 				$claimItemIDs[] = $row['civ_id'];
 			}
 			return $claimItemIDs;
 		}
 	}
*/

 	private function getQuery($values, $use_sorting = true)
 	{
		$q = Doctrine_Query::create()
			->from('RaItem ri')
			->addWhere('ri.ra_file_id = (?)', $values['ra_file_id'])
			->addWhere('ri.ra_claim_type = (?)', ($values['mode'] == 'accepted' ? 1 : 2));

		if ($use_sorting && array_key_exists('sort', $values) && is_array($values['sort'])) {
			$cols = $this->resultColumns;
			foreach ($values['sort'] as $colNum => $order)
			{
				if (array_key_exists($colNum, $cols)) {
					if (is_array($cols[$colNum]['column'])) {
						foreach ($cols[$colNum]['column'] as $sortCol)
						{
							$q->addOrderBy($sortCol. ' ' . ($order ? 'desc' : 'asc'));
						}
					}
					else {
						$q->addOrderBy($cols[$colNum]['column'] . ' ' . ($order ? 'desc' : 'asc'));
					}
				}
			}
		}
		else {
			$q->orderBy('ci.id asc');
		}
		return $q;
 	}
}