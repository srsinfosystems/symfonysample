<?php

class BatchDetailsReport {

    protected $includeSelectCell = true;
    protected $resultColumns = array(
        array(
            'column' => 'c.acct_num',
            'rs_key' => 'account_num',
            'header_text' => 'Account Num'
        ),
        array(
            'column' => 'c.id',
            'rs_key' => 'claim_id',
            'header_text' => 'Claim ID',
        ),
        array(
            'column' => 'ci.id',
            'rs_key' => 'id',
            'header_text' => 'Item ID',
        ),
        array(
            'column' => 'c.pay_prog',
            'rs_key' => 'pay_prog',
            'header_text' => 'Claim Type',
        ),
        array(
            'column' => array('c.lname', 'c.fname'),
            'rs_key' => 'last_first',
            'header_text' => 'Patient Name',
        ),
        array(
            'column' => 'ci.service_code',
            'rs_key' => 'service_code',
            'header_text' => 'Service Code',
        ),
        array(
            'column' => 'ci.service_date',
            'rs_key' => 'service_date',
            'header_text' => 'Service Date',
        ),
        array(
            'column' => 'ci.fee_subm',
            'rs_key' => 'fee_subm',
            'header_text' => 'Fee Submitted',
        ),
        array(
            'column' => 'ci.fee_paid',
            'rs_key' => 'fee_paid',
            'header_text' => 'Fee Paid',
        ),
        array(
            'column' => 'ci.status',
            'rs_key' => 'status_text',
            'header_text' => 'Status',
        ),
        array(
            'column' => 'c.batch_number',
            'rs_key' => 'batch_number',
            'header_text' => 'Current Batch Number',
        ),
        array(
            'column' => 'ci.ra',
            'rs_key' => 'ra',
            'header_text' => 'RA File'
        ),
    );
    protected $values;

    public function __construct($values = array()) {
        $this->values = $values;
    }

    public function getData($allRecords = false) {
        $v = $this->values;
        $v['claimItemIDs'] = $this->getAppropriateClaimItemIDs($v);

        if (!count($v['claimItemIDs'])) {
            return false;
        }

        $columns = array('id');
        $options = array(
            'date_format' => $v['date_format'],
        );

        $rs['headers'] = array();
        $rs['data'] = array();

        foreach ($this->resultColumns as $column) {
            $rs['headers'][$column['rs_key']] = $column['header_text'];
            $columns[] = $column['rs_key'];
        }

        if ($allRecords) {
            $data = $this->getQuery($v)->execute();
        } else {
            $pager = new Doctrine_Pager($this->getQuery($v), $v['pageNumber'] + 1, 500);
            $data = $pager->execute();
        }

        foreach ($data as $row) {
            $rs['data'][] = $row->getGenericJSON($columns, $options);
        }

        if (!$allRecords) {
            $rs['count'] = $pager->getNumResults();
        }

        return $rs;
    }

    private function getAppropriateClaimItemIDs($values) {
        $claimItemIDs = array();
        $claimIDs = array();
        $date = null;

        $batchDate = Doctrine_Query::create()
                ->select('b.id, b.batch_date, b.created_at')
                ->from('Batch b')
                ->addWhere('b.id = (?)', ($values['batchID']))
                ->addWhere('b.client_id = (?)', $values['client_id'])
                ->fetchOne(array(), Doctrine::HYDRATE_SCALAR);

        if (is_array($batchDate)) {
            if (substr($batchDate['b_batch_date'], 0, 10) == substr($batchDate['b_created_at'], 0, 10)) {
                $date = $batchDate['b_created_at'];
            } else {
                $date = $batchDate['b_batch_date'] . ' 23:59:59.000';
            }
        }

        $rs = Doctrine_Query::create()
                ->select('cv.id')
                ->distinct()
                ->from('ClaimVersion cv')
                ->addWhere('cv.batch_id = (?)', $values['batchID'])
                ->addWhere('cv.deleted = (?) OR cv.deleted is null', false)
                ->execute(array(), Doctrine::HYDRATE_SCALAR);

        foreach ($rs as $row) {
            $claimIDs[] = $row['cv_id'];
        }

        if (count($claimIDs)) {
            $q = Doctrine_Query::create()
                    ->select('civ.id')
                    ->distinct()
                    ->from('ClaimItemVersion civ')
                    ->addWhere('civ.status != (?)', ClaimItemTable::STATUS_DELETED)
                    ->whereIn('civ.claim_id', $claimIDs);

            if ($date) {
                $q->addWhere('civ.created_at <= (?)', $date);
            }

            foreach ($q->execute(array(), Doctrine::HYDRATE_SCALAR) as $row) {
                $claimItemIDs[] = $row['civ_id'];
            }

            return $claimItemIDs;
        }
    }

    private function getQuery($values, $use_sorting = true) {
        if (count($values['claimItemIDs'])) {
            $q = Doctrine_Query::create()
                    ->from('ClaimItem ci')
                    ->leftJoin('ci.Claim c')
                    ->addWhere('c.client_id = (?)', $values['client_id'])
                    ->whereIn('ci.id', $values['claimItemIDs']);

            if ($use_sorting && array_key_exists('sort', $values) && is_array($values['sort'])) {
                $cols = $this->resultColumns;
                foreach ($values['sort'] as $colNum => $order) {
                    if (array_key_exists($colNum, $cols)) {
                        if (is_array($cols[$colNum]['column'])) {
                            foreach ($cols[$colNum]['column'] as $sortCol) {
                                $q->addOrderBy($sortCol . ' ' . ($order ? 'desc' : 'asc'));
                            }
                        } else {
                            $q->addOrderBy($cols[$colNum]['column'] . ' ' . ($order ? 'desc' : 'asc'));
                        }
                    }
                }
            } else {
                $q->orderBy('ci.id asc');
            }
            return $q;
        }
    }

}
