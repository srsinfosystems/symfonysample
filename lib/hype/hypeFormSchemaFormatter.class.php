<?php

class hypeFormSchemaFormatter extends sfWidgetFormSchemaFormatter
{
	protected
	$rowFormat              = "<tr>\n  <th>%label%</th>\n  <td %error_class%>%field%%error%%help%%hidden_fields%</td>\n</tr>\n",
	$errorListFormatInARow = "\n<ul class=\"error_list\">%errors%</ul>\n",
	$errorRowFormat        = "<tr><td colspan=\"2\">\n%errors%</td></tr>\n",
	$helpFormat            = '<div style="clear:left">%help%</div>',
	$errorClass            = "class=\"%class%\"",
	$decoratorFormat       = "<table>\n  %content%</table>";

	public function formatRow($label, $field, $errors = array(), $help = '', $hiddenFields = null)
	{
		return strtr($this->getRowFormat(), array(
				'%label%'         => $label,
				'%field%'         => $field,
				'%error%'         => $this->formatErrorsForRow($errors),
				'%help%'          => $this->formatHelp($help),
				'%error_class%'   => $this->formatErrorClass($errors),
				'%hidden_fields%' => null === $hiddenFields ? '%hidden_fields%' : $hiddenFields,
		));
	}
	public function formatErrorClass($errors)
	{
		if (!$errors) {
			return '';
		}

		return strtr($this->getErrorClassFormat(), array('%class%' => 'error'));
	}
	public function getErrorClassFormat()
	{
		return $this->errorClass;
	}
	public function formatErrorsDiv($errors)
	{
		if (!$errors) {
			return '';
		}

		$rs = "<div class='alert alert-danger btn-squared'>";

		foreach ($errors as $count => $data)
		{
			if ($count != 0) {
				$rs .= '<br />';
			}
			if ($data[0]) {
				$rs .= "<b>" . $data[0] . '</b>: ' . $data[1];
			}
			else {
				$rs .= $data[1];
			}
		}

		$rs .= "</div>";
		return $rs;
	}
}
?>