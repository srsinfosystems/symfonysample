<?php

class hypeComponents extends sfComponents
{
    /**
     * @return hypeUser The current sfUser implementation instance
     */
    public function getUser()
    {
        return $this->context->getUser();
    }

}