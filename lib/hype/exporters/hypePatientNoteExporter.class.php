<?php

class hypePatientNoteExporter extends hypeExporterBase
{
    public function run()
    {
        $this->openFile();

        if ($this->dataExportRequest->getStatus() == DataExportRequestTable::FILE_STATUS_UPLOADED) {
            $this->dataExportRequest->setStatus(DataExportRequestTable::FILE_STATUS_PROCESSING);
            $this->dataExportRequest->save();
        }

        do {
            if (!$this->dataExportRequest->getHasHeader()) {
                $this->writeLine($this->generateHeader());
                $this->flushFile();

                $this->dataExportRequest->setHasHeader(true);
                $this->dataExportRequest->save();
            }

            $notes = $this->getNextRecord();
            foreach ($notes as $note)
            {
                if ($note instanceof Notes) {
                    $this->writeLine($this->generateLine($note));

                    $this->flushFile();
                    $this->dataExportRequest->setRecordId($note->get('id'));
                    $this->dataExportRequest->save();

                    $this->count++;
                }

                if ($this->dataExportRequest->getRecordCount() >= $this->fileLimit) {
                    $this->splitFile();
                }
            }
        } while ($notes->count() && $this->count <= $this->recordLimit);

        $this->closeFile();
        $this->dataExportRequest->save();

        if (!$notes->count()) {
            $this->setFile();
            if ($this->dataExportRequest->getEmailAddress()) {
                $this->sendNotificationEmail();
            }
        }
    }

    private function generateHeader()
    {
        return array(
            'PATIENT ID',
            'LAST NAME',
            'FIRST NAME',
            'HEALTH CARD NUM',
            'DATE',
            'NOTE',
        );
    }

    private function getNextRecord()
    {
        return Doctrine_Query::create()
            ->from('Notes n')
            ->leftJoin('n.Patient p')
            ->addWhere('n.client_id = (?)', array($this->dataExportRequest->getClientId()))
            ->addWhere('n.id > (?)', array($this->dataExportRequest->getRecordId()))
            ->limit(50)
            ->execute();
    }

    private function generateLine(Notes $note)
    {
        $rs = array(
            'PATIENT ID' => $note->getPatientId(),
            'LAST NAME' => $note->Patient->getLname(),
            'FIRST NAME' => $note->Patient->getFname(),
            'HEALTH CARD NUM' => $note->Patient->getHCN(),
            'DATE' => $note->get('created_at'),
            'NOTE' => $note->getNote(),
        );

        return $rs;
    }
}

