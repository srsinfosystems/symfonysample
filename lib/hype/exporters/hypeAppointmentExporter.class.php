<?php

class hypeAppointmentExporter extends hypeExporterBase
{
    public function run()
    {
        $this->openFile();

        if ($this->dataExportRequest->getStatus() == DataExportRequestTable::FILE_STATUS_UPLOADED) {
            $this->writeLine($this->generateHeader());
            $this->dataExportRequest->setStatus(DataExportRequestTable::FILE_STATUS_PROCESSING);
            $this->dataExportRequest->save();
        }

        do {
            $attendees = $this->getNextRecord();

            foreach ($attendees as $attendee)
            {
                if ($attendee instanceof AppointmentAttendee) {
                    $this->writeLine($this->generateLine($attendee));
                    $this->dataExportRequest->setRecordId($attendee->get('id'));

                    if ($this->count % 25 == 0) {
                        $this->flushFile();
                        $this->dataExportRequest->save();
                    }
                    $this->count++;
                }
            }
        } while ($attendees->count() && $this->count <= $this->recordLimit);

        $this->closeFile();
        $this->dataExportRequest->save();

        if (!$attendees->count()) {
            $this->dataExportRequest->setStatus(DataExportRequestTable::FILE_STATUS_COMPLETED);
            $this->dataExportRequest->setExportFile(file_get_contents($this->getFilePath()));
            $this->dataExportRequest->setCompletionDate(date(hype::DB_ISO_DATE));
            $this->dataExportRequest->save();
            $this->deleteFile();
            if ($this->dataExportRequest->getEmailAddress()) {
                $this->sendNotificationEmail();
            }
        }
    }
    private function getNextRecord()
    {
        return Doctrine_Query::create()
            ->from('AppointmentAttendee att')
            ->leftJoin('att.Appointment a')
            ->leftJoin('a.Doctor d')
            ->leftJoin('a.Location l')
            ->leftJoin('a.AppointmentType at')
            ->addWhere('a.client_id = (?)', array($this->dataExportRequest->getClientId()))
            ->addWhere('att.client_id = ?', array($this->dataExportRequest->getClientId()))
            ->addWhere('(a.status != (?))', array(AppointmentTable::STATUS_DELETED))
            ->addWhere('att.id > (?)', array($this->dataExportRequest->getRecordId()))
            ->orderBy('att.id asc')
            ->limit(50)
            ->execute();
    }

    private function generateHeader()
    {
        return array(
            'APPOINTMENT ID',
            'CLINIC',
            'DOCTOR GROUP NUM',
            'DOCTOR PROVIDER NUM',
            'DOCTOR LNAME',
            'DOCTOR FNAME',
            'START DATE & TIME',
            'END DATE & TIME',
            'APPOINTMENT TYPE',
            'APPOINTMENT STATUS',

            'PATIENT ID',
            'PATIENT CHART NUM',
            'PATIENT LNAME',
            'PATIENT FNAME',
            'PATIENT HEALTH CARD NUM',

            'SERVICE CODE',
            'DIAG CODE',
            'NOTES',
        );
    }
    private function generateLine(AppointmentAttendee $attendee)
    {
        $appointment = $attendee->Appointment;

        $rs = array(
            'APPOINTMENT ID' => $attendee->get('id'),
            'CLINIC' => $appointment->getLocationId() ? $appointment->Location->getName() : null,
            'DOCTOR GROUP NUM' => $appointment->getDoctorId() ? $appointment->Doctor->getGroupNum() : null,
            'DOCTOR PROVIDER NUM' => $appointment->getDoctorId() ? $appointment->Doctor->getProviderNum() : null,
            'DOCTOR LNAME' => $appointment->getDoctorId() ? $appointment->Doctor->getLname() : null,
            'DOCTOR FNAME' => $appointment->getDoctorId() ? $appointment->Doctor->getFname() : null,
            'START DATE & TIME' => $appointment->getStartDate() ? $appointment->getStartDate() : null,
            'END DATE & TIME' => $appointment->getEndDate() ? $appointment->getEndDate() : null,
            'APPOINTMENT TYPE' => $appointment->getAppointmentTypeId() ? $appointment->AppointmentType->getName() : null,
            'APPOINTMENT STATUS' => $appointment->getStatus() ? AppointmentTable::getStatusString($appointment->getStatus(), true) : null,

            'PATIENT ID' => $attendee->Patient->get('id'),
            'PATIENT CHART NUM' => $attendee->Patient->getPatientNumber(),
            'PATIENT LNAME' => $attendee->Patient->getLname(),
            'PATIENT FNAME' => $attendee->Patient->getFname(),
            'PATIENT HEALTH CARD NUM' => $attendee->Patient->getHCN(),

            'SERVICE CODE' => $appointment->getServiceCode(),
            'DIAG CODE' => $appointment->getDiagCode(),
            'NOTES' => $appointment->getNotes(),
        );
        return $rs;
    }
}

