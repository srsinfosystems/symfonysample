<?php

require_once('./lib/vendor/phpExcel/Classes/PHPExcel.php');

class hypeDataExporter
{
	private $exportType = null;

	public function __construct($arguments, $options)
	{
		if (array_key_exists('exportType', $options) && $options['exportType']) {
			$this->exportType = DataExportRequestTable::getExportTypeConstantFromName(strtoupper($options['exportType']));
		}
	}
	public function run()
	{
		$dataExporter = $this->getDataExporterRequest();
		$exporter = null;

		if (!$dataExporter instanceof DataExportRequest) {
			$this->log('nothing to export');
			return;
		}
		$this->log(date('Y-m-d H:i:s') . ': hypeDataExporter');
		$this->log('Export type: ' . $dataExporter->getExportTypeString());
		$this->log('Client ID: ' . $dataExporter->getClientId());
		$this->log('');

		if ($dataExporter->getExportType() == DataExportRequestTable::EXPORT_TYPE_PATIENTS) {
			$exporter = new hypePatientExporter($dataExporter);
		}

		else if ($dataExporter->getExportType() == DataExportRequestTable::EXPORT_TYPE_CLAIMS) {
			$exporter = new hypeClaimExporter($dataExporter);
		}
		else if ($dataExporter->getExportType() == DataExportRequestTable::EXPORT_TYPE_APPOINTMENTS) {
			$exporter = new hypeAppointmentExporter($dataExporter);
		}
		else if ($dataExporter->getExportType() == DataExportRequestTable::EXPORT_TYPE_REFERRING_DOCTORS) {
			$exporter = new hypeReferringDoctorExporter($dataExporter);
		}
		else if ($dataExporter->getExportType() == DataExportRequestTable::EXPORT_TYPE_PATIENT_NOTES) {
			$exporter = new hypePatientNoteExporter($dataExporter);
		}

		if ($exporter != null) {
			if ($dataExporter->getStatus() == DataExportRequestTable::FILE_STATUS_CANCELLED) {
				$exporter->deleteFile();

				$dataExporter->setStatus(DataExportRequestTable::FILE_STATUS_CANCELLED_COMPLETED);
				$dataExporter->save();
			}
			else {
				$exporter->run();
			}
		}
	}
	private function getDataExporterRequest()
	{
		$q = Doctrine_Query::create()
			->from('DataExportRequest d')
			->addWhere('d.status != (?) and d.status != (?) and d.status != (?)', array(DataExportRequestTable::FILE_STATUS_COMPLETED, DataExportRequestTable::FILE_STATUS_CANCELLED_COMPLETED, DataExportRequestTable::FILE_STATUS_COMPLETED_DOWNLOADED))
            ->orderBy('d.status desc')
			->limit(1);

		if ($this->exportType) {
			$q->addWhere('d.export_type = (?)', array($this->exportType));
		}

		return $q->fetchOne();
	}
	private function log($str)
	{
		echo $str . "\n";
	}
}
