<?php

class hypeExporterBase
{
    protected $file = null;
    /**
     * @var DataExportRequest
     */
    protected $dataExportRequest = null;
    protected $count = 0;
    protected $recordLimit = 1000;
    protected $fileLimit = 10000;

    public function __construct($dataExporterRequest)
    {
        $this->dataExportRequest = $dataExporterRequest;
    }


    protected function log($str)
    {
        echo $str . "\n";
    }

    protected function writeLine($v)
    {
        if (!is_resource($this->file)) {
            $this->openFile();
        }

        foreach ($v as $i => $val)
        {
            $v[$i] = $this->processString($val . '');
        }
        fputcsv($this->file, $v);
        $this->dataExportRequest->setRecordCount($this->dataExportRequest->getRecordCount() + 1);
    }

    protected function openFile()
    {
        $file = ini_get('upload_tmp_dir') . '/' . $this->dataExportRequest->getFilename();

        $this->file = fopen($file, 'a');
    }

    private function processString($str, $includeNewLines = false)
    {
        if (!$includeNewLines) {
            $str = str_replace(array("\n", "\r", "\r\n"), ' -- ', $str);
        }
        return strtoupper(trim(iconv("UTF-8", "UTF-8//IGNORE", $str)));
    }

    protected function flushFile()
    {
        fflush($this->file);
    }

    protected function splitFile()
    {
        /**
         * @var DataExportRequest
         */
        $clone = $this->dataExportRequest->copy();

        $clone->setPartNumber($this->dataExportRequest->getPartNumber() + 1);
        $clone->setRecordCount(0);
        $clone->setHasHeader(false);
        if (!$clone->getFirstFileId()) {
            $clone->setFirstFileId($this->dataExportRequest->get('id'));
        }
        $clone->save();

        $this->dataExportRequest->setIsLastFile(false);
        $this->setFile();
        $this->dataExportRequest->insertPartNumberToFilename();

//        if ($this->dataExportRequest->getEmailAddress()) {
//            $this->sendNotificationEmail();
//        }
        $this->dataExportRequest->save();
        exit();
    }

    protected function setFile()
    {
        $this->dataExportRequest->setStatus(DataExportRequestTable::FILE_STATUS_COMPLETED);
        if (is_resource($this->file)) {
            $this->closeFile();
        }
        $this->dataExportRequest->setExportFile(file_get_contents($this->getFilePath()));
        $this->dataExportRequest->setCompletionDate(date(hype::DB_ISO_DATE));
        if ($this->dataExportRequest->getIsLastFile() && $this->dataExportRequest->getPartNumber() > 1) {
            $this->dataExportRequest->insertPartNumberToFilename();
        }
        $this->dataExportRequest->save();
        $this->deleteFile();
    }

    protected function closeFile()
    {
        fclose($this->file);
    }

    protected function getFilePath()
    {
        return ini_get('upload_tmp_dir') . '/' . $this->dataExportRequest->getFilename();
    }

    public function deleteFile()
    {
        $file = ini_get('upload_tmp_dir') . '/' . $this->dataExportRequest->getFilename();

        if (file_exists($file)) {
            unlink($file);
        }
    }

    protected function sendNotificationEmail()
    {
        $messageText = 'A data export file that you requested is ready for downloading.' . "\n\n";
        $messageText .= 'Export Type: ' . $this->dataExportRequest->getExportTypeString() . "\n\n";

        $files = $this->dataExportRequest->getAssociatedFiles();

        if (count($files) > 1) {
            $messageText .= 'Because of the size of the data request, your report has been split into ' . count($files) . ' files:' . "\n";
            foreach ($files as $file) {
                $messageText .= '   ' . $file . "\n";
            }
            $messageText .= "\n";
        }
        else {
            $messageText .= 'File Name: ' . $this->dataExportRequest->getFileName() . "\n\n";
        }

        $messageText .= 'You can download the file' . (count($files) > 1 ? 's' : '') . ' by logging into Hype Medical and going to the SysAdmin->Exports screen.' . "\n\n";


        if (!class_exists('Swift_SmtpTransport')) {
            require_once (dirname(__FILE__) . '../../../vendor/Swift-5.0.3/lib/swift_required.php');
        }

        $username = 'support@hypesystems.com';
        $password = '0w3n4l0n';
        $smtp = 'mail.hypesystems.com';
        $port = 25;
        $from = array('support@hypesystems.com' => 'Hype Support');
        $to = $this->dataExportRequest->getEmailAddress();
        $subject = 'Hype Medical - Completed Export Request';

        $transport = Swift_SmtpTransport::newInstance($smtp, $port)
            ->setUsername($username)
            ->setPassword($password);
        $mailer = Swift_Mailer::newInstance($transport);

        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($from)
            ->setTo($to)
            ->setBody($messageText);

        $mailer->send($message);
    }
}