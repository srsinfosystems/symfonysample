<?php

class hypeReferringDoctorExporter extends hypeExporterBase
{
    public function run()
    {
        $this->openFile();

        if ($this->dataExportRequest->getStatus() == DataExportRequestTable::FILE_STATUS_UPLOADED) {
            $this->dataExportRequest->setStatus(DataExportRequestTable::FILE_STATUS_PROCESSING);
            $this->dataExportRequest->save();
        }

        do {
            if (!$this->dataExportRequest->getHasHeader()) {
                $this->writeLine($this->generateHeader());
                $this->flushFile();

                $this->dataExportRequest->setHasHeader(true);
                $this->dataExportRequest->save();
            }

            $refDocs = $this->getNextRecord();

            foreach ($refDocs as $refDoc)
            {
                if ($refDoc instanceof ReferringDoctor) {
                    $this->writeLine($this->generateLine($refDoc));

                    $this->flushFile();
                    $this->dataExportRequest->setRecordId($refDoc->get('id'));
                    $this->dataExportRequest->save();

                    $this->count++;
                }
            }
        } while ($refDocs->count() && $this->count <= $this->recordLimit);

        $this->closeFile();
        $this->dataExportRequest->save();

        if (!count($refDocs)) {
            $this->setFile();
            if ($this->dataExportRequest->getEmailAddress()) {
                $this->sendNotificationEmail();
            }
        }
    }

    private function generateHeader()
    {
        return array(
            'REF DOC ID',
            'LNAME',
            'FNAME',
            'PROVIDER NUM',
            'SPEC CODE',
            'ADDRESS 1',
            'ADDRESS 2',
            'CITY',
            'PROVINCE',
            'POSTAL CODE',
            'PHONE',
            'FAX',
            'EMAIL',
        );
    }

    private function getNextRecord()
    {
        return Doctrine_Query::create()
            ->from('ReferringDoctor rd')
            ->addWhere('rd.client_id = (?)', array($this->dataExportRequest->getClientId()))
            ->addWhere('rd.id > (?)', array($this->dataExportRequest->getRecordId()))
            ->addWhere('rd.active = (?)', true)
            ->limit(50)
            ->execute();
    }

    private function generateLine(ReferringDoctor $refDoc)
    {
        $rs = array(
            'REF DOC ID' => $refDoc->get('id'),
            'LNAME' => $refDoc->getLname(),
            'FNAME' => $refDoc->getFname(),
            'PROVIDER NUM' => $refDoc->getProviderNum(),
            'SPEC CODE' => $refDoc->get('spec_code'),
            'ADDRESS 1' => $refDoc->getAddress(),
            'ADDRESS 2' => $refDoc->getAddress2(),
            'CITY' => $refDoc->getCity(),
            'PROVINCE' => $refDoc->getProvince(),
            'POSTAL CODE' => hype::displayPostalCode($refDoc->getPostalCode()),
            'PHONE' => hype::displayPhone($refDoc->getPhone()),
            'FAX' => hype::displayPhone($refDoc->getFax()),
            'EMAIL' => $refDoc->getEmail(),
        );

        return $rs;
    }
}

