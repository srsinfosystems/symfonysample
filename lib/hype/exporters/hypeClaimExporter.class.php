<?php

class hypeClaimExporter extends hypeExporterBase
{
    public function run()
    {
        $this->openFile();

        if ($this->dataExportRequest->getStatus() == DataExportRequestTable::FILE_STATUS_UPLOADED) {
            $this->dataExportRequest->setStatus(DataExportRequestTable::FILE_STATUS_PROCESSING);
            $this->dataExportRequest->save();
        }

        do {
            if (!$this->dataExportRequest->getHasHeader()) {
                $this->writeLine($this->generateHeader());
                $this->flushFile();

                $this->dataExportRequest->setHasHeader(true);
                $this->dataExportRequest->save();
            }

            $claimItems = $this->getNextRecord();
            foreach ($claimItems as $claimItem)
            {
                if ($claimItem instanceof ClaimItem) {
                    $this->writeLine($this->generateLine($claimItem));
                    $this->flushFile();

                    $this->dataExportRequest->setRecordId($claimItem->get('id'));
                    $this->dataExportRequest->save();

                    $this->count++;
                }

                if ($this->dataExportRequest->getRecordCount() >= $this->fileLimit) {
                    $this->splitFile();
                }
            }
        } while ($claimItems->count() && $this->count <= $this->recordLimit);

        $this->closeFile();
        $this->dataExportRequest->save();

        if (!$claimItems->count()) {
            $this->setFile();
            if ($this->dataExportRequest->getEmailAddress()) {
                $this->sendNotificationEmail();
            }
        }
    }

    private function generateHeader()
    {
        return array(
            'CLAIM ITEM ID',
            'CLAIM ID',
            'ACCT NUM',

            'BATCH NUMBER',
            'BATCH FILE NAME',

            'DOCTOR GROUP',
            'DOCTOR PROVIDER NUM',
            'DOCTOR SPEC CODE',
            'DOCTOR LNAME',
            'DOCTOR FNAME',

            'CLINIC',

            'PATIENT ID',
            'PATIENT CHART NUM',
            'PATIENT HCN',
            'PATIENT VC',
            'PATIENT LNAME',
            'PATIENT FNAME',
            'PATIENT DOB',
            'PATIENT SEX',

            'PROVINCE',

            'CLAIM TYPE',
            'REF DOC NUM',
            'FACILITY NUM',
            'ADMIT DATE',
            'MANUAL REVIEW',
            'SLI',

            'ITEM NUM',
            'SERVICE DATE',
            'SERVICE CODE',
            'DIAG CODE',
            'NUM SERVICES',
            'FEE SUBMITTED',
            'FEE PAID',

            'MOH CLAIM',
        );
    }

    private function getNextRecord()
    {
        return Doctrine_Query::create()
            ->from('ClaimItem ci')
            ->leftJoin('ci.Claim c')
            ->leftJoin('c.Batch b')
            ->leftJoin('c.Doctor d')
            ->leftJoin('c.Location l')
            ->addWhere('ci.client_id = (?)', array($this->dataExportRequest->getClientId()))
            ->addWhere('c.deleted = (?)', array(false))
            ->addWhere('ci.status != (?)', array(ClaimItemTable::STATUS_DELETED))
            ->addWhere('ci.id > (?)', array($this->dataExportRequest->getRecordId()))
            ->orderBy('ci.id asc')
            ->limit(100)
            ->execute();
    }

    private function generateLine(ClaimItem $claimItem)
    {
        $rs = array(
            'CLAIM ITEM ID' => $claimItem->get('id'),
            'CLAIM ID' => $claimItem->Claim->get('id'),
            'ACCT NUM' => $claimItem->Claim->getAcctNum(),

            'BATCH NUMBER' => $claimItem->Claim->getBatchId() ? $claimItem->Claim->Batch->getBatchNumber() : null,
            'BATCH FILE NAME' => $claimItem->Claim->getBatchId() ? $claimItem->Claim->Batch->getFileName() : null,

            'DOCTOR GROUP' => $claimItem->Claim->getDoctorId() ? $claimItem->Claim->Doctor->getGroupNum() : null,
            'DOCTOR PROVIDER NUM' => $claimItem->Claim->getDoctorId() ? $claimItem->Claim->Doctor->getProviderNum() : null,
            'DOCTOR SPEC CODE' => $claimItem->Claim->getDoctorId() ? $claimItem->Claim->Doctor->getSpecCode()->getMohSpecCode() : null,
            'DOCTOR LNAME' => $claimItem->Claim->getDoctorId() ? $claimItem->Claim->Doctor->getLname() : null,
            'DOCTOR FNAME' => $claimItem->Claim->getDoctorId() ? $claimItem->Claim->Doctor->getFname() : null,

            'CLINIC' => $claimItem->Claim->getLocationId() ? $claimItem->Claim->Location->getName() : null,

            'PATIENT ID' => $claimItem->Claim->getPatientId(),
            'PATIENT CHART NUM' => $claimItem->Claim->getPatientNumber(),
            'PATIENT HCN' => hype::decryptHCN($claimItem->Claim->getHealthNum()),
            'PATIENT VC' => $claimItem->Claim->getVersionCode(),
            'PATIENT LNAME' => $claimItem->Claim->getLname(),
            'PATIENT FNAME' => $claimItem->Claim->getFname(),
            'PATIENT DOB' => $claimItem->Claim->getDob() ? hype::getFormattedDate(hype::DB_DATE_FORMAT, $claimItem->Claim->getDob(), hype::DB_ISO_DATE) : null,
            'PATIENT SEX' => $claimItem->Claim->getSex(),

            'PROVINCE' => $claimItem->Claim->getProvince(),

            'CLAIM TYPE' => $claimItem->Claim->getPayProg(),
            'REF DOC NUM' => $claimItem->Claim->getRefDoc(),
            'FACILITY NUM' => $claimItem->Claim->getFacilityNum(),
            'ADMIT DATE' => $claimItem->Claim->getAdmitDate() ? hype::getFormattedDate(hype::DB_DATE_FORMAT, $claimItem->Claim->getAdmitDate(), hype::DB_ISO_DATE) : null,
            'MANUAL REVIEW' => $claimItem->Claim->getManualReview() ? '1' : '0',
            'SLI' => $claimItem->Claim->getSli(),

            'ITEM NUM' => $claimItem->getItemId(),
            'SERVICE DATE' => $claimItem->getServiceDate() ? hype::getFormattedDate(hype::DB_DATE_FORMAT, $claimItem->getServiceDate(), hype::DB_ISO_DATE) : null,
            'SERVICE CODE' => $claimItem->getServiceCode(),
            'DIAG CODE' => $claimItem->getDiagCode(),
            'NUM SERVICES' => $claimItem->getNumServ(),
            'FEE SUBMITTED' => $claimItem->getFeeSubm(),
            'FEE PAID' => $claimItem->getFeePaid(),

            'MOH CLAIM' => $claimItem->getMohClaim(),
        );

        return $rs;
    }
}

