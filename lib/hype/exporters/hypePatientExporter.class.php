<?php

class hypePatientExporter extends hypeExporterBase
{
    public function run()
    {
        $this->openFile();

        if ($this->dataExportRequest->getStatus() == DataExportRequestTable::FILE_STATUS_UPLOADED) {
            $this->dataExportRequest->setStatus(DataExportRequestTable::FILE_STATUS_PROCESSING);
            $this->dataExportRequest->save();
        }

        do {
            if (!$this->dataExportRequest->getHasHeader()) {
                $this->writeLine($this->generateHeader());
                $this->flushFile();

                $this->dataExportRequest->setHasHeader(true);
                $this->dataExportRequest->save();
            }

            $patients = $this->getNextRecord();
            foreach ($patients as $patient)
            {
                if ($patient instanceof Patient) {
                    $this->writeLine($this->generateLine($patient));

                    $this->flushFile();
                    $this->dataExportRequest->setRecordId($patient->get('id'));
                    $this->dataExportRequest->save();

                    $this->count++;
                }

                if ($this->dataExportRequest->getRecordCount() >= $this->fileLimit) {
                    $this->splitFile();
                }
            }

        } while ($patients->count() && $this->count <= $this->recordLimit);

        $this->closeFile();
        $this->dataExportRequest->save();

        if (!count($patients)) {
            $this->setFile();
            if ($this->dataExportRequest->getEmailAddress()) {
                $this->sendNotificationEmail();
            }
        }
    }

    private function generateHeader()
    {
        return array(
            'PATIENT ID',
            'CHART NUMBER',
            'FIRST',
            'LAST',
            'CLINIC',
            'HC NUM',
            'VERSION CODE',
            'EXPIRY DATE',
            'HC PROV',
            'DOB',
            'SEX',
            'ADDRESS',
            'CITY',
            'PROVINCE',
            'POSTAL CODE',
            'EMAIL',
            'HOME PH',
            'CELL PH',
            'WORK PH',
            'REF DOC NUM',
            'FAM DOC NUM',
            'PATIENT STATUS',
            'PATIENT TYPE'
        );
    }

    private function getNextRecord()
    {
        return Doctrine_Query::create()
            ->from('Patient p')
            ->leftJoin('p.Location l')
            ->leftJoin('p.PatientStatus')
            ->leftJoin('p.PatientType')
            ->addWhere('p.client_id = (?)', array($this->dataExportRequest->getClientId()))
            ->addWhere('p.id > (?)', array($this->dataExportRequest->getRecordId()))
            ->addWhere('p.deleted = (?)', array(false))
            ->orderBy('p.id asc')
            ->limit(50)
            ->execute();
    }

    private function generateLine(Patient $patient)
    {
        $rs = array(
            'PATIENT ID' => $patient->get('id'),
            'CHART NUMBER' => $patient->getPatientNumber(),
            'FIRST' => $patient->getFname(),
            'LAST' => $patient->getLname(),
            'CLINIC' => $patient->getLocationId() ? $patient->Location->getName() : null,
            'HC NUM' => hype::decryptHCN($patient->getHcnNum()),
            'VERSION CODE' => $patient->getHcnVersionCode(),
            'EXPIRY DATE' => $patient->getHcnExpYear() ? hype::getFormattedDate(hype::DB_DATE_FORMAT, $patient->getHcnExpYear(), hype::DB_ISO_DATE) : null,
            'HC PROV' => $patient->getMohProvince() ? $patient->getMohProvince() : $patient->getProvince(),
            'DOB' => $patient->getDob() ? hype::getFormattedDate(hype::DB_DATE_FORMAT, $patient->getDob(), hype::DB_ISO_DATE) : null,
            'SEX' => $patient->getSex(),
            'ADDRESS' => $patient->getAddress(),
            'CITY' => $patient->getCity(),
            'PROVINCE' => $patient->getProvince(),
            'POSTAL CODE' => $patient->getDisplayPostalCode(),
            'EMAIL' => $patient->getEmail(),
            'HOME PH' => $patient->getDisplayHphone(),
            'CELL PH' => $patient->getDisplayCellphone(),
            'WORK PH' => $patient->getDisplayWphone(),
            'REF DOC NUM' => $patient->getRefDocNum(),
            'FAM DOC NUM' => $patient->getFamDocNum(),
            'PATIENT STATUS' => $patient->getPatientStatusId() ? $patient->PatientStatus->getName() : null,
            'PATIENT TYPE' => $patient->getPatientTypeId() ? $patient->PatientType->getName() : null
        );
        return $rs;
    }
}

