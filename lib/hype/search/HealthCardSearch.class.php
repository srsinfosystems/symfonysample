<?php

class HealthCardSearch {

    protected $_debug = false;
    protected $_patients = null;
    protected $_query_num = 1;
    protected $date_format = null;
    protected $client_id = null;
    protected $dob = null;
    protected $expiry_date = null;
    protected $fname = null;
    protected $health_num = null;
    protected $lname = null;
    protected $sex = null;
    protected $version_code = null;

    protected function _debug_echo($v) {
        if ($this->_debug) {
            echo $v;
        }
    }

    protected function _searchHcnExactMatch() {
        $this->_debug_echo($this->_query_num . ': Health card number & client id       - ');

        $this->_patients = Doctrine_Query::create()
                ->from('Patient p')
                ->addWhere('p.client_id = ?', $this->client_id)
                ->addWhere('p.hcn_num = ?', $this->health_num)
                ->addWhere('p.deleted = (?)', false)
                ->execute();

        $this->_debug_echo(count($this->_patients) . ' patient(s) found' . "\n");
    }

    protected function _searchLastFirst() {
        $this->_debug_echo($this->_query_num . ': First, last, client id               - ');

        $this->_patients = Doctrine_Query::create()
                ->from('Patient p')
                ->addWhere('p.client_id = ?', $this->client_id)
                ->addWhere('p.fname = ?', $this->fname)
                ->addWhere('p.lname = ?', $this->lname)
                ->addWhere('p.deleted = (?)', false)
                ->execute();

        $this->_debug_echo(count($this->_patients) . ' patient(s) found' . "\n");
    }

    protected function _searchLastFirstDob() {
        $this->_debug_echo($this->_query_num . ': Last, first, dob, client id          - ');

        $this->_patients = Doctrine_Query::create()
                ->from('Patient p')
                ->addWhere('p.client_id = ?', $this->client_id)
                ->addWhere('p.fname = ?', $this->fname)
                ->addWhere('p.lname = ?', $this->lname)
                ->addWhere('p.dob = ?', $this->dob)
                ->addWhere('p.deleted = (?)', false)
                ->execute();

        $this->_debug_echo(count($this->_patients) . ' patient(s) found' . "\n");
    }

    protected function _searchLastOnly() {
        $this->_debug_echo($this->_query_num . ': Last, client id                      - ');

        $this->_patients = Doctrine_Query::create()
                ->from('Patient p')
                ->addWhere('p.client_id = ?', $this->client_id)
                ->addWhere('p.lname = ?', $this->lname)
                ->addWhere('p.deleted = (?)', false)
                ->execute();

        $this->_debug_echo(count($this->_patients) . ' patient(s) found' . "\n");
    }

    protected function _searchSwapLastFirst() {
        $this->_debug_echo($this->_query_num . ': Last & first swapped, dob, client id - ');

        $this->_patients = Doctrine_Query::create()
                ->from('Patient p')
                ->addWhere('p.client_id = ?', $this->client_id)
                ->addWhere('p.fname = ?', $this->lname)
                ->addWhere('p.lname = ?', $this->fname)
                ->addWhere('p.dob = ?', $this->dob)
                ->addWhere('p.deleted = (?)', false)
                ->execute();

        $this->_debug_echo(count($this->_patients) . ' patient(s) found' . "\n");
    }

    public function debug($v) {
        $this->_debug = $v;
        return $this;
    }

    public function getPatients() {
        $functions = array(
            '_searchHcnExactMatch',
            '_searchLastFirstDob',
  			'_searchLastFirst',
            '_searchSwapLastFirst',
  			'_searchLastOnly',
        );

        foreach ($functions as $fn) {
            call_user_func(array($this, $fn));
            if (count($this->_patients)) {
                return $this->_patients;
            }
            $this->_query_num++;
        }
    }

    public function getTerms($mode = 'patient') {
        if ($mode == 'patient') {
            return array(
                'hcn_num' => hype::decryptHCN($this->health_num),
                'fname' => $this->fname,
                'lname' => $this->lname,
                'dob' => date($this->date_format, hype::parseDateToInt($this->dob, hype::DB_DATE_FORMAT)),
                'sex' => $this->sex,
                'hcn_version_code' => $this->version_code,
                'hcn_exp_year' => $this->expiry_date ? date($this->date_format, hype::parseDateToInt($this->expiry_date, hype::DB_DATE_FORMAT)) : '',
            );
        }

        return array(
            'health_num' => hype::decryptHCN($this->health_num),
            'fname' => $this->fname,
            'lname' => $this->lname,
            'dob' => date($this->date_format, hype::parseDateToInt($this->dob, hype::DB_DATE_FORMAT)),
            'sex' => $this->sex,
            'version_code' => $this->version_code,
            'expiry_date' => $this->expiry_date ? date($this->date_format, hype::parseDateToInt($this->expiry_date, hype::DB_DATE_FORMAT)) : '',
        );
    }

    public function setClientId($v) {
        $this->client_id = $v;
        return $this;
    }

    public function setDateFormat($f) {
        $this->date_format = $f;
        return $this;
    }

    public function setDob($v) {
        $this->dob = $v;
        return $this;
    }

    public function setExpiryDate($v) {
        $this->expiry_date = $v;
        return $this;
    }

    public function setFname($v) {
        $this->fname = $v;
        return $this;
    }

    public function setHealthCardNumber($v) {
        $this->health_num = hype::encryptHCN($v);
        return $this;
    }

    public function setLname($v) {
        $this->lname = $v;
        return $this;
    }

    public function setSex($v) {
        $this->sex = $v;
        return $this;
    }

    public function setStatus($v) {
        $this->status = $v;
        return $this;
    }

    public function setVersionCode($v) {
        $this->version_code = $v;
        return $this;
    }

    public static function create() {
        return new HealthCardSearch();
    }

    public static function formatExpiryDate($v, $format) {
        if (intval($v)) {
            $year = '20' . substr($v, 0, 2);
            $month = substr($v, 2, 2);
            $day = '01';
            return $year . '-' . $month . '-' . $day;
        }
        return null;
    }

    public static function formatDob($v, $format) {
        if ($v) {
            $year = substr($v, 0, 4);
            $month = substr($v, 4, 2);
            $day = substr($v, 6, 2);

            return $year . '-' . $month . '-' . $day;
        }
        return null;
    }

}

?>