<?php

class PatientSearch
{
 	protected $_terms = array();
 	protected $_q = null;

 	public function setTerms($arr = array()) {
 		$this->_terms = $arr;
 		unset($this->_terms['_csrf_token']);
 		unset($this->_terms['pay_prog']);
 		unset($this->_terms['claim_items']);
 		unset($this->_terms['doctor_code']);
 		unset($this->_terms['doctor_id']);
 		unset($this->_terms['ref_lab']);
 		unset($this->_terms['manual_review']);
 		unset($this->_terms['facility_num']);
 		unset($this->_terms['location_id']);
 		unset($this->_terms['ref_doc_description']);
 		unset($this->_terms['fam_doc_description']);
 		unset($this->_terms['fam_doc_id']);
 		unset($this->_terms['ref_doc_id']);
 		unset($this->_terms['third_party_id']);
 		unset($this->_terms['policy_number']);

 		unset($this->_terms['payment_method']);
 		unset($this->_terms['cheque_number']);
 		unset($this->_terms['card_type']);
 		unset($this->_terms['other_card']);
 		unset($this->_terms['transaction_number']);
 		unset($this->_terms['total_billed']);
 		unset($this->_terms['total_paid']);

 		if (array_key_exists('patient_id', $this->_terms)) {
 			$this->_terms['id'] = $this->_terms['patient_id'];
 			unset($this->_terms['patient_id']);
 		}
 		if (array_key_exists('version_code', $this->_terms)) {
 			$this->_terms['hcn_version_code'] = $this->_terms['version_code'];
 			unset($this->_terms['version_code']);
 		}
 		if (array_key_exists('ref_doc', $this->_terms)) {
 			$this->_terms['ref_doc_num'] = $this->_terms['ref_doc'];
 			unset($this->_terms['ref_doc']);
 		}
 		if (array_key_exists('health_num', $this->_terms)) {
 			$this->_terms['hcn_num'] = $this->_terms['health_num'];
 			unset($this->_terms['health_num']);
 		}

 		if (array_key_exists('province', $this->_terms) && $this->_terms['province'] == '0') {
 			unset($this->_terms['province']);
 		}
 		if (array_key_exists('ref_doc_num', $this->_terms) && $this->_terms['ref_doc_num'] == '0') {
 			unset($this->_terms['ref_doc_num']);
 		}
 		if (array_key_exists('location', $this->_terms)) {
 			$this->_terms['location_id'] = $this->_terms['location'];
 			unset($this->_terms['location']);
 		}
 		return $this;
 	}
 	public function setClientId($v)
 	{
 		$this->_terms['client_id'] = $v;

 		return $this;
 	}
 	public function search() {
 		
 		$this->_q = Doctrine_Query::create()
 			->from('Patient p')
 			->leftJoin('p.PatientStatus ps')
 			->addWhere('p.active = ?', true)
 			->addWhere('p.deleted = (?)', false);

 		foreach ($this->_terms as $term => $value)
 		{
 			$fn = '_addTerm' . sfInflector::camelize($term);
 			if ($value) {
 				if (method_exists($this, $fn)) {
 					call_user_func(array($this, $fn), $value);
 				}
 				else {
 					$this->_q->addWhere('p.' . $term . ' = ?', $value);
 				}
 			}
 		}

 		$this->_q->limit(500);

 		return $this->_q->execute();
 	}
 	protected function _addTermPatientNumber($v)
 	{
 		if ($v) {
 			$this->_q->addWhere('(p.id = ? OR p.patient_number = ?)', array(intval($v), $v . ''));
 		}
 	}
 	protected function _addTermPhone($v)
 	{
 		$raw_phone = preg_replace("/[^0-9]/", "", $v);
 		$phone = '%' . $raw_phone . '%';
 		if (strlen($raw_phone) >= 10) {
 			$a = substr($raw_phone, 0, 3);
 			$b = substr($raw_phone, 3, 3);
 			$c = substr($raw_phone, 6, 4);
 			$d = substr($raw_phone, 10);

 			$phone = '%' . $a . '%' . $b . '%' . $c . '%' . $d . '%';
 		}

 		$this->_q->addWhere('(p.hphone LIKE (?) OR p.wphone LIKE (?) OR p.cellphone LIKE (?) OR p.other_phone LIKE (?))', array($phone, $phone, $phone, $phone));
 	}
 	protected function _addTermLname($v) {
 		$this->_q->addWhere('p.lname LIKE (?)',  $v . '%');
 	}
 	protected function _addTermLocationid($v) {
 		$this->_q->addWhere('p.location_id = ?',  $v );
 	}
 	protected function _addTermFname($v) {
 		$this->_q->addWhere('p.fname LIKE (?)', $v . '%');
 	}
 	protected function _addTermHcnNum($v) {
 		$this->_q->addWhere('p.last_4_hcn = ?', $v);
 	}
 	protected function _addTermDob($v) {
 		$d = substr($v, 0, 2);
 		$m = substr($v, 3, 2);
 		$y = substr($v, 6, 4);

 		$this->_q->addWhere('p.dob = ?', "$y-$m-$d");
 	}
 	protected function _addTermAdmitDate($v) {
 		$d = substr($v, 0, 2);
 		$m = substr($v, 3, 2);
 		$y = substr($v, 6, 4);

 		$this->_q->addWhere('p.admit_date = ?', "$y-$m-$d");
 	}
 	protected function _addTermHcnExpYear($v) {
 		$d = substr($v, 0, 2);
 		$m = substr($v, 3, 2);
 		$y = substr($v, 6, 4);

 		$this->_q->addWhere('p.hcn_exp_year = ?', "$y-$m-$d");
 	}
 	protected function _addTermSameGender($v) {
 		$this->_q->addWhere('p.same_gender = ?', ($v == 'on'));
 	}
 	protected function _addTermPostalCode($v) {
 		$this->_q->addWhere('p.postal_code LIKE (?)', '%' . $v . '%');
 	}
 	protected function _addTermCity($v) {
 		$this->_q->addWhere('p.city LIKE (?)', '%' . $v . '%');
 	}
 	protected function _addTermAddress($v) {
 		$this->_q->addWhere('p.address LIKE (?)', '%' . $v . '%');
 	}
 	protected function _addTermEmail($v) {
 		$this->_q->addWhere('p.email LIKE (?)', '%' . $v . '%');
 	}
 	public static function create() {
 		return new PatientSearch();
 	}
}
?>