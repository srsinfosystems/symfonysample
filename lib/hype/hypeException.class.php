<?php
/**
 * CHANGELOG:
 * 2014-08-01
 * + If Computer Name is 'SHANNON-PC' don't send the error report
 *
 * 2014-07-22
 * + Ticket 1339: added function getComputerName to handle differences in $_SERVER variable between windows & linux
 *
 */ 

class hypeException extends sfException {
	protected $emailAddress = array('scott@hypesystems.com' => 'Scott Gilmour');
	protected $context;
	protected $event;

	public function __construct(sfEvent $event)
	{
		$this->event = $event;
	}
	public function setContext($context)
	{
		$this->context = $context;
	}
	public function sendEmail()
	{
		$computerName = $this->getComputerName();

		$messageText = '';
		$exception = $this->event->getSubject();

		$request = print_r ($this->context->getRequest()->getParameterHolder()->getAll(), true);
		$user = print_r (sfDebug::userAsArray($this->context->getUser()), true);
 		$errorMsg = $exception->getMessage();
		$trace = print_r (self::getTraces($exception, 'txt'), true);

		$messageText .= 'An uncaught Hype Medical exception has been encountered on ' . $computerName . "\n\n";
		$messageText .= '============================================================================================' . "\n\n";
		$messageText .= $errorMsg . "\n\n";
		$messageText .= '============================================================================================' . "\n\n";
		$messageText .= $trace . "\n\n";
		$messageText .= '============================================================================================' . "\n\n";
		$messageText .= $request . "\n\n";
		$messageText .= '============================================================================================' . "\n\n";
		$messageText .= $user . "\n\n";
		$messageText .= '============================================================================================' . "\n\n";
		$messageText .= date(hype::DB_ISO_DATE) . ' ' . $computerName . "\n\n\n";

 		if (!class_exists('Swift_SmtpTransport')) {
			require_once (dirname(__FILE__) . '../../../vendor/Swift-5.0.3/lib/swift_required.php');
		}

		$username = 'support@hypesystems.com';
		$password = '0w3n4l0n';
		$smtp = 'mail.hypesystems.com';
		$port = 25;
		$from = array('support@hypesystems.com' => 'Hype Support');
		$to = $this->emailAddress;
		$subject = 'Hype Medical Exception Report';

		$transport = Swift_SmtpTransport::newInstance($smtp, $port)
			->setUsername($username)
			->setPassword($password);
		$mailer = Swift_Mailer::newInstance($transport);

		$message = Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom($from)
			->setTo($to)
			->setBody($messageText);

		$result = $mailer->send($message);
	}

	private function getComputerName()
	{
		if (array_key_exists('COMPUTERNAME', $_SERVER)) {
			return $_SERVER['COMPUTERNAME'];
		}
		if (array_key_exists('SERVER_NAME', $_SERVER)) {
			return $_SERVER['SERVER_NAME'];
		}
		return php_uname();
	}
}