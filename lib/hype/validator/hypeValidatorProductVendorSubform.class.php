<?php

class hypeValidatorProductVendorSubform extends sfValidatorSchema
{
 	public function __construct($leftField, $rightField, $otherField, $options = array(), $messages = array())
 	{
 		$this->addOption('left_field', $leftField);
 		$this->addOption('right_field', $rightField);
 		$this->addOption('other_field', $otherField);

 		parent::__construct(null, $options, $messages);
 	}
 	protected function doClean($values)
 	{
 		if (null === $values)
 		{
 			$values = array();
 		}

 		if (!is_array($values))
 		{
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$leftValue  = array_key_exists($this->getOption('left_field'), $values) ? $values[$this->getOption('left_field')] : null;
 		$rightValue = array_key_exists($this->getOption('right_field'), $values) ? $values[$this->getOption('right_field')] : null;
 		$otherValue = array_key_exists($this->getOption('other_field'), $values) ? $values[$this->getOption('other_field')] : null;

 		if (!($leftValue XOR $rightValue XOR $otherValue)) {
 			$error = new sfValidatorError($this, 'invalid');
 			throw new sfValidatorErrorSchema($this, array($this->getOption('left_field') => $error));
 		}

 		return $values;
 	}
}
