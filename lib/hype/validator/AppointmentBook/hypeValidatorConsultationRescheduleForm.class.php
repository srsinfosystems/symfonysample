<?php

class hypeValidatorConsultationRescheduleForm extends sfValidatorSchema
{
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$errors = new sfValidatorErrorSchema($this);

 		$new_appointment = Doctrine_Query::create()
 			->from('Appointment a')
 			->addWhere('a.client_id = ?', $values['client_id'])
 			->addWhere('a.id = ?', $values['new_appointment_id'])
 			->fetchOne();

 		if (!$new_appointment instanceof Appointment) {
 			$e = new sfValidatorError($this, 'Invalid New Appointment ID entered.');
 			$errors->addError($e);
 		}
 		else {
 			$booked_appointments = Doctrine_Query::create()
 				->from('AppointmentAttendee att')
 				->addWhere('att.appointment_id = ?', $new_appointment->id)
 				->addWhere('att.status = ?', AppointmentAttendeeTable::STATUS_BOOKED)
 				->count();

 			if ($new_appointment->max_attendees <= $booked_appointments) {
 				$e = new sfValidatorError($this, 'There are already ' . $new_appointment->max_attendees . ' patients in this appointment.');
 				$errors->addError($e);
 			}
 		}

 		if (count($errors)) {
 			throw $errors;
 		}

 		return $values;
 	}
}
