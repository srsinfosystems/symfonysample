<?php

class hypeValidatorDoctorAppointmentRescheduleForm extends sfValidatorSchema
{
 	protected $doctor_ids = array();
 	protected $appointment_title_format = null;
 	protected $date_format;

 	public function __construct($appointment_title_format, $skip_overlap, $doctor_ids, $date_format, $options = array(), $messages = array())
 	{
 		$this->appointment_title_format = $appointment_title_format;
 		$this->skip_overlap = $skip_overlap;
 		$this->doctor_ids = $doctor_ids;
 		$this->date_format = $date_format;

 		parent::__construct(array(), $options, $messages);
 	}
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$errors = new sfValidatorErrorSchema($this);

 		if ($values['start_date'] && $values['start_time']) {
 			$values['start_date'] = hype::parseDateToInt(date(hype::DB_DATE_FORMAT, $values['start_date']) . ' ' . $values['start_time'], hype::DB_ISO_DATE);
 			$values['end_date'] = strtotime(date(hype::DB_ISO_DATE, $values['start_date']) . ' + ' . $values['length'] . ' minutes');
 		}

 		$old_appointment = Doctrine_Query::create()
 			->from('Appointment a')
 			->addWhere('a.client_id = ?', $values['client_id'])
 			->addWhere('a.id = ?', $values['original_appointment_id'])
 			->fetchOne();

 		if (!$old_appointment instanceof Appointment) {
 			$e = new sfValidatorError($this, 'Invalid Original Appointment');
 			$errors->addError($e);
 		}
 		else {
 			$values['OldAppointment'] = $old_appointment;
 		}

 		if ($values['doctor_id'] && !in_array($values['doctor_id'], $this->doctor_ids)) {
 				$e = new sfValidatorError($this, 'You cannot book an appointment for this doctor.');
 				$errors->addError('doctor_id');
 		}

 		if ($old_appointment->doctor_id) {
 			if (!$values['doctor_id']) {
 				$e = new sfValidatorError($this, 'The Original Appointment has a doctor but the new one does not.');
 				$errors->addError($e);
 			}
 			else {

 				$dsb = Doctrine_Query::create()
 					->from('DoctorScheduleBlock dsb')
 					->leftJoin('dsb.Doctor d')
 					->addWhere('dsb.location_id = ?', $values['location_id'])
 					->addWhere('dsb.doctor_id = ?', $values['doctor_id'])
 					->addWhere('dsb.client_id = ?', $values['client_id'])
 					->addWhere('(?) between dsb.start_date and dsb.end_date', date('Y-m-d H:i', $values['start_date']))
 					->addWhere('(?) between dsb.start_date and dsb.end_date', date('Y-m-d H:i', $values['end_date']))
 					->fetchOne();

 				if (!$dsb instanceof DoctorScheduleBlock) {
 					$e = new sfValidatorError($this, 'This doctor is not scheduled from ' . date('g:i a', $values['start_date']) . ' until ' . date('g:i a', $values['end_date']));
 					$errors->addError($e);
 				}
 				else {
 					$values['doctor_code'] = $dsb->Doctor->qc_doctor_code;
 				}
 			}
 		}

 		if ($values['length'] != $old_appointment['length']) {
 			$e = new sfValidatorError($this, 'Cannot change an appointment length while rescheduling.');
 			$errors->addError($e);
 		}

 		if (!count($errors) && !$values['override_reschedule'] && !$this->skip_overlap) {
 			$appt_query = Doctrine_Query::create()
 				->from('Appointment a')
 				->leftJoin('a.AppointmentAttendee att')
 				->addWhere('a.client_id = ?', $values['client_id'])
                ->addWhere('att.client_id = (?) or att.client_id is null', array($values['client_id']))
 				->addWhere('a.location_id = ?', $values['location_id'])
 				->addWhere('a.doctor_id = ?', $values['doctor_id'])

 				->addWhere('(a.start_date > (?) and a.start_date < (?)) OR (a.end_date > (?) and a.end_date < (?))', array(
 						date(hype::DB_ISO_DATE, $values['start_date']),
 						date(hype::DB_ISO_DATE, $values['end_date']),
 						date(hype::DB_ISO_DATE, $values['start_date']),
 						date(hype::DB_ISO_DATE, $values['end_date'])
 				))
 				->addWhere('a.status = ?', AppointmentTable::STATUS_POSTED);

 			if ($values['original_appointment_id']) {
 				$appt_query->addWhere('a.id != (?)', $values['original_appointment_id']);
 			}
 			$appointments = $appt_query->execute();

 			if (count($appointments)) {
 				$e = new sfValidatorError($this, 'This appointment overlaps with ' . count($appointments) . ' other appointment(s).');
 				$errors->addError($e);

 				foreach ($appointments as $appt) {
 					$e = new sfValidatorError($this, 'Overlapping Appointment '
 						. ' [' . date('g:i a', hype::parseDateToInt($appt->start_date, hype::DB_ISO_DATE)) . '-' . date('g:i a', hype::parseDateToInt($appt->end_date, hype::DB_ISO_DATE)) . ']: '
 						. $appt->processAppointmentTitle($this->appointment_title_format, array('date_format' => $this->date_format))
 						);
 					$errors->addError($e);
 				}
 			}
 		}

 		if (count($errors)) {
 			throw $errors;
 		}

 		return $values;
 	}
}
