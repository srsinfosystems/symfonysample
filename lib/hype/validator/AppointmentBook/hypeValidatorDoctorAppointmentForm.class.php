<?php

class hypeValidatorDoctorAppointmentForm extends sfValidatorSchema
{
 	protected $location_title;
 	protected $complex_appointment_types;
 	protected $create_repeating_in_past;
 	protected $appointment_title_format;
 	protected $doctor_ids;
 	protected $date_format;

 	public function __construct($location_title, $complex_appointment_types, $create_repeating_in_past, $appointment_title_format, $skip_overlap, $doctor_ids, $date_format, $options = array(), $messages = array())
 	{
 		$this->location_title = $location_title;
 		$this->complex_appointment_types = $complex_appointment_types;
 		$this->create_repeating_in_past = $create_repeating_in_past;
 		$this->appointment_title_format = $appointment_title_format;
 		$this->skip_overlap = $skip_overlap;
 		$this->doctor_ids = $doctor_ids;
 		$this->date_format = $date_format;

 		parent::__construct(array(), $options, $messages);
 	}
 	protected function doClean($values)
 	{           
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$errors = new sfValidatorErrorSchema($this);

 		if ($values['start_date']) {
 			$values['start_date'] = hype::parseDateToInt(date('Y-m-d ' . $values['start_time'], $values['start_date']), hype::DB_ISO_DATE);
 			$values['end_date'] = strtotime(date(hype::DB_ISO_DATE, $values['start_date']) . ' + ' . $values['length'] . ' minutes');
 		}

 		if ($values['location_id']) {
 			$location = Doctrine_Query::create()
 				->from('Location l')
 				->addWhere('l.client_id = ?', $values['client_id'])
 				->addWhere('l.id = ?', $values['location_id'])
 				->addWhere('l.active = ?', true)
 				->fetchOne();

 			if (!$location instanceof Location) {
 				$e = new sfValidatorError($this, 'Invalid ' . ucwords($this->location_title) . '.');
 				$errors->addError($e, 'location_id');
 			}
 			else {
 				$values['location_name'] = $location->getShortCode();
 				$values['province'] = $location->prov_code;
 			}
 		}

 		if ($values['doctor_id'] && !in_array($values['doctor_id'], $this->doctor_ids)) {
 			$e = new sfValidatorError($this, 'You cannot book appointments for this doctor.');
 			$errors->addError($e, 'doctor_id');
 		}
 		else if ($values['doctor_id']) {
 			$doctor = Doctrine_Query::create()
 				->from('Doctor d')
 				->leftJoin('d.DoctorLocation dl')
 				->addWhere('d.client_id = ?', $values['client_id'])
 				->addWhere('d.id = ?', $values['doctor_id'])
 				->addWhere('d.active = ?', true)
 				->addWhere('dl.location_id = ?', $values['location_id'])
 				->fetchOne();

 			if (!$doctor instanceof Doctor) {
 				$e = new sfValidatorError($this, 'This doctor profile does not work in this ' . $this->location_title . '.');
 				$errors->addError($e, 'doctor_id');
 			}
 			else {
 				$values['doctor_code'] = $doctor->qc_doctor_code;
 				$values['province'] = $doctor->province;
 			}
 		}

 		if ($values['doctor_id'] && $values['location_id'] && $values['start_date'] && $values['end_date']) {
 			$dsb = Doctrine_Query::create()
 				->from('DoctorScheduleBlock dsb')
 				->addWhere('dsb.location_id = ?', $values['location_id'])
 				->addWhere('dsb.doctor_id = ?', $values['doctor_id'])
 				->addWhere('dsb.status = ?', DoctorScheduleBlockTable::STATUS_POSTED)
 				->addWhere('(?) between dsb.start_date and dsb.end_date', date(hype::DB_ISO_DATE, $values['start_date']))
 				->addWhere('(?) between dsb.start_date and dsb.end_date', date(hype::DB_ISO_DATE, $values['end_date']))
 				->fetchOne();

 			if (!$dsb instanceof DoctorScheduleBlock) {
 				$e = new sfValidatorError($this, 'No doctor is working at this ' . $this->location_title . ' from ' . date('g:i a', $values['start_date']) . ' to ' . date('g:i a', $values['end_date']) . ' inclusive.');
 				$errors->addError($e);
 			}
 		}

 		$appointment_type_query = Doctrine_Query::create()
 			->from('AppointmentType at')
 			->addWhere('at.id = ?', $values['appointment_type_id'])
 			->addWhere('at.client_id = ?', $values['client_id'])
 			->addWhere('at.active = ?', true);

 		if ($this->complex_appointment_types) {
 			$appointment_type_query->leftJoin('at.AppointmentTypeProvince atp')
 				->addWhere('atp.province_code = ? OR atp.province_code IS NULL', $values['province'])
 				->addWhere('atp.active = ?', true);
 		}
 		$appointment_type = $appointment_type_query->fetchOne();

 		if (!$appointment_type instanceof AppointmentType) {
 			$e = new sfValidatorError($this, 'Invalid Appointment Type');
 			$errors->addError($e);
 		}
 		else if ($appointment_type->patient_required && $values['patient_id']) {
 			$patient = Doctrine_Query::create()
 				->from('Patient p')
 				->leftJoin('p.PatientStatus ps')
 				->addWhere('p.client_id = ?', $values['client_id'])
 				->addWhere('p.id = ?', $values['patient_id'])
 				->addWhere('p.deleted = (?)', false)
 				->fetchOne();

 			if (!$patient instanceof Patient) {
 				$e = new sfValidatorError($this, 'PID not found or invalid.');
 				$errors->addError($e);
 			}
 			else if ($patient->offsetExists('PatientStatus') && !$patient->PatientStatus->bookable && !$values['id']) {
 				$e = new sfValidatorError($this, 'Invalid patient due to non-bookable patient status.');
 				$errors->addError($e);
 			}
 			else {
 				$values['patient_lname'] = $patient->lname;
 				$values['patient_fname'] = $patient->fname;
 				$values['patient_phone'] = $patient->hphone;
 				$values['patient_email'] = $patient->email;
 			}
 		}
 		else if ($appointment_type->patient_required) {
 			$e = new sfValidatorError($this, 'This appointment type requires a patient.');
 			$errors->addError($e);
 		}
 		else if (!$appointment_type->patient_required && $values['patient_id']) {
 			unset($values['patient_id']);
 		}

 		if ($appointment_type instanceof AppointmentType) {
 			if ($values['slot_type'] != $appointment_type->slot_type) {
 				$e = new sfValidatorError($this, 'Invalid slot type');
 				$errors->addError($e, 'slot_type');
 			}

 			if ($this->complex_appointment_types) {
 				$atp = $appointment_type->AppointmentTypeProvince[0];

 				if ($atp->min_scheduling && $atp->min_scheduling > $values['length']) {
 					$e = new sfValidatorError($this, 'Appointment must be at least ' . $atp->min_scheduling . ' minutes.');
 					$errors->addError($e, 'length');
 				}
 			}
 		}

 		if ($values['start_date'] && !$this->skip_overlap) {
 			if (!$values['override_schedule_check'] && !$values['repeats']) {
 				$q = Doctrine_Query::create()
 					->from('Appointment a')
 					->addWhere('a.client_id = ?', $values['client_id'])
 					->addWhere('a.location_id = ?', $values['location_id'])
 					->addWhere('a.doctor_id = ?', $values['doctor_id'])
 					->addWhere('a.slot_type = ? OR a.slot_type = ?', array(AppointmentTable::SLOT_TYPE_NORMAL, AppointmentTable::SLOT_TYPE_OTHER))
 					->addWhere('a.status = ?', AppointmentTable::STATUS_POSTED)
 		 			->addWhere('a.start_date < (?)', date(hype::DB_ISO_DATE, $values['end_date']))
 					->addWhere('a.end_date > (?)', date(hype::DB_ISO_DATE, $values['start_date']));

 				if ($values['id']) {
 					$q->addWhere('a.id != ?', $values['id']);
 				}
 				if ($this->complex_appointment_types) {
 					$q->leftJoin('a.AppointmentType at')
 					->leftJoin('at.AppointmentTypeProvince atp')
 					->addWhere('atp.province_code = (?)', $values['province']);
 				}

 				$appointments = $q->execute();

 				if (count($appointments)) {
 					$e = new sfValidatorError($this, 'This appointment overlaps with ' . count($appointments) . ' other appointment(s).');
 					$errors->addError($e);

 					if ($this->complex_appointment_types) {
 						$can_overlap = $appointment_type->AppointmentTypeProvince[0]->allow_double_book;
 					}
 					else {
 						$can_overlap = $appointment_type->allow_double_book;
 					}
 					$overlap_str = AppointmentTypeProvinceTable::OVERLAP_OK;
 					$no_overlap_str = AppointmentTypeProvinceTable::NO_OVERLAP;

 					foreach ($appointments as $appt)
 					{
 						if ($this->complex_appointment_types) {
 							$overlap_existing = $appt->AppointmentType->AppointmentTypeProvince[0]->allow_double_book;
 						}
 						else {
 							$overlap_existing = $appt->AppointmentType->allow_double_book;
 						}

 						if ($can_overlap && $overlap_existing) {
 							$overlap_msg = $overlap_str;
 						}
 						else {
 							$overlap_msg = $no_overlap_str;
 						}

 						$e = new sfValidatorError($this, 'Overlapping Appointment '
 						. ' [' . date('g:i a', hype::parseDateToInt($appt->start_date, hype::DB_ISO_DATE)) . '-' . date('g:i a', hype::parseDateToInt($appt->end_date, hype::DB_ISO_DATE)) . ']: '
 						. $appt->processAppointmentTitle($this->appointment_title_format, array('date_format' => $this->date_format)) . ($overlap_msg)
 						);
 						$errors->addError($e);

 					}
 				}
 			}
 		}

 		if ($values['repeats'] && $values['apply_to'] == 'only_current') {

 		}
 		else if ($values['repeats'] && !$values['id'] && !$this->create_repeating_in_past && $values['start_date'] < hype::parseDateToInt(date('Y-m-d 00:00:00'), hype::DB_ISO_DATE)) {
 			$e = new sfValidatorError($this, 'New Repeating Appointments must not begin in the past.');
 			$errors->addError($e, 'repeats');
 		}
 		else if ($values['repeats'] && $values['slot_type'] == AppointmentTable::SLOT_TYPE_OTHER) {
 			if (!$values['repeat_end_date']) {
 				$e = new sfValidatorError($this, 'Repeating Miscellaneous Appointments must have an end date.');
 				$errors->addError($e, 'repeat_end_date');
 			}
 			else if ($values['repeat_end_date'] < $values['start_date']) {
 				$e = new sfValidatorError($this, 'Repeating Appointments must end after the appointment being created.');
 				$errors->addError($e, 'repeat_end_date');
 			}
 			else if ($values['repeat_end_date'] > strtotime(date(hype::DB_DATE_FORMAT, $values['start_date']) . ' + 6 months')) {
 				$e = new sfValidatorError($this, 'Repeating Appointments must end within 6 months of the start date.');
 				$errors->addError($e, 'repeat_end_date');
 			}

 			if (!count($values['repeat_weekdays'])) {
 				$e = new sfValidatorError($this, 'Repeating Appointments must happen on at least one weekday.');
 				$errors->addError($e, 'repeat_weekdays');
 			}
 		}
 		else if ($values['repeats']) {
 			if (!count($values['repeat_weekdays'])) {
 				$e = new sfValidatorError($this, 'Repeating Appointments must happen on at least one weekday.');
 				$errors->addError($e, 'repeat_weekdays');
 			}

 			if (!$values['repeat_end_date']) {
 				$e = new sfValidatorError($this, 'Repeating Appointments must have an end date.');
 				$errors->addError($e, 'repeat_end_date');
 			}
 			else if ($values['repeat_end_date'] && $values['repeat_end_date'] < $values['start_date']) {
 				$e = new sfValidatorError($this, 'Repeating Appointments must end after the appointment being created.');
 				$errors->addError($e, 'repeat_end_date');
 			}
 		}
 		else {
 			$values['repeat_weekdays'] = array();
 			$values['repeat_end_date'] = null;
 			$values['appointment_template_id'] = null;
 		}

 		if ($values['repeats'] && !count($errors)) {
 			if (!$values['appointment_template_id'] || $values['apply_to'] == 'all_events') {
 				$values['AppointmentTemplate'] = $this->createNewAppointmentTemplate($values);

 				$values['repeating_appointment_dates'] = $values['AppointmentTemplate']->getAppointmentDates(false);
 				ksort($values['repeating_appointment_dates']);

 				foreach ($values['repeating_appointment_dates'] as $date)
 				{
 					$r_start = $date . ' ' . $values['AppointmentTemplate']->start_time;
 					$r_end = $date . ' ' . $values['AppointmentTemplate']->end_time;

 					$q = Doctrine_Query::create()
 						->from('Appointment a')
 						->addWhere('a.client_id = ?', $values['client_id'])
 						->addWhere('a.location_id = ?', $values['location_id'])
 						->addWhere('a.doctor_id = ?', $values['doctor_id'])
 						->addWhere('a.slot_type = ? OR a.slot_type = ?', array(AppointmentTable::SLOT_TYPE_NORMAL, AppointmentTable::SLOT_TYPE_OTHER))
 						->addWhere('a.status = ?', AppointmentTable::STATUS_POSTED)
 			 			->addWhere('a.start_date < (?)', $r_end)
 						->addWhere('a.end_date > (?)', $r_start);

 					if ($values['id']) {
 						$q->addWhere('a.id != ?', $values['id']);
 					}
 					if ($values['apply_to'] == 'all_events') {
 						$q->addWhere('a.appointment_template_id != (?)', $values['original_appointment_template_id']);
 					}
 					if ($this->complex_appointment_types) {
 						$q->leftJoin('a.AppointmentType at')
 						->leftJoin('at.AppointmentTypeProvince atp')
 						->addWhere('atp.province_code = (?)', $values['province']);
 					}

 					$appointments = $q->execute();

 					if (count($appointments) && array_key_exists('override_repeating_check', $values) && $values['override_repeating_check']) {
 						if ($this->complex_appointment_types) {
 							$can_overlap = $appointment_type->AppointmentTypeProvince[0]->allow_double_book;
 						}
 						else {
 							$can_overlap = $appointment_type->allow_double_book;
 						}

 						foreach ($appointments as $appt)
 						{
 							if ($this->complex_appointment_types) {
 								$overlap_existing = $appt->AppointmentType->AppointmentTypeProvince[0]->allow_double_book;
 							}
 							else {
 								$overlap_existing = $appt->AppointmentType->allow_double_book;
 							}

 							if (!($can_overlap && $overlap_existing)) {
 								unset($values['repeating_appointment_dates'][date(hype::DB_DATE_FORMAT, hype::parseDateToInt($appt->start_date, hype::DB_ISO_DATE))]);
 							}
 						}
 					}
 					else if (count($appointments)) {
 						if ($this->complex_appointment_types) {
 							$can_overlap = $appointment_type->AppointmentTypeProvince[0]->allow_double_book;
 						}
 						else {
 							$can_overlap = $appointment_type->allow_double_book;
 						}

 						$overlap_str = AppointmentTypeProvinceTable::OVERLAP_OK;
 						$no_overlap_str = AppointmentTypeProvinceTable::NO_OVERLAP;

 						foreach ($appointments as $appt)
 						{
 							if ($this->complex_appointment_types) {
 								$overlap_existing = $appt->AppointmentType->AppointmentTypeProvince[0]->allow_double_book;
 							}
 							else {
 								$overlap_existing = $appt->AppointmentType->allow_double_book;
 							}

 							if ($can_overlap && $overlap_existing) {
 								$overlap_msg = $overlap_str;
 							}
 							else {
 								$overlap_msg = $no_overlap_str;
 							}

 							$e = new sfValidatorError($this, 'Overlapping Future Appointment '
 							. ' [' . date('M j Y, g:i a', hype::parseDateToInt($appt->start_date, hype::DB_ISO_DATE)) . '-' . date('g:i a', hype::parseDateToInt($appt->end_date, hype::DB_ISO_DATE)) . ']: '
 							. $appt->processAppointmentTitle($this->appointment_title_format, array('date_format' => $this->date_format)) . ($overlap_msg)
 							);
 							$errors->addError($e);

 						}
 					}
 				}
 			}
 		}

 		if ($values['original_appointment_template_id'] && $values['original_appointment_template_id'] != $values['appointment_template_id']) {
 			$e = new sfValidatorError($this, 'Repeating Appointment Template ID has been altered in the form.');
 			$errors->addError($e, 'appointment_template_id');
 		}

 		if (count($errors)) {
 			throw $errors;
 		}

 		return $values;
 	}
 	private function createNewAppointmentTemplate($values)
 	{
 		$at = new AppointmentTemplate();
 		$at->client_id           = $values['client_id'];
 		if ($values['doctor_id']) {
 			$at->doctor_id           = $values['doctor_id'];
 		}
 		$at->location_id         = $values['location_id'];
 		$at->appointment_type_id = $values['appointment_type_id'];
 		$at->repeat_sun          = in_array('sun', $values['repeat_weekdays']);
 		$at->repeat_mon          = in_array('mon', $values['repeat_weekdays']);
 		$at->repeat_tues         = in_array('tues', $values['repeat_weekdays']);
 		$at->repeat_wed          = in_array('wed', $values['repeat_weekdays']);
 		$at->repeat_thurs        = in_array('thurs', $values['repeat_weekdays']);
 		$at->repeat_fri          = in_array('fri', $values['repeat_weekdays']);
 		$at->repeat_sat          = in_array('sat', $values['repeat_weekdays']);
 		$at->start_time          = date('H:i', $values['start_date']);
 		$at->end_time            = date('H:i', $values['end_date']);
 		$at->start_date          = date('Y-m-d 00:00:00', $values['start_date']);
 		$at->status              = AppointmentTemplateTable::STATUS_POSTED;
 		if ($values['repeat_end_date']) {
 			$at->end_date        = date('Y-m-d 23:59:59', $values['repeat_end_date']);
 		}

 		return $at;
 	}
}
