<?php

class hypeValidatorConsultationPatientForm extends sfValidatorSchema
{
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$errors = new sfValidatorErrorSchema($this);

 		if ($values['reconsultation']) {
 			if (!$values['last_location_id']) {
 				$e = new sfValidatorError($this, 'Last Clinic is required for reconsultations.');
 				$errors->addError($e, 'last_location_id');
 			}

 			if (!$values['years_ago']) {
 				$e = new sfValidatorError($this, 'Years Ago is requried for reconsultations.');
 				$errors->addError($e, 'years_ago');
 			}
 		}

 		// if paid ensure we have a method and amount
 		if ($values['deposit_paid']) {
 			if (!$values['payment_method']) {
 				$e = new sfValidatorError($this, 'Payment method is required for paid attendees.');
 				$errors->addError($e, 'payment_method');
 			}

 			if (!$values['payment_amount']) {
 				$e = new sfValidatorError($this, 'Amount Paid is required for paid attendees.');
 				$errors->addError($e, 'payment_amount');
 			}

 			if (!$values['authorization_code']) {
 				$e = new sfValidatorError($this, 'Authorization code is required for paid attendees.');
 				$errors->addError($e, 'authorization_code');
 			}
 		}

 		// find out what the calculated deposit and total amount should be
 		if (!count($errors) && $values['patient_type']) {
 			$appointment = Doctrine_Query::create()
 				->from('Appointment a')
 				->leftJoin('a.Location l')
 				->leftJoin('a.AppointmentAttendee att')
 				->addWhere('a.id = ?', $values['appointment_id'])
 				->addWhere('att.status = (?) OR att.status = (?) OR att.status = (?) OR att.status is null',
 					array(AppointmentAttendeeTable::STATUS_BOOKED, AppointmentAttendeeTable::STATUS_ATTENDED, AppointmentAttendeeTable::STATUS_NO_SHOW))
 				->fetchOne();

 			if ($appointment instanceOf Appointment) {
 				$province_code = $appointment->Location->prov_code;

 				$consultation_fee = Doctrine_Query::create()
 					->from('ConsultationFee cf')
 					->addWhere('cf.prov_code = ?', $province_code)
 					->addWhere('cf.client_id = ?', $appointment->client_id)
 					->addWhere('cf.start_date <= (?)', $appointment->start_date)
 					->addWhere('cf.end_date >= (?) OR cf.end_date is null', $appointment->end_date)
 					->fetchOne();

 				if ($consultation_fee instanceOf ConsultationFee) {

 					if ($values['reconsultation']) {
 						$values['calculated_total'] = $consultation_fee->reconsultation_total;
 						$values['calculated_deposit'] = $consultation_fee->reconsultation_total;
 					}
 					else {
 						$values['calculated_deposit'] = $consultation_fee->consultation_deposit;

 						switch ($values['patient_type']) {
 							case AppointmentAttendeeTable::PAT_TYPE_CHILD:
 								$values['calculated_total'] = $consultation_fee->child_total;
 								break;
 							case AppointmentAttendeeTable::PAT_TYPE_REGULAR:
 								$values['calculated_total'] = $consultation_fee->consultation_total;
 								break;
 							case AppointmentAttendeeTable::PAT_TYPE_STUDENT:
 								$values['calculated_total'] = $consultation_fee->student_total;
 								break;
 						}
 					}
 				}

 				if (!$values['id'] && count($appointment->AppointmentAttendee) >= $appointment->max_attendees) {
 					$e = new sfValidatorError($this, 'This consultation is fully booked.');
 					$errors->addError($e);
 				}
 			}
 		}

 		if (count($errors)) {
 			throw $errors;
 		}

 		return $values;
 	}
}
