<?php

class hypeValidatorConsultationCancellationForm extends sfValidatorSchema
{
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$errors = new sfValidatorErrorSchema($this);

 		if ($values['deposit_originally_paid']) {
 			if (array_key_exists('refund_required', $values) && $values['refund_required']) {
 //				if (!$values['volusion_number']) {
 //					$error = new sfValidatorError($this, 'Reference Number is required.');
 //					$errors->addError($error, 'volusion_number');
 //				}
 //				else if ($values['volusion_number'] != $values['original_authorization_code']) {
 //					$error = new sfValidatorError($this, 'Reference Number should match original transaction reference number.');
 //					$errors->addError($error, 'volusion_number');
 //				}
 			}
 			else {
 //				if ($values['volusion_number']) {
 //					$error = new sfValidatorError($this, 'No refund.  Reference Number should be empty.');
 //					$errors->addError($error, 'volusion_number');
 //				}
 			}

 		}
 		else {
 			if (array_key_exists('refund_required', $values) && $values['refund_required']) {
 				$error = new sfValidatorError($this, 'A deposit was not originally paid.');
 				$errors->addError($error, 'refund_required');
 			}

 //			if ($values['volusion_number']) {
 //				$error = new sfValidatorError($this, 'No refund.  Reference Number should be blank.');
 //				$errors->addError($error, 'volusion_number');
 //			}

 		}

 		if (count($errors)) {
 			throw $errors;
 		}

 		return $values;
 	}
}
