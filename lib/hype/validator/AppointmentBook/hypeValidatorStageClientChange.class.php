<?php

class hypeValidatorStageClientChange extends sfValidatorSchema
{
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$errors = new sfValidatorErrorSchema($this);
 		if ($values['original_stage_client_id'] && $values['original_stage_client_id'] != $values['stage_client_id']) {
 			$old_stage = Doctrine_Query::create()
 				->from('StageClient sc')
 				->addWhere('sc.client_id = (?)', $values['client_id'])
 				->addWhere('sc.id = (?)', $values['original_stage_client_id'])
 				->fetchOne();

 			$new_stage = Doctrine_Query::create()
 				->from('StageClient sc')
 				->addWhere('sc.client_id = (?)', $values['client_id'])
 				->addWhere('sc.id = (?)', $values['stage_client_id'])
 				->addWhere('sc.active = (?)', true)
 				->fetchOne();

 			if (!$old_stage instanceOf StageClient) {
 				$e = new sfValidatorError($this, 'Invalid Appointment Stage');
 				$errors->addError($e, 'stage_client_id');
 			}
 			if (!$new_stage instanceOf StageClient) {
 				$e = new sfValidatorError($this, 'Invalid Appointment Stage');
 				$errors->addError($e, 'stage_client_id');
 			}

 			if (count($errors)) {
 				throw $errors;
 			}

 			$values['stage_changed'] = true;

 			$valid_stage_changes = $old_stage->getStageFollowingArray();

 			if (!array_key_exists($new_stage->id, $valid_stage_changes)) {
 				$e = new sfValidatorError($this, 'Invalid Appointment Stage transition.');
 				$errors->addError($e, 'stage_client_id');
 			}
 		}

 		if (count($errors)) {
 			throw $errors;
 		}

 		return $values;
 	}
}
