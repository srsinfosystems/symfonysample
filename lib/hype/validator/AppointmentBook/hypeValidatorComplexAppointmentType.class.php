<?php

class hypeValidatorComplexAppointmentType extends sfValidatorSchema
{
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$errors = new sfValidatorErrorSchema($this);

 		// no duplicate provinces
 		$provinces = array();

 		$c = 0;
 		while (array_key_exists($c . '_province_code', $values))
 		{
 			if (in_array($values[$c . '_province_code'], $provinces)) {
 				$e = new sfValidatorError($this, 'A record for ' . $values[$c . '_province_code'] . ' has already been defined');
 				$errors->addError($e, $c . '_province_code');
 			}
 			else {
 				$provinces[$c] = $values[$c . '_province_code'];
 			}
 			$c++;
 		}

 		// no duplicate appointment type names
 		$appointment_type = Doctrine_Query::create()
 			->from('AppointmentType at')
 			->addWhere('at.name = ?', strtoupper($values['name']))
 			->addWhere('at.client_id = ?', $values['client_id']);

 		if ($values['id']) {
 			$appointment_type->addWhere('at.id != ?', $values['id']);
 		}
 		if ($values['slot_type']) {
 			$appointment_type->addWhere('at.slot_type = ?', $values['slot_type']);
 		}

 		$appointment_type = $appointment_type->fetchOne();

 		if ($appointment_type instanceof AppointmentType) {
 			$e = new sfValidatorError($this, 'You cannot define a second appointment type with this name.');
 			$errors->addError($e, 'name');
 		}

 		// each default length must be greater or equal the provincial minimum
 		$c = 0;
 		while (array_key_exists($c . '_province_code', $values))
 		{
 			$lengths = $values[$c . '_appt_lengths'];
 			$min = $values[$c . '_min_scheduling'];

 			if ($lengths && $min) {
 				if (strpos($lengths, ',')) {
 					$lengths = explode(',', $lengths);
 				}
 				else {
 					$lengths = array($lengths);
 				}

 				foreach ($lengths as $key => $length)
 				{
 					$length = trim($length);
 					if (intval($length) < intval($min)) {
 						$e = new sfValidatorError($this, 'Appointment Lengths must be greater than or equal Scheduling Minimum');
 						$errors->addError($e, $c . '_appt_lengths');
 						break;
 					}

 					$lengths[$key] = $length;
 				}

 				sort($lengths, SORT_NUMERIC);
 				$values[$c . '_appt_lengths'] = implode(',', $lengths);

 			}

 			$c++;
 		}

 //		echo '<pre>';
 //		print_r ($values);
 //		exit();

 		if (count($errors)) {
 			throw $errors;
 		}

 		return $values;
 	}
}
