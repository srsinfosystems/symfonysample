<?php

class hypeValidatorCallback extends sfValidatorCallback
{
	protected function configure($options = array(), $messages = array())
	{
		$this->addRequiredOption('callback');
		$this->addOption('arguments', array());

		$this->setOption('required', false);
		$this->addMessage('maxLength', 'Exceeds %length% characters');
		$this->addMessage('regex', 'Invalid');
		$this->addMessage('minimum_year', 'Minimum Year not met');
	}
}
