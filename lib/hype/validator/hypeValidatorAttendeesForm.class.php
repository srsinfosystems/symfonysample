<?php

class hypeValidatorAttendeesForm extends sfValidatorSchema
{
 	protected function doClean($values)
 	{
 		if (null === $values)
 		{
 			$values = array();
 		}

 		if (!is_array($values))
 		{
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}
 		$form_errors = array();

 		// basic form error checking - put these in $form_errors

 		foreach (array_keys($values) as $index) {
 			$widget_errors = array();
 			if (strpos($index, 'attendee_') === 0) {

 				$check_patient_fname = false;
 				// require patient_fname for first attendee
 				if ($index == 'attendee_1' || $values[$index]['id']) {
 					$check_patient_fname = true;
 				}
 				else {
 					// if anything is filled in, check patient_fname
 					foreach ($values[$index] as $val) {
 						if ($val) {
 							$check_patient_fname = true;
 						}
 					}

 					// if nothing is filled in, remove the index
 					if (!$check_patient_fname) {
 						unset($values[$index]);
 					}
 				}

 				// check patient name
 				if ($check_patient_fname && !$values[$index]['patient_fname']) {
 					$widget_errors['patient_fname'] = new sfValidatorError($this, 'required');
 				}

 				// other checks - add to $widget_errors

 				if (count($widget_errors)) {
 					$form_errors[$index] = new sfValidatorErrorSchema($this, $widget_errors);
 				}

 				if (array_key_exists($index, $values)) {
 					$values['attendees'][] = $values[$index];
 					unset($values[$index]);
 				}
 			}
 		}
 		if (count($form_errors)) {
 			throw new sfValidatorErrorSchema($this, $form_errors);
 		}

 		return $values;
 	}
}
