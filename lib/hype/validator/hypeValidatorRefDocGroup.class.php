<?php

class hypeValidatorRefDocGroup extends sfValidatorSchema
{
 	public function __construct($fields = array(), $options = array(), $messages = array())
 	{
 		$this->addOption('refdoc_id', $fields['refdoc_id']);
 		$this->addOption('addr_id', $fields['addr_id']);
 		$this->addOption('description', $fields['description']);
 		$this->addOption('provider_num', $fields['provider_num']);

 	    parent::__construct(null, $options, $messages);
 	}
 	function doClean($values)
 	{
 		$refdoc_id = $values[$this->getOption('refdoc_id')];
 		$addr_id = $values[$this->getOption('addr_id')];
 		$description = $values[$this->getOption('description')];
 		$provider_num = $values[$this->getOption('provider_num')];

 		if ($description) {
 			$subdesc = substr($description, 0, 6);
 			if (intval($subdesc) == $subdesc) {
 				$values[$this->getOption('provider_num')] = $subdesc;
 			}
 		}

 		if (!$refdoc_id) {
 			$values[$this->getOption('refdoc_id')] = null;
 		}
 		if (!$addr_id) {
 			$values[$this->getOption('addr_id')] = null;
 		}

 		return $values;
 	}
}

