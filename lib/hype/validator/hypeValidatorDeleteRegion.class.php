<?php

class hypeValidatorDeleteRegion extends sfValidatorSchema
{
 	public function __construct($location_title, $options = array(), $messages = array())
 	{
 		$this->location_title = $location_title;
 		parent::__construct(null, $options, $messages);
 	}
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$errors = new sfValidatorErrorSchema($this, array(), array());

 		if ($values['deleted']) {
 			if ($values['active']) {
 				$e = new sfValidatorError($this, 'Cannot delete an active region.');
 				$errors->addError($e, 'active');
 			}
 			else {
 				$locations = Doctrine_Query::create()
 					->from('Location l')
 					->addWhere('l.region_id = ?', $values['id'])
 					->addWhere('l.active = ?', true)
 					->execute();

 				if (count($locations)) {
 					$locs = array();
 					foreach ($locations as $loc) {
 						$locs[] = $loc;
 					}

 					$e = new sfValidatorError($this, 'Cannot de-activate a region while there are associated active ' .  $this->location_title . 's.  (' . implode(', ', $locs) . ')');
 					$errors->addError($e, 'active');
 				}
 			}
 		}
 		else if (!$values['active'] && $values['id']) {
 			$locations = Doctrine_Query::create()
 				->from('Location l')
 				->addWhere('l.region_id = ?', $values['id'])
 				->addWhere('l.active = ?', true)
 				->execute();

 				if (count($locations)) {
 					$locs = array();
 					foreach ($locations as $loc) {
 						$locs[] = $loc;
 					}

 					$e = new sfValidatorError($this, 'Cannot de-activate a region while there are associated active ' .  $this->location_title . 's.  (' . implode(', ', $locs) . ')');
 					$errors->addError($e, 'active');
 				}
 		}

 		if (count($errors)) {
 			throw $errors;
 		}
 		return $values;
 	}
}
