<?php

class hypeValidatorUserProfile extends sfValidatorSchema
{
 	public function __construct($options = array(), $messages = array())
 	{
 		parent::__construct(null, $options, $messages);
 	}
 	protected function configure($options = array(), $messages = array())
 	{
 		$this->addOption('throw_global_error', false);

 		$this->setMessage('invalid', 'We already have this username in our system.');
 	}
 	protected function doClean($values)
 	{
 		$originalValues = $values;
 		$user_id = array_key_exists('sfg_user_id', $values) ? $values['sfg_user_id'] : null;

 		$object = Doctrine_Query::create()
 			->from('sfGuardUser sfg')
 			->leftJoin('sfg.UserProfile up')
 			->addWhere('sfg.username = (?)', $values['username'])
 			->addWhere('up.archived = (?)', false)
 			->fetchOne();

 		if (!$object || $object->id == $user_id) {
 			return $originalValues;
 		}
 		throw new sfValidatorErrorSchema($this, array('username' => new sfValidatorError($this, 'invalid')));
 	}
}
