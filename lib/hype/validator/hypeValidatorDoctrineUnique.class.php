<?php

class hypeValidatorDoctrineUnique extends sfValidatorSchema
{
 	public function __construct($options = array(), $messages = array())
 	{
 		parent::__construct(null, $options, $messages);
 	}
 	protected function configure($options = array(), $messages = array())
 	{
 		$this->addRequiredOption('model');
 		$this->addRequiredOption('column');
 		$this->addOption('primary_key', null);
 		$this->addOption('connection', null);
 		$this->addOption('throw_global_error', false);

 		$this->setMessage('invalid', 'An object with the same "%column%" already exist.');
 	}
 	protected function doClean($values)
 	{
 		$originalValues = $values;
 		$table = Doctrine_Core::getTable($this->getOption('model'));
 		if (!is_array($this->getOption('column'))) {
 			$this->setOption('column', array($this->getOption('column')));
 		}

 		//if $values isn't an array, make it one
 		if (!is_array($values)) {
 			//use first column for key
 			$columns = $this->getOption('column');
 			$values = array($columns[0] => $values);
 		}

 		$q = Doctrine_Core::getTable($this->getOption('model'))->createQuery('a');
 		foreach ($this->getOption('column') as $column)
 		{
 			$colName = $table->getColumnName($column);
 			if (!array_key_exists($column, $values)) {
 				// one of the column has be removed from the form
 				return $originalValues;
 			}

 			$q->addWhere('a.' . $colName . ' = ?', $values[$column]);
 		}

 		if ($table->hasColumn('deleted')) {
 			$q->addWhere('a.deleted = ?', false);
 		}

 		$object = $q->fetchOne();

 		// if no object or if we're updating the object, it's ok
 		if (!$object || $this->isUpdate($object, $values))
 		{
 			return $originalValues;
 		}

 		$error = new sfValidatorError($this, 'invalid', array('column' => implode(', ', $this->getOption('column'))));

 		if ($this->getOption('throw_global_error'))
 		{
 			throw $error;
 		}

 		$columns = $this->getOption('column');

 		throw new sfValidatorErrorSchema($this, array($columns[0] => $error));
 	}
 	protected function isUpdate(Doctrine_Record $object, $values)
 	{
 		// check each primary key column
 		foreach ($this->getPrimaryKeys() as $column)
 		{
 			if (!array_key_exists($column, $values) || $object->$column != $values[$column])
 			{
 				return false;
 			}
 		}

 		return true;
 	}
 	protected function getPrimaryKeys()
 	{
 		if (null === $this->getOption('primary_key'))
 		{
 			$primaryKeys = Doctrine_Core::getTable($this->getOption('model'))->getIdentifier();
 			$this->setOption('primary_key', $primaryKeys);
 		}

 		if (!is_array($this->getOption('primary_key')))
 		{
 			$this->setOption('primary_key', array($this->getOption('primary_key')));
 		}

 		return $this->getOption('primary_key');
 	}
}