<?php

class hypeValidatorRegionProvince extends sfValidatorSchema
{
 	function doClean($values)
 	{
 		$errors = new sfValidatorErrorSchema($this, array(), array());

 		if ($values['region_id'] && $values['prov_code']) {
 			$region = Doctrine_Query::create()
 				->from('Region r')
 				->addWhere('r.id = ?', $values['region_id'])
 				->addWhere('r.province_code = ?', $values['prov_code'])
 				->addWhere('r.deleted = ?', false)
 				->fetchOne();

 			if (!$region instanceof Region) {
 				$e = new sfValidatorError($this, 'Region & Province do not match');
 				$errors->addError($e, 'region_id');
 			}
 		}

 		if (count($errors)) {
 			throw $errors;
 		}
 		return $values;
 	}

}