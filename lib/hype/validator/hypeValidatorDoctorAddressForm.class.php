<?php

class hypeValidatorDoctorAddressForm extends sfValidatorSchema
{
 	function doClean($values)
 	{
 		$errors = new sfValidatorErrorSchema($this, array(), array());

 		if (!$values['phone'] && !$values['cell']) {
 			$e = new sfValidatorError($this, 'We require at least one of home, cell phone');
 			$errors->addError($e, 'phone');
 		}

 		if ($values['province'] && $values['province'] != 'ON') {
 			$e = new sfValidatorError($this, 'Ontario is the only valid province in our system currently');
 			$errors->addError($e, 'province');
 		}

 		if (count($errors)) {
 			throw $errors;
 		}
 		return $values;
 	}
}