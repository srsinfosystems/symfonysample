<?php

class hypeValidatorHolidayDuplicate extends sfValidatorSchema
{
 	public function __construct($options = array(), $messages = array())
 	{
 		$this->addOption('date_format', $options['date_format']);
 		parent::__construct(null, $options, $messages);
 	}
 	protected function doClean($values)
 	{
 		if (null === $values)
 		{
 			$values = array();
 		}

 		if (!is_array($values))
 		{
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$errors = new sfValidatorErrorSchema($this, array(), array());

 		if (array_key_exists('date', $values)) {
 			$duplicates = Doctrine_Query::create()
 				->from('Holiday h')
 				->addWhere('h.client_id = ?', $values['client_id'])
 				->addWhere('h.date = ?', date(hype::DB_DATE_FORMAT, $values['date']))
 				->addWhere('h.active = ?', true);

 			if ($values['id']) {
 				$duplicates->addWhere('h.id != (?)', $values['id']);
 			}
 			$duplicates = $duplicates->execute();

 			foreach ($duplicates as $dup) {
 				if ($dup->province_code == $values['province_code']) {
 					$text = 'There is already a holiday (' . $dup->name . ') set for this date';
 					if ($dup->province_code) {
 						$text .= ' in ' . $dup->province_code;
 					}
 					$text .= '.';

 					$e = new sfValidatorError($this, $text);
 					$errors->addError($e, 'date');
 				}
 				else if (!$dup->province_code || !$values['province_code']) {
 					$text = 'There is already a holiday (' . $dup->name . ') set for this date in ' . ($dup->province_code ? $dup->province_code : $values['province_code']);

 					$e = new sfValidatorError($this, $text);
 					$errors->addError($e, 'date');
 				}
 			}

 			// check for duplicates in the same calendar year with matching name and province code
 			if (!count($errors)) {
 				$duplicates = Doctrine_Query::create()
 					->from('Holiday h')
 					->addWhere('h.client_id = ?', $values['client_id'])
 					->addWhere('year(h.date) = ?', date('Y', $values['date']))
 					->addWhere('h.active = ?', true)
 					->addWhere('h.name = ?', $values['name']);

 				if ($values['id']) {
 					$duplicates->addWhere('h.id != (?)', $values['id']);
 				}
 				$duplicates = $duplicates->execute();

 				foreach ($duplicates as $dup)
 				{
 					if ($dup->province_code == $values['province_code'] || !$dup->province_code || !$values['province_code']) {
 						$text = 'There is already a ' . $values['name'] . ' in ' . date('Y', $values['date']);
 						if ($dup->province_code) {
 							$text .= ' in ' . $dup->province_code;
 						}
 						$text .= 'on ' . date($this->getOption('date_format'), hype::parseDateToInt($dup->date, hype::DB_ISO_DATE)) . '.';

 						$e = new sfValidatorError($this, $text);
 						$errors->addError($e, 'date');
 					}
 				}
 			}
 		}

 		if (count($errors)) {
 			throw $errors;
 		}

 		return $values;
 	}
}
