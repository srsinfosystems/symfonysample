<?php

class hypeValidatorStageClientStageName extends sfValidatorSchema
{
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$errors = new sfValidatorErrorSchema($this);

 		$q = Doctrine_Query::create()
 			->from('StageClient sc')
 			->addWhere('sc.client_id = ?', $values['client_id'])
 			->addWhere('sc.stage_name = ?', $values['stage_name']);

 		if ($values['id']) {
 			$q->addWhere('sc.id != ?', $values['id']);
 		}

 		$stage_client = $q->fetchOne();

 		if ($stage_client instanceof StageClient) {
 			$error = new sfValidatorError($this, 'You already have a stage with this name');
 			$errors->addError($error, 'stage_name');
 			throw $errors;
 		}

 		$stage = Doctrine_Query::create()
 			->from('Stage s')
 			->addWhere('s.stage_name = ?', $values['stage_name'])
 			->fetchOne();

 		$values['stage_id'] = null;
 		if ($stage instanceof Stage) {
 			$values['stage_id'] = $stage->id;
 		}

 		if (count($errors)) {
 			throw $errors;
 		}

 		return $values;
 	}
}
