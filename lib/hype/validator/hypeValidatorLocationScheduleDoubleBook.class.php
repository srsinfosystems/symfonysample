<?php

class hypeValidatorLocationScheduleDoubleBook extends sfValidatorSchema
{
 	public function __construct($options = array(), $messages = array())
 	{
 		parent::__construct(null, $options, $messages);
 	}
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}


 		if ($values['effective_start']) {
 			$date = date(hype::DB_DATE_FORMAT, $values['effective_start']);
 			$location_schedules = Doctrine_Query::create()
 				->from('LocationSchedule ls')
 				->addWhere('ls.location_id = ?', $values['location_id'])
 				->addWhere('ls.id != (?)', $values['id'])
 				->addWhere('ls.effective_start <= (?) AND ls.effective_end >= (?)', array($date, $date));

 			if ($location_schedules->count()) {
 				$errors = new sfValidatorErrorSchema($this, array(), array());
 				$e = new sfValidatorError($this, 'Effective Start Date overlaps with existing schedules.');
 				$errors->addError($e, 'effective_start');
 				throw $errors;
 			}
 		}

 		if ($values['effective_end']) {
 			$date = date(hype::DB_DATE_FORMAT, $values['effective_end']);
 			$location_schedules = Doctrine_Query::create()
 				->from('LocationSchedule ls')
 				->addWhere('ls.location_id = ?', $values['location_id'])
 				->addWhere('ls.id != (?)', $values['id'])
 				->addWhere('ls.effective_start <= (?) AND ls.effective_end >= (?)', array($date, $date));

 			if ($location_schedules->count()) {
 				$errors = new sfValidatorErrorSchema($this, array(), array());
 				$e = new sfValidatorError($this, 'Effective End Date overlaps with existing schedules.');
 				$errors->addError($e, 'effective_end');
 				throw $errors;
 			}
 		}

 		return $values;
 	}
}
