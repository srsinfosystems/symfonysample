<?php

class hypeValidatortime extends sfValidatorBase 
{
 	protected function configure($options = array(), $messages = array())
 	{
 		$this->addMessage('invalid_date', 'Date must be of the form H:i (24-hour clock)');
 		$this->addMessage('invalid_hour', 'Hour must be between 0 and 23');
 		$this->addMessage('invalid_minute', 'Minute must be between 0 and 59');
 		$this->addRequiredOption('time_format', $options['time_format']);
 	}
 	function doClean($value)
 	{
 		if (is_string($value) && $this->getOption('time_format') == 'H:i') {
 			$hour = intval(substr($value, 0, 2));
 			$minute = substr($value, 3, 2);

 			if ($hour >= 24 || $hour < 0) {
 				throw new sfValidatorError($this, 'invalid_hour', array());
 			}

 			if ($minute >= 60 || $minute < 0) {
 				throw new sfValidatorError($this, 'invalid_minute', array());
 			}

 			return $hour . ':' . $minute;

 			throw new sfValidatorError($this, 'invalid_date', array());
 		}

        return '';
 	}
}