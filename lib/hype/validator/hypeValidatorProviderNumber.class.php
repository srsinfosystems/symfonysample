<?php

class hypeValidatorProviderNumber extends sfValidatorSchema
{
 	function doClean($values)
 	{
 		$errors = new sfValidatorErrorSchema($this, array(), array());

 		if ($values['provider_num'] && !is_numeric($values['provider_num'])) {
 			$e = new sfValidatorError($this, 'Provider Number can only be digits. (6 digits max)');
 			$errors->addError($e, 'provider_num');
 		}

 		if (count($errors)) {
 			throw $errors;
 		}
 		return $values;
 	}

}