<?php

class hypeValidatorConsultationFeeOverlap extends sfValidatorSchema
{
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		if ($values['start_date']) {
 			$date = date(hype::DB_DATE_FORMAT, $values['start_date']);
 			$consult_fees = Doctrine_Query::create()
 				->from('ConsultationFee cf')
 				->addWhere('cf.prov_code = ?', $values['prov_code'])
 				->addWhere('cf.start_date <= (?) AND cf.end_date >= (?)', array($date, $date));

 			if ($values['id']) {
 				$consult_fees->addWhere('cf.id != (?)', $values['id']);
 			}

 			if ($consult_fees->count()) {
 				$errors = new sfValidatorErrorSchema($this, array(), array());
 				$e = new sfValidatorError($this, 'Start Date overlaps with existing record(s).');
 				$errors->addError($e, 'start_date');
 				throw $errors;
 			}
 		}

 		if ($values['end_date']) {
 			$date = date(hype::DB_DATE_FORMAT, $values['end_date']);
 			$consult_fees = Doctrine_Query::create()
 				->from('ConsultationFee cf')
 				->addWhere('cf.prov_code = ?', $values['prov_code'])
 				->addWhere('cf.start_date <= (?) AND cf.end_date >= (?)', array($date, $date));

 			if ($values['id']) {
 				$consult_fees->addWhere('cf.id != (?)', $values['id']);
 			}

 			if ($consult_fees->count()) {
 				$errors = new sfValidatorErrorSchema($this, array(), array());
 				$e = new sfValidatorError($this, 'Effective End Date overlaps with existing record(s).');
 				$errors->addError($e, 'end_date');
 				throw $errors;
 			}
 		}

 		return $values;
 	}
}
