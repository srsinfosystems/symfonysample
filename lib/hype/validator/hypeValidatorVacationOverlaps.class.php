<?php

class hypeValidatorVacationOverlaps extends sfValidatorSchema
{
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}
 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$errors = new sfValidatorErrorSchema($this, array(), array());

 		if ($values['start_date'] && $values['end_date'] && $values['doctor_id']) {
 			$q = Doctrine_Query::create()
 				->from('VacationDay vd')
 				->addWhere('vd.doctor_id = ?', $values['doctor_id'])
 				->addWhere('vd.status = ?', VacationDayTable::STATUS_PENDING)
 				->addWhere('vd.active = ?', true)
 				->addWhere('vd.date >= ?', date('Y-m-d 00:00', $values['start_date']))
 				->addWhere('vd.date <= ?', date('Y-m-d 23:59', $values['end_date']));

 			if ($values['id']) {
 				$q->addWhere('vd.vacation_id != (?)', $values['id']);
 			}

 			$vacation_days = $q->execute();

 			$error_days = array();
 			if ($vacation_days) {
 				foreach ($vacation_days as $vd)
 				{
 					$error_days[] = date('D, M d', hype::parseDateToInt($vd->date, hype::DB_ISO_DATE));
 				}

 				if (count($error_days)) {
 					$e = new sfValidatorError($this, 'This doctor already has vacation days on: ' . implode('; ', $error_days));
 					$errors->addError($e, 'start_date');
 				}
 			}
 		}

 		if (count($errors)) {
 			throw $errors;
 		}
 		return $values;
 	}
}
