<?php

class hypeValidatorClientUser extends sfValidatorBase
{
 	public function configure($options = array(), $messages = array())
 	{
 		$this->addOption('username_field', 'username');
 		$this->addOption('password_field', 'password');
 		$this->addOption('throw_global_error', false);

 		$this->setMessage('invalid', 'There is a problem with your client account. Please contact your administrator');
 	}
 	protected function doClean($values)
 	{

 		if (array_key_exists('user', $values)) {
 			if (!$values['user']->UserProfile->active || !$values['user']->UserProfile->Client->canLogIn()) {
     			throw new sfValidatorErrorSchema($this, array(new sfValidatorError($this, 'invalid')));
 			}
 		}

 		return $values;
 	}
 	protected function getTable()
 	{
 		return Doctrine::getTable('sfGuardUser');
 	}
}
