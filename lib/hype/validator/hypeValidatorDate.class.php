<?php

class hypeValidatorDate extends sfValidatorBase {

    protected function configure($options = array(), $messages = array()) {
        $this->addMessage('invalid_date', 'Date must be of the form %date_format%');
        $this->addRequiredOption('date_format', $options['date_format']);
        $this->addOption('with_time', false);
        $this->addOption('minimum_year', false);
    }

    public function doClean($value) {
        if (is_string($value)) {
            if (strlen($value) > 16) {
                $value = substr($value, 0, -3);
            }
            
            $date = date_parse_from_format($this->getOption('date_format'), $value);

            if ($date['year'] < $this->getOption('minimum_year')) {
                throw new sfValidatorError($this, 'Year must be greater than or equal to ' . $this->getOption('minimum_year'));
            } else if ($date['warning_count'] || $date['error_count']) {
                throw new sfValidatorError($this, 'Invalid Date Format');
            }

            $date = mktime($date['hour'], $date['minute'], 0, $date['month'], $date['day'], $date['year']);

            if (!$date) {
                $date = DateTime::createFromFormat($this->getOption('date_format'), $value);
                if (!$this->getOption('with_time')) {
                    $date->setTime(0, 0, 0);
                }
                return $date->format("Y-m-d H:i:s");
            }

            return $date;
        }
        return $value;
    }
}
