<?php

class hypeValidatorTimeAmPm extends sfValidatorBase
{
 	protected function configure($options = array(), $messages = array())
 	{
 		$this->addMessage('bad_format', '"%value%" does not match the time format (%time_format%).');

 		$this->addOption('time_format', null);
 		$this->addOption('time_output', 'H:i:s');
 		$this->addOption('time_format_error');
 	}
 	protected function doClean($value)
 	{
 		if (is_array($value))
 		{
 			$clean = $this->convertTimeArrayToTimestamp($value);
 		}
 		else if ($regex = $this->getOption('time_format'))
 		{
 			if (!preg_match($regex, $value, $match))
 			{
 				throw new sfValidatorError($this, 'bad_format', array('value' => $value, 'time_format' => $this->getOption('time_format_error') ? $this->getOption('time_format_error') : $this->getOption('time_format')));
 			}

 			$clean = $this->convertTimeArrayToTimestamp($match);
 		}
 		else if (!ctype_digit($value))
 		{
 			$clean = strtotime($value);
 			if (false === $clean)
 			{
 				throw new sfValidatorError($this, 'invalid', array('value' => $value));
 			}
 		}
 		else
 		{
 			$clean = (integer) $value;
 		}

 		return $clean === $this->getEmptyValue() ? $clean : date($this->getOption('time_output'), $clean);
 	}
 	protected function convertTimeArrayToTimestamp($value)
 	{
 		// all elements must be empty or a number
 		foreach (array('hour', 'minute', 'second') as $key)
 		{
 			if (array_key_exists($key, $value) && !preg_match('#^\d+$#', $value[$key]) && !empty($value[$key]))
 			{
 				throw new sfValidatorError($this, 'invalid', array('value' => $value));
 			}
 		}

 		if ($value['ampm'] == 'pm' && $value['hour'] != 12) {
 			$value['hour'] += 12;
 		}

 		// if second is set, minute and hour must be set
 		// if minute is set, hour must be set
 		if (
 				$this->isValueSet($value, 'second') && (!$this->isValueSet($value, 'minute') || !$this->isValueSet($value, 'hour'))
 				||
 				$this->isValueSet($value, 'minute') && !$this->isValueSet($value, 'hour')
 		)
 		{
 			throw new sfValidatorError($this, 'invalid', array('value' => $value));
 		}

 		$clean = mktime(
 				array_key_exists('hour', $value) ? intval($value['hour']) : 0,
 				array_key_exists('minute', $value) ? intval($value['minute']) : 0,
 				array_key_exists('second', $value) ? intval($value['second']) : 0
 		);

 		if (false === $clean)
 		{
 			throw new sfValidatorError($this, 'invalid', array('value' => var_export($value, true)));
 		}

 		return $clean;
 	}
 	protected function isValueSet($values, $key)
 	{
 		return array_key_exists($key, $values) && !in_array($values[$key], array(null, ''), true);
 	}
 	protected function isEmpty($value)
 	{
 		if (is_array($value))
 		{
 			// array is not empty when a value is found
 			foreach($value as $key => $val)
 			{
 				// int and string '0' are 'empty' values that are explicitly accepted
 				if ($val === 0 || $val === '0' || !empty($val)) return false;
 			}
 			return true;
 		}

 		return parent::isEmpty($value);
 	}
}
