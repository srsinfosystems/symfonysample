<?php

class hypeValidatorVacationDrScheduleOverlaps extends sfValidatorSchema
{
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}
 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$errors = new sfValidatorErrorSchema($this, array(), array());

 		if (!$values['override_date_check'] && $values['start_date'] && $values['end_date'] && $values['doctor_id']) {
 			$doctor_schedule_blocks = Doctrine_Query::create()
 				->from('DoctorScheduleBlock dsb')
 				->addWhere('dsb.client_id = ?', $values['client_id'])
 				->addWhere('dsb.doctor_id = ?', $values['doctor_id'])
 				->whereIn('dsb.status', array(DoctorScheduleBlockTable::STATUS_PENDING, DoctorScheduleBlockTable::STATUS_POSTED))
 				->addWhere('dsb.start_date >= ?', date('Y-m-d 00:00', $values['start_date']))
 				->addWhere('dsb.start_date <= ?', date('Y-m-d 23:59', $values['end_date']))
 				->count();

 			if ($doctor_schedule_blocks) {
 				$e = new sfValidatorError($this, 'This doctor is scheduled to work ' . $doctor_schedule_blocks . ' time(s) during this period.', array());
 				$errors->addError($e, 'end_date');
 			}
 		}

 		if (count($errors)) {
 			throw $errors;
 		}
 		return $values;
 	}
}
