<?php

class hypeValidatorHolidayOverlaps extends sfValidatorSchema
{
 	public function __construct($include_consultations = false, $options = array(), $messages = array())
 	{
 		$this->addOption('include_consultations', $include_consultations);
 		parent::__construct(null, $options, $messages);
 	}
 	protected function doClean($values)
 	{
 		if (null === $values)
 		{
 			$values = array();
 		}

 		if (!is_array($values))
 		{
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$errors = new sfValidatorErrorSchema($this, array(), array());

 		// if there is a province set we need to find out all the clinics in that province and check the location_id against that.
 		$location_ids = array();
 		if ($values['province_code']) {
 			$locations = Doctrine_Query::create()
 				->from('Location l')
 				->addWhere('l.client_id = ?', $values['client_id'])
 				->addWhere('l.prov_code = ?', $values['province_code'])
 				->execute();

 			foreach ($locations as $location) {
 				$location_ids[] = $location->id;
 			}
 		}

 		if (!$values['override_date']) {
 			$q = Doctrine_Query::create()
 				->from('DoctorScheduleBlock dsb')
 				->addWhere('dsb.client_id = ?', $values['client_id'])
 				->addWhere('dsb.start_date between (?) and (?)', array(date('Y-m-d 00:00:00', $values['date']), date('Y-m-d 23:59:59', $values['date'])))
 				->whereIn('dsb.status', array(DoctorScheduleBlockTable::STATUS_PENDING, DoctorScheduleBlockTable::STATUS_POSTED));

 			if (count($location_ids)) {
 				$q->whereIn('dsb.location_id', $location_ids);
 			}

 			$dsbs = $q->count();

 			if ($dsbs) {
 				$e = new sfValidatorError($this, 'There are ' . $dsbs . ' doctors scheduled to work on this day.', array());
 				$errors->addError($e, 'date');
 			}

 			$q = Doctrine_Query::create()
 				->from('Appointment a')
 				->addWhere('a.client_id = ?', $values['client_id'])
 				->addWhere('a.start_date between (?) and (?)', array(date('Y-m-d 00:00:00', $values['date']), date('Y-m-d 23:59:59', $values['date'])))
 				->addWhere('a.slot_type = ?', AppointmentTable::SLOT_TYPE_CONSULT)
 				->addWhere('a.status = ?', AppointmentTable::STATUS_POSTED);

 			if (count($location_ids)) {
 				$q->whereIn('a.location_id', $location_ids);
 			}

 			$consults = $q->count();

 			if ($consults) {
 				$e = new sfValidatorError ($this, 'There are ' . $consults . ' consultations scheduled on this day.', array());
 				$errors->addError($e, 'date');
 			}

 		}
 		if ($this->getOption('include_consultations') && $values['consult_date'] && !$values['override_consult_date']) {
 			$q = Doctrine_Query::create()
 				->from('Appointment a')
 				->addWhere('a.client_id = ?', $values['client_id'])
 				->addWhere('a.start_date between (?) and (?)', array(date('Y-m-d 00:00:00', $values['consult_date']), date('Y-m-d 23:59:59', $values['consult_date'])))
 				->addWhere('a.slot_type = ?', AppointmentTable::SLOT_TYPE_CONSULT)
 				->addWhere('a.status = ?', AppointmentTable::STATUS_POSTED);

 			if (count($location_ids)) {
 				$q->whereIn('a.location_id', $location_ids);
 			}

 			$consults = $q->count();

 			if ($consults) {
 				$e = new sfValidatorError ($this, 'There are ' . $consults . ' consultations scheduled on this day.', array());
 				$errors->addError($e, 'consult_date');
 			}
 		}

 		if (count($errors)) {
 			throw $errors;
 		}

 		return $values;
 	}
}
