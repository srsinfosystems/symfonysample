<?php

class hypeValidatorLocationDoctorTemplateOverlap extends sfValidatorSchema
{
 	protected $doctor_locations = null;
 	protected $list = null;
 	protected $field_name = null;

 	public function __construct($doctor_locations, $list, $field_name, $options = array(), $messages = array())
 	{
 		$this->doctor_locations = $doctor_locations;
 		$this->list = $list;
 		$this->field_name = $field_name;

 		parent::__construct($options, $messages);
 	}
 	protected function doClean($values)
 	{
 		if (null === $values)
 		{
 			$values = array();
 		}

 		if (!is_array($values))
 		{
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		if (!is_array($values[$this->field_name])) {
 			return $values;
 		}

 		if (count($this->doctor_locations)) {
 			$errors = new sfValidatorErrorSchema($this, array(), array());
 			$ids = array();
 			$items_to_check = array();
 			$item_errors = array();
 			$id_column = $this->field_name == 'doctors' ? 'doctor_id' : 'location_id';

 			foreach ($this->doctor_locations as $doc_loc) {
 				if (array_key_exists($doc_loc->$id_column, $this->list)) {
 					$ids[] = $doc_loc->$id_column;
 				}
 			}

 			foreach ($ids as $id) {
 				if (!in_array($id, $values[$this->field_name])) {
 					$items_to_check[] = $id;
 				}
 			}

 			if (!count($items_to_check)) {
 				return $values;
 			}

 			$doc_templates = Doctrine_Query::create()
 				->from('DoctorGeneralSchedule dgs')
 				->whereIn('dgs.' . $id_column, $items_to_check)
 				->addWhere('dgs.end_date IS NULL OR dgs.end_date >= (?)', date(hype::DB_DATE_FORMAT))
 				->addWhere('dgs.recurrence_type != (?)', DoctorGeneralScheduleTable::RECURRENCE_DELETED)
 				->execute();

 			foreach ($doc_templates as $dt)
 			{
 				if (!array_key_exists($dt->$id_column, $item_errors)) {
 					if ($this->field_name == 'doctors') {
 						$e = new sfValidatorError($this, 'Doctor ' . $dt->Doctor->qc_doctor_code . ' cannot be removed from this list until their active doctor templates have finished.');
 					}
 					else {
 						$e = new sfValidatorError($this, $dt->Location->getShortCode() . ' cannot be removed from this list until its active doctor templates have finished.');
 					}
 					$item_errors[$dt->$id_column] = $e;
 					$errors->addError($e);
 				}
 			}
 		}

 		if (count($errors)) {
 			throw $errors;
 		}

 		return $values;
 	}
}
