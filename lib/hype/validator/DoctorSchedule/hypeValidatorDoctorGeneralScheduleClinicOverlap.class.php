<?php

class hypeValidatorDoctorGeneralScheduleClinicOverlap extends sfValidatorSchema
{
 	public function __construct($options = array(), $messages = array())
 	{
 		$this->addOption('allow_double_booking', $options['allow_double_booking']);

 		parent::__construct(null, $options, $messages);
 	}
 	function doClean($values)
 	{
 		$errors = new sfValidatorErrorSchema($this, array(), array());

 		if (!$values['override_location_check'] && $values['location_id']) {
 			$template_dates = $this->generate_form_dates($values, $values['start_date'], $values['end_date']);

 			// get all the doctor templates with the same weekday, location_id as the template we are checking
 			$doctor_templates = Doctrine_Query::create()
 				->from('DoctorGeneralSchedule dgs')
 				->leftJoin('dgs.Doctor d')
 				->addWhere('dgs.location_id = ?', $values['location_id'])
 				->addWhere('dgs.recurrence_type != ?', DoctorGeneralScheduleTable::RECURRENCE_DELETED)
 				->addWhere('DATE_FORMAT(dgs.start_date, "%w") = ?', (date('w', $values['start_date'])))
 				->addWhere('dgs.client_id = ?', $values['client_id']);

 			if ($values['id']) {
 				$doctor_templates->addWhere('dgs.id != ?', $values['id']);
 			}

 			$doctor_templates = $doctor_templates->execute();

 			// get all the location schedules for the given location
 			$clinic_schedules = Doctrine_Query::create()
 				->from('LocationSchedule ls')
 				->addWhere('ls.location_id = ?', $values['location_id'])
 				->execute();

 			// define a dates array where the key is the date of a generated doctor template event in the format YYYY-MM-DD
 			$dates = array();

 			// find all the dates for each doctor template
 			foreach ($doctor_templates as $key => $dt)
 			{
 				$generated_dates = $this->generate_doc_template_dates($dt, $values['start_date'], $values['end_date']);
 				foreach ($generated_dates as $generated_date) {
 					$dates[$generated_date][] = $key;
 				}
 			}

 			if (!$this->getOption('allow_double_booking')) {
 				foreach ($clinic_schedules as $cs)
 				{
 					$generated_dates = $this->generate_clinic_dates($cs, $values['start_date'], $values['end_date'], date('w', $values['start_date']));

 					foreach ($generated_dates as $generated_date)
 					{
 						if (!count($errors) && array_key_exists($generated_date, $dates) && count($dates[$generated_date]) + 1 > $cs->max_doctors) {
 							$overlap_keys = array();
 							foreach ($dates[$generated_date] as $key)
 							{
 								if (in_array($generated_date, $template_dates) && ($values['start_time'] == $doctor_templates[$key]['start_time'] || $values['end_time'] == $doctor_templates[$key]['end_time'] || ($values['start_time'] < $doctor_templates[$key]['end_time'] && $values['end_time'] > $doctor_templates[$key]['start_time']))) {
 									$overlap_keys[] = $doctor_templates[$key]['Doctor']->getDropdownString();
 								}
 							}

 							if (count($overlap_keys) + 1 > $cs->max_doctors) {
 								$message = implode(', ', $overlap_keys) . ' is concurrently scheduled into ' . $doctor_templates[$key]['Location']->getShortCode() . ' on ' . date('D, M d, Y', hype::parseDateToInt($generated_date, hype::DB_DATE_FORMAT)) .'.';

 								$e = new sfValidatorError($this, $message, array());
 								$errors->addError($e, 'location_id');
 							}
 						}
 					}
 				}
 			}

 		}

 		if (count($errors)) {
 			throw $errors;
 		}

 		return $values;
 	}
 	protected function generate_form_dates($vals, $start, $end)
 	{
 		$rs = array();

 		if (is_null($end)) {
 			$end = $vals['end_date'] ? $vals['end_date'] : strtotime('today 23:59:59 + 3 months');
 		}
 		$current_date = $vals['start_date'];

 		while ($current_date <= $end && $current_date != null)
 		{
 			if ($current_date >= $start) {
 				$rs[] = date(hype::DB_DATE_FORMAT, $current_date);
 			}

 			$current_date = DoctorGeneralScheduleTable::calculateNextDate($current_date, $vals['recurrence_type']);
 		}

 		return $rs;
 	}
 	protected function generate_doc_template_dates($doc_template, $start, $end)
 	{
 		$rs = array();

 		if (is_null($end)) {
 			$end = $doc_template['end_date'] ? hype::parseDateToInt($doc_template['end_date'], hype::DB_ISO_DATE) : strtotime('today 23:59:59 + 3 months');
 		}
 		$doc_template['end_date'] = $doc_template['end_date'] ? hype::parseDateToInt($doc_template['end_date'], hype::DB_ISO_DATE) : $end;
 		$current_date = hype::parseDateToInt($doc_template['start_date'], hype::DB_ISO_DATE);

 		while ($current_date <= $end && $current_date != null)
 		{
 			if ($current_date >= $start) {
 				$rs[] = date(hype::DB_DATE_FORMAT, $current_date);
 			}

 			$current_date = DoctorGeneralScheduleTable::calculateNextDate($current_date, $doc_template->recurrence_type);
 		}

 		return $rs;
 	}
 	protected function generate_clinic_dates($cs, $start, $end, $weekday)
 	{
 		$rs = array();

 		if (is_null($end)) {
 			$end = $cs['effective_end'] ? hype::parseDateToInt($cs['effective_end'], hype::DB_ISO_DATE) : strtotime('today 23:59:59 + 3 months');
 		}

 		$cs['effective_end'] = $cs['effective_end'] ? hype::parseDateToInt($cs['effective_end'], hype::DB_ISO_DATE) : $end;
 		$current_date = hype::parseDateToInt($cs['effective_start'], hype::DB_ISO_DATE);

 		while(date('w', $current_date) != $weekday )
 		{
 			$current_date = strtotime(date(hype::DB_DATE_FORMAT, $current_date) . ' + 1 day');
 		}

 		while ($current_date <= $end)
 		{
 			if ($current_date >= $start) {
 				$rs[] = date(hype::DB_DATE_FORMAT, $current_date);
 			}
 			$current_date = DoctorGeneralScheduleTable::calculateNextDate($current_date, DoctorGeneralScheduleTable::RECURRENCE_WEEKLY);
 		}

 		return $rs;
 	}

}

