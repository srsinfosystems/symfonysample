<?php

class hypeValidatorAppointmentTemplate extends sfValidatorSchema
{
 	public function __construct($location_title, $create_in_past = true, $complex_appointment_types = false, $title_format = '', $date_format = hype::DB_DATE_FORMAT, $options = array(), $messages = array())
 	{
 		$this->addOption('location_title', $location_title);
 		$this->addOption('create_in_past', $create_in_past);
 		$this->addOption('appointment_title_format', $title_format);
 		$this->addOption('complex_appointment_types', $complex_appointment_types);
 		$this->addOption('date_format', $date_format);

 		parent::__construct(null, $options, $messages);
 	}
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		$errors = new sfValidatorErrorSchema($this, array());

 		if ($this->getOption('create_in_past') == false && $values['start_date'] <= hype::parseDateToInt(date('d-m-Y 00:00:00'), hype::DB_ISO_DATE)) {
 			$e = new sfValidatorError($this, 'Cannot create repeating events in the past');
 			$errors->addError($e, 'start_date');
 		}

 		else if ($values['doctor_id'] && $values['doctor_id'] != 'n/a') {
 			$doc_loc = Doctrine_Query::create()
 				->from('DoctorLocation d')
 				->addWhere('d.doctor_id = ?', $values['doctor_id'])
 				->addWhere('d.location_id = ?', $values['location_id'])
 				->fetchOne();

 			if (!$doc_loc instanceof DoctorLocation) {
 				$e = new sfValidatorError($this, 'This doctor does not work at the selected ' . $this->getOption('location_title'));
 				$errors->addError($e, 'doctor_id');
 			}

 			if ($values['appointment_type_id']) {
 				$appointment_type = Doctrine_Query::create()
 					->from('AppointmentType at')
 					->addWhere('at.id = ?', $values['appointment_type_id'])
 					->fetchOne();

 				if ($appointment_type->slot_type != AppointmentTable::SLOT_TYPE_NORMAL) {
 					$e = new sfValidatorError($this, 'This appointment type cannot be used with a doctor.');
 					$errors->addError($e, 'appointment_type_id');
 				}
 			}

 			// check to see if current / future appointment overlaps with non-overlapable appointments
 			$at = new AppointmentTemplate();
 			$at->fromArray($values);
 			$at['repeat_sun'] = in_array('sun', $values['repeat_weekdays']);
 			$at['repeat_mon'] = in_array('mon', $values['repeat_weekdays']);
 			$at['repeat_tues'] = in_array('tues', $values['repeat_weekdays']);
 			$at['repeat_wed'] = in_array('wed', $values['repeat_weekdays']);
 			$at['repeat_thurs'] = in_array('thurs', $values['repeat_weekdays']);
 			$at['repeat_fri'] = in_array('fri', $values['repeat_weekdays']);
 			$at['repeat_sat'] = in_array('sat', $values['repeat_weekdays']);

 			$at['start_date'] = date(hype::DB_ISO_DATE, $values['start_date']);
 			$at['end_date'] = $values['end_date'] ? date(hype::DB_ISO_DATE, $values['end_date']) : null;
 			$at['status'] = AppointmentTemplateTable::STATUS_POSTED;

 			if ($values['doctor_id'] == 'n/a') {
 				$at['doctor_id'] = null;
 			}

 			$values['repeating_appointment_dates'] = $at->getAppointmentDates(false);
 			ksort($values['repeating_appointment_dates']);

 			$province = LocationTable::getProvinceForLocation($values['location_id']);

 			foreach ($values['repeating_appointment_dates'] as $date)
 			{
 				$r_start = $date . ' ' . $at->start_time;
 				$r_end = $date . ' ' . $at->end_time;

 				$appointment_type = Doctrine_Query::create()
 					->from('AppointmentType at')
 					->leftJoin('at.AppointmentTypeProvince atp')
 					->addWhere('at.id = ?', $values['appointment_type_id'])
 					->addWhere('atp.province_code = (?)', $province)
 					->fetchOne();

 				$appointments = Doctrine_Query::create()
 					->from('Appointment a')
 					->leftJoin('a.AppointmentType at')
 					->leftJoin('at.AppointmentTypeProvince atp')
 					->addWhere('atp.province_code = (?)', $province)
 					->addWhere('a.client_id = ?', $values['client_id'])
 					->addWhere('a.location_id = ?', $values['location_id'])
 					->addWhere('a.doctor_id = ?', $values['doctor_id'])
 					->addWhere('a.slot_type = ? OR a.slot_type = ?', array(AppointmentTable::SLOT_TYPE_NORMAL, AppointmentTable::SLOT_TYPE_OTHER))
 					->addWhere('a.status = ?', AppointmentTable::STATUS_POSTED)
 		 			->addWhere('a.start_date < (?)', $r_end)
 					->addWhere('a.end_date > (?)', $r_start)
 					->execute();

 				if (count($appointments) && $values['override_repeating_check']) {
 					if ($this->getOption('complex_appointment_types')) {
 						$can_overlap = $appointment_type->AppointmentTypeProvince[0]->allow_double_book;
 					}
 					else {
 						$can_overlap = $appointment_type->allow_double_book;
 					}

 					foreach ($appointments as $appt)
 					{
 						if ($this->getOption('complex_appointment_types')) {
 							$overlap_existing = $appt->AppointmentType->AppointmentTypeProvince[0]->allow_double_book;
 						}
 						else {
 							$overlap_existing = $appt->AppointmentType->allow_double_book;
 						}

 						if (!$can_overlap || !$overlap_existing) {
 							unset($values['repeating_appointment_dates'][date(hype::DB_DATE_FORMAT, hype::parseDateToInt($appt->start_date, hype::DB_ISO_DATE))]);
 						}
 					}
 				}
 				else if (count($appointments)) {
 					if ($this->getOption('complex_appointment_types')) {
 						$can_overlap = $appointment_type->AppointmentTypeProvince[0]->allow_double_book;
 					}
 					else {
 						$can_overlap = $appointment_type->allow_double_book;
 					}

 					$overlap_str = AppointmentTypeProvinceTable::OVERLAP_OK;
 					$no_overlap_str = AppointmentTypeProvinceTable::NO_OVERLAP;

 					foreach ($appointments as $appt)
 					{
 						if ($this->getOption('complex_appointment_types')) {
 							$overlap_existing = $appt->AppointmentType->AppointmentTypeProvince[0]->allow_double_book;
 						}
 						else {
 							$overlap_existing = $appt->AppointmentType->allow_double_book;
 						}

 						if ($can_overlap && $overlap_existing) {
 							$overlap_msg = $overlap_str;
 						}
 						else {
 							$overlap_msg = $no_overlap_str;
 						}

 						$e = new sfValidatorError($this, 'Overlapping Future Appointment '
 							. ' [' . date('M j Y, g:i a', hype::parseDateToInt($appt->start_date, hype::DB_ISO_DATE)) . '-' . date('g:i a', hype::parseDateToInt($appt->end_date, hype::DB_ISO_DATE)) . ']: '
 							. $appt->processAppointmentTitle($this->getOption('appointment_title_format'), array('date_format' => $this->getOption('date_format'))) . ($overlap_msg)
 						);
 						$errors->addError($e);

 					}
 				}
 			}
 		}

 		else if ($values['doctor_id'] && $values['doctor_id'] == 'n/a') {
 			if ($values['appointment_type_id']) {
 				$appointment_type = Doctrine_Query::create()
 					->from('AppointmentType at')
 					->addWhere('at.id = ?', $values['appointment_type_id'])
 					->fetchOne();

 				if ($appointment_type->slot_type != AppointmentTable::SLOT_TYPE_OTHER) {
 					$e = new sfValidatorError($this, 'This appointment type must be used with a doctor.');
 					$errors->addError($e, 'appointment_type_id');
 				}
 				else if ($appointment_type->slot_type == AppointmentTable::SLOT_TYPE_OTHER) {
 					if (!$values['end_date']) {
 						$e = new sfValidatorError($this, 'Must have an end date because of appointment type.');
 						$errors->addError($e, 'end_date');
 					}
 					else if ($values['end_date'] > strtotime(date(hype::DB_DATE_FORMAT, $values['start_date']) . ' + 6 months')) {
 						$e = new sfValidatorError($this, 'Repeating Appointments must end within 6 months of the start date.');
 						$errors->addError($e, 'end_date');
 					}
 				}
 			}
 		}

 		if (count($errors)) {
 			throw $errors;
 		}

 		return $values;
 	}
}
