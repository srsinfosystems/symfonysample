<?php

class hypeValidatorDoctorScheduleBlockStatus extends sfValidatorSchema
{
 	public function __construct($status_field, $cancelled, $deleted, $options = array(), $messages = array())
 	{
 		$this->addOption('status_field', $status_field);
 		$this->addOption('cancelled', $cancelled);
 		$this->addOption('deleted', $deleted);

 		$this->addMessage('invalid2', 'Invalid');
 		parent::__construct(null, $options, $messages);
 	}
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		$status = $values[$this->getOption('status_field')];
 		$cancelled = $this->getOption('cancelled');
 		$deleted = $this->getOption('deleted');

 		$errors = new sfValidatorErrorSchema($this, array());
 		// if status is deleted or cancelled require notes
 		if (!$values['id']) {
 			if ($status == $cancelled) {
 				$e = new sfValidatorError($this, 'You cannot cancel an event that has not yet been saved.', array());
 				$errors->addError($e, $this->getOption('status_field'));
 			}
 			else if ($status == $deleted) {
 				$e = new sfValidatorError($this, 'You cannot delete an event that has not yet been saved.', array());
 				$errors->addError($e, $this->getOption('status_field'));
 			}
 		}
 		else if (array_key_exists('notes', $values) && !$values['notes'] && $status == $cancelled) {
 			$e = new sfValidatorError($this, 'Notes are required for this event status.', array());
 			$errors->addError($e, 'notes');
 		}

 		if (count($errors)) {
 			throw $errors;
 		}

 		return $values;
 	}
}
