<?php

class hypeValidatorDoctorScheduleBlockOfficeHours extends sfValidatorSchema
{
 	public function __construct($location_field, $date_field, $options = array(), $messages = array())
 	{
 		$this->addOption('location_id', $location_field);
 		$this->addOption('date', $date_field);

 		$this->addMessage('invalid2', 'Invalid');
 		parent::__construct(null, $options, $messages);
 	}
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		if ($values['start_time'] == null && $values['end_time'] == null) {
 			return $values;
 		}

 		if (array_key_exists('recurrence_type', $values) && $values['recurrence_type'] == DoctorGeneralScheduleTable::RECURRENCE_DELETED) {
 			return $values;
 		}
 		else if (array_key_exists('status', $values) && $values['status'] == DoctorScheduleBlockTable::STATUS_DELETED) {
 			return $values;
 		}

 		$location_id = $this->getOption('location_id') ? $values[$this->getOption('location_id')] : '';
 		$date = $this->getOption('date') ? $values[$this->getOption('date')] : '';

 		if (is_int($date)) {
 			$date = date(hype::DB_DATE_FORMAT, $date);
 		}

 		$loc_sched = Doctrine_Query::create()
 			->from('LocationSchedule l')
 			->addWhere('l.location_id = ?', $location_id)
 			->addWhere('l.effective_start <= (?)', $date)
 			->addWhere('(l.effective_end >= (?) OR l.effective_end IS NULL)', $date)
 			->fetchOne();

 		if ($loc_sched instanceof LocationSchedule)
 		{
 			// figure out day of week
 			$dow = strtolower(date('D', hype::parseDateToInt($date, hype::DB_DATE_FORMAT)));
 			switch ($dow) {
 				case 'tue':
 					$dow = 'tues';
 					break;
 				case 'thu':
 					$dow = 'thurs';
 					break;
 			}

 			$dow_word = date('l', hype::parseDateToInt($date, hype::DB_DATE_FORMAT));

 			if ($loc_sched[$dow . '_open'] == null || $loc_sched[$dow . '_close'] == null) {
 				$e =  new sfValidatorError($this, 'invalid2', array(
 					'weekday' => $dow_word,
 				));
 				throw new sfValidatorErrorSchema($this, array('start_time' => $e));
 			}

 			$open_string = LocationScheduleTable::get12HourFormat($loc_sched[$dow . '_open']);
 			$end_string  = LocationScheduleTable::get12HourFormat($loc_sched[$dow . '_close']);

 			if ($loc_sched[$dow . '_open'] > $values['start_time']) {
 				$e = new sfValidatorError($this, 'invalid', array(
 					'start' => $open_string,
 					'finish' => $end_string,
 					'weekday' => $dow_word,
 				));
 				throw new sfValidatorErrorSchema($this, array('start_time' => $e));
 			}

 			if ($loc_sched[$dow . '_close'] < $values['end_time']) {
 				$e = new sfValidatorError($this, 'invalid', array(
 					'start' => $open_string,
 					'finish' => $end_string,
 					'weekday' => $dow_word,
 				));
 				throw new sfValidatorErrorSchema($this, array('end_time' => $e));
 			}
 		}

 		return $values;
 	}
}
