<?php

class hypeValidatorDoctorScheduleBlockVacation extends sfValidatorSchema
{
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}
		
 		if ($values['status'] == DoctorScheduleBlockTable::STATUS_DELETED || $values['status'] == DoctorScheduleBlockTable::STATUS_CANCELLED) {
 			return $values;
 		}
		
 		$vacations = Doctrine_Query::create()
 			->from('VacationDay v')
 			->addWhere('v.doctor_id = ?', $values['doctor_id'])
 			->addWhere('v.active = ?', true)
 			->addWhere('v.date = ?', $values['date'])
 			->fetchOne();

 		if ($vacations instanceof VacationDay) {
 			$errors = new sfValidatorErrorSchema($this, array(), array());
 			$e = new sfValidatorError($this, 'This doctor has a vacation scheduled for this day.');
 			$errors->addError($e, 'doctor_id');
			
 			throw $errors;
 		}

 		return $values;
 	}
}
