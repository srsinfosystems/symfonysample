<?php

class hypeValidatorDoctorScheduleBlockHoliday extends sfValidatorSchema
{
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		if ($values['status'] == DoctorScheduleBlockTable::STATUS_CANCELLED || $values['status'] == DoctorScheduleBlockTable::STATUS_DELETED) {
 			return $values;
 		}

 		if (array_key_exists('max_attendees', $values)) {
 			$holidays = Doctrine_Query::create()
 				->from('Holiday h')
 				->addWhere('h.client_id = ?', $values['client_id'])
 				->addWhere('h.active = ?', true)
 				->addWhere('h.date = (?) OR h.consult_date = (?)', array($values['date'], $values['date']))
 				->execute();
 		}
 		else {
 			$holidays = Doctrine_Query::create()
 				->from('Holiday h')
 				->addWhere('h.client_id = ?', $values['client_id'])
 				->addWhere('h.active = ?', true)
 				->addWhere('h.date = ?', $values['date'])
 				->execute();
 		}

 		foreach ($holidays as $holiday)
 		{
 			if ($holiday['province_code'] == null) {
 				$e = new sfValidatorError($this, 'Cannot schedule an appointment on a holiday.');
 				throw $e;
 			}
 			else {
 				$location = Doctrine::getTable('Location')->findOneById($values['location_id']);

 				if ($location instanceOf Location && $location['prov_code'] == $holiday['province_code']) {
 					$e = new sfValidatorError($this, 'Cannot schedule an appointment on a holiday.');
 					throw $e;
 				}

 			}
 		}

 		return $values;
 	}
}
