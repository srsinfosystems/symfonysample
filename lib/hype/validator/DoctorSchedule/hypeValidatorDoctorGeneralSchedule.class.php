<?php

class hypeValidatorDoctorGeneralSchedule extends sfValidatorSchema {

 	function doClean($values)
 	{
 		$errors = new sfValidatorErrorSchema($this, array(), array());

 		if ($values['recurrence_type'] == DoctorGeneralScheduleTable::RECURRENCE_DELETED) {
 			return $values;
 		}

 		if ($values['recurrence_type'] == DoctorGeneralScheduleTable::RECURRENCE_VACATION) {
 			if (!$values['start_date']) {
 				$e = new sfValidatorError($this, 'Required', array());
 				$errors->addError($e, 'start_date');
 			}

 			if (!$values['end_date']) {
 				$e = new sfValidatorError($this, 'Required', array());
 				$errors->addError($e, 'end_date');
 			}
 		}
 		else {
 			if (!$values['start_date']) {
 				$e = new sfValidatorError($this, 'Required', array());
 				$errors->addError($e, 'start_date');
 			}

 			if (!$values['start_time']) {
 				$e = new sfValidatorError($this, 'Required', array());
 				$errors->addError($e, 'start_time');
 			}

 			if (!$values['end_time']) {
 				$e = new sfValidatorError($this, 'Required', array());
 				$errors->addError($e, 'end_time');
 			}

 			if (!$values['location_id']) {
 				$e = new sfValidatorError($this, 'Required', array());
 				$errors->addError($e, 'location_id');
 			}

 			if (array_key_exists('max_attendees', $values) && !$values['max_attendees']) {
 				$e = new sfValidatorError($this, 'Required', array());
 				$errors->addError($e, 'max_attendees');
 			}
 		}

 		if ($values['start_date'] && $values['end_date'] && $values['start_date'] > $values['end_date']) {
 			$e = new sfValidatorError($this, 'Start Date must be before End Date', array());
 			$errors->addError($e, 'start_date');
 		}

 		if (count($errors)) {
 			throw $errors;
 		}
 		return $values;
 	}
}

