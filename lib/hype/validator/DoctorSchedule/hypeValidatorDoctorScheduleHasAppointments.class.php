<?php

class hypeValidatorDoctorScheduleHasAppointments extends sfValidatorSchema
{
 	public function __construct($original_object, $location_title, $options = array(), $messages = array())
 	{
 		$this->addOption('original_object', $original_object);
 		$this->addOption('location_title', $location_title);

 		parent::__construct(null, $options, $messages);
 	}
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!$values['status'] || !$values['start_time'] || !$values['end_time'] || !$values['location_id'] || !$values['doctor_id']) {
 			return $values;
 		}

 		$object = $this->getOption('original_object');
 		$start_date = $values['date'] . ' ' . $values['start_time'];
 		$end_date = $values['date'] . ' ' . $values['end_time'];

 		if (!$object->id) {
 			return $values;
 		}

 		$errors = new sfValidatorErrorSchema($this, array());

 		$field = null;

 		$status = $values['status'] != $object->status && $values['status'] != DoctorScheduleBlockTable::STATUS_POSTED;
 		$field = ($status && !$field) ? 'status' : null;

 		$location = $values['location_id'] != $object->location_id;
 		$field = ($location && !$field) ? 'location_id' : null;

 		$doctor = $values['doctor_id'] != $object->doctor_id;
 		$field = ($doctor && !$field) ? 'doctor_id' : null;

 		$start_time = $start_date != substr($object->start_date, 0, 16);
 		$field = ($start_time && !$field) ? 'start_time' : null;

 		$end_time = $end_date != substr($object->end_date, 0, 16);
 		$field = ($end_time && !$field) ? 'end_time' : null;

 		$affected_appointments = null;
 		$moveable_appointments = null;

 		if ($status || $location || $doctor || $start_time || $end_time) {
 			$affected_appointments = Doctrine_Query::create()
 				->from('Appointment a')
 				->addWhere('a.client_id = ?', $values['client_id'])
 				->addWhere('a.location_id = ?', $object->location_id)
 				->addWhere('a.doctor_id = ?', $object->doctor_id)
 				->addWhere('a.status = ?', AppointmentTable::STATUS_POSTED)
 				->addWhere('a.orphan = ?', false)
 				->addWhere('a.start_date BETWEEN (?) AND (?)', array($object->start_date, $object->end_date))
 				->addWhere('a.appointment_template_id IS NULL');
 		}

 		if (($doctor || $start_time || $end_time) && !$status && !$location) {
 			$moveable_appointments = Doctrine_Query::create()
 				->from('Appointment a')
 				->addWhere('a.client_id = ?', $values['client_id'])
 				->addWhere('a.location_id = ?', $object->location_id)
 				->addWhere('a.doctor_id = ?', $object->doctor_id)
 				->addWhere('a.status = ?', AppointmentTable::STATUS_POSTED)
 				->addWhere('a.orphan = ?', false)
 				->addWhere('a.start_date BETWEEN (?) AND (?)', array($start_date, $end_date))
 				->addWhere('a.appointment_template_id IS NULL');
 		}

 		if ($values['override_appointment_overlap_check']) {
 			$values['affected_appointments'] = array();
 			$values['moveable_appointments'] = array();
 			if ($moveable_appointments != null) {
 				$moveable_appointments = $moveable_appointments->execute();
 				foreach ($moveable_appointments as $appt) {
 					$values['moveable_appointments'][] = $appt['id'];
 				}
 			}
 			if ($affected_appointments != null) {
 				$affected_appointments = $affected_appointments->execute();
 				foreach ($affected_appointments as $appt) {
 					if (!in_array($appt['id'], $values['moveable_appointments'])) {
 						$values['affected_appointments'][] = $appt['id'];
 					}
 				}
 			}
 			$values['update_appointments'] = true;
 			return $values;
 		}

 		if (!is_null($affected_appointments)) {
 			$affected_appointments = $affected_appointments->count();
 		}

 		if (!is_null($moveable_appointments)) {
 			$moveable_appointments = $moveable_appointments->count();
 		}

 		if ($moveable_appointments) {
 			$e = new sfValidatorError($this, $affected_appointments . ' appointments will be affected by your changes.  ' . $moveable_appointments . ' of these appointments can be moved, ' . ($affected_appointments - $moveable_appointments) . ' will need to be rescheduled.');
 			$errors->addError($e, $field);
 		}
 		else if ($affected_appointments) {
 			$e = new sfValidatorError($this, $affected_appointments . ' appointments will be affected by your changes and need to be rescheduled.');
 			$errors->addError($e, $field);
 		}

 		if (count($errors)) {
 			throw $errors;
 		}

 		return $values;
 	}
}
