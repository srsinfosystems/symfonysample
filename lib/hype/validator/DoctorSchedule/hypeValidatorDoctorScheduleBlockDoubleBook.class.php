<?php

class hypeValidatorDoctorScheduleBlockDoubleBook extends sfValidatorSchema
{
 	public function __construct($doctor_id, $location_id, $date, $start_time, $end_time, $status, $options = array(), $messages = array())
 	{
 		$this->addOption('doctor_id', $doctor_id);
 		$this->addOption('location_id', $location_id);
 		$this->addOption('date', $date);
 		$this->addOption('start', $start_time);
 		$this->addOption('end', $end_time);
 		$this->addOption('status', $status);
 		$this->addOption('allow_simultaneous_scheduling', $options['allow_simultaneous_scheduling']);

 		parent::__construct(null, $options, $messages);
 	}
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$doctor_id   = array_key_exists($this->getOption('doctor_id'), $values) ? $values[$this->getOption('doctor_id')] : null;
 		$location_id = array_key_exists($this->getOption('location_id'), $values) ? $values[$this->getOption('location_id')] : null;
 		$date        = array_key_exists($this->getOption('date'), $values) ? $values[$this->getOption('date')] : null;
 		$start       = array_key_exists($this->getOption('start'), $values) ? $values[$this->getOption('start')] : null;
 		$end         = array_key_exists($this->getOption('end'), $values) ? $values[$this->getOption('end')] : null;
 		$status      = array_key_exists($this->getOption('status'), $values) ? $values[$this->getOption('status')] : null;

 		if ($status == DoctorScheduleBlockTable::STATUS_DELETED) {
 			return $values;
 		}

 		$start_date = $date . ' ' . $start;
 		$end_date = $date . ' ' . $end;

 		$q = Doctrine_Query::create()
 			->from('DoctorScheduleBlock dsb')
 			->addWhere('dsb.doctor_id = ?', $doctor_id)
 			->addWhere('dsb.start_date < (?)', $end_date)
 			->addWhere('dsb.end_date > (?)', $start_date)
 			->addWhere('dsb.status != (?)', DoctorScheduleBlockTable::STATUS_CANCELLED)
 			->addWhere('dsb.status != (?)', DoctorScheduleBlockTable::STATUS_DELETED)
 			->addWhere('dsb.status != (?)', DoctorScheduleBlockTable::STATUS_VACATION);

 		if ($values['id']) {
 			$q->addWhere('dsb.id != (?)', $values['id']);
 		}
 		$dsb = $q->fetchOne();

 		if ($dsb instanceOf DoctorScheduleBlock) {
 			$e = new sfValidatorError($this, 'invalid', array(
 				'start' => date('g:i a', hype::parseDateToInt($dsb->start_date, hype::DB_ISO_DATE)),
 				'end' => date('g:i a', hype::parseDateToInt($dsb->end_date, hype::DB_ISO_DATE))
 			));

 			$es = new sfValidatorErrorSchema($this, array($this->getOption('start') => $e));
 			throw $es;
 		}

 		if (!$this->getOption('allow_simultaneous_scheduling')) {
 			if ($location_id && $doctor_id && !$values['override_location_check']) {
 				$q = Doctrine_Query::create()
 				->from('DoctorScheduleBlock dsb')
 				->leftJoin('dsb.Doctor d')
 				->leftJoin('dsb.Location l')
 				->addWhere('dsb.location_id = ?', $location_id)
 				->addWhere('dsb.start_date < (?)', $end_date)
 				->addWhere('dsb.end_date > (?)', $start_date)
 				->addWhere('dsb.status != (?)', DoctorScheduleBlockTable::STATUS_CONFLICT)
 				->addWhere('dsb.status != (?)', DoctorScheduleBlockTable::STATUS_CANCELLED)
 				->addWhere('dsb.status != (?)', DoctorScheduleBlockTable::STATUS_DELETED);

 				if ($values['id']) {
 					$q->addWhere('dsb.id != (?)', $values['id']);
 				}
 				$dsb = $q->fetchOne();

 				if ($dsb) {
 					$e = new sfValidatorError ($this, $dsb->Doctor->getDropdownString() . ' is  concurrently scheduled into ' . $dsb->Location->getShortCode() . '.');
 					$es = new sfValidatorErrorSchema($this, array($this->getOption('location_id') => $e));
 					throw $es;
 				}
 			}
 		}

 		return $values;
 	}
}
