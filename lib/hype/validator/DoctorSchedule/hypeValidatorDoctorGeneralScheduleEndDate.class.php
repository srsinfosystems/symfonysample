<?php

class hypeValidatorDoctorGeneralScheduleEndDate extends  sfValidatorSchema 
{
 	function doClean($values)
 	{
 		$errors = new sfValidatorErrorSchema($this, array(), array());

 		$values['original_end_date'] = $values['original_end_date'] ? hype::parseDateToInt($values['original_end_date'], hype::DB_ISO_DATE) : '';

 		if ($values['original_end_date'] != $values['end_date'] && $values['id'] && !$values['override_end_date_check']) {
 			$doctor_schedule_blocks = Doctrine_Query::create()
 				->from('DoctorScheduleBlock dsb')
 				->addWhere('dsb.doctor_general_schedule_id = ?', $values['id'])
 				->addWhere('dsb.start_date >= (?)', date('Y-m-d 00:00:00', $values['end_date']))
 				->addWhere('dsb.status != (?)', DoctorScheduleBlockTable::STATUS_CANCELLED)
 				->addWhere('dsb.status != (?)', DoctorScheduleBlockTable::STATUS_DELETED)
 				->addWhere('dsb.status != (?)', DoctorScheduleBlockTable::STATUS_VACATION)
 				->addWhere('dsb.created_at = dsb.updated_at')
 				->execute();

 			if ($doctor_schedule_blocks) {
 				$e = new sfValidatorError($this, 'There are doctor schedule blocks already generated from this template after the new end date');
 				$errors->addError($e, 'end_date');
 			}
 		}

 		if (count($errors)) {
 			throw $errors;
 		}
 		return $values;
 	}
}

