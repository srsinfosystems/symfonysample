<?php

class hypeValidatorDoctorScheduleBlockDocLoc extends sfValidatorSchema
{
 	public function __construct($doctor_id, $location_id, $options = array(), $messages = array())
 	{
 		$this->addOption('doctor_id', $doctor_id);
 		$this->addOption('location_id', $location_id);
 		$this->addOption('throw_global_error', false);

 		parent::__construct(null, $options, $messages);
 	}
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		if (array_key_exists('status', $values) && ($values['status'] == DoctorScheduleBlockTable::STATUS_DELETED || $values['status'] == DoctorScheduleBlockTable::STATUS_CANCELLED)) {
 			return $values;
 		}

 		$doctor_id  = array_key_exists($this->getOption('doctor_id'), $values) ? $values[$this->getOption('doctor_id')] : null;
 		$location_id = array_key_exists($this->getOption('location_id'), $values) ? $values[$this->getOption('location_id')] : null;

 		$doc_loc = Doctrine_Query::create()
 			->from('DoctorLocation d')
 			->addWhere('d.doctor_id = ?', $doctor_id)
 			->addWhere('d.location_id = ?', $location_id)
 			->fetchOne();

 		if (!$doc_loc instanceof DoctorLocation) {
 			$e = new sfValidatorError($this, 'invalid');

 			if ($this->getOption('throw_global_error')) {
 				throw $e;
 			}

 			throw new sfValidatorErrorSchema($this, array($this->getOption('doctor_id') => $e));
 		}
 		return $values;
 	}
}
