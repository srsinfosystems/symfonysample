<?php

class hypeValidatorDoctorGeneralScheduleOverlappingRows extends sfValidatorSchema
{
 	protected function _weekdays($r1, $r2)
 	{
 		return $r1['weekday'] === $r2['weekday'];
 	}
 	protected function _times($r1, $r2)
 	{
 		if ($r1['start_time'] < $r2['end_time'] && $r1['end_time'] > $r2['start_time']) {
 			return true;
 		}
 		return false;
 	}
 	protected function _dates ($r1, $r2)
 	{
 		if (!$r1['end_date']) {
 			$r1['end_date'] = strtotime('today + 25 years');
 		}
 		if (!$r2['end_date']) {
 			$r2['end_date'] = strtotime('today + 25 years');
 		}

 		if ($r1['start_date'] < $r2['end_date'] && $r1['end_date'] > $r2['start_date']) {
 			return true;
 		}

 		return false;
 	}
 	protected function _recurrence($r1, $r2)
 	{
 		if ($r1['recurrence_type'] == DoctorGeneralScheduleTable::RECURRENCE_NONE && $r2['recurrence_type'] == DoctorGeneralScheduleTable::RECURRENCE_NONE) {
 			return $r1['start_date'] == $r2['start_date'];
 		}

 		if ($r1['recurrence_type'] == DoctorGeneralScheduleTable::RECURRENCE_NONE) {
 			return $this->_recurrence($r2, $r1);
 		}

 		if ($r2['start_date'] < $r1['start_date']) {
 			return $this->_recurrence($r2, $r1);
 		}

 		while ($r1['start_date'] < $r2['start_date'])
 		{
 			$r1['start_date'] = DoctorGeneralScheduleTable::calculateNextDate($r1['start_date'], $r1['recurrence_type']);
 		}

 		return $r1['start_date'] == $r2['start_date'];
 	}
 	function doClean($values)
 	{
 		$errors = new sfValidatorErrorSchema($this, array(), array());

 		$maxindex = null;
 		foreach ($values as $key => $row)
 		{
 			if (!is_array($row)) {
 				continue;
 			}
 			if ($row['recurrence_type'] == DoctorGeneralScheduleTable::RECURRENCE_DELETED) {
 				continue;
 			}

 			if ($row['start_date']) {
 				$values[$key]['weekday'] = date('w', $row['start_date']);
 			}
 			$maxindex = $key;
 		}

 		foreach ($values as $key1 => $row)
 		{
 			if (!is_array($row)) {
 				continue;
 			}
 			if ($row['recurrence_type'] == DoctorGeneralScheduleTable::RECURRENCE_DELETED) {
 				continue;
 			}

 			$e = null;
 			for ($key2 = $key1 + 1; $key2 <= $maxindex; $key2++) {
 				if (!array_key_exists($key2, $values)) {
 					continue;
 				}
 				if ($values[$key2]['recurrence_type'] == DoctorGeneralScheduleTable::RECURRENCE_DELETED) {
 					continue;
 				}

 				if (array_key_exists($key2, $values)) {
 					if ($this->_weekdays($row, $values[$key2]) && $this->_times($row, $values[$key2]) && $this->_dates($row, $values[$key2]) && $this->_recurrence($row, $values[$key2]) ) {
 						$e = new sfValidatorError($this, 'Dates overlap between rows ' . ($key1 + 1) . ' and ' . ($key2 + 1));
 						$errors->addError($e);
 					}
 				}
 			}
 		}

 		if (count($errors)) {
 			throw $errors;
 		}
 		return $values;
 	}
}

