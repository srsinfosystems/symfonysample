<?php

class hypeValidatorConsultationDoubleBook extends sfValidatorSchema
{
 	public function __construct($location_id, $date, $start_time, $end_time, $options = array(), $messages = array())
 	{
 		$this->addOption('location_id', $location_id);
 		$this->addOption('date', $date);
 		$this->addOption('start', $start_time);
 		$this->addOption('end', $end_time);

 		parent::__construct(null, $options, $messages);
 	}
 	protected function doClean($values)
 	{
 		if (null === $values) {
 			$values = array();
 		}

 		if (!is_array($values)) {
 			throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
 		}

 		$location_id = array_key_exists($this->getOption('location_id'), $values) ? $values[$this->getOption('location_id')] : null;
 		$date        = array_key_exists($this->getOption('date'), $values) ? $values[$this->getOption('date')] : null;
 		$start       = array_key_exists($this->getOption('start'), $values) ? $values[$this->getOption('start')] : null;
 		$end         = array_key_exists($this->getOption('end'), $values) ? $values[$this->getOption('end')] : null;

 		$start_date = $date . ' ' . $start;
 		$end_date = $date . ' ' . $end;

 		$q = Doctrine_Query::create()
 			->from('Appointment a')
 			->leftJoin('a.Location l')
 			->addWhere('a.location_id = ?', $location_id)
 			->addWhere('a.start_date < (?)', $end_date)
 			->addWhere('a.end_date > (?)', $start_date)
 			->addWhere('a.status != (?)', AppointmentTable::STATUS_CANCELLED)
 			->addWhere('a.status != (?)', AppointmentTable::STATUS_DELETED)
 			->addWhere('a.slot_type = (?)', AppointmentTable::SLOT_TYPE_CONSULT);

 		if ($values['id']) {
 			$q->addWhere('a.id != (?)', $values['id']);
 		}
 		$appt = $q->fetchOne();

 		if ($appt) {
 			$e = new sfValidatorError($this, 'invalid', array(
 				'start' => date('g:i a', hype::parseDateToInt($appt->start_date, hype::DB_ISO_DATE)),
 				'end' => date('g:i a', hype::parseDateToInt($appt->end_date, hype::DB_ISO_DATE))
 			));

 			$es = new sfValidatorErrorSchema($this, array($this->getOption('start') => $e));
 			throw $es;
 		}

 		return $values;
 	}
}
