<?php

class hypeValidatorDateRange extends sfValidatorSchema
{
 	public function __construct($start_field, $end_field, $format, $options = array(), $messages = array())
 	{
 		$this->addOption('start', $start_field);
 		$this->addOption('end', $end_field);
 		$this->addOption('format', $format);

 	    parent::__construct(null, $options, $messages);
 	}
 	function doClean($values)
 	{
 		$start = $values[$this->getOption('start')];
 		$end = $values[$this->getOption('end')];
 		$format = $this->getOption('format');

 		if ($start) {
 			$values[$this->getOption('start')] = $this->convertDate($start, $format, array('d' => '01', 'm' => '01', 'h' => '00', 'i' => '00', 's' => '00', 'Y' => date('Y')));
 		}
 		if ($end) {
 			$values[$this->getOption('end')] = $this->convertDate($end, $format, array('d' => '31', 'm' => '12', 'h' => '23', 'i' => '59', 's' => '59', 'Y' => date('Y')));
 		}

 		if ($values[$this->getOption('start')] > $values[$this->getOption('end')] && $values[$this->getOption('start')] && $values[$this->getOption('end')]) {
 			$error = new sfValidatorError($this, 'Start Date must be earlier than End Date');

 			throw new sfValidatorErrorSchema($this, array($this->getOption('start') => $error));
 		}

 		return $values;
 	}
 	protected function convertDate($date, $format, $defaults)
 	{
 		if (is_int($date)) {
 			$date = date($format, $date);
 		}

 		$dateTime = date_parse_from_format($format, $date);

 		foreach ($dateTime as $key => $value)
 		{
 			if ($value) {
 				if ($key == 'minute') {
 					$defaults['i'] = $value;
 				}
 				if ($key == 'year') {
 					$defaults['Y'] = $value;
 				}
 				if ($key == 'month') {
 					$defaults['m'] = $value;
 				}
 				if ($key == 'day') {
 					$defaults['d'] = $value;
 				}
 				if ($key == 'hour') {
 					$defaults['h'] = $value;
 				}
 				if ($key == 'second') {
 					$defaults['s'] = $value;
 				}
 			}
 		}

 		return mktime(intval($defaults['h']), intval($defaults['i']), intval($defaults['s']), intval($defaults['m']), intval($defaults['d']), intval($defaults['Y']));
 	}
}
