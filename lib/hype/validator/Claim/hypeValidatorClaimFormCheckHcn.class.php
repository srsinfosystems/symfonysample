<?php

class hypeValidatorClaimFormCheckHcn extends sfValidatorSchema
{
 	function doClean($values)
 	{
 		if (array_key_exists('hcn_num', $values)) {
 			if ($values['hcn_num']) {
 				$hcn = $values['hcn_num'];
 				$patient_id = $values['id'];
 				$client_id = $values['client_id'];

 				$patient = Doctrine_Query::create()
 					->from('Patient p')
 					->addWhere('p.client_id = ?', $client_id)
 					->addWhere('p.hcn_num = ? OR p.hcn_num = ?', array($hcn, hype::encryptHCN($hcn)))
 					->addWhere('p.deleted = (?)', false);

 				if ($patient_id) {
 					$patient->addWhere('p.id != ?', $patient_id);
 				}

 				$patient = $patient->fetchOne();

 				if ($patient instanceof Patient) {
 					$errors = new sfValidatorErrorSchema($this, array(), array());
 					$e = new sfValidatorError($this, 'We already have a patient with this health card number in our system.');
 					$errors->addError($e, 'hcn_num');
 					throw $errors;
 				}
 			}
 			else if ($values['hcn_version_code'] || $values['hcn_exp_year']) {
 				$errors = new sfValidatorErrorSchema($this, array(), array());
 				if ($values['hcn_version_code']) {
 					$e = new sfValidatorError($this, 'Cannot enter a version code without a Health Card number.');
 					$errors->addError($e, 'hcn_version_code');
 				}
 				if ($values['hcn_exp_year']) {
 					$e = new sfValidatorError($this, 'Cannot enter an expiry date without a Health Card number.');
 					$errors->addError($e, 'hcn_exp_year');
 				}
 				throw $errors;
 			}
 		}

 		return $values;
 	}

}