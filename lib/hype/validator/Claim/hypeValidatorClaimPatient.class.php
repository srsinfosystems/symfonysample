<?php

class hypeValidatorClaimPatient extends sfValidatorSchema
{
	public function __construct($params, $options = array(), $messages = array())
	{
		$this->addOption('require_existing_record', $params['require_existing_patient']);

	    parent::__construct(null, $options, $messages);
	}
	public function doClean($values)
	{
		$errors = new sfValidatorErrorSchema($this, array(), array());

		$require_patient_record = $this->getOption('require_existing_record');
		$edit_before_save = $this->getOption('edit_patients_presave');

		if ($values['patient_id']) {
			$patient = Doctrine_Query::create()
				->from('Patient p')
				->addWhere('p.id = ?', $values['patient_id'])
				->addWhere('p.client_id = ?', $values['client_id'])
				->addWhere('p.deleted = (?)', false)
				->fetchOne();

			if (!$patient instanceof Patient) {
				$error = new sfValidatorError($this, 'Invalid patient record.');
				$errors->addError($error, 'patient_id');
			}
		}

		if ($require_patient_record) {
			if (!$values['patient_id']) {
				$error = new sfValidatorError($this, 'Claims require an existing patient record.');
				$errors->addError($error, 'patient_id');
			}
		}

		if (count($errors)) {
			throw $errors;
		}
		return $values;
	}
}