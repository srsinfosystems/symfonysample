<?php
class hypePrint
{
	public static function formatPara ($s) {

		$s = '<p>' . $s . '</p>';
		return $s;
	}
	public static function formatTable ($s, $width, $border) {

		$s = '<table border="' . $border . '" width="' . $width . '%" cellpadding="0">' . $s . '</table>';
		return $s;

	}
	public static function formatP ($s, $align) {
		$s = '<p style="text-align:' . $align . ';">' . $s . '</p>';
		return $s;
	}
	public static function formatRow ($s) {

		$s = '<tr>' . $s . '</tr>';
		return $s;
	}
	public static function formatD ($s) {

		$s = '<td>' . $s . '</td>';
		return $s;
	}
	public static function formatDW ($s, $w = '100') {

		$s = '<td width="'.$w.'%">' . $s . '</td>';
		return $s;
	}
	public static function formatFont ($s, $size) {
		$s = '<font size="' . $size . '">' . $s . '</font>';
		return $s;
	}
	public static function formatH1 ($s, $j) {
		$s = '<h1 style="text-align:' . $j . '">' . $s . '</h1>';
		return $s;
	}
	public static function formatH2 ($s, $j) {
		$s = '<h2 style="text-align:' . $j . '">' . $s . '</h2>';
		return $s;
	}
	public static function formatH3 ($s, $j) {
		$s = '<h3 style="text-align:' . $j . '">' . $s . '</h3>';
		return $s;
	}
	public static function formatH4 ($s, $j) {
		$s = '<h4 style="text-align:' . $j . '">' . $s . '</h4>';
		return $s;
	}
	public static function formatH5 ($s, $j) {
		$s = '<h5 style="text-align:' . $j . '">' . $s . '</h5>';
		return $s;
	}
	public static function formatH6 ($s, $j) {
		$s = '<h6 style="text-align:' . $j . '">' . $s . '</h6>';
		return $s;
	}
}

