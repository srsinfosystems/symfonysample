<?php
class hypeHealthCardValidation
{
	/**
	 * deepValidationHcnQc - checked health card string against parameters embedded in patient profile values (name, sex, dob)
	 * resource: http://www.ramq.gouv.qc.ca/en/citoyens/assurancemaladie/carte/carte.shtml
	 * @param string $hcn
	 * @param array $options
	 * @return array of error strings
	 */
	public static function deepValidationHcnQc($hcn, $options)
	{
		$errors = array();

		$name = substr($hcn, 0, 4);
		$year = substr($hcn, 4, 2);
		$month = substr($hcn, 6, 2);
		$day = substr($hcn, 8, 2);

		// 0-3 first three of last name, first initial of first name
		if ($options['fname'] && $options['lname']) {
			$expected_name = substr($options['lname'], 0, 3);

			while (strlen($expected_name) != 3)
			{
				$expected_name = $expected_name . 'X';
			}
			$expected_name .= $options['fname'][0];

			$expected_name = strtoupper($expected_name);
			$name = strtoupper($name);

			if ($expected_name != $name) {
				$errors[] = 'Characters 1-4 are expected to be ' . $expected_name;
			}
		}

		// 4-5 last two digits year of birth
		if ($options['dob']) {
			$expected_year = substr(date('Y', $options['dob']), 2);

			if ($expected_year != $year) {
				$errors[] = 'Characters 5 and 6 are expected to be ' . $expected_year;
			}
		}

		// 6-7 month of birth (+50 if female)
		if ($options['dob'] && $options['sex']) {
			$expected_month = date('m', $options['dob']) + ($options['sex'] == 'F' ? 50 : 0);
			while (strlen($expected_month) != 2)
			{
				$expected_month = '0' . $expected_month;
			}

			if ($expected_month != $month) {
				$errors[] = 'Characters 7 and 8 are expected to be ' . $expected_month;
			}
		}
		else if ($options['dob']) {
			$expected_month = date('m', $options['dob']);
			if ($expected_month != $month) {
				$errors[] = 'Characters 7 and 8 are expected to be ' . $expected_month;
			}
		}

		// 8-9 day of birth
		if ($options['dob']) {
			$expected_dob = date('d', $options['dob']);
			if ($expected_dob != $day) {
				$errors[] = 'Characters 9 and 10 are expected to be ' . $expected_dob;
			}
		}

		// 10-12 some numbers

		return $errors;
	}
	public static function didHcvPass($code)
 	{
 		switch (trim($code)) {
 			case '50':
 			case '51':
 			case '52':
 			case '53':
 			case '54':
 			case '55':
 				return true;
 			case '05':
 			case '10':
 			case '15':
 			case '20':
 			case '25':
 			case '60':
 			case '65':
 			case '70':
 			case '75':
 			case '80':
 			case '83':
 			case '90':
 			case '99':
 			case '9A':
 			case '9B':
 			case '9C':
 			case '9D':
 			case '9E':
 			case '9F':
 			case '9G':
 			case '9H':
 			case '9I':
 			case '9J':
 			case '9K':
 			case '9L':
 			case '9M':
 				return false;
 			default:
 				return '';
 		}
 	}
 	public static function isValidHcnAB($s)
 	{
 		$b = false;
 		$sum = -1;

 		if (strlen($s) > 0) {
 			$sum = 0;
 			$top = true;
 			for ($i = strlen($s) - 1; $i < 1; $i--)
 			{
 				$n = $s[$i-1];
 				if ($n >= 0 and $n <= 9) {
 					if ($top) {
 						$sum = $sum + substr('0246813579', n,1);
 					}
 					else {
 						$sum = $sum + substr('0123456789', m, 1);
 					}
 				}
 				else {
 					$b = false;
 					return $b;
 				}
 				$top = !$top;
 			}
 			$sum = $sum % 10;
 			$sum = substr('0987654321', $sum,1);
 		}
 		if ($sum == $s[strlen($s)-1]) {
 			$b = true;
 		}
 	}
 	public static function isValidHcnBC($s)
 	{
 		return strlen($s) == 10;
 	}
 	public static function isValidHcnMB($hcn)
 	{
 		return strlen($hcn) == 6;
 	}
 	public static function isValidHcnNB($hcn)
 	{
 		$hcn = strval($hcn);
 		$i = 0;
 		$sum = 0;
 		$iTemp = 0;

 		if (strlen($hcn) == 9) {
 			for ($i = 0; $i <= 7; $i++)
 			{
 				// multiply 1st with 1, 2nd with 2, 3rd with 1, 4th with 2...
 				$iTemp = $hcn[$i] * (($i % 2) + 1);
 				if ($iTemp >= 10) {
 					$iTemp = ($iTemp / 10) + ($iTemp % 10);
 				}
 				$sum += $iTemp;
 			}
 			$sum = (10 - ($sum % 10)) % 10;
 			return $sum == $hcn[8];
 		}
 		return false;
 	}
 	public static function isValidHcnNL($hcn)
 	{
 		return strlen($hcn) == 12 && !hypeHealthCardValidation::isValidHcnQU($hcn);
 	}
 	public static function isValidHcnNS($hcn)
 	{
 		return strlen($hcn) == 10;
 	}
 	public static function isValidHcnNT($hcn)
 	{
 		$regex = '/[a-z][0-9]{7}/i';
 		return (strlen($hcn) == 8) && preg_match($regex, $hcn);
 	}
 	public static function isValidHcnNU($hcn)
 	{
 		return hypeHealthCardValidation::isValidHcnNT($hcn);
 	}
 	public static function isValidHcnON($s)
 	{
 		$s = strval($s);
 		$b = false;
 		$t = 0;
 	
 		if (strlen($s) == 10) {
 			$i = 0;
 			while ($i < 10)
 			{
 				if (!is_numeric($s[$i])) {
 					return false;
 				}
 				else {
 					$n = intval($s[$i]);
 					if (($i+1) % 2 == 1) {
 						$t = $t + (($n * 2) % 10) + intval(($n * 2) / 10);
 					}
 					else {
 						$t = $t + $n;
 					}
 				}
 				$i = $i + 1;
 			}
 		}
 		else {
 			return false;
 		}
 	
 		if ($t % 10 == 0) {
 			$b = true;
 		}
 		else {
 			$b = false;
 		}
 	
 		return $b;
 	}
 	public static function isValidHcnPE($hcn)
 	{
 		return strlen($hcn) == 8 && !hypeHealthCardValidation::isValidHcnNT($hcn);
 	}
 	public static function isValidHcnQC($hcn)
 	{
 		return hypeHealthCardValidation::isValidHcnQU($hcn);
 	}
 	public static function isValidHcnQU($hcn)
 	{
 		$regex = '/[a-z]{4}[0-9]{8}/i';
 		return (strlen($hcn) == 12) && preg_match($regex, $hcn);
 	}
 	public static function isValidHcnSK($hcn)
 	{
 		$hcn = strval($hcn);
 		$sum = -1;
 	
 		if (strlen($hcn) == 9) {
 			$sum = 0;
 			for ($i = 0; $i <= 7; $i++)
 			{
 			$sum += $hcn[$i] * (10 - ($i + 1));
 			}
 			$sum = 11 - ($sum % 11);
 	
 			return $sum == $hcn[8];
 		}
 		return false;
 	}
 	public static function isValidHcnYT($hcn)
 	{
 		return strlen($hcn) == 9 && !hypeHealthCardValidation::isValidHcnSk($hcn) && !hypeHealthCardValidation::isValidHcnNb($hcn);
 	}
 	
 	
	public static function getProvinceFromHCN($hcn)
	{
		$hcn_len = strlen($hcn);

		if ($hcn_len == 6) {
			return 'MB';
		}
		if ($hcn_len == 8) {
			if (hypeHealthCardValidation::isValidHcnNT($hcn)) {
				return 'NT';
			}
			return 'PE';
		}
		if ($hcn_len == 9) {
			if (hypeHealthCardValidation::isValidHcnSK($hcn)) {
				return 'SK';
			}
			else if (hypeHealthCardValidation::isValidHcnNB($hcn)) {
				return 'NB';
			}
			return 'YT';
		}
		if ($hcn_len == 10) {
			if (hypeHealthCardValidation::isValidHcnON($hcn)) {
				return 'ON';
			}
			else if (hypeHealthCardValidation::isValidHcnBc($hcn)) {
				return 'BC';
			}
			else if (hypeHealthCardValidation::isValidHcnNS($hcn)) {
				return 'NS';
			}
			return 'ON';
		}
		if ($hcn_len == 11) {
			if (hypeHealthCardValidation::isValidHcnAB($hcn)) {
				return 'AB';
			}
		}
		if ($hcn_len == 12) {
			if (hypeHealthCardValidation::isValidHcnQU($hcn)) {
				return 'QC';
			}
			return 'NL';

		}
		return '';
	}
}

