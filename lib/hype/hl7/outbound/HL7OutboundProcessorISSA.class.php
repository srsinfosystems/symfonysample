<?php

class HL7OutboundProcessorISSA extends HL7OutboundProcessorBase
{
 	protected $vendor_name = 'QuickClaim';            // goes in MSH Segment
 	protected $vendor_version = '2.3';                // goes in MSH Segment
 	protected $partner_app = 'ISSA';                 // goes in MSH Segment

 	public function processADT($patient, $message_count)
 	{
 		$this->connectEzHL7();

 		$datestamp = date('Ymd000000.0000-H:i:s');
 		$timestamp = date('Ymd000000');
 		$message_type = 'ADT';
 		$subtype = $this->subtype ? $this->subtype : self::getADTSubtype($patient->is_new_hl7);

 		$guid = $this->createGuid('PAT' . $patient->id . $patient->version, $datestamp, $message_count);
 		$has_ohip = $patient->moh_province == 'ON' && $patient->hcn_num && $patient->status == PatientTable::HCV_STATUS_CLOSED_OHIP;

 		$seg_count = 1;
 		$msh = $this->getMSHSegment($guid, $seg_count++, $timestamp, $message_type, $subtype, $patient->Client->company_name);
 		$evn = $this->getEVNSegment($guid, $seg_count++, $message_type, $timestamp, $subtype);
 		$pid = $this->getPIDSegment($guid, $seg_count++, $patient);
 		$pv1 = $this->getPV1Segment($guid, $seg_count++, null, null, $patient->ReferringDoctor);
 		$in1 = $this->getIN1Segment($guid, $seg_count, $has_ohip);

 		$hl7_message = $this->buildHl7Message(array($msh, $evn, $pid, $pv1, $in1));
 		$this->db_queries[] = $this->generateInsertHL7Data($guid, $message_type, $subtype, $seg_count, $hl7_message);

 		$this->insertDataEzHL7();
 	}
 	public function processMFN($referring_doctor, $message_count)
 	{
 		$this->connectEzHL7();

 		$datestamp = date('Ymd000000.0000-H:i:s');
 		$timestamp = date('Ymd000000');
 		$message_type = 'MFN';

 		$subtype = $this->subtype ? $this->subtype : self::getMFNSubtype();
 		$guid = $this->createGuid('RD' . $referring_doctor->id . $referring_doctor->version, $datestamp, $message_count);

 		$seg_count = 1;
 		$msh = $this->getMSHSegment($guid, $seg_count++, $timestamp, $message_type, $subtype, $referring_doctor->Client->company_name);
  		$mfi = $this->getMFISegment($guid, $seg_count++, $timestamp);
 		$mfe = $this->getMFESegment($guid, $seg_count++, $referring_doctor->id);
 		$stf = $this->getSTFSegment($guid, $seg_count++, $referring_doctor);
 		$pra = $this->getPRASegment($guid, $seg_count, $referring_doctor);

 		$hl7_message = $this->buildHL7Message(array($msh, $mfi, $mfe, $stf, $pra));
 		$this->db_queries[] = $this->generateInsertHL7Data($guid, $message_type, $subtype, $seg_count, $hl7_message);

 		$this->insertDataEzHL7();
 	}
 	public function processSIU($attendee, $message_count)
 	{
 		$this->connectEzHL7();

 		$datestamp = date('Ymd000000.0000-H:i:s');
 		$timestamp = date('Ymd000000');
 		$message_type = 'SIU';

 		$subtype = $this->subtype ? $this->subtype : self::getSIUSubtype($attendee->Appointment->status, $attendee->is_new_hl7);
 		$guid = $this->createGuid('ATT' . $attendee->id . $attendee->version, $datestamp, $message_count);

 		$seg_count = 1;
 		$msh = $this->getMSHSegment($guid, $seg_count++, $timestamp, $message_type, $subtype, $attendee->Appointment->Client->company_name);
 		$sch = $this->getSCHSegment($guid, $seg_count++, $subtype, $attendee->Appointment);
 		$nte = $this->getNTESegment($guid, $seg_count++, $attendee->Appointment);
 		$pid = $this->getPIDSegment($guid, $seg_count++, $attendee->Patient);
 		$pv1 = $this->getPV1Segment($guid, $seg_count++, $attendee->Appointment, $attendee->Appointment->Doctor, $attendee->Patient->ReferringDoctor);
 		$rgs = $this->getRGSSegment($guid, $seg_count++);
 		$aig = $this->getAIGSegment($guid, $seg_count++, $attendee->Appointment->Doctor, $attendee->Appointment->start_date, $attendee->Appointment->end_date);
 		$ail = $this->getAILSegment($guid, $seg_count++, $attendee->Appointment);
 		$aip = $this->getAIPSegment($guid, $seg_count, $attendee->Appointment->Doctor, $attendee->Appointment->start_date, $attendee->Appointment->end_date);

 		$hl7_message = $this->buildHL7Message(array($msh, $sch, $nte, $pid, $pv1, $rgs, $aig, $ail, $aip));
 		$this->db_queries[] = $this->generateInsertHL7Data($guid, $message_type, $subtype, $seg_count, $hl7_message);

 		$this->insertDataEzHL7();
 	}
}
