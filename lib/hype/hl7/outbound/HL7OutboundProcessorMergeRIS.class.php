<?php

class HL7OutboundProcessorMergeRIS extends HL7OutboundProcessorBase
{
 	protected $vendor_name = 'QuickClaim';            // goes in MSH Segment
 	protected $vendor_version = '2.3';                // goes in MSH Segment
 	protected $partner_app = 'QCHL7';                 // goes in MSH Segment

 	public function processADT($patient, $message_count)
 	{
 		// todo: fill in patient's HL7 Partner ID if we don't have it already from the Chart number field
 		$patient->setHl7IDForPartner($patient->id, 'MERGERIS');
 		$patient->save();

 		$this->connectEzHL7();

 		$datestamp = date('Ymd000000.0000-H:i:s');
 		$timestamp = date('Ymd000000');
 		$message_type = 'ADT';
 		$subtype = $this->subtype ? $this->subtype : self::getADTSubtype($patient->is_new_hl7);

 		$guid = $this->createGuid('PAT' . $patient->id . $patient->version, $datestamp, $message_count);
 		$has_ohip = (bool) $patient->hcn_num;

 		$seg_count = 1;
 		$segments = array();

 		$msh = $this->getMSHSegment($guid, $seg_count++, $timestamp, $message_type, $subtype, $patient->Client->company_name);
 		$segments[] = $msh;

 		$evn = $this->getEVNSegment($guid, $seg_count++, $message_type, $timestamp, $subtype);
 		$segments[] = $evn;

 		$pid = $this->getPIDSegment($guid, $seg_count++, $patient);
 		$segments[] = $pid;

 		// 2012-07-23: Please remove PV1 segment if not using for that patient
 		// 2012-07-26: Check using ref_doc_id instead of lazy loaded realtional object (would always resolve to true)
 		// 2012-07-30: The column is called  ref_doc_refdoc_id, duh
		// 2017-08-09 ref_doc_refdoc_id is no longer used
 		if ($patient->ref_doc_id) {
 			$pv1 = $this->getPV1Segment($guid, $seg_count++, null, null, $patient->ReferringDoctor);
 			$segments[] = $pv1;
 		}

 		// 2012-07-23: Please remove IN1 segments if not using for that patient
 // 		if ($has_ohip) {
 			$in1 = $this->getIN1SegmentMerge($guid, $seg_count, $has_ohip, $patient);
 			$segments[] = $in1;
 // 		}

 		$hl7_message = $this->buildHl7Message($segments);
 		$this->db_queries[] = $this->generateInsertHL7Data($guid, $message_type, $subtype, $seg_count, $hl7_message);

 		$this->insertDataEzHL7();
 	}
 	public function processMFN($referring_doctor, $message_count)
 	{
 		$this->connectEzHL7();

 		// 2012-08-08: don't send MFN messages

 		$this->insertDataEzHL7();
 	}
 	public function processSIU($attendee, $message_count)
 	{
 		$this->connectEzHL7();

 		$datestamp = date('Ymd000000.0000-H:i:s');
 		$timestamp = date('Ymd000000');
 		$message_type = 'SIU';

 		$subtype = $this->subtype ? $this->subtype : self::getSIUSubtype($attendee->Appointment->status, $attendee->is_new_hl7);
 		$guid = $this->createGuid('ATT' . $attendee->id . $attendee->version, $datestamp, $message_count);

 		$seg_count = 1;
 		$msh = $this->getMSHSegment($guid, $seg_count++, $timestamp, $message_type, $subtype, $attendee->Appointment->Client->company_name);
 		$sch = $this->getSCHSegment($guid, $seg_count++, $subtype, $attendee->Appointment);
 		$nte = $this->getNTESegment($guid, $seg_count++, $attendee->Appointment);
 		$pid = $this->getPIDSegment($guid, $seg_count++, $attendee->Patient);
 		$pv1 = $this->getPV1Segment($guid, $seg_count++, $attendee->Appointment, $attendee->Appointment->Doctor, $attendee->Patient->ReferringDoctor);
 		$rgs = $this->getRGSSegment($guid, $seg_count++);
 		$aig = $this->getAIGSegment($guid, $seg_count++, $attendee->Appointment->Doctor, $attendee->Appointment->start_date, $attendee->Appointment->end_date);
 		$ail = $this->getAILSegment($guid, $seg_count++, $attendee->Appointment);
 		$aip = $this->getAIPSegment($guid, $seg_count, $attendee->Appointment->Doctor, $attendee->Appointment->start_date, $attendee->Appointment->end_date);

 		$hl7_message = $this->buildHL7Message(array($msh, $sch, $nte, $pid, $pv1, $rgs, $aig, $ail, $aip));
 		$this->db_queries[] = $this->generateInsertHL7Data($guid, $message_type, $subtype, $seg_count, $hl7_message);

 		$this->insertDataEzHL7();
 	}
 	protected function getAIGSegment($guid, $msg_count, $doctor, $start_date, $end_date)
 	{
 		$resource_id = array();

 		$segment[] = array('AIG');

 		// F1 set ID
 		$segment[] = array('1');

 		// F2 segment action code
 		$segment[] = array();

 		// F3 resource id
 		if ($doctor instanceof Doctor) {
 			$resource_id = array('D-' . $doctor->id, substr($doctor->lname, 0, 40), substr($doctor->fname, 0, 40), '', '', substr($doctor->title, 0, 40));
 		}
 		$segment[] = $resource_id;

 		// F4 resource type
 		$segment[] = array($doctor instanceof Doctor ? 'Doctor' : '');

 		// F5 resource group
 		$segment[] = array();

 		// F6 resource quantity
 		$segment[] = array();

 		// F7 resource quantity units
 		$segment[] = array();

 		// F8 start time / date
 		$segment[] = array(date('YmdHis',hype::parseDateToInt($start_date, hype::DB_DATETIME_FORMAT)));

 		// F9 start date / time offset
 		$segment[] = array();

 		// F10 start date / time offset units
 		$segment[] = array();

 		// F11 duration
 		$segment[] = array((hype::parseDateToInt($end_date, hype::DB_DATETIME_FORMAT) - hype::parseDateToInt($start_date, hype::DB_DATETIME_FORMAT)) / 60);

 		// F12 duration units
 		$segment[] = array('minutes');

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'AIG', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('AIG', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
 	protected function getAIPSegment($guid, $msg_count, $doctor, $start_date, $end_date)
 	{

 		$segment[] = array('AIP');

 		// F1 set id
 		$segment[] = array('1');

 		// F2 segment action code
 		$segment[] = array();

 		// F3 personnel resource id
 		$resource_id = array();
 		if ($doctor instanceof Doctor) {
 			$resource_id = array('D-' . $doctor->id, substr($doctor->lname, 0, 35), substr($doctor->fname, 0, 35), '', '', substr($doctor->title, 0, 35));
 		}
 		$segment[] = $resource_id;

 		// F4 resource role
 		$segment[] = array('Doctor');

 		// F5 resource group
 		$segment[] = array();

 		// F6 start date/ time
 		$segment[] = array(date('YmdHis',hype::parseDateToInt($start_date, hype::DB_DATETIME_FORMAT)));

 		// F7 start date/time offset
 		$segment[] = array();

 		// F8 start date/time offset units
 		$segment[] = array();

 		// F9 duration
 		$segment[] = array(((hype::parseDateToInt($end_date, hype::DB_DATETIME_FORMAT)) - hype::parseDateToInt($start_date, hype::DB_DATETIME_FORMAT)) / 60);

 		// F10 duration units
 		$segment[] = array('minutes');

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'AIP', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('AIP', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
 	protected function getIN1SegmentMerge($guid, $msg_count, $has_ohip, $patient)
 	{
 		$hcn = $patient->getHCN();
 		$exp_date = $patient->getHcnExpYear() ? date('Ymd', hype::parseDateToInt($patient->getHcnExpYear(), hype::DB_DATETIME_FORMAT)) : '';

 		$segment[] = array('IN1');
 		// F1
 		$segment[] = array($has_ohip ? '1' : '');
 		// F2
 		$segment[] = array('OHIP');
 		// F3
 		// 2012-08-10: IN1-3 is missing ("1").
 		$segment[] = array('OHIP');
 		// F4
 		$segment[] = array();
 		// F5
 		$segment[] = array();
 		// F6
 		$segment[] = array();
 		// F7
 		$segment[] = array();
 		// F8
 		$segment[] = array();
 		// F9
 		$segment[] = array();
 		// F10
 		$segment[] = array();
 		// F11
 		$segment[] = array();
 		// F12
 		$segment[] = array();
 		// F13 - expiration date
 		$segment[] = array($exp_date);
 		// F14
 		$segment[] = array();
 		// F15
 		$segment[] = array();
 		// F16
 		$segment[] = array();
 		// F17
 		$segment[] = array();
 		// F18
 		$segment[] = array();
 		// F19
 		$segment[] = array();
 		// F20
 		$segment[] = array();
 		// F21
 		$segment[] = array();
 		// F22
 		$segment[] = array();
 		// F23
 		$segment[] = array();
 		// F24
 		$segment[] = array();
 		// F25
 		$segment[] = array();
 		// F26
 		$segment[] = array();
 		// F27
 		$segment[] = array();
 		// F28
 		$segment[] = array();
 		// F29
 		$segment[] = array();
 		// F30
 		$segment[] = array();
 		// F31
 		$segment[] = array();
 		// F32
 		$segment[] = array();
 		// F33
 		$segment[] = array();
 		// F34
 		$segment[] = array();
 		// F35
 		$segment[] = array();
 		// F36 - Health Card Number (number plus version code)
 		$segment[] = array(trim($hcn . $patient->hcn_version_code));

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'IN1', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('IN1', 'A', $guid, $msg_count, $segment);
 		$this->db_queries[] = $this->generateInsertSegment('IN1', 'B', $guid, $msg_count, array());

 		return $segment;
 	}
 	protected function getPIDSegment($guid, $msg_count, $patient)
 	{
 		$segment[] = array('PID');

 		// F1 set ID
 		$segment[] = array('1');

 		// F2 Patient Id
 		$segment[] = array($patient->id);

 		// F3 Patient Identifier List
 		$segment[] = array();

 		// F4 Alternate Patient ID - PID
 		$segment[] = array();

 		// F5 Patient Name
 		$segment[] = array(substr($patient->lname, 0, 35), substr($patient->fname, 0, 35));

 		// F6 Mother's Maiden Name
 		$segment[] = array();

 		// F7 Date/Time of Birth
 		$segment[] = array($patient->dob ? date('Ymd', hype::parseDateToInt($patient->dob, hype::DB_DATETIME_FORMAT)) : '');

 		// F8 Sex
 		$segment[] = array($patient->sex ? $patient->sex : 'U') ;

 		// F9 Patient Alias
 		$segment[] = array();

 		// F10 Race
 		$segment[] = array();

 		// F11 Patient Address
 		$country = $patient->getCountry() == 'CAN' ? 'CANADA' : '';
 		$segment[] = array(substr($patient->address, 0, 40), '', substr($patient->city, 0, 40), $patient->province, substr($patient->postal_code, 0, 40), $country);

 		// F12 Country Code
 		$segment[] = array();

 		// F13 Phone Number - Home
 		// 2012-07-23: all phone numbers must be ten digits
 		$segment[] = array(substr($patient->hphone, 0, 10), '', substr($patient->email, 0, 20), '', '', '', substr($patient->cellphone, 0, 10));

 		// F14 Phone Number - Business
 		// 2012-07-23: all phone numbers must be ten digits
 		$segment[] = array(substr($patient->wphone, 0, 10));

 		// F15 primary language
 		$segment[] = array();

 		// F16 marital status
 		// 2012-07-23: default to  U for unknown
 		$segment[] = array('U');

 		// F17 religion
 		$segment[] = array();

 		/**
 		 * SEND Chart NUMBER HERE
 		 */
 		$segment[] = array($patient->id);
 		$segment[] = array();

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'PID', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('PID', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
 	protected function getPV1Segment($guid, $msg_count, $appointment, $attending_doctor, $referring_doctor)
 	{
 		$location_id = $location_name = '';
 		if ($appointment instanceof Appointment) {
 			$location_id = $appointment->location_id;
 			$location_name = $appointment->Location->name;
 		}

 		$doctor_id = $doctor_lname = $doctor_fname = '';
 		if ($attending_doctor instanceof Doctor) {
 			$doctor_id = 'D-' . $attending_doctor->id;
 			$doctor_lname = substr($attending_doctor->lname, 0, 35);
 			$doctor_fname = substr($attending_doctor->fname, 0, 35);
 		}

 		$ref_doc_id = $ref_doc_lname = $ref_doc_fname = $ref_doc_salutation = '';
 		if ($referring_doctor instanceof ReferringDoctor && $referring_doctor->id) {
 			$ref_doc_id = 'RD-' . $referring_doctor->id;
 			$ref_doc_lname = substr($referring_doctor->lname, 0, 35);
 			$ref_doc_fname = substr($referring_doctor->fname, 0, 35);
 			$ref_doc_salutation = substr($referring_doctor->salutation, 0, 35);
 		}

 		$segment[] = array('PV1');

 		// F1 set ID
 		$segment[] = array('1');

 		// F2 Patient Class
 		$segment[] = array('O');

 		// F3 Assigned Patient Location
 		// 2012-08-10:  PV1-3.4 \96 could you put a \931\94 as you did with the AIL segment please
 		$segment[] = array($location_id, '', '', '1', '', '', '', '', substr($location_name, 0, 20));

 		// F4 Admission Type
 		$segment[] = array();

 		// F5 Preadmit Number
 		$segment[] = array();

 		// F6 Prior Patient Location
 		$segment[] = array();

 		// F7 Attending Doctor
 		$segment[] = array($doctor_id, $doctor_lname, $doctor_fname);

 		// F8 Referring Doctor
 		$segment[] = array($ref_doc_id, $ref_doc_lname, $ref_doc_fname, '', '', $ref_doc_salutation);

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'PV1', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('PV1', 'A', $guid, $msg_count, $segment);
 		$this->db_queries[] = $this->generateInsertSegment('PV1', 'B', $guid, $msg_count, array());

 		return $segment;
 	}
 	protected function getSCHSegment($guid, $msg_count, $subtype, $appointment)
 	{
 		$minutes = (hype::parseDateToInt($appointment->end_date, hype::DB_DATETIME_FORMAT) - hype::parseDateToInt($appointment->start_date, hype::DB_DATETIME_FORMAT)) / 60;

 		$segment[] = array('SCH');

 		// F1 Appointment ID
 		$segment[] = array($appointment->id);

 		// F2 Filler Appointment ID
 		$segment[] = array();

 		// F3 Occurrence Number
 		$segment[] = array();

 		// F4 Group Number
 		$segment[] = array();

 		// F5 Schedule ID
 		$segment[] = array();

 		// F6 Event Reason
 		$segment[] = array(substr($appointment->service_code, 0, 40));

 		// F7 Appointment Reason
 		// 2012-08-09:  let\92s set this to \93BOOKED\94 in SCH-7.1.  To be consistent, can we put \93BOOKED\94 in SCH-8.1 as well please?
 		// 2012-08-09: notes is limited to 40 chars
 		$segment[] = array(substr('BOOKED', 0, 40), substr(str_replace(array("\r", "\n", "\r\n"), '   ', $appointment->notes), 0, 40));

 		// F8 Appointment Type
 		// 2012-08-09:  let\92s set this to \93BOOKED\94 in SCH-7.1.  To be consistent, can we put \93BOOKED\94 in SCH-8.1 as well please?
 		$segment[] = array(substr('BOOKED', 0, 40), substr($appointment->AppointmentType->name, 0, 40));

 		// F9 Appointment Duration
 		$segment[] = array($minutes);

 		// F10 Appointment Duration Units
 		$segment[] = array('M');

 		// F11 Appointment Timing Quantity
 		$segment[] = array('', '', substr(60 * $minutes, 0, 40), date('YmdHis', hype::parseDateToInt($appointment->start_date, hype::DB_DATETIME_FORMAT)), date('YmdHis', hype::parseDateToInt($appointment->end_date, hype::DB_DATETIME_FORMAT)));

 		// F12 Contact Person
 		$segment[] = array();

 		// F13 Contact Phone Number
 		$segment[] = array();

 		// F14 Contact Address
 		$segment[] = array();

 		// F15 Contact Location
 		$segment[] = array();

 		// F16 Filler Contact Person
 		$segment[] = array();

 		// F17 Filler Contact Phone Number
 		$segment[] = array();

 		// F18 Filler Contact Address
 		$segment[] = array();

 		// F19 Filler Contact Location
 		$segment[] = array($appointment->location_id);

 		// F20 Entered By Person
 		$segment[] = array();

 		// F21 Entered By Phone Number
 		$segment[] = array();

 		// F22 Entered By Location
 		$segment[] = array();

 		// F23 Parent Appointment ID
 		$segment[] = array();

 		// F24 Parent Filler Appointment ID
 		$segment[] = array();

 		// F25 Filler Status Code
 		// 2012-08-10: Unfortunately, the Appointment Status updates in SCH-25.2 are case sensitive.   BOOKED^Arrived and BOOKED^Visit Complete
 		$stage_name = $this->processStageName($appointment->StageClient->stage_name);
 		$segment[] = array(substr($appointment->stage_client_id ? $stage_name : '', 0, 40));

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'SCH', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('SCH', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
 	private function processStageName($name)
 	{
 		if ($name == 'BOOKED^VISIT COMPLETE') {
 			return 'BOOKED^Visit Complete';
 		}
 		if ($name == 'BOOKED^ARRIVED') {
 			return 'BOOKED^Arrived';
 		}
 		return $name;
 	}
}
