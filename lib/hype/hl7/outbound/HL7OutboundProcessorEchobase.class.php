<?php

class HL7OutboundProcessorEchobase extends HL7OutboundProcessorBase
{
	protected $vendor_name = 'QuickClaim';            // goes in MSH Segment
	protected $vendor_version = '2.3';                // goes in MSH Segment
	protected $partner_app = 'QCHL7';                 // goes in MSH Segment

	public function processADT($patient, $message_count)
	{
		$this->connectEzHL7();

		$datestamp = date('Ymd000000.0000-H:i:s');
		$timestamp = date('Ymd000000');
		$message_type = 'ADT';
		$subtype = $this->subtype ? $this->subtype : self::getADTSubtype($patient->is_new_hl7);

		$guid = $this->createGuid('PAT' . $patient->id . $patient->version, $datestamp, $message_count);
		$has_ohip = $patient->moh_province == 'ON' && $patient->hcn_num && $patient->status == PatientTable::HCV_STATUS_CLOSED_OHIP;

		$seg_count = 1;
		$msh = $this->getMSHSegment($guid, $seg_count++, $timestamp, $message_type, $subtype, $patient->Client->company_name);
		$evn = $this->getEVNSegment($guid, $seg_count++, $message_type, $timestamp, $subtype);
		$pid = $this->getPIDSegment($guid, $seg_count++, $patient);
		$pv1 = $this->getPV1Segment($guid, $seg_count++, null, null, $patient->ReferringDoctor);
		$in1 = $this->getIN1Segment($guid, $seg_count, $has_ohip);

		$hl7_message = $this->buildHl7Message(array($msh, $evn, $pid, $pv1, $in1));
		$this->db_queries[] = $this->generateInsertHL7Data($guid, $message_type, $subtype, $seg_count, $hl7_message);

		$this->insertDataEzHL7();
	}
	public function processMFN($referring_doctor, $message_count)
	{
		$this->connectEzHL7();

		$datestamp = date('Ymd000000.0000-H:i:s');
		$timestamp = date('Ymd000000');
		$message_type = 'MFN';

		$subtype = $this->subtype ? $this->subtype : self::getMFNSubtype();
		$guid = $this->createGuid('RD' . $referring_doctor->id . $referring_doctor->version, $datestamp, $message_count);

		$seg_count = 1;
		$msh = $this->getMSHSegment($guid, $seg_count++, $timestamp, $message_type, $subtype, $referring_doctor->Client->company_name);
 		$mfi = $this->getMFISegment($guid, $seg_count++, $timestamp);
		$mfe = $this->getMFESegment($guid, $seg_count++, $referring_doctor->id);
		$stf = $this->getSTFSegment($guid, $seg_count++, $referring_doctor);
		$pra = $this->getPRASegment($guid, $seg_count, $referring_doctor);

		$hl7_message = $this->buildHL7Message(array($msh, $mfi, $mfe, $stf, $pra));
		$this->db_queries[] = $this->generateInsertHL7Data($guid, $message_type, $subtype, $seg_count, $hl7_message);

		$this->insertDataEzHL7();
	}
	public function processSIU($attendee, $message_count)
	{
		$this->connectEzHL7();

		$datestamp = date('Ymd000000.0000-H:i:s');
		$timestamp = date('Ymd000000');
		$message_type = 'SIU';

		$subtype = $this->subtype ? $this->subtype : self::getSIUSubtype($attendee->Appointment->status, $attendee->is_new_hl7);
		$guid = $this->createGuid('ATT' . $attendee->id . $attendee->version, $datestamp, $message_count);

		$seg_count = 1;
		$msh = $this->getMSHSegment($guid, $seg_count++, $timestamp, $message_type, $subtype, $attendee->Appointment->Client->company_name);
		$sch = $this->getSCHSegment($guid, $seg_count++, $subtype, $attendee->Appointment);
		$nte = $this->getNTESegment($guid, $seg_count++, $attendee->Appointment);
		$pid = $this->getPIDSegment($guid, $seg_count++, $attendee->Patient);
		$pv1 = $this->getPV1Segment($guid, $seg_count++, $attendee->Appointment, $attendee->Appointment->Doctor, $attendee->Patient->ReferringDoctor);
		$rgs = $this->getRGSSegment($guid, $seg_count++);
		$aig = $this->getAIGSegment($guid, $seg_count++, $attendee->Appointment->Doctor, $attendee->Appointment->start_date, $attendee->Appointment->end_date);
		$ail = $this->getAILSegment($guid, $seg_count++, $attendee->Appointment);
		$aip = $this->getAIPSegment($guid, $seg_count, $attendee->Appointment->Doctor, $attendee->Appointment->start_date, $attendee->Appointment->end_date);

		$hl7_message = $this->buildHL7Message(array($msh, $sch, $nte, $pid, $pv1, $rgs, $aig, $ail, $aip));
		$this->db_queries[] = $this->generateInsertHL7Data($guid, $message_type, $subtype, $seg_count, $hl7_message);

		$this->insertDataEzHL7();
	}
	protected function getNTESegment($guid, $msg_count, $appointment)
	{
		$segment[] = array('NTE');

		// F1 Set ID
		$segment[] = array(1);

		// F2 Source Of Comment
		$segment[] = array();

		// F3 Comment
		$segment[] = array(str_replace(array("\r\n", "\r", "\n"), '     ', $appointment->notes));

		// F4 Comment Type
		$segment[] = array();

		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'NTE', $msg_count, $this->buildHL7Segment($segment));
		$this->db_queries[] = $this->generateInsertSegment('NTE', 'A', $guid, $msg_count, $segment);

		return $segment;
	}
	protected function getSCHSegment($guid, $msg_count, $subtype, $appointment)
	{
		$minutes = (hype::parseDateToInt($appointment->end_date, hype::DB_DATETIME_FORMAT) - hype::parseDateToInt($appointment->start_date, hype::DB_DATETIME_FORMAT)) / 60;

		$segment[] = array('SCH');

		// F1 Appointment ID
		$segment[] = array($appointment->id);

		// F2 Filler Appointment ID
		$segment[] = array();

		// F3 Occurrence Number
		$segment[] = array();

		// F4 Group Number
		$segment[] = array();

		// F5 Schedule ID
		$segment[] = array();

		// F6 Event Reason
		$segment[] = array(substr($appointment->service_code, 0, 40));

		// F7 Appointment Reason
		$segment[] = array(substr($appointment->service_code, 0, 40), '');

		// F8 Appointment Type
		$segment[] = array(substr(AppointmentTable::getStatusString($appointment->status, true), 0, 40), substr($appointment->AppointmentType->name, 0, 40));

		// F9 Appointment Duration
		$segment[] = array($minutes);

		// F10 Appointment Duration Units
		$segment[] = array('M');

		// F11 Appointment Timing Quantity
		$segment[] = array('', '', substr(60 * $minutes, 0, 40), date('YmdHis', hype::parseDateToInt($appointment->start_date, hype::DB_DATETIME_FORMAT)), date('YmdHis', hype::parseDateToInt($appointment->end_date, hype::DB_DATETIME_FORMAT)));

		// F12 Contact Person
		$segment[] = array();

		// F13 Contact Phone Number
		$segment[] = array();

		// F14 Contact Address
		$segment[] = array();

		// F15 Contact Location
		$segment[] = array();

		// F16 Filler Contact Person
		$segment[] = array();

		// F17 Filler Contact Phone Number
		$segment[] = array();

		// F18 Filler Contact Address
		$segment[] = array();

		// F19 Filler Contact Location
		$segment[] = array($appointment->location_id);

		// F20 Entered By Person
		$segment[] = array();

		// F21 Entered By Phone Number
		$segment[] = array();

		// F22 Entered By Location
		$segment[] = array();

		// F23 Parent Appointment ID
		$segment[] = array();

		// F24 Parent Filler Appointment ID
		$segment[] = array();

		// F25 Filler Status Code
		$segment[] = array(substr($appointment->stage_client_id ? $appointment->StageClient->stage_name : '', 0, 40));

		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'SCH', $msg_count, $this->buildHL7Segment($segment));
		$this->db_queries[] = $this->generateInsertSegment('SCH', 'A', $guid, $msg_count, $segment);

		return $segment;
	}

}
