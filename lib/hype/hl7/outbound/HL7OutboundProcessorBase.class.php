<?php

abstract class HL7OutboundProcessorBase
{
 	protected $dbh = null;                            // database connection
 	protected $db_queries = array();                  // array full of database queries
 	protected $debug = false;                         // debugging text on?
 	protected $hl7_queue = null;
 	protected $subtype = null;

 	abstract public function processADT($patient, $message_count);
 	abstract public function processMFN($referring_doctor, $message_count);
 	abstract public function processSIU($attendee, $message_count);

 	public function __construct()
 	{
 		return $this;
 	}
 	public function setDebug($v)
 	{
 		$this->debug = $v;
 	}
 	public function setHl7Queue($q)
 	{
 		$this->hl7_queue = $q;
 	}
 	public function setMessageSubtype($t)
 	{
 		$this->subtype = $t;
 	}
 	protected function connectEzHL7()
 	{
 		try {
 			$this->dbh = new PDO(
 				sfConfig::get('app_hl7_inbound_database_dsn'),
 				sfConfig::get('app_hl7_inbound_database_username'),
 				(!sfConfig::get('app_hl7_inbound_database_password') ? '' : sfConfig::get('app_hl7_inbound_database_password'))
 			);

 			$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 		} catch (PDOException $e) {
 			throw new Exception('PDO Connection Error: ' . $e->getMessage());
 		}
 	}
 	protected function insertDataEzHL7()
 	{
 		try {
 			$this->dbh->beginTransaction();

 			foreach ($this->db_queries as $q)
 			{
 				$statement = $this->dbh->prepare($q['query']);
 				$statement->execute(array_values($q['parameters']));
 			}
 			$this->dbh->commit();
 		}
 		catch (PDOException $e) {
 			if ($this->hl7_queue) {
  				print_r ($q);
  				echo $e->getMessage();exit();

 				$this->hl7_queue->status = HL7QueueTable::STATUS_ERRORS;
 				$this->hl7_queue->errors = 'Error occurred while inserting data into EZHL7 Database. ';// . implode('; ', $q['parameters']);
 				$this->hl7_queue->save();
 				return;
 			}

 			$this->dbh->rollback();
 			// rollback
 			throw new Exception('PDO Query Error: ' . $e->getMessage());
 		}

 		if ($this->hl7_queue) {
 			$this->hl7_queue->status = HL7QueueTable::STATUS_PROCESSED;
 			$this->hl7_queue->save();
 		}
 	}
 	protected function createGuid($id, $ts, $message_count)
 	{
 		while (strlen($message_count) < 3)
 		{
 			$message_count .= '0';
 		}

 		$guid = str_replace(' ', '', $id . microtime() . $message_count);

 		while (!$this->isGuidUnique($guid)) {
 			$guid = $this->createGuid($id, $ts, $message_count + 1);
 		}
 		return $guid;
 	}
 	protected function isGuidUnique($guid)
 	{
 		$query = 'SELECT count(*) as count from HYPE_HL7Data where MessageId = (?)';
 		$statement = $this->dbh->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
 		$statement->execute(array($guid));

 		$rs = $statement->fetch();
 		return $rs['count'] == 0;
 	}
 	protected function buildHL7Message($segments)
 	{
 		$message = '';
 		foreach ($segments as $segment)
 		{
 			$message .= $this->buildHL7Segment($segment);
 			$message .= "\n";
 		}

 		return $message;
 	}
 	protected function buildHL7Segment($seg)
 	{
 		$message = '';
 		foreach ($seg as $component) {
 			$message .= implode('^', $component);
 			$message .= '|';
 		}

 		return $message;
 	}
 	protected function buildParametersString($count)
 	{
 		$rs = '';
 		for ($a = 1; $a <= $count; $a++)
 		{
 			if (strlen($rs)) {
 				$rs .= ', ';
 			}
 			$rs .= '?';
 		}
 		return $rs;
 	}
 	protected function generateInsertMessageManifest($guid, $type, $index, $segment)
 	{
 		$query = 'INSERT INTO HYPE_MessageManifest (MessageId, SegmentName, SegmentIDX, SegmentData) values (?, ?, ?, ?)';

 		$parameters = array(
 			'MessageId' =>  $guid,
 			'SegmentName' => $type,
 			'SegmentIDX' => $index,
 			'SegmentData' => $segment,
 		);

 		return array(
 			'query' => $query,
 			'parameters' => $parameters
 		);
 	}
 	protected function generateInsertHL7Data($guid, $type, $subtype, $msg_count, $message)
 	{
 		$query = 'INSERT INTO HYPE_HL7Data '
 			. ' (MessageId, VendorName, VendorVersion, MsgControl, PartnerAPP, MsgType, MsgEvent, Outbound, Inbound, Processed, SchemaLoaded, SegmentCount, MessageSize, HL7Message) '
 			. ' values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

 		$parameters = array(
 			'MessageId' =>     $guid,
 			'VendorName' =>    $this->vendor_name,
 			'VendorVersion' => $this->vendor_version,
 			'MsgControl' =>    $guid,
 			'PartnerAPP' =>    $this->partner_app,
 			'MsgType' =>       $type,
 			'MsgEvent' =>      $subtype,
 			'Outbound' =>      1,
 			'Inbound' =>       0,
 			'Processed' =>     0,
 			'SchemaLoaded' =>  1,
 			'SegmentCount' =>  $msg_count,
 			'MessageSize' =>   strlen($message),
 			'HL7Message' =>    $message,
 		);

 		return array(
 			'query' => $query,
 			'parameters' => $parameters
 		);
 	}
 	protected function generateInsertSegment($seg_type, $mode, $guid, $index, $data)
 	{
 		$columns = array('MessageId', 'IDX');
 		$parameters = array(
 			'MessageId' => $guid,
 			'IDX' => $index
 		);

 		foreach ($data as $f_key => $field)
 		{
 			foreach ($field as $c_key => $component)
 			{
 				if ($component && $f_key != '0') {
 					$col = $seg_type . '_F' . ($f_key) . '_C' . ($c_key + 1);
 					$columns[] = $col;
 					$parameters[$col] = $component;
 				}
 			}
 		}

 		$query = 'INSERT INTO HYPE_Segment_' . $seg_type . '_' . $mode . ' (' . implode(', ', $columns) . ') values (' . $this->buildParametersString(count($columns)) . ')';

 		return array(
 			'query' => $query,
 			'parameters' => $parameters
 		);
 	}
 	protected function generateInsertSegmentMSH($guid, $index, $data)
 	{
 		$columns = array('MessageId', 'IDX');
 		$parameters = array(
 			'MessageId' => $guid,
 			'IDX' => $index
 		);

 		foreach ($data as $f_key => $field)
 		{
 			if ($f_key != 0 && $f_key) {
 				foreach ($field as $c_key => $component)
 				{
 					if ($component) {
 						$col = 'MSH_F' . ($f_key + 1) . '_C' . ($c_key + 1);
 						$columns[] = $col;
 						$parameters[$col] = $component;
 					}
 				}
 			}
 		}

 		$query = 'INSERT INTO HYPE_Segment_MSH_A (' . implode(', ', $columns) . ') values (' . $this->buildParametersString(count($columns)) . ')';

 		return array(
 			'query' => $query,
 			'parameters' => $parameters
 		);
 	}
 	protected function getAIGSegment($guid, $msg_count, $doctor, $start_date, $end_date)
 	{
 		$resource_id = array();

 		$segment[] = array('AIG');

 		// F1 set ID
 		$segment[] = array('1');

 		// F2 segment action code
 		$segment[] = array();

 		// F3 resource id
 		if ($doctor instanceof Doctor) {
 			$resource_id = array($doctor->id, substr($doctor->lname, 0, 40), substr($doctor->fname, 0, 40), '', '', substr($doctor->title, 0, 40));
 		}
 		$segment[] = $resource_id;

 		// F4 resource type
 		$segment[] = array($doctor instanceof Doctor ? 'Doctor' : '');

 		// F5 resource group
 		$segment[] = array();

 		// F6 resource quantity
 		$segment[] = array();

 		// F7 resource quantity units
 		$segment[] = array();

 		// F8 start time / date
 		$segment[] = array(date('YmdHis',hype::parseDateToInt($start_date, hype::DB_DATETIME_FORMAT)));

 		// F9 start date / time offset
 		$segment[] = array();

 		// F10 start date / time offset units
 		$segment[] = array();

 		// F11 duration
 		$segment[] = array((hype::parseDateToInt($end_date, hype::DB_DATETIME_FORMAT) - hype::parseDateToInt($start_date, hype::DB_DATETIME_FORMAT)) / 60);

 		// F12 duration units
 		$segment[] = array('minutes');

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'AIG', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('AIG', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
 	protected function getAILSegment($guid, $msg_count, $appointment)
 	{
 		$segment[] = array('AIL');

 		// F1 set id
 		$segment[] = array(1);

 		// F2 segment action code
 		$segment[] = array();

 		// F3 location resource id
 		$segment[] = array($appointment->location_id, '', '', '1', '', '', '', '', substr($appointment->Location->name, 0, 20));

 		// F4 location type - ail
 		$segment[] = array('Place of Service');

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'AIL', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('AIL', 'A', $guid, $msg_count, $segment);
 		return $segment;
 	}
 	protected function getAIPSegment($guid, $msg_count, $doctor, $start_date, $end_date)
 	{

 		$segment[] = array('AIP');

 		// F1 set id
 		$segment[] = array('1');

 		// F2 segment action code
 		$segment[] = array();

 		// F3 personnel resource id
 		$resource_id = array();
 		if ($doctor instanceof Doctor) {
 			$resource_id = array($doctor->id, substr($doctor->lname, 0, 35), substr($doctor->fname, 0, 35), '', '', substr($doctor->title, 0, 35));
 		}
 		$segment[] = $resource_id;

 		// F4 resource role
 		$segment[] = array('Doctor');

 		// F5 resource group
 		$segment[] = array();

 		// F6 start date/ time
 		$segment[] = array(date('YmdHis',hype::parseDateToInt($start_date, hype::DB_DATETIME_FORMAT)));

 		// F7 start date/time offset
 		$segment[] = array();

 		// F8 start date/time offset units
 		$segment[] = array();

 		// F9 duration
 		$segment[] = array(((hype::parseDateToInt($end_date, hype::DB_DATETIME_FORMAT)) - hype::parseDateToInt($start_date, hype::DB_DATETIME_FORMAT)) / 60);

 		// F10 duration units
 		$segment[] = array('minutes');

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'AIP', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('AIP', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
 	protected function getEVNSegment($guid, $msg_count, $event_type, $timestamp, $reason_code)
 	{
 		$segment[] = array('EVN');

 		// Event Type Code
 		$segment[] = array($event_type);

 		// Recorded Date/Time
 		$segment[] = array($timestamp);

 		// Date/Time Planned Event
 		$segment[] = array();

 		// Event Reason Code
 		$segment[] = array($reason_code);

 		// Operator ID
 		$segment[] = array();

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'EVN', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('EVN', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
 	protected function getIN1Segment($guid, $msg_count, $has_ohip)
 	{
 		$segment[] = array('IN1');
 		// F1
 		$segment[] = array($has_ohip ? '1' : '');
 		// F2
 		$segment[] = array();
 		// F3
 		$segment[] = array();
 		// F4
 		$segment[] = array($has_ohip ? 'OHIP' : '');

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'IN1', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('IN1', 'A', $guid, $msg_count, $segment);
 		$this->db_queries[] = $this->generateInsertSegment('IN1', 'B', $guid, $msg_count, array());

 		return $segment;
 	}
 	protected function getMFESegment($guid, $msg_count, $id)
 	{
 		$segment[] = array('MFE');

 		// F1 record-level event code
 		$segment[] = array('MUP');

 		// F2 mfn control id
 		$segment[] = array();

 		// F3 effective date/time
 		$segment[] = array();

 		// F4 primary key value
 		$segment[] = array($id);

 		// F5 primary key value type

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'MFE', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('MFE', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
 	protected function getMFISegment($guid, $msg_count, $timestamp)
 	{
 		$segment[] = array('MFI');

 		// F1 master file identifier
 		$segment[] = array('PRA');

 		// F2 master file application identifier
 		$segment[] = array();

 		// F3 file-level event code
 		$segment[] = array('REP');

 		// F4 entered date/time
 		$segment[] = array();

 		// F5 effective date/time
 		$segment[] = array($timestamp);

 		// F6 response level code
 		$segment[] = array('NE');

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'MFI', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('MFI', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
 	protected function getMSHSegment($guid, $message_count, $timestamp, $seg_type, $sub_type, $client_name)
 	{
 		// Field Separator
 		$segment[] = array('MSH');

 		// F1 Encoding Characters
 		$segment[] = array('^~\&');

 		// F2 Sending Application: namespace id; universal id; universal id type
 		$segment[] = array(substr($this->vendor_name, 0, 50), substr($this->vendor_name, 0, 50), 'CSM');

 		// F3 Sending Facility
 		$segment[] = array(substr($client_name, 0, 50));

 		// F4 Receiving Application: namespace id; universal id; universal id type
 		$segment[] = array('OTHER', 'VENDOR', 'GUID');

 		// F5 Receiving Facility:
 		$segment[] = array();

 		// F6 Date/Time of Message
 		$segment[] = array($timestamp);

 		// F7 Security
 		$segment[] = array();

 		// F8 Message Type: Component 1; Component 2
 		$segment[] = array($seg_type, $sub_type);

 		// F9 Message Control ID
 		$segment[] = array($guid);

 		// F10 Processing ID
 		$segment[] = array('P');

 		// F11 Version ID
 		$segment[] = array($this->vendor_version);

 		// F12 Sequence Number
 		$segment[] = array();

 		// F13 Continuation Pointer
 		$segment[] = array();

 		// F14 Accept Acknowledgement Type
 		$segment[] = array();

 		// F15 Application Acknowledgement Type
 		$segment[] = array('NE');

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'MSH', $message_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegmentMSH($guid, $message_count, $segment);

 		return $segment;
 	}
 	protected function getNTESegment($guid, $msg_count, $appointment)
 	{
 		$segment[] = array('NTE');

 		// F1 Set ID
 		$segment[] = array(1);

 		// F2 Source Of Comment
 		$segment[] = array();

 		// F3 Comment
 		$segment[] = array(str_replace(array("\r\n", "\r", "\n"), '     ', $appointment->notes));

 		// F4 Comment Type
 		$segment[] = array();

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'NTE', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('NTE', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
 	protected function getPIDSegment($guid, $msg_count, $patient)
 	{
 		$segment[] = array('PID');

 		// F1 set ID
 		$segment[] = array('1');

 		// F2 Patient Id
 		$segment[] = array($patient->id);

 		// F3 Patient Identifier List
 		$segment[] = array($patient->id);

 		// F4 Alternate Patient ID - PID
 		$segment[] = array();

 		// F5 Patient Name
 		$segment[] = array(substr($patient->lname, 0, 35), substr($patient->fname, 0, 35));

 		// F6 Mother's Maiden Name
 		$segment[] = array();

 		// F7 Date/Time of Birth
 		$segment[] = array($patient->dob ? date('Ymd000000', hype::parseDateToInt($patient->dob, hype::DB_DATETIME_FORMAT)) : '');

 		// F8 Sex
 		$segment[] = array($patient->sex);

 		// F9 Patient Alias
 		$segment[] = array();

 		// F10 Race
 		$segment[] = array();

 		// F11 Patient Address
 		$segment[] = array(substr($patient->address, 0, 40), '', substr($patient->city, 0, 40), $patient->province, substr($patient->postal_code, 0, 40));

 		// F12 Country Code
 		$segment[] = array();

 		// F13 Phone Number - Home
 		$segment[] = array(substr($patient->hphone, 0, 20), '', substr($patient->email, 0, 20), '', '', '', substr($patient->cellphone, 0, 20));

 		// F14 Phone Number - Business
 		$segment[] = array(substr($patient->wphone, 0, 20));

 		// F15 primary language
 		$segment[] = array();

 		// F16 marital status
 		$segment[] = array();

 		// F17 religion
 		$segment[] = array();

 		// F18 patient account number
 		$hcn = $patient->getHCN();
 		/** 2012-03-19:
 		 * We decided to send the entire health card number through to our partners, despite the data not being
 		 * encrypted in the ezHL7 data tables or the HL7 messages generated on disk. We will address this issue
 		 * through strong firewalls and encrypted data transfers to our partners.
 		 */
 		$exp_year = $patient->hcn_exp_year ? date('Ymd000000', hype::parseDateToInt($patient->hcn_exp_year, hype::DB_DATETIME_FORMAT)) : '';
 		$segment[] = array($hcn, $patient->hcn_version_code, $exp_year);

 		// F19 ssn number - patient
 		$segment[] = array();

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'PID', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('PID', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
 	protected function getPRASegment($guid, $msg_count, $referring_doctor)
 	{
 		$segment[] = array('PRA');

 		// F1 primary key
 		$segment[] = array($referring_doctor->id);

 		// F2 practitioner group
 		$segment[] = array();

 		// F3 practitioner category
 		$segment[] = array();

 		// F4 provider billing
 		$segment[] = array();

 		// F5 specialty
 		$segment[] = array($referring_doctor->SpecCode->moh_spec_code);

 		// F6 practitioner id numbers
 		$segment[] = array($referring_doctor->provider_num);

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'PRA', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('PRA', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
 	protected function getPV1Segment($guid, $msg_count, $appointment, $attending_doctor, $referring_doctor)
 	{
 		$location_id = $location_name = '';
 		if ($appointment instanceof Appointment) {
 			$location_id = $appointment->location_id;
 			$location_name = $appointment->Location->name;
 		}

 		$doctor_id = $doctor_lname = $doctor_fname = '';
 		if ($attending_doctor instanceof Doctor) {
 			$doctor_id = $attending_doctor->id;
 			$doctor_lname = substr($attending_doctor->lname, 0, 35);
 			$doctor_fname = substr($attending_doctor->fname, 0, 35);
 		}

 		$ref_doc_id = $ref_doc_lname = $ref_doc_fname = $ref_doc_salutation = '';
 		if ($referring_doctor instanceof ReferringDoctor) {
 			$ref_doc_id = $referring_doctor->id;
 			$ref_doc_lname = substr($referring_doctor->lname, 0, 35);
 			$ref_doc_fname = substr($referring_doctor->fname, 0, 35);
 			$ref_doc_salutation = substr($referring_doctor->salutation, 0, 35);
 		}

 		$segment[] = array('PV1');

 		// F1 set ID
 		$segment[] = array('1');

 		// F2 Patient Class
 		$segment[] = array('O');

 		// F3 Assigned Patient Location
 		$segment[] = array($location_id, '', '', '', '', '', '', '', substr($location_name, 0, 20));

 		// F4 Admission Type
 		$segment[] = array();

 		// F5 Preadmit Number
 		$segment[] = array();

 		// F6 Prior Patient Location
 		$segment[] = array();

 		// F7 Attending Doctor
 		$segment[] = array($doctor_id, $doctor_lname, $doctor_fname);

 		// F8 Referring Doctor
 		$segment[] = array($ref_doc_id, $ref_doc_lname, $ref_doc_fname, '', '', $ref_doc_salutation);

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'PV1', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('PV1', 'A', $guid, $msg_count, $segment);
 		$this->db_queries[] = $this->generateInsertSegment('PV1', 'B', $guid, $msg_count, array());

 		return $segment;
 	}
 	protected function getRGSSegment($guid, $msg_count)
 	{
 		$segment[] = array('RGS');

 		// Set ID
 		$segment[] = array(1);

 		// Segment Action Code
 		$segment[] = array('A');

 		// Resouce Group ID

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'RGS', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('RGS', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
 	protected function getSCHSegment($guid, $msg_count, $subtype, $appointment)
 	{
 		$minutes = (hype::parseDateToInt($appointment->end_date, hype::DB_DATETIME_FORMAT) - hype::parseDateToInt($appointment->start_date, hype::DB_DATETIME_FORMAT)) / 60;

 		$segment[] = array('SCH');

 		// F1 Appointment ID
 		$segment[] = array($appointment->id);

 		// F2 Filler Appointment ID
 		$segment[] = array();

 		// F3 Occurrence Number
 		$segment[] = array();

 		// F4 Group Number
 		$segment[] = array();

 		// F5 Schedule ID
 		$segment[] = array();

 		// F6 Event Reason
 		$segment[] = array(substr($appointment->service_code, 0, 40));

 		// F7 Appointment Reason
 		$segment[] = array(substr($appointment->service_code, 0, 40), substr(str_replace(array("\r", "\n", "\r\n"), '   ', $appointment->notes), 0, 100));

 		// F8 Appointment Type
 		$segment[] = array(substr(AppointmentTable::getStatusString($appointment->status, true), 0, 40), substr($appointment->AppointmentType->name, 0, 40));

 		// F9 Appointment Duration
 		$segment[] = array($minutes);

 		// F10 Appointment Duration Units
 		$segment[] = array('M');

 		// F11 Appointment Timing Quantity
 		$segment[] = array('', '', substr(60 * $minutes, 0, 40), date('YmdHis', hype::parseDateToInt($appointment->start_date, hype::DB_DATETIME_FORMAT)), date('YmdHis', hype::parseDateToInt($appointment->end_date, hype::DB_DATETIME_FORMAT)));

 		// F12 Contact Person
 		$segment[] = array();

 		// F13 Contact Phone Number
 		$segment[] = array();

 		// F14 Contact Address
 		$segment[] = array();

 		// F15 Contact Location
 		$segment[] = array();

 		// F16 Filler Contact Person
 		$segment[] = array();

 		// F17 Filler Contact Phone Number
 		$segment[] = array();

 		// F18 Filler Contact Address
 		$segment[] = array();

 		// F19 Filler Contact Location
 		$segment[] = array($appointment->location_id);

 		// F20 Entered By Person
 		$segment[] = array();

 		// F21 Entered By Phone Number
 		$segment[] = array();

 		// F22 Entered By Location
 		$segment[] = array();

 		// F23 Parent Appointment ID
 		$segment[] = array();

 		// F24 Parent Filler Appointment ID
 		$segment[] = array();

 		// F25 Filler Status Code
 		$segment[] = array(substr($appointment->stage_client_id ? $appointment->StageClient->stage_name : '', 0, 40));

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'SCH', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('SCH', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
 	protected function getSTFSegment($guid, $msg_count, $refdoc)
 	{
 		$segment[] = array('STF');

 		// F1 primary key value
 		$segment[] = array($refdoc->id);

 		// F2 staff id code
 		$segment[] = array($refdoc->id);

 		// F3 staff name
 		$segment[] = array(substr($refdoc->lname, 0, 35), substr($refdoc->fname, 0, 35));

 		// F4 staff type
 		$segment[] = array('RD');

 		// F5 sex
 		$segment[] = array();

 		// F6 date time of birth
 		$segment[] = array();

 		// F7 active/inactive
 		$segment[] = array($refdoc->active ? 'A' : 'I');

 		// F8 department
 		$segment[] = array();

 		// F9 hospital service
 		$segment[] = array();

 		$address = null;
 		if (count($refdoc->ReferringDoctorAddress)) {
 			foreach ($refdoc->ReferringDoctorAddress as $rda) {
 				if (!$address && $rda->active) {
 					$address = $rda;
 					break;
 				}
 			}
 		}

 		if ($address instanceof ReferringDoctorAddress) {
 			// F10 phone
 			$segment[] = array(substr($rda->phone, 0, 20), 'PH', substr($rda->fax, 0, 20), 'FX');

 			// F11 office/home address
 			$segment[] = array(substr($rda->address, 0, 40), substr($rda->address2, 0, 40), substr($rda->city, 0, 40), substr($rda->province, 0, 40), substr($rda->postal_code, 0, 40));

 			// F12 institution activation date
 			$segment[] = array();

 			// F13 institution inactivation date
 			$segment[] = array();

 			// F14 backup person id
 			$segment[] = array();

 			// F15 email address
 			$segment[] = array(substr($rda->email, 0, 40));

 			// F16 preferred method of contact
 			$segment[] = array();

 			// F17 marital status
 			$segment[] = array();

 			// F18 job title
 			$segment[] = array('MD');
 		}

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'STF', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('STF', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
 	public static function create($partner_name)
 	{
 		if (class_exists('HL7OutboundProcessor' . $partner_name))
 		{
 			$hL7Class = 'HL7OutboundProcessor'. $partner_name;
 			$processor = new $hL7Class();
 		}
 		return $processor;
 	}
 	public static function getSIUSubtype($status, $is_new)
 	{
 		if ($is_new) {
 			return 'S12';
 		}
 		else if ($status == AppointmentTable::STATUS_DELETED) {
 			return 'S15';
 		}
 		else if ($status == AppointmentTable::STATUS_CANCELLED) {
 			return 'S15';
 		}
 		else if ($status == AppointmentTable::STATUS_CANCELLED_BY_LOCATION) {
 			return 'S15';
 		}
 		else if ($status == AppointmentTable::STATUS_RESCHEDULED) {
 			return 'S13';
 		}
 		return 'S14';
 	}
 	public static function getADTSubtype($deleted, $is_new)
 	{
 		if ($is_new) {
 			return 'A04';
 		}
 		else if ($deleted) {
 			return 'A23';
 		}
 		return 'A08';
 	}
 	public static function getMFNSubtype($is_new)
 	{
 		return 'M02';
 	}
 	public static function deferADT($patient, $is_new, $partner)
 	{
 		$hl7_queue = new Hl7Queue();
 		$hl7_queue->client_id = $patient->client_id;
 		$hl7_queue->version_number = $patient->version;
 		$hl7_queue->message_type = 'ADT';
 		$hl7_queue->message_subtype = self::getADTSubtype($patient->deleted, $is_new);
 		$hl7_queue->model_id = $patient->id;
 		$hl7_queue->model_type = 'PATIENT';
 		$hl7_queue->status = HL7QueueTable::STATUS_UNPROCESSED;
 		$hl7_queue->partner_name = $partner;

 		$hl7_queue->save();
 	}
 	public static function deferMFN($referring_doctor, $is_new, $partner)
 	{
 		$hl7_queue = new Hl7Queue();
 		$hl7_queue->client_id = $referring_doctor->client_id;
 		$hl7_queue->version_number = $referring_doctor->version;
 		$hl7_queue->message_type = 'MFN';
 		$hl7_queue->message_subtype = self::getMFNSubtype($is_new);
 		$hl7_queue->model_id = $referring_doctor->id;
 		$hl7_queue->model_type = 'REFDOC';
 		$hl7_queue->status = HL7QueueTable::STATUS_UNPROCESSED;
 		$hl7_queue->partner_name = $partner;

 		$hl7_queue->save();
 	}
 	public static function deferSIU($attendee, $is_new, $partner)
 	{
 		$hl7_queue = new Hl7Queue();
 		$hl7_queue->client_id = $attendee->Appointment->client_id;
 		$hl7_queue->version_number = $attendee->version;
 		$hl7_queue->message_type = 'SIU';
 		$hl7_queue->message_subtype = self::getSIUSubtype($attendee->Appointment->status, $is_new);
 		$hl7_queue->model_id = $attendee->id;
 		$hl7_queue->model_type = 'APPT';
 		$hl7_queue->status = HL7QueueTable::STATUS_UNPROCESSED;
 		$hl7_queue->partner_name = $partner;

 		$hl7_queue->save();
 	}
}
