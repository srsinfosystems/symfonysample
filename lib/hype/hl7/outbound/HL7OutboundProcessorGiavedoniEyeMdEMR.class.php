<?php

class HL7OutboundProcessorGiavedoniEyeMdEMR extends HL7OutboundProcessorEyeMdEMR
{
	public function processMFN($referring_doctor, $message_count)
	{
		if ($this->hl7_queue) {
			$this->hl7_queue->status = HL7QueueTable::STATUS_PROCESSED;
			$this->hl7_queue->save();
		}
	}
	protected function getPIDSegment($guid, $msg_count, $patient)
 	{
 		$segment[] = array('PID');

 		// F1 set ID
 		$segment[] = array('1');

 		// F2 Patient Id
 		$segment[] = array($patient->id);

 		// F3 Chart Number
 		$segment[] = array($patient->getPatientNumber());

 		// F4 Alternate Patient ID - PID
 		$segment[] = array($patient->getPatientNumber());

 		// F5 Patient Name
 		$segment[] = array(substr($patient->lname, 0, 35), substr($patient->fname, 0, 35));

 		// F6 Mother's Maiden Name
 		$segment[] = array();

 		// F7 Date/Time of Birth
 		$segment[] = array($patient->dob ? date('Ymd000000', hype::parseDateToInt($patient->dob, hype::DB_DATETIME_FORMAT)) : '');

 		// F8 Sex
 		$segment[] = array($patient->sex);

 		// F9 Patient Alias
 		$segment[] = array();

 		// F10 Race
 		$segment[] = array();

 		// F11 Patient Address
 		$segment[] = array(substr($patient->address, 0, 40), '', substr($patient->city, 0, 40), $patient->province, substr($patient->postal_code, 0, 40));

 		// F12 Country Code
 		$segment[] = array();

 		// F13 Phone Number - Home
 		$segment[] = array(substr($patient->hphone, 0, 10), '', '', substr($patient->email, 0, 19), '', '', substr($patient->cellphone, 0, 10));

 		// F14 Phone Number - Business
 		$segment[] = array(substr($patient->wphone, 0, 10), '', '', '', '', '', '', substr($patient->wphone, 10));

 		// F15 primary language
 		$segment[] = array();

 		// F16 marital status
 		$segment[] = array();

 		// F17 religion
 		$segment[] = array();

 		$segment[] = array();

 		// F18 patient account number
 		$hcn = $patient->getHCN();
 		//$exp_year = $patient->hcn_exp_year ? date('Ymd000000', hype::parseDateToInt($patient->hcn_exp_year, hype::DB_DATETIME_FORMAT)) : '';
 		$segment[] = array($hcn . ' ' . $patient->hcn_version_code);//, $exp_year);

 		// F19 ssn number - patient
 		$segment[] = array();

 		$this->db_queries[] = $this->generateInsertMessageManifest($guid, 'PID', $msg_count, $this->buildHL7Segment($segment));
 		$this->db_queries[] = $this->generateInsertSegment('PID', 'A', $guid, $msg_count, $segment);

 		return $segment;
 	}
}
