<?php

class HL7InboundVamsHalabiISSA extends HL7InboundISSA
{
 	protected function determineGroupNumber($serviceCode, $locationCode)
 	{
 		$this->logMessage('determineGroupNumber(' . $serviceCode . ', ' .  $locationCode . ') called.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$rs = null;
 		$mode = $serviceCode[strlen($serviceCode) - 1];

 		$exceptions = array(
 			'X184B',
 			'X185B',	
 			'X194B',
 			'X192B',
 			'X201B',
 			'X178B',
 		);
		
 		if ($mode == 'A') {
 			$rs = '0000';
 		}
 		else if ($mode == 'C') {
 			$rs = '0000';
 		}
 		else if ($locationCode == 'Y02') {
 			$rs = 'A4BC';
 			if (in_array($serviceCode, $exceptions)) {
 				$rs = 'A4BD';
 			}
 		}
 		else if ($locationCode == 'L01') {
 			$rs = 'A4BJ';
 			if (in_array($serviceCode, $exceptions)) {
 				$rs = 'A4BB';
 			}
 		}
		
 		$this->indent += 2;
 		$this->logMessage('Group Number determined to be ' . $rs, HL7InboundBase::LOG_LEVEL_MINIMUM);
 		$this->indent -= 2;

 		return $rs;
 	}
}