<?php

class HL7InboundEchobase extends HL7InboundBase
{

	private $serviceCodes = null;

	// AIL 3.4 - HYPE Location ID
	// AIL 3.9 - Location Name

	// AIP 3.1 - HYPE Doctor ID
	// AIP 3.2 - Last Name
	// AIP 3.3 - First Name
	// AIP 3.6 - Doctor Prefix

	// PID 2.1 - HYPE Patient ID
	// PID 3.1 - unknown
	// PID 5.1 - Last Name
	// PID 5.2 - First Name
	// PID 5.3 - Middle Name [currenlty unsupported]
	// PID 5.4 - Suffix [currenlty unsupported]
	// PID 5.5 - Prefix [currenlty unsupported]
	// PID 7.1 - Date Of Birth [Ymdhi]
	// PID 8.1 - Sex
	// PID 11.1 - Address
	// PID 11.2 - Street Address 2 - APPENDED TO ADDRESS - no Address 2 line
	// PID 11.3 - City
	// PID 11.4 - Province
	// PID 11.5 - Postal Code
	// PID 13.1 - Phone Number [Home]
	// PID 13.3 - Email
	// PID 13.7 - Phone Number [Cell]
	// PID 14.1 - Phone Number [Work]
	// PID 18.1 - HCN
	// PID 18.2 - Version Code
	// PID  18.3 - Expiry Date [Ymdhi]

	// SCH 1.1 - HYPE APPOINTMENT ID
	// SCH 8.1 - Appointment Status [Text String]
	// SCH 8.2 - Appointment Type [Text String]
	// SCH 11.4 - Start Date [Ymdhis]
	// SCH 11.5 - End Date [Ymdhis]

	public function processADT()
	{
 		$this->logMessage('processADT() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$patient = $this->handlePID();
 		// we ignore PV1 with Echobase ADTs

		$this->logMessage('Saving patient record', HL7InboundBase::LOG_LEVEL_MINIMUM);
		$patient->save();
		$this->logMessage('Patient ID: ' . $patient->id, HL7InboundBase::LOG_LEVEL_MINIMUM);

 		$this->setProcessed(HL7InboundBase::STATUS_PROCESSED);
 		$this->hl7Inbound->setStatus(HL7InboundTable::STATUS_PROCESSED);
 		$this->logMessage('Processing is complete. Updated HL7 Message to staus ' . HL7InboundBase::STATUS_PROCESSED, HL7InboundBase::LOG_LEVEL_MINIMUM);

 		$this->indent -= 2;
	}
	public function processDFT()
	{
		$this->logMessage('processDFT() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
		$this->indent += 2;
		$this->logMessage('Processing DFT Message', HL7InboundBase::LOG_LEVEL_MINIMUM);

 		$this->serviceCodes = new Doctrine_Collection('ServiceCode');

 		$claim = new Claim();
 		$patient = $this->handlePID();
 		$location = $this->getLocation();
 		$doctor = null;

		if (!$location instanceof Location) {
			$this->is_valid = false;
			$this->logMessage('The location ID is invalid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_LOCATION, 'The location ID is invalid.');
			$location = new Location();
		}
		else {
			if (!$patient['location_id']) {
				$patient['location_id'] = $location['id'];
			}
			$claim->location_id = $location['id'];
			$claim->Location = $location;

			$this->hl7Inbound->setLocation($location);

			$this->logMessage('Location of service: ' . $location->code, HL7InboundBase::LOG_LEVEL_MINIMUM);
		}

		$this->logMessage('Setting Patient Details to claim.', HL7InboundBase::LOG_LEVEL_ALL);
		$output = $claim->setPatientDetails($patient, true);

		$this->hl7Inbound
		->setPatient($patient)
		->setFname($patient->getFname())
		->setLname($patient->getLname());

		foreach ($output as $line)
		{
			$this->logMessage($line, HL7InboundBase::LOG_LEVEL_ALL);
		}

		$ft1s = $this->getFT1();
		$pid = $this->getPID();
		$pv1a = $this->getPV1();
		$pv1b = $this->getPV1B();

		$referring_doctor = $this->getReferringDoctor($pv1a, $pv1b);
		if ($referring_doctor instanceof ReferringDoctor)
		{
			$claim->ref_doc = $referring_doctor->provider_num;
			$claim->ref_doc_id = $referring_doctor->id;
			$claim->ref_doc_addr_id = $referring_doctor->ReferringDoctorAddress[0]->id;

			$this->logMessage('Set claim refDoc num to ' . $claim->ref_doc, HL7InboundBase::LOG_LEVEL_ALL);
		}

		$doctor = $this->getDoctorById($pv1a['PV1_F7_C1']);
		if ($doctor instanceof Doctor) {
			$provider_number = $doctor->getProviderNum();
			$this->logMessage('Provider Number is ' . $provider_number, HL7InboundBase::LOG_LEVEL_MINIMUM);
		}
		else {
			$this->is_valid = false;
			$this->logMessage('No provider number provided', HL7InboundBase::LOG_LEVEL_ALL);
			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_DOCTOR, 'No provider number provided');
		}

		foreach ($ft1s as $ft1)
		{
			$claim = $this->handleFT1($claim, $ft1);
		}

		if (!$doctor instanceof Doctor) {
			$this->is_valid = false;
			$this->logMessage('No doctor provided', HL7InboundBase::LOG_LEVEL_ALL);
			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_GROUP_NUMBER, 'No doctor provided');
		}
		else {
			$this->hl7Inbound->setDoctorId($doctor->id);
			$claim->Doctor = $doctor;
			$claim->doctor_id = $doctor->id;
			$claim->doctor_code = $doctor->qc_doctor_code;

			$this->logMessage('Set claim doctor code to ' . $claim->doctor_code, HL7InboundBase::LOG_LEVEL_ALL);
		}


		foreach ($claim->ClaimItem as $key => $claimItem)
		{
			$mode = $claimItem->service_code[strlen($claimItem->service_code) - 1];
			if ($doctor instanceof Doctor) {
				$claimItem->setBaseFee($this->serviceCodes[$key]->determineFee($mode, $doctor->spec_code_id));

			}
			else {
				$claimItem->getBaseFee($this->serviceCodes[$key]->determineFee($mode));
			}
			$claimItem->fee_subm = $claimItem->num_serv * $claimItem->getBaseFee();

			$this->logMessage('Set claim item ' . $key . ' fee_subm to ' . $claimItem->fee_subm, HL7InboundBase::LOG_LEVEL_ALL);
			$this->logMessage('Set claim item ' . $key . ' base_fee to ' . $claimItem->getBaseFee(), HL7InboundBase::LOG_LEVEL_ALL);

			if (!$this->duplicateServicesCheck($claimItem)) {
				unset($claim->ClaimItem[$key]);
				if (!count($claim->ClaimItem)) {
					$this->is_valid = false;
					$this->save_temporary = false;
				}
			}
		}

		$claim->batch_number = ClaimTable::EMPTY_BATCH;
		$claim->date_created = date('Y-m-d H:i:s');
		$claim->manual_review = false;

		if (!$claim->health_num && $claim->pay_prog == ClaimTable::CLAIM_TYPE_HCPP) {
			$this->is_valid = false;
			$this->logMessage('A health card number must be provided for this claim to be valid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_HEALTH_CARD, 'A health card number must be provided for this claim to be valid');
		}

		// TODO: sli -> ignore for now

		if (!$this->is_valid && $this->save_temporary) {
			$temporary_form = TemporaryFormTable::createHL7Claim($claim);
			$set_processed = $this->setProcessed(HL7InboundBase::STATUS_FORM_ERROR);
			$this->hl7Inbound->setStatus(HL7InboundTable::STATUS_FORM_ERROR);
			$this->hl7Inbound->setTemporaryFormId($temporary_form->id);

			$this->logMessage('Claim saved in temporary tables due to invalid data', HL7InboundBase::LOG_LEVEL_MINIMUM);
			$this->logMessage('Processing is complete. Updated HL7 Message to staus ' . HL7InboundBase::STATUS_FORM_ERROR, HL7InboundBase::LOG_LEVEL_MINIMUM);
		}
		else if (!$this->is_valid) {
			$this->hl7Inbound->setStatus(HL7InboundTable::STATUS_DUPLICATE);
			$set_processed = $this->setProcessed(HL7InboundBase::STATUS_DUPLICATE);

			$this->logMessage('Claim already handled - data ignored', HL7InboundBase::LOG_LEVEL_MINIMUM);
			$this->logMessage('Processing is complete. Updated HL7 Message to staus ' . HL7InboundBase::STATUS_DUPLICATE, HL7InboundBase::LOG_LEVEL_MINIMUM);
		}
		else {
			$claim->save();

			$this->hl7Inbound
			->setStatus(HL7InboundTable::STATUS_PROCESSED)
			->setClaimId($claim->getId());

			$set_processed = $this->setProcessed(HL7InboundBase::STATUS_PROCESSED); //after claim is saved --> set processed  to 1

			$this->logMessage('Claim saved. Claim id: ' . $claim->id, HL7InboundBase::LOG_LEVEL_MINIMUM);
			$this->logMessage('Processing is complete. Updated HL7 Message to staus ' . HL7InboundBase::STATUS_PROCESSED, HL7InboundBase::LOG_LEVEL_MINIMUM);
		}
		$this->indent -= 2;
	}
	public function processSIU()
	{
		$this->logMessage('processSIU() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
		$this->indent += 2;

		$sch = $this->getSCH();
		$patient = $this->handlePID();
		$ail = $this->getAIL();
		$aip = $this->getAIP();

		$attendee = $this->handleSCH($sch);

		$this->updateAttendeeFromPatient($attendee, $patient);

		// SCH 8.1 - Appointment Status [Text String]
		if ($sch['SCH_F8_C1']) {
			$appointment_status = AppointmentTable::getStatusFromString($sch['SCH_F8_C1']);
			if ($appointment_status) {
				$attendee->Appointment->setStatus($appointment_status);
				$this->logMessage('Set Appointment Status to ' . $appointment_status . ' [' . $sch['SCH_F8_C1'] . ']', HL7InboundBase::LOG_LEVEL_ALL);
			}
		}

		// SCH 8.2 - Appointment Type [Text String]
		if ($sch['SCH_F8_C2']) {
			$appointment_type = $this->getAppointmentTypeByName($sch['SCH_F8_C2']);

			if ($appointment_type instanceof AppointmentType) {
				$attendee->Appointment->setAppointmentType($appointment_type);
				$this->logMessage('Set Appointment Type to ' . $appointment_type->getName() . ' [ID: ' . $appointment_type->getId() . ']', HL7InboundBase::LOG_LEVEL_ALL);
			}
		}

		// SCH 11.4 - Start Date [Ymdhis]
		if ($sch['SCH_F11_C4']) {
			$date = date(hype::DB_ISO_DATE, hype::parseDateToInt(substr($sch['SCH_F11_C4'], 0, 14), 'YmdHis'));
			$attendee->Appointment->setStartDate($date);
			$this->logMessage('Set Appointment Start Date to ' . $attendee->Appointment->getStartDate(), HL7InboundBase::LOG_LEVEL_ALL);
		}

		// SCH 11.5 - End Date [Ymdhis]
		if ($sch['SCH_F11_C5']) {
			$date = date(hype::DB_ISO_DATE, hype::parseDateToInt(substr($sch['SCH_F11_C5'], 0, 14), 'YmdHis'));
			$attendee->Appointment->setEndDate($date);
			$this->logMessage('Set Appointment End Date to ' . $attendee->Appointment->getEndDate(), HL7InboundBase::LOG_LEVEL_ALL);
		}

		$location = $this->getLocationByName($ail['AIL_F3_C9']);
		if ($location instanceof Location) {
			$attendee->Appointment->Location = $location;
		}

		$doctor = $this->getDoctorByName($aip['AIP_F3_C2'], $aip['AIP_F3_C3']);
		if ($doctor instanceof Doctor) {
			$attendee->Appointment->setDoctorCode($doctor->getQcDoctorCode());
			$attendee->Appointment->setDoctorId($doctor->getId());
		}
		$attendee->save();

		$this->setProcessed(HL7InboundBase::STATUS_PROCESSED);
		$this->hl7Inbound->setStatus(HL7InboundTable::STATUS_PROCESSED);
		$this->logMessage('Processing is complete. Updated HL7 Message to staus ' . HL7InboundBase::STATUS_PROCESSED, HL7InboundBase::LOG_LEVEL_MINIMUM);

		$this->indent -= 2;
	}

	private function createReferringDoctor()
	{
		$this->logMessage('createReferringDoctor() called.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		$refdoc = new ReferringDoctor();
		$refdoc->client_id = $this->client_id;
		$refdoc->active = true;
		$refdoc->spec_code = $this->default_refdoc_spec_code;

		$rda = new ReferringDoctorAddress();
		$rda->active = true;

		$refdoc->ReferringDoctorAddress->add($rda);

		return $refdoc;
	}

	private function getLocation()
	{
		$this->logMessage('getLocation() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
		$pv1_record = $this->getPV1();

		return $this->getLocationByName($pv1_record['PV1_F3_C4']);
	}
	private function getPatient($hypeID, $echobaseID, $pid)
	{
 		$this->logMessage('getPatient(' . $hypeID . ', ' . $echobaseID . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;
 		$patient = null;

 		if ($hypeID) {
 			$patient = $this->getPatientFromHypeId($hypeID);
 		}
 		if ($patient instanceof Patient) {
 			$this->logMessage('Patient found with HYPE ID ' . $patient->getId() . ' (' . $patient->getFname() . ' ' . $patient->getLname() . ')', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->indent -= 2;
 			return $patient;
 		}

 		// TODO: search by Echobase ID

 		$fname = $pid['PID_F5_C2'];
 		$lname = $pid['PID_F5_C1'];
		$dob = null;
 		if ($pid['PID_F7_C1']) {
 			$dob = date('Y-m-d', hype::parseDateToInt($pid['PID_F7_C1']), 'Ymdhi');
 		}

 		if ($fname && $lname && $dob) {
			$patient = $this->getPatientFromNameAndDob($fname, $lname, $dob);
		}
 		if ($patient instanceof Patient) {
 			$this->logMessage('Patient found with HYPE ID ' . $patient->getId() . ' (' . $patient->getFname() . ' ' . $patient->getLname() . ')', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->indent -= 2;
 			return $patient;
 		}

 		// TODO: search by health card number

 		$this->indent -= 2;
 		return $this->createNewPatient();
	}
	private function getReferringDoctor($pv1, $pv1b)
	{
		$this->logMessage('getReferringDoctor() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		if (!($pv1['PV1_F8_C3'] && $pv1['PV1_F8_C2'])) {
			return;
		}
		$this->indent += 2;
		$this->logMessage('find Referring Doctor record for ' . $pv1['PV1_F8_C3'] . ' ' . $pv1['PV1_F8_C2'], HL7InboundBase::LOG_LEVEL_MINIMUM);

		$refdoc = Doctrine_Query::create()
			->from('ReferringDoctor d')
			->addWhere('d.client_id = (?)', $this->client_id)
			->addWhere('d.fname = (?)', $pv1['PV1_F8_C3'])
			->addWhere('d.lname = (?)', $pv1['PV1_F8_C2'])
			->fetchOne();

		if (!$refdoc instanceof ReferringDoctor) {
			$refdoc = $this->createReferringDoctor();
		}
		if ($refdoc instanceof ReferringDoctor) {
			$refdoc = $this->updateReferringDoctor($refdoc, $pv1, $pv1b);
		}

		$this->logMessage('Saving refdoc record', HL7InboundBase::LOG_LEVEL_MINIMUM);
		$refdoc->save();
		$this->logMessage('Ref Doc ID: ' . $refdoc->id, HL7InboundBase::LOG_LEVEL_MINIMUM);

		$this->indent -= 2;
		return $refdoc;
	}
	private function handleFT1($claim, $ft1)
	{
 		$this->logMessage('handleFT1() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$req_ref_doc = $req_facility_num = $req_admit_date = $interval = $req_diag_code = false;

 		$item = new ClaimItem();
 		$item->client_id = $this->client_id;

 		$service_code = $this->getServiceCode($ft1['FT1_F25_C1']);
 		$diag_code = $this->getDiagnosticCode($ft1['FT1_F19_C1']);

		if (!$service_code instanceof ServiceCode) {
			$this->is_valid = false;
			$this->logMessage('Service code ' .  $service_code . ' is invalid', HL7InboundBase::LOG_LEVEL_MINIMUM);
			$this->addErrorMessage(HL7InboundTable::ERROR_INVALID_SERVICE_CODE, 'Service code ' . $service_code . ' is invalid');
		}
		else {
			$req_diag_code = $service_code['req_diag_code'];
			$req_ref_doc = $service_code['req_ref_doctor'];
			$req_facility_num = $service_code['req_facility_num'];
			$req_admit_date = $service_code['req_admit_date'];
			$interval = $service_code['inter_val'];

			$mode = 'A';
			if (substr($ft1['FT1_F25_C1'], 4)) {
				$mode = $ft1['FT1_F25_C1'][4];
			}

			$item->setServiceCode($ft1['FT1_F25_C1'] . $mode);
			$this->serviceCodes[count($claim->ClaimItem)] = $service_code;

			$this->logMessage('Service Code set to ' . $item->service_code, HL7InboundBase::LOG_LEVEL_ALL);
		}

		$date = trim($ft1['FT1_F4_C1']);
		if ($date) {
			$item->service_date = substr($date,0,4) . "-" . substr($date,4,2) . "-" . substr($date,6,2) . " 00:00:00";
			$this->logMessage('Set service date to ' . $item->service_date, HL7InboundBase::LOG_LEVEL_ALL);
		}
		else {
			$this->is_valid = false;
			$this->logMessage('Missing service date', HL7InboundBase::LOG_LEVEL_MINIMUM);
			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'Service Date required.');
		}
		$item->num_serv = trim($ft1['FT1_F10_C1']) ? trim($ft1['FT1_F10_C1']) : 1;
		$item->setModifier($item->getNumServ() * 100);
		$this->logMessage('Set num_serv to ' . $item->num_serv, HL7InboundBase::LOG_LEVEL_ALL);
		$this->logMessage('Set modifier to ' . $item->getModifier(), HL7InboundBase::LOG_LEVEL_ALL);

		if ($diag_code instanceof DiagCode) {
			$item->diag_code = $diag_code->diag_code;
			$this->logMessage('Set diag code to ' . $item->diag_code, HL7InboundBase::LOG_LEVEL_ALL);
		}

		if (!$claim->pay_prog) {
			$claim->pay_prog = $ft1['FT1_F18_C1'] ? $ft1['FT1_F18_C1'] : $this->default_pay_prog;
		}
		$item->item_id = count($claim->ClaimItem) + 1;
		$item->status = ClaimItemTable::STATUS_UNSUBMITTED;
		$this->logMessage('Set claim pay prog to ' . $claim->pay_prog, HL7InboundBase::LOG_LEVEL_ALL);
		$this->logMessage('Item number is ' . $item->item_id, HL7InboundBase::LOG_LEVEL_ALL);

		$claim->ClaimItem->add($item);

		if ($req_diag_code && !$item->diag_code) {
			$this->is_valid = false;
			$this->logMessage('Service Code ' . $item->service_code . ' requires a diagnostic code', HL7InboundBase::LOG_LEVEL_MINIMUM);
			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'Diagnositc Code required');
		}
		if ($req_ref_doc && !$claim->ref_doc) {
			$this->is_valid = false;
			$this->logMessage('Service Code ' . $item->service_code . ' requires a referring doctor', HL7InboundBase::LOG_LEVEL_MINIMUM);
			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'Referring Doctor required');
		}
		if ($req_facility_num && !$claim->facility_num) {
			$this->is_valid = false;
			$this->logMessage('Service Code ' . $item->service_code . ' requires a facility number', HL7InboundBase::LOG_LEVEL_MINIMUM);
			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'Facility Number required');
		}
		if ($req_admit_date && !$claim->admit_date) {
			$this->is_valid = false;
			$this->logMessage('Service Code ' . $item->service_code . ' requires an admit date', HL7InboundBase::LOG_LEVEL_MINIMUM);
			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'Admit Date required');
		}
		$this->indent -= 2;

		return $claim;
	}
	private function handlePID()
	{
 		$this->logMessage('handlePID() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$pid = $this->getPID();

		$hypeId = intval($pid['PID_F2_C1']) ? $pid['PID_F2_C1'] : null;
		$echobaseID = null;
		// TODO - handle new patients coming from Echobase without a HYPE Patient ID

 		$patient = $this->getPatient($hypeId, $echobaseID, $pid);
 		$patient = $this->updatePatient($patient, $pid);

 		$this->indent -= 2;
 		return $patient;
	}
	private function handleSCH($sch)
	{
		$this->logMessage('handleSCH() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
		$this->indent += 2;

		// SCH 1.1 - HYPE APPOINTMENT ID
		$hypeID = $sch['SCH_F1_C1'] ? $sch['SCH_F1_C1'] : null;
		$echobaseID = null;

		$attendee = $this->getAttendeeByHypeID($hypeID);

		if (!$attendee instanceof AppointmentAttendee) {
			// TODO: get attendee another way
		}

		$this->indent -= 2;
		return $attendee;
	}
	private function updateAttendeeFromPatient(AppointmentAttendee $attendee, Patient $patient)
	{
		$this->logMessage('updateAttendeeFromPatient() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		$attendee->setPatientLname($patient->getLname());
		$attendee->setPatientFname($patient->getFname());
		$attendee->setPatientPhone($patient->getHphone());
		$attendee->setPatientEmail($patient->getEmail());
		$attendee->setPatientID($patient->getId());

		return $attendee;
	}
	private function updatePatient(Patient $patient, $pid)
	{
		$this->logMessage('updatePatient() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
		$this->indent += 2;

		// PID 5.1 - Last Name
		if ($pid['PID_F5_C1']) {
			$this->logMessage('Set patient lname to ' . $pid['PID_F5_C1'], HL7InboundBase::LOG_LEVEL_ALL);
			$patient->setLname($pid['PID_F5_C1']);
		}

		// PID 5.2 - First Name
		if ($pid['PID_F5_C2']) {
			$this->logMessage('Set patient fname to ' . $pid['PID_F5_C2'], HL7InboundBase::LOG_LEVEL_ALL);
			$patient->setFname($pid['PID_F5_C2']);
		}

 		// PID 7.1 - DOB [YYYMMDD]
		if ($pid['PID_F7_C1']) {
			$date = date('Y-m-d', hype::parseDateToInt(substr($pid['PID_F7_C1'], 0, 8), 'Ymd'));

 			$this->logMessage('Set patient dob to ' . $date, HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setDob($date);
		}

		// PID 8.1 - Sex [M|F|U] [U will not be mapped]
		if ($pid['PID_F8_C1'] == 'M' || $pid['PID_F8_C1'] == 'F') {
			$this->logMessage('Set patient sex to ' . $pid['PID_F8_C1'], HL7InboundBase::LOG_LEVEL_ALL);
			$patient->setSex($pid['PID_F8_C1']);
		}

 		// PID 11.1 - Street Address
 		if ($pid['PID_F11_C1']) {
 			$this->logMessage('Set patient address to ' . $pid['PID_F11_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setAddress($pid['PID_F11_C1']);
 		}

		// PID 11.2 - Street Address 2 - APPENDED TO ADDRESS - no Address 2 line
		if ($pid['PID_F11_C2']) {
			$this->logMessage('Add to patient address ' . $pid['PID_F11_C2'], HL7InboundBase::LOG_LEVEL_ALL);
			$patient->setAddress($patient->getAddress() . ' ' . $pid['PID_F11_C2']);
		}

		// PID 11.3 - City
		if ($pid['PID_F11_C3']) {
			$this->logMessage('Set patient city to ' . $pid['PID_F11_C3'], HL7InboundBase::LOG_LEVEL_ALL);
			$patient->setCity($pid['PID_F11_C3']);
		}

		// PID 11.4 - Province
		if ($pid['PID_F11_C4']) {
			$patient->setProvince($pid['PID_F11_C4']);
			$this->logMessage('Set patient province to ' . $patient->province, HL7InboundBase::LOG_LEVEL_ALL);
		}

		// PID 11.5 - Postal [Zip] Code
		if ($pid['PID_F11_C5']) {
			$this->logMessage('Set patient postal code to ' . $pid['PID_F11_C5'], HL7InboundBase::LOG_LEVEL_ALL);
			$patient->setPostalCode($pid['PID_F11_C5']);
		}

		// PID 13.1 - Home Phone
		if ($pid['PID_F13_C1']) {
			$this->logMessage('Set patient home phone to ' . $pid['PID_F13_C1'], HL7InboundBase::LOG_LEVEL_ALL);
			$patient->setHphone($pid['PID_F13_C1']);
		}

		// PID 13.3 - Email Address
		if ($pid['PID_F13_C3']) {
			$this->logMessage('Set patient email to ' . $pid['PID_F13_C3'], HL7InboundBase::LOG_LEVEL_ALL);
			$patient->setEmail($pid['PID_F13_C3']);
		}

		// PID 13.7 - Cell Phone
		if ($pid['PID_F13_C7']) {
			$this->logMessage('Set patient cell phone to ' . $pid['PID_F13_C7'], HL7InboundBase::LOG_LEVEL_ALL);
			$patient->setCellphone($pid['PID_F13_C7']);
		}

		// PID 14.1 - Work Phone
		if ($pid['PID_F14_C1']) {
			$this->logMessage('Set patient work phone to ' . $pid['PID_F14_C1'], HL7InboundBase::LOG_LEVEL_ALL);
			$patient->setWphone($pid['PID_F14_C1']);
		}

		// Patient Status
		if (!$patient->patient_status_id) {
			$cp = Doctrine::getTable('ClientParam')->getParamForClient($this->client_id, 'patient_status_default');
			if ($cp instanceof ClientParam) {
				$this->logMessage('Set patient status ID to ' . $cp->value, HL7InboundBase::LOG_LEVEL_ALL);
				$patient->patient_status_id = $cp->value;
			}
		}

		// Patient Type
		if (!$patient->patient_type_id) {
			$cp = Doctrine::getTable('ClientParam')->getParamForClient($this->client_id, 'default_patient_type_id');
			if ($cp instanceof ClientParam) {
				$this->logMessage('Set patient type ID to ' . $cp->value, HL7InboundBase::LOG_LEVEL_ALL);
				$patient->patient_type_id = $cp->value;
			}
		}

		// PID 18.1 Health Card Number
		if (intval($pid['PID_F18_C1'])) {
			$patient->setHcnNum($pid['PID_F18_C1']);
			// PID 18.2 Version Code
			$patient->setHcnVersionCode($pid['PID_F18_C2']);
			// PID 18.3 Expiry Date (Ymd000000)
			if ($pid['PID_F18_C3']) {
				$patient->setHcnExpYear(date('Y-m-d', hype::parseDateToInt(substr($pid['PID_F18_C3'], 0, 8), 'Ymd')));
			}
		}

		// TODO: primary Location
		// TODO: moh province

		$this->indent -= 2;
		return $patient;
	}

	private function updateReferringDoctor($refdoc, $pv1, $pv1b)
	{
		$this->logMessage('updateReferringDoctor() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
		$this->indent += 2;

		if ($pv1['PV1_F8_C1']) {
			$this->logMessage('Set refdoc provider number to ' . $pv1['PV1_F8_C1'], HL7InboundBase::LOG_LEVEL_ALL);
			$refdoc->provider_num = $pv1['PV1_F8_C1'];
		}

		if ($pv1['PV1_F8_C5']) {
			$this->logMessage('Set refdoc spec code to ' . $pv1['PV1_F8_C5'], HL7InboundBase::LOG_LEVEL_ALL);
			$refdoc->spec_code = $pv1['PV1_F8_C5'];
		}

		if ($refdoc->spec_code) {
			$sc = $this->getSpecCode($refdoc->spec_code);

			if ($sc instanceof SpecCode) {
				$this->logMessage('Set refdoc spec code ID to ' . $sc->id, HL7InboundBase::LOG_LEVEL_ALL);
				$this->logMessage('Set refdoc spec code Description to ' . $sc->description, HL7InboundBase::LOG_LEVEL_ALL);

				$refdoc->spec_code_id = $sc->id;
				$refdoc->spec_code_description = $sc->description;
			}
		}

		if ($pv1['PV1_F8_C3']) {
			$this->logMessage('Set refdoc fname to ' . $pv1['PV1_F8_C3'], HL7InboundBase::LOG_LEVEL_ALL);
			$refdoc->fname = $pv1['PV1_F8_C3'];
		}

		if ($pv1['PV1_F8_C2']) {
			$this->logMessage('Set refdoc lname to ' . $pv1['PV1_F8_C2'], HL7InboundBase::LOG_LEVEL_ALL);
			$refdoc->lname = $pv1['PV1_F8_C2'];
		}

		$this->indent -= 2;
		return $refdoc;
	}
}