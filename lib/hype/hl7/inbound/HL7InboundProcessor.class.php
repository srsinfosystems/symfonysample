<?php

class HL7InboundProcessor
{
 	protected $debug = false;
 	protected $dbh = null; // database connection
 	protected $doctrine_connection = null;
 	protected $hl7_partner;
 	protected $limit = 10;
 	protected $log = '';

 	public function __construct()
 	{
 		return $this;
 	}
 	public function setDebug($v)
 	{
 		$this->debug = $v;
 	}
 	public function setDoctrineConnection($v)
 	{
 		$this->doctrine_connection = $v;
 	}
 	public function execute()
 	{
 		$this->connectEzHL7();
 		$result_set = $this->getUnprocessedMessages();

 		foreach ($result_set as $result)
 		{
 			$this->deleteHl7InboundMessages($result['MessageID']);
 			$this->handleHl7Message($result);
 		}
 	}
 	public function getEchobaseClient($client_name)
 	{
 		return Doctrine_Query::create()
 			->from('Client c')
 			->addWhere('c.company_name = (?)', $client_name)
 			->fetchOne();
 	}
 	public function checkRamsoftClient($client_name) {
 		$client_record = Doctrine_Query::create()
 			->from('Client c')
 			->addWhere('c.name = (?)', $client_name)
 			->addWhere('c.hl7_partner_id LIKE (?)', '%' . $this->hl7_partner . ':' . $client_name . '^%')
 			->fetchOne();
 		return $client_record;
 	}
 	public function checkMergeRISClient($client_name) {
 		$client_record = Doctrine_Query::create()
 			->from('Client c')
 			->addWhere('c.name = (?)', $client_name)
 			->addWhere('c.hl7_partner_id LIKE (?)', '%' . $this->hl7_partner . ':' . $client_name . '^%')
 			->fetchOne();
 		return $client_record;
 	}
 	public function checkEyemdClient($client_name)
 	{
 		$client_record = Doctrine_Query::create()
 			->from('Client c')
 			->addWhere('c.hl7_partner_id LIKE (?)', '%' . $this->hl7_partner . ':' . $client_name . '^%')
 			->fetchOne();

 		return $client_record;
 	}
 	public function deleteHl7InboundMessages($messageID)
 	{
 		$hl7s = Doctrine_Query::create()
 			->from('Hl7Inbound i')
 			->addWhere('i.message_name = (?)', $messageID)
 			->execute();

 		foreach ($hl7s as $hl7)
 		{
 			$hl7->delete();
 		}
 	}
 	public function setProcessed($messageID, $status)
 	{
 		$sql = 'update HYPE_HL7Data set processed = ? where MessageID = ?';
 		$sth = $this->dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
 		$rows_affected = $sth->execute(array($status, $messageID));
 		return($rows_affected);
 	}
 	public function connectEzHL7()
 	{
 		try {
 			$this->dbh = new PDO(sfConfig::get('app_hl7_inbound_database_dsn'), sfConfig::get('app_hl7_inbound_database_username'),
 			(!sfConfig::get('app_hl7_inbound_database_password') ? '' : sfConfig::get('app_hl7_inbound_database_password')));

 			$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

 		} catch (PDOException $e) {
 			throw new Exception('PDO Connection Error: ' . $e->getMessage());
 		}
 	}
 	public function getUnprocessedMessages()
 	{
 		$sql_query = 'SELECT * from HYPE_HL7DATA where Processed = 0 AND Inbound = 1 AND (MsgType = ? OR MsgType = ? OR MsgType = ?) order by Dateloaded limit ' . $this->limit;

 		$new_sth = $this->dbh->prepare($sql_query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
 		$new_sth->execute(array('DFT', 'ADT', 'SIU'));
 		return $new_sth->fetchAll();
 	}
 	public function getCVIAClient($sendingApp, $client_name)
 	{
 		if ($sendingApp != 'MERGE RIS') {
 			return null;
 		}
 		return Doctrine_Query::create()
 			->from('Client c')
 			->addWhere('c.hl7_partner_id LIKE (?)', '%MERGERIS:' . $client_name . '^%')
 			->fetchOne();
 	}
 	public function getWestTorontoEyeCareClient($sendingApp, $clientName)
 	{
 		if ($sendingApp != 'BRAZELEYEMDEMR') {
 			return null;
 		}
 		return Doctrine_Query::create()
 			->from('Client c')
 			->addWhere('c.hl7_partner_id LIKE (?)', '%BRAZELEYEMDEMR:' . $clientName . '^%')
 			->fetchOne();
 	}
 	public function getGiavedoniClient($sendingApp, $clientName)
 	{
 		if ($sendingApp != 'GIAVEDONIEYEMDEMR') {
 			return null;
 		}
 		return Doctrine_Query::create()
 			->from('Client c')
 			->addWhere('c.hl7_partner_id LIKE (?)', '%GIAVEDONIEYEMDEMR:' . $clientName . '^%')
 			->fetchOne();
 	}
 	public function getMargolinClient($sendingApp, $clientName)
 	{
 		if ($sendingApp != 'MARGOLINEYEMDEMR') {
 			return null;
 		}
 		return Doctrine_Query::create()
 			->from('Client c')
 			->addWhere('c.hl7_partner_id LIKE (?)', '%MARGOLINEYEMDEMR:' . $clientName . '^%')
 			->fetchOne();
 	}
 	public function getODCClient($clientName)
 	{
 		return Doctrine_Query::create()
 			->from('Client c')
 			->addWhere('c.hl7_partner_id LIKE (?)', '%MERGERIS:' . $clientName . '^%')
 			->fetchOne();
 	}
 	public function getISSAClient($sendingApp, $clientName)
 	{
 		if ($sendingApp != 'ISSA') {
 			return null;
 		}
 		return Doctrine_Query::create()
 			->from('Client c')
 			->addWhere('c.hl7_partner_id LIKE (?)', '%ISSA:' . $clientName . '^%')
 			->fetchOne();
 	}

 	public function handleHl7Message($hl7)
 	{
  		$messageID = $hl7['MessageID'];
  		$msh = $this->getMSHSegement($messageID);
  		$sendingApplication = trim(strtoupper($msh['MSH_F3_C1']));
  		$companyName = strtoupper(trim($msh['MSH_F4_C1']));
  		$companyName2 = trim(strtoupper($msh['MSH_F3_C2']));

  		$this->log = '';
  		$this->logMessage('.................................................................', true, false);
 		$this->logMessage('BEGINNING HL7 Inbound Processor on ' . date('Y-m-d H:i:s'), true, true);
  		$this->logMessage('The Message ID is ' . $messageID, true, true);
  		$this->logMessage('The sending application is '. $sendingApplication, true, true);

  		$set_processed = $this->setProcessed($messageID, HL7InboundBase::STATUS_PROCESSING);

  		$hl7Inbound = new Hl7Inbound();
  		$hl7Inbound
  			->setHl7Message($hl7['HL7Message'])
  			->setStatus(Hl7InboundTable::STATUS_PROCESSING)
  			->setMessageName($hl7['MessageID'])
  			->setMsgType($hl7['MsgType'])
  			->setMsgEvent($hl7['MsgEvent'])
 	 		->setLogText($this->log);

  		/***
  		 * STARTING THIS OVER WITH CVIA. CVIA WILL GET ITS OWN INBOUND CLASS WHICH EXTENDS HL7InboundBase
  		 * Identifiers for this client account in MSH:
  		 * - MSH-3.1 [sendingApplication]: MERGE RIS
  		 * - MSH-4.1 [companyName]:        Glodon Management Inc. [matches with Client->hl7_partner_id]
  		 */
 		if ($sendingApplication == 'MERGE RIS' && $companyName == 'GLODON MANAGEMENT INC.') {
 	 		$client = $this->getCVIAClient($sendingApplication, $companyName);

 	 		if ($client instanceof Client) {
 	 			$hl7Inbound->Client = $client;

 	 			$this->logMessage('CVIA identified as the client. [ID: ' . $client->id . ']', true, true);

 	 			$hl7Inbound->save();

 	 			$inboundProcessor = new HL7InboundCVIA($messageID, $msh, $client->id, 'MERGERIS', $this->dbh, $this->doctrine_connection);
 	 			$inboundProcessor
 	 				->setDebug($this->debug)
 	 				->setHL7Inbound($hl7Inbound)
 	 				->setRawMessage($hl7['HL7Message'])
 	 				->process();

 	 			$hl7Inbound = $inboundProcessor->getHL7Inbound();
 	 			$hl7Inbound->save();
 	 			return;
 	 		}

 	 		$this->logMessage('The HL7 message ' . $messageID . ' cannot be processed. The client ' . $companyName . ' cannot be matched to a HYPE Medical client account.');
  			$set_processed = $this->setProcessed($messageID, HL7InboundBase::STATUS_FATAL_ERROR);
  			$hl7Inbound->setStatus(HL7InboundTable::STATUS_FATAL_ERROR);

  			$hl7Inbound->save();
  			return;
 		}

 		/***
 		 * GIAVEDONI OR BRAZEL
 		 * GIAVEDONI [Dr Louis Giavedonni] Their HL7 Partner in Client should be GIAVEDONI:EYEMDEMR^
 		 * BRAZEL [West Toronto Eye Care] Their HL7 Partner in Client should be BRAZELEYEMDEMR:EYEMDEMR^
 		 */
 		if ($sendingApplication == 'EYEMDEMR' && $companyName == 'EYEMDEMR') {

 			$client = $this->getWestTorontoEyeCareClient('BRAZELEYEMDEMR', $companyName);
 			if ($client instanceof Client) {
 				$hl7Inbound->Client = $client;

 				$this->logMessage('West Toronto Eye Care identified as the client. [ID: ' . $client->id . ']', true, true);

 				$hl7Inbound->save();

 				$inboundProcessor = new HL7InboundBrazelEyeMdEMR($messageID, $msh, $client->id, 'MERGERIS', $this->dbh, $this->doctrine_connection);
 				$inboundProcessor
 					->setDebug($this->debug)
 					->setHL7Inbound($hl7Inbound)
 					->setRawMessage($hl7['HL7Message'])
 					->process();

 				$hl7Inbound = $inboundProcessor->getHL7Inbound();
 				$hl7Inbound->save();
 				return;
 			}

 			$client = $this->getGiavedoniClient('GIAVEDONIEYEMDEMR', $companyName);
 			if ($client instanceof Client) {
 				$hl7Inbound->Client = $client;

 				$this->logMessage('Louis Giavedonni identified as the client. [ID: ' . $client->id . ']', true, true);

 				$hl7Inbound->save();

 				$inboundProcessor = new HL7InboundGiavedonniEyeMdEMR($messageID, $msh, $client->id, 'MERGERIS', $this->dbh, $this->doctrine_connection);
 				$inboundProcessor
 				->setDebug($this->debug)
 				->setHL7Inbound($hl7Inbound)
 				->setRawMessage($hl7['HL7Message'])
 				->process();

 				$hl7Inbound = $inboundProcessor->getHL7Inbound();
 				$hl7Inbound->save();
 				return;
 			}

 			$this->logMessage('The HL7 message ' . $messageID . ' cannot be processed. The client ' . $companyName . ' cannot be matched to a HYPE Medical client account.');
 			$set_processed = $this->setProcessed($messageID, HL7InboundBase::STATUS_FATAL_ERROR);
 			$hl7Inbound->setStatus(HL7InboundTable::STATUS_FATAL_ERROR);

 			$hl7Inbound->save();
 			return;
 		}

 		/***
 		 * MARGOLIN []
 		 * Their HL7 Partner in Client should be MARGOLINEYEMDEMR:^
 		 */

 		/***
 		 * ODC [Ontario Diagnostics Centre]
 		 * Their HL7 Partner ID should be 'ONTARIO DIAGNOSTIC CORPORATION'
 		 */
 		if ($sendingApplication == 'MERGERIS' && $companyName == 'ONTARIO DIAGNOSTIC CORPORATION') {
 			$client = $this->getODCClient($companyName2);

 			if ($client instanceof Client) {
 				$hl7Inbound->client_id = $client->getID();
  				$this->logMessage('ODC identified as the client. [ID: ' . $client->id . ']', true, true);

  				$hl7Inbound->save();

  				$inboundProcessor = new HL7InboundOntarioDiagnosticCentre($messageID, $msh, $client->id, 'MERGERIS', $this->dbh, $this->doctrine_connection);

  				$inboundProcessor
  					->setDebug($this->debug)
  					->setHL7Inbound($hl7Inbound)
  					->setRawMessage($hl7['HL7Message'])
  					->process();

  				$hl7Inbound = $inboundProcessor->getHL7Inbound();
  				$hl7Inbound->save();
  				return;
 			}

  			$this->logMessage('The HL7 message ' . $messageID . ' cannot be processed. The client ' . $companyName . ' cannot be matched to a HYPE Medical client account.');
  			$set_processed = $this->setProcessed($messageID, HL7InboundBase::STATUS_FATAL_ERROR);
  			$hl7Inbound->setStatus(HL7InboundTable::STATUS_FATAL_ERROR);

  			$hl7Inbound->save();
  			return;
 		}

 		/***
 		 * VAMS-HALABI - I3Solutions
 		 * Their HL7 Partner ID should be 'ISSA'
 		 */
 		if ($sendingApplication == 'ISSA' && $companyName == 'VAMS-HALABI') {
 			$client = $this->getISSAClient($sendingApplication, $companyName);

 			if ($client instanceof Client) {
 				$hl7Inbound->Client = $client;

 				$this->logMessage('I3Solutions identified as the client. [ID: ' . $client->id . ']', true, true);

 				$hl7Inbound->save();

 				$inboundProcessor = new HL7InboundVamsHalabiISSA($messageID, $msh, $client->id, 'ISSA', $this->dbh, $this->doctrine_connection);
 				$inboundProcessor
 					->setDebug($this->debug)
 					->setHL7Inbound($hl7Inbound)
 					->setRawMessage($hl7['HL7Message'])
 					->process();

 				$hl7Inbound = $inboundProcessor->getHL7Inbound();
 				$hl7Inbound->save();
 				return;
 			}

 			$this->logMessage('The HL7 message ' . $messageID . ' cannot be processed. The client ' . $companyName . ' cannot be matched to a HYPE Medical client account.');
 			$set_processed = $this->setProcessed($messageID, HL7InboundBase::STATUS_FATAL_ERROR);
 			$hl7Inbound->setStatus(HL7InboundTable::STATUS_FATAL_ERROR);

 			$hl7Inbound->save();
 			return;
 		}

 		/***
 		 * HYPE MOBILE [ECHOBASE RESONATE]
 		 * Their HL7 Partner ID should be
 		 */
 		if ($sendingApplication == 'RESONATE') {
 			$companyName = trim(strtoupper($msh['MSH_F6_C1']));
 			$client = $this->getEchobaseClient($companyName);

 			if ($client instanceof Client) {
 				$this->logMessage('Echobase Resonate client identified. Client Name: ' . $client->getCompanyName() . ' [ID: ' . $client->getId() . ']', true, true);

 				$hl7Inbound->Client = $client;
 				$hl7Inbound->save();

 				$inboundProcessor = new HL7InboundEchobase($messageID, $msh, $client->getId(), 'ECHOBASE', $this->dbh, $this->doctrine_connection);
  				$inboundProcessor
  					->setDebug($this->debug)
  					->setHL7Inbound($hl7Inbound)
  					->setRawMessage($hl7['HL7Message'])
  					->process();

 				$hl7Inbound = $inboundProcessor->getHL7Inbound();
 				$hl7Inbound->save();
 				return;
 			}

 			$this->logMessage('The HL7 message ' . $messageID . ' cannot be processed. The Echobase Resonate client ' . $companyName . ' cannot be matched to a HYPE Medical client account.');
 			$set_processed = $this->setProcessed($messageID, HL7InboundBase::STATUS_FATAL_ERROR);
 			$hl7Inbound->setStatus(HL7InboundTable::STATUS_FATAL_ERROR);

 			$hl7Inbound->save();
 			return;
 		}

 		$client_record = null;
 		if ($sendingApplication == 'EYEMDEMR') {
 				$client_id = trim(strtoupper($msh['MSH_F3_C1']));
 				$client_record = $this->checkEyemdClient($client_id);
 				$this->hl7_partner = 'EYEMDEMR';
 		}
 		else if ($sendingApplication == 'MERGERIS') {
 			$client_name = trim(strtoupper($msh['MSH_F3_C2']));
 			//$hl7_partner_id = trim(strtoupper($msh['MSH_F3_C3']));
 			$client_record = $this->checkMergeRISClient($client_name);
 			$this->hl7_partner = $sendingApplication;
 		}
 		else if ($sendingApplication == 'RAMSOFT') {
 			$client_name = trim(strtoupper($msh['MSH_F3_C2']));
 			//$hl7_partner_id = trim(strtoupper($msh['MSH_F3_C3']));
 			$client_record = $this->checkRamsoftClient($client_name);
 			$this->hl7_partner = $sendingApplication;
 		}
 		else if ($sendingApplication == 'ECHOBASE') {
 			$sendingApplication = 'ECHOBASE';
 			$client_name = trim(strtoupper($msh['MSH_F6_C1']));
 			$this->hl7_partner = 'ECHOBASE';
 			$client_record = $this->getEchobaseClient($client_name);
 		}

 		if (!$client_record instanceof Client) {
 			$this->logMessage('The HL7 message ' . $messageID . ' cannot be processed. The client ' . $client_name . ' is invalid.', true, true);

 			$set_processed = $this->setProcessed($messageID, HL7InboundBase::STATUS_FATAL_ERROR);

 			$hl7Inbound
 				->setStatus(HL7InboundTable::STATUS_FATAL_ERROR)
 				->save();
 			return;
 		}
 		else {
 			$client_id = $client_record['id'];
 			$hl7Inbound->Client = $client_record;
 		}

 		$this->logMessage('Client identified as ' . $client_record['company_name'] .'_'. $client_record['name'] . ' (ID: ' . $client_id . ")", true, true);

 		$sendingApplication_name = ucfirst(strtolower($sendingApplication));

 		// if class exists with naming convention HL7Inbound{$clientName}{$messageType} Hl7EchoBaseADT then create instance of that class
 		if (class_exists('HL7Inbound' . $sendingApplication_name)) {
 			$hL7Class = 'HL7Inbound'. $sendingApplication_name;

 			$newHL7InboundProcessor = new $hL7Class($messageID, $msh, $client_id, $this->hl7_partner, $this->dbh, $this->doctrine_connection);
 			$newHL7InboundProcessor->
 				setDebug($this->debug)
 				->setHL7Inbound($hl7Inbound)
 				->process();

 			$hl7Inbound = $newHL7InboundProcessor->getHL7Inbound();
 			$hl7Inbound->save();

 		}
 		else
 		{
 			$this->logMessage('Unable to process message. Class "HL7Inbound' . $sendingApplication_name . '" does not exist.', true, true);

 			$hl7Inbound
 				->setStatus(HL7InboundTable::STATUS_FATAL_ERROR)
 				->save();

 			return;
 		}
 	}
 	public function getMSHSegement($messageID)
 	{
 		$sql = 'SELECT * from HYPE_Segment_MSH_A where MessageID = ? limit 1';
 		$sth = $this->dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
 		$sth->execute(array($messageID));
 		return  $sth->fetch(PDO::FETCH_ASSOC);
 	}
 	public function logMessage($msg, $toScreen = true, $toLog = true)
 	{
 		if ($toLog) {
 			if (strlen($this->log)) {
 				$this->log .= "\n";
 			}
 			$this->log .= $msg;
 		}

 		if ($this->debug && $toScreen) {
 			echo $msg . "\n";
 		}
 	}
}
