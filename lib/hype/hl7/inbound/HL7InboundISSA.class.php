<?php

class HL7InboundISSA extends HL7InboundBase
{
	// PID 1.1 - 1
	// PID 2.1 - HYPE Patient ID [Same as 3.1] ONLY IF PATIENT CAME FROM HYPE
	// PID 3.1 - ISSA Chart Number (chart number on RIS)
	// PID 5.1 - Last Name
	// PID 5.2 - First Name
	// PID 5.3 - Middle Name [will not be imported]
	// PID 7.1 - DOB [YYYMMDD]
	// PID 8.1 - Sex [M|F|U] [U will not be mapped]
	// PID 10.1 - Race - NOT MAPPED
	// PID 11.1 - Street Address
	// PID 11-2 - Street Address 2 - APPENDED TO ADDRESS - no Address 2 line
	// PID 11.3 - City
	// PID 11.4 - State
	// PID 11.5 - Postal [Zip] Code
	// PID 11.6 - Country
	// PID 13.1 - Home Phone
	// PID 14.1 - Work Phone
	// PID 15.1 - Language - NOT MAPPED
	// PID 16.1 - Marital Status - NOT MAPPED
	// PID 18.1 - HYPE Patient ID [Same As 3.1]
	// PID 18.1 - Health Card Number
	// PID 18.2 - Version Code

	// PV1 1.1 - 1
	// PV1 2.1 - O
	// PV1 3-1 - Location Code
	// PV1 3-4 - Location Name
	// PV1 7.1 - Attending Doctor provider number [6 digits]
	// PV1 7.2 - Attending Doctor Last Name
	// PV1 7.3 - Attending Doctor First Name
	// PV1 7.5 - Attending Doctor OHIP Spec Code
	// PV1 8.1 - Referring Doctor's provider number [6 digits]
	// PV1 8.2 - Referring Doctor's Last Name
	// PV1 8.3 - Referring Doctor's First Name
	// PV1 8.5 - Referring Doctor's Spec Code
	// PV1 19.1 - ?

	// FT1 4.1 - Date of Service
	// FT1 25.1 - Service Code
	// FT1 10.1 - Number of Services
	// FT1 18.1 - Claim Type
	// FT1 19.1 - Diagnostic Code

 	private $default_refdoc_spec_code = '00';
 	private $default_spec_code = '33';
 	private $default_group_number = '0000';
 	private $default_pay_prog = ClaimTable::CLAIM_TYPE_HCPP;
 	private $serviceCodes = null;
 	private $group_numbers = array();

 	private $directServiceCodesCheck = true;
 	private $save_temporary = true;

 	public function processADT()
 	{
 		$this->logMessage('processADT() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$this->logMessage('We do not process ADT messages.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

  		$patient = $this->handlePID();

  		$pv1 = $this->getPV1();
  		$pv1b = $this->getPV1B();

  		$referring_doctor = $this->getReferringDoctor($pv1, $pv1b);

  		if ($referring_doctor instanceof ReferringDoctor) {
  			$patient->ref_doc_num = $referring_doctor->provider_num;
  			$patient->ref_doc_refdoc_id = $referring_doctor->id;
  			$patient->ref_doc_refdocaddr_id = $referring_doctor->ReferringDoctorAddress[0]->id;
  		}

  		$this->logMessage('Saving patient record', HL7InboundBase::LOG_LEVEL_MINIMUM);
  		$patient->save();
  		$this->logMessage('Patient ID: ' . $patient->id, HL7InboundBase::LOG_LEVEL_MINIMUM);

 		$this->setProcessed(HL7InboundBase::STATUS_PROCESSED);
 		$this->hl7Inbound->setStatus(HL7InboundTable::STATUS_PROCESSED);
 		$this->logMessage('Processing is complete. Updated HL7 Message to status ' . HL7InboundBase::STATUS_PROCESSED, HL7InboundBase::LOG_LEVEL_MINIMUM);

 		$this->indent -= 2;
 	}
 	public function processDFT()
 	{
 		$this->logMessage('processDFT() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;
 		$this->logMessage('Processing DFT Message', HL7InboundBase::LOG_LEVEL_MINIMUM);

 		$this->serviceCodes = new Doctrine_Collection('ServiceCode');

  		$claim = new Claim();
  		$patient = $this->handlePID();
  		$location = $this->getLocation();
  		$doctor = null;

  		if (!$location instanceof Location) {
  			$this->is_valid = false;
  			$this->logMessage('The location ID is invalid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
  			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_LOCATION, 'The location ID is invalid.');
  			$location = new Location();
  		}
  		else {
  			if (!$patient['location_id']) {
  				$patient['location_id'] = $location['id'];
  			}
  			$claim->location_id = $location['id'];
  			$claim->Location = $location;

  			$this->hl7Inbound->setLocation($location);

  			$this->logMessage('Location of service: ' . $location->code, HL7InboundBase::LOG_LEVEL_MINIMUM);
  		}

  		$this->logMessage('Setting Patient Details to claim.', HL7InboundBase::LOG_LEVEL_ALL);
  		$output = $claim->setPatientDetails($patient, true);

  		$this->hl7Inbound
  			->setPatient($patient)
  			->setFname($patient->getFname())
  			->setLname($patient->getLname());

 		foreach ($output as $line)
 		{
 			$this->logMessage($line, HL7InboundBase::LOG_LEVEL_ALL);
 		}

  		$ft1s = $this->getFT1();
  		$pid = $this->getPID();
  		$pv1a = $this->getPV1();
  		$pv1b = $this->getPV1B();

   		$referring_doctor = $this->getReferringDoctor($pv1a, $pv1b);
   		if ($referring_doctor instanceof ReferringDoctor)
   		{
   			$claim->ref_doc = $referring_doctor->provider_num;
   			$claim->ref_doc_id = $referring_doctor->id;
   			$claim->ref_doc_addr_id = $referring_doctor->ReferringDoctorAddress[0]->id;

   			$this->logMessage('Set claim refDoc num to ' . $claim->ref_doc, HL7InboundBase::LOG_LEVEL_ALL);
   		}

   		$provider_number = trim(strtoupper($pv1a['PV1_F7_C1']));
   		if (intval($provider_number)) {
   			while (strlen($provider_number) < 6)
   			{
   				$provider_number = '0' . $provider_number;
   			}
   			$this->logMessage('Provider Number is ' . $provider_number, HL7InboundBase::LOG_LEVEL_MINIMUM);
   		}
   		else {
   			$this->is_valid = false;
   			$this->logMessage('No provider number provided', HL7InboundBase::LOG_LEVEL_ALL);
   			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_DOCTOR, 'No provider number provided');
   		}

  		foreach ($ft1s as $ft1)
  		{
  			$claim = $this->handleFT1($claim, $ft1);
  		}

  		$distinct_groups = array_unique($this->group_numbers);

  		if (count($distinct_groups) == 1 && $this->is_valid) {
 			$doctor = $this->getDoctor($provider_number, $this->group_numbers[0], $pv1a, $pv1b, $claim->Location);

 			if (!$doctor instanceof Doctor) {
 				$this->is_valid = false;
 				$this->logMessage('No group number provided', HL7InboundBase::LOG_LEVEL_ALL);
 				$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_GROUP_NUMBER, 'No group number provided');
 			}
 			else {
 				$this->hl7Inbound->setDoctorId($doctor->id);
 				$claim->Doctor = $doctor;
 				$claim->doctor_id = $doctor->id;
 				$claim->doctor_code = $doctor->qc_doctor_code;

 				$this->logMessage('Set claim doctor code to ' . $claim->doctor_code, HL7InboundBase::LOG_LEVEL_ALL);
 			}
  		}
  		else if (count($distinct_groups) > 1) {
  			$this->is_valid = false;
  			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MORE_THAN_ONE_GROUP_NUMBER, 'More than one group number identified; more than one claim required.');
  			$this->logMessage('UNHANDLED EXCEPTION: MORE THAN ONE GROUP NUMBER IDENTIFIED. MORE THAN ONE CLAIM REQUIRED!', HL7InboundBase::LOG_LEVEL_MINIMUM);
  		}

  		foreach ($claim->ClaimItem as $key => $claimItem)
  		{
  			$mode = $claimItem->service_code[strlen($claimItem->service_code) - 1];
  			if ($doctor instanceof Doctor) {
  				$claimItem->setBaseFee($this->serviceCodes[$key]->determineFee($mode, $doctor->spec_code_id));

  			}
  			else {
  				$claimItem->getBaseFee($this->serviceCodes[$key]->determineFee($mode));
  			}
  			$claimItem->fee_subm = $claimItem->num_serv * $claimItem->getBaseFee();

  			$this->logMessage('Set claim item ' . $key . ' fee_subm to ' . $claimItem->fee_subm, HL7InboundBase::LOG_LEVEL_ALL);
  			$this->logMessage('Set claim item ' . $key . ' base_fee to ' . $claimItem->getBaseFee(), HL7InboundBase::LOG_LEVEL_ALL);

  			if (!$this->duplicateServicesCheck($claimItem)) {
  				unset($claim->ClaimItem[$key]);
  				if (!count($claim->ClaimItem)) {
  					$this->is_valid = false;
  					$this->save_temporary = false;
  				}
  			}
  		}

  		$claim->batch_number = ClaimTable::EMPTY_BATCH;
  		$claim->date_created = date('Y-m-d H:i:s');
  		$claim->manual_review = false;

  		if (!$claim->health_num && $claim->pay_prog == ClaimTable::CLAIM_TYPE_HCPP) {
  			$this->is_valid = false;
  			$this->logMessage('A health card number must be provided for this claim to be valid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
  			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_HEALTH_CARD, 'A health card number must be provided for this claim to be valid');
  		}

   		// TODO: sli -> ignore for now

  		if (!$this->is_valid && $this->save_temporary) {
  			$temporary_form = TemporaryFormTable::createHL7Claim($claim);
  			$set_processed = $this->setProcessed(HL7InboundBase::STATUS_FORM_ERROR);
  			$this->hl7Inbound->setStatus(HL7InboundTable::STATUS_FORM_ERROR);
  			$this->hl7Inbound->setTemporaryFormId($temporary_form->id);

  			$this->logMessage('Claim saved in temporary tables due to invalid data', HL7InboundBase::LOG_LEVEL_MINIMUM);
  			$this->logMessage('Processing is complete. Updated HL7 Message to status ' . HL7InboundBase::STATUS_FORM_ERROR, HL7InboundBase::LOG_LEVEL_MINIMUM);
  		}
 		else if (!$this->is_valid) {
 			$this->hl7Inbound->setStatus(HL7InboundTable::STATUS_DUPLICATE);
 			$set_processed = $this->setProcessed(HL7InboundBase::STATUS_DUPLICATE);

 			$this->logMessage('Claim already handled - data ignored', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->logMessage('Processing is complete. Updated HL7 Message to status ' . HL7InboundBase::STATUS_DUPLICATE, HL7InboundBase::LOG_LEVEL_MINIMUM);
 		}
 		else {
 			$claim->save();

 			$this->hl7Inbound
 				->setStatus(HL7InboundTable::STATUS_PROCESSED)
 				->setClaimId($claim->getId());

 			$set_processed = $this->setProcessed(HL7InboundBase::STATUS_PROCESSED); //after claim is saved --> set processed  to 1

 			$this->logMessage('Claim saved. Claim id: ' . $claim->id, HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->logMessage('Processing is complete. Updated HL7 Message to status ' . HL7InboundBase::STATUS_PROCESSED, HL7InboundBase::LOG_LEVEL_MINIMUM);
 		}
 		$this->indent -= 2;
 	}
 	private function handleFT1($claim, $ft1)
 	{
 		$this->logMessage('handleFT1() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$req_ref_doc = $req_facility_num = $req_admit_date = $interval = $req_diag_code = false;

 		$item = new ClaimItem();
 		$item->client_id = $this->client_id;

 		$service_code = $this->getServiceCode($ft1['FT1_F25_C1']);
 		$diag_code = $this->getDiagnosticCode($ft1['FT1_F19_C1']);

 		if (!$service_code instanceof ServiceCode) {
 			$this->is_valid = false;
 			$this->logMessage('Service code ' .  $service_code . ' is invalid', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_INVALID_SERVICE_CODE, 'Service code ' . $service_code . ' is invalid');
 		}
 		else {
 			$req_diag_code = $service_code['req_diag_code'];
 			$req_ref_doc = $service_code['req_ref_doctor'];
 			$req_facility_num = $service_code['req_facility_num'];
 			$req_admit_date = $service_code['req_admit_date'];
 			$interval = $service_code['inter_val'];

 			$item->setServiceCode($ft1['FT1_F25_C1']);
 			$this->serviceCodes[count($claim->ClaimItem)] = $service_code;
 			$this->group_numbers[count($claim->ClaimItem)] = $this->determineGroupNumber($item->service_code, $claim->Location->code);

 			$this->logMessage('Service Code set to ' . $item->service_code, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$date = trim($ft1['FT1_F4_C1']);
 		if ($date) {
 			$item->service_date = substr($date,0,4) . "-" . substr($date,4,2) . "-" . substr($date,6,2) . " 00:00:00";
 			$this->logMessage('Set service date to ' . $item->service_date, HL7InboundBase::LOG_LEVEL_ALL);
 		}
 		else {
 			$this->is_valid = false;
 			$this->logMessage('Missing service date', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'Service Date required.');
 		}
 		$item->num_serv = trim($ft1['FT1_F10_C1']) ? trim($ft1['FT1_F10_C1']) : 1;
 		$item->setModifier($item->getNumServ() * 100);
 		$this->logMessage('Set num_serv to ' . $item->num_serv, HL7InboundBase::LOG_LEVEL_ALL);
 		$this->logMessage('Set modifier to ' . $item->getModifier(), HL7InboundBase::LOG_LEVEL_ALL);

 		if ($diag_code instanceof DiagCode) {
 			$item->diag_code = $diag_code->diag_code;
 			$this->logMessage('Set diag code to ' . $item->diag_code, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$claim->pay_prog = $ft1['FT1_F18_C1'] ? $ft1['FT1_F18_C1'] : $this->default_pay_prog;
 		$item->item_id = count($claim->ClaimItem) + 1;
 		$item->status = ClaimItemTable::STATUS_UNSUBMITTED;
 		$this->logMessage('Set claim pay prog to ' . $claim->pay_prog, HL7InboundBase::LOG_LEVEL_ALL);
 		$this->logMessage('Item number is ' . $item->item_id, HL7InboundBase::LOG_LEVEL_ALL);

 		$claim->ClaimItem->add($item);

 		if ($req_diag_code && !$item->diag_code) {
 			$this->is_valid = false;
 			$this->logMessage('Service Code ' . $item->service_code . ' requires a diagnostic code', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'Diagnositc Code required');
 		}
 		if ($req_ref_doc && !$claim->ref_doc) {
 			$this->is_valid = false;
 			$this->logMessage('Service Code ' . $item->service_code . ' requires a referring doctor', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'Referring Doctor required');
 		}
 		if ($req_facility_num && !$claim->facility_num) {
 			$this->is_valid = false;
 			$this->logMessage('Service Code ' . $item->service_code . ' requires a facility number', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'Facility Number required');
 		}
 		if ($req_admit_date && !$claim->admit_date) {
 			$this->is_valid = false;
 			$this->logMessage('Service Code ' . $item->service_code . ' requires an admit date', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'Admit Date required');
 		}
 		$this->indent -= 2;

 		return $claim;
 	}
 	private function createReferringDoctor()
 	{
 		$this->logMessage('createReferringDoctor() called.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

 		$refdoc = new ReferringDoctor();
 		$refdoc->client_id = $this->client_id;
 		$refdoc->active = true;
 		$refdoc->spec_code = $this->default_refdoc_spec_code;

 		$rda = new ReferringDoctorAddress();
 		$rda->active = true;

 		$refdoc->ReferringDoctorAddress->add($rda);

 		return $refdoc;
 	}

 	protected function determineGroupNumber($service_code, $location_code)
 	{
 		/*$group1000 = array(
 			'G319A',
 			'J087C',
 			'J080C',
 			'J809C',
 			'J813C',
 			'J814C',
 			'J866C'
 		);

 		$this->logMessage('determineGroupNumber(' . $service_code . ', ' .  $location_code . ') called.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$rs = null;
 		$mode = $service_code[strlen($service_code) - 1];

 		if ($service_code == 'G570A' && $location_code == 'MIL') {
 			$rs = '0826';
 		}
 		else if ($service_code == 'G572A' && $location_code == 'MIL') {
 			$rs = '0826';
 		}
 		else if ($location_code == 'MIL' && in_array($service_code, $group1000)) {
 			$rs = '1000';
 		}
 		else if ($mode == 'B' && $location_code == 'MIL') {
 			$rs = 'AT86';
 		}
 		else if ($mode == 'B' && $location_code == 'ISSA') {
 			$rs = 'AT52';
 		}
 		else if ($mode == 'C') {
 			$rs = '0000';
 		}

 		$this->indent += 2;
 		$this->logMessage('Group Number determined to be ' . $rs, HL7InboundBase::LOG_LEVEL_MINIMUM);
 		$this->indent -= 2;

 		return $rs;
 		*/
 		return '0000';
 	}

 	private function getDoctor($provider_number, $group_number, $pv1a, $pv1b, $location)
 	{
 		$this->logMessage('getDoctor called for doctor number ' . $provider_number . ' and group number ' . $group_number, HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		if (!$group_number) {
 			return null;
 		}

 		$doctor = Doctrine_Query::create()
 			->from('Doctor d')
 			->leftJoin('d.DoctorLocation dl')
 			->addWhere('d.client_id = (?)', $this->client_id)
 			->addWhere('d.provider_num = (?)', $provider_number)
 			->addWhere('d.group_num = (?)', $group_number)
 			->fetchOne();

 		if (!$doctor instanceof Doctor) {
 			$this->logMessage('create Doctor record', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

 			$doctor = new Doctor();
 			$doctor->client_id = $this->client_id;
 			$doctor->group_num = $group_number ? $group_number : $this->default_group_number;
 			$doctor->provider_num = $provider_number;
 			$doctor->spec_code_id = strtoupper(trim($pv1a['PV1_F7_C5'])) ? strtoupper(trim($pv1a['PV1_F7_C5'])) : $this->default_spec_code;
 			$doctor->fname = strtoupper(trim($pv1a['PV1_F7_C3']));
 			$doctor->lname = strtoupper(trim($pv1a['PV1_F7_C2']));

 			$doctor_code = substr($doctor['fname'], 0, 1) . substr($doctor['lname'], 0, 1);
 			$doctor->qc_doctor_code = DoctorTable::checkDoctorCode($doctor_code, $this->client_id, 1);
 			$doctor->province = 'ON';
 			$doctor->active = 1;

 			$doctor->setHl7IDForPartner($provider_number, $this->hl7_partner);
 			// TODO: MOH OFFICE ID
 			// TODO: SLI

 			$this->logMessage('Set doctor group num to ' . $doctor->group_num, HL7InboundBase::LOG_LEVEL_ALL);
 			$this->logMessage('Set doctor provider num to ' . $doctor->provider_num, HL7InboundBase::LOG_LEVEL_ALL);
 			$this->logMessage('Set doctor spec code id to ' . $doctor->spec_code_id, HL7InboundBase::LOG_LEVEL_ALL);
 			$this->logMessage('Set doctor first name to ' . $doctor->fname, HL7InboundBase::LOG_LEVEL_ALL);
 			$this->logMessage('Set doctor last name to ' . $doctor->lname, HL7InboundBase::LOG_LEVEL_ALL);
 			$this->logMessage('Set doctor code to ' . $doctor->qc_doctor_code, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$in_location = false;
 		foreach ($doctor->DoctorLocation as $dl)
 		{
 			if ($dl->location_id == $location->id) {
 				$in_location = true;
 			}
 		}

 		if (!$in_location) {
 			$dl = new DoctorLocation();
 			$dl->location_id = $location->id;
 			$doctor->DoctorLocation->add($dl);
 		}
 		$doctor->save();

 		$this->indent -= 2;
 		return $doctor;
 	}
 	private function getLocation()
 	{
 		$this->logMessage('getLocation() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$pv1_record = $this->getPV1();
 		$hl7_partner_id = trim(strtoupper($pv1_record['PV1_F3_C1']));

 		return Doctrine_Query::create()
 			->from('Location l')
 			->addWhere('l.client_id = (?)', $this->client_id)
 			->addWhere('l.hl7_partner_id LIKE (?)', '%' . $this->hl7_partner . ':' . $hl7_partner_id . '^%')
 			->fetchOne();
 	}
 	private function getPatient($hype_id, $issa_id, $pid)
 	{
 		$this->logMessage('getPatient(' . $hype_id . ', ' . $issa_id . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;
 		$patient = null;

 		if ($hype_id) {
 			$patient = $this->getPatientFromHypeId($hype_id);
 		}
 		if ($patient instanceof Patient) {
 			$this->logMessage('Patient found with HYPE ID ' . $patient->id . ' (' . $patient->fname . ' ' . $patient->lname . ')', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->indent -= 2;
 			return $patient;
 		}

 		$fname = $pid['PID_F5_C2'];
 		$lname = $pid['PID_F5_C1'];
 		$date = date_parse_from_format('Ymd', $pid['PID_F7_C1']);
 		$date = mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);
 		$dob = date('Y-m-d', $date);

 		if ($fname && $lname && $date && $dob) {
 			$patient = $this->getPatientFromNameAndDob($fname, $lname, $dob);
 		}
 		if ($patient instanceof Patient) {
 			$this->logMessage('Patient found with HYPE ID ' . $patient->id . ' (' . $patient->fname . ' ' . $patient->lname . ')', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->indent -= 2;
 			return $patient;
 		}

 		// TODO: search by health card number

 		$this->indent -= 2;
 		return $this->createNewPatient();
 	}
 	private function getPatientFromISSAId($issa_id)
 	{
 		$this->logMessage('getPatientFromISSAId(' . $issa_id . ') called', HL7InboundBase::LOG_LEVEL_MINIMUM);

 		if ($issa_id) {
 			return Doctrine_Query::create()
 				->from('Patient p')
 				->addWhere('p.client_id = (?)', $this->client_id)
 				->addWhere('p.patient_number = (?)', $issa_id)
 				->addWhere('p.deleted = (?)', false)
 				->fetchOne();
 		}
 	}
 	private function getReferringDoctor($pv1, $pv1b)
 	{
 		$this->logMessage('getReferringDoctor() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

 		if (!$pv1['PV1_F8_C1']) {
 			return;
 		}
 		$this->indent += 2;
 		$this->logMessage('find Referring Doctor record for provider number ' . $pv1['PV1_F8_C1'], HL7InboundBase::LOG_LEVEL_MINIMUM);

 		$refdoc = Doctrine_Query::create()
 			->from('ReferringDoctor d')
 			->addWhere('d.client_id = (?)', $this->client_id)
 			->addWhere('d.provider_num = (?)', $pv1['PV1_F8_C1'])
 			->fetchOne();

 		if (!$refdoc instanceof ReferringDoctor) {
 			$refdoc = $this->createReferringDoctor();
 		}
 		if ($refdoc instanceof ReferringDoctor) {
 			$refdoc = $this->updateReferringDoctor($refdoc, $pv1, $pv1b);
 		}

 		$this->logMessage('Saving refdoc record', HL7InboundBase::LOG_LEVEL_MINIMUM);
 		$refdoc->save();
 		$this->logMessage('Ref Doc ID: ' . $refdoc->id, HL7InboundBase::LOG_LEVEL_MINIMUM);

 		$this->indent -= 2;
 		return $refdoc;
 	}
 	private function handlePID()
 	{
 		$this->logMessage('handlePID() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$pid = $this->getPID();

 		$hypeId = intval($pid['PID_F2_C1']) == $pid['PID_F2_C1'] ? intval($pid['PID_F2_C1']) : null;
 		$issaId = $pid['PID_F3_C1'];

 		$patient = $this->getPatient($hypeId, $issaId, $pid);
 		if (!$patient->id) {
 			$patient = $this->updatePatient($patient, $pid);
 		}

 		$this->indent -= 2;
 		return $patient;
 	}
 	private function sendNewPatientADT($patient)
 	{
 		$this->logMessage('sendNewPatientADT called.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$hl7_queue = new Hl7Queue();
 		$hl7_queue->client_id = $patient->client_id;
 		$hl7_queue->version_number = $patient->version;
 		$hl7_queue->message_type = 'ADT';
 		$hl7_queue->message_subtype = 'A08';
 		$hl7_queue->model_id = $patient->id;
 		$hl7_queue->model_type = 'PATIENT';
 		$hl7_queue->status = HL7QueueTable::STATUS_UNPROCESSED;
 		$hl7_queue->partner_name = 'ISSA';

 		$hl7_queue->save();
 		$this->indent -= 2;
 	}
 	private function updatePatient($patient, $pid)
 	{
 		$this->logMessage('updatePatient() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		// PID 3.1 - ISSA Chart Number
 		if ($pid['PID_F3_C1']) {
 			$this->logMessage('Set Chart number to ' . $pid['PID_F3_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->patient_number = $pid['PID_F3_C1'];
 		}

 		// PID 5.1 - Last Name
 		if ($pid['PID_F5_C1']) {
 			$this->logMessage('Set patient lname to ' . $pid['PID_F5_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->lname = $pid['PID_F5_C1'];
 		}

 		// PID 5.2 - First Name
 		if ($pid['PID_F5_C2']) {
 			$this->logMessage('Set patient fname to ' . $pid['PID_F5_C2'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->fname = $pid['PID_F5_C2'];
 		}

 		// PID 5.3 - Middle Name [will not be imported]

 		// PID 7.1 - DOB [YYYMMDD]
 		if ($pid['PID_F7_C1']) {
 			$date = date_parse_from_format('Ymd', $pid['PID_F7_C1']);
 			$date = mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);

 			$this->logMessage('Set patient dob to ' . date('Y-m-d', $date), HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->dob = date('Y-m-d', $date);
 		}

 		// PID 8.1 - Sex [M|F|U] [U will not be mapped]
 		if ($pid['PID_F8_C1'] == 'M' || $pid['PID_F8_C1'] == 'F') {
 			$this->logMessage('Set patient sex to ' . $pid['PID_F8_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->sex = $pid['PID_F8_C1'];
 		}

 		// PID 10.1 - Race - NOT MAPPED

 		// PID 11.1 - Street Address
 		if ($pid['PID_F11_C1']) {
 			$this->logMessage('Set patient address to ' . $pid['PID_F11_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->address = $pid['PID_F11_C1'];
 		}

 		// PID 11.2 - Street Address 2 - APPENDED TO ADDRESS - no Address 2 line
 		if ($pid['PID_F11_C2']) {
 			$this->logMessage('Add to patient address ' . $pid['PID_F11_C2'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->address .= ' ' . $pid['PID_F11_C2'];
 		}

 		// PID 11.3 - City
 		if ($pid['PID_F11_C3']) {
 			$this->logMessage('Set patient city to ' . $pid['PID_F11_C3'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->city = $pid['PID_F11_C3'];
 		}

 		// PID 11.4 - Province
 		if ($pid['PID_F11_C4']) {
 			$patient->province = Doctrine::getTable('Province')->getProvinceCodeForProvinceName($pid['PID_F11_C4']);
 			$this->logMessage('Set patient province to ' . $patient->province, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PID 11.5 - Postal [Zip] Code
 		if ($pid['PID_F11_C5']) {
 			$this->logMessage('Set patient postal code to ' . $pid['PID_F11_C5'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->postal_code = $pid['PID_F11_C5'];
 		}

 		// PID 11.6 - Country
 		if ($pid['PID_F11_C6']) {
 			$this->logMessage('Set patient country to ' . $pid['PID_F11_C6'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->country = $pid['PID_F11_C6'];
 		}

 		// PID 13.1 - Home Phone
 		if ($pid['PID_F13_C1']) {
 			$this->logMessage('Set patient home phone to ' . $pid['PID_F13_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setHphone($pid['PID_F13_C1']);
 		}

 		// PID 14.1 - Work Phone
 		if ($pid['PID_F14_C1']) {
 			$this->logMessage('Set patient work phone to ' . $pid['PID_F14_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setWphone($pid['PID_F14_C1']);
 		}

 		// PID 15.1 - Language - NOT MAPPED

 		// PID 16.1 - Marital Status - NOT MAPPED

 		// Patient Status
 		if (!$patient->patient_status_id) {
 			$cp = Doctrine::getTable('ClientParam')->getParamForClient($this->client_id, 'patient_status_default');
 			if ($cp instanceof ClientParam) {
 				$this->logMessage('Set patient status ID to ' . $cp->value, HL7InboundBase::LOG_LEVEL_ALL);
 				$patient->patient_status_id = $cp->value;
 			}
 		}

 		// Patient Type
 		if (!$patient->patient_type_id) {
 			$cp = Doctrine::getTable('ClientParam')->getParamForClient($this->client_id, 'default_patient_type_id');
 			if ($cp instanceof ClientParam) {
 				$this->logMessage('Set patient type ID to ' . $cp->value, HL7InboundBase::LOG_LEVEL_ALL);
 				$patient->patient_type_id = $cp->value;
 			}
 		}

 		// Patient Health Card Number
 		if ($pid['PID_F18_C1']) {
 			$this->logMessage('Set patient Health Card Number to ' . $pid['PID_F18_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setHcnNum(hype::encryptHCN($pid['PID_F18_C1']));
 		}

 		// Patient version Code
 		if ($pid['PID_F18_C2']) {
 			$this->logMessage('Set patient version code to ' . $pid['PID_F18_C2'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setHcnVersionCode($pid['PID_F18_C2']);
 		}

 		// HL7 partner ID
 		$this->logMessage('Set HL7 Partner ID for ISSA to ' . $patient->patient_number, HL7InboundBase::LOG_LEVEL_ALL);
 		$patient->setHl7IDForPartner($patient->patient_number, 'ISSA');

 		// TODO: primary Location
 		// TODO: moh province
 		$this->indent -= 2;
 		return $patient;
 	}
 	private function updateReferringDoctor($refdoc, $pv1, $pv1b)
 	{
 		$this->logMessage('updateReferringDoctor() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		if ($pv1['PV1_F8_C1']) {
 			$this->logMessage('Set refdoc provider number to ' . $pv1['PV1_F8_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$refdoc->provider_num = $pv1['PV1_F8_C1'];
 		}

 		if ($pv1['PV1_F8_C5']) {
 			$this->logMessage('Set refdoc spec code to ' . $pv1['PV1_F8_C5'], HL7InboundBase::LOG_LEVEL_ALL);
 			$refdoc->spec_code = $pv1['PV1_F8_C5'];
 		}

 		if ($refdoc->spec_code) {
 			$sc = $this->getSpecCode($refdoc->spec_code);

 			if ($sc instanceof SpecCode) {
 				$this->logMessage('Set refdoc spec code ID to ' . $sc->id, HL7InboundBase::LOG_LEVEL_ALL);
 				$this->logMessage('Set refdoc spec code Description to ' . $sc->description, HL7InboundBase::LOG_LEVEL_ALL);

 				$refdoc->spec_code_id = $sc->id;
 				$refdoc->spec_code_description = $sc->description;
 			}
 		}

 		if ($pv1['PV1_F8_C3']) {
 			$this->logMessage('Set refdoc fname to ' . $pv1['PV1_F8_C3'], HL7InboundBase::LOG_LEVEL_ALL);
 			$refdoc->fname = $pv1['PV1_F8_C3'];
 		}

 		if ($pv1['PV1_F8_C2']) {
 			$this->logMessage('Set refdoc lname to ' . $pv1['PV1_F8_C2'], HL7InboundBase::LOG_LEVEL_ALL);
 			$refdoc->lname = $pv1['PV1_F8_C2'];
 		}

 		$this->indent -= 2;
 		return $refdoc;
 	}
}