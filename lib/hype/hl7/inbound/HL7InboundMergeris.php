<?php

class HL7InboundMergeris extends HL7InboundBase
{
	// FT1 1.1 - Item ID
	// FT1 2.1 - HL7 Claim Item ID
	// FT1 4.1 - Service Date
	// FT1 10.1 - Number of Services
	// FT1 18.1 - Claim Type [THIRD PART, RMB, HCP - P]
	// FT1 19.1 - Diagnostic Code
	// FT1 23.1 - HL7 ID for Claim
	// FT1 25.1 - Service Code

	// IN1 2.1 - Policy Number
	// IN1 2.2 - Out Of Province HC #
	// IN1 4.1 - Third Party Provider HL7 ID
	// IN1 19.3 - Patient Province

	// PID 4.1 - Chart Number
	// PID 5.1 - Last Name
	// PID 5.2 - First Name
	// PID 7.1 - Date Of Birth
	// PID 8.1 - Sex
	// PID 11.1 - Address
	// PID 11.3 - City
	// PID 11.5 - Postal Code
	// PID 11.6 - Country
	// PID 13.1 - Home Phone
	// PID 20.1 - HC#
	// PID 20.2 - Version Code
	// PID 20.3 - Expiry Date

	// PV1 3.1 - Location Partner ID / Attending Doctor IHF Number
	// PV1 6.1 - Attending Doctor Group Number
	// PV1 7.1 - Attending Doctor Provider Number
	// PV1 7.2 - Attending Doctor Last Name
	// PV1 7.3 - Attending Doctor First Name
	// PV1 7.4 - MOH Office ID
	// PV1 7.5 - Attending Doctor Spec Code
	// PV1 8.1 - Referring Doctor Provider Number
	// PV1 8.2 - Referring Doctor Lname
	// PV1 8.3 - Referring Doctor Fname
	// PV1 8.5 - Referring Doctor Spec Code
	// PV1 44.1 - Admission Date
	// PV1 44.2 - Facility Number

 	protected $ServiceCode;
 	protected $mode;
 	protected $sli;
 	protected $save_temporary = true;

 	public function processADT()
 	{
 	}
 	public function processDFT()
 	{
 		$this->logMessage('processDFT() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;
 		$this->logMessage('Processing DFT Message', HL7InboundBase::LOG_LEVEL_MINIMUM);

 		$pv1 = $this->getPV1();
 		$pv1b = $this->getPV1B();
 		$ft1s = $this->getFT1();

 		$claim = new Claim();

 		$patient = $this->getPatient();
 		$this->logMessage('Patient name: ' . $patient->full_name . ' (ID: ' . $patient->id . ')', HL7InboundBase::LOG_LEVEL_MINIMUM);
 		$this->logMessage('Set Patient details to claim.', HL7InboundBase::LOG_LEVEL_MINIMUM);

 		$output = $claim->setPatientDetails($patient, true);

  		$this->hl7Inbound
  			->setPatient($patient)
  			->setFname($patient->getFname())
  			->setLname($patient->getLname());

  		$this->indent += 2;
 		foreach ($output as $line)
 		{
 			$this->logMessage($line, HL7InboundBase::LOG_LEVEL_ALL);
 		}
 		$this->indent -= 2;

 		$location = $this->getLocation($pv1);
 		if ($location instanceof Location) {
 			$claim->setLocation($location);
 			$claim->setLocationId($location->getId());
 			$this->hl7Inbound->setLocation($location);

 			if (!$patient->getLocationId()) {
 				$patient->setLocationId($location->getId());
 			}

 			$this->logMessage('Location identified as ' . $location->getName(), HL7InboundBase::LOG_LEVEL_MINIMUM);
 		}
 		else {
 			$this->is_valid = false;
 			$this->logMessage('The Location ID (' . $pv1['PV1_F3_C1'] . ') is invalid', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_LOCATION, 'The Location ID (' . $pv1['PV1_F3_C1'] . ') is invalid');
 		}

 		$referringDoctor = $this->getReferringDoctor($pv1);
 		if ($referringDoctor instanceof ReferringDoctor) {
 			$claim->setRefDocId($referringDoctor->getId());
 			$claim->setRefDoc($referringDoctor->getProviderNum());
 		}
 		else {
 			$this->is_valid = false;
 			$this->logMessage('The Referring Doctor Provider Number ' . $pv1['PV1_F8_C1'] . ' is invalid', hl7inboundbase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_REFERRING_DOCTOR, 'The Referring Doctor Provider Number ' . $pv1['PV1_F8_C1'] . ' is invalid');
 		}

 		// PV1 44.2 - Facility Number
 		$facility_number = $this->handleClientFacilityNumberModification(strtoupper(trim($pv1b['PV1_F44_C2'])));
 		if ($facility_number) {
 			$claim->setFacilityNum($facility_number);
 			$this->logMessage('Set Facility Number to ' . $claim->getFacilityNum(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$claim = $this->handlePayProg($claim, $ft1s);
 		$claim->setDateCreated(date('Y-m-d H:i:s'));
 		$claim->setManualReview(false);

 		$spec_code = trim($pv1['PV1_F7_C5']);
 		$claim = $this->handleFT1s($claim, $ft1s, $spec_code);
 		$doctor = $this->determineDoctor($claim, $pv1, $pv1b, $ft1s);
 		if ($doctor instanceof Doctor) {
 			$claim->setDoctor($doctor);
 			$claim->setDoctorId($doctor->getId());
 			$claim->setDoctorCode($doctor->getQcDoctorCode());

 			$this->hl7Inbound->setDoctor($doctor);
 			$this->hl7Inbound->setDoctorId($doctor->getId());

 			$this->logMessage('Set claim doctor code to ' . $claim->doctor_code, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// 2012-08-09: just set the SLI to what we determined it should be
 		if ($this->sli) {
 			$claim->setSli($this->sli);
 			$this->logMessage('Set claim SLI to ' . $this->sli, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PV1 44.1 - Admission Date
 		$admission_date = $pv1b['PV1_F44_C1'];
 		if($admission_date) {
 			$claim->setAdmitDate(trim(substr($admission_date, 0, 4) . '-' . substr($admission_date, 4, 2) . '-' . substr($admission_date, 6, 2)));
 		}

 		if ($claim->getPayProg() == ClaimTable::CLAIM_TYPE_HCPP && !$claim->getHealthNum()) {
 			$this->is_valid = false;
 			$this->logMessage('A health card number must be provided for this claim to be valid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_HEALTH_CARD, 'A health card number must be provided for this claim to be valid.');
 		}
		else if (strlen($claim->getHCN()) != 10 && $claim->getPayProg() == ClaimTable::CLAIM_TYPE_HCPP) {
 			$this->is_valid = false;
 			$this->logMessage('Invalid health card length.', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_INVALID_HEALTH_CARD, 'Invalid health card length.');
 		}

 	  	foreach ($claim->ClaimItem as $key => $claimItem)
  		{
   			if (!$this->duplicateServicesCheck($claimItem)) {
   				unset($claim->ClaimItem[$key]);
   				if (!count($claim->ClaimItem)) {
   					$this->is_valid = false;
   					$this->save_temporary = false;
   				}
   			}
  		}

 		$claim = $this->handleClientPostMappingChecks($claim);

 		if (!$this->is_valid && $this->save_temporary) {
 			$temporary_form = TemporaryFormTable::createHL7Claim($claim);
 			$set_processed = $this->setProcessed(HL7InboundBase::STATUS_FORM_ERROR);
 			$this->hl7Inbound->setStatus(HL7InboundTable::STATUS_FORM_ERROR);
 			$this->hl7Inbound->setTemporaryFormId($temporary_form->id);

 			$this->logMessage('Claim saved in temporary tables due to invalid data', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->logMessage('Processing is complete. Updated HL7 Message to staus ' . HL7InboundBase::STATUS_FORM_ERROR, HL7InboundBase::LOG_LEVEL_MINIMUM);
 		}
 		else if (!$this->is_valid) {
 			$this->hl7Inbound->setStatus(HL7InboundTable::STATUS_DUPLICATE);
 			$set_processed = $this->setProcessed(HL7InboundBase::STATUS_DUPLICATE);

 			$this->logMessage('Claim already handled - data ignored', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->logMessage('Processing is complete. Updated HL7 Message to staus ' . HL7InboundBase::STATUS_DUPLICATE, HL7InboundBase::LOG_LEVEL_MINIMUM);
 		}
 		else {
 			$claim->save();

 			$this->hl7Inbound
 				->setStatus(HL7InboundTable::STATUS_PROCESSED)
 				->setClaimId($claim->getId());

 			$set_processed = $this->setProcessed(HL7InboundBase::STATUS_PROCESSED); //after claim is saved --> set processed  to 1

 			$this->logMessage('Claim saved. Claim id: ' . $claim->id, HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->logMessage('Processing is complete. Updated HL7 Message to staus ' . HL7InboundBase::STATUS_PROCESSED, HL7InboundBase::LOG_LEVEL_MINIMUM);
 		}

 		$this->indent -= 2;
 	}
 	public function handleFT1s($claim, $ft1s, $spec_code)
 	{
 		$this->logMessage('handleFT1s() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		foreach ($ft1s as $ft1)
 		{
 			$req_diag_code = false;
 			$req_ref_doc = false;
 			$req_facility_num = false;
 			$req_admit_date = false;
 			$mode = '';

 			$item = new ClaimItem();
 			$item->client_id = $this->client_id;

 			// FT1 25.1 - Service Code
 			$service_code = trim(strtoupper($ft1['FT1_F25_C1']));
 			$service_code = $this->handleClientServiceCodeModification($service_code);

 			if ($service_code) {
 				$sc = substr($service_code, 0, 4);

 				$serviceCode = $this->getServiceCode($sc);
 				if ($serviceCode instanceof ServiceCode) {
 					$req_diag_code = $serviceCode->getReqDiagCode();
 					$req_ref_doc = $serviceCode->getReqRefDoctor();
 					$req_facility_num = $serviceCode->getReqFacilityNum();
 					$req_admit_date = $serviceCode->getReqAdmitDate();

 					$this->ServiceCode = $serviceCode;
 				}
 				else {
 					$this->is_valid = false;
 					$this->logMessage('Service Code ' . $service_code . ' is invalid', HL7InboundBase::LOG_LEVEL_MINIMUM);
 					$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_INVALID_SERVICE_CODE, 'Service Code ' . $service_code . ' is invalid');
 				}

 				if (strlen($service_code) == 5) {
 					$mode = substr($service_code, 4, 1);
 					$this->mode = $mode;
 				}
 				else if (strlen($service_code) > 5) {
 					$this->is_valid = false;
 					$this->logMessage('Service code '. $service_code . ' is invalid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
 					$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_INVALID_SERVICE_CODE, 'Service code '. $service_code . ' is invalid.');
 				}

 				if (!$mode) {
 					$this->is_valid = false;
 					$this->logMessage('Service code '. $service_code . ' is missing A, B or C extension', HL7InboundBase::LOG_LEVEL_MINIMUM);
 					$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_INVALID_SERVICE_CODE, 'Service code '. $service_code . ' is missing A, B or C extension');
 				}
 			}
 			else {
 				$this->is_valid = false;
 				$this->logMessage('Service Code cannot be null or empty', HL7InboundBase::LOG_LEVEL_MINIMUM);
 				$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_INVALID_SERVICE_CODE, 'Service Code cannot be null or empty');
 			}

 			$item->setServiceCode($service_code);
 			$this->logMessage('Service Code set to ' . $service_code, HL7InboundBase::LOG_LEVEL_ALL);

 			// FT1 19.1 - Diagnostic Code
 			$item->setDiagCode(substr(trim(strtoupper($ft1['FT1_F19_C1'])), 0, 3));
 			$this->logMessage('Diagnostic Code set to ' . $item->getDiagCode(), HL7InboundBase::LOG_LEVEL_ALL);

 			if ($req_diag_code && !$item->getDiagCode()) {
 				$this->is_valid = false;
 				$this->logMessage('A diagnostic code is required for this service code to be valid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
 				$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'A diagnostic code is required for this service code to be valid.');

 			}
 			else if ($req_diag_code) {
 				$diag_code = $this->getDiagnosticCode($item->getDiagCode());
 				if (!$diag_code instanceof DiagCode) {
 					$this->is_valid = false;
 					$this->logMessage('The diagnostic code ' . $item->getDiagCode() . ' is invalid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
 					$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'The diagnostic code ' . $item->getDiagCode() . ' is invalid.');
 				}
 			}

 			if ($req_ref_doc && !$claim->getRefDoc()) {
 				$this->is_valid = false;
 				$this->logMessage('A referring doctor is required for this service code to be valid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
 				$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_REFERRING_DOCTOR, 'A referring doctor is required for this service code to be valid.');
 			}

 			// FT1 4.1 - Service Date
 			$service_date = $ft1['FT1_F4_C1'];
 			if ($service_date) {
 				$item->setServiceDate(date('Y-m-d', hype::parseDateToInt($service_date, 'YmdHis')));
 			}
 			else {
 				$this->is_valid = false;
 				$this->logMessage('A service date is required for this service code to be valid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
 				$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'A service date is required for this service code to be valid.');
 			}

 			// FT1 1.1 - Item ID
 			$item->setItemId(trim(strtoupper($ft1['FT1_F1_C1'])));

 			// FT1 10.1 - Number of Services
 			$item->setNumServ(trim($ft1['FT1_F10_C1']));
 			$item->setModifier($item->getNumServ() * 100);

 			// FT1 2.1 - HL7 Claim Item ID
 			$item->setHl7IDForPartner($ft1['FT1_F2_C1'], $this->hl7_partner);

 			$item->setStatus(ClaimItemTable::STATUS_UNSUBMITTED);

 			if ($serviceCode instanceof ServiceCode) {
 				$item->setBaseFee($serviceCode->determineFee($mode, $spec_code));
 				$item->setFeeSubm($item->getNumServ() * $item->getBaseFee());

 				$this->logMessage('Service Code: ' . $item['service_code'] . ' and fee Submitted: $' . number_format($item['fee_subm'], 2), HL7InboundBase::LOG_LEVEL_MINIMUM);
 			}

 			$claim->ClaimItem->add($item);
 		}

 		$this->indent -= 2;
 		return $claim;
 	}
 	public function handlePayProgThirdParty($claim)
 	{
 		$this->logMessage('handlePayProgThirdParty() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$in1 = $this->getIN1();

 		// IN1 4.1 - Third Party ID
 		$thirdParty = $this->getThirdParty($in1);

 		if ($thirdParty instanceof ThirdParty) {
 			$claim->setThirdPartyId($thirdParty->getId());
 			$claim->setThirdParty($thirdParty);

 			$this->logMessage('The Third Party is ' . $thirdParty->getName(), HL7InboundBase::LOG_LEVEL_ALL);
 		}
 		else {
 			$this->is_valid = false;
 			$this->logMessage('A valid Third Party is required', HL7InboundBase::LOG_LEVEL_ALL);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_PAY_PROG_REQUIREMENTS, 'A valid Third Party is required');
 		}

 		// IN1 2.1 - Policy Number
 		if ($in1['IN1_F2_C1']) {
 			$claim->setPolicyNumber(strtoupper(trim($in1['IN1_F2_C1'])));
 			$this->logMessage('Claim Policy Number set to ' . $claim->getPolicyNumber(), HL7InboundBase::LOG_LEVEL_ALL);
 		}
 		else {
 			$this->is_valid = false;
 			$this->logMessage('A policy number is required to process the Third Party claim.', HL7InboundBase::LOG_LEVEL_ALL);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_PAY_PROG_REQUIREMENTS, 'A policy number is required to process the Third Party claim.');
 		}

 		$claim->setBatchNumber(ClaimTable::CLAIM_TYPE_NAME_THIRD_PARTY);

 		$this->indent -= 2;
 		return $claim;
 	}
 	public function handlePayProgRMB($claim)
 	{
 		$this->logMessage('handlePayProgRMB() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$in1 = $this->getIN1();

 		// IN1 2.2 - Out Of Province HC #
 		$hcn = trim(strtoupper($in1['IN1_F2_C2']));

 		// IN1 19.3 - Patient Province
 		//$prov = trim(strtoupper($in1['IN1_F19_C3']));
 		$prov = trim(strtoupper(str_replace('RMB - ', '', $in1['IN1_F2_C1']))); 		// 2014-01-12 -> Province Code for RMB claims is in IN1_F2_C1 in the format "RMB - {ProvCode}"

 		if ($hcn) {
 			$claim->Patient->setHcnNum(hype::encryptHCN($hcn));
 			$claim->setHealthNum(hype::encryptHCN($hcn));
 			$this->logMessage('Set patient and claim Health Card Number to ' . $claim->getHealthNum(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$claim->Patient->setProvince($prov);
 		$claim->setProvince($prov);
 		$this->logMessage('Set patient and claim Province to ' . $claim->getProvince(), HL7InboundBase::LOG_LEVEL_ALL);

 		$this->indent -= 2;
 		return $claim;
 	}
 	public function handlePayProg($claim, $ft1s)
 	{
 		$this->logMessage('handlePayProg() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		if (!count($ft1s)) {
 			$this->is_valid = false;
 			$this->logMessage('Claims require at least one FT1 record', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_SERVICES, 'Claims require at least one FT1 record');

 			$this->indent -= 2;
 			return $claim;
 		}

 		// FT1 18.1 - Claim Type [THIRD PART, RMB, HCP - P]
 		$pay_prog = trim(strtoupper($ft1s[0]['FT1_F18_C1']));

 		if ($pay_prog == 'THIRD PART') {
 			$pay_prog = ClaimTable::CLAIM_TYPE_THIRD_PARTY;
 		}
 		$claim->setPayProg($pay_prog);
 		$this->logMessage('Claim Type set to ' . $claim->getPayProg(), HL7InboundBase::LOG_LEVEL_ALL);

 		// FT1 23.1 - HL7 ID for Claim
 		$claim->setHl7IDForPartner($ft1s[0]['FT1_F23_C1'], $this->hl7_partner);

 		if ($claim->getPayProg() == ClaimTable::CLAIM_TYPE_THIRD_PARTY) {
 			$claim = $this->handlePayProgThirdParty($claim);
 		}
 		else if ($claim->getPayProg() == ClaimTable::CLAIM_TYPE_RMB) {
 			$claim = $this->handlePayProgRMB($claim);
 		}
 		else if ($claim->getPayProg() == ClaimTable::CLAIM_TYPE_DIRECT) {
 			$claim->setBatchNumber(ClaimTable::CLAIM_TYPE_NAME_DIRECT);
 		}

 		if (!$claim->getBatchNumber()) {
 			$claim->setBatchNumber(ClaimTable::EMPTY_BATCH);
 		}

 		$this->indent -= 2;
 		return $claim;
 	}
 	public function getReferringDoctor($pv1)
 	{
 		$this->logMessage('getReferringDoctor() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$hl7PartnerId = strtoupper(trim($pv1['PV1_F8_C1']));
 		$referringDoctor = $this->getReferringDoctorFromHl7PartnerId($hl7PartnerId);

 		if (!$referringDoctor instanceof ReferringDoctor) {
 			$referringDoctor = $this->createNewReferringDoctor($pv1);
 		}
 		$referringDoctor = $this->updateReferringDoctor($referringDoctor, $pv1);

 		$this->indent -= 2;
 		return $referringDoctor;
 	}
 	public function getThirdParty($in1)
 	{
 		$this->logMessage('getThirdParty() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		// IN1 4.1 - Third Party Provider HL7 ID
 		$partnerID = trim(strtoupper($in1['IN1_F4_C1']));

 		$thirdParty = $this->getThirdPartyFromHl7PartnerId($partnerID);

 		$this->indent -= 2;
 		return $thirdParty;
 	}
 	public function updateReferringDoctor($referringDoctor, $pv1)
 	{
 		$this->logMessage('updateReferringDoctor() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		if (strlen($pv1['PV1_F8_C1']) != 6) {
 			$this->logMessage('No Referring Doctor Provider Number sent', HL7InboundBase::LOG_LEVEL_ALL);
 			return null;
 		}

 		// PV1 8.1 - Referring Doctor Provider Number
 		if ($pv1['PV1_F8_C1']) {
 			$referringDoctor->setProviderNum($pv1['PV1_F8_C1']);
 			$this->logMessage('Set Referring Doctor Provider Num to ' . $referringDoctor->getProviderNum(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PV1 8.5 - Referring Doctor Spec Code
 		if ($pv1['PV1_F8_C5'] && strlen($pv1['PV1_F8_C5']) < 3) {
 			$referringDoctor->setSpecCode($pv1['PV1_F8_C5']);
 			$this->logMessage('Set Referring Doctor Spec Code to ' . $referringDoctor->getSpecCode(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PV1 8.3 - Referring Doctor Fname
 		if ($pv1['PV1_F8_C3']) {
 			$referringDoctor->setFname($pv1['PV1_F8_C3']);
 			$this->logMessage('Set Referring Doctor First Name to ' . $referringDoctor->getFname(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PV1 8.2 - Referring Doctor Lname
 		if ($pv1['PV1_F8_C2']) {
 			$referringDoctor->setLname($pv1['PV1_F8_C2']);
 			$this->logMessage('Set Referring Doctor Last Name to ' . $referringDoctor->getLname(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$referringDoctor->setActive(true);
 		$referringDoctor->setHl7IDForPartner($referringDoctor->getProviderNum(), $this->hl7_partner);

		$referringDoctor->save();

 		$this->indent -= 2;
 		return $referringDoctor;
 	}
 	public function getPatient()
 	{
 		$this->logMessage('getPatient() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$pid = $this->getPID();

 		$hl7PartnerId = $pid['PID_F4_C1'];
 		$fname = trim(strtoupper($pid['PID_F5_C2']));
 		$lname = trim(strtoupper($pid['PID_F5_C1']));
 		$hcn = trim(strtoupper($pid['PID_F20_C1']));
 		$dob = date('Y-m-d', hype::parseDateToInt(trim($pid['PID_F7_C1']), 'YmdHis'));
 		$versionCode = trim(strtoupper($pid['PID_F20_C2']));

 		$patient = $this->getPatientFromHl7PartnerId($hl7PartnerId);

 		if (!$patient instanceof Patient && $hcn && $fname && $lname) {
 			$patient = $this->getPatientFromNameAndHealthCardNumber($fname, $lname, $hcn, $versionCode);
 		}
 		if (!$patient instanceof Patient && $fname && $lname && $dob) {
 			$patient = $this->getPatientFromNameAndDob($fname, $lname, $dob);
 		}
 		if (!$patient instanceof Patient) {
 			$patient = $this->createNewPatient();
 		}

 		$patient = $this->updatePatient($patient, $pid);

 		$this->indent -= 2;
 		return $patient;
 	}
 	public function updatePatient($patient, $pid)
 	{
 		$this->logMessage('updatePatient() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		// PID 5.2 - First Name
 		if ($pid['PID_F5_C2']) {
 			$patient->setFname($pid['PID_F5_C2']);
 			$this->logMessage('Patient First Name set to ' . $patient->getFname(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PID 5.1 - Last Name
 		if ($pid['PID_F5_C1']) {
 			$patient->setLname($pid['PID_F5_C1']);
 			$this->logMessage('Patient Last Name set to ' . $patient->getLname(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PID 20.1 - HC#
 		if ($pid['PID_F20_C1']) {
 			$patient->setHcnNum(hype::encryptHCN($pid['PID_F20_C1']));
 			$this->logMessage('Patient HC # set to ' . $patient->getHCN(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PID 20.2 - Version Code
 		if ($pid['PID_F20_C2']) {
 			$patient->setHcnVersionCode($pid['PID_F20_C2']);
 			$this->logMessage('Patient Version Code set to ' . $patient->getHcnVersionCode(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PID 20.3 - Expiry Date
 		if ($pid['PID_F20_C3']) {
 			$patient->setHcnExpYear($pid['PID_F20_C3']);
 			$this->logMessage('Patient HC Expiry set to ' . $patient->getHcnExpYear(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PID 7.1 - Date Of Birth
 		if ($pid['PID_F7_C1']) {
 			$dob = date('Y-m-d', hype::parseDateToInt(trim($pid['PID_F7_C1']), 'YmdHis'));
 			$patient->setDob($dob);
 			$this->logMessage('Patient DOB set to ' . $patient->getDob(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PID 8.1 - Sex
 		if ($pid['PID_F8_C1']) {
 			$sex = strtoupper(trim($pid['PID_F8_C1']));
 			$patient->setSex($sex[0]);
 			$this->logMessage('Patient Sex set to ' . $patient->getSex(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PID 11.1 - Address
 		if ($pid['PID_F11_C1']) {
 			$patient->setAddress($pid['PID_F11_C1']);
 			$this->logMessage('Patient Address set to ' . $patient->getAddress(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PID 11.3 - City
 		if ($pid['PID_F11_C3']) {
 			$patient->setCity($pid['PID_F11_C3']);
 			$this->logMessage('Patient City set to ' . $patient->getCity(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		if (!$patient->getProvince()) {
 			$patient->setProvince('ON');
 			$this->logMessage('Patient Province set to ' . $patient->getProvince(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PID 11.5 - Postal Code
 		if ($pid['PID_F11_C5']) {
 			$patient->setPostalCode($pid['PID_F11_C5']);
 			$this->logMessage('Patient Postal Code set to ' . $patient->getPostalCode(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PID 11.6 - Country
 		if ($pid['PID_F11_C6']) {
 			$patient->setCountry($pid['PID_F11_C6']);
 			$this->logMessage('Patient Country set to ' . $patient->getCountry(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PID 13.1 - Home Phone
 		if ($pid['PID_F13_C1']) {
 			$patient->setHphone($pid['PID_F13_C1']);
 			$this->logMessage('Patient Home Phone set to ' . $patient->getHphone(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PID 4.1 - Chart Number
 		if ($pid['PID_F4_C1']) {
 			$patient->setHl7IDForPartner($pid['PID_F4_C1'], $this->hl7_partner);
 			$patient->setPatientNumber($pid['PID_F4_C1'] . '');
 			$this->logMessage('Chart Number set to ' . $patient->getPatientNumber(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		if (!$patient->getPatientStatusId()) {
 			$cp = Doctrine::getTable('ClientParam')->getParamForClient($this->client_id, 'patient_status_default');
 			if ($cp instanceof ClientParam) {
 				$patient['patient_status_id'] = $cp->value;
 				$this->logMessage('Patient Status ID set to ' . $patient->getPatientStatusID(), HL7InboundBase::LOG_LEVEL_ALL);
 			}
 		}

 		if (!$patient->getPatientTypeId()) {
 			$cp = Doctrine::getTable('ClientParam')->getParamForClient($this->client_id, 'default_patient_type_id');
 			if ($cp instanceof ClientParam) {
 				$patient['patient_type_id'] = $cp->value;
 				$this->logMessage('Patient Type ID set to ' . $patient->getPatientTypeID(), HL7InboundBase::LOG_LEVEL_ALL);
 			}
 		}

		$patient->save();
 		$this->logMessage('Saved Patient with ID ' . $patient->getId(), HL7InboundBase::LOG_LEVEL_MINIMUM);

 		$this->indent -= 2;
 		return $patient;
 	}
 	public function getLocation($pv1)
 	{
 		$this->logMessage('getLocation() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$hl7PartnerId = trim(strtoupper($pv1['PV1_F3_C1']));

 		return Doctrine_Query::create()
 			->from('Location l')
 			->addWhere('l.client_id = (?)', $this->client_id)
 			->addWhere('l.hl7_partner_id LIKE (?)', '%' . $this->hl7_partner . ':' . $hl7PartnerId . '^%')
 			->fetchOne();
 	}
 	protected function updateDoctor($doctor, $providerNumber, $groupNumber, $sli, $pv1)
 	{
 		$this->logMessage('updateDoctor() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$doctor->setGroupNum($groupNumber);
 		$this->logMessage('Doctor Group Number set to ' . $doctor->getGroupNum(), HL7InboundBase::LOG_LEVEL_ALL);

 		$doctor->setProviderNum($providerNumber);
 		$this->logMessage('Doctor Provider Number set to ' . $doctor->getProviderNum(), HL7InboundBase::LOG_LEVEL_ALL);

 		// PV1 7.5 - Attending Doctor Spec Code
 		$doctor->setSpecCodeId(strtoupper(trim($pv1['PV1_F7_C5'])));
 		$this->logMessage('Doctor Spec Code set to ' . $doctor->getSpecCodeId(), HL7InboundBase::LOG_LEVEL_ALL);

 		if (!$doctor->getSpecCodeId()) {
 			$this->is_valid = false;
 			$this->logMessage('Provider specialty code must be provided.', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_DOCTOR_REQUIREMENTS, 'Provider specialty code must be provided.');

 			$this->indent -= 2;
 			return null;
 		}

 		// PV1 7.3 - Attending Doctor First Name
 		$doctor->setFname(trim(strtoupper($pv1['PV1_F7_C3'])));
 		$this->logMessage('Doctor First Name set to ' . $doctor->getFname(), HL7InboundBase::LOG_LEVEL_ALL);

 		// PV1 7.2 - Attending Doctor Last Name
 		$doctor->setLname(trim(strtoupper($pv1['PV1_F7_C2'])));
 		$this->logMessage('Doctor Last Name set to ' . $doctor->getLname(), HL7InboundBase::LOG_LEVEL_ALL);

 		$doctor_code = $doctor['fname'][0] . $doctor['lname'][0];
 		$doctor->setQcDoctorCode(DoctorTable::checkDoctorCode($doctor_code, $doctor->getClientId()));
 		$this->logMessage('Doctor Code set to ' . $doctor->getQcDoctorCode(), HL7InboundBase::LOG_LEVEL_ALL);

 		$doctor->setSli($sli);
 		$this->logMessage('Doctor SLI set to ' . $doctor->getSli(), HL7InboundBase::LOG_LEVEL_ALL);

 		// PV1 7.4 - MOH Office ID
 		$mohOffice = $this->getMOHOfficeFromMOHOfficeCode(trim(strtoupper($pv1['PV1_F7_C4'])));
 		if ($mohOffice instanceof MOHOffice) {
 			$doctor->setMohOfficeId($mohOffice->getId());
 			$this->logMessage('Doctor MOH Office set to ' . $pv1['PV1_F7_C4'], HL7InboundBase::LOG_LEVEL_ALL);
 		}
 		else {
 			$this->is_valid = false;
 			$this->logMessage('MOH Office code '. $pv1['PV1_F7_C4'] . ' is invalid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_DOCTOR_REQUIREMENTS, 'MOH Office code '. $pv1['PV1_F7_C4'] . ' is invalid.');

 			$this->indent -= 2;
 			return null;
 		}

 		$doctor->setProvince('ON');
 		$doctor->setHl7IDForPartner($doctor->getProviderNum(), $this->hl7_partner);
 		$doctor->setActive(true);
 		$doctor->save();

 		$this->indent -= 2;
 		return $doctor;
 	}
}