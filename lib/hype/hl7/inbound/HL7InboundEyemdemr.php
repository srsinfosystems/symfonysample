<?php

class HL7InboundEyemdemr extends HL7InboundBase
{
 	public function processDFT()
 	{
 		$this->logMessage('Processing DFT Message.', HL7InboundBase::LOG_LEVEL_MINIMUM);

 		$claim = new Claim();
 		$patient = $this->getPatient();

 		$this->logMessage('Patient name: ' . $patient->full_name . ' (ID: ' . $patient->id . ')', HL7InboundBase::LOG_LEVEL_MINIMUM);

  		$claim->Patient = $patient;
 		$output = $claim->setPatientDetails($patient, true);

  		$this->hl7Inbound
  			->setPatient($patient)
  			->setFname($patient->getFname())
  			->setLname($patient->getLname());

 		foreach ($output as $line)
 		{
 			$this->logMessage($line, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$ft1_records = $this->getFT1();
 		$evn_record = $this->getEVN();
 		$pv1 = $this->getPV1();

  		$location = $this->getLocation($pv1['PV1_F39_C1']);
  		$claim->setLocation($location);
  		$this->hl7Inbound->setLocation($location);

  		$doctor = $this->getDoctor($pv1['PV1_F7_C1'], $pv1['PV1_F7_C2'], $pv1['PV1_F7_C3']);
  		if ($doctor instanceof Doctor) {
  			$claim->Doctor = $doctor;
  			$claim->setDoctorCode($doctor->getQcDoctorCode());
  			$this->hl7Inbound->setDoctorId($doctor->getId());
  		}

  		$last_diag_code = null;

 		$ref_doc = $this->getReferringDoctorByID($pv1['PV1_F8_C1']);
 		if ($ref_doc instanceof ReferringDoctor) {
 			$claim->setRefDocId($ref_doc->id);
 			$claim->setRefDoc($ref_doc->provider_num);
 			if (count($ref_doc->ReferringDoctorAddress)) {
 				$claim->setRefDocAddrId($ref_doc->ReferringDoctorAddress[0]->getId());
 			}

 			$this->logMessage("The Referring Doctor is " . $ref_doc->getProviderNum() . ' ' . $ref_doc->getFname() . ' ' . $ref_doc->getLname(), HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->logMessage('Claim referring doctor id set to ' . $claim->ref_doc_id, HL7InboundBase::LOG_LEVEL_ALL);
 			$this->logMessage('Claim referring doctor address id set to ' . $claim->ref_doc_addr_id, HL7InboundBase::LOG_LEVEL_ALL);
 			$this->logMessage('Claim referring doctor number set to ' . $claim->ref_doc, HL7InboundBase::LOG_LEVEL_ALL);
 		}

  		foreach ($ft1_records as $ft1)
  		{
  			$claim = $this->handleFT1($claim, $ft1);
 		}

  		$claim = $this->handleClientCustomizations($claim);

  		$claim->setPayProg(ClaimTable::CLAIM_TYPE_HCPP);
  		$claim->setBatchNumber(ClaimTable::EMPTY_BATCH);
  		$claim->setDateCreated(date('Y-m-d H:i:s'));

 		if ($claim->getPayProg() == ClaimTable::CLAIM_TYPE_HCPP && !$claim->getHealthNum()) {
 			$this->is_valid = false;
 			$this->logMessage('Claim is invalid due to missing health card number', HL7InboundBase::LOG_LEVEL_ALL);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_HEALTH_CARD, 'Claim is invalid due to missing health card number');
 		}

  		$attendees = $this->getAppointmentAttendeesForClaim($claim);

 		if ($this->is_valid) {
  			$claim->save();
  			foreach ($attendees as $attendee)
  			{
  				$attendee->claim_id = $claim->id;
  				$attendee->Appointment->status = AppointmentTable::STATUS_CLAIM;

  				$attendee->save();
  			}
  			$this->setProcessed(HL7InboundBase::STATUS_PROCESSED);
  			$this->hl7Inbound->setStatus(HL7InboundTable::STATUS_PROCESSED)
  				->setClaimId($claim->getId());

  			$this->logMessage('Finished Processing the claim. Claim ID: ' . $claim->getId(), HL7InboundBase::LOG_LEVEL_ALL);
 		}
  		else {
  			$attendee_id = null;
  			if (count($attendees)) {
  				$attendee_id = $attendees[0]->getId();
  			}

  			$this->logMessage('Finished Processing the claim with errors.', HL7InboundBase::LOG_LEVEL_ALL);

  			$temporary_form = TemporaryFormTable::createHL7Claim($claim, $attendee_id);
  			$this->setProcessed(HL7InboundBase::STATUS_FORM_ERROR);
  			$this->hl7Inbound->setStatus(HL7InboundTable::STATUS_FORM_ERROR);
  			$this->hl7Inbound->setTemporaryFormId($temporary_form->getId());
  		}
 	}
 	private function handleFT1($claim, $ft1)
 	{
 		$this->logMessage('handleFT1() called.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$check_interval = false;
 		$cp = Doctrine::getTable('ClientParam')->getParamForClient($this->client_id, 'claim_item_interval_check');
 		if ($cp instanceof ClientParam) {
 			$check_interval = (bool) $cp->value;
 		}

 		$req_ref_doc = $req_facility_num = $req_admit_date = $interval = $req_diag_code = false;
 		$item = new ClaimItem();
 		$item->client_id = $this->client_id;

 		$this->logMessage('Claim Item created.', HL7InboundBase::LOG_LEVEL_ALL);

 		$service_code = $this->getServiceCode($ft1['FT1_F25_C1']);
 		$item->setServiceCode($ft1['FT1_F25_C1']);
 		$this->logMessage('Claim Item service code set to ' . $item->getServiceCode(), HL7InboundBase::LOG_LEVEL_ALL);

 		if ($service_code instanceof ServiceCode) {
 			$req_diag_code = $service_code->getReqDiagCode();
 			$req_ref_doc = $service_code->getReqRefDoctor();
 			$req_facility_num = $service_code->getReqFacilityNum();
 			$req_admit_date = $service_code->getReqAdmitDate();
 			$interval = $service_code->getInterVal();
 		}
 		else {
 			$this->is_valid = false;
 			$this->logMessage('Service Code ' . $item->getServiceCode() . ' is invalid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_INVALID_SERVICE_CODE, 'Service Code ' . $item->getServiceCode() . ' is invalid.');
 		}

 		$diag_code = substr(trim($ft1['FT1_F19_C1']), 0, 3);
 		if (intval($diag_code)) {
 			$this->logMessage('The diagnostic code is ' . $diag_code, HL7InboundBase::LOG_LEVEL_ALL);

 			$diagnostic_code = $this->getDiagnosticCode($diag_code);
 			if ($diagnostic_code instanceof DiagCode) {
 				$item->setDiagCode($diag_code);
 				$this->logMessage('Claim Item diagnostic code set to ' . $item->getDiagCode(), HL7InboundBase::LOG_LEVEL_ALL);
 			}
 		}

 		if ($req_diag_code && !$item->getDiagCode()) {
 			$this->is_valid = false;
 			if (intval($diag_code)) {
 				$item->setDiagCode($diag_code);
 				$this->logMessage('Claim Item diagnostic code set to ' . $item->getDiagCode(), HL7InboundBase::LOG_LEVEL_ALL);
 				$this->logMessage('The diagnostic code ' . $diag_code . ' is invalid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
 				$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'The diagnostic code ' . $diag_code . ' is invalid.');
 			}
 			else {
 				$this->logMessage('A diagnostic code is required for this service code to be valid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
 				$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'A diagnostic code is required for this service code to be valid.');
 			}
 		}

 		if ($req_ref_doc && !$claim->ref_doc_id) {
 			$this->is_valid = false;
 			$this->logMessage('A referring doctor is required for this service code to be valid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'A referring doctor is required for this service code to be valid.');
 		}

 		$service_date = $ft1['FT1_F4_C1'];
 		$numServ = intval($ft1['FT1_F10_C1']) ? intval($ft1['FT1_F10_C1']) : 1;
 		$modifier = intval($ft1['FT1_F26_C1']) ? intval($ft1['FT1_F26_C1']) : 100;

 		if ($modifier != 100) {
 			$numServ = ceil($modifier / 100);
 		}
 		else if ($numServ != 1) {
 			$modifier = $numServ * 100;
 		}

 		$item->setServiceDate(substr($service_date, 0, 4) . "-" . substr($service_date, 4, 2) . "-" . substr($service_date, 6, 2));
 		$item->setStatus(ClaimItemTable::STATUS_UNSUBMITTED);
 		$item->setItemId(trim(strtoupper($ft1['FT1_F1_C1'])));
 		$item->setNumServ($numServ);
 		$item->setModifier($modifier);

 		$this->logMessage('Claim Item service date set to ' . $item->getServiceDate(), HL7InboundBase::LOG_LEVEL_ALL);
 		$this->logMessage('Claim Item status set to unsubmitted.', HL7InboundBase::LOG_LEVEL_ALL);
 		$this->logMessage('Claim Item item id set to ' . $item->getItemId(), HL7InboundBase::LOG_LEVEL_ALL);
 		$this->logMessage('Claim Item number of services set to ' . $item->getNumServ(), HL7InboundBase::LOG_LEVEL_ALL);
 		$this->logMessage('Claim Item modifier set to ' . $item->getModifier(), HL7InboundBase::LOG_LEVEL_ALL);

 		if ($interval && $check_interval) {
 			$prev_items = $claim->Patient->checkForPreviousServices($item->service_date, $service_code->inter_val, $service_code->service_code);

 			if (count($prev_items)) {
 				$this->is_valid = false;
 				$this->logMessage('Cannot bill this service again within a ' . $interval . ' interval', HL7InboundBase::LOG_LEVEL_MINIMUM);
 				$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_INVALID_SERVICE_CODE, 'Cannot bill this service again within a ' . $interval . ' interval');
 			}
 		}

 		if ($service_code instanceof ServiceCode) {
 			if (strlen($item->service_code) == 4) {
 				$item->service_code .= 'A';
 			}

 			$sc = substr($item->getServiceCode(), 0, 4);
 			$mode  = substr($item->getServiceCode(), 4, 1);

 			$item->setBaseFee($service_code->determineFee($mode, $claim->Doctor->spec_code_id));
 			$item->setFeeSubm(number_format($item->getModifier() * $item->getBaseFee() / 100, 2, '.', ''));
 			$this->logMessage('Claim Item fee_subm set to ' . $item->getFeeSubm(), HL7InboundBase::LOG_LEVEL_ALL);
 			$this->logMessage('Claim Item base_fee set to ' . $item->getBaseFee(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$claim->ClaimItem->add($item);

 		$this->indent -= 2;
 		return $claim;
 	}
  	private function getPatient()
  	{
  		$this->logMessage('getPatient called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
  		$this->indent += 2;

  		$pid = $this->getPID();
  		$hype_patient_id = strtoupper(trim($pid['PID_F2_C1']));
  		$fname = strtoupper(trim($pid['PID_F5_C2']));
  		$lname = strtoupper(trim($pid['PID_F5_C1']));
  		$hcn = strtoupper(trim($pid['PID_F21_C1']));
   		$dob = date('Y-m-d', hype::parseDateToInt($pid['PID_F7_C1'], 'Ymd'));
  		$vc = '';

  	 	if (strpos($hcn, ' ')) {
  			list ($hcn, $vc) = explode(' ', $hcn);
  		}

  		$this->logMessage('Patient Identified as ' . $fname . ' ' . $lname, HL7InboundBase::LOG_LEVEL_ALL);
  		$this->logMessage('HYPE Patient ID is ' . $hype_patient_id, HL7InboundBase::LOG_LEVEL_ALL);

  		$patient = $this->getPatientFromHypeId($hype_patient_id);

  		if (!$patient instanceof Patient) {
  			$patient = $this->getPatientFromNameAndHealthCardNumber($fname, $lname, $hcn, $vc);
  		}

   		// check against fname, lname, dob
   		if (!$patient instanceof Patient && $dob) {
   			$patient = $this->getPatientFromNameAndDob($fname, $lname, $dob);
   		}

   		if ($patient instanceof Patient) {
   			$this->logMessage('Patient Identified as ' . $fname . ' ' . $lname . ' (ID: ' . $patient->getId(), HL7InboundBase::LOG_LEVEL_ALL);
   			$patient = $this->updatePatient($pid, $patient);
   		}
   		else {
   			$this->logMessage('No patient found.', HL7InboundBase::LOG_LEVEL_ALL);
   			$patient = $this->createPatient($pid);
   		}

   		$this->indent -= 2;
  		return $patient;
  	}
 	private function createPatient($pid)
 	{
 		$this->logMessage('createPatient() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$patient = new Patient();
  		$patient->setClientID($this->client_id);
  		$this->logMessage('Patient client ID set to ' . $patient->getClientID(), HL7InboundBase::LOG_LEVEL_ALL);
  		$patient->setHl7IDForPartner($pid['PID_F2_C1'], $this->hl7_partner);
  		$patient->setPatientNumber($pid['PID_F2_C1']);
  		$this->logMessage('Patient Chart Number set to ' . $patient->getPatientNumber(), HL7InboundBase::LOG_LEVEL_ALL);

  		$cp = Doctrine::getTable('ClientParam')->getParamForClient($this->client_id, 'patient_status_default');
  		if ($cp instanceof ClientParam) {
  			$patient->setPatientStatusId($cp->value);
  		}

  		$cp = Doctrine::getTable('ClientParam')->getParamForClient($this->client_id, 'default_patient_type_id');
  		if ($cp instanceof ClientParam) {
  			$patient->setPatientTypeId($cp->value);
  		}

 		$patient = $this->updatePatient($pid, $patient);

 		$this->indent -= 2;
 		return $patient;
 	}
 	private function updatePatient($pid, $patient)
 	{
 		$this->logMessage('updatePatient() called.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$fname = strtoupper(trim($pid['PID_F5_C2']));
 		if ($fname && $fname != $patient->getFname()) {
 			$this->logMessage('Patient First Name set to ' . $fname, HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setFname($fname);
 		}

 		$lname = strtoupper(trim($pid['PID_F5_C1']));
 		if ($lname && $lname != $patient->getLname()) {
 			$patient->setLname($lname);
 			$this->logMessage('Patient Last Name set to ' . $lname, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$sex = strtoupper(trim($pid['PID_F8_C1']));
 		if ($sex && $sex != $patient->getSex()) {
 			$patient->setSex($sex[0]);
 			$this->logMessage('Patient Sex set to ' . $sex[0], HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$address = strtoupper(trim($pid['PID_F11_C1']));
 		if ($address && $address != $patient->getAddress()) {
 			$patient->setAddress($address);
 			$this->logMessage('Patient Address set to ' . $address, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$city = strtoupper(trim($pid['PID_F11_C3']));
 		if ($city && $city != $patient->getCity()) {
 			$patient->setCity($city);
 			$this->logMessage('Patient City set to ' . $city, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$province = strtoupper(trim($pid['PID_F11_C4']));
 		if ($province && $province != $patient->getProvince()) {
 			$patient->setProvince($province);
  			$patient->setMohProvince($patient->getProvince());
  			$this->logMessage('Patient Province set to ' . $province, HL7InboundBase::LOG_LEVEL_ALL);
 		}
 		else if (!$patient->getProvince()) {
 			$patient->setProvince('ON');
 			$patient->setMohProvince($patient->getProvince());
  			$this->logMessage('Patient Province set to ON', HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$postal = strtoupper(trim($pid['PID_F11_C5']));
 		if ($postal && $postal != $patient->getPostalCode()) {
 			$patient->setPostalCode($city);
  			$this->logMessage('Patient Postal Code set to ' . $postal, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$country = strtoupper(trim($pid['PID_F11_C6']));
 		if ($country && $country != $patient->getCountry()) {
 			$patient->setCountry($country);
  			$this->logMessage('Patient Country set to ' . $country, HL7InboundBase::LOG_LEVEL_ALL);
 		}
 		else if (!$patient->getCountry()) {
 			$patient->setCountry('CAN');
  			$this->logMessage('Patient Country set to CAN', HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$phone = $pid['PID_F13_C1'];
 		if ($phone && $phone != $patient->getHphone()) {
 			$patient->setHphone($phone);
  			$this->logMessage('Patient Home Phone set to ' . $phone, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$dob = $pid['PID_F7_C1'];
 		if ($dob) {
 			$patient->setDob(substr($dob,0,4) . "-" . substr($dob,4,2) . "-" . substr($dob,6,2));
  			$this->logMessage('Patient DOB set to ' . $patient->getDob(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$hcn = $pid['PID_F21_C1'];
 		$vc = '';

 		if (strpos($hcn, ' ') !== false) {
 			list ($hcn, $vc) = explode(' ', $hcn);
 		}

 		if ($hcn) {
 			$patient->setLast_4Hcn(substr($hcn, -4));
 			$patient->setHcnNum(hype::encryptHCN($hcn));
  			$this->logMessage('Patient HC # set to ' . $hcn, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		if ($vc) {
 			$patient->setHcnVersionCode($vc);
  			$this->logMessage('Patient Version Code set to ' . $vc, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$patient->save();
 		return $patient;
 	}
  	private function getLocation($location_id)
  	{
  		$this->logMessage('getLocation(' . $location_id . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
  		$this->indent += 2;

 		$location = Doctrine_Query::create()
 			->from('Location l')
 			->addWhere('l.client_id = (?)', $this->client_id)
 			->addWhere('l.id = (?)', $location_id)
 			->fetchOne();

  		if (!$location instanceof Location) {
  			$this->is_valid = false;
  			$this->logMessage('The location ID '. $location_id . ' is invalid.', HL7InboundBase::LOG_LEVEL_ALL);
  			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_LOCATION, 'The location ID '. $location_id . ' is invalid.');
  		}
  		else {
  			$this->logMessage('Location: ' . $location->getDropdownString() . ' (ID: ' . $location->getId() . ')', HL7InboundBase::LOG_LEVEL_ALL);
  		}

  		$this->indent -= 2;
  		return $location;
  	}
  	private function getDoctor($id, $lname, $fname)
  	{
  		$this->logMessage('getDoctor(' . $id . ', ' . $lname . ', ' . $fname . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
  		$this->indent += 2;

  		$doctor = Doctrine_Query::create()
  			->from('Doctor d')
  			->addWhere('d.client_id = (?)', $this->client_id)
  			->addWhere('d.id = (?)', $id)
  			->fetchOne();

  		if (!$doctor instanceof Doctor) {
  			$this->is_valid = false;
  			$this->logMessage('The doctor ' . $fname . ' ' . $lname . ' is invalid.', HL7InboundBase::LOG_LEVEL_ALL);
  			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_DOCTOR, 'The doctor ' . $fname . ' ' . $lname . ' is invalid.');
  		}
  		else {
  			$this->logMessage('Doctor: ' . $doctor->getDropdownString() . ' (ID: ' . $doctor->getId() . ')', HL7InboundBase::LOG_LEVEL_ALL);
  		}

  		$this->indent -= 2;
  		return $doctor;
  	}
 	private function getReferringDoctorByID($ref_doctor_id)
 	{
 		$this->logMessage('getReferringDoctorByID(' . $ref_doctor_id . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

 		return Doctrine_Query::create()
 			->from('ReferringDoctor r')
 			->leftJoin('r.ReferringDoctorAddress')
 			->addWhere('r.client_id = (?)', $this->client_id)
 			->addWhere('r.id = (?)', $ref_doctor_id)
 			->fetchOne();
 	}
 	private function getAppointmentAttendeesForClaim($claim)
 	{
 		$this->logMessage('getAppointmentAttendeesForClaim() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$rs = new Doctrine_Collection('AppointmentAttendee');

 		$patient_id = $claim->Patient->getId();
 		$doctor_id = $claim->Doctor->getId();
 		$fname = $claim->getFname();
 		$lname = $claim->getLname();

 		foreach ($claim->ClaimItem as $item)
 		{
 			$date = $item->getServiceDate();

 			$appointment = null;
 			if ($patient_id) {
 				$appointment = $this->getAttendeeByPatientID($patient_id, $doctor_id, $claim->getClientId(), $date);
 			}
 			else {
 				$appointment = $this->getAttendeeByPatientName($fname, $lname, $doctor_id, $claim->getClientId(), $date);
 			}

 			if ($appointment instanceof AppointmentAttendee) {
 				$rs->add($appointment);
 			}
 		}

 		$this->indent -= 2;
 		return $rs;
 	}
 	private function getAttendeeByPatientID($patient_id, $doctor_id, $client_id, $date)
 	{
 		$this->logMessage('getAttendeeByPatientID(' . $patient_id . ', ' . $doctor_id . ', ' . $client_id . ', ' . $date . ')', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

 		return Doctrine_Query::create()
 			->from('AppointmentAttendee att')
 			->leftJoin('att.Appointment a')
 			->addWhere('a.client_id = (?)', $client_id)
 			->addWhere('att.patient_id = (?)', $patient_id)
 			->addWhere('a.start_date between (?) and (?)', array($date . ' 00:00:00', $date . ' 23:59:59'))
 			->addWhere('att.claim_id IS NULL')
 			->addWhere('a.status = (?) OR a.status = (?)', array(AppointmentTable::STATUS_ATTENDED, AppointmentTable::STATUS_POSTED))
 			->fetchOne();
 	}
 	private function getAttendeeByPatientName($fname, $lname, $doctor_id, $client_id, $date)
 	{
 		$this->logMessage('getAttendeeByPatientName(' . $fname . ', ' . $lname . ', ' . $doctor_id . ', ' . $client_id . ', ' . $date . ')', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

 		return Doctrine_Query::create()
 		->from('AppointmentAttendee att')
 		->leftJoin('att.Appointment a')
 		->addWhere('a.client_id = (?)', $client_id)
 		->addWhere('att.patient_fname = (?)', $fname)
 		->addWhere('att.patient_lname = (?)', $lname)
 		->addWhere('a.start_date between (?) and (?)', array($date . ' 00:00:00', $date . ' 23:59:59'))
 		->addWhere('att.claim_id IS NULL')
 		->addWhere('a.status = (?) OR a.status = (?)', array(AppointmentTable::STATUS_ATTENDED, AppointmentTable::STATUS_POSTED))
 		->fetchOne();
 	}
}
