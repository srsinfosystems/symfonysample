<?php

class HL7InboundRamsoft extends HL7InboundBase
{
	// PID 1.1 - 1 - Not Mapped
	// PID 2.1 - RAMSOFT Patient ID
	// PID 3.1 - Visit Number - Not Mapped
	// PID 3.4 - ? - Not Mapped
	// PID 3.5 - ? - Not Mapped
	// PID 5.1 - Last Name
	// PID 5.2 - First Name
	// PID 5.3 - Middle Name - Not Mapped
	// PID 5.4 - Suffix - Not Mapped
	// PID 5.5 - Prefix - Not Mapped
	// PID 7.1 - DOB - format yyyymmdd
	// PID 8.1 - Sex [M|F]
	// PID 10.1 - ? - Not Mapped
	// PID 10.2 - ? - Not Mapped
	// PID 10.3 - ? - Not Mapped
	// PID 11.1 - Street Address Pt 1
	// PID 11.3 - City
	// PID 11.4 - Province
	// PID 11.5 - Postal Code
	// PID 11.6 - Country
	// PID 13.1 - Home Phone Number
	// PID 13.4 - Email Address - Not Mapped - should only be one field
	// PID 13.7 - Cell Phone Number
	// PID 14.1 - Work Phone Number
	// PID 15.1 - Language Code - Not Mapped
	// PID 16.1 - Marital Status - Not Mapped
	// PID 18.1 - HC # - Not Mapped
	// PID 18.2 - Version Code - Not Mapped
	// PID 18.3 - Expiry Date (yyyymmdd) - Not Mapped
	// PID 19.1 - ? - Not Mapped
	// PID 20.1 - Health Card Number
	// PID 20.2 - Version Code
	// PID 20.3 - Expiry Date (yyyy-mm-dd)

	// PV1 3.1 - Location Code - Location Partner ID
	// PV1 7.1 - Attending Doctor - Provider Number
	// PV1 7.2 - Attending Doctor - Last Name
	// PV1 7.3 - Attending Doctor - First Name
	// PV1 8.1 - Referring Doctor - Provider Number
	// PV1 8.2 - Referring Doctor - Last Name
	// PV1 8.3 - Referring Doctor - First Name

	// FT1 1-1 item number
	// FT1 2-1 not mapped
	// FT1 4-1 date of service
	// FT1 5-1 not mapped
	// FT1 6-1 not mapped
	// FT1 7-1 not mapped
	// FT1 10-1 number of services
	// FT1 14-1 health card number - not mapped
	// FT1 16-4 location name - not mapepd
	// FT1 18-1 claim type
	// FT1 19-1 diagnostic code
	// FT1 19-2 not mapped
	// FT1 20-1 attending doctor provider number not mapped
	// FT1 20-2 attending doctor lname not mapped
	// FT1 20-3 attending doctor fname not mapped
	// FT1 21-1 referring doctor provider number not mapped
	// FT1 21-2 referring doctor not mapped
	// FT1 21-3 referring doctor not mapped
	// FT1 21-5 referring doctor not mapped
	// FT1 21-6 referring doctor not mapped
	// FT1 23-1 order number not mapped
	// FT1 23-2 not mapped
	// FT1 25-1 service code
	// FT1 25-2 not mapped
	// FT1 25-3 not mapped

 	private $default_refdoc_spec_code = '00';
 	private $default_spec_code = '33';
 	private $default_group_number = '0000';
 	private $default_pay_prog = ClaimTable::CLAIM_TYPE_HCPP;
 	private $serviceCodes = null;
 	private $group_numbers = array();

 	public function processDFT()
 	{
 		$this->logMessage('processDFT() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;
 		$this->logMessage('Processing DFT Message', HL7InboundBase::LOG_LEVEL_MINIMUM);

 		$claim = new Claim();
 		$patient = $this->handlePID();
 		$location = $this->getLocation();

 		if (!$location instanceof Location) {
 			$this->is_valid = false;
 			$this->logMessage('The location ID is invalid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$location = new Location();
 		}
 		else {
 			if (!$patient['location_id']) {
 				$patient['location_id'] = $location['id'];
 				$patient->save();
 			}
 			$claim->location_id = $location['id'];
 			$claim->Location = $location;

 			$this->logMessage('Location of service: ' . $location->code, HL7InboundBase::LOG_LEVEL_MINIMUM);
 		}

 	 	$this->logMessage('Setting Patient Details to claim.', HL7InboundBase::LOG_LEVEL_ALL);
  		$output = $claim->setPatientDetails($patient, true);
 		foreach ($output as $line)
 		{
 			$this->logMessage($line, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$ft1s = $this->getFT1();
 		$pid = $this->getPID();
 		$pv1a = $this->getPV1();
 		$pv1b = $this->getPV1B();

 		$referring_doctor = $this->getReferringDoctor($pv1a, $pv1b);
 		if ($referring_doctor instanceof ReferringDoctor) {
 			$claim->ref_doc = $referring_doctor->provider_num;
 			$claim->ref_doc_id = $referring_doctor->id;
 			$claim->ref_doc_addr_id = $referring_doctor->ReferringDoctorAddress[0]->id;

 			$this->logMessage('Set claim refDoc num to ' . $claim->ref_doc, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$provider_number = trim(strtoupper($pv1a['PV1_F7_C1']));
 		while (strlen($provider_number) < 6)
 		{
 			$provider_number = '0' . $provider_number;
 		}
 		$this->logMessage('Provider Number is ' . $provider_number, HL7InboundBase::LOG_LEVEL_MINIMUM);

 		foreach ($ft1s as $ft1)
 		{
 			$claim = $this->handleFT1($claim, $ft1);
 		}

 		$distinct_groups = array_unique($this->group_numbers);

 		if (count($distinct_groups) == 1) {
 			$doctor = $this->getDoctor($provider_number, $this->group_numbers[0], $pv1a, $pv1b, $claim->Location);

 			if ($doctor instanceof Doctor) {
 				$claim->Doctor = $doctor;
 				$claim->doctor_id = $doctor->id;
 				$claim->doctor_code = $doctor->qc_doctor_code;

 				$this->logMessage('Set claim doctor code to ' . $claim->doctor_code, HL7InboundBase::LOG_LEVEL_ALL);
 			}
 			else {
 				$this->is_valid = false;
 				$this->logMessage('Invalid Doctor profile', HL7InboundBase::LOG_LEVEL_ALL);
 			}
 		}
 		else {
 			$this->is_valid = false;

 			$this->logMessage('UNHANDLED EXCEPTION: MORE THAN ONE GROUP NUMBER IDENTIFIED. MORE THAN ONE CLAIM REQUIRED!', HL7InboundBase::LOG_LEVEL_MINIMUM);
 		}

 	 	foreach ($claim->ClaimItem as $key => $claimItem)
  		{
  			$mode = $claimItem->service_code[strlen($claimItem->service_code) - 1];
  			$claimItem->base_fee = $this->serviceCodes[$key]->determineFee($mode, $doctor->spec_code_id);
  			$claimItem->fee_subm = $claimItem->num_serv * $claimItem->base_fee;
  			$this->logMessage('Set claim item ' . $key . ' fee_subm to ' . $claimItem->fee_subm, HL7InboundBase::LOG_LEVEL_ALL);
  		}

  		$claim->batch_number = ClaimTable::EMPTY_BATCH;
  		$claim->date_created = date('Y-m-d H:i:s');
  		$claim->manual_review = false;

  		if (!$claim->health_num && $claim->pay_prog == ClaimTable::CLAIM_TYPE_HCPP) {
  			$this->is_valid = false;
  			$this->logMessage('A health card number must be provided for this claim to be valid.', HL7InboundBase::LOG_LEVEL_MINIMUM);
  		}

  		// TODO: sli -> ignore for now

  		if (!$this->is_valid) {
  			$temporary_form = TemporaryFormTable::createHL7Claim($claim);
  			$set_processed = $this->setProcessed(HL7InboundBase::STATUS_FORM_ERROR);

  			$this->logMessage('Claim saved in temporary tables due to invalid data', HL7InboundBase::LOG_LEVEL_MINIMUM);
  			$this->logMessage('Processing is complete. Updated HL7 Message to staus ' . HL7InboundBase::STATUS_FORM_ERROR, HL7InboundBase::LOG_LEVEL_MINIMUM);
  		}
  		else {
  			$claim->save();
  			$set_processed = $this->setProcessed(HL7InboundBase::STATUS_PROCESSED); //after claim is saved --> set processed  to 1

  			$this->logMessage('Claim saved. Claim id: ' . $claim->id, HL7InboundBase::LOG_LEVEL_MINIMUM);
  			$this->logMessage('Processing is complete. Updated HL7 Message to staus ' . HL7InboundBase::STATUS_PROCESSED, HL7InboundBase::LOG_LEVEL_MINIMUM);
  		}
  		$this->indent -= 2;
 	}
 	protected function createNewPatient()
 	{
 		$this->logMessage('createNewPatient() called.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

 		$patient = new Patient();
 		$patient->client_id = $this->client_id;

 		return $patient;
 	}
 	private function getPatientFromRamsoftIdAndHealthCardNumber($ramsoftId, $healthCardNumber)
 	{
 		$this->logMessage('getPatientFromRamsoftIdAndHealthCardNumber(' . $ramsoftId . ', ' . $healthCardNumber . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

 		if ($ramsoftId && $healthCardNumber) {
 			return Doctrine_Query::create()
 				->from('Patient p')
 				->addWhere('p.client_id = (?)', $this->client_id)
 				->addWhere('p.hl7_partner_id LIKE (?)', '%' . $this->hl7_partner . ':' . $ramsoftId . '^%')
 				->addWhere('p.hcn_num = (?)', hype::encryptHCN($healthCardNumber))
 				->addWhere('p.deleted = (?)', false)
 				->fetchOne();
 		}
 	}
 	private function getPatient($ramsoftId, $healthCardNumber)
 	{
 		$this->logMessage('getPatient(' . $ramsoftId . ', ' . $healthCardNumber . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;
 		$patient = null;

 		if ($ramsoftId && $healthCardNumber) {
 			$patient = $this->getPatientFromRamsoftIdAndHealthCardNumber($ramsoftId, $healthCardNumber);
 		}

 		if ($patient instanceof Patient) {
 			$this->logMessage('Patient found with RAMSOFT ID ' . $ramsoftId . ' (' . $patient->fname . ' ' . $patient->lname . ')', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->indent -= 2;
 			return $patient;
 		}

 		// TODO: search by HYPE ID if we are receiving it
 		// TODO: search by Health Card Number only

 		$this->indent -= 2;
 		return $this->createNewPatient();
 	}
 	private function handlePID()
 	{
 		$this->logMessage('handlePID() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$pid = $this->getPID();

 		$ramsoftId = $pid['PID_F2_C1'];
 		$healthCardNumber = $pid['PID_F20_C1'];

 		$patient = $this->getPatient($ramsoftId, $healthCardNumber);
 		$patient = $this->updatePatient($patient, $pid);

 		$this->index -= 2;
 		return $patient;
 	}
 	private function getLocation()
 	{
 		$this->logMessage('getLocation() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$pv1_record = $this->getPV1();
 		$hl7_partner_id = trim(strtoupper($pv1_record['PV1_F3_C4']));

 		return Doctrine_Query::create()
 			->from('Location l')
 			->addWhere('l.client_id = (?)', $this->client_id)
 			->addWhere('l.hl7_partner_id LIKE (?)', '%' . $this->hl7_partner . ':' . $hl7_partner_id . '^%')
 			->fetchOne();
 	}
 	private function updatePatient($patient, $pid)
 	{
 		$this->logMessage('updatePatient() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		// PID 2.1 - RAMSOFT Patient ID
 		if ($pid['PID_F2_C1']) {
 			$this->logMessage('Set Chart number to ' . $pid['PID_F2_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setPatientNumber($pid['PID_F2_C1']);
 		}

 		// PID 5.1 - Last Name
 		if ($pid['PID_F5_C1']) {
 			$this->logMessage('Set patient lname to ' . $pid['PID_F5_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setLname($pid['PID_F5_C1']);
 		}

 		// PID 5.2 - First Name
 		if ($pid['PID_F5_C2']) {
 			$this->logMessage('Set patient fname to ' . $pid['PID_F5_C2'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setFname($pid['PID_F5_C2']);
 		}

 		// PID 7.1 - DOB [YYYMMDD]
 		if ($pid['PID_F7_C1']) {
 			$date = date_parse_from_format('Ymd', $pid['PID_F7_C1']);
 			$date = mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);

 			$this->logMessage('Set patient dob to ' . date('Y-m-d', $date), HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setDob(date('Y-m-d', $date));
 		}

 		// PID 8.1 - Sex [M|F]
 		if ($pid['PID_F8_C1'] == 'M' || $pid['PID_F8_C1'] == 'F') {
 			$this->logMessage('Set patient sex to ' . $pid['PID_F8_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setSex($pid['PID_F8_C1']);
 		}

 		// PID 11.1 - Street Address
 		if ($pid['PID_F11_C1']) {
 			$this->logMessage('Set patient address to ' . $pid['PID_F11_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setAddress($pid['PID_F11_C1']);
 		}

 		// PID 11.3 - City
 		if ($pid['PID_F11_C3']) {
 			$this->logMessage('Set patient city to ' . $pid['PID_F11_C3'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setCity($pid['PID_F11_C3']);
 		}

 		// PID 11.4 - Province
 		if ($pid['PID_F11_C4']) {
 			$patient->setProvince(Doctrine::getTable('Province')->getProvinceCodeForProvinceName($pid['PID_F11_C4']));
 			$this->logMessage('Set patient province to ' . $patient->province, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// PID 11.5 - Postal [Zip] Code
 		if ($pid['PID_F11_C5']) {
 			$this->logMessage('Set patient postal code to ' . $pid['PID_F11_C5'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setPostalCode($pid['PID_F11_C5']);
 		}

 		// PID 11.6 - Country
 		if ($pid['PID_F11_C6']) {
 			$this->logMessage('Set patient country to ' . $pid['PID_F11_C6'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setCountry($pid['PID_F11_C6']);
 		}

 		// PID 13.1 - Home Phone
 		if ($pid['PID_F13_C1']) {
 			$this->logMessage('Set patient home phone to ' . $pid['PID_F13_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setHphone($pid['PID_F13_C1']);
 		}

 		// PID 13.4 - Email Address
 		if ($pid['PID_F13_C4']) {
 			$this->logMessage('Set patient email address to ' . $pid['PID_F13_C4'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setEmailAddress($pid['PID_F13_C4']);
 		}

 		// PID 13.7 - Cellphone
 		if ($pid['PID_F13_C7']) {
 			$this->logMessage('Set patient cellphone to ' . $pid['PID_F13_C7'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setCellphone($pid['PID_F13_C7']);
 		}

 		// PID 14.1 - Work Phone
 		if ($pid['PID_F14_C1']) {
 			$this->logMessage('Set patient work phone to ' . $pid['PID_F14_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setWphone($pid['PID_F14_C1']);
 		}

 		// PID 20.1 - Health Card Number
 		if ($pid['PID_F20_C1']) {
 			$this->logMessage('Set patient Health Card Number to ' . $pid['PID_F20_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setHcnNum(hype::encryptHCN(strtoupper(trim($pid['PID_F20_C1']))));
 		}

 		// PID 20.2 - Version Code
 		if ($pid['PID_F20_C2']) {
 			$this->logMessage('Set patient Health Card Version Code to ' . $pid['PID_F20_C2'], HL7InboundBase::LOG_LEVEL_ALL);
 			$patient->setHcnVersionCode($pid['PID_F20_C2']);
 		}

 		// PID 20.3 - Expiry Date
 		if ($pid['PID_F20_C3']) {
 			$patient->setHcnExpYear($pid['PID_F20_C3']);
 			$this->logMessage('Set patient HCN Expiry Date to ' . $patient->getHcnExpYear(), HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// Patient Status
 		if (!$patient->patient_status_id) {
 			$cp = Doctrine::getTable('ClientParam')->getParamForClient($this->client_id, 'patient_status_default');
 			if ($cp instanceof ClientParam) {
 				$this->logMessage('Set patient status ID to ' . $cp->value, HL7InboundBase::LOG_LEVEL_ALL);
 				$patient->patient_status_id = $cp->value;
 			}
 		}

 		// Patient Type
 		if (!$patient->patient_type_id) {
 			$cp = Doctrine::getTable('ClientParam')->getParamForClient($this->client_id, 'default_patient_type_id');
 			if ($cp instanceof ClientParam) {
 				$this->logMessage('Set patient type ID to ' . $cp->value, HL7InboundBase::LOG_LEVEL_ALL);
 				$patient->patient_type_id = $cp->value;
 			}
 		}

 		// HL7 partner ID
 		$this->logMessage('Set HL7 Partner ID for RAMSOFT to ' . $patient->patient_number, HL7InboundBase::LOG_LEVEL_ALL);
 		$patient->setHl7IDForPartner($patient->patient_number, $this->hl7_partner);

 		// TODO: primary Location
 		// TODO: moh province

 		$this->indent -= 2;
 		return $patient;
 	}
 	public function getDoctor($hl7_partner_id)
 	{
 		$this->logMessage('getDoctor(' . $hl7_partner_id . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

 		return Doctrine_Query::create()
 			->from('Doctor d')
 			->addWhere('d.client_id = (?)', $this->client_id)
 			->addWhere('d.hl7_partner_id LIKE (?)', '%' . $this->hl7_partner . ':' . $hl7_partner_id . '^%')
 			->fetchOne();
 	}
 	public function getRefDoctorID($hl7_partner_id)
 	{
 		$ref_doctor_record = Doctrine_Query::create()
 			->from('ReferringDoctor r')
 			->addWhere('r.client_id = (?)', $this->client_id)
 			->addWhere('r.hl7_partner_id LIKE (?)', '%' . $this->hl7_partner . ':' . $hl7_partner_id . '^%')
 			->fetchOne();
 		return $ref_doctor_record;
 	}
 	public function getThirdPartyRecord($hl7_partner_id)
 	{
 		$third_party_record = Doctrine_Query::create()
 			->from('ThirdParty tp')
 			->addWhere('tp.client_id = (?)', $this->client_id)
 			->addWhere('tp.hl7_partner_id LIKE (?)', '%' . $this->hl7_partner . ':' . $hl7_partner_id . '^%')
 			->fetchOne();
 		return $third_party_record;
 	}
 	private function updateReferringDoctor($refdoc, $pv1, $pv1b)
 	{
 		$this->logMessage('updateReferringDoctor() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		if ($pv1['PV1_F8_C1']) {
 			$this->logMessage('Set refdoc HL7 Partner ID for RAMSOFT to ' . $pv1['PV1_F8_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$refdoc->setHl7IDForPartner($pv1['PV1_F8_C1'], $this->hl7_partner);

 			$this->logMessage('Set refdoc provider number to ' . $pv1['PV1_F8_C1'], HL7InboundBase::LOG_LEVEL_ALL);
 			$refdoc->setProviderNum($pv1['PV1_F8_C1']);
 		}

 		if ($pv1['PV1_F8_C2']) {
 			$this->logMessage('Set refdoc lname to ' . $pv1['PV1_F8_C2'], HL7InboundBase::LOG_LEVEL_ALL);
 			$refdoc->setLname($pv1['PV1_F8_C2']);
 		}

 		if ($pv1['PV1_F8_C3']) {
 			$this->logMessage('Set refdoc fname to ' . $pv1['PV1_F8_C3'], HL7InboundBase::LOG_LEVEL_ALL);
 			$refdoc->setFname($pv1['PV1_F8_C3']);
 		}

 		$this->indent -= 2;
 		return $refdoc;
 	}
 	private function getReferringDoctor($pv1, $pv1b)
 	{
 		$this->logMessage('getReferringDoctor() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

 		if (!$pv1['PV1_F8_C1']) {
 			return;
 		}
 		$this->indent += 2;
 		$this->logMessage('find Referring Doctor record for provider number ' . $pv1['PV1_F8_C1'], HL7InboundBase::LOG_LEVEL_MINIMUM);

 		$refdoc = Doctrine_Query::create()
 			->from('ReferringDoctor d')
 			->addWhere('d.client_id = (?)', $this->client_id)
 			->addWhere('d.hl7_partner_id LIKE (?)', '%' . $this->hl7_partner . ':' . $pv1['PV1_F8_C1'] . '^%')
 			->fetchOne();

 		if (!$refdoc instanceof ReferringDoctor) {
 			$refdoc = $this->createReferringDoctor();
 		}
 		if ($refdoc instanceof ReferringDoctor) {
 			$refdoc = $this->updateReferringDoctor($refdoc, $pv1, $pv1b);
 		}

 		$this->logMessage('Saving refdoc record', HL7InboundBase::LOG_LEVEL_MINIMUM);
 		$refdoc->save();
 		$this->logMessage('Ref Doc ID: ' . $refdoc->id, HL7InboundBase::LOG_LEVEL_MINIMUM);

 		$this->indent -= 2;
 		return $refdoc;
 	}
 	private function createReferringDoctor()
 	{
 		$this->logMessage('createReferringDoctor() called.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

 		$refdoc = new ReferringDoctor();
 		$refdoc->client_id = $this->client_id;
 		$refdoc->active = true;
 		$refdoc->spec_code = $this->default_refdoc_spec_code;

 		$rda = new ReferringDoctorAddress();
 		$rda->active = true;

 		$refdoc->ReferringDoctorAddress->add($rda);

 		return $refdoc;
 	}
 	private function handleFT1($claim, $ft1)
 	{
  		$this->logMessage('handleFT1() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
  		$this->indent += 2;

  		$req_ref_doc = $req_facility_num = $req_admit_date = $interval = $req_diag_code = false;

  		$item = new ClaimItem();
  		$item->client_id = $this->client_id;

  		$service_code = $this->getServiceCode($ft1['FT1_F25_C1']);
 		$diag_code = $this->getDiagnosticCode($ft1['FT1_F19_C1']);

  		if (!$service_code instanceof ServiceCode) {
  			$this->is_valid = false;
  			$this->logMessage('Service code ' .  $service_code . ' is invalid', HL7InboundBase::LOG_LEVEL_MINIMUM);
  		}
  		else {
  			$req_diag_code = $service_code['req_diag_code'];
  			$req_ref_doc = $service_code['req_ref_doctor'];
  			$req_facility_num = $service_code['req_facility_num'];
  			$req_admit_date = $service_code['req_admit_date'];
  			$interval = $service_code['inter_val'];

  			$item->setServiceCode($ft1['FT1_F25_C1']);
  			$this->serviceCodes[count($claim->ClaimItem)] = $service_code;
  			$this->group_numbers[count($claim->ClaimItem)] = $this->determineGroupNumber($item->service_code, $claim->Location->code);

  			$this->logMessage('Service Code set to ' . $item->service_code, HL7InboundBase::LOG_LEVEL_ALL);
 		}

  		$date = trim($ft1['FT1_F4_C1']);
  		$item->service_date = substr($date,0,4) . "-" . substr($date,4,2) . "-" . substr($date,6,2) . " 00:00:00";
  		$item->num_serv = trim($ft1['FT1_F10_C1']) ? trim($ft1['FT1_F10_C1']) : 1;
  		$item->modifier = $item->num_serv * 100;

 		$this->logMessage('Set service date to ' . $item->service_date, HL7InboundBase::LOG_LEVEL_ALL);
 		$this->logMessage('Set num_serv to ' . $item->num_serv, HL7InboundBase::LOG_LEVEL_ALL);

 		if ($diag_code instanceof DiagCode) {
 			$item->diag_code = $diag_code->diag_code;
 			$this->logMessage('Set diag code to ' . $item->diag_code, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$claim->pay_prog = $ft1['FT1_F18_C1'] ? $ft1['FT1_F18_C1'] : $this->default_pay_prog;
  		$item->item_id = count($claim->ClaimItem) + 1;
 		$item->status = ClaimItemTable::STATUS_UNSUBMITTED;
 		$this->logMessage('Set claim pay prog to ' . $claim->pay_prog, HL7InboundBase::LOG_LEVEL_ALL);
 		$this->logMessage('Item number is ' . $item->item_id, HL7InboundBase::LOG_LEVEL_ALL);

 		$claim->ClaimItem->add($item);

 		if ($req_diag_code && !$item->diag_code) {
 			$this->is_valid = false;
 			$this->logMessage('Service Code ' . $item->service_code . ' requires a diagnostic code', HL7InboundBase::LOG_LEVEL_MINIMUM);
 		}
 		if ($req_ref_doc && !$claim->ref_doc) {
 			$this->is_valid = false;
 			$this->logMessage('Service Code ' . $item->service_code . ' requires a referring doctor', HL7InboundBase::LOG_LEVEL_MINIMUM);
 		}
 		if ($req_facility_num && !$claim->facility_num) {
 			$this->is_valid = false;
 			$this->logMessage('Service Code ' . $item->service_code . ' requires a facility number', HL7InboundBase::LOG_LEVEL_MINIMUM);
 		}
 		if ($req_admit_date && !$claim->admit_date) {
 			$this->is_valid = false;
 			$this->logMessage('Service Code ' . $item->service_code . ' requires an admit date', HL7InboundBase::LOG_LEVEL_MINIMUM);
 		}
 		$this->indent -= 2;

 		return $claim;
 	}
 	private function determineGroupNumber($service_code, $location_code)
 	{
 		$this->logMessage('determineGroupNumber(' . $service_code . ', ' .  $location_code . ') called.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$rs = null;
 		$mode = $service_code[strlen($service_code) - 1];

 		// TODO: code for determining group number
 		$rs = '0000';

 		$this->indent += 2;
 		$this->logMessage('Group Number determined to be ' . $rs, HL7InboundBase::LOG_LEVEL_MINIMUM);
 		$this->indent -= 2;

 		return $rs;
 	}
}

