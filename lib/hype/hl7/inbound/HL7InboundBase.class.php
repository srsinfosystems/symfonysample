<?php

abstract class HL7InboundBase
{
	// PDO database's HYPE_HL7Data table's processed column
	const STATUS_UNPROCESSED = 0;
	const STATUS_PROCESSED = 1;
	const STATUS_FATAL_ERROR = 2; // no matching client
	const STATUS_FORM_ERROR = 3; // form fields missing or wrong
	const STATUS_PROCESSING = 4; // initial status to indicate message is being processed
	const STATUS_DUPLICATE = 33; // claim already in database
	const STATUS_NO_MATCHING_PROCESSOR = 5; // someone sent a message type that we don't support for that client

	const LOG_LEVEL_MINIMUM = 1;
	const LOG_LEVEL_FUNCTIONS = 9;
	const LOG_LEVEL_ALL = 15;

	protected $dbh = null;
	protected $connection = null;

	protected $message_id = null;
	protected $client_id = null;
	protected $is_valid = true;

	protected $debug = true;
	// level 1: show major workflow steps as well as database saves
	// level 9: show function calls
	// level 15: show variable sets in all objects
	protected $debug_level = 15;
	protected $indent = 0;

	protected $msh = null;
	protected $hl7_partner;
	protected $raw_message = null;
	protected $hl7Inbound = null;

	abstract function processDFT();

	public function __construct($message_id, $msh, $client_id, $partner, $dbh, $doctrine_connection)
	{
		$this->message_id = $message_id;
		$this->client_id = $client_id;
		$this->hl7_partner = $partner;
		$this->dbh = $dbh;
		$this->msh = $msh;
		$this->connection = $doctrine_connection;

		return $this;
	}
	public function process()
	{
		try {
			if ($this->msh['MSH_F9_C1'] == 'DFT') {
				$this->processDFT();
			}
			else if ($this->msh['MSH_F9_C1'] == 'ADT') {
				$this->processADT();
			}
			else if ($this->msh['MSH_F9_C1'] == 'SIU') {
				$this->processSIU();
			}
		}
		catch (Exception $e) {
			echo $e->getMessage() . "\n";
			$this->logMessage($e->getTraceAsString(), HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->connection->rollback();
// 			throw $e;
		}
	}
	public function setHl7Inbound($hl7Inbound)
	{
		$this->hl7Inbound = $hl7Inbound;
		return $this;
	}
	public function getHl7Inbound()
	{
		return $this->hl7Inbound;
	}
	public function setDebug($debug)
	{
		$this->debug = $debug;
		return $this;
	}
	public function setDebugLevel($debug_level)
	{
		$this->debug_level = $debug_level;
	}

	private function getSingleSegment($type, $tableNum = 'A')
	{
		$sql = 'SELECT * from HYPE_Segment_' . $type . '_' . $tableNum . ' where MessageID = ? limit 1';
		$sth = $this->dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$sth->execute(array($this->message_id));
		return $sth->fetch(PDO::FETCH_ASSOC);
	}
	public function getIN1()
	{
		return $this->getSingleSegment('IN1');
	}
	public function getACC()
	{
		return $this->getSingleSegment('ACC');
	}
	public function getAIL()
	{
		return $this->getSingleSegment('AIL');
	}
	public function getAIP()
	{
		return $this->getSingleSegment('AIP');
	}
	public function getUB2()
	{
		return $this->getSingleSegment('UB2');
	}
	public function getPID()
	{
		return $this->getSingleSegment('PID');
	}
	public function getPV1()
	{
		return $this->getSingleSegment('PV1');
	}
	public function getPV1B()
	{
		return $this->getSingleSegment('PV1', 'B');
	}
	public function getSCH()
	{
		return $this->getSingleSegment('SCH');
	}

	private function getMultipleSegments($type, $tableNum = 'A')
	{
		$sql = 'SELECT * from HYPE_Segment_' . $type . '_' . $tableNum . ' where MessageID = ?';
		$sth = $this->dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$sth->execute(array($this->message_id));
		return $sth->fetchAll(PDO::FETCH_ASSOC);
	}
	public function getFT1()
	{
		return $this->getMultipleSegments('FT1');
	}
	public function getEVN()
	{
		return $this->getMultipleSegments('EVN');
	}

	public function setProcessed($status)
	{
		$sql = 'update HYPE_HL7Data set processed = ? where MessageID = ?';
		$sth = $this->dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$rows_affected = $sth->execute(array($status, $this->message_id));
		return($rows_affected);
	}
	protected function processDateYmd($date)
	{
		$year = substr($date, 0, 4);
		$month = substr($date, 4, 2);
		$day = substr($date, 6, 2);

		return $year . '-' . $month . '-' . $day . ' 00:00:00';
	}
	public function setRawMessage($message)
	{
		$this->raw_message = $message;
		return $this;
	}
	protected function indent()
	{
		$a = 0;
		$rs = '';
		while ($a < $this->indent)
		{
			$rs .= ' ';
			$a++;
		}
		return $rs;
	}
	protected function logMessage($msg, $min_level)
	{
		if ($this->debug && $this->debug_level >= $min_level) {
			echo $this->indent() . $msg . "\n";

		}
		$this->hl7Inbound->addLog($this->indent() . $msg);
	}
	protected function getArrayValues($arr)
	{
		foreach ($arr as $key => $values)
		{
			if ($values) {
				echo '  ' . $key . ' => ' . $values . "\n";
			}
		}
	}
	protected function getDiagnosticCode($code)
	{
		$this->logMessage('getDiagnosticCode(' . $code . ') called.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		if ($code) {
			return Doctrine_Query::create()
			->from('DiagCode dc')
			->addWhere('dc.diag_code = (?)', $code)
			->fetchOne();
		}

		return;
	}
	protected function getServiceCode($service_code)
	{
		$this->logMessage('getServiceCode(' . $service_code . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		if ($service_code) {
			return Doctrine_Query::create()
				->from('ServiceCode sc')
				->addWhere('sc.service_code = (?)', substr($service_code, 0, 4))
				->fetchOne();
		}
	}
	protected function getSpecCode($spec_code)
	{
		$this->logMessage('getSpecCode(' . $spec_code . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		return Doctrine_Query::create()
			->from('SpecCode sc')
			->addWhere('sc.moh_spec_code = (?)', $spec_code)
			->fetchOne();
	}

	protected function createNewDoctor()
	{
		$this->logMessage('createNewDoctor() called.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		$doctor = new Doctor();
		$doctor->setClientId($this->client_id);
		return $doctor;
	}
	protected function createNewPatient()
	{
		$this->logMessage('createNewPatient() called.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		$patient = new Patient();
		$patient->client_id = $this->client_id;
		return $patient;
	}
	protected function createNewReferringDoctor()
	{
		$this->logMessage('createNewReferringDoctor() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		$referringDoctor = new ReferringDoctor();
		$referringDoctor->setClientId($this->client_id);
		return $referringDoctor;
	}

	protected function duplicateServicesCheck($item)
	{
		$this->logMessage('duplicateServicesCheck called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
		$this->indent += 2;

		$query = Doctrine_Query::create()
			->from('ClaimItem ci')
			->leftJoin('ci.Claim c')
			->addWhere('c.client_id = (?)', $this->client_id)
			->addWhere('c.deleted = (?)', false)
			->addWhere('ci.status != (?)', ClaimItemTable::STATUS_DELETED)
			->addWhere('ci.service_date = (?)', $item->service_date)
			->addWhere('ci.service_code = (?) OR c.pay_prog = (?)', array($item->service_code, ClaimTable::CLAIM_TYPE_DIRECT));

		if ($item->Claim->patient_id) {
			$query->addWhere('c.patient_id = (?)', $item->Claim->patient_id);
		}
		else {
			$query->addWhere('c.health_num = (?)', hype::encryptHCN($item->Claim->health_num));
		}
		$duplicates = $query->execute();

		if (count($duplicates)) {
			$this->logMessage(count($duplicates) . ' possible duplicates found on same day.', HL7InboundBase::LOG_LEVEL_ALL);

			foreach ($duplicates as $dup)
			{
				if ($dup->getServiceCode() == $item->getServiceCode()) {
					$this->hl7Inbound->setDuplicateClaimId($dup->claim_id);
					$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_DUPLICATE_CLAIM, 'Claim Item ' . ($item->item_id . ': ' . $dup->getServiceCode() . ' is a duplicate.'));
					$this->logMessage('Claim Item ' . ($item->item_id) . ': ' . $dup->getServiceCode() . ' is a duplciate. Removing claim item.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
					$this->indent -= 2;
					return false;
				}
				else if ($dup->Claim->getPayProg() == ClaimTable::CLAIM_TYPE_DIRECT) {
					$dsc = Doctrine_Query::create()
					->from('DirectServiceCode dsc')
					->addWhere('dsc.client_id = (?)', $this->client_id)
					->addWhere('dsc.service_code = (?)', $dup->service_code)
					->addWhere('dsc.ohip_codes LIKE (?)', '%' . $item->getServiceCode() . '%')
					->fetchOne();

					if ($dsc instanceof DirectServiceCode) {
						$this->hl7Inbound->setDuplicateClaimId($dup->claim_id);
						$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_DUPLICATE_CLAIM, 'Claim Item ' . ($item->item_id) . ': ' . $dup->getServiceCode() . ' is a duplicate.');
						$this->logMessage('Claim Item ' . ($item->item_id) . ': ' . $dup->getServiceCode() . ' is a duplciate of Direct Service Code' . $dsc->service_code .'. Removing claim item.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
						$this->indent -= 2;
						return false;
					}
				}
				else {
					$this->logMessage('Claim Item ' . ($item->item_id) . ': ' . $dup->getServiceCode() . '; no duplicates found.', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
				}
			}
		}

		$this->indent -= 2;
		return true;
	}

	protected function getAppointmentTypeByName($name)
	{
		$this->logMessage('getAppointmentTypeByName(' . $name . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		if ($name) {
			return Doctrine_Query::create()
			->from('AppointmentType at')
			->addWhere('at.client_id = (?)', $this->client_id)
			->addWhere('at.name = (?)', strtoupper(trim($name)))
			->fetchOne();
		}
	}
	protected function getAttendeeByHypeID($hypeID)
	{
		$this->logMessage('getAttendeeByHypeID(' . $hypeID . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		if ($hypeID) {
			return Doctrine_Query::create()
			->from('AppointmentAttendee att')
			->leftJoin('att.Appointment a')
			->addWhere('a.client_id = (?)', $this->client_id)
			->addWhere('att.id = (?)', $hypeID)
			->fetchOne();
		}
	}
	protected function getDoctorById($id)
	{
		$this->logMessage('getDoctorById(' . $id . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		return Doctrine_Query::create()
		->from('Doctor d')
		->addWhere('d.client_id = (?)', $this->client_id)
		->addWhere('d.id = (?)', $id)
		->fetchOne();
	}
	protected function getDoctorByName($lname, $fname)
	{
		$this->logMessage('getDoctorByName(' . $lname . ', ' . $fname . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		return Doctrine_Query::create()
			->from('Doctor d')
			->addWhere('d.client_id = (?)', $this->client_id)
			->addWhere('d.lname = (?)', $lname)
			->addWhere('d.fname = (?)', $fname)
			->fetchOne();
	}
	protected function getDoctorFromProviderAndGroupNumber($providerNum, $groupNum)
	{
		$this->logMessage('getDoctorFromProviderAndGroupNumber(' . $providerNum . ', ' . $groupNum . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		return Doctrine_Query::create()
			->from('Doctor d')
			->addWhere('d.client_id = (?)', $this->client_id)
			->addWhere('d.provider_num = (?)', $providerNum)
			->addWhere('d.group_num = (?)', $groupNum)
			->fetchOne();
	}
	protected function getDoctorLocationFromDoctorIdAndLocationId($doctorId, $locationId)
	{
		$this->logMessage('getDoctorLocationFromDoctorIdAndLocationId(' . $doctorId . ', ' . $locationId . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		return Doctrine_Query::create()
			->from('DoctorLocation dl')
			->addWhere('dl.doctor_id = (?)', $doctorId)
			->addWhere('dl.location_id = (?)', $locationId)
			->fetchOne();
	}
	protected function getLocationByName($locationName)
	{
		$this->logMessage('getLocationByName(' . $locationName . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		return Doctrine_Query::create()
			->from('Location l')
			->addWhere('l.name = (?)', $locationName)
			->addWhere('l.client_id = (?)', $this->client_id)
			->fetchOne();
	}
	protected function getMOHOfficeFromMOHOfficeCode($mohOfficeCode)
	{
		$this->logMessage('getMOHOfficeFromMOHOfficeCode(' . $mohOfficeCode . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		return Doctrine_Query::create()
			->from('MOHOffice mo')
			->addWhere('mo.moh_office_code = (?)', $mohOfficeCode)
			->fetchOne();
	}
	protected function getPatientFromHl7PartnerId($partnerId)
	{
		$this->logMessage('getPatientFromHl7PartnerId(' . $partnerId . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		if ($partnerId) {
			return Doctrine_Query::create()
				->from('Patient p')
				->addWhere('p.client_id = (?)', $this->client_id)
				->addWhere('p.hl7_partner_id LIKE (?)', '%' . $this->hl7_partner . ':' . $partnerId . '^%')
				->addWhere('p.deleted = (?)', false)
				->fetchOne();
		}
	}
	protected function getPatientFromHypeId($hypeID)
	{
		$this->logMessage('getPatientFromHypeId(' . $hypeID . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		if ($hypeID) {
			return Doctrine_Query::create()
				->from('Patient p')
				->addWhere('p.client_id = (?)', $this->client_id)
				->addWhere('p.id = (?)', $hypeID)
				->addWhere('p.deleted = (?)', false)
				->fetchOne();
		}
	}
	protected function getPatientFromNameAndDob($fname, $lname, $dob)
	{
		$this->logMessage('getPatientFromNameAndDob(' . $fname . ', ' . $lname . ', ' . $dob . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		if ($fname && $lname && $dob) {
			return Doctrine_Query::create()
				->from('Patient p')
				->addWhere('p.client_id = (?)', $this->client_id)
				->addWhere('p.fname = (?)', $fname)
				->addWhere('p.lname = (?)', $lname)
				->addWhere('p.dob = (?)', $dob)
				->fetchOne();
		}
	}
	protected function getPatientFromNameAndHealthCardNumber($fname, $lname, $hcn, $version_code) {
		$this->logMessage('getPatientFromNameAndHealthCardNumber(' . $fname . ', ' . $lname . ', ' . $hcn . ', ' . $version_code . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		return Doctrine_Query::create()
			->from('Patient p')
			->addWhere('p.client_id = (?)', $this->client_id)
			->addWhere('p.fname = (?)', $fname)
			->addWhere('p.lname = (?)', $lname)
			->addWhere('p.hcn_num = (?)', hype::encryptHCN($hcn))
			->addWhere('p.hcn_version_code = (?)', $version_code)
			->fetchOne();
	}
	protected function getReferringDoctorFromHl7PartnerId($partnerId)
	{
		$this->logMessage('getReferringDoctorFromHl7PartnerId(' . $partnerId . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		if ($partnerId) {
			return Doctrine_Query::create()
			->from('ReferringDoctor r')
			//->leftJoin('r.ReferringDoctorAddress rda')
			->addWhere('r.client_id = (?)', $this->client_id)
			->addWhere('r.hl7_partner_id LIKE (?)', '%' . $this->hl7_partner . ':' . $partnerId . '^%')
			->fetchOne();
		}
	}
	protected function getThirdPartyFromHl7PartnerId($partnerId)
	{
		$this->logMessage('getThirdPartyFromHl7PartnerId(' . $partnerId . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		if ($partnerId) {
			return Doctrine_Query::create()
				->from('ThirdParty tp')
				->addWhere('tp.client_id = (?)', $this->client_id)
				->addWhere('tp.hl7_partner_id LIKE (?)', '%' . $this->hl7_partner . ':' . $partnerId . '^%')
				->fetchOne();
		}
	}
}
