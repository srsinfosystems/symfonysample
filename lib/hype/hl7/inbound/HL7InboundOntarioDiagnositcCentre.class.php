<?php

class HL7InboundOntarioDiagnosticCentre extends HL7InboundMergeris
{
 	protected $indent = 0;
 	protected $invalidServiceCodes = array(
 		'G650A', 'G651A', 'G652A', 'G693A',
 		'J138A', 'J162A', 'J168A', 'J310A', 'J435A', 'J850A',
 		'X090A', 'X091A', 'X106A', 'X153A',
 	);

	//Claim Splitting Rules --------------------------------------------------------------------------------------------------------------
	//service code, mode, group number
	//general case: mode = C, group number = 0000 (including J306C, J311C, J307C, J310C, J333C, J340C)
	//except for J327C and J304C -> billed under 7438 and sli = OFF
	//All technical B codes are billed under IHF number (@@##) and have and SLI (IHF)
	//except for J327B and J304B which are billed under 7438 (non-IHF)
	//All G-codes with A suffix are billed under a non-IHF number (####) and have an SLI of (OFF)
	//J306B, J311B, J307B, J310B, J333B, J340B have to billed under IHF number AL25
	//Hospital billing requires a facility number of 4759 and an admission date, billed under solo group numbers

	// 2013-08-26: Z771A has to be billed in an independent claim
	// 2013-09-26: Claims ending with A and Prefix of A, C, E, J, Z should have a group number of 0000
	// 2013-09-26: Claims ending with A and Prefix of G should have a group number
	// 2013-09-26: G370A and G700A should have a group number of 0000

 	protected function handleClientFacilityNumberModification($v)
 	{
 		$this->logMessage('handleClientFacilityNumberModification(' . $v . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		if ($v) {
 			$v = '4759';
 			$this->logMessage('Facility Number updated to ' . $v, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$this->indent -= 2;
 		return $v;

 	}
 	protected function handleClientServiceCodeModification($service_code)
 	{
 		$this->logMessage('handleClientServiceCodeModification(' . $service_code . ') called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		if (strlen($service_code) == 4 && ($service_code[0] == 'G' || $service_code == 'Z771')) {
 			$service_code = substr($service_code, 0, 4) . 'A';
 			$this->logMessage('Service Code updated to ' . $service_code, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		$this->indent -= 2;
 		return $service_code;
 	}
 	protected function handleClientPostMappingChecks($claim)
 	{
 		$this->logMessage('handleClientPostMappingChecks() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		if (!count($claim->ClaimItem)) {
 			$this->indent -= 2;
 			return $claim;
 		}

 		$canMerge = true;
 		foreach ($claim->ClaimItem as $claimItem)
 		{
 			if ($claimItem->getServiceCode() == 'Z771A') {
 				$canMerge = false;
 			}
 		}

 		if (!$canMerge) {
 			$this->indent -= 2;
 			return $claim;
 		}

 		$q = Doctrine_Query::create()
 			->from('Claim c')
 			->leftJoin('c.ClaimItem ci')
 			->addWhere('c.health_num = (?)', hype::encryptHCN($claim->health_num))
 			->addWhere('c.patient_id = (?)', $claim->getPatientId())
 			->addWhere('c.doctor_id = (?)', $claim->getDoctorId())
 			->addWhere('c.pay_prog = (?)', $claim->getPayProg())
 			->addWhere('c.batch_id is null')
 			->addWhere('ci.service_date = (?)', $claim->ClaimItem[0]->getServiceDate())
 			->addWhere('c.deleted = (?)', false);

 		// 2013-12-17 - added SLI check in order to try to minimize V68 rejections
 		if ($claim->getSli()) {
 			$q->addWhere('c.sli = (?)', $claim->getSli());
 		}
 		else {
 			$q->addWhere('c.sli = (?) OR c.sli IS NULL', '');
 		}

 		$oldClaims = $q->execute();

 		foreach ($oldClaims as $oldClaim)
 		{
 			$canMerge = true;
 			foreach ($oldClaim->ClaimItem as $oldClaimItem)
 			{
 				if ($oldClaimItem->getServiceCode() == 'Z771A') {
 					$canMerge = false;
 				}
 			}

 			if ($canMerge) {
 				foreach ($claim->ClaimItem as $claimItem)
 				{
 					$claimItem->setItemId(count($oldClaim->ClaimItem) + 1);
 					$oldClaim->ClaimItem->add($claimItem);

 					$this->logMessage('Claim Item Added to Claim ID ' . $oldClaim->getId(), HL7InboundBase::LOG_LEVEL_MINIMUM);
 				}

 				$this->indent -= 2;
 				return $oldClaim;
 			}
 		}
 		$this->indent -= 2;
 		return $claim;

 	}
 	protected function determineDoctor($claim, $pv1, $pv1b, $ft1s)
 	{
 		$this->logMessage('determineDoctor() called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);
 		$this->indent += 2;

 		$doctor = null;
 		$sli = null;

 		if (!count($claim->ClaimItem)) {
 			$this->is_valid = false;
 			$this->logMessage('No FT1 records in this claim', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_SERVICES, 'No FT1 records in this claim');

 			$this->indent -= 2;
 			return $doctor;
 		}

 		if (!$this->ServiceCode instanceof ServiceCode) {
 			$this->indent -= 2;
 			return $doctor;
 		}

 		if (!$this->mode) {
 			$this->indent -= 2;
 			return $doctor;
 		}

 		$ohipCode = $this->ServiceCode->getServiceCode();
 		$mode = $this->mode;
 		$prefix = $ohipCode[0];

 		// PV1 7.1 - Attending Doctor Provider Number
 		$providerNumber = trim(strtoupper($pv1['PV1_F7_C1']));

 		// PV1 6.1 - Attending Doctor Group Number
 		$groupNum = trim(strtoupper($pv1['PV1_F6_C1']));

 		// PV1 3.1 - Attending Doctor IHF Number - Same as Location Code
 		$ihfNum = trim(strtoupper($pv1['PV1_F3_C1']));

 		if ($providerNumber) {
 			while (strlen($providerNumber) < 6)
 			{
 				$providerNumber = '0' . $providerNumber;
 			}
 		}
 		else {
 			$this->is_valid = false;
 			$this->logMessage('Provider Number is required', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_DOCTOR, 'Provider Number is required');

 			$this->indent -= 2;
 			return $doctor;
 		}

 		$this->logMessage('Provider Number is ' . $providerNumber, HL7InboundBase::LOG_LEVEL_ALL);
 		$this->logMessage('Group Number is ' . $groupNum, HL7InboundBase::LOG_LEVEL_ALL);
 		$this->logMessage('IHF Number is ' . $ihfNum, HL7InboundBase::LOG_LEVEL_ALL);

 		if ($groupNum) {
 			$doctorGroupNum = $groupNum;
 		}
 		else {
 			$doctorGroupNum = $ihfNum;
 		}
 		$this->logMessage('Doctor Group Number will be ' . $doctorGroupNum, HL7InboundBase::LOG_LEVEL_ALL);

 		// 2013-09-26: list of service codes that should not be charged
 		if (in_array(($ohipCode . $mode), $this->invalidServiceCodes)) {
 			$this->is_valid = false;
 			$this->logMessage('Service Code ' . $ohipCode . $mode . ' is in Invalid Service Codes list', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_INVALID_SERVICE_CODE, 'Service Code ' . $ohipCode . $mode . ' is in Invalid Service Codes list');

 			$this->indent -= 2;
 			return $doctor;
 		}

 		// 2013-09-26: G370A and G700A should have a group number of 0000
 		else if (($ohipCode . $mode) == 'G370A' || ($ohipCode . $mode) == 'G700A') {
 			$this->logMessage('G370A and G700A should have a group number of 0000');

 			$doctorGroupNum = '0000';
 		}

 		else if (!$this->ServiceCode->getReqProfGroupNum() && $mode == 'C') {
 			$this->logMessage('Service Code is C [Professional] and Service Code does not require Professional Service Code', HL7InboundBase::LOG_LEVEL_ALL);

 			$doctorGroupNum = '0000';
 			$sli = null;

 			$this->logMessage('Doctor Group Number will be ' . $doctorGroupNum, HL7InboundBase::LOG_LEVEL_ALL);
 			$this->logMessage('SLI will be null' . $sli, HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// Applicable only to J327C/B, J304C/B and G-codes with 'A' suffix
 		else if ($this->ServiceCode->getReqNumericGroupNum()) {
 			$this->logMessage('Service Code Requires a Numeric Group Number', HL7InboundBase::LOG_LEVEL_ALL);

 			if (!is_numeric($doctorGroupNum)) {
 				$this->is_valid = false;
 				$this->logMessage('Group Number is not numeric', HL7InboundBase::LOG_LEVEL_MINIMUM);
 				$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_GROUP_NUMBER, 'Group Number is not numeric');

 				$this->indent -= 2;
 				return $doctor;
 			}
 			else {
 				// 2013-12-17 - PDF sli used to be OFF.
 				$sli = 'PDF';
 				$this->logMessage('SLI will be PDF', HL7InboundBase::LOG_LEVEL_ALL);
 			}
 		}

 		// All the technical B codes are billed under IHF number (@@##) except J327B and J304B
 		else if ($mode == 'B' && !$this->ServiceCode->getReqNumericGroupNum()) {
 			if (is_numeric($doctorGroupNum)) {
 				$this->is_valid = false;
 				$this->logMessage('An alpha-numeric group number is required to bill service code '. $this->ServiceCode->getServiceCode(), HL7InboundBase::LOG_LEVEL_ALL);
 				$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_GROUP_NUMBER, 'An alpha-numeric group number is required to bill service code '. $this->ServiceCode->getServiceCode());

 				$this->indent -= 2;
 				return $doctor;
 			}
 			if ($doctorGroupNum != 'AL25' && in_array($this->ServiceCode->getServiceCode(), array('J306', 'J311', 'J307', 'J310', 'J333', 'J340'))) {
 				$this->is_valid = false;
 				$this->logMessage('Service code '. $this->ServiceCode->getServiceCode() . ' has to be billed under IHF number AL25.', HL7InboundBase::LOG_LEVEL_ALL);
 				$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_GROUP_NUMBER, 'Service code '. $this->ServiceCode->getServiceCode() . ' has to be billed under IHF number AL25.');

 				$this->indent -= 2;
 				return $doctor;
 			}

 			$sli = 'IHF';
 			$this->logMessage('SLI will be IHF', HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// Hospital Billing
 		else if ($claim->getFacilityNum()) {
 			$doctorGroupNum = '0000';

 			if (!$claim->getAdmitDate()) {
 				$this->is_valid = false;
 				$this->logMessage('Admit Date is required', HL7InboundBase::LOG_LEVEL_ALL);
 				$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_MISSING_SERVICE_REQUIREMENTS, 'Admit Date is required');

 				$this->indent -= 2;
 				return $doctor;
 			}
 		}

 		// 2013-09-26: Claims ending with A and Prefix of A, C, E, J, Z should have a group number of 0000
 		else if ($mode == 'A' && in_array($prefix, array('A', 'C', 'E', 'J', 'Z'))) {
 			$doctorGroupNum = '0000';

 			$this->logMessage('Claims ending with A and beginning with ' . $prefix . ' should have a group number of 0000', HL7InboundBase::LOG_LEVEL_ALL);
 		}

 		// 2013-09-26: Claims ending with A and Prefix of G should have a group number
 		else if ($mode == 'A' && $prefix == 'G') {
 			//$doctorGroupNum = $doctorGroupNum; // doing this is stupid but it keeps the rules order
 		}

 		// Is there any way to block Dr. Osten, Dr. Lam, Dr. Horlic from having reports under their professional 0000. (2012-09-11)
 		// From Cristina Laszcz
 		if ($doctorGroupNum == '0000' && ($providerNumber == '163352' || $providerNumber == '017922' || $providerNumber == '016262')) {
 			$this->is_valid = false;
 			$this->logMessage('Creating Claims for group number 0000 and provider number ' . $providerNumber . ' is not allowed.', HL7InboundBase::LOG_LEVEL_MINIMUM);
 			$this->hl7Inbound->addErrorMessage(HL7InboundTable::ERROR_NO_DOCTOR, 'Creating Claims for group number 0000 and provider number ' . $providerNumber . ' is not allowed.');

 			$this->indent -= 2;
 			return $doctor;
 		}

 		$this->logMessage('Provider Number: ' . $providerNumber . ' and Group Number: ' . $doctorGroupNum, HL7InboundBase::LOG_LEVEL_ALL);

 		$doctor = $this->getDoctorFromProviderAndGroupNumber($providerNumber, $doctorGroupNum);

 		if (!$doctor instanceof Doctor) {
 			$doctor = $this->createNewDoctor();
 			$doctor = $this->updateDoctor($doctor, $providerNumber, $doctorGroupNum, $sli, $pv1);
 		}

 		if ($doctor instanceof Doctor && $doctor->getGroupNum() == '0000' && $claim->getLocationId()) {
 			$doctorLocation = $this->getDoctorLocationFromDoctorIdAndLocationId($doctor->getId(), $claim->getLocationId());

 			if (!$doctorLocation instanceof DoctorLocation) {
 				$doctorLocation = new DoctorLocation();
 				$doctorLocation->setDoctorId($doctor->getId());
 				$doctorLocation->setLocationId($claim->getLocationId());
 				$doctorLocation->save();
 			}

 		}
 		$this->sli = $sli;

 		$this->indent -= 2;
 		return $doctor;
 	}
}
