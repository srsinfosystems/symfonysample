<?php

class HL7InboundGiavedonniEyeMdEMR extends HL7InboundEyemdemr
{
 	protected function handleClientCustomizations($claim)
 	{
 		$this->logMessage('handleClientCustomizations called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

 		$this->indent += 2;

 		$this->indent -= 2;
 		return $claim;
 	}
}