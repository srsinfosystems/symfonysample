<?php

class HL7InboundBrazelEyeMdEMR extends HL7InboundEyemdemr
{
	protected function handleClientCustomizations($claim)
	{
		$this->logMessage('handleClientCustomizations called', HL7InboundBase::LOG_LEVEL_FUNCTIONS);

		$this->indent += 2;

		$last_diag_code = null;
		foreach ($claim->ClaimItem as $key => $item)
		{
			if (!$last_diag_code && $item->diag_code) {
				$last_diag_code = $item->diag_code;
			}

			if (!$item->diag_code) {
				$this->logMessage('Set Claim Item ' . $item->item_id . ' diag code to ' . $last_diag_code, HL7InboundBase::LOG_LEVEL_ALL);
				$claim->ClaimItem[$key]->diag_code = $last_diag_code;
			}

			$sc = substr($item->service_code, 0, 4);

			// E140, E132, E205: add facility number 1443
			if ($sc == 'E140' || $sc == 'E132' || $sc == 'E205') {
				$this->logMessage('Service Code is ' . $sc . ' so set Claim facility number to 1443', HL7InboundBase::LOG_LEVEL_ALL);
				$claim->facility_num = '1443';
			}

			// G858: add SLI of OFF
			if ($sc == 'G858') {
				$claim->sli = 'OFF';
				$this->logMessage('Service Code is ' . $sc . ' so set Claim sli to 1443', HL7InboundBase::LOG_LEVEL_ALL);
			}
		}

		$this->indent -= 2;
		return $claim;
	}
}