<?php

class hypeLabel
{
	public static function getChartLabel($patient, $client_id) {

		//s = the entire string
		//t = the current segment (line)

		$q = Doctrine_Query::create()
			->from('Patient p')
			->addWhere('p.id = ?', $patient)
			->addWhere('p.client_id = ?', $client_id)
			->addWhere('p.deleted = (?)', false)
			->fetchone();

		$t = 'PID: ';

		$t = $t . $q->patient_number;

		$s = hypePrint::formatTable( hypePrint::formatRow( hypePrint::formatD( $q->lname . ', ' . $q->fname) . hypePrint::formatD($t)),100,0  );

		$s = $s . hypePrint::formatTable( hypePrint::formatRow ( hypePrint::formatD ( $q->address)) ,100,0);

		$s = $s . hypePrint::formatTable( hypePrint::formatRow ( hypePrint::formatD ( $q->city . ' ' . $q->province . ' ' . $q->postal_code . ' ' . $q->country )), 100,0);
		$s = $s . hypePrint::formatTable(	hypePrint::formatRow(    hypePrint::formatD('HN: ' . $q->getHCN()) . hypePrint::formatD('DOB: ' . date("d-m-Y", hype::parseDateToInt($q->dob, hype::DB_ISO_DATE)) . ' Sex: ' . $q->sex)),100,0);
		$s = $s . hypePrint::formatTable(	hypePrint::formatRow(    hypePrint::formatD('H: ' . $q->getDisplayHphone()) . hypePrint::formatD('R: ' . $q->ref_doc_num )),100,0);
		$s = $s . hypePrint::formatTable(hypePrint::formatRow(    hypePrint::formatD('W: ' . $q->getDisplayWPhone()     ,400,1) . hypePrint::formatD('F: ' . $q->fam_doc_num )),100,0);

		return $s;
	}
	public static function getNameLabel($patient, $client_id) {

		//s = the entire string
		//t = the current segment (line)

		$q = Doctrine_Query::create()
			->from('Patient p')
			->addWhere('p.id = ?', $patient)
			->addWhere('p.client_id = ?' , $client_id)
			->addWhere('p.deleted = (?)', false)
			->fetchone();

		$s = hypePrint::formatTable( hypePrint::formatRow( hypePrint::formatD( $q->lname . ', ' . $q->fname))  ,100,0  );
		$s = $s . hypePrint::formatTable( hypePrint::formatRow( hypePrint::formatD( 'ID: ' . $q->patient_number)),100,0);

		return $s;

	}
	public static function getAddressLabel($patient, $client_id) {

		//s = the entire string
		//t = the current segment (line)

		$q = Doctrine_Query::create()
			->from('Patient p')
			->addWhere('p.id = ?', $patient)
			->addWhere('p.client_id = ?', $client_id)
			->addWhere('p.deleted = (?)', false)
			->fetchone();

		$s = hypePrint::formatTable( hypePrint::formatRow( hypePrint::formatD( $q->lname . ', ' . $q->fname))  ,100,0  );
		$s = $s . hypePrint::formatTable( hypePrint::formatRow( hypePrint::formatD( $q->address)),100,0);
		if (strtoupper($q->country) == 'CANADA') {
			$s = $s . hypePrint::formatTable( hypePrint::formatRow( hypePrint::formatD( $q->city . ', ' . $q->province )),100,0);
			$s = $s . hypePrint::formatTable( hypePrint::formatRow( hypePrint::formatD( $q->postal_code )), 100,0);
		} else {
			$s = $s . hypePrint::formatTable( hypePrint::formatRow( hypePrint::formatD( $q->city . ', ' . $q->province . ' ' . $q->postal_code )),100,0);
			$s = $s . hypePrint::formatTable( hypePrint::formatRow( hypePrint::formatD( $q->country )), 100,0);
		}

		return $s;

	}
	public static function getRefDocLabel($ref_doc, $client_id) {

		//s = the entire string
		//t = the current segment (line)

		// TODO: How to tell the difference between 2 different ref doc address.

		try {

			$q = Doctrine_Query::create()
				->leftjoin('ra.ReferringDoctorOld r')
				->addWhere('r.provider_num = ?', $ref_doc)
				->addWhere('r.client_id = ?', $client_id)
				->addWhere('r.active = ?', 'true')
				->addWhere('ra.active = ?', 'true')
				->fetchone();

			$s = hypePrint::formatTable( hypePrint::formatRow( hypePrint::formatD( $q->ReferringDoctorOld->lname . ', ' . $q->ReferringDoctorOld->fname))  ,100,0  );
			if ($q->address != '') {$s = $s . hypePrint::formatTable( hypePrint::formatRow( hypePrint::formatD( $q->address )),100,0);}
			if ($q->address2 != '') {$s = $s . hypePrint::formatTable( hypePrint::formatRow( hypePrint::formatD( $q->address2)),100,0);}
			$s = $s . hypePrint::formatTable( hypePrint::formatRow( hypePrint::formatD( $q->city . ', ' . $q->province)),100,0);
			$s = $s . hypePrint::formatTable( hypePrint::formatRow( hypePrint::formatD( $q->postal_code)),100,0);

			return $s;
		}catch(exception $e) {
			// TODO: something useful
		}

	}
	public static function getBagLabel($patient) {
		// TODO: Maybe?
		return $s;
	}
}
