<?php

class ClientParamTable extends Doctrine_Table {

    public static function getParamOrDefaultValueForClient($clientID, $parameterName) {
        $value = ClientParamTable::getDefaultForValue($parameterName);
        if ($clientID) {
            $cp = Doctrine_Query::create()
                    ->from('ClientParam cp')
                    ->addWhere('cp.client_id = ?', $clientID)
                    ->addWhere('cp.param = ?', $parameterName)
                    ->fetchOne(array());
            if ($cp instanceof ClientParam) {
                $value = $cp['value'];
            }
        }
        return $value;
    }

    public static function getDefaultForValue($parameterName) {
        
        switch ($parameterName) {
            case 'allow_simultaneous_doctor_schedules': return true;
            case 'appointment_book_enable_counts': return false;
            case 'appointment_book_enable_daily_notes': return true;
            case 'appointment_book_show_misc': return false;
            case 'appointment_book_timeline_minutes': return false;
            case 'appointment_book_timeslots_per_hour': return 4;
            case 'appointment_book_views': return array(1, 3, 5, 7);
            case 'appointment_close_only_past': return false;
            case 'appointment_create_past_repeating': return true;
            case 'appointment_detail_columns': return array('patient_id', 'full_name', 'hcn', 'start_time', 'appt_type');
            case 'appointment_drag_to_reschedule': return false;
            case 'appointment_dropdown_interval': return 1;
            case 'appointment_enable_patient_creation': return true;
            case 'appointment_hide_start_times': return false;
            case 'appointment_ignore_overlap': return false;
            case 'appointment_include_recent_note': return false;      // Appointment Book - Include Most Recent Note
            case 'appointment_nudge_events': return false;
            case 'appointment_overwrite_service_code': return false;
            case 'appointment_search_date_end': return null;
            case 'appointment_search_date_start': return null;
            case 'appointment_search_default_columns': return array_keys(AppointmentSearchForm::getResultsHeader(false, true));
            case 'appointment_search_default_sort': return 'date';
            case 'appointment_search_default_statuses': return array('all');
            case 'appointment_search_pdf_include_date_range': return false;      // Show Dates in Appointment Search PDF Header
            case 'appointment_search_remove_provinces': return false;
            case 'appointment_search_include_inactive_doctors': return false;
            case 'appointment_search_remove_regions': return false;
            case 'appointment_search_report_header': return 'Appointments Report';
            case 'appointment_show_end_times': return false;
            case 'appointment_fade_non_booked': return false;
            case 'appointment_skip_confirm_on_reschedule': return false;
            case 'appointment_title_alignment': return 'center';
            case 'appointment_title_format': return '%appointment_type%: %patient_name%';
            case 'appointment_title_red_name': return false;
            case 'autofill_claims': return true;
            case 'autofill_service_date': return true;
            case 'chart_numbers': return true;
            case 'claim_always_show_temporary_button': return true;
            case 'claim_item_interval_check': return false;
            case 'claim_pdf_payee': return 'doctor';
            case 'claim_pdf_payee_id': return null;
            case 'claim_search_creation_date_end': return 'today';
            case 'claim_search_creation_date_start': return '';
            case 'claim_search_default_claim_status': return array('all');
            case 'claim_search_default_claim_type': return array('all');
            case 'claim_search_remove_dob': return false;
            case 'claim_search_remove_refdoc': return false;
            case 'claim_search_remove_sex': return false;
            case 'claim_search_service_date_end': return 'today';
            case 'claim_search_service_date_start': return '';
            case 'claim_update_appointment_status': return false;
            case 'claims_default_suffix': return 'A';
            case 'claims_show_direct_codes_for_third_party': return false;
            case 'claims_skip_search_tab': return false;
            case 'client_hl7_inbound_partners': return null;
            case 'client_hl7_outbound_partners': return null;
            case 'client_hl7_partners': return null;
            case 'consultation_appointment_title': return 'Consultation (%patient_count%/%max_patients%)';
            case 'consultation_cancel_note': return null;
            case 'consultation_cancel_patient_status': return null;
            case 'consultation_cancel_reason_code': return null;
            case 'consultation_cancellation_window': return 48;
            case 'consultation_colour': return '#68A1E5';
            case 'consultation_earliest_time': return '06:00';
            case 'consultation_latest_time': return '13:00';
            case 'consultation_no_show_note': return null;
            case 'consultation_no_show_patient_status': return null;
            case 'consultation_no_show_reason_code': return null;
            case 'consultation_patient_note': return null;
            case 'consultation_patient_reason_code': return null;
            case 'consultation_patient_status': return null;
            case 'consultation_relocate_on_reschedule': return true;
            case 'consultation_title_bar_colour': return '#000682';
            case 'consult_length': return 90;
            case 'dashboard_appointment_book_widget': return true;
            case 'dashboard_appointment_follow_up_widget': return true;
            case 'dashboard_appointment_orphans_widget': return true;
            case 'dashboard_follow_up_widget': return true;
            case 'date_format': return 'd/m/Y';
            case 'default_patient_type_id': return null;
            case 'default_stage_id': return null;
            case 'direct_autocheck_make_a_payment': return true;
            case 'doctor_title_format': return 'getDropdownString2';
            case 'doctor_id': return 'getDoctorId';
            case 'echobase_create_practice_send_date': return null;
            case 'echobase_practice_admin': return null;
            case 'echobase_practice_adminpw': return null;
            case 'echobase_practice_name': return null;
            case 'enable_print_service': return false;
            case 'fill_down_diag_code': return true;
            case 'font_options': return 8;
            case 'generate_blank_appointments': return false;
            case 'generate_blank_appointment_type_id': return null;
            case 'generate_blank_fname': return 'TEMPORARY';
            case 'generate_blank_lname': return 'HUMAN';
            case 'google_maps_api_key': return 'AIzaSyCLJ1GDoZkxKQn_3W4o-Qo9xvF7Cxf6WZE';
            case 'location_open_weekends': return true;
            case 'location_title': return 'Location';
            case 'mdc_send_email': return false;        // TODO: deprecate
            case 'mdc_send_email_subject': return 'Appointment Notification';   // TODO: deprecate
            case 'new_direct_always_make_payment': return true;
            case 'no_claim_no_location_warning': return false;
            case 'past_claims_report_header': return 'Billing Cycle Report';
            case 'patient_alpha_patient_numbers': return false;
            case 'patient_appointment_check': return array();
            case 'patient_default_province': return 'ON';
            case 'patient_dob_limit': return true;
            case 'patient_form_default_follow_up_date': return 'none';
            case 'patient_search_always_show_results_dialog': return false;
            case 'patient_search_first': return false;
            case 'patient_search_match_type': return 'exact';      // allows users to jump down to health card number when a long number is entered
            case 'patient_search_skip_to_hcn': return true;
            case 'patient_status_default': return null;
            case 'patient_suggested_fields': return array('sex', 'dob', 'ref_doc_description', 'hcn_num');
            case 'patient_type_check': return array();
            case 'pay_progs': return array_keys(ClaimTable::getClaimTypes());
            case 'pdf_footer_hype_systems': return 'Solution Designed by Hype Systems Inc.';
            case 'pdf_preference': return 'old';
            case 'require_doctor_email': return false;
            case 'require_loc_id': return false;
            case 'dialog_box_printed_number_of_copy': return false;
            case 'require_policy_num': return true;
            case 'reporting_batch_submission_date': return 'current_date';
            case 'service_code_group_number_check': return true;
            case 'service_code_tech_require_ref_doc': return true;
            case 'service_date_multiselect': return false;
            case 'show_report_widget': return false;      // TODO: deprecate this widget - My Doctor Care only
            case 'spec_code_on': return null;
            case 'split_claim': return false;
            case 'subtotal_current_claim': return true;
            case 'swipe_replace': return true;
            case 'time_format': return 'H:i';
            case 'update_claim_on_patient_save_unsubmitted': return false;
            case 'update_claim_on_patient_save_submitted': return false;
            case 'update_claim_on_patient_save_rejected': return false;
            case 'update_existing_claims': return true;
            case 'use_complex_appointment_types': return false;
            case 'use_moh_province': return false;
            case 'visit_sheet_letterhead_size': return 0;
            case 'visit_sheet_include_notes': return true;
            case 'claim_patient_require_name_fields': return true;
            case 'include_inactive_doctors': return false;
            case 'patient_save_update_appointments': return true;
            case 'patient_history_selection': return false;
            case 'premium_code_bypass': return false;
            case 'new_pdf_flag': return true;
            case 'down_arrow_option': return false;
            case 'load_last_five_services': return false;
            case 'compare_autofill': return false;
            case 'consultation_length': return 90; // TODO: deprecate
            case 'is_default_doctor': return false;
            case 'claims_default_doctor': return null;
            case 'appt_srch_default_btns': return null;
            case 'billing_cycle_srch_default_btns': return null;
            case 'text_color': return null;
            case 'font_sizes': return 0;
            case 'appt_book_sidemenu_position': return 'max';
            case 'dt_page_size' : return '50';
            case 'main_menu_pos' : return 'max';
            case 'appt_side_bar' : return 'appt_max';
            case 'dashboard_workflow' : return 'min';
            case 'past_claims_default_report_cols' : return '';
            case 'transfer_patient_data_to_claim_form' : return false;
            case 'quick_service_bg_color' : return '';
            case 'direct_service_bg_color' : return '';
            case 'quick_service_font_color' : return '';
            case 'direct_service_font_color' : return '';
            default:
                throw new Exception('Undefined Client Parameter in ClientParamTable::getDefaultForValue() - ' . $parameterName);
                break;
        }

        return $parameterName;
    }

    public static function getParamForClient($client_id, $param_name) {
        if ($client_id) {
            return Doctrine_Query::create()
                            ->from('ClientParam cp')
                            ->addWhere('cp.client_id = ?', $client_id)
                            ->addWhere('cp.param = ?', $param_name)
                            ->fetchOne();
        }
    }

    public static function getSpecificDefaults($columns) {
        $rs = array();
        foreach ($columns as $column) {
            $val = ClientParamTable::getDefaultForValue($column);
            if ($val) {
                $rs[$column] = $val;
            }
        }
        return $rs;
    }

    public static function setParamForClient($client_id, $param_name, $value) {
        $cp = Doctrine_Query::create()
                ->from('ClientParam cp')
                ->addWhere('cp.client_id = ?', $client_id)
                ->addWhere('cp.param = ?', $param_name)
                ->fetchOne();

        if (!$cp instanceof ClientParam) {
            $cp = new ClientParam();
            $cp->setParam($param_name);
            $cp->setClientId($client_id);
        }

        $cp->setValue($value);
        $cp->save();

        return $cp;
    }

    public static function getInstance() {
        return Doctrine_Core::getTable('ClientParam');
    }

    public static function getPatientSuggestedFieldChoices() {
        return array(
            'sex' => 'Sex',
            'dob' => 'Date Of Birth',
            'ref_doc_description' => 'Referring Doctor',
            'hcn_num' => 'Health Card #',
        );
    }

    public static function getCounter($fieldname, $client_id, $str_length = 0) {
        $param = Doctrine_Query::create()
                ->from('ClientParam sp')
                ->where('sp.name = ?', $fieldname)
                ->addWhere('sp.client_id = (?)', array($client_id))
                ->fetchOne();

        if ($param) {
            $val = intval($param->value);
        } else {
            $val = 1;
            $param = new SystemParam();
            $param->name = $fieldname;
        }
        $param->value = $val + 1;
        $param->save();

        if ($str_length) {
            $val = $val % pow(10, $str_length);
//            if ($val == 0) {
//                $val++;
//            }

            $val = strval($val);
            while (strlen($val) != $str_length) {
                $val = "0" . $val;
            }
        }

        return $val;
    }

}
