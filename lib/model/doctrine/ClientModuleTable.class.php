<?php

class ClientModuleTable extends Doctrine_Table
{
 	public static function hasHL7($client_id)
     {
     	$client_module = Doctrine_Query::create()
     		->from('ClientModule cm')
     		->addWhere('cm.client_id = (?)', $client_id)
     		->addWhere('cm.module_name = (?)', hype::MODULE_HL7)
     		->fetchOne();

     	if ($client_module instanceof ClientModule) {
     		return $client_module->active;
     	}
     	return false;
     }
 	public static function getInstance()
     {
         return Doctrine_Core::getTable('ClientModule');
     }
}