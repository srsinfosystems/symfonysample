<?php

/**
 * BaseReconcileBatch
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property int $reconcile_id
 * @property int $batch_id
 * @property Reconcile $Reconcile
 * @property Batch $Batch
 * 
 * @method int            getReconcileId()  Returns the current record's "reconcile_id" value
 * @method int            getBatchId()      Returns the current record's "batch_id" value
 * @method Reconcile      getReconcile()    Returns the current record's "Reconcile" value
 * @method Batch          getBatch()        Returns the current record's "Batch" value
 * @method ReconcileBatch setReconcileId()  Sets the current record's "reconcile_id" value
 * @method ReconcileBatch setBatchId()      Sets the current record's "batch_id" value
 * @method ReconcileBatch setReconcile()    Sets the current record's "Reconcile" value
 * @method ReconcileBatch setBatch()        Sets the current record's "Batch" value
 * 
 * @package    HypeMedical
 * @subpackage model
 * @author     HYPE Systems
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseReconcileBatch extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('reconcile_batch');
        $this->hasColumn('reconcile_id', 'int', null, array(
             'type' => 'int',
             'primary' => true,
             ));
        $this->hasColumn('batch_id', 'int', null, array(
             'type' => 'int',
             'primary' => true,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Reconcile', array(
             'local' => 'reconcile_id',
             'foreign' => 'id'));

        $this->hasOne('Batch', array(
             'local' => 'batch_id',
             'foreign' => 'id'));
    }
}