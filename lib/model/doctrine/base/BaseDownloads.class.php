<?php

/**
 * BaseDownloads
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $file_name
 * @property integer $client_id
 * @property text $file_content
 * @property text $provider_label
 * @property int $status
 * 
 * @method integer   getFileName()       Returns the current record's "file_name" value
 * @method integer   getClientId()       Returns the current record's "client_id" value
 * @method text      getFileContent()    Returns the current record's "file_content" value
 * @method text      getProviderLabel()  Returns the current record's "provider_label" value
 * @method int       getStatus()         Returns the current record's "status" value
 * @method Downloads setFileName()       Sets the current record's "file_name" value
 * @method Downloads setClientId()       Sets the current record's "client_id" value
 * @method Downloads setFileContent()    Sets the current record's "file_content" value
 * @method Downloads setProviderLabel()  Sets the current record's "provider_label" value
 * @method Downloads setStatus()         Sets the current record's "status" value
 * 
 * @package    HypeMedical
 * @subpackage model
 * @author     HYPE Systems
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseDownloads extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('downloads');
        $this->hasColumn('file_name', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('client_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('file_content', 'text', null, array(
             'type' => 'text',
             ));
        $this->hasColumn('provider_label', 'text', null, array(
             'type' => 'text',
             ));
        $this->hasColumn('status', 'int', null, array(
             'type' => 'int',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $auditable0 = new Auditable(array(
             ));
        $versionable0 = new Doctrine_Template_Versionable(array(
             ));
        $this->actAs($timestampable0);
        $this->actAs($auditable0);
        $this->actAs($versionable0);
    }
}