<?php

/**
 * BaseThirdPartyInvoicePayment
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $client_id
 * @property integer $third_party_invoice_id
 * @property float $payment_amount
 * @property datetime $payment_date
 * @property Client $Client
 * @property ThirdPartyInvoice $ThirdPartyInvoice
 * @property Doctrine_Collection $ThirdPartyInvoiceClaimItemPayment
 * 
 * @method integer                  getClientId()                          Returns the current record's "client_id" value
 * @method integer                  getThirdPartyInvoiceId()               Returns the current record's "third_party_invoice_id" value
 * @method float                    getPaymentAmount()                     Returns the current record's "payment_amount" value
 * @method datetime                 getPaymentDate()                       Returns the current record's "payment_date" value
 * @method Client                   getClient()                            Returns the current record's "Client" value
 * @method ThirdPartyInvoice        getThirdPartyInvoice()                 Returns the current record's "ThirdPartyInvoice" value
 * @method Doctrine_Collection      getThirdPartyInvoiceClaimItemPayment() Returns the current record's "ThirdPartyInvoiceClaimItemPayment" collection
 * @method ThirdPartyInvoicePayment setClientId()                          Sets the current record's "client_id" value
 * @method ThirdPartyInvoicePayment setThirdPartyInvoiceId()               Sets the current record's "third_party_invoice_id" value
 * @method ThirdPartyInvoicePayment setPaymentAmount()                     Sets the current record's "payment_amount" value
 * @method ThirdPartyInvoicePayment setPaymentDate()                       Sets the current record's "payment_date" value
 * @method ThirdPartyInvoicePayment setClient()                            Sets the current record's "Client" value
 * @method ThirdPartyInvoicePayment setThirdPartyInvoice()                 Sets the current record's "ThirdPartyInvoice" value
 * @method ThirdPartyInvoicePayment setThirdPartyInvoiceClaimItemPayment() Sets the current record's "ThirdPartyInvoiceClaimItemPayment" collection
 * 
 * @package    HypeMedical
 * @subpackage model
 * @author     HYPE Systems
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseThirdPartyInvoicePayment extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('third_party_invoice_payment');
        $this->hasColumn('client_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('third_party_invoice_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('payment_amount', 'float', null, array(
             'type' => 'float',
             ));
        $this->hasColumn('payment_date', 'datetime', null, array(
             'type' => 'datetime',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Client', array(
             'local' => 'client_id',
             'foreign' => 'id'));

        $this->hasOne('ThirdPartyInvoice', array(
             'local' => 'third_party_invoice_id',
             'foreign' => 'id'));

        $this->hasMany('ThirdPartyInvoiceClaimItemPayment', array(
             'local' => 'id',
             'foreign' => 'third_party_invoice_payment_id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $auditable0 = new Auditable(array(
             ));
        $versionable0 = new Doctrine_Template_Versionable(array(
             ));
        $this->actAs($timestampable0);
        $this->actAs($auditable0);
        $this->actAs($versionable0);
    }
}