<?php

/**
 * BaseUserLocation
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property int $user_id
 * @property int $location_id
 * @property UserProfile $UserProfile
 * @property Location $Location
 * 
 * @method int          getUserId()      Returns the current record's "user_id" value
 * @method int          getLocationId()  Returns the current record's "location_id" value
 * @method UserProfile  getUserProfile() Returns the current record's "UserProfile" value
 * @method Location     getLocation()    Returns the current record's "Location" value
 * @method UserLocation setUserId()      Sets the current record's "user_id" value
 * @method UserLocation setLocationId()  Sets the current record's "location_id" value
 * @method UserLocation setUserProfile() Sets the current record's "UserProfile" value
 * @method UserLocation setLocation()    Sets the current record's "Location" value
 * 
 * @package    HypeMedical
 * @subpackage model
 * @author     HYPE Systems
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseUserLocation extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('user_location');
        $this->hasColumn('user_id', 'int', null, array(
             'type' => 'int',
             'primary' => true,
             ));
        $this->hasColumn('location_id', 'int', null, array(
             'type' => 'int',
             'primary' => true,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('UserProfile', array(
             'local' => 'user_id',
             'foreign' => 'id'));

        $this->hasOne('Location', array(
             'local' => 'location_id',
             'foreign' => 'id'));
    }
}