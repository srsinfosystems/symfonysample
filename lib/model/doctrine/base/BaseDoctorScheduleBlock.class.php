<?php

/**
 * BaseDoctorScheduleBlock
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property int $doctor_general_schedule_id
 * @property int $client_id
 * @property int $doctor_id
 * @property int $location_id
 * @property date $start_date
 * @property date $end_date
 * @property integer $status
 * @property text $notes
 * @property DoctorGeneralSchedule $DoctorGeneralSchedule
 * @property Client $Client
 * @property Doctor $Doctor
 * @property Location $Location
 * 
 * @method int                   getDoctorGeneralScheduleId()    Returns the current record's "doctor_general_schedule_id" value
 * @method int                   getClientId()                   Returns the current record's "client_id" value
 * @method int                   getDoctorId()                   Returns the current record's "doctor_id" value
 * @method int                   getLocationId()                 Returns the current record's "location_id" value
 * @method date                  getStartDate()                  Returns the current record's "start_date" value
 * @method date                  getEndDate()                    Returns the current record's "end_date" value
 * @method integer               getStatus()                     Returns the current record's "status" value
 * @method text                  getNotes()                      Returns the current record's "notes" value
 * @method DoctorGeneralSchedule getDoctorGeneralSchedule()      Returns the current record's "DoctorGeneralSchedule" value
 * @method Client                getClient()                     Returns the current record's "Client" value
 * @method Doctor                getDoctor()                     Returns the current record's "Doctor" value
 * @method Location              getLocation()                   Returns the current record's "Location" value
 * @method DoctorScheduleBlock   setDoctorGeneralScheduleId()    Sets the current record's "doctor_general_schedule_id" value
 * @method DoctorScheduleBlock   setClientId()                   Sets the current record's "client_id" value
 * @method DoctorScheduleBlock   setDoctorId()                   Sets the current record's "doctor_id" value
 * @method DoctorScheduleBlock   setLocationId()                 Sets the current record's "location_id" value
 * @method DoctorScheduleBlock   setStartDate()                  Sets the current record's "start_date" value
 * @method DoctorScheduleBlock   setEndDate()                    Sets the current record's "end_date" value
 * @method DoctorScheduleBlock   setStatus()                     Sets the current record's "status" value
 * @method DoctorScheduleBlock   setNotes()                      Sets the current record's "notes" value
 * @method DoctorScheduleBlock   setDoctorGeneralSchedule()      Sets the current record's "DoctorGeneralSchedule" value
 * @method DoctorScheduleBlock   setClient()                     Sets the current record's "Client" value
 * @method DoctorScheduleBlock   setDoctor()                     Sets the current record's "Doctor" value
 * @method DoctorScheduleBlock   setLocation()                   Sets the current record's "Location" value
 * 
 * @package    HypeMedical
 * @subpackage model
 * @author     HYPE Systems
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseDoctorScheduleBlock extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('doctor_schedule_block');
        $this->hasColumn('doctor_general_schedule_id', 'int', null, array(
             'type' => 'int',
             'notnull' => true,
             ));
        $this->hasColumn('client_id', 'int', null, array(
             'type' => 'int',
             ));
        $this->hasColumn('doctor_id', 'int', null, array(
             'type' => 'int',
             'notnull' => true,
             ));
        $this->hasColumn('location_id', 'int', null, array(
             'type' => 'int',
             'notnull' => true,
             ));
        $this->hasColumn('start_date', 'date', null, array(
             'type' => 'date',
             'notnull' => true,
             ));
        $this->hasColumn('end_date', 'date', null, array(
             'type' => 'date',
             'notnull' => true,
             ));
        $this->hasColumn('status', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('notes', 'text', null, array(
             'type' => 'text',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('DoctorGeneralSchedule', array(
             'local' => 'doctor_general_schedule_id',
             'foreign' => 'id'));

        $this->hasOne('Client', array(
             'local' => 'client_id',
             'foreign' => 'id'));

        $this->hasOne('Doctor', array(
             'local' => 'doctor_id',
             'foreign' => 'id'));

        $this->hasOne('Location', array(
             'local' => 'location_id',
             'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $auditable0 = new Auditable(array(
             ));
        $versionable0 = new Doctrine_Template_Versionable(array(
             ));
        $this->actAs($timestampable0);
        $this->actAs($auditable0);
        $this->actAs($versionable0);
    }
}