<?php

/**
 * BaseVacation
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property int $client_id
 * @property int $doctor_id
 * @property datetime $start_date
 * @property datetime $end_date
 * @property boolean $active
 * @property Client $Client
 * @property Doctor $Doctor
 * @property Doctrine_Collection $VacationDay
 * 
 * @method int                 getClientId()    Returns the current record's "client_id" value
 * @method int                 getDoctorId()    Returns the current record's "doctor_id" value
 * @method datetime            getStartDate()   Returns the current record's "start_date" value
 * @method datetime            getEndDate()     Returns the current record's "end_date" value
 * @method boolean             getActive()      Returns the current record's "active" value
 * @method Client              getClient()      Returns the current record's "Client" value
 * @method Doctor              getDoctor()      Returns the current record's "Doctor" value
 * @method Doctrine_Collection getVacationDay() Returns the current record's "VacationDay" collection
 * @method Vacation            setClientId()    Sets the current record's "client_id" value
 * @method Vacation            setDoctorId()    Sets the current record's "doctor_id" value
 * @method Vacation            setStartDate()   Sets the current record's "start_date" value
 * @method Vacation            setEndDate()     Sets the current record's "end_date" value
 * @method Vacation            setActive()      Sets the current record's "active" value
 * @method Vacation            setClient()      Sets the current record's "Client" value
 * @method Vacation            setDoctor()      Sets the current record's "Doctor" value
 * @method Vacation            setVacationDay() Sets the current record's "VacationDay" collection
 * 
 * @package    HypeMedical
 * @subpackage model
 * @author     HYPE Systems
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseVacation extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('vacation');
        $this->hasColumn('client_id', 'int', null, array(
             'type' => 'int',
             'notnull' => true,
             ));
        $this->hasColumn('doctor_id', 'int', null, array(
             'type' => 'int',
             'notnull' => true,
             ));
        $this->hasColumn('start_date', 'datetime', null, array(
             'type' => 'datetime',
             'notnull' => true,
             ));
        $this->hasColumn('end_date', 'datetime', null, array(
             'type' => 'datetime',
             'notnull' => true,
             ));
        $this->hasColumn('active', 'boolean', null, array(
             'type' => 'boolean',
             'default' => true,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Client', array(
             'local' => 'client_id',
             'foreign' => 'id'));

        $this->hasOne('Doctor', array(
             'local' => 'doctor_id',
             'foreign' => 'id'));

        $this->hasMany('VacationDay', array(
             'local' => 'id',
             'foreign' => 'vacation_id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $auditable0 = new Auditable(array(
             ));
        $versionable0 = new Doctrine_Template_Versionable(array(
             ));
        $this->actAs($timestampable0);
        $this->actAs($auditable0);
        $this->actAs($versionable0);
    }
}