<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('Items', 'doctrine');

/**
 * BaseItems
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $claim_id
 * @property integer $item_id
 * @property string $service_code
 * @property string $diag_code
 * @property integer $num_serv
 * @property float $fee_subm
 * @property timestamp $service_date
 * @property float $fee_paid
 * @property integer $status
 * @property string $errors
 * @property string $order_id
 * @property string $ordernum
 * @property string $orderstatus
 * @property integer $placer_id
 * @property string $description1
 * @property string $description2
 * @property string $location
 * @property timestamp $created_at
 * @property timestamp $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $version
 * @property string $hl7_partner_id
 * @property string $invoice_note
 * @property integer $doctor_reviewed
 * @property integer $discounted
 * @property float $original_fee
 * @property float $base_fee
 * @property float $modifier
 * @property string $moh_claim
 * @property string $ra
 * @property integer $reconcile_id
 * @property integer $error_reconcile_id
 * @property string $created_username
 * @property string $updated_username
 * @property integer $client_id
 * @property string $created_user_id
 * @property string $updated_user_id
 * @property Placer $Placer
 * @property Claims $Claims
 * @property Client $Client
 * @property Reconcile $Reconcile
 * @property Doctrine_Collection $ErrorReportItem
 * @property Doctrine_Collection $RaItem
 * @property Doctrine_Collection $ThirdPartyInvoiceClaimItemPayment
 * 
 * @method integer             getId()                                Returns the current record's "id" value
 * @method integer             getClaimId()                           Returns the current record's "claim_id" value
 * @method integer             getItemId()                            Returns the current record's "item_id" value
 * @method string              getServiceCode()                       Returns the current record's "service_code" value
 * @method string              getDiagCode()                          Returns the current record's "diag_code" value
 * @method integer             getNumServ()                           Returns the current record's "num_serv" value
 * @method float               getFeeSubm()                           Returns the current record's "fee_subm" value
 * @method timestamp           getServiceDate()                       Returns the current record's "service_date" value
 * @method float               getFeePaid()                           Returns the current record's "fee_paid" value
 * @method integer             getStatus()                            Returns the current record's "status" value
 * @method string              getErrors()                            Returns the current record's "errors" value
 * @method string              getOrderId()                           Returns the current record's "order_id" value
 * @method string              getOrdernum()                          Returns the current record's "ordernum" value
 * @method string              getOrderstatus()                       Returns the current record's "orderstatus" value
 * @method integer             getPlacerId()                          Returns the current record's "placer_id" value
 * @method string              getDescription1()                      Returns the current record's "description1" value
 * @method string              getDescription2()                      Returns the current record's "description2" value
 * @method string              getLocation()                          Returns the current record's "location" value
 * @method timestamp           getCreatedAt()                         Returns the current record's "created_at" value
 * @method timestamp           getUpdatedAt()                         Returns the current record's "updated_at" value
 * @method integer             getCreatedBy()                         Returns the current record's "created_by" value
 * @method integer             getUpdatedBy()                         Returns the current record's "updated_by" value
 * @method integer             getVersion()                           Returns the current record's "version" value
 * @method string              getHl7PartnerId()                      Returns the current record's "hl7_partner_id" value
 * @method string              getInvoiceNote()                       Returns the current record's "invoice_note" value
 * @method integer             getDoctorReviewed()                    Returns the current record's "doctor_reviewed" value
 * @method integer             getDiscounted()                        Returns the current record's "discounted" value
 * @method float               getOriginalFee()                       Returns the current record's "original_fee" value
 * @method float               getBaseFee()                           Returns the current record's "base_fee" value
 * @method float               getModifier()                          Returns the current record's "modifier" value
 * @method string              getMohClaim()                          Returns the current record's "moh_claim" value
 * @method string              getRa()                                Returns the current record's "ra" value
 * @method integer             getReconcileId()                       Returns the current record's "reconcile_id" value
 * @method integer             getErrorReconcileId()                  Returns the current record's "error_reconcile_id" value
 * @method string              getCreatedUsername()                   Returns the current record's "created_username" value
 * @method string              getUpdatedUsername()                   Returns the current record's "updated_username" value
 * @method integer             getClientId()                          Returns the current record's "client_id" value
 * @method string              getCreatedUserId()                     Returns the current record's "created_user_id" value
 * @method string              getUpdatedUserId()                     Returns the current record's "updated_user_id" value
 * @method Placer              getPlacer()                            Returns the current record's "Placer" value
 * @method Claims              getClaims()                            Returns the current record's "Claims" value
 * @method Client              getClient()                            Returns the current record's "Client" value
 * @method Reconcile           getReconcile()                         Returns the current record's "Reconcile" value
 * @method Doctrine_Collection getErrorReportItem()                   Returns the current record's "ErrorReportItem" collection
 * @method Doctrine_Collection getRaItem()                            Returns the current record's "RaItem" collection
 * @method Doctrine_Collection getThirdPartyInvoiceClaimItemPayment() Returns the current record's "ThirdPartyInvoiceClaimItemPayment" collection
 * @method Items               setId()                                Sets the current record's "id" value
 * @method Items               setClaimId()                           Sets the current record's "claim_id" value
 * @method Items               setItemId()                            Sets the current record's "item_id" value
 * @method Items               setServiceCode()                       Sets the current record's "service_code" value
 * @method Items               setDiagCode()                          Sets the current record's "diag_code" value
 * @method Items               setNumServ()                           Sets the current record's "num_serv" value
 * @method Items               setFeeSubm()                           Sets the current record's "fee_subm" value
 * @method Items               setServiceDate()                       Sets the current record's "service_date" value
 * @method Items               setFeePaid()                           Sets the current record's "fee_paid" value
 * @method Items               setStatus()                            Sets the current record's "status" value
 * @method Items               setErrors()                            Sets the current record's "errors" value
 * @method Items               setOrderId()                           Sets the current record's "order_id" value
 * @method Items               setOrdernum()                          Sets the current record's "ordernum" value
 * @method Items               setOrderstatus()                       Sets the current record's "orderstatus" value
 * @method Items               setPlacerId()                          Sets the current record's "placer_id" value
 * @method Items               setDescription1()                      Sets the current record's "description1" value
 * @method Items               setDescription2()                      Sets the current record's "description2" value
 * @method Items               setLocation()                          Sets the current record's "location" value
 * @method Items               setCreatedAt()                         Sets the current record's "created_at" value
 * @method Items               setUpdatedAt()                         Sets the current record's "updated_at" value
 * @method Items               setCreatedBy()                         Sets the current record's "created_by" value
 * @method Items               setUpdatedBy()                         Sets the current record's "updated_by" value
 * @method Items               setVersion()                           Sets the current record's "version" value
 * @method Items               setHl7PartnerId()                      Sets the current record's "hl7_partner_id" value
 * @method Items               setInvoiceNote()                       Sets the current record's "invoice_note" value
 * @method Items               setDoctorReviewed()                    Sets the current record's "doctor_reviewed" value
 * @method Items               setDiscounted()                        Sets the current record's "discounted" value
 * @method Items               setOriginalFee()                       Sets the current record's "original_fee" value
 * @method Items               setBaseFee()                           Sets the current record's "base_fee" value
 * @method Items               setModifier()                          Sets the current record's "modifier" value
 * @method Items               setMohClaim()                          Sets the current record's "moh_claim" value
 * @method Items               setRa()                                Sets the current record's "ra" value
 * @method Items               setReconcileId()                       Sets the current record's "reconcile_id" value
 * @method Items               setErrorReconcileId()                  Sets the current record's "error_reconcile_id" value
 * @method Items               setCreatedUsername()                   Sets the current record's "created_username" value
 * @method Items               setUpdatedUsername()                   Sets the current record's "updated_username" value
 * @method Items               setClientId()                          Sets the current record's "client_id" value
 * @method Items               setCreatedUserId()                     Sets the current record's "created_user_id" value
 * @method Items               setUpdatedUserId()                     Sets the current record's "updated_user_id" value
 * @method Items               setPlacer()                            Sets the current record's "Placer" value
 * @method Items               setClaims()                            Sets the current record's "Claims" value
 * @method Items               setClient()                            Sets the current record's "Client" value
 * @method Items               setReconcile()                         Sets the current record's "Reconcile" value
 * @method Items               setErrorReportItem()                   Sets the current record's "ErrorReportItem" collection
 * @method Items               setRaItem()                            Sets the current record's "RaItem" collection
 * @method Items               setThirdPartyInvoiceClaimItemPayment() Sets the current record's "ThirdPartyInvoiceClaimItemPayment" collection
 * 
 * @package    HypeMedical
 * @subpackage model
 * @author     HYPE Systems
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseItems extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('items');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('claim_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('item_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('service_code', 'string', 25, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('diag_code', 'string', 4, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('num_serv', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('fee_subm', 'float', null, array(
             'type' => 'float',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('service_date', 'timestamp', 25, array(
             'type' => 'timestamp',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('fee_paid', 'float', null, array(
             'type' => 'float',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('status', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('errors', 'string', 55, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 55,
             ));
        $this->hasColumn('order_id', 'string', 50, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 50,
             ));
        $this->hasColumn('ordernum', 'string', 50, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 50,
             ));
        $this->hasColumn('orderstatus', 'string', 50, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 50,
             ));
        $this->hasColumn('placer_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('description1', 'string', 50, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 50,
             ));
        $this->hasColumn('description2', 'string', null, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('location', 'string', 50, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 50,
             ));
        $this->hasColumn('created_at', 'timestamp', 25, array(
             'type' => 'timestamp',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('updated_at', 'timestamp', 25, array(
             'type' => 'timestamp',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('created_by', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('updated_by', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('version', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('hl7_partner_id', 'string', 100, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 100,
             ));
        $this->hasColumn('invoice_note', 'string', null, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('doctor_reviewed', 'integer', 1, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 1,
             ));
        $this->hasColumn('discounted', 'integer', 1, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 1,
             ));
        $this->hasColumn('original_fee', 'float', null, array(
             'type' => 'float',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('base_fee', 'float', null, array(
             'type' => 'float',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('modifier', 'float', null, array(
             'type' => 'float',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('moh_claim', 'string', 50, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 50,
             ));
        $this->hasColumn('ra', 'string', 250, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 250,
             ));
        $this->hasColumn('reconcile_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('error_reconcile_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('created_username', 'string', 183, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 183,
             ));
        $this->hasColumn('updated_username', 'string', 183, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 183,
             ));
        $this->hasColumn('client_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('created_user_id', 'string', 183, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 183,
             ));
        $this->hasColumn('updated_user_id', 'string', 183, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 183,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Placer', array(
             'local' => 'placer_id',
             'foreign' => 'id'));

        $this->hasOne('Claims', array(
             'local' => 'claim_id',
             'foreign' => 'id'));

        $this->hasOne('Client', array(
             'local' => 'client_id',
             'foreign' => 'id'));

        $this->hasOne('Reconcile', array(
             'local' => 'reconcile_id',
             'foreign' => 'id'));

        $this->hasMany('ErrorReportItem', array(
             'local' => 'id',
             'foreign' => 'item_id'));

        $this->hasMany('RaItem', array(
             'local' => 'id',
             'foreign' => 'item_id'));

        $this->hasMany('ThirdPartyInvoiceClaimItemPayment', array(
             'local' => 'id',
             'foreign' => 'item_id'));
    }
}