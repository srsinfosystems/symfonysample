<?php

/**
 * BaseSystemParam
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $name
 * @property string $value
 * 
 * @method string      getName()  Returns the current record's "name" value
 * @method string      getValue() Returns the current record's "value" value
 * @method SystemParam setName()  Sets the current record's "name" value
 * @method SystemParam setValue() Sets the current record's "value" value
 * 
 * @package    HypeMedical
 * @subpackage model
 * @author     HYPE Systems
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseSystemParam extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('system_params');
        $this->hasColumn('name', 'string', 30, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 30,
             ));
        $this->hasColumn('value', 'string', 50, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 50,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $versionable0 = new Doctrine_Template_Versionable(array(
             'tableName' => 'system_params_version',
             ));
        $this->actAs($versionable0);
    }
}