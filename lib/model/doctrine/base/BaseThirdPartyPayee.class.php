<?php

/**
 * BaseThirdPartyPayee
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property int $client_id
 * @property string $name
 * @property string $address1
 * @property string $address2
 * @property string $city
 * @property string $province
 * @property string $postal_code
 * @property boolean $active
 * @property Client $Client
 * @property Doctrine_Collection $ThirdPartyInvoice
 * 
 * @method int                 getClientId()          Returns the current record's "client_id" value
 * @method string              getName()              Returns the current record's "name" value
 * @method string              getAddress1()          Returns the current record's "address1" value
 * @method string              getAddress2()          Returns the current record's "address2" value
 * @method string              getCity()              Returns the current record's "city" value
 * @method string              getProvince()          Returns the current record's "province" value
 * @method string              getPostalCode()        Returns the current record's "postal_code" value
 * @method boolean             getActive()            Returns the current record's "active" value
 * @method Client              getClient()            Returns the current record's "Client" value
 * @method Doctrine_Collection getThirdPartyInvoice() Returns the current record's "ThirdPartyInvoice" collection
 * @method ThirdPartyPayee     setClientId()          Sets the current record's "client_id" value
 * @method ThirdPartyPayee     setName()              Sets the current record's "name" value
 * @method ThirdPartyPayee     setAddress1()          Sets the current record's "address1" value
 * @method ThirdPartyPayee     setAddress2()          Sets the current record's "address2" value
 * @method ThirdPartyPayee     setCity()              Sets the current record's "city" value
 * @method ThirdPartyPayee     setProvince()          Sets the current record's "province" value
 * @method ThirdPartyPayee     setPostalCode()        Sets the current record's "postal_code" value
 * @method ThirdPartyPayee     setActive()            Sets the current record's "active" value
 * @method ThirdPartyPayee     setClient()            Sets the current record's "Client" value
 * @method ThirdPartyPayee     setThirdPartyInvoice() Sets the current record's "ThirdPartyInvoice" collection
 * 
 * @package    HypeMedical
 * @subpackage model
 * @author     HYPE Systems
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseThirdPartyPayee extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('third_party_payee');
        $this->hasColumn('client_id', 'int', null, array(
             'type' => 'int',
             'notnull' => true,
             ));
        $this->hasColumn('name', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('address1', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('address2', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('city', 'string', 60, array(
             'type' => 'string',
             'length' => 60,
             ));
        $this->hasColumn('province', 'string', 5, array(
             'type' => 'string',
             'length' => 5,
             ));
        $this->hasColumn('postal_code', 'string', 10, array(
             'type' => 'string',
             'length' => 10,
             ));
        $this->hasColumn('active', 'boolean', null, array(
             'type' => 'boolean',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Client', array(
             'local' => 'client_id',
             'foreign' => 'id'));

        $this->hasMany('ThirdPartyInvoice', array(
             'local' => 'id',
             'foreign' => 'payee_id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $auditable0 = new Auditable(array(
             ));
        $versionable0 = new Doctrine_Template_Versionable(array(
             ));
        $this->actAs($timestampable0);
        $this->actAs($auditable0);
        $this->actAs($versionable0);
    }
}