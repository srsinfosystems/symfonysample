<?php

/**
 * BaseUserPermissionNew
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property int $user_id
 * @property int $permission_constant
 * @property string $permission_name
 * @property boolean $active
 * @property UserProfile $UserProfile
 * 
 * @method int               getUserId()              Returns the current record's "user_id" value
 * @method int               getPermissionConstant()  Returns the current record's "permission_constant" value
 * @method string            getPermissionName()      Returns the current record's "permission_name" value
 * @method boolean           getActive()              Returns the current record's "active" value
 * @method UserProfile       getUserProfile()         Returns the current record's "UserProfile" value
 * @method UserPermissionNew setUserId()              Sets the current record's "user_id" value
 * @method UserPermissionNew setPermissionConstant()  Sets the current record's "permission_constant" value
 * @method UserPermissionNew setPermissionName()      Sets the current record's "permission_name" value
 * @method UserPermissionNew setActive()              Sets the current record's "active" value
 * @method UserPermissionNew setUserProfile()         Sets the current record's "UserProfile" value
 * 
 * @package    HypeMedical
 * @subpackage model
 * @author     HYPE Systems
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseUserPermissionNew extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('user_permission_new');
        $this->hasColumn('user_id', 'int', null, array(
             'type' => 'int',
             ));
        $this->hasColumn('permission_constant', 'int', null, array(
             'type' => 'int',
             ));
        $this->hasColumn('permission_name', 'string', 30, array(
             'type' => 'string',
             'length' => 30,
             ));
        $this->hasColumn('active', 'boolean', null, array(
             'type' => 'boolean',
             'default' => true,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('UserProfile', array(
             'local' => 'user_id',
             'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $auditable0 = new Auditable(array(
             ));
        $versionable0 = new Doctrine_Template_Versionable(array(
             ));
        $this->actAs($timestampable0);
        $this->actAs($auditable0);
        $this->actAs($versionable0);
    }
}