<?php

/**
 * BaseDirectServiceCode
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property int $client_id
 * @property string $service_code
 * @property text $description
 * @property float $fee
 * @property boolean $active
 * @property string $ohip_codes
 * @property Client $Client
 * @property Doctrine_Collection $DirectServiceCodeTax
 * @property Doctrine_Collection $DirectServiceCodeDiscount
 * 
 * @method int                 getClientId()                  Returns the current record's "client_id" value
 * @method string              getServiceCode()               Returns the current record's "service_code" value
 * @method text                getDescription()               Returns the current record's "description" value
 * @method float               getFee()                       Returns the current record's "fee" value
 * @method boolean             getActive()                    Returns the current record's "active" value
 * @method string              getOhipCodes()                 Returns the current record's "ohip_codes" value
 * @method Client              getClient()                    Returns the current record's "Client" value
 * @method Doctrine_Collection getDirectServiceCodeTax()      Returns the current record's "DirectServiceCodeTax" collection
 * @method Doctrine_Collection getDirectServiceCodeDiscount() Returns the current record's "DirectServiceCodeDiscount" collection
 * @method DirectServiceCode   setClientId()                  Sets the current record's "client_id" value
 * @method DirectServiceCode   setServiceCode()               Sets the current record's "service_code" value
 * @method DirectServiceCode   setDescription()               Sets the current record's "description" value
 * @method DirectServiceCode   setFee()                       Sets the current record's "fee" value
 * @method DirectServiceCode   setActive()                    Sets the current record's "active" value
 * @method DirectServiceCode   setOhipCodes()                 Sets the current record's "ohip_codes" value
 * @method DirectServiceCode   setClient()                    Sets the current record's "Client" value
 * @method DirectServiceCode   setDirectServiceCodeTax()      Sets the current record's "DirectServiceCodeTax" collection
 * @method DirectServiceCode   setDirectServiceCodeDiscount() Sets the current record's "DirectServiceCodeDiscount" collection
 * 
 * @package    HypeMedical
 * @subpackage model
 * @author     HYPE Systems
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseDirectServiceCode extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('direct_service_codes');
        $this->hasColumn('client_id', 'int', null, array(
             'type' => 'int',
             ));
        $this->hasColumn('service_code', 'string', 25, array(
             'type' => 'string',
             'unique' => true,
             'length' => 25,
             ));
        $this->hasColumn('description', 'text', null, array(
             'type' => 'text',
             ));
        $this->hasColumn('fee', 'float', null, array(
             'type' => 'float',
             ));
        $this->hasColumn('active', 'boolean', null, array(
             'type' => 'boolean',
             'default' => true,
             ));
        $this->hasColumn('ohip_codes', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('doctor_ids', 'string', 5, array(
            'type' => 'text'
        ));

        $this->hasColumn('direct_service_bg_color', 'string', 5, array(
            'type' => 'text'
        ));

        $this->hasColumn('direct_service_font_color', 'string', 5, array(
            'type' => 'text'
        ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Client', array(
             'local' => 'client_id',
             'foreign' => 'id'));

        $this->hasMany('DirectServiceCodeTax', array(
             'local' => 'id',
             'foreign' => 'direct_service_code_id'));

        $this->hasMany('DirectServiceCodeDiscount', array(
             'local' => 'id',
             'foreign' => 'direct_service_code_id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $auditable0 = new Auditable();
        $versionable0 = new Doctrine_Template_Versionable(array(
             'tableName' => 'direct_service_codes_version',
             ));
        $this->actAs($timestampable0);
        $this->actAs($auditable0);
        $this->actAs($versionable0);
    }
}