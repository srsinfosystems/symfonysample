<?php

/**
 * BaseAppointment
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property int $client_id
 * @property int $location_id
 * @property string $doctor_code
 * @property int $max_attendees
 * @property timestamp $start_date
 * @property timestamp $end_date
 * @property int $slot_type
 * @property string $notes
 * @property int $stage_client_id
 * @property string $service_code
 * @property string $diag_code
 * @property int $consultation_template_id
 * @property int $status
 * @property int $doctor_id
 * @property int $appointment_type_id
 * @property boolean $orphan
 * @property int $appointment_template_id
 * @property boolean $is_new_hl7
 * @property boolean $doctor_reviewed
 * @property boolean $dummy_data
 * @property Client $Client
 * @property Location $Location
 * @property StageClient $StageClient
 * @property Claim $Claim
 * @property ConsultationTemplate $ConsultationTemplate
 * @property Doctor $Doctor
 * @property AppointmentType $AppointmentType
 * @property AppointmentTemplate $AppointmentTemplate
 * @property Doctrine_Collection $AppointmentAttendee
 * @property Doctrine_Collection $AppointmentStageTrack
 * 
 * @method int                  getClientId()                 Returns the current record's "client_id" value
 * @method int                  getLocationId()               Returns the current record's "location_id" value
 * @method string               getDoctorCode()               Returns the current record's "doctor_code" value
 * @method int                  getMaxAttendees()             Returns the current record's "max_attendees" value
 * @method timestamp            getStartDate()                Returns the current record's "start_date" value
 * @method timestamp            getEndDate()                  Returns the current record's "end_date" value
 * @method int                  getSlotType()                 Returns the current record's "slot_type" value
 * @method string               getNotes()                    Returns the current record's "notes" value
 * @method int                  getStageClientId()            Returns the current record's "stage_client_id" value
 * @method string               getServiceCode()              Returns the current record's "service_code" value
 * @method string               getDiagCode()                 Returns the current record's "diag_code" value
 * @method int                  getConsultationTemplateId()   Returns the current record's "consultation_template_id" value
 * @method int                  getStatus()                   Returns the current record's "status" value
 * @method int                  getDoctorId()                 Returns the current record's "doctor_id" value
 * @method int                  getAppointmentTypeId()        Returns the current record's "appointment_type_id" value
 * @method boolean              getOrphan()                   Returns the current record's "orphan" value
 * @method int                  getAppointmentTemplateId()    Returns the current record's "appointment_template_id" value
 * @method boolean              getIsNewHl7()                 Returns the current record's "is_new_hl7" value
 * @method boolean              getDoctorReviewed()           Returns the current record's "doctor_reviewed" value
 * @method boolean              getDummyData()                Returns the current record's "dummy_data" value
 * @method Client               getClient()                   Returns the current record's "Client" value
 * @method Location             getLocation()                 Returns the current record's "Location" value
 * @method StageClient          getStageClient()              Returns the current record's "StageClient" value
 * @method Claim                getClaim()                    Returns the current record's "Claim" value
 * @method ConsultationTemplate getConsultationTemplate()     Returns the current record's "ConsultationTemplate" value
 * @method Doctor               getDoctor()                   Returns the current record's "Doctor" value
 * @method AppointmentType      getAppointmentType()          Returns the current record's "AppointmentType" value
 * @method AppointmentTemplate  getAppointmentTemplate()      Returns the current record's "AppointmentTemplate" value
 * @method Doctrine_Collection  getAppointmentAttendee()      Returns the current record's "AppointmentAttendee" collection
 * @method Doctrine_Collection  getAppointmentStageTrack()    Returns the current record's "AppointmentStageTrack" collection
 * @method Appointment          setClientId()                 Sets the current record's "client_id" value
 * @method Appointment          setLocationId()               Sets the current record's "location_id" value
 * @method Appointment          setDoctorCode()               Sets the current record's "doctor_code" value
 * @method Appointment          setMaxAttendees()             Sets the current record's "max_attendees" value
 * @method Appointment          setStartDate()                Sets the current record's "start_date" value
 * @method Appointment          setEndDate()                  Sets the current record's "end_date" value
 * @method Appointment          setSlotType()                 Sets the current record's "slot_type" value
 * @method Appointment          setNotes()                    Sets the current record's "notes" value
 * @method Appointment          setStageClientId()            Sets the current record's "stage_client_id" value
 * @method Appointment          setServiceCode()              Sets the current record's "service_code" value
 * @method Appointment          setDiagCode()                 Sets the current record's "diag_code" value
 * @method Appointment          setConsultationTemplateId()   Sets the current record's "consultation_template_id" value
 * @method Appointment          setStatus()                   Sets the current record's "status" value
 * @method Appointment          setDoctorId()                 Sets the current record's "doctor_id" value
 * @method Appointment          setAppointmentTypeId()        Sets the current record's "appointment_type_id" value
 * @method Appointment          setOrphan()                   Sets the current record's "orphan" value
 * @method Appointment          setAppointmentTemplateId()    Sets the current record's "appointment_template_id" value
 * @method Appointment          setIsNewHl7()                 Sets the current record's "is_new_hl7" value
 * @method Appointment          setDoctorReviewed()           Sets the current record's "doctor_reviewed" value
 * @method Appointment          setDummyData()                Sets the current record's "dummy_data" value
 * @method Appointment          setClient()                   Sets the current record's "Client" value
 * @method Appointment          setLocation()                 Sets the current record's "Location" value
 * @method Appointment          setStageClient()              Sets the current record's "StageClient" value
 * @method Appointment          setClaim()                    Sets the current record's "Claim" value
 * @method Appointment          setConsultationTemplate()     Sets the current record's "ConsultationTemplate" value
 * @method Appointment          setDoctor()                   Sets the current record's "Doctor" value
 * @method Appointment          setAppointmentType()          Sets the current record's "AppointmentType" value
 * @method Appointment          setAppointmentTemplate()      Sets the current record's "AppointmentTemplate" value
 * @method Appointment          setAppointmentAttendee()      Sets the current record's "AppointmentAttendee" collection
 * @method Appointment          setAppointmentStageTrack()    Sets the current record's "AppointmentStageTrack" collection
 * 
 * @package    HypeMedical
 * @subpackage model
 * @author     HYPE Systems
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseAppointment extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('appointments');
        $this->hasColumn('client_id', 'int', null, array(
             'type' => 'int',
             'notnull' => true,
             ));
        $this->hasColumn('location_id', 'int', null, array(
             'type' => 'int',
             'notnull' => true,
             ));
        $this->hasColumn('doctor_code', 'string', 8, array(
             'type' => 'string',
             'length' => 8,
             ));
        $this->hasColumn('max_attendees', 'int', null, array(
             'type' => 'int',
             'default' => 1,
             ));
        $this->hasColumn('patient_id', 'int', null, array(
             'type' => 'int',
             'default' => null,
             ));
        $this->hasColumn('start_date', 'timestamp', null, array(
             'type' => 'timestamp',
             ));
        $this->hasColumn('end_date', 'timestamp', null, array(
             'type' => 'timestamp',
             ));
        $this->hasColumn('slot_type', 'int', null, array(
             'type' => 'int',
             ));
        $this->hasColumn('notes', 'string', 250, array(
             'type' => 'string',
             'length' => 250,
             ));
        $this->hasColumn('stage_client_id', 'int', null, array(
             'type' => 'int',
             ));
        $this->hasColumn('service_code', 'string', 50, array(
             'type' => 'string',
             'length' => 50,
             ));
        $this->hasColumn('diag_code', 'string', 4, array(
             'type' => 'string',
             'length' => 4,
             ));
        $this->hasColumn('consultation_template_id', 'int', null, array(
             'type' => 'int',
             ));
        $this->hasColumn('status', 'int', null, array(
             'type' => 'int',
             ));
        $this->hasColumn('doctor_id', 'int', null, array(
             'type' => 'int',
             ));
        $this->hasColumn('appointment_type_id', 'int', null, array(
             'type' => 'int',
             ));
        $this->hasColumn('orphan', 'boolean', null, array(
             'type' => 'boolean',
             'default' => false,
             ));
        $this->hasColumn('appointment_template_id', 'int', null, array(
             'type' => 'int',
             ));
        $this->hasColumn('is_new_hl7', 'boolean', null, array(
             'type' => 'boolean',
             ));
        $this->hasColumn('doctor_reviewed', 'boolean', null, array(
             'type' => 'boolean',
             'default' => 0,
             ));
        $this->hasColumn('dummy_data', 'boolean', null, array(
             'type' => 'boolean',
             'default' => 0,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Client', array(
             'local' => 'client_id',
             'foreign' => 'id'));

        $this->hasOne('Location', array(
             'local' => 'location_id',
             'foreign' => 'id'));

        $this->hasOne('StageClient', array(
             'local' => 'stage_client_id',
             'foreign' => 'id'));

        $this->hasOne('Claim', array(
             'local' => 'claim_id',
             'foreign' => 'id'));

        $this->hasOne('ConsultationTemplate', array(
             'local' => 'consultation_template_id',
             'foreign' => 'id'));

        $this->hasOne('Doctor', array(
             'local' => 'doctor_id',
             'foreign' => 'id'));

        $this->hasOne('AppointmentType', array(
             'local' => 'appointment_type_id',
             'foreign' => 'id'));

        $this->hasOne('AppointmentTemplate', array(
             'local' => 'appointment_template_id',
             'foreign' => 'id'));

        $this->hasMany('AppointmentAttendee', array(
             'local' => 'id',
             'foreign' => 'appointment_id'));

        $this->hasMany('AppointmentStageTrack', array(
             'local' => 'id',
             'foreign' => 'appointment_id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $auditable0 = new Auditable();
        $versionable0 = new Doctrine_Template_Versionable(array(
             'tableName' => 'appointments_version',
             ));
        $this->actAs($timestampable0);
        $this->actAs($auditable0);
        $this->actAs($versionable0);
    }
}