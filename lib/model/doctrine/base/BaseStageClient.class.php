<?php

/**
 * BaseStageClient
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $stage_id
 * @property string $stage_name
 * @property integer $client_id
 * @property text $stage_following
 * @property string $hl7_trigger
 * @property integer $appointment_status
 * @property integer $go_to_appointment_status
 * @property boolean $active
 * @property Stage $Stage
 * @property Client $Client
 * @property Doctrine_Collection $Appointment
 * @property Doctrine_Collection $AppointmentStageTrack
 * 
 * @method integer             getStageId()                  Returns the current record's "stage_id" value
 * @method string              getStageName()                Returns the current record's "stage_name" value
 * @method integer             getClientId()                 Returns the current record's "client_id" value
 * @method text                getStageFollowing()           Returns the current record's "stage_following" value
 * @method string              getHl7Trigger()               Returns the current record's "hl7_trigger" value
 * @method integer             getAppointmentStatus()        Returns the current record's "appointment_status" value
 * @method integer             getGoToAppointmentStatus()    Returns the current record's "go_to_appointment_status" value
 * @method boolean             getActive()                   Returns the current record's "active" value
 * @method Stage               getStage()                    Returns the current record's "Stage" value
 * @method Client              getClient()                   Returns the current record's "Client" value
 * @method Doctrine_Collection getAppointment()              Returns the current record's "Appointment" collection
 * @method Doctrine_Collection getAppointmentStageTrack()    Returns the current record's "AppointmentStageTrack" collection
 * @method StageClient         setStageId()                  Sets the current record's "stage_id" value
 * @method StageClient         setStageName()                Sets the current record's "stage_name" value
 * @method StageClient         setClientId()                 Sets the current record's "client_id" value
 * @method StageClient         setStageFollowing()           Sets the current record's "stage_following" value
 * @method StageClient         setHl7Trigger()               Sets the current record's "hl7_trigger" value
 * @method StageClient         setAppointmentStatus()        Sets the current record's "appointment_status" value
 * @method StageClient         setGoToAppointmentStatus()    Sets the current record's "go_to_appointment_status" value
 * @method StageClient         setActive()                   Sets the current record's "active" value
 * @method StageClient         setStage()                    Sets the current record's "Stage" value
 * @method StageClient         setClient()                   Sets the current record's "Client" value
 * @method StageClient         setAppointment()              Sets the current record's "Appointment" collection
 * @method StageClient         setAppointmentStageTrack()    Sets the current record's "AppointmentStageTrack" collection
 * 
 * @package    HypeMedical
 * @subpackage model
 * @author     HYPE Systems
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseStageClient extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('stage_client');
        $this->hasColumn('stage_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('stage_name', 'string', 40, array(
             'type' => 'string',
             'length' => 40,
             ));
        $this->hasColumn('client_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('stage_following', 'text', null, array(
             'type' => 'text',
             ));
        $this->hasColumn('hl7_trigger', 'string', 3, array(
             'type' => 'string',
             'length' => 3,
             ));
        $this->hasColumn('appointment_status', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('go_to_appointment_status', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('active', 'boolean', null, array(
             'type' => 'boolean',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Stage', array(
             'local' => 'stage_id',
             'foreign' => 'id'));

        $this->hasOne('Client', array(
             'local' => 'client_id',
             'foreign' => 'id'));

        $this->hasMany('Appointment', array(
             'local' => 'id',
             'foreign' => 'stage_client_id'));

        $this->hasMany('AppointmentStageTrack', array(
             'local' => 'id',
             'foreign' => 'stage_client_id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $auditable0 = new Auditable(array(
             ));
        $versionable0 = new Doctrine_Template_Versionable(array(
             ));
        $this->actAs($timestampable0);
        $this->actAs($auditable0);
        $this->actAs($versionable0);
    }
}