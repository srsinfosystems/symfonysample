<?php

class ClientParam extends BaseClientParam
{
	public function getValue()
	{
		$type = gettype(ClientParamTable::getDefaultForValue($this->param));
		
		if ($type == 'boolean') {
			return (bool) $this->_get('value');
		}
		if ($type == 'integer') {
			return (integer) $this->_get('value');
		}
		if ($type == 'string') {
			return $this->_get('value');
		}
		if ($type == 'array') {
			return explode(',', $this->_get('value'));
		}
		if ($type == 'NULL') {
			return $this->_get('value');
		}
		var_dump($type, $this->param);
		exit();
		return $this->_get('value');
	}
}