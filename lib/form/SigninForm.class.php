<?php

class SigninForm extends BasesfGuardFormSignin {

    public function configure() {
        $this->disableLocalCSRFProtection();
        parent::configure();
    }

    public function setup() {
        $this->setWidgets(array(
            'username' => new sfWidgetFormInputText(array()),
            'password' => new sfWidgetFormInputPassword(array('type' => 'password')),
            'remember' => new sfWidgetFormInputCheckbox(),
        ));

        $this->setValidators(array(
            'username' => new sfValidatorString(),
            'password' => new sfValidatorString(array('required' => false)),
            'remember' => new sfValidatorBoolean(),
        ));

        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
            new sfGuardValidatorUser(),
            new hypeValidatorCallback(array('callback' => array($this, 'validateActiveUserAndClient'))),
        )));

        $this->widgetSchema->setNameFormat('signin[%s]');

        $decorator = new hypeFormSchemaFormatter($this->widgetSchema);
        $this->widgetSchema->addFormFormatter('custom', $decorator);
        $this->widgetSchema->setFormFormatterName('custom');
    }

    /*     * ** VALIDATOR FUNCTIONS *** */

    public function validateActiveUserAndClient($validator, $values) {
        if (array_key_exists('user', $values)) {
            if (!$values['user']->UserProfile->active) {
                $errors = new sfValidatorErrorSchema($validator, array(), array());
                $error = new sfValidatorError($validator, 'Inactive user');
                $errors->addError($error);
                throw $errors;
            }
            else if (!$values['user']->UserProfile->Client->canLogIn()) {
                $errors = new sfValidatorErrorSchema($validator, array(), array());
                $error = new sfValidatorError($validator, 'Subscription Expired');
                $errors->addError($error);
                throw $errors;
            }
        }
        return $values;
    }

}
