<?php

class SignupForm  extends BaseForm
{
    public function configure()
    {
        $this->setWidgets(array(
            'email' => new sfWidgetFormInputText(array(), array('style' => 'width: 200px')),
            'fname' => new sfWidgetFormInputText(array(), array('style' => 'width: 200px')),
            'lname' => new sfWidgetFormInputText(array(), array('style' => 'width: 200px')),
            'companyname' => new sfWidgetFormInputText(array(), array('style' => 'width: 200px')),
            'address' => new sfWidgetFormInputText(array(), array('style' => 'width: 200px')),
            'city' => new sfWidgetFormInputText(array(), array('style' => 'width: 200px')),
            'postal' => new sfWidgetFormInputText(array(), array('style' => 'width: 200px')),
            'phone' => new sfWidgetFormInputText(array(), array('style' => 'width: 200px')),
            'username' => new sfWidgetFormInputText(array(), array('style' => 'width: 200px')),
            'password' => new sfWidgetFormInputText(array(), array('style' => 'width: 200px')),
        ));

        $this->widgetSchema->setLabels(array(
            'email' => 'Email',
            'fname' => 'Client Name',
            'lname' => 'Client Name',
            'companyname' => 'Client Name',
            'address' => 'Client Name',
            'city' => 'Client Name',
            'phone' => 'Client Name',
            'username' => 'Client Name',
            'password' => 'Client Name',

        ));

        $this->setValidators(array(
            'email' => new sfValidatorString(array('required' => true, 'max_length' => '100'), array('required' => 'Email is required')),
            'lname' => new sfValidatorString(array('required' => true, 'max_length' => '100'), array('required' => 'Name is required')),
            'fname' => new sfValidatorString(array('required' => true, 'max_length' => '100'), array('required' => 'Name is required')),
            'companyname' => new sfValidatorString(array('required' => true, 'max_length' => '100'), array('required' => 'Company Name is required')),
            'address' => new sfValidatorString(array('required' => true, 'max_length' => '100'), array('required' => 'Address is required')),
            'city' => new sfValidatorString(array('required' => true, 'max_length' => '100'), array('required' => 'City is required')),
            'phone' => new sfValidatorString(array('required' => true, 'max_length' => '100'), array('required' => 'Phone is required')),
            'username' => new sfValidatorString(array('required' => true, 'max_length' => '100'), array('required' => 'Username is required')),
            'password' => new sfValidatorString(array('required' => true, 'max_length' => '100'), array('required' => 'Password is required')),
        ));

//        $this->validatorSchema->setPostValidator(
//            new sfValidatorDoctrineUnique(array('model' => 'Client', 'column' => array('name')), array('invalid' => 'We already have this client in our system.'))
//        );
//
//        $this->widgetSchema->setNameFormat('client[%s]');
//
//        $decorator = new hypeFormSchemaFormatter($this->widgetSchema);
//        $this->widgetSchema->addFormFormatter('custom', $decorator);
//        $this->widgetSchema->setFormFormatterName('custom');
    }

//    protected function doSave($con = null)
//    {
//        $values = $this->getValues();
//        $values = array_map('strtoupper', $values);
//
//        $this->getObject()->name = $values['name'];
//
//        if (!$this->getObject()->id) {
//            $user_types = new Doctrine_Collection('UserType');
//
//            $ut = new UserType();
//            $ut->name = 'ADMINISTRATOR';
//            $this->getObject()->UserType->add($ut);
//
//            $ut = new UserType();
//            $ut->name = 'BASE USER';
//            $this->getObject()->UserType->add($ut);
//        }
//
//        $this->getObject()->save();
//
//        return $this->getObject();
//    }
}
