<?php

class BaseForm extends sfFormSymfony
{
	public function setValue($field, $value) {
		$this->values[$field] = $value; // set the value for this request
		$this->taintedValues[$field] = $value; // override the value entered by the user
		$this->resetFormFields(); // force a refresh on the field schema
	}
	public function processHypeValidatorDate($v) {
		if ($v) {
			return date(hype::DB_ISO_DATE, $v);
		}

		return $v;
	}
	public function renderBottomErrorBox()
	{
		if ($this->widgetSchema->getFormFormatterName() == 'custom') {
			return $this->widgetSchema->getFormFormatter()->formatErrorsDiv($this->getErrorsWithLabels($this->getWidgetSchema(), $this->getErrorSchema()));
		}
        return '';
	}
	public function getErrorsWithLabels($widgets, $errors)
	{
		$errs = array();

		foreach ($errors->getGlobalErrors() as $error) {
			$errs[] = array('', $error->getMessage());
		}

		foreach ($errors->getNamedErrors() as $name => $error)
		{
			if (($error instanceof ArrayAccess || is_array($error)) && method_exists($widgets[$name],'getWidgetSchema')) {
					$errs = array_merge($errs, $this->getErrorsWithLabels($widgets[$name]->getWidgetSchema(), $errors[$name]->getErrorSchema()));
			}
			else {
				if ($name) {
					$label = $widgets[$name]->getLabel();
					if ($label == '') {
						$label = ucwords(str_replace('_', ' ', $name));
					}
					$errs[] = array($label, $error->getMessage());
				}
				else {
					$errs[] = array('', $error->getMessage());
				}
			}
		}
		return $errs;
	}

	/** VALIDATORS FOR CLAIM FORM AND OTHER "SAVE ANYWAYS" COMPLIANT FORMS **/
	public function validateBoolean($validator, $value, $arguments)
	{
		if (!array_key_exists('fieldName', $arguments)) {
			throw new Exception('validateBoolean requires $arguments key of "fieldName"');
		}

		if (in_array($value, array('true', 't', 'yes', 'y', 'on', '1'))) {
			$value = true;
		}
		else if (in_array($value, array('false', 'f', 'no', 'n', 'off', '0'))) {
			$value = false;
		}
		else {
			$this->taintedValues[$arguments['fieldName']] = null;
			throw new sfValidatorError($validator, 'invalid');
		}
		$this->taintedValues[$arguments['fieldName']] = $value;
		return $value;
	}
	public function validateString($validator, $value, $arguments)
	{
		if (!array_key_exists('fieldName', $arguments)) {
			throw new Exception('validateString requires $arguments key of "fieldName"');
		}

		$value = trim(strtoupper($value));

		if (array_key_exists('maxLength', $arguments)) {
			if (strlen($value) > $arguments['maxLength']) {
				$this->taintedValues[$arguments['fieldName']] = null;
				throw new sfValidatorError($validator, 'maxLength', array('length' => $arguments['maxLength']));
			}
		}

		return $value;
	}
	public function validateRegex($validator, $value, $arguments)
	{
		if (!array_key_exists('fieldName', $arguments)) {
			throw new Exception('validateRegex requires $arguments key of "fieldName"');
		}
		if (!array_key_exists('regex', $arguments)) {
			throw new Exception('validateRegex requires $arguments key of "regex"');
		}

		$value = trim(strtoupper($value));

		if ($value) {
			if (array_key_exists('maxLength', $arguments)) {
				if (strlen($value) > $arguments['maxLength']) {
					$this->taintedValues[$arguments['fieldName']] = null;
					throw new sfValidatorError($validator, 'maxLength', array('length' => $arguments['maxLength']));
				}
			}

			if (!preg_match($arguments['regex'], $value, $matches)) {
				$this->taintedValues[$arguments['fieldName']] = null;
				throw new sfValidatorError($validator, 'regex');
			}
		}
		return $value;
	}
	public function validateDate($validator, $value, $arguments)
	{
		if (!array_key_exists('fieldName', $arguments)) {
			throw new Exception('validateDate requires $arguments key of "fieldName"');
		}
		if (!array_key_exists('format', $arguments)) {
			throw new Exception('validateDate requires $arguments key of "format"');
		}

		$value = strtoupper(trim($value));

		if ($value) {
			$date = null;
			try {
				$date = DateTime::createFromFormat($arguments['format'], $value);
			}
			catch (Exception $e) {
				$this->taintedValues[$arguments['fieldName']] = null;
				throw new sfValidatorError($validator, 'invalid', array('format' => $arguments['screen_format']));
			}

//                        echo $date;exit;
			if (array_key_exists('minimum_year', $arguments)) {
//                            if($date){
                                if ($date->format('Y') < $arguments['minimum_year']) {
					$this->taintedValues[$arguments['fieldName']] = null;
					throw new sfValidatorError($validator, 'minimum_year', array('format' => $arguments['screen_format']));
				}
//                            }				
			}

			if (array_key_exists('with_time', $arguments) && !$arguments['with_time']) {
//                            if($date){
                                $date->setTime(0, 0, 0);
//                            }				
			}

//                        if($date){
                            $value = $date->getTimestamp();
//                        }
			
			if (!$value) {
				$value = $date->format(hype::DB_ISO_DATE);
			}
		}

 		$this->taintedValues[$arguments['fieldName']] = $value;
		return $value;
	}
	public function validateArray($validator, $value, $arguments)
	{
		if (!array_key_exists('fieldName', $arguments)) {
			throw new Exception('validateArray requires $arguments key of "fieldName"');
		}
		if (!array_key_exists('options', $arguments)) {
			throw new Exception('validateArray requires $arguments key of "options"');
		}

		$value = strtoupper(trim($value));

		if ($value) {
			if (!in_array($value, $arguments['options'])) {
				$this->taintedValues[$arguments['fieldName']] = null;
				throw new sfValidatorError($validator, 'invalid');
			}
		}

		$this->taintedValues[$arguments['fieldName']] = $value;
		return $value;
	}
	public function validateFloat($validator, $value, $arguments)
	{
		if (!array_key_exists('fieldName', $arguments)) {
			throw new Exception('validateFloat requires $arguments key of "fieldName"');
		}

		if (!is_numeric($value)) {
			$this->taintedValues[$arguments['fieldName']] = null;
			throw new sfValidatorError($validator, 'invalid');
		}

		$value = floatval($value);

		if ($value) {
			if (array_key_exists('min', $arguments) && $arguments['min'] > $value) {
				$this->taintedValues[$arguments['fieldName']] = $value;
				throw new sfValidatorError($validator, 'min');
			}
			if (array_key_exists('max', $arguments) && $arguments['max'] < $value) {
				$this->taintedValues[$arguments['fieldName']] = $value;
				throw new sfValidatorError($validator, 'max');
			}

		}

		$this->taintedValues[$arguments['fieldName']] = $value;
		return $value;
	}


 	public function validateSli($validator, $value, $options = array())
 	{
		$value = trim(strtoupper($value));

		if ($value) {
			$valid = false;

			if (preg_match('/[a-z]{3}/i', $value, $matches) && strlen($value) == 3) {
				$valid = true;
			}
			else if (preg_match('/[0-9]{4}/i', $value, $matches) && strlen($value) == 4) {
				$valid = true;
			}

			if (!$valid && array_key_exists('clearTaintedValues', $options) && $options['clearTaintedValues']) {
 				$this->taintedValues['sli'] = null;
				throw new sfValidatorError($validator, 'invalid');
			}
			else if (!$valid) {
				throw new sfValidatorError($validator, 'invalid');
			}
		}

 		return $value;
 	}
}
