<?php

class AuditableListener extends Doctrine_Record_Listener
{
	public function preInsert(Doctrine_Event $event)
	{
		if (sfContext::hasInstance()) {
			$user_id = sfContext::getInstance()->getUser()->getUserID();
			$event->getInvoker()->created_by = $user_id;
			$event->getInvoker()->updated_by = $user_id;
		}
 		else {
 			$event->getInvoker()->created_by = null;
 			$event->getInvoker()->updated_by = null;
 		}
	}
	public function preUpdate(Doctrine_Event $event)
	{
		if (sfContext::hasInstance()) {
			$user_id = sfContext::getInstance()->getUser()->getUserID();
			$event->getInvoker()->updated_by = $user_id;
		}
 		else {
 			$event->getInvoker()->updated_by = null;
 		}
	}
}
