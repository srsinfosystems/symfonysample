<?php

class Auditable extends Doctrine_Template
{
	public function setTableDefinition()
	{
		$this->hasColumn('created_by', 'int');
		$this->hasColumn('updated_by', 'int');

		$this->addListener(new AuditableListener());
	}
	public function setUp()
	{
	}
}