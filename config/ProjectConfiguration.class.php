<?php
$currentDir = getcwd();
// check dir is in web or not

$chkDir = explode('/',$currentDir);
if(in_array('web', $chkDir)){
    $url = getcwd().'/symfony/lib/autoload/sfCoreAutoload.class.php';
} else {
    $url = getcwd().'/web/symfony/lib/autoload/sfCoreAutoload.class.php';
}

require_once $url;
//require_once 'C:\Users\Scott\Documents\symfony\lib\autoload\sfCoreAutoload.class.php';


sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
	public function setup()
	{
		$this->enablePlugins(array(
			'sfDoctrinePlugin',
			'sfDoctrineGuardPlugin',
            'uUtilitiesPlugin'
		));

		$this->dispatcher->connect('application.throw_exception', array($this, 'handleException'));
	}

	public function handleException(sfEvent $event)
	{
		if ($event->getReturnValue() != 'emailed') {
			$handler = new hypeException($event);
			$handler->setContext(sfContext::getInstance());
			$handler->sendEmail();

			$event->setReturnValue('emailed');
		}
	}
}

